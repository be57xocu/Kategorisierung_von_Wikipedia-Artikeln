Gerhard Mercator (* 5. März 1512 in Rupelmonde, Grafschaft Flandern; † 2. Dezember 1594 in Duisburg, Vereinigte Herzogtümer Jülich-Kleve-Berg) war ein Geograph und Kartograf, der schon zu Lebzeiten als der Ptolemäus seiner Zeit angesehen wurde und bis in die arabisch-islamische Welt berühmt war. Er hieß eigentlich Gerard de Kremer (latinisiert Gerardus Mercator, deutsch auch Gerhard Krämer).

Heute vorwiegend als Kartograf und Globenhersteller bekannt, war Mercator im 16. Jahrhundert auch als Kosmograf, Theologe und Philosoph von großer Bedeutung und setzte Maßstäbe als Schriftkünstler. Auf Mercator bezieht sich die 1996 gegründete Stiftung Mercator, die dem interkulturellen Wissensaustausch gewidmet ist.

Gerhard De Kremer wurde als sechstes Kind von Hubert De Cremer und dessen Ehefrau Emerance in Rupelmonde geboren. Seine Kindheit verbrachte er in Gangelt, wo sein Vater Schuhmacher war.[2] Nach dem Tod seines Vaters im Jahre 1526 wurde Gerhard bei den Brüdern vom gemeinsamen Leben in Herzogenbusch erzogen. Hier war unter anderem Georgius Macropedius sein Lehrer.

Ab 1530 studierte Gerhard an der Universität Löwen unter Gemma Frisius[3], wurde 1532 zum Magister artium promoviert und betrieb anschließend private Studien der Theologie, Philosophie und Mathematik, vor allem in ihren praktischen Anwendungsmöglichkeiten (Karten, Globen und Instrumente). 1534 bis 1537 war Mercator als Mitarbeiter bei Gemma Frisius an der Erstellung von dessen Erd- und Himmelsgloben beteiligt.

Im Jahre 1536 heiratete er Barbara Schellekens, die 1537 ihren ersten Sohn Arnold gebar. Im Jahre 1537 schuf er eine erste Karte Amplissima Terrae Sanctae descriptio ad utriusque Testamenti intelligentiam. Im Jahre 1538 schuf er seine erste Weltkarte.[4] Im selben Jahr wurde seine erste Tochter geboren, die nach Mercators Mutter Emerance getauft wurde. 1539 folgte die zweite Tochter Dorothéa.

Ebenfalls 1539 brachte Mercator eine Karte von Flandern heraus, die ihm die Aufmerksamkeit des Königs einbrachte. Im Jahre 1541 brachte Mercator seinen ersten Globus heraus, der sich über Jahrzehnte in großen Stückzahlen verkaufte. In den Jahren 1540 und 1541 wurden die beiden Söhne Bartholomäus (1540–1568)[3] und Rumold geboren. Im Jahre 1542 folgte als sechstes und letztes Kind die Tochter Katharina.

Ob Mercator den theologischen Doktorgrad erworben hat, ist fraglich. 1544 wurde er wegen „Lutherey“ (also als Anhänger der Reformation Martin Luthers) verhaftet und für viele Monate eingekerkert. In dieser Zeit erschien das naturwissenschaftliche Hauptwerk Philipp Melanchthons Initia Doctrinae Physicae (1549), das großen Einfluss auf Mercator ausgeübt haben dürfte. Der erst vor kurzem aufgefundene Briefwechsel mit Melanchthon ist noch nicht ausgewertet.

1551 fertigte Mercator zum ersten Mal den komplementären Himmelsglobus zu seinem Globus von 1541. Fortan wurden diese Globen zumeist in Paaren verkauft, wovon noch mindestens 22 existieren.

1551 lud ihn Wilhelm der Reiche ein, Professor für Kosmografie an der neu zu gründenden Universität Duisburg zu werden. Mercator nahm das Angebot an und übersiedelte 1552 nach Duisburg, wo er ein Haus an der Oberstraße bewohnte;[5]  ob Glaubensgründe eine entscheidende Rolle spielten, ist umstritten. Jedenfalls lebte er in der von religiöser Toleranz geprägten Stadt im Herzogtum Kleve vor Anfeindungen sicher.

Von 1559 bis 1562 war er am neugegründeten Duisburger Akademischen Gymnasium, dem heutigen Landfermann-Gymnasium, als Lehrer für Mathematik und Kosmografie tätig. Ein Student Mercators war ab 1562 Johannes Corputius, der 1566 die nach ihm benannte außergewöhnlich exakte Stadtansicht Duisburgs, den Plan des Corputius fertigte. Die Gründung der Universität kam zu Mercators Lebzeiten nicht mehr zustande, obwohl der Herzog 1564 die päpstliche Erlaubnis und 1566 auch das kaiserliche Privileg zur Gründung erhalten hatte.

Mit seiner großen Weltkarte von 1569 (Nova et aucta orbis terrae descriptio ad usum navigantium) erlangte Mercator Weltruhm. Möglicherweise angeregt von Erhard Etzlaub, entwickelte er eine bis heute wegen ihrer Winkeltreue für die Seefahrt (und Luftfahrt) wichtige Projektion, die als „Mercator-Projektion“ bekannt wurde.
Laut John Vermeulen hat Mercator mit seinem Zeitgenossen Abraham Ortelius zusammengearbeitet.

In den 1570er Jahren hat Mercator theologische Schriften veröffentlicht, die größtenteils bis vor kurzem unbekannt, zumindest unbeachtet waren. Er befasste sich mit der Theologie der Schweizer Reformierten. Auch dürfte er von der sogenannten „Christlichen Physik“ des Lambertus Danaeus, einem Schüler Calvins, angeregt worden sein.

Etwa gleichzeitig mit der Weltkarte erschien Mercators theologisch geprägte Weltgeschichte Chronologia. Hoc est temporum demonstratio exactissima, ab initio mundi usque ad annum Domini MDLXVIII (1568). Nach weiteren kartografischen Arbeiten folgte im Jahr 1590 der Kommentar des Römerbriefs, der bislang nur als Manuskript in Leiden vorhanden ist, in dem er die theologisch-systematischen Grundlagen für seine Kosmografie erarbeitete.

Kurz vor seinem Tod vollendete Mercator das Hauptwerk, die Kosmografie Atlas, sive Cosmographicae Meditationes de Fabrica Mundi et fabricati figura, die postum 1595 von Mercators Sohn Rumold herausgegeben wurde. Bislang wurden hauptsächlich die Karten zur Kenntnis genommen, gelegentlich erschienen diese sogar ohne den Text. Besonders hervorzuheben ist die christologische Ausrichtung, die sich grundsätzlich von den mittelalterlichen Kosmografien bis zu Sebastian Münster unterscheidet.

Mercator starb 1594 als angesehener und reicher Mann. Sein Grab in der Salvatorkirche zu Duisburg ist verschollen, nur sein prachtvolles Epitaph befindet sich noch dort.

Mercator sah sich selbst mehr als wissenschaftlichen Kosmografen und nicht als jemand, der seinen Lebensunterhalt mit der Herstellung und dem Verkauf von Karten verdienen musste. Seine Produktion war nicht recht umfangreich: Wir kennen zwölf Globen-Paare (Himmel und Erde), fünf Wandkarten, viele Karten von Regionen wie Weltkarten sowie eine Chronologie (mit Evangelienharmonie) und die Kosmografie. Viele seiner Werke sind heute in der Schatzkammer des Kultur- und Stadthistorischen Museums der Stadt Duisburg ausgestellt.

Aus der Duisburger Periode kennen wir nur vier Wandkarten:

Diese letzte Karte kann mit Recht Mercators Meisterwerk genannt werden. Es ist die erste Weltkarte, bei der eine winkeltreue Projektion verwendet wurde (Mercator-Projektion).

Nach der Ausgabe der Weltkarte verlegte Mercator sich mehr und mehr auf die Herstellung einer Kosmografie. Mercator hatte große Pläne: ein riesiges kosmografisches Werk über die Schöpfung, deren Ursprung und dessen Geschichte.

Die ersten Ideen dafür schrieb er 1569 in der Einleitung zu seiner Chronologia. Die Kosmografie würde aus fünf Teilen bestehen:

Mercators wissenschaftliche Einstellung wurde ihm zum Schicksal. Immer wieder verschob er in der Hoffnung auf neue Informationen die Herausgabe seines Werks. Der kartografische Teil seiner Kosmografie wurde daher nur teilweise vollendet.

Zuerst wurde seine Ptolemäus-Ausgabe von 1578 angefertigt. Mercator sah diese Ausgabe bloß als Darstellung der Welt nach den Ideen der klassischen Autoren. Die 28 Ptolemäischen Karten sind nie in einem anderen Atlas eingefügt, während sie noch im Jahr 1730 neu herausgegeben worden sind.

Erst im Jahr 1585, fünfzehn Jahre nach der Ausgabe des Theatrum, kam Mercator mit einer unvollendeten Ausgabe seiner 'modernen Geografie'.

Das Kartenbuch enthält 51 Karten: 16 von Frankreich, neun von den Niederlanden und 26 von Deutschland. Von diesen Ländern hatte er die zuverlässigsten Beschreibungen. Jeder Teil hat ein eigenes Titelblatt: Galliae Tabulae Geographicae, Belgii Inferioris Geographicae Tabulae und Germaniae tabulae geographicae. Das ganze hatte noch keinen Titel.

Im Jahr 1589 folgten 22 Karten von Südosteuropa, Italiae, Sclavoniae et Graeciae tabulae geographicae. Mercator hatte leider nicht die Möglichkeit, gemäß seiner ursprünglichen Planung seine Tabulae Geographicae zu einem richtigen Weltatlas mit einem Umfang von etwa 120 Karten zu erweitern.

Ein Jahr nach seinem Tod gab sein Sohn Rumold Mercator eine Ergänzung mit 34 Karten heraus. Hierin befinden sich 29 von Gerhard Mercator gravierte Karten der fehlenden Teile Europas (Island, die Britischen Inseln und die nord- und osteuropäischen Länder).

Rumolds „vollständige Ausgabe“ hat ein eigenes Titelblatt und Vorwort. Der Titel lautet Atlas sive Cosmographicae Meditationes de Fabrica Mundi et Fabricati Figura (Atlas oder kosmografische Meditationen über die Schöpfung der Welt und die Form der Schöpfung).

Die Wahl des Titels Atlas erläuterte Mercator in einer Einleitung. Danach ist der Name von dem mythischen König und ersten Astrologen Atlas von Mauretanien abgeleitet,[6] der laut Mercators Darstellung und beigegebener Stammtafel der Atlas der griechischen Mythologie war. Mercator beruft sich dabei auf die Erzählungen bei Diodor.[7]
„Meine Bestimmung ist es also, es diesem Atlas nachzutun, einem in Belesenheit, Menschlichkeit und Weisheit so herausragendem Mann, wie von einem hohen Wachturm aus die Kosmografie zu betrachten, so weit meine Kraft und Fähigkeit es erlauben, um zu sehen, ob ich möglicherweise durch meinen Fleiß einige Wahrheiten in noch unbekannten Dingen finden kann, welche dem Studium der Weisheit dienen könnten.“

Atlas ist auf dem Titelkupfer von Mercators Atlas mit einem Himmels- und einem Erden-Globus abgebildet.

Das Werk beginnt mit einer Biografie von Gerardus Mercator, verfasst von dem Duisburger Ratsherrn Walter Ghim, darauf folgt die Atlas-Mythologie. Den ersten Teil bildet Mercators Werk über die Schöpfung Mundi Creatione et Fabrica Liber. Die 107 Karten bilden den zweiten Teil.

Bezeichnend für sein Verständnis als Wissenschaftler der Renaissance ist, dass die Anschaulichkeit seines Denkens von einem hohen Anspruch an die Ästhetik
der Darstellung durchdrungen war. In der Geschichte der Schriftentwicklung nimmt Mercator auch einen bedeutenden Platz als Schreibmeister ein.[8][9][10]
1540 brachte er eine Anleitung zum Schreiben der italienischen Kursive heraus: Literarum latinarum, quas Italicas, cursoriasque vocant scribendarum ratio.[11] Damit gehörte er nicht nur zu den ersten Schriftkünstlern nördlich der Alpen, der im Rahmen eines Schreibmeisterbuchs diesen platzsparenden und klaren lateinischen Schrifttyp gegenüber der gotischen Schrift popularisierte, sondern er war auch der Erste, der diese Kursive für die Beschriftung der Karten verwendete.

Seine Unterweisungen (52 Seiten) waren zunächst noch in Holz geschnitten und erfuhren bereits im selben Jahr sowie 1549, 1550, 1556 und 1557 Nachauflagen.[12]
Bei der späteren Anwendung des Kupferstichs konnte die elegante Gestaltung dieser Schrift besser zur Geltung kommen. Mercator setzte damit ästhetische Maßstäbe für die Beschriftung der Karten und prägte so den Kartenstil für die nachfolgenden zweihundert Jahre.

Die 1655 gegründete Universität Duisburg wird 1818 geschlossen. Die im Jahre 1972 neu gegründete Gesamthochschule Duisburg hatte seit 1994 den Namen Gerhard-Mercator-Universität, bis sie 2003 mit der Universität Essen zur neuen Universität Duisburg-Essen fusionierte. Am Campus Duisburg besteht die Mercator School of Management.

Des Weiteren sind das Duisburger Mercator-Gymnasium, eine Berliner Grundschule, ein Veranstaltungs- und Kongresszentrum (Mercatorhalle), ein Einkaufszentrum in Duisburg-Meiderich sowie ein Moerser Berufskolleg nach ihm benannt, ebenso die südliche Ringstraße Mercatorstraße/Kremerstraße in der Duisburger Innenstadt und ein Duisburger Ausflugsdampfer. In der Mercator-Kaserne in Euskirchen ist u. a. das Amt für Geoinformationswesen der Bundeswehr stationiert.

Seit 1950 besteht auch die Mercator-Gesellschaft Duisburg e. V. – Verein für Geschichte und Heimatkunde.

Die Mercatorplakette für besondere Verdienste, insbesondere auf wissenschaftlichem oder künstlerischem Gebiet wird von der Stadt Duisburg in unregelmäßigen Abständen verliehen. Träger waren bislang zum Beispiel Gründungsrektoren der Duisburger Universität sowie Kulturdezernenten und Generalmusikdirektoren, aber auch der Schauspieler Hans Caninenberg und der Maler Heinz Trökes.

Darüber hinaus existiert die Stiftung Mercator. Diese Stiftung ist eine der großen Stiftungen in Deutschland, die insbesondere in den Bereichen Integration, Klimawandel und kulturelle Bildung aktiv ist. Projekte, wie „Förderunterricht für Kinder und Jugendliche mit Migrationshintergrund“, „jamtruck“ spiegeln die Vielfalt möglichen Engagements wider.

Der Mondkrater Mercator wurde 1935 nach ihm benannt, der Asteroid (4798) Mercator trägt seit 1991 seinen Namen.

Über Mercator

Werke online

Ausstellungen
