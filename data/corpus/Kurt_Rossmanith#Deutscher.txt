Kurt J. Rossmanith (* 22. November 1944 in Raase, Landkreis Freudenthal im Reichsgau Sudetenland) ist ein deutscher Politiker (CSU). 

Nach der Mittleren Reife an der Staatlichen Realschule Kaufbeuren absolvierte Rossmanith eine Lehre zum Industriekaufmann und leistete anschließend von 1963 bis 1965 seinen Wehrdienst ab. Danach war er bis 1971 als Leiter der Exportabteilung eines Unternehmens tätig und arbeitete nach einer erneuten Ausbildung von 1974 bis 1980 als Berufsberater.

Seit seinem Ausscheiden aus dem Bundestag ist er für eine Unternehmensberatung in Berlin tätig. Er ist Reserveoffizier im Dienstgrad Oberst.

Kurt Rossmanith ist verheiratet hat vier Kinder aus erster Ehe.

Rossmanith trat 1967 in die CSU ein und gehört dem Vorstand des CSU-Bezirksverbandes Schwaben an.

Er saß von 1978 bis 1999 im Kreistag des Landkreises Ostallgäu und war von 1980 bis 2009 Mitglied des Deutschen Bundestages. Dort arbeitete Rossmanith seit 1994 als Vorsitzender der Parlamentsgruppe Luft- und Raumfahrt und ab 2004 auch als Vorsitzender der Deutsch-Brasilianischen  Parlamentariergruppe. Von 1997 bis 1998 war er außerdem Vorsitzender des Verteidigungsausschusses des Bundestages.

In der Diskussion um eine Namensänderung der ehemals nach dem Wehrmacht-Generaloberst Eduard Dietl benannten Kaserne in Füssen in den 1980er/1990er Jahren trat Rossmanith als Gegner der Umbenennung in Erscheinung: „Generaloberst Dietl war und ist für mich auch heute noch Vorbild in menschlichem und soldatischem Handeln.“

Kurt Rossmanith ist stets als direkt gewählter Abgeordneter des Wahlkreises Ostallgäu in den Bundestag eingezogen. Bei der Bundestagswahl 2005 erreichte er hier 60,9 % der Erststimmen.

Franz Josef Strauß (1952–1953) |
Richard Jaeger (1953–1965) |
Friedrich Zimmermann (1965–1972) |
Hermann Schmidt (1972–1975) |
Werner Buchstaller (1975–1976) |
Manfred Wörner (1976–1980) |
Werner Marx (1980–1982) |
Alfred Biehle (1982–1990) |
Uwe Ronneburger (1990) |
Fritz Wittmann (1990–1994) |
Klaus Rose (1994–1997) |
Kurt Rossmanith (1997–1998) |
Helmut Wieczorek (1998–2002) |
Reinhold Robbe (2002–2005) |
Ulrike Merten (2005–2009) |
Susanne Kastner (2009–2013) |
Hans-Peter Bartels (2014–2015) |
Wolfgang Hellmich (seit 2015)
