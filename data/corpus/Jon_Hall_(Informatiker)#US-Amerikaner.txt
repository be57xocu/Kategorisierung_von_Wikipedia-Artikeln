Jon »Maddog« Hall (* 7. August 1950) ist einer der Gründer der internationalen Open-Source-Initiative.

Er ist seit 1969 in der Computerbranche und nutzt, laut seiner offiziellen Biografie, seit 1977 Unix und seit 1994 Linux. Seit 1995 ist er Chef von Linux International, einer in den USA beheimateten gemeinnützigen Organisation zur Förderung von Linux.

Er hat an der Drexel University und später am Rensselaer Polytechnic Institute in Troy (New York) studiert. Nach seinem Studium arbeitete er zunächst bei Aetna Life und Casualty an IBM-Mainframes. Dann wurde er Dekan der Informatikfakultät des Hartford State Technical College, wo seine Studenten begannen, ihn mad dog (deutsch: verrückter Hund) zu nennen. Später wurde er Systemadministrator bei den Bell Laboratories um dann sechzehn Jahre lang bei DEC (Digital Equipment Corporation) zu arbeiten. In dieser Zeit traf er auch Linus Torvalds und unterstützte diesen bei der Portierung von Linux auf den 64-Bit-Prozessor Alpha.

Im Juni 2012 veröffentlichte Hall einen Artikel im Linux-Magazin, zu Ehren von Alan Turing, in dem er bekannt gab, dass er homosexuell ist.[1]