Stefano Aurelio Bacciarini (* 8. November 1873 in Lavertezzo; † 27. Juni 1935 in Sorengo) war Apostolischer Administrator des Tessins.

Aurelio Bacciarini wuchs in bescheidenen Verhältnissen auf. Er studierte am Knabenseminar in Barlassina bei Mailand und an den Priesterseminaren in Lugano, Mailand und Monza. Seine Studien schloss er mit dem Doktorat in Theologie ab.

1897 wurde er zum Priester geweiht. 1897–1903 war er Pfarrer in Arzo, und dann 1903–1906 Spiritual am Knabenseminar von Pollegio. 1906 trat Stefano Aurelio Bacciarini in die Kongregation der Servi della Carità ein. 1912 wurde er Pfarrer zu S. Giuseppe al Trionfale in Rom. Beim Erdbeben in den Abruzzen von 1915 brachte er unter eigener Lebensgefahr den Verunglückten Hilfe. Beim Tode Don Luigi Guanellas am 24. Oktober 1915 wurde er General-Vorsteher seiner Kongregation

Am 12. Januar 1917 wurde er Apostolischer Administrator des Tessins. Die Bischofsweihe fand am 21. Januar 1917 in Rom statt. 1921–1924 war er Oberer der Servi della Carità.

Trotz seiner schwachen Gesundheit bewirkte er dank seiner großen Selbstdisziplin die religiöse Erneuerung seiner Diözese unter Betonung der Erfüllung religiöser Pflichten und des traditionellen Glaubenslebens. Im Sinne der Katholischen Aktion förderte er die Bildung von Vereinen und katholischen Gewerkschaften. 1926 gründete er die Tageszeitung Giornale del Popolo. Aurelio Bacciarini stellte das Tessin unter den Schutz der Madonna del Sasso. Bischof Bacciarini war von 1932 bis 1933 Vorsitzender der Schweizer Bischofskonferenz (SBK).

Schon 1947 wurde sein Seligsprechungsprozess eingeleitet, der aber nicht abgeschlossen wurde. Die angebliche wunderbare Heilung einer in Lugano wohnhaften Person veranlasste den Tessiner Bischof Pier Giacomo Grampa am 27. März 2006 das diözesane Verfahren für den Seligsprechungsprozess wieder zu aktivieren. Am 22. Mai 2006 wurde das Dossier an Postulator Mario Carrera übergeben, der den Antrag vor der vatikanischen Kongregation für die Seligsprechungsverfahren vertreten wird.

Pierre-François de Preux |
Etienne Marilley |
Carl Johann Greith |
Eugène Lachat |
Gaspard Mermillod |
Adrien Jardinier |
Augustin Egger |
Johannes Fidelis Battaglia |
Jules-Maurice Abbet |
Jakob Stammler |
Georg Schmid von Grüneck |
Aurelio Bacciarini |
Viktor Bieler |
Angelo Jelmini |
Johannes Vonderach |
François-Nestor Adam |
Anton Hänggi |
Pierre Mamie |
Otmar Mäder |
Henri Schwery |
Otmar Mäder |
Pierre Mamie |
Henri Salina |
Amédée Grab |
Kurt Koch |
Norbert Brunner |
Markus Büchel |
Charles Morerod (seit 2016)
