Karl Adolph Gjellerup (* 2. Juni 1857 in Roholte, Dänemark; † 11. Oktober 1919 in Klotzsche bei Dresden, Deutsches Reich) war ein dänischer Schriftsteller und Literaturnobelpreisträger.

Der Sohn des Pastors Carl Adolph Gjellerup und seiner Frau Anna Fibiger wurde 1857 in Roholte (Faxe Kommune, etwa 6 km südwestlich von Faxe Ladeplads) geboren. Der Vater starb drei Jahre später, sodass die Mutter mit ihrem Sohn noch im selben Jahr zu ihrem Cousin, dem Schriftsteller und Pastor Johannes Fibiger nach Kopenhagen zog. Bereits zu Schulzeiten begann Karl Gjellerup zu schreiben, kurz nach Abschluss der Schule entstanden die Stücke Scipio Africanus und Arminius. Karl Gjellerup sollte eigentlich wie sein Vater Pastor werden und absolvierte in Kopenhagen ein Studium der Theologie, das er 1878 mit Summa cum laude abschloss. Nach dem Studium wandte er sich erneut der Literatur zu und veröffentlichte im November 1878 En idealist (dt. Ein Idealist) und Den evige Strid, die beide am selben Tag und unter einem Pseudonym erschienen. Der Erfolg der beiden Werke brachte Gjellerup schon bald mit den Künstlern seiner Zeit wie Georg Brandes, Holger Drachmann und Jens Peter Jacobsen zusammen. Es folgten weitere Werke wie Arvelighed og Moral (1881), Germanernes Lærling (dt. Ein Jünger der Germanen; 1882) oder Aander og Tider (1882), das sich mit Charles Darwin beschäftigte. Eine Erbschaft ermöglichte Karl Gjellerup, Europa zu bereisen, und so lebte er 1883 mehrere Monate in Rom; sein Rückweg führte ihn durch die Schweiz, Griechenland, Russland und Deutschland. Seine Notizen zu diesen Reisen, wie auch der gesamte schriftliche Nachlass Gjellerups, werden heute von der Sächsischen Staats- und Universitätsbibliothek Dresden aufbewahrt und wurden teilweise in den Werken En klassisk Maaned (1884) und Vandreaaret (1885) veröffentlicht.

Seine lyrische Tragödie Brynhild (1884) wurde Karl Gjellerups endgültiger literarischer Durchbruch. Er hatte sie seiner Geliebten Eugenia Bendix (geb. Heusinger) gewidmet, einer gebürtigen Dresdnerin (zuvor Ehefrau von Fritz Bendix), die er am 24. Oktober 1887 heiratete. Karl Gjellerup lebte von 1885 bis 1887 in Dresden, auch sein weitgehend biographischer Roman Minna (1889; dt. Seit ich zuerst sie sah, 1918) spielt hauptsächlich in Dresden und Rathen und ist die nur wenig verschlüsselte Liebesgeschichte mit Eugenie. Im März 1892 ließ er sich endgültig in Dresden nieder. Gjellerup, der neben Deutsch, Englisch und Französisch auch die griechische Sprache beherrschte, veröffentlichte seine Werke bis in die 1890er Jahre auf Dänisch, bevor er mit seinem Werk Pastor Mors (1894) dazu überging, mit Unterstützung seiner Frau auf Deutsch zu schreiben. Nach 1898 erschienen die meisten seiner Werke in deutscher Sprache, seine Vorbilder waren neben Schiller, Goethe und Heine auch Kant, Schopenhauer und Nietzsche.


Gjellerup durchlief eine intensive buddhistische Phase, wofür sein Roman Der Pilger Kamanita (1906) und das Drama Das Weib des Vollendeten (1907) wichtige Beispiele sind. Der Autor verbindet dabei den Buddhismus mit dem Motiv der romantischen Liebe, wodurch er sich im Gegensatz zum damals vorherrschenden Bild vom Buddhismus in Europa befand: 
„Gjellerups Buddhismus kennt mit der Beziehung zweier Menschen bis zum Nirvana einen positiven Wert im Weltlichen. Die Wiedergeburten erhalten erst unmittelbar vor dem Eintritt der Erlösung überwiegend leidhaften Charakter, der sich in sehr subtilen Formen der Erkenntnis der Nichtdauer zeigt. Zuvor wird das Wandern durch die Welten mit allen Irren und Wirren als große Pilgerreise nicht pessimistisch empfunden, sondern als Prozeß des Reifens.“[1]
Im Jahr 1917 erhielt Karl Gjellerup zusammen mit Henrik Pontoppidan den Literaturnobelpreis. Während Pontoppidan für sein Werk Der Teufel am Herd ausgezeichnet wurde, ehrte man Gjellerup „für seine vielseitig reiche und von hohen Idealen getragene Dichtung“.[2] Wegen des Ersten Weltkrieges fand jedoch keine Zeremonie statt, sodass Gjellerup die Verleihungsmappe und Medaille im Juni 1918 zugesandt bekam.[3] Für das Preisgeld erfüllte er sich einen lang gehegten Traum und kaufte sich im September 1918 die „Villa Baldur“ im Dresdner Vorort Klotzsche. Nur ein Jahr später starb Gjellerup; er liegt auf dem Alten Friedhof in Klotzsche begraben.

In heutigen Dresdner Stadtteil Klotzsche wurde eine im Jahr 2004 angelegte Karl-Gjellerup-Straße (Lage51.116213.7678) nach ihm benannt.[4]
Prudhomme (1901) |
Mommsen (1902) |
Bjørnson (1903) |
F. Mistral/Echegaray (1904) |
Sienkiewicz (1905) |
Carducci (1906) |
Kipling (1907) |
Eucken (1908) |
Lagerlöf (1909) |
Heyse (1910) |
Maeterlinck (1911) |
Hauptmann (1912) |
Tagore (1913) |
nicht verliehen (1914) |
Rolland (1915) |
Heidenstam (1916) |
Gjellerup/Pontoppidan (1917) |
nicht verliehen (1918) |
Spitteler (1919) |
Hamsun (1920) |
France (1921) |
Benavente (1922) |
Yeats (1923) |
Reymont (1924) |
Shaw (1925) |
Deledda (1926) |
Bergson (1927) |
Undset (1928) |
Mann (1929) |
Lewis (1930) |
Karlfeldt (1931) |
Galsworthy (1932) |
Bunin (1933) |
Pirandello (1934) |
nicht verliehen (1935) |
O’Neill (1936) |
Martin du Gard (1937) |
Buck (1938) |
Sillanpää (1939) |
nicht verliehen (1940–1943) |
Jensen (1944) |
G. Mistral (1945) |
Hesse (1946) |
Gide (1947) |
Eliot (1948) |
Faulkner (1949) |
Russell (1950) |
Lagerkvist (1951) |
Mauriac (1952) |
Churchill (1953) |
Hemingway (1954) |
Laxness (1955) |
Jiménez (1956) |
Camus (1957) |
Pasternak (1958) |
Quasimodo (1959) |
Perse (1960) |
Andrić (1961) |
Steinbeck (1962) |
Seferis (1963) |
Sartre (1964) |
Scholochow (1965) |
Agnon/Sachs (1966) |
Asturias (1967) |
Kawabata (1968) |
Beckett (1969) |
Solschenizyn (1970) |
Neruda (1971) |
Böll (1972) |
White (1973) |
Johnson/Martinson (1974) |
Montale (1975) |
Bellow (1976) |
Aleixandre (1977) |
Singer (1978) |
Elytis (1979) |
Miłosz (1980) |
Canetti (1981) |
García Márquez (1982) |
Golding (1983) |
Seifert (1984) |
Simon (1985) |
Soyinka (1986) |
Brodsky (1987) |
Mahfuz (1988) |
Cela (1989) |
Paz (1990) |
Gordimer (1991) |
Walcott (1992) |
Morrison (1993) |
Ōe (1994) |
Heaney (1995) |
Szymborska (1996) |
Fo (1997) |
Saramago (1998) |
Grass (1999) |
Gao (2000) |
Naipaul (2001) |
Kertész (2002) |
Coetzee (2003) |
Jelinek (2004) |
Pinter (2005) |
Pamuk (2006) |
Lessing (2007) |
Le Clézio (2008) |
Müller (2009) |
Vargas Llosa (2010) |
Tranströmer (2011) |
Mo (2012) |
Munro (2013) |
Modiano (2014) |
Alexijewitsch (2015) |
Dylan (2016) |
Ishiguro (2017)
