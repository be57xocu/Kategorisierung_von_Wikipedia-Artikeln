Klára Issová (* 26. April 1979 in Prag) ist eine tschechische Film- und Theaterschauspielerin.

Issová wurde als Tochter einer tschechischen Mutter und eines aus Syrien stammenden Vaters geboren. Im Alter von sechs Jahren nahm sie Schauspielunterricht und studierte dieses Fach an der Theaterfakultät der Akademie der Musischen Künste in Prag. Nach ihrem Abschluss im Jahr 1997 studierte sie zusätzlich Pädagogik.

Issová steht seit 1995 vor der Kamera; ihre erste größere Fernsehrolle übernahm sie 1999 im zweiteiligen Historienfilm Jeanne d’Arc – Die Frau des Jahrtausends. Neben tschechischen Filmproduktionen wirkte Issová auch in Hollywoodproduktionen wie der 2004 produzierten Filmkomödie Der Prinz & ich mit.

Ihre Cousine Marta Issová ist ebenfalls als Schauspielerin tätig.
