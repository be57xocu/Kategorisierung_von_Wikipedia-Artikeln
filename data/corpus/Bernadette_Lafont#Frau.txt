Bernadette Lafont (* 28. Oktober 1938 in Nîmes; † 25. Juli 2013 ebenda) war eine französische Theater- und Filmschauspielerin. Ab dem Ende der 1950er Jahre spielte sie in über 170 Film- und Fernsehproduktionen.

Lafont begann ihre Karriere als Tänzerin, bevor sie mit dem Einsetzen der Nouvelle Vague für den französischen Film entdeckt wurde. Zahlreiche Auftritte hatte sie vor allem unter der Regie von François Truffaut und Claude Chabrol. Ihr Debüt als Filmschauspielerin gab sie 1958 zusammen mit ihrem Ehemann Gérard Blain in Truffauts Die Unverschämten.[1] 
Ab den 1970er Jahren arbeitete sie unter anderem mit Nelly Kaplan (Die Piratenbraut), Jacques Rivette (Out 1), Nadine Trintignant, Marion Vernoux und Luc Béraud zusammen.
Für erhebliches Aufsehen sorgte ihr Auftritt in Jean Eustaches Film Die Mama und die Hure, der 1973 in Cannes zunächst auf erbitterten Widerstand stieß, dann jedoch den Großen Preis der Jury gewann.
1986 gewann sie den César als beste Nebendarstellerin in dem Film Das freche Mädchen von Claude Miller; auch für Chabrols schwarze Komödie Masken war sie in dieser Kategorie für den César nominiert. 2003 wurde sie für ihr Lebenswerk mit einem Ehren-César ausgezeichnet.
Ab 2009 war sie zudem Offizier der Ehrenlegion.

Lafont war von 1957 bis 1959 mit dem Schauspieler Gérard Blain verheiratet. Aus der späteren Ehe mit dem ungarischen Bildhauer Diourka Medveczky gingen neben ihrem Sohn David die Schauspielerinnen Élisabeth und Pauline Lafont hervor. Die 1963 geborene Pauline kam 1988 bei einem Gebirgsunfall ums Leben.

Bernadette Lafont starb am 25. Juli 2013 im Alter von 74 Jahren in einem Krankenhaus ihrer südfranzösischen Geburtsstadt Nîmes.[2] Ihr Grab befindet sich auf einem privaten Grundstück in Saint-André-de-Valborgne im Département Gard.[3] Staatspräsident François Hollande würdigte die Verstorbene.[4]
Ihre Autobiographie „La Fiancée du cinéma“ (Die Verlobte des Kinos) erschien 1999. Eine 2004 preisgekrönte Edelrose trägt ihren Namen.[5]