Thomas Ballhausen (* 1975 in Wien) ist ein österreichischer Schriftsteller, Literatur- und Filmwissenschaftler, Herausgeber und Übersetzer.

Ballhausen unterrichtet an der Universität Wien und an der Universität Mozarteum Salzburg. Für das Kulturmagazin skug fungiert Ballhausen als Leiter der Literaturabteilung, außerdem war er Mitbegründer der Autorenvereinigung die flut.

In Ballhausens belletristischen Veröffentlichungen dominieren Reflexionen über die Unmöglichkeit glücklicher Beziehungen und die Versuche, das Leben zu meistern. In Ballhausens Essays findet Populärkultur ebenso ihren Platz wie Hochkultur. 2010 war Ballhausen zum Ingeborg-Bachmann-Preis eingeladen. In der Reihe Bibliothek der Nacht gibt Thomas Ballhausen Phantastikliteratur heraus, so zum Beispiel Romane von Paul Leppin oder Furio Jesi.

