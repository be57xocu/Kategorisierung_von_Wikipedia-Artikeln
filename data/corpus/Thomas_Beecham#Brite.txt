Sir Thomas Beecham, 2. Baronet CH FRCM (* 29. April 1879 in St Helens, damals Lancashire; † 8. März 1961 in London) war ein britischer Dirigent, der mehrere britische Symphonieorchester gründete. Zu diesen Gründungen zählen unter anderem das New Symphony Orchestra (1906), das London Philharmonic Orchestra (1932) und das Royal Philharmonic Orchestra (1947).

Beecham war darüber hinaus ein Förderer zahlreicher Komponisten seiner Zeit. Er setzte sich maßgeblich für die Aufführung von Werken von Frederick Delius, Ethel Smyth und Jean Sibelius ein. Er spielte als erster in England Die Meistersinger von Nürnberg von Richard Wagner und die Opern Elektra und Salome von Richard Strauss und trug maßgeblich zur Bereicherung des Musiklebens der britischen Hauptstadt bei.

Thomas Beecham war als Sohn eines reichen Pharmazeuten in Oxford erzogen, auf musikalischem Gebiet war er Autodidakt. Aufgrund seiner finanziellen Verhältnisse konnte er es sich erlauben, sich in breitem Stil musikalisch zu engagieren. Bereits in der Zeit von 1902 bis 1904 leitete er ein kleines Opern-Ensemble; auch die Übernahme der künstlerischen und wirtschaftlichen Leitung des Covent Garden war ihm nur dank der finanziellen Unterstützung seiner Familie möglich. Genauso wirtschaftlich mutig gründete er während des Ersten Weltkriegs die Beecham Opera Company, die er als eigenes Ensemble 1920 wieder aufgeben musste, da die Ausgaben die Einnahmen bei weitem überstiegen.

Zu seiner Form des Dirigierens gehörte es, am Schluss eines Konzerts als Zugabe besonders publikumswirksame kleine Stücke anzuhängen. Beecham selbst bezeichnete diese Werkauswahl gern als „lollipops“ (Süßigkeiten).

Um Beecham, der über einen schlagfertigen Witz verfügte, ranken sich zahlreiche Anekdoten, von denen eine Auswahl von seinem ehemaligen Privatsekretär in Buchform herausgegeben wurden („Beecham Stories“). So werden beispielsweise folgende beiden Bonmots Beecham zugeschrieben:

“There are two golden rules for an orchestra: start together and finish together. The public doesn't give a damn what goes on in between.”

„Es gibt für ein Orchester zwei goldene Regeln: gemeinsam beginnen und gemeinsam aufhören. Das Publikum schert sich nicht darum, was dazwischen passiert.[1]“

“The English may not like music, but they absolutely love the noise it makes.”

„Die Engländer mögen Musik vielleicht nicht, aber sie lieben den Lärm, den sie macht, ohne Einschränkung.[1]“
