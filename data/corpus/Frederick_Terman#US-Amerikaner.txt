Frederick Emmons Terman (* 7. Juni 1900 in English, Indiana; † 19. Dezember 1982 in Palo Alto, Kalifornien) war ein US-amerikanischer Elektroingenieur. Er gilt zusammen mit William Shockley als der „Vater von Silicon Valley“.

Terman studierte an der Stanford University und wurde später Professor (Ingenieurwissenschaften) und Provost dieser Universität. Er ermutigte seine Studenten, eigene Unternehmen in der Nähe zu gründen oder in benachbarten Firmen zu arbeiten, wobei er auch selbst bei den Gründungen mithalf. Nach dem Zweiten Weltkrieg zog Terman viele staatlich finanzierte Forschungsprojekte an sich und machte Stanford so zu einer „Talentschmiede“ im Hightechbereich.

Terman beeinflusste und förderte auch David Packard und William Hewlett, die 1939 in ihrer legendären kleinen Garage in Silicon Valley ein Unternehmen gründeten. Dieses entwickelte sich dann rasch zu einer der ersten und erfolgreichsten Hightechfirmen der Welt – Hewlett-Packard (HP). 1945 wurde er in die American Academy of Arts and Sciences gewählt.

Außerdem errichtete Terman 1951 den „Stanford Industrial Park“ für neu gegründete Firmen, in dem sich wenige Jahre später Firmen wie IBM und HP angesiedelt hatten. Kurze Zeit später waren es dann über 150 Unternehmen, unter anderem auch Kodak und Apple. Auch heute noch hat HP dort seinen Hauptsitz.
