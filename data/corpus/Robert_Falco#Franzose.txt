Appellationsgerichtsrat Robert Falco (* 26. Februar 1882 in Paris; † 14. Januar 1960 ebenda) war einer der französischen Richter bei den Nürnberger Prozessen.

Als Sohn jüdischer Eltern stammte Falco aus einer sehr traditionsbewussten Familie. Schon sein Urgroßvater war von Louis Philipp mit der médaille de la reconnaissance nationale ausgezeichnet worden. Sein Großvater arbeitete für Leopold II. von Belgien als Architekt und sein Vater kämpfte im Deutsch-Französischen Krieg von 1870/71, wurde für seine Verdienste mit dem Orden der Ehrenlegion ausgezeichnet und machte sich später als Vorsitzender des Handelsgerichtshof von Paris einen Namen.

Nach seinem Studium arbeitete Falco ab 1903 zunächst als Staatsanwalt, promovierte 1907 mit einer Arbeit über die Pflichten und Rechte der Zuschauer im Theater[1] und diente dann als Richter am Kanzleigericht. Bereits 1906 trat er in die Rechtsanwaltskammer von Paris ein, erhielt seine Zulassung jedoch erst 1908. Bis 1919 übte er den Anwaltsberuf aus.

Später wurde er zum Generalanwalt des Appellationsgerichts von Paris ernannt. Von diesem Posten wurde er jedoch während der deutschen Besetzung Frankreichs wegen seiner jüdischen Herkunft 1944 entfernt.

Bei der Vorbereitung des Nürnberger Prozesses im Juni 1945 auf der Londoner Konferenz vertrat er zusammen mit dem Völkerrechtsprofessor André Gros Frankreich. Er war zudem einer der Hauptautoren des Londoner Statuts.

1946 wurde er, aufgrund seiner Mitwirkung am Nürnberger Prozess, zum Honorary Bencher am Gray’s Inn ernannt.

1947 wurde Falco auf eigenes Betreiben vom Kassationsgerichtshof rehabilitiert.

Präsident und Stellvertreter:
Sir Geoffrey Lawrence (Präsident) |
William Norman Birkett |
Robert Falco |
John Johnston Parker |
Alexander Woltschkow

Francis Beverley Biddle |
Henri Donnedieu de Vabres |
Iona Timofejewitsch Nikittschenko
