Alfred Edthofer (1900 – 24. März 1959) war ein österreichischer Schauspieler, der bei den Salzburger Festspielen aufgetreten ist.

1931 zählte er zu den Gründungsmitgliedern von Stella Kadmons Kleinkunstbühne Der liebe Augustin in Wien.[1] Bei den Salzburger Festspielen wirkte er 1939 als Jacques im Bürger als Edelmann mit, der Nachkriegszeit war er im Jedermann als Hausvogt (1946 und 1947), sowie als Armer Nachbar (1948 und 1949, laut einer inoffiziellen Quelle auch 1951) zu sehen.[2][3] Edthofer war zuletzt am Salzburger Landestheater tätig.[4]
Eine Verwandtschaft mit dem bekannten Schauspieler Anton Edthofer ist nicht verbürgt.
