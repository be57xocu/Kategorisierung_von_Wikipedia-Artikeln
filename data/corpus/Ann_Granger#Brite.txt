Patricia Ann Granger (* 1939 in Portsmouth, England) ist eine englische Krimi-Schriftstellerin.

Ann Granger studierte moderne Sprachen an der London University und war im Anschluss als Englischlehrerin in Frankreich, Jugoslawien, der Tschechoslowakei und in Österreich beschäftigt. Sie ging danach in den diplomatischen Dienst. Gemeinsam mit ihrem Ehemann war sie in Sambia und später in Deutschland tätig. Daraufhin kehrte sie mit ihren zwei Kindern nach England zurück, wo sie ihre Tätigkeit als Schriftstellerin begann. Sie veröffentlichte historische Romane unter dem Pseudonym Ann Hulme, bevor sie seit 1991 durch ihre Krimiserien bekannt wurde. In einer der Serien spielen Meredith Mitchell und Inspektor Alan Markby die Hauptrollen, die Heldin der anderen Serie ist die Detektivin Fran Varady.
Um das Paar Mitchell/Markby sind 15 Romane erschienen, um Fran Varady bisher sieben Bände. Der erste Band einer dritten Serie, diesmal vom Genre Historische Krimis, rankt sich um Lizzi Martin und Benjamin Ross.

Granger lebt in Bicester bei Oxford.

Ann Grangers Krimiserien sind im Bastei Lübbe Verlag auch in digitaler Form erhältlich.
