Paul Doumer [pɔl duˈmɛːʀ] (* 22. März 1857 in Aurillac, Frankreich; † 7. Mai 1932 in Paris, Frankreich) war ein französischer Staatsmann und vorletzter Präsident der Dritten Republik (1931-1932). Er war Generalgouverneur von Indochina (1897–1902) und bekleidete mehrfach das Amt des Finanzministers (von 1895–1896, 1921–1922, 1925–1926).

Der früh verwaiste Sohn einer Arbeiterfamilie war zunächst Mathematiklehrer, bevor er sich dem Journalismus zuwandte und 1888 in einem Wahlkreis des Départements Aisne erstmals zum Parlamentsabgeordneten gewählt wurde. Als hochgradiger Freimaurer im Grand-Orient schloss er sich in der Abgeordnetenkammer der Fraktion der Parti radicale an. 1912 wurde er in Korsika zum Senator gewählt. 1927 wählte der Senat Doumer, dessen vier Söhne im Ersten Weltkrieg gefallen waren, zu seinem Präsidenten.

Den Höhepunkt seiner politischen Karriere erreichte er am 13. Mai 1931 mit der Wahl zum französischen Staatspräsidenten im zweiten Durchgang gegen Aristide Briand. Am 6. Mai 1932 wurde er Opfer eines Attentats. Bei der Eröffnung einer Buchmesse in Paris feuerte der russische Emigrant Pawel Gorgulow (in französischer Schreibweise Paul Gorguloff) mehrmals auf Doumer. Dieser trug schwere Verletzungen davon und verstarb am Tag darauf.

Nach Doumer sind die Doumer-Insel und der Doumer Hill in der Antarktis benannt.

Adolphe Thiers |
Patrice de Mac-Mahon |
Jules Grévy |
Marie François Sadi Carnot |
Jean Casimir-Perier |
Félix Faure |
Émile Loubet |
Armand Fallières |
Raymond Poincaré |
Paul Deschanel |
Alexandre Millerand |
Gaston Doumergue |
Paul Doumer |
Albert Lebrun
