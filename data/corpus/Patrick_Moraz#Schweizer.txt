Patrick Philippe Moraz (* 24. Juni 1948 in Morges am Genfersee, Schweiz) ist ein Progressive Rock-Musiker. Als Keyboarder spielt er alle Tasteninstrumente. 

Patrick Moraz stammt aus einer französisch-schweizerischen Mittelklassefamilie. Sein Vater war Roadmanager des Pianisten  Ignacy Jan Paderewski und Stepptänzer, er hatte mit Fred Astaire und Maurice Chevalier getanzt. Patrick erhielt bereits sehr früh eine musikalische Ausbildung. Mit fünf Jahren lernte er Violine, Klavier und Perkussion, und er komponierte bereits mit fünf Jahren eigene kleine Klavierstücke. Seine Interessen umfassten Jazz und klassische Musik.

Mit 13 Jahren brach er sich vier Finger seiner rechten Hand beim Rollschuhlaufen. Man sagte ihm, er würde nie wieder klassische Musik spielen können. Doch eine gute Therapie und zähes Üben verhalf ihm zu einer noch besseren Technik und Geläufigkeit, vor allem auch der linken Hand, mit der er Rückstände der Rechten auszugleichen suchte. Später besuchte er einige Seminare am Konservatorium Lausanne, doch gehörte seine Leidenschaft schon früh dem Jazz. Moraz spielte in lokalen Jazzbands und lernte, die Kirchenorgel zu spielen. Mit 17 Jahren gewann er einen Preis als junger Jazz-Solist, der ihm einige Stunden beim Jazz-Violinisten und -Keyboarder Stéphane Grappelli bescherte. Bis heute sind bei Moraz zudem deutliche Einflüsse von Bill Evans, Jan Hammer und Maurice Ravel zu hören.

Patrick Moraz reiste bereits in seiner Jugend sehr viel. Mit seinem eigenen Jazz-Trio/Quartett tourte er in den 1960er Jahren durch Europa, oft als Vorgruppe für bekanntere Jazzmusiker, darunter auch der Saxophonist John Coltrane. 

Mit 17 Jahren arbeitete er einige Zeit lang als Tauchlehrer an der spanischen Mittelmeerküste, 1964 ging er auf eigene Faust nach Bournemouth, lernte Englisch und erarbeitete sich seinen Lebensunterhalt als Schulkoch und als Sprachlehrer für Latein und Französisch. Vor allem aber begann er, sich in der lokalen Musikszene zu etablieren. Er spielte kurzzeitig bei den Night People, musste das Land aber nach einem halben Jahr wieder verlassen.

Zurück in der Schweiz verkaufte er Teppiche und Enzyklopädien, um sich ein Studium an der Genfer Universität leisten zu können, wo er Wirtschaft und Politik studierte. Dann wechselte er an die Columbia University in New York City, nur um kurz darauf, 1966, zu einer afrikanischen Import/Export-Firma zu wechseln. Er arbeitete dort unter der Woche und verdiente genug, um an den Wochenenden in die Schweiz zurückzukehren und mit seinem Patrick Moraz-Trio/Quartett Konzerte zu spielen. 1968, nach zwei Jahren in Afrika, entschloss er sich endgültig für eine Musikerkarriere.

Schon in diesen Jahren traf Moraz mehrfach auf die Bands Yes und The Nice, wenn sie zufällig am gleichen Ort Konzerte gaben, wie etwa 1969 in Montreux (entweder am 24./25. April oder am 2. Dezember), wo Moraz Partys für die beim jährlichen Jazz-Festival auftretenden Musiker gab, und in Basel, wo er sogar mit Lee Jackson und Brian Davison in einer Jamsession spielte (29. Oktober 1969). Dies brachte ihm später eine Empfehlung des The-Nice-Keyboarders Keith Emerson ein, der Moraz als Ersatz für sich selbst vorschlug, nachdem er einige Zeit später The Nice verlassen hatte. Von beiden Bands zeigte sich Moraz sehr beeindruckt.

Nach einer Reise in die USA formierte er im Jahr 1969 in der Schweiz seine eigene Gruppe Mainhorse mit Jean Ristori (E-Bass) und den beiden Engländern Bryson Graham (Schlagzeug) und Peter Cockett (Gitarre), mit denen er nach England ging, um sich dort zu etablieren. 1971 wurde ein Album bei Polydor herausgegeben: Mainhorse. Das Album wurde in den Studios von Deep Purple in Kingsway aufgenommen und war erfolgreich genug, dass die Band damit in Deutschland einige Konzerte geben konnte. Allerdings reichte es nicht zum Durchbruch, und da sich die Band keinen Manager leisten konnte, zerfiel sie 1972. Moraz blieb aber während seiner gesamten Karriere in engem Kontakt mit Jean Ristori.

Zwischen 1969 und 1974 schrieb Moraz Musik für sieben Filme, darunter L'Invitation von Claude Goretta (1973), der den großen Preis in Cannes und eine Oscar-Nominierung gewann.

1973 war Moraz als musikalischer Direktor einer achtzehnköpfigen brasilianischen Balletttruppe unterwegs, unter anderem in Japan, Thailand und Hongkong. Moraz lebte währenddessen einige Monate in Hongkong, wo er eine Band mit den 6 Perkussionisten der Balletttruppe formierte und einige Male im Fernsehen auftrat. Nach seiner Rückkehr in die Schweiz erhielt er einen Anruf von Lee Jackson. Nach der Auflösung von The Nice durch Keith Emerson hatte dieser die wenig erfolgreiche Band Jackson Heights ins Leben gerufen. Wegen des mangelnden Erfolgs wandte er sich an Moraz, um von diesem Unterstützung für ein weiteres Albumprojekt der Jackson Heights zu erbitten. Schnell stellte sich heraus, dass Moraz’ Ideen für die Band ungeeignet waren, und die beiden gründeten zusammen mit dem Nice-Schlagzeuger Brian Davison die Progressive Rock-Gruppe Refugee, die 1973 ein erfolgreiches Album produzierte und eine nicht weniger erfolgreiche England-Tournee absolvierte.

Seinen kommerziellen Durchbruch als Rockmusiker feierte Moraz als Keyboarder der britischen Progressive-Rock-Band Yes, deren Keyboarder Rick Wakeman er im August 1974 zunächst nur widerwillig ersetzte. Er wollte Jackson und Davison, die soeben noch vor einem Neuanfang gestanden hatten, eigentlich nicht vor den Kopf stoßen. Die Weiterentwicklung der damaligen elektronischen Tasteninstrumente und die finanzielle Situation einer internationalen Top-Band ermöglichten ihm jedoch ganz neue Spielweisen, die Moraz auf dem Album Relayer 1974 auf einzigartige Weise einbrachte. Nach Abschluss der Arbeiten ging die Band auf Tour (Relayer-Tour, 8. November 1974 bis 23. August 1975, 89 Shows).

1976 spielten alle Mitglieder der Gruppe Soloalben ein. Moraz steuerte auf den Alben von Steve Howe und Chris Squire sein Keyboardspiel bei. Sein eigenes, teilweise in Südamerika, teilweise in der Schweiz (u. a. mit dem Bassisten Jeff Berlin) mit der Hilfe von Jean Ristori aufgenommenes Solo-Album, ein Konzeptalbum, das weitgehend in brasilianischen Rhythmen und mit entsprechenden Instrumenten produziert wurde, betitelte er The story of I. Es enthält eine gelungene Fusion von Pop, Progressive Rock, von der Romantik geprägter Neoklassik, Musical und Jazz, wurde aufgrund dieser stilistischen Breite als das erste Album der Weltmusik bezeichnet und erhielt großen Beifall bei Musikerkollegen. Peter Gabriel fragte Moraz nach der Telefonnummer seiner brasilianischen Rhythmusgruppe.

Das von Jean Ristori abgemischte Relayer-Album war das einzige Yes-Album, auf dem Moraz mitwirkte. Nach der Relayer-Tour und der Solo-Album-Tour (28. Mai 1976 bis 22. August 1976, 53 Shows) wurde er im November 1976 mitten in der Arbeit am Nachfolgealbum von Relayer, Going for the One, von seinem Vorgänger Wakeman ersetzt, als Yes die Möglichkeit sahen, diesen wieder in die Band zu holen.

Nach seinem zweiten, stilistisch ähnlichen, Soloalbum, Out in the Sun (1977), ersetzte der mittlerweile (1978) in Brasilien lebende Moraz Michael Pinder bei der reformierten Band The Moody Blues, zunächst als Sessionmusiker. Die Reise zum Vorspiel nach England hatte er durch einige Konzerte in der Schweiz, darunter beim Montreux Jazz Festival 1978, finanziert. Das erste gemeinsame Album, Long Distance Voyager 1981, wurde das zweite und letzte der Moody Blues, das in den USA die Spitze der Charts erreichte. Moraz blieb bis 1991 bei der Band, seit 1979 als Vollmitglied (nach der Trennung vertraten die anderen Bandmitglieder vor Gericht jedoch die Position, Moraz sei lediglich „a hired keyboard player“ gewesen - diese Uneinigkeiten hatten vor allem finanzielle Hintergründe). 

Nebenbei arbeitete er weiter an verschiedenen Projekten, vorwiegend Soloaufnahmen, mal mit Band, seit den 1990er Jahren vorwiegend solo am Flügel. Moraz' stilistisches Spektrum reicht dabei von brasilianischen Musiken über Klassik und Jazz bis hin zum New-Age (letzteres v. a. auf Human Interface, 1987), Stilrichtungen, die er meist in verspielten, im besten Sinne als „poppig“ zu bezeichnenden Motiven und Phrasen zu einem sehr einheitlichen, eigenständigen Stil zu vereinen versteht. Die beiden Future Memories-Alben enthalten überdies weitgehend improvisierte Musik (Moraz nennt es „instant composition“), die anlässlich einer Fernseh-Liveübertragung im Studio entstand und im Falle von Future Memories Live On TV (1979) nur wenige Tage nach der Aufnahme im Laden stand.

Nach seinem Ausstieg bei den Moody Blues und der Veröffentlichung seines ersten Solo-Piano-Albums Windows of Time (1994) ging Patrick Moraz in den USA auf eine kleine Tournee in Kirchen und kleinen Konzerthallen. Auf dieser Coming Home America Tour (C. H. A. T., 'Geplauder') spielte er ausgewählte Stücke und berichtete von einigen Erlebnissen aus seiner Karriere. Eines dieser Konzerte ist 1995 unter dem Titel PM in Princeton auf CD erschienen. Seither sind Moraz' Konzerte selten geworden.

Gemeinsame Arbeiten mit anderen Musikern schließen Alben mit Chick Corea, dem Panflötisten Syrinx und Bill Bruford ein. Das Duo Moraz/Syrinx veröffentlichte 1980 ein Album namens Coexistence, das 1989 unter dem Titel Libertate (rumänisch für „Freiheit“) wieder veröffentlicht wurde, um damit Gelder für ein Kinderhilfsprojekt in Rumänien zu sammeln. Das Duo Moraz/Bruford, das 1983 in Surrey nur eine halbe Meile auseinander wohnte, veröffentlichte 1983 und 1985 zwei Jazz-Alben, Music for Piano and Drums, ein akustisches Album, das überwiegend Moraz-Kompositionen enthielt und in nur drei Tagen entstand, und Flags, ein Album von größerer Klangbreite, das überwiegend von beiden Musikern gemeinsam geschrieben wurde.

Die britische Plattenfirma Voiceprint veröffentlichte 2006/2007 eine Reihe der Soloalben Moraz', ergänzt durch Bonustracks. Beim Remastering half auch Moraz' Freund Jean Ristori, mit dem Moraz seit den Tagen von Mainhorse nie den Kontakt verloren hatte. Auch zwei DVDs, Future Memories I & II und PM in Princeton, sind 2007 erschienen. Derzeit ist Patrick Moraz mit einem Neuarrangement seines Titels Cachaca auf dem Solo-Album des Schlagzeugers Jacob Armen zu hören, Konzerte der beiden (und eventuell einem Bassisten), vielleicht auch in Europa, sind zumindest ins Auge gefasst. Es ist geplant, große Teile von The Story of i (vor allem Seite 1), Patrick Moraz III und den Moraz/Bruford Alben zu spielen. 

Derzeit arbeitet Moraz an der Musik für einen unabhängigen Kurzfilm. 2015 veröffentlichte er ein Album zusammen mit dem Schlagzeuger Greg Alban.

 Solo-Alben 

Filmmusiken

 Weitere Solo-Aktivitäten 

Moraz/Bruford 

Mit dem Panflötisten Syrinx 

Mit dem Schlagzeuger Greg Alban 

Mit Mainhorse

siehe Mainhorse

Mit Refugee

siehe Refugee

Mit Yes

Mit The Moody Blues

siehe The Moody Blues
