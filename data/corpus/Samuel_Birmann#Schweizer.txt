Samuel Birmann (* 11. August 1793 in Basel; † 27. September 1847 ebenda) war ein Schweizer  Landschaftsmaler der Romantik.

Seine Werke der Landschaftsmalerei sind am Anfang stark durch seinen Vater Peter Birmann beeinflusst, von dem er auch ausgebildet wurde. Zunächst suchte er seine Motive im Jura und der badischen Nachbarschaft, später lagen seine bevorzugten Motive im Berner Oberland, im Wallis und in Savoyen.

Viele seiner Werke, von denen die wichtigsten aus der Zeit um 1820 stammen, befinden sich heute im Kunstmuseum Basel.

Verheiratet war er mit Juliana Birmann-Vischer (* 1785; † 1859), die nach seinem Tode 1853 Martin Grieder adoptierte, der durch sein soziales und politisches Engagement in die Kantonsgeschichte von Basel-Landschaft einging. 
