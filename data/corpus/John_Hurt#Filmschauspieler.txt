Sir John Vincent Hurt, Kt CBE (* 22. Januar 1940 in Chesterfield, Derbyshire; † 25. Januar 2017 in Cromer, Norfolk[1]), war ein britischer Theater- und Filmschauspieler sowie Synchronsprecher. Hurt zählte seit Mitte der 1970er Jahre zu den profiliertesten britischen Charakterdarstellern.[2] 1979 wurde er für sein Mitwirken in Alan Parkers Filmdrama 12 Uhr nachts – Midnight Express als bester Nebendarsteller mit einem Golden Globe ausgezeichnet.[3]
Nach eigener Darstellung hat Hurt, Sohn eines Pfarrers, bereits im Alter von neun Jahren beschlossen, Schauspieler zu werden: „I had had an enormous sense of theatre […] from an early age“ („Ich hatte schon in jungen Jahren ein großes Gespür für Theater“). In der Schule betätigte er sich bei Theateraufführungen. Für seine konservativen Eltern war der Beruf des Schauspielers keine gute Berufswahl. Sie betrachteten ein Leben als freier Künstler als „zu unsicher“ und schickten ihren Sohn auf die Londoner Saint-Martins-Kunsthochschule, damit er als Kunstlehrer eine sichere Anstellung anstreben konnte.

Als Hurt jedoch 1962 ein Theaterstipendium an der renommierten Royal Academy of Dramatic Art angeboten wurde, brach er sein Kunststudium ab, um die Schauspielerei zu erlernen. Da ihn seine Eltern dabei nicht unterstützten, nahm er zunächst jede Rolle an, um sich finanziell über Wasser zu halten. Er trat in den unterschiedlichsten Theaterstücken und Fernsehserien auf, machte sich so in der Branche bekannt und erhielt schließlich eine Hauptrolle in dem Stück Little Malcolm and His Struggle Against the Eunuchs.

Hurt selbst sah das Stück, das in Dublin Premiere feierte, im Nachhinein als seinen Durchbruch an, da er dort auf den Filmregisseur Fred Zinnemann traf. Dieser war von seiner Leistung derart beeindruckt, dass er ihn für eine Nebenrolle in dem Historiendrama Ein Mann zu jeder Jahreszeit verpflichtete. Der Film, der sechs Oscars erhielt, wurde zu einem von Zinnemanns größten Erfolgen. Die Zusammenarbeit mit Zinnemann verlief so gut, dass dieser auch seinen Freund John Huston auf Hurt aufmerksam machte.

Huston besetzte ihn – gegen den Widerstand des Studios, das ihn für nicht bekannt genug hielt – für die Hauptrolle in der Komödie Dave – Zuhaus in allen Betten. Obwohl der Film floppte, erhielt Hurt weitere Angebote. 1972 wurde er für seine schauspielerische Leistung als der vermeintliche Mörder Timothy Evans in dem Thriller John Christie, der Frauenwürger von London mit einer Nominierung für den Britischen Filmpreis bedacht.

Hurts Filmkarriere gewann nun an Dynamik, Kritiker wurden auf ihn aufmerksam und seine Leistungen gelobt. Insgesamt war er sechsmal für den Britischen Filmpreis nominiert, dreimal wurde er ausgezeichnet. Hurt ließ sich nicht auf einzelne Genres festlegen und nahm die unterschiedlichsten Rollen an – mit wechselhaftem Erfolg. Aufgrund seiner markanten Stimme war er auch als Synchronsprecher für Zeichentrickfilme gefragt, unter anderem für Unten am Fluß (1978) und im selben Jahr für Der Herr der Ringe.

In den Filmen The Naked Civil Servant (1975) und An Englishman in New York (2008) spielte er Quentin Crisp; im erstgenannten sehr zum eigenen Entzücken des englischen Exzentrikers, der hierdurch große Berühmtheit erlangte. Hurt selbst fand nach seiner exzellenten, expliziten Darstellung eines Homosexuellen im britischen Fernsehen zunächst aber kaum mehr neue Engagements.

Auf internationaler Bühne machte er sich vor allem mit seinen Auftritten in 12 Uhr nachts – Midnight Express, für den er 1978 seine erste Oscar-Nominierung erhielt, und Ridley Scotts Alien – Das unheimliche Wesen aus einer fremden Welt einen Namen. Sein Auftritt in dem Science-Fiction-Horrorfilm war verhältnismäßig kurz, doch spielte er die Hauptfigur in einer der einprägsamsten und blutigsten Szenen des Films, in der ein Alien aus seinem Brustkorb schlüpft. Hurt wurde bis zu seinem Tod auf der Straße und in Interviews auf diese Szene angesprochen. In Mel Brooks’ Spaceballs parodierte er acht Jahre später diese Szene.

Die Rolle, die Hurt als die körperlich anstrengendste in seiner Karriere bezeichnet, war die Titelrolle als John Merrick in David Lynchs Der Elefantenmensch (1980). Das Auftragen der ausgefallenen Maske dauerte vor Beginn der Dreharbeiten jeweils bis zu zwölf Stunden. Hurt soll zu seiner damaligen Frau gesagt haben: „Ich glaube, jetzt haben sie mich dazu gebracht, die Schauspielerei zu hassen.“ Die von ihm in Kauf genommenen Strapazen wurden aber mit einer weiteren Oscar-Nominierung belohnt.

Vier Jahre später übernahm er die Rolle des Winston Smith in Michael Radfords Verfilmung von George Orwells dystopischem Roman 1984, in der Richard Burton in seiner letzten Filmrolle als Smiths Peiniger O’Brien zu sehen ist. 2006 spielte Hurt, wie in ironischer Bezugnahme auf diese Rolle, den Großkanzler Sutler (Führer des autokratischen Großbritanniens) in V wie Vendetta. 2001 übernahm er in der Romanverfilmung von Harry Potter und der Stein der Weisen die Rolle des Zauberstabmachers Mr. Ollivander. 2010 und 2011 war er in den Fortsetzungen Harry Potter und die Heiligtümer des Todes – Teil 1 und Teil 2 in ebendieser Rolle zu sehen. 2013 spielte er den „Kriegsdoktor“ in der Jubiläumsfolge zum 50-jährigen Bestehen von Doctor Who.

Hurt entzog sich weitgehend dem Starsystem und übernahm schwierige, auch unsympathische Rollen, die ihm möglicherweise kein großes Publikum, dafür aber darstellerische Herausforderungen boten. Von Zeit zu Zeit trat er auch in größeren Hollywood-Produktionen auf (beispielhaft dafür: Indiana Jones und das Königreich des Kristallschädels), um dadurch auch in kleineren, weniger finanzstarken Projekten mitwirken zu können. Im Januar 2012 wurde er für Joon-ho Bongs Thriller Snowpiercer gecastet.[4] Seine letzte Filmrolle hatte Hurt in That Good Night übernommen.

Hurt war seit 2008 in der britischen Fantasyserie Merlin – Die neuen Abenteuer in der Originalstimme des Drachen zu hören. Im Juni 2004 wurde er von der britischen Königin Elisabeth II. zum Commander of the Order of the British Empire ernannt und 2015 zum Knight Bachelor erhoben. Seit einigen Jahren setzte sich Hurt mit der englischen Organisation Project Harar Ethiopia für benachteiligte Kinder in Afrika ein, die an Gesichtsverletzungen (z. B. Noma, Hyänenbisse, Verbrennungen, Gesichtstumore) oder -missbildungen (z. B. Gaumenspalten) leiden.

Mitte 2015 wurde bei Hurt Bauchspeicheldrüsenkrebs diagnostiziert.[5] Er starb am 25. Januar 2017 im Alter von 77 Jahren.[1]
John Hurt hatte im Laufe seiner Karriere viele deutsche Synchronsprecher; seit Wie man sein Leben lebt (1975) war es vor allem Jürgen Thormann.

John Hurt war viermal verheiratet und hatte zwei Kinder mit Jo Dalton, seiner dritten Frau. Wegen alkoholbedingter Eskapaden und anschließender Entziehungskuren machte er immer wieder Schlagzeilen.

Sein Bruder Michael lebt als Benediktinermönch mit dem Ordensnamen Bruder Anselm im Kloster Glenstal Abbey in Murroe im County Limerick in Irland. Er ist dort als Küchenchef tätig und hat ein Kochbuch mit Lieblingsrezepten verfasst, für das John Hurt das Vorwort schrieb.[6]
Oscar

British Academy Film Award

Golden Globe Award

Teddy Award

Sonstige

“I’ve done some stinkers in the cinema. You can’t regret it; there are always reasons for doing something, even if it’s just the location.”

„Ich habe einigen Schrott gemacht. Das kann man nicht bereuen; es gibt immer einen Grund, etwas zu machen, selbst wenn es nur der Drehort ist.“

“I turn up in Los Angeles every now and then, so I can get some big money films in order to finance my smaller money films.”

„Von Zeit zu Zeit tauche ich in Los Angeles auf, um Filme mit einem großen Budget zu machen, damit ich die Filme mit kleinem Budget machen kann.“

Kanonische Darsteller:
1. William Hartnell |
2. Patrick Troughton |
3. Jon Pertwee |
4. Tom Baker |
5. Peter Davison |
6. Colin Baker |
7. Sylvester McCoy |
8. Paul McGann |
Kriegsdoktor John Hurt |
9. Christopher Eccleston |
10. David Tennant |
11. Matt Smith |
12. Peter Capaldi |
13. Jodie Whittaker

Neugecastete Darsteller:
Richard Hurndall (1. Doktor) |
David Bradley (1. Doktor)

Darsteller irregulärer Inkarnationen:
Adrian Gibs (Watcher) |
Michael Jayston (Valeyard) |
Toby Jones (Dreamlord)

Außerkanonische Darsteller:
Rowan Atkinson |
Jim Broadbent |
Peter Cushing |
Hugh Grant |
Richard E. Grant |
Joanna Lumley
