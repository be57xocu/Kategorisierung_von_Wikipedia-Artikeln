Daniel Chamier (* 1565 auf Schloss Le Mont bei Mocas in der Dauphiné; † 17. Oktober 1621 in Montauban) war ein reformierter Theologe aus Frankreich.

Seit 1612 war er Professor in Montauban und fiel bei der Belagerung dieser Stadt 1621 auf den Wällen. Als entschlossener Verteidiger seiner Kirche war er Vorsitzender der meisten reformierten Nationalsynoden und konfessionellen Verhandlungen in Frankreich. 
