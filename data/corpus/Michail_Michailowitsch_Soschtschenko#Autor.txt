Michail Michailowitsch Soschtschenko (russisch Михаил Михайлович Зощенко, wiss. Transliteration Michail Michajlovič Zoščenko; * 28. Julijul./ 9. August 1894greg.[1] in Sankt Petersburg, Russisches Kaiserreich; † 22. Juli 1958 in Leningrad, UdSSR) war ein russischer Schriftsteller.

Soschtschenko wurde als Sohn eines Malers und einer Schauspielerin geboren und wuchs mit sieben Geschwistern auf.

Nach dem Besuch des Gymnasiums war er in den Jahren 1913 und 1915 an der juristischen Fakultät in St. Petersburg eingeschrieben; er verfasste bereits in dieser Zeit seine ersten Erzählungen. Sein Studium wurde vom Ersten Weltkrieg unterbrochen, zu dem sich Soschtschenko 1915 freiwillig meldete.

Während des Krieges erlitt der Dichter eine Gasvergiftung mit bleibenden Schäden für sein Herz, aufgrund derer er 1917 aus der Armee entlassen wurde. Die schwere Krankheit spiegelt sich in etlichen seiner Werke wider.

Nach der Rückkehr nach Petersburg (nun Petrograd) schrieb er einige unveröffentlichte Erzählungen, denen ein Einfluss Maupassants nachgesagt wird. Trotz seiner Herzkrankheit nahm Soschtschenko 1918/19 auf der Seite der Roten Armee am Bürgerkrieg in Russland teil.

Nach dem Bürgerkrieg übte Soschtschenko verschiedene Berufe aus, um seinen Lebensunterhalt zu verdienen. Er war u.a. Postleiter, Detektiv, Tierzüchter, Milizionär, Schuster, Tischler und Büroangestellter. Ab 1920, zurück in Petrograd, begann er wieder zu schreiben; einzelne Erzählungen erschienen in den zahlreichen satirischen Zeitschriften der NEP-Zeit, und eine erste Sammlung von Erzählungen kam 1922 heraus und trug den Titel Erzählungen des Nasar Iljitsch Herrn Blaubauch (Рассказы Назара Ильича, господина Синебрюхова) und strahlte durch den dummdreisten Ich-Erzähler à la Schwejk eine Tragikomik aus, die bei Publikum und Kritik erfolgreich angenommen wurde. Der neue Helden-Typus des wenig von Geistesgaben belasteten „Sowjetmenschen“, der sich durch den Alltag kämpft, fand sich auch in den weiteren Erzählungen Soschtschenkos. Als Stilmittel verwendete Soschtschenko dazu den sog. Skas, eine auch schon in den Werken Gogols vorkommende pseudo-mündliche Erzählweise in umgangssprachlichem Duktus. In den folgenden Jahren gehörte Soschtschenko zu den meistgelesenen Autoren des Landes und seine Erzählsammlungen und Werke wurden in zahlreichen Zeitschriften, Zeitungen und Verlagen in hohen Auflagen herausgegeben.

1921 schloss sich der Dichter der neu gegründeten literarischen Gruppe der Serapionsbrüder (Серапионовы братья) an und war bis zu ihrem Ende ihr ständiges Mitglied.

Allmählich wendete sich Soschtschenko von den Erzählungen ab und begann größere Werke wie Die wiedererlangte Jugend («Возвращённая молодость», 1933) oder Das Himmelblaubuch (Голубая книга, 1935) zu schreiben. Stets entfernte er sich in seinen späteren Werken immer mehr von der satirischen und humoresken Bahn und experimentierte mit verschiedenen Kompositions- und Darstellungsmethoden. Nach Erscheinen des Himmelblaubuches konnte Soschtschenko nur noch Feuilletonartikel und Kindergeschichten veröffentlichen.

Während des Zweiten Weltkrieges wurde Soschtschenko aus dem belagerten Leningrad nach Alma-Ata evakuiert und arbeitete in den dortigen Filmstudios. Noch während des Krieges, nachdem die Blockade aufgehoben war, kehrte er nach Leningrad zurück und nahm seine schriftstellerische Tätigkeit wieder auf, unter anderem auch in satirischen Zeitschriften wie Krokodil.

Durch seine Veröffentlichungen geriet er dabei unerwartet ins Kreuzfeuer der staatlichen Kritik. Besonders nach dem Erscheinen der ersten Kapitel seines Werkes Vor Sonnenaufgang (Перед восходом солнца) im Jahre 1943 in der Zeitschrift Oktjabr’ wurde der Dichter, ebenso wie Anna Achmatowa, Angriffen der Parteikritik ausgesetzt: der Leningrader Parteisekretär Andrej Schdanow bezeichnete Vor Sonnenaufgang als „ekelhaftes Werk“. Soschtschenkos Werke durften nicht mehr gedruckt werden, und selbst seine Herausgeber distanzierten sich von ihm. Es folgte der Ausschluss aus sämtlichen Stellungen und schließlich auch aus dem Schriftstellerverband im Jahre 1946.

Erst nach Stalins Tod im Jahre 1953 wurde Soschtschenko rehabilitiert und wieder in den Schriftstellerverband aufgenommen, und zwei Jahre vor seinem Tod im Jahre 1958 konnte 1956 ein Auswahlband seiner Werke erscheinen.

Einem breiteren ostdeutschen Publikum wurde er durch die von Manfred Krug am 31. Oktober 1965 im Rahmen einer Veranstaltung von Lyrik – Jazz – Prosa vorgetragene Erzählung Die Kuh im Propeller (im Original: Der Agitator aus Teterkin bestellt einen Aeroplan) bekannt.
