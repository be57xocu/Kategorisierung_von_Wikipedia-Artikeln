Teresa Stratas (* 26. Mai 1938 in Toronto, Ontario, eigentlich Anastasia Strataki) ist eine kanadische Opernsängerin (Sopran) griechischer Abstammung.

Sie sang zunächst in Nachtlokalen Schlager, bevor sie von Freunden für klassische Musik gewonnen wurde. 20-jährig stand sie in ihrer Heimatstadt Toronto als Mimi in Giacomo Puccinis La Bohème erstmals auf einer Opernbühne. Weltweit ins Gespräch brachte sich die zierliche Sängerin als Violetta in Franco Zeffirellis La traviata-Film von 1982 (zusammen mit Plácido Domingo), in dem sie sowohl stimmlich als auch schauspielerisch begeisterte. Bereits 1965 sang sie mit großem Erfolg diese Partie an der Seite von Fritz Wunderlich (in weiteren Rollen Hermann Prey und Brigitte Fassbaender) an der Oper von München.  

Bei den Salzburger Festspielen war sie zwischen 1969 und 1980 in verschiedenen Rollen in Così fan tutte und Le nozze di Figaro unter Seiji Ozawa, Karl Böhm, Herbert von Karajan und Gustav Kuhn zu hören.

In Kurt Weills Aufstieg und Fall der Stadt Mahagonny sang sie die Jenny; in Alban Bergs Lulu sang sie 1979 die Titelrolle (Uraufführung der von Friedrich Cerha komplettierten dreiaktigen Version an der Pariser Oper unter Leitung von Pierre Boulez, Inszenierung Patrice Chéreau), welche 1981 auch mit dem Grammy Award ausgezeichnet wurde. Zwei Jahre nach dieser spektakulären Aufführung sagte sie alle Operntermine ab, um in Kalkutta in Mutter Teresas Waisenhaus zu arbeiten. 

Ab 1986 trat sie dann wieder auf der Bühne auf. Zunächst war sie in dem Musical Rags am Broadway zu sehen. 1991 wirkte sie bei der Uraufführung von John Coriglianos Oper The Ghosts of Versailles an der Met mit.
