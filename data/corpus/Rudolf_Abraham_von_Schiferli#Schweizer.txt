Rudolf Abraham von Schiferli (* 30. September 1775 in Thun; † 3. Juni 1837 in Bern) war ein Schweizer Chirurg und Professor.

Er studierte in Bern, Jena, Wien und Paris und wurde 1796 in Jena zum Doktor der Medizin promoviert. 1798 erwarb er das Patent als praktischer Arzt in Bern und wurde 1799 Oberchirurg, 1800 Generalinspektor der Gesundheitspflege und 1802 Oberfeldarzt der Helvetischen Truppen.

1799 war er Mitbegründer des Medizinischen Instituts in Bern und wirkte danach an diesem als Lehrer. 1803 wurde er in Bern zum Garnisonsarzt, 1804 zum kantonalen Oberimpfarzt und 1805 zum ordentlichen Professor für Chirurgie und Gynäkologie an der Berner Hochschule.

1812 bis 1837 war er Cavalier d’Honneur (Oberhofmeister) der russischen Großfürstin Anna Feodorowna und Verwalter von deren Gut Elfenau. Nach einer unglücklichen Ehe mit Konstantin Pawlowitsch, dem Bruder des Zaren Alexander I., war Anna Feodorowna aus Sankt Petersburg entflohen, um sich 1813 in Bern niederzulassen. Schiferli war der Vater ihrer 1812 geborenen Tochter Hilda.

1814 bis 1831 war Schiferli Mitglied des Grossen Rates von Bern, schon 1811 war er Mecklenburg-Schwerinischer Hofrat geworden und 1827 wurde er russischer Staatsrat und kaiserlicher Hofrat.

1821 schloss er in Genua Freundschaft mit dem Astronomen Franz Xaver von Zach (1754–1832). Vom folgenden regen Briefwechsel sind einige Schreiben Zachs nach Bern erhalten, die sich von 1826 an bis zu Zachs Tod vor allem um dessen Blasensteine drehen. Schiferlis Nachlass liegt in der Burgerbibliothek Bern.
