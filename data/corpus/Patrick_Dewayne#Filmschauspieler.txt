Patrick Dewayne (* 4. April 1976 in Hanau) ist ein deutscher Schauspieler, Moderator und Sänger.

Dewayne wuchs bei Adoptiveltern auf. Nach seinem Abitur 1996 absolvierte er eine Ausbildung zum Bankkaufmann. Bis Mitte 2003 arbeitete er hauptberuflich an der Börse und dem Tradingfloor der Deutschen Bank in Frankfurt am Main, New York City und London.

Im Jahr 2004 spielte er in über 100 Folgen die Rolle des Kike Valdez in der Vorabendserie Gute Zeiten, schlechte Zeiten auf RTL. Passend zu seiner Filmfigur nahm Dewayne 2004 mit den Berliner Musikproduzenten Valicon (Silbermond, Bell, Book & Candle) und dem Frankfurter Produzenten Greg de Neufville das Album Close up auf, das beim Plattenlabel SonyBMG veröffentlicht wurde. 
Die erste Auskopplung Alles was bleibt wurde auf mehr als 750.000 Tonträgern (inklusive Compilations) veröffentlicht und kam unter die ersten 20 der deutschen Singlecharts. Auftritte in Fernsehsendungen wie Top of the Pops,[1]Bravo TV oder Exclusiv – Das Starmagazin folgten. 

Nachdem er seinen Vater in den USA und seine Mutter in Deutschland gefunden hatte, begann er, sich stärker mit seiner afroamerikanischen Herkunft zu befassen und trat der Gruppe SFD – Schwarze Filmschaffende in Deutschland bei. 

2007 übernahm er im Musical Martin Luther King – The King of Love eine Hauptrolle (als Zweitbesetzung).[2] In diesem Musical verkörperte Dewayne den schwarzen Bürgerrechtler Malcolm X und eines Priester. 2008 erhielt er bei Ivana Chubbuck Schauspielunterricht. Im Juli 2009 stand Dewayne für die Fernsehserie Ein Fall für zwei vor der Kamera, ebenso wie für den Kurzfilm Herr Schwiegersohn. 2010 mimte er einen Polizisten in dem englischsprachigen Kinofilm The Big Black. Ebenfalls im Jahr 2010 entstanden zwei Folgen für den „Kinder-Tatort“ Krimi.de auf KiKA, in denen Dewayne die Hauptrolle des Oberkommissars Anthony Nkruma verkörperte.

Dewayne war auch als Model für die Lufthansa, FAZ, Nokia oder Converse tätig.

2012 und 2013 spielte Patrick Dewayne in dem Theaterstück La Cage aux Folles (Ein Käfig voller Narren) in Frankfurt die Rolle des schwulen Butlers „Jacob“. Seit 2013 arbeitete Patrick auch für den inzwischen eingestellten deutschsprachigen Privatsender QLAR. Er moderierte dort die Sendungen KinoVision und Update.

2014 stand für den internationalen Kurzfilm The Heavy Load von Regisseur Joshua Krull vor der Kamera, sowie für die schweizerisch-amerikanische Kinoproduktion 12 Theses und die Großproduktion Point Break von Regisseur Ericson Core, ein Remake des Films Gefährliche Brandung mit Keanu Reeves und Patrick Swayze.

Seit 2015 Patrick Dewayne ist auch als Moderator für das Der Aktionär TV und den Nachrichtensender N24 von der Frankfurter Wertpapierbörse tätig. Dort berichtet er u. a. für Sendungen wie Börse am Mittag und Börse am Abend.

In den Jahre 2016 und 2017 trat Patrick Dewayne wieder vermehrt als Schauspieler in Erscheinung, so u.a. für die Bankenserie des ZDF/ARTE - Letterbox Filmproduktion, Bad Banks, unter der Regie des preisgekrönten Regisseurs, Christian Schwochow.

Bei diesem Projekt war Patrick Dewayne, durch sein Bankenwissen, auch als Berater und Coach der Extras involviert und stand auch fachlich mit Rat und Tat zu Seite.
