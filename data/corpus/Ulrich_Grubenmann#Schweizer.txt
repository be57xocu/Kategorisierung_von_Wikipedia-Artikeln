Ulrich Grubenmann (* 1668 in Teufen, Appenzell Ausserrhoden; † 27. Juni 1736 in Teufen) war ein Schweizer Baumeister.

Ulrich Grubenmann heiratete Barbara Zürcher, mit der er die drei Söhne Jakob, Johannes und Hans Ulrich hatte.
