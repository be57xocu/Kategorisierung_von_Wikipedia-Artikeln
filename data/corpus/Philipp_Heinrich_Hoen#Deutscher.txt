Philipp Heinrich von Hoen (Hoenonius, Reichsadel 1629; * 23. Juli 1576 in Diez/Lahn; † 28. April 1649 auf einer Dienstreise in Frankfurt/Main, bestattet in Dillenburg) war Jurist, Professor und Staatsmann.

Hoen besuchte die Lateinschule zu Diez und das Herborner Pädagogium (1588–1591), anschließend die dortige Hohe Schule Herborn (1594) und studierte Rechtswissenschaft an der Universität Jena und Universität Marburg. Er wurde 1604 zum Dr. iur. utr. promoviert. In Herborn war Hoen Schüler von Johannes Gottsleben, dem er in Verbundenheit seine im Jahre 1598 zu Jena erschienene »Dissertatio de variis feudorum divisionibus« widmete.

Nach Abschluss seines Studiums erhielt Hoen als Professor der Jurisprudenz die 2. Lehrstelle in Herborn, rückte 1606 in die 1. Stelle auf und übernahm das Rektorat sowie 1608 das Prorektorat, wurde im gleichen Jahr zum nassauischen Rat ernannt und verlegte seinen Wohnsitz nach Dillenburg. Noch während seiner Studienzeit hatte er sich als Hofmeister Graf Adolfs von Nassau-Siegen auf dessen Grand Tour durch die Schweiz, Frankreich und England wertvolle Einblicke in fremde Staatswesen verschaffen können, so dass er bald zum Kanzleidirektor und 1627 zum Geheimen Rat aufstieg.

Hoen diente allen Gliedern der nassauischen Grafenfamilie: so Graf Johann Ludwig von Nassau-Hadamar und Graf Johann Moritz von Nassau-Siegen (dem Brasilianer), Graf Georg von Nassau-Beilstein, der 1620 die Regentschaft in der Grafschaft Dillenburg angetreten hatte, dessen Sohn Ludwig Heinrich und der Landgräfin Juliana von Hessen-Darmstadt.

Hoens diplomatischem Geschick dürfte es, in Verbindung mit dem 1629 zu Wien erfolgten Übertritt von Johann Ludwig von Nassau-Hadamar zur katholischen Kirche, gelungen sein, einen gegen das nassauische Grafenhaus angestrengten fiskalischen Prozess, wegen Teilnahme am Böhmisch-Pfälzischen Krieg auf Seiten Kurfürst Friedrichs, durch Niederschlagung seitens des Wiener Hofgerichts zu beenden.

Ebenso dürfte er wesentlich dazu beigetragen haben, dass Graf Ludwig Heinrich von Nassau-Dillenburg, der unter Gustav Adolf ein Regiment zu Fuß und zu Pferd kommandierte, nach dem Prager Frieden zur kaiserlichen Partei übertrat.

Als kluger Ratgeber hat Hoen auch bei Einigungsbestrebungen zwischen den lutherischen und reformierten deutschen Reichsständen gewirkt (Leipziger Konvent 1631).

Als Syndikus der Wetterauer Grafenvereinigung, deren Interessen er damals vertrat, nahm er an 25 Grafentagen und verschiedenen Reichstagen teil.

Er war Verfasser zahlreicher juristischer Abhandlungen, die zumindest in Form von Disputationen erschienen sind. Von Hoen stammen auch mehrere juristisch gut fundierte Rechtsgutachten und Streitschriften, die teils den Erbauseinandersetzungen innerhalb des Hauses Nassau-Siegen (evangelische kontra katholische Linie), teils aber auch der Wahrung der Interessen der Häuser Nassau-Dillenburg und Nassau-Diez in deren Streit um die in der Grafschaft Diez gelegenen Kirchgüter galten, die - als Fundus der Hohen Schule Herborn - durch das Restitutionsedikt von 1629 gefährdet waren.
