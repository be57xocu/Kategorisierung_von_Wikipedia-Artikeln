Kurt Bauch (* 25. November 1897 in Neustadt i. Meckl.; † 1. März 1975 in Freiburg im Breisgau) war ein deutscher Kunsthistoriker.

Nach der Soldatenzeit im Ersten Weltkrieg absolvierte Kurt Bauch von 1919 bis 1921 ein Volontariat im Staatlichen Museum Schwerin. Seit 1922 studierte er Kunstgeschichte an der Universität Freiburg, wo er 1923 bei Hans Jantzen promoviert wurde und sich 1927 habilitierte. In Freiburg lehrte er bis 1932 als Privatdozent, danach am Kunstgeschichtlichen Institut der Universität Frankfurt am Main als Dozent. Seit dem 1. Mai 1933 war  er Mitglied der NSDAP. Zum Sommersemester 1933 wurde Kurt Bauch auf den Lehrstuhl für Kunstgeschichte der Universität Freiburg berufen.

Im Jahr 1932 begann eine freundschaftliche Beziehung zwischen Kurt Bauch und Martin Heidegger. Der Briefwechsel ist seit 2010 veröffentlicht.[1] 1939 schlug er einen Ruf an die Universität Hamburg aus nachdem ihm ein Ausbau des Freiburger Instituts zugesagt wurde. Bauch war Mitglied des NS-Dozentenbundes und dort Referent für Wissenschaft.[2]
Er wurde 1939 eingezogen und diente bis 1944 als Korvettenkapitän bei der Marine. Danach lehrte er weiter in Freiburg und wurde 1962 emeritiert.

Seit 1958 war Bauch Mitglied der Heidelberger Akademie der Wissenschaften.

Zu Bauchs wichtigsten Forschungsfeldern zählte die niederländische Malerei, insbesondere Rembrandt.
