Caryl Churchill (* 3. September 1938 in London) ist eine englische Autorin von Dramen. Sie ist bekannt für ihren Gebrauch nichtrealistischer Techniken und für ihre feministischen Themen.

Churchill wurde in London geboren. Während des Zweiten Weltkriegs emigrierte ihre Familie nach
Montreal, Kanada, wo sie die Trafalgar School für Mädchen besuchte. Sie kehrte für den Besuch der Universität nach England zurück und schloss 1960 ihr Studium der Englischen Literatur an der Oxford University ab. Dort begann sie auch ihre literarische Laufbahn, indem sie drei Stücke für studentische Theatergruppen schrieb: Downstairs, You’ve No Need to be Frightened und Having a Wonderful Time. 

1961 heiratete sie David Harter, einen Rechtsanwalt aus Oxford, und zog drei Söhne groß. Sie begann kurze Radiostücke für die BBC zu schreiben. Dazu gehörten The Ants (1962), Not, Not, Not, Not Enough Oxygen (1971) und Schreber’s Nervous Illness (1972). 

1972 schrieb Churchill Owners, ihr erstes Stück für die Bühne. Ihre sozialistischen Überzeugungen werden in dem Stück sehr deutlich. Sie kritisiert darin die Werte, die von den meisten Kapitalisten für selbstverständlich gehalten werden: aggressiv sein, vorwärtskommen, erfolgreich sein.
Von 1974 bis 1975 war sie „resident dramatist“ am Royal Court Theatre. Später begann sie eine Zusammenarbeit mit Theatergruppen wie Joint Stock und Monstrous Regiment (einer feministischen Theatervereinigung), die sich ausgedehnter Workshops bedienten, um neue Stücke zu entwickeln. Churchill benutzte auch weiterhin Workshops mit Improvisationen, um einige ihrer Stücke zu entwickeln.

Das erste Stück von ihr, das breite Aufmerksamkeit fand, war Cloud 9 (1979), das zum Teil in einer britischen Kolonie im Viktorianischen Zeitalter spielt. Sie untersucht darin die mit der Kolonisierung verbundenen Beziehungen und lässt Frauen Männerrollen spielen und umgekehrt, um komische und lehrreiche Effekte zu erzielen.

Churchills Schreiben wurde immer weniger von den Konventionen des Realismus eingeengt. Dabei entwickelte sie auch die feministischen Themen weiter. Top Girls (1982) ist ein Stück nur mit weiblichen Rollen und ist auf Marlene konzentriert, die ihr Heim und ihr Familienleben geopfert hat, um in der Geschäftswelt Erfolg zu haben. Die Hälfte der Handlung spielt während eines Festessens, bei dem Marlene historische und fiktive Frauen trifft, die in der Männerwelt Erfolg gehabt haben, aber immer um einen gewissen Preis.

In The Skriker (1994) benutzte Churchill eine assoziative Traumlogik, die einige Kritiker unsinnig fanden. Das Stück, eine visionäre Erkundung des modernen städtischen Lebens, folgt Skriker,
einer Art nördlicher Kobold, in seiner Suche nach Liebe und Rache, wie er zwei junge Frauen bis nach London verfolgt und dabei bei jeder Begegnung seine Gestalt wechselt.

Serious Money (1987) ist ein Versdrama, das einen satirischen Blick auf die Börse wirft. Es wurde begeistert aufgenommen, was zum Teil auch daran lag, dass es gerade nach dem Börsenkrach von 1987 herauskam.

Ihr Stück A Number (2002) behandelt das Thema menschliches Klonen.

Churchill schrieb auch Fernsehstücke für die BBC. Diese sowie einige ihrer Radiostücke wurden später für die Bühne adaptiert.

Churchill ist für den Neustadt International Prize for Literature 2016 nominiert.[1]