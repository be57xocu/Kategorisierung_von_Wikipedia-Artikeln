Aldo Nove, auch Aldo 9, (* 1967 in Viggiù; eigentlich Antonello Satta Centanin) ist ein italienischer Schriftsteller. Unter seinem bürgerlichen Namen ist er auch als erfolgreicher Lyriker bekannt.

Er gehört der italienischen Gruppe der „Kannibalen“ (nach der Anthologie: Gioventù Cannibale – Texte der hyperrealistischen, ungesitteten Generation (Einaudi 1996), in der er die Kurzgeschichte Il mondo dell’amore veröffentlichte) an. Seit dem Erscheinen von Amore mio infinito zählt er zur ersten Riege der jungen, zeitgenössischen Literatur.
