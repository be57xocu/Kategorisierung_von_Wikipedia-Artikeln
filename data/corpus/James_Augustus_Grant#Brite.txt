James Augustus Grant (* 11. April 1827 in Nairn, Schottland; † 11. Februar 1892 ebenda) war ein britischer Offizier und Afrikareisender. Er erforschte 1860 bis 1864 mit John Hanning Speke die Quellen des Nil.

Grant wurde in Nairn im schottischen Hochland geboren. Er besuchte das Marischal College in Aberdeen, welches heute Teil der University of Aberdeen ist. Er trat 1846 in die indische Armee ein und kämpfte während des Ersten und Zweiten Sikh-Krieges unter Hugh Gough (1849) und während des Sepoy-Aufstandes (1857–1858) in Indien. Er wurde unter Havelock 1857 beim Entsatz von Lakhnau verwundet und stieg bis zum Oberst auf.

Gemeinsam mit John Hanning Speke unternahm er 1860–63 eine Expedition zu den Nilquellen, bei der er bis zum Viktoriasee (heute in Uganda) gelangte. Das Tagebuch seiner zweieinhalbjährigen Reise veröffentlichte er 1864 als A Walk Across Africa. Grant erhielt für seine Verdienste eine Goldmedaille der Royal Geographical Society. 

1868 begleitete er die abessinische Expedition unter Robert Napier und trat dann als Oberstleutnant aus dem Dienst. 

Grant war seit dem 25. Juli 1865 mit Margaret Laurie verheiratet, mit der er zwei Söhne und drei Töchter hatte. Sein ältester Sohn trat in die Fußstapfen seines Vaters und unternahm ebenfalls Reisen durch Afrika. Er begleitete Joseph Thomson bei seiner Expedition zum Bangweulusee und kartografierte die Oberläufe des Kongo und des Sambesi. Der jüngere Sohn fiel im Februar 1900 als Kavallerieoffizier im Burenkrieg.
