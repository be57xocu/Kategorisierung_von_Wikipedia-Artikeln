Elie Bertrand (getauft 17. Mai 1713 in Orbe VD; † 23. August 1797 in Yverdon) war ein reformierter Pfarrer und Naturwissenschaftler aus der Schweiz.

Nach einem Theologiestudium an der Akademie von Lausanne, der Akademie von Genf und der Universität Leiden wirkte er von 1740 bis 1744 als Pfarrer in Ballaigues und Orbe und war 1744 bis 1765 zunächst Vikar und dann Pfarrer der französischen Gemeinde in Bern.

1765 wurde er Geheimrat des polnischen Königs Stanislaus II. August, leitete bis 1766 in Warschau das Ministerium für Industrie, Landwirtschaft und Naturwissenschaft und wurde 1768 für seine Verdienste in den polnischen Adelsstand erhoben. Bereits 1767 kehrte er jedoch wieder in die Schweiz nach Champagne bei Yverdon nieder.

Bertrand war Mitglied der Ökonomischen Gesellschaft in Bern, der Akademie der Wissenschaften zu Göttingen, der Brandenburgischen Akademie der Wissenschaften in Berlin, der Baierischen Akademie der Wissenschaften in München, der Königlich Schwedischen Akademie der Wissenschaften in Stockholm sowie der Akademien von Leipzig, Basel, Lyon und Florenz, korrespondierte mit Voltaire, Albrecht von Haller und Carl von Linné, und interessierte sich neben der Theologie vor allem für Philosophie, Sprachwissenschaft, Naturgeschichte, Seismologie und Hydrografie.
