Claude de France (* 13. Oktober 1499 in Romorantin; † 20. Juli 1524 in Blois), deutsch Claudia von Frankreich, aus dem Haus Valois war die Tochter des französischen Königs Ludwig XII. und durch Heirat mit dessen Thronfolger, seinem Cousin Franz I., Königin von Frankreich. Sie trug auch die Titel einer Gräfin von Soissons und einer Herzogin der Bretagne.

Die Tochter König Ludwigs XII. von Frankreich und dessen zweiter Ehefrau Anne de Bretagne, somit Prinzessin von Frankreich, trug ferner die Titel Gräfin von Soissons, Blois, Coucy, Étampes und Montfort. Sie war stets kränklich und litt seit ihrer Geburt unter einer Gehbehinderung. Ihre Mutter hatte noch zehn weitere Kinder geboren, von denen aber nur ihre elf Jahre jüngere Schwester Renée das Erwachsenenalter erreichte.

Anne de Bretagne verlobte ihre Tochter früh an Karl von Luxemburg, um die Bretagne nicht an Frankreich fallen zu lassen. Diese vertraglich vereinbarte Ehe wurde jedoch auf Drängen von Luise von Savoyen 1505 annulliert und Claude 1506 an Luises Sohn Franz von Orléans-Angoulême versprochen, einen königlichen Cousin, der mangels eines Sohns Ludwigs französischer Thronanwärter war. Zwischen Louise de Savoie und Anne de Bretagne herrschte darum Feindschaft, sodass Claude erst nach dem Tod ihrer Mutter mit Franz vermählt wurde. Die Heirat fand am 18. Mai 1514 in der Kapelle des Schlosses Saint-Germain-en-Laye statt. Neben ihrer Popularität und einem festeren Anspruch auf den Thron brachte Claude auch die Bretagne als Erbgut in die Ehe ein.

Nach dem Tod ihres Vaters am 1. Januar 1515 wurde Franz I. König von Frankreich und Claude seine Königin. Die junge Königin zeigte sich mehr an Religion als an Politik interessiert. In neun Jahren Ehe schenkte Claude Franz I. acht Kinder, starb aber bereits im Alter von fünfundzwanzig Jahren.

Sie wurde in der Grablege der französischen Könige, der Basilika Saint-Denis, beigesetzt. Bei der Plünderung der Königsgräber von Saint-Denis während der Französischen Revolution wurde ihr Grab am 20. Oktober 1793 geöffnet und geplündert, ihre Überreste wurden in einem Massengrab außerhalb der Kirche beerdigt.

Die Pflaumenart Reineclaude (Reine Claude = Königin Claudia) soll ihr zu Ehren benannt worden sein.

Aus der Ehe mit Franz I. gingen acht Kinder hervor:
