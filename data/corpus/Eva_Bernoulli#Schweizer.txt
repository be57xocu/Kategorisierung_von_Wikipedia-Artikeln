Eva Bernoulli (* 4. März 1903 in Berlin; † 12. Juni 1995 in Basel) war eine Schweizer Logopädin und Pädagogin aus der Familiendynastie Bernoulli.

Eva Bernoulli war die Tochter des schweizerischen Theologen und Schriftstellers Carl Albrecht Bernoulli und dessen Ehefrau Paula Heydenreich. Ihre Jugend verbrachte sie in Arlesheim. Mit 22 Jahren begann sie eine Ausbildung an der Schauspielschule Basel. 1928 beendete Bernoulli erfolgreich diese Ausbildung, und man betraute sie mit der Leitung des Chores am Basler Stadttheater.

Vier Jahre später begann Bernoulli ein Studium der Logopädie in Zürich. Nach Abschluss dieses Studiums ließ sie sich als selbstständige Therapeutin nieder und wurde 1948 die erste Logopädin der Schweiz. Sie erteilte u. a. Sprecherziehungskurse für Pädagogen und Pastoren. Nebenbei leitete sie verschiedene Laien-Theatergruppen.

Zwischen 1936 und 1940 war sie an den Kammerspielen Bernoulli in Basel tätig. Mit ihrer Sprachtherapie leistete sie massgebliche Arbeit bei Sprechbehinderungen. Nebenbei, aber dennoch sehr erfolgreich, trat Bernoulli mit vielen Rezitations-Programmen und Hörspielen an die Öffentlichkeit. Ausserdem engagierte sie sich sehr für den Erhalt der Basler Mundart. Um diesen Dialekt zu lernen, bot sie über Jahre spezielle Kurse an.

An ihrem 75. Geburtstag wurde Bernoulli als Ehrenmitglied in die Schweizer Gesellschaft für Phoniatrie aufgenommen und an ihrem 83. Geburtstag ehrte sie die Universität Basel mit dem Ehrentitel Dr. med. h.c.

Zeit ihres Lebens förderte Bernoulli Künstlerinnen und viele Jahre war sie engagiertes Mitglied im Zonta- und Lyceumsclub.

Im Alter von 92 Jahren starb Eva Bernoulli am 12. Juni 1995 in Basel.

