Johann Büttikofer (* 9. August 1850 in Ranflüh; † 24. Juni 1927 in Bern) war ein Schweizer Zoologe.

Nach einem Biologiestudium an der Universität Bern fand er 1879 eine Stelle am Rijksmuseum van Natuurlijke Historie in Leiden und wirkte von 1884 bis 1897 als dessen Kurator. Er unternahm zwei Reisen nach Liberia, die erste von 1879 bis 1882 und die zweite von 1886 bis 1887. Von 1893 bis 1894 begleitete er die Expedition von Anton Willem Nieuwenhuis ins Innere von Borneo. Zwischen 1897 und 1924 war er Direktor des Zoologischen Gartens von Rotterdam, und seinen Lebensabend verbrachte er in Bern.

1895 erhielt Büttikofer den Ehrendoktortitel der Universität Bern.
