Gustav Alfred Julius Meyer (* 5. Oktober 1891 in Göttingen; † 11. April 1945 in Hessisch Oldendorf) war ein nationalsozialistischer Funktionär. Er trat 1928 der NSDAP bei und war von 1930 bis 1945 Gauleiter des Gaus Westfalen-Nord und von 1933 bis 1945 Reichsstatthalter in Lippe und Schaumburg-Lippe. Nach dem Beginn des Krieges gegen die Sowjetunion wurde er Staatssekretär im Reichsministerium für die besetzten Ostgebiete.

Meyer wurde in ein evangelisches Elternhaus geboren. Sein Vater war zu dieser Zeit als preußischer Regierungsbaurat in Göttingen tätig und wurde nach weiteren Stationen schließlich Kreisbaurat in Soest. Nach dem Abitur am Archigymnasium im Jahr 1911 schlug Alfred Meyer die Offizierslaufbahn ein, wurde 1912 Fahnenjunker, 1914 Kompanie- und später Bataillonschef. Während des Ersten Weltkrieges wurde er zweimal verwundet und mit dem Eisernen Kreuz II. und I. Klasse ausgezeichnet; nachträglich erhielt er das Verwundetenabzeichen. Im April 1917 geriet er in französische Kriegsgefangenschaft, aus der er erst 1920 entlassen wurde. Die Reichswehr hatte keine Verwendung für ihn und entließ ihn als Hauptmann.

Im Alter von nun dreißig Jahren arbeitete Meyer als Werkstudent und studierte Rechts- und Staatswissenschaften sowie Nationalökonomie an den Universitäten in Lausanne, Bonn und Würzburg. 1922 promovierte er bei Christian Meurer mit dem Thema „Der belgische Volkskrieg“ zum Doktor der Staatswissenschaften (Dr. rer. pol.). Meyer war von 1923 bis 1930 Zechenbeamter in der juristischen Abteilung auf der Zeche Graf Bismarck in Gelsenkirchen. Am 1. April 1928 trat er der NSDAP bei und wurde sogleich Ortsgruppenleiter der Partei in Gelsenkirchen.[2] 1929/30 stieg er zum Leiter des Bezirks Emscher-Lippe auf. Im November 1929 wurde er als einziges NSDAP-Mitglied in den Rat der Stadt Gelsenkirchen gewählt.[3]
Im September 1930 zog er als Abgeordneter in den Reichstag für den Wahlkreis Westfalen-Nord ein. Sein am 31. Dezember 1932 niedergelegtes Reichstagsmandat wurde am 1. Januar 1933 von Heinrich August Knickmann fortgeführt.[4] Später im Jahr 1933 kehrte Meyer in den mittlerweile nationalsozialistischen Reichstag zurück und blieb dort bis 1945 Mitglied.[2]
Nachdem der NSDAP-Gau Westfalen geteilt worden war, wurde Meyer 1931 NSDAP-Gauleiter für Westfalen-Nord in Münster, ab dem 16. Mai 1933 Reichsstatthalter von Lippe und Schaumburg-Lippe und 1936 Führer der lippischen Landesregierung. In den Jahren 1932 und 1933 war er Mitglied des Preußischen Landtags.[2] 1938 wurde Meyer zum Oberpräsidenten der Provinz Westfalen ernannt und zum SA-Obergruppenführer befördert.

1941 wurde Alfred Meyer zum „ständigen Vertreter“ Alfred Rosenbergs in dessen Eigenschaft als Chef des im Juli 1941 durch Geheimerlass geschaffenen „Reichsministeriums für die besetzten Ostgebiete“ ernannt und erreichte damit den Höhepunkt seiner Karriere. In dieser Funktion, die er seiner langjährigen Bewährung als „Gefolgsmann“ Rosenbergs zu verdanken hatte, leitete Meyer vom Sommer 1941 bis November 1942 die drei Hauptabteilungen des Ministeriums für Politik, Verwaltung und Wirtschaft. Damit war er an der Ausbeutung und Plünderung der besetzten sowjetischen Gebiete und an der Unterdrückung, Verschleppung und Ermordung ihrer Bewohner, besonders der jüdischen Bevölkerung, an verantwortlicher Stelle unmittelbar beteiligt.

Zur Wannsee-Konferenz war Meyer eingeladen, weil im Verwaltungsgebiet seines Ministeriums der Holocaust durch die Einsatzgruppen der Sicherheitspolizei und des SD bereits begonnen hatte. Auf der Konferenz forderte Meyer, „gewisse vorbereitende Arbeiten“ jeweils an Ort und Stelle durchzuführen, ohne jedoch die Bevölkerung zu beunruhigen.[5] In einem Schreiben, datiert auf den 16. Juli 1942, schlug er vor, in der Sowjetunion gegen „jüdische Mischlinge“ dieselben Maßnahmen wie gegen Juden zu treffen. Den Vorteil seines Vorschlags sah Meyer in der Vorsorgewirkung der Maßnahme zur Stabilisierung der deutschen Herrschaft:

„Damit sind aber Befürchtungen dahin, dass aus diesen Mischlingen infolge ihres artverwandten Blutseinschlages besondere Gefahren gegen die deutsche Herrschaft im Ostraum erwachsen, nicht berechtigt.“[6]
Da Meyer seine Vorschläge an die Parteikanzlei, das Reichsinnenministerium, die Vierjahresplanbehörde, das Auswärtige Amt, das Rasse- und Siedlungshauptamt der SS sowie den Chef der Sicherheitspolizei und des SD sandte, ist es nach Ansicht seines Biographen, des Historikers Heinz-Jürgen Priamus, „auch den autoritätsorientierten, bürokratisch-eilfertigen ‚Bemühungen‘ Meyers zuzuschreiben, daß man den Judenbegriff bei den Ausrottungen im Osten außerordentlich weit faßte.“[7]
Im November 1942 übernahm Meyer als Gauleiter für Westfalen-Nord auch die Funktion eines „Reichsverteidigungskommissars“, die damals allen Gauleitern übertragen wurde, um Kompetenzstreitigkeiten zu beenden.

Am 11. April 1945 beging Meyer am Fuße des Hohensteins bei dem zu Hessisch Oldendorf gehörenden Ortsteil Zersen im Süntel Suizid.

Meyer wurde 1936 von der Stadt Bückeburg die Ehrenbürgerwürde verliehen.
Anfang Januar 2016 entzog der Rat der Stadt Bückeburg ihm posthum die Ehrenbürgerwürde. Obgleich die Ehrenbürgerwürde mit dem Tode erlischt, begründete der Bückeburger Bürgermeister diesen Schritt mit den Worten: „Wir wollen nicht, dass dieser Name in den Büchern der Stadt stehen bleibt.“

Clemens Becker |
Heinrich Drake |
Ernst Krappe |
Hans-Joachim Riecke |
Alfred Meyer |
Heinrich Drake

Siehe auch: Haus Lippe

Ludwig von Vincke (1816–1844) |
Eduard von Schaper (1845–1846) |
Eduard von Flottwell (1846–1850) |
Franz von Duesberg (1850–1871) |
Friedrich von Kühlwetter (1871–1882) |
Robert Eduard von Hagemeister (1883–1889) |
Conrad von Studt (1889–1899) |
Eberhard von der Recke von der Horst (1899–1911) |
Karl Prinz von Ratibor und Corvey (1911–1919) |
Felix von Merveldt (1919) |
Bernhard Wuermeling (1919–1922) |
Felix von Merveldt (1922) |
Johannes Gronowski (1922–1933) |
Ferdinand von Lüninck (1933–1938) |
Alfred Meyer (1938–1945) |
Rudolf Amelunxen (1945–1946)
