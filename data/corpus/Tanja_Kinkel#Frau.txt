Tanja Kinkel (* 27. September 1969 in Bamberg) ist eine deutsche Schriftstellerin, die unter anderem als Verfasserin historischer Romane bekannt wurde.

Mit acht Jahren begann Kinkel, Geschichten und Gedichte zu schreiben. 1978 gewann sie einen Jugendliteraturpreis und 1979 schrieb sie ihren ersten Roman. 1987 erhielt sie einen 1. Preis Fränkischer Jugendliteratur-Wettbewerb für den besten Einzeltext.

1988 begann sie ein Studium der Germanistik, Theater- und Kommunikationswissenschaft an der Ludwig-Maximilians-Universität in München. 1991 bekam Kinkel ein Stipendium an der Hochschule für Fernsehen und Film München, das sie für eine Ausbildung zum Drehbuchautor nutzte. 1992 folgte ein Förderpreis des Freistaates Bayern für junge Schriftsteller. Im gleichen Jahr gründete sie den Verein „Brot und Bücher e.V.“, der die Bildung von Kindern in Afrika, Deutschland und Indien fördert. 1995 hatte sie einen Förderaufenthalt des deutschen Innenministeriums in der „Casa Baldi“ in Olevano Romano bei Rom und erhielt 1996 ein Stipendium in der Villa Aurora in Los Angeles.

1997 wurde sie mit einer Arbeit über das Werk Lion Feuchtwangers promoviert. 2000 erhielt Kinkel den Kulturpreis der Oberfränkischen Wirtschaft. 2001 war sie im  Beirat des Bertelsmann Buchclubs (bis zu dessen Auflösung Ende 2002). Sie war 2001 Gründungsmitglied der Internationalen Feuchtwanger Gesellschaft in Los Angeles. 

Das Bayerische Staatsministerium für Unterricht, Kultus, Wissenschaft und Kunst berief sie 2006 in das Kuratorium des Internationalen  Künstlerhauses Villa Concordia, Bamberg. Im selben Jahr wurde Kinkel im Rahmen der Ausstellung 100 Köpfe von morgen als kreative Leistungsträgerin gewürdigt.

Kinkel ist Mitglied im PEN-Zentrum Deutschland und im Bundesverband junger Autoren und Autorinnen e.V. (BVjA). Sie lebt in München.
