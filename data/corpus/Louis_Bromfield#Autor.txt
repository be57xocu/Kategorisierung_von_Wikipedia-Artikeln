Louis Bromfield, eigentlich Louis Brumfield, (* 27. Dezember 1896 in Mansfield, Ohio; † 18. März 1956 in Columbus, Ohio) war ein US-amerikanischer Schriftsteller.

Bromfields Vorfahren waren unter den ersten Siedlern Ohios; sein Vater war Charles Brumfield und seine Mutter war Annette M. Coulter. Nach seiner Schulzeit begann er 1914 Landwirtschaft an der Cornell University (Ithaca) zu studieren. 1916 wechselte er an die Columbia University (New York) um dort, Journalismus, Literatur und Philosophie zu studieren. Dort schloss er sich auch der Studentenverbindung Phi Delta Theta an.

Durch diese Vereinigung machte Bromfield auch Bekanntschaft mit dem American Field Service. Als dessen Mitglied kam er während des Ersten Weltkriegs auch nach Frankreich. Später kämpfte er dort auch als Soldat. Nach dem Krieg kehrte er in die USA zurück und ließ sich in New York nieder. Dort arbeitete er als Lektor und Dramaturg und als Musik- und Theaterkritiker[1] und begann sein erstes Buch zu schreiben.

1921 heirate Bromfield in New York Mary Appleton Wood, eine Tochter des Staatsanwalts Chalmers Wood und dessen Ehefrau Ellen Apleton Smith. Mit seiner Ehefrau hatte Bromfield drei Töchter, Ann, Hope und Ellen. 1924 konnte Bromfield mit Das Leben der Lily Shane (The Green Bay Tree) sehr erfolgreich debütieren. Diesem ersten Band einer Tetralogie folgten schon bald – ebenfalls erfolgreich – die Bände Die Besessenen (Possession), Früher Herbst (Early Autumn) und Welch eine Frau ... (A good woman).

Ab 1928 verbrachte Bromfield mehrere Jahre in Frankreich und in Indien. In Frankreich arbeitete er auch journalistisch, einen großen Teil seiner Zeit verwendete er aber für sein eigenes literarisches Werk. 1938 kehrte er zurück und ließ sich im Richland County (Ohio) nieder. Dort gründete er seine Malabar Farm[2] (heute Malabar Farm State Park).

Mit beinahe 60 Jahren starb Louis Bromfield am 18. März 1956 in Columbus (Ohio) und fand dort auch seine letzte Ruhestätte.
