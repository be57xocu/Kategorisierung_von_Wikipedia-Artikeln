Louis Étienne de Thouvenin (* 12. November 1791 zu Moyenvic, Département Meurthe; † 1882) war ein französischer Offizier und Waffenkonstrukteur.

Thouvenin wurde 1811 Artillerieleutnant im französischen Heer, focht mit Auszeichnung in den Feldzügen der Koalitionskriege 1813 bis 1815, dann 1823 in Spanien, 1828 in Griechenland, trat 1853 als Général de brigade in den Ruhestand und starb 1882.

Er schlug 1840 eine Verbesserung des gezogenen Gewehrs vor, indem er einen Dorn in der Schwanzschraube anbrachte, und konstruierte 1844 eine Dornbüchse mit Langgeschoss, die 1846 angenommen, fast in allen Heeren als Jägerwaffe, auch als Pirsch- und Scheibenbüchse benutzt und erst durch das Minie- und Zündnadelgewehr verdrängt wurde.
