Robert Gadocha (* 10. Januar 1946 in Kraków, Polen; genannt Piłat) ist ein ehemaliger polnischer Fußballspieler.

Er galt als dribbelstarker Linksaußen, der für Polen 62 Länderspiele bestritt und dabei 16 Tore erzielte.

