Laurie R. King (* 1952) ist eine US-amerikanische Schriftstellerin.

King wuchs in San Francisco auf und studierte Theologie an den Universitäten von Santa Cruz und Berkeley. Später bereiste sie mit ihrem Mann zwanzig Länder auf fünf Kontinenten. Ihr erstes Buch The beekeeper's apprentice (dt. Die Gehilfin des Bienenzüchters) entstand 1987 und wurde erst 1994 veröffentlicht. Es bildet den Auftakt einer Reihe von Pastiche-Krimis mit den Hauptfiguren Mary Russell und Sherlock Holmes.

Die Autorin lebt heute mit ihrer Familie im kalifornischen Watsonville.
