Russell Albion „Russ“ Meyer (* 21. März 1922 in Oakland, Kalifornien; † 18. September 2004 in Los Angeles, Kalifornien) war ein US-amerikanischer Regisseur, Drehbuchautor und Produzent. Bekannt wurde er durch erfolgreiche Low-Budget-Filme, die dem Sexploitation-Genre zuzuordnen sind.

Meyer wurde als Sohn eines deutschstämmigen Polizisten geboren, der die Familie noch vor der Geburt seines Sohnes verließ. Schon im Alter von 15 Jahren drehte Meyer zu Hause kleinere Filme. Im Zweiten Weltkrieg war er als Kriegsberichterstatter in Europa stationiert. Nach seiner Rückkehr in die USA fand er Arbeit als Fotograf, unter anderem für das neu gegründete Magazin Playboy.

1958 gründete er mit seiner damaligen Frau Eve eine eigene Produktionsfirma und konnte schon mit einem seiner ersten Filme, Der unmoralische Mr. Teas (The Immoral Mr. Teas) 1959 über 1 Million US-Dollar einnehmen, was ihm erlaubte, sich von nun an ausschließlich dem Filmemachen zu widmen.

Im folgenden Jahrzehnt drehte er um die zwanzig Filme, die durch eine möglichst schnelle und billige Produktion oft ein Vielfaches ihres Budgets an Gewinn einbrachten. Sein größter Erfolg war der Film Blumen ohne Duft (Beyond the Valley of the Dolls) aus dem Jahr 1970, der ihm bei einem Budget von etwa 1 Million einen Erlös von 6 Millionen Dollar einbrachte. Weitere bekannte Filme Meyers sind Die Satansweiber von Tittfield (Faster, Pussycat! Kill! Kill!) und Im tiefen Tal der Superhexen (Beneath the Valley of the Ultra-Vixens). In den USA der 1960er Jahre galten viele seiner Filme als Pornografie, mittlerweile werden sie in Unterscheidung zu Hardcore-Pornofilmen als „Softcore“ bezeichnet.

Seine Darstellerinnen wählte er stets entsprechend seiner Vorliebe für extrem große Oberweiten aus (in einem Playboy-Interview gab er an, mindestens 120 cm), was ihm oft als Sexismus vorgeworfen wurde, von seinen Fans aber als Kult bezeichnet wird. Immer wieder entdeckte er neue Darstellerinnen mit abnorm wirkenden Oberweiten und oft exotischen Künstlernamen, darunter Babette Bardot, Raven de la Croix, Uschi Digard, Erica Gavin, Haji, Tura Satana, Lorna Maitland, Kitten Natividad, Lori Williams, Susanne Weigandt und Pandora Peaks. In einem Interview mit der Süddeutschen Zeitung am 22. Oktober 1993 sagte Meyer: „Hätte ich mich nicht so sehr für Titten interessiert, wäre aus mir vielleicht ein großer Filmemacher geworden“.

Im Jahr 1972 war Meyer mit fünf seiner Filme Teilnehmer der Kasseler Documenta 5 in der Abteilung Filmschau: Russ Meyer-Retrospektive. 1983 gab es eine Retrospektive von Russ-Meyer-Produktionen in der Pariser Cinémathèque française. Drei seiner Filme wurden in die Sammlung des New Yorker Museum of Modern Art aufgenommen.

Meyer war insgesamt dreimal verheiratet. Diese Ehen mit Betty Valdovinos, Eve Turner und Edy Williams endeten jeweils durch Scheidung. Danach lebte er mehrere Jahre mit Kitten Natividad[1] und später mit der Stripperin Melissa Mounds zusammen.

Die Titel der Russ-Meyer-Filme haben einige Rock-Bands zu ihrem Namen inspiriert. Neben der Gründungsvätern des Grunge Mudhoney haben sich ebenso Vixen, Motorpsycho und Faster Pussycat bei der Namensfindung von seinen Filmen anregen lassen.

als Regisseur

als Darsteller
