Walther Christoph Zimmerli (* 6. Mai 1945 in Zürich) ist ein Schweizer Professor für Philosophie.

Seine Eltern waren Irmgard Zimmerli, geborene von der Ropp, und der Theologieprofessor Walther Theodor Zimmerli (1907–1983). Seine Schulzeit verbrachte Walther Zimmerli an einem humanistischen Gymnasium in Göttingen. In der Schweiz absolvierte er einen zweijährigen Militärdienst. 1963 verbrachte er einen Studienaufenthalt in Yale.

An den Universitäten Göttingen und Zürich studierte er Philosophie, Germanistik und Anglistik. Nach seiner Promotion 1971 wurde als Assistent und Lehrbeauftragter für Philosophie in Zürich und habilitierte sich 1978. Anschließend wechselte er auf eine ordentliche Professur an die Technische Universität Braunschweig, wo er zehn Jahre lang blieb. Nächste Stationen waren die Universitäten Bamberg und Erlangen-Nürnberg. 1996 wechselte er an die Philipps-Universität Marburg und 1999 (als Nachfolger des Gründungspräsidenten Konrad Schily) an die private Universität Witten/Herdecke.

Von Juni 2002 bis Ende März 2007 war er Gründungspräsident der Volkswagen AutoUni Wolfsburg. 2003 ernannte die TU Braunschweig Zimmerli zum Honorarprofessor.

Am 27. Februar 2007 wurde er vom Senat der Brandenburgischen Technischen Universität Cottbus mit knapper Mehrheit vor seinem Gegenkandidaten, Michael Daxner, zum neuen Präsidenten der Universität gewählt. Am 15. Mai 2007 hatte er sein Amt als Präsident angetreten und bis zum 30. Juni 2013 inne.

Zimmerli ist unter anderem bekannt im Bereich der politischen Philosophie, wo er die Meinung vertritt, dass der sogenannte totale Frieden als Kombination von Freiheit und Freundschaft in allen Bereichen des menschlichen Zusammenlebens herbeigeführt werden kann.

Zimmerli ist oder war Mitglied von acatech (Konvent für Technikwissenschaften), des Senats der Stiftung Niedersachsen[1] und des Senats der GISMA Business School in Hannover sowie ordentliches Einzelmitglied der Schweizerischen Akademie der Technischen Wissenschaften (SATW)[2].

Er ist verheiratet, hat vier Kinder und acht Enkel.
