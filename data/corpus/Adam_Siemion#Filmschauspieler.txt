Adam Siemion (* 10. März 1981) ist ein polnischer Fernseh- und Filmschauspieler.

Seinen ersten Auftritt hatte er bereits 1990 in Andrzej Wajdas und Agnieszka Hollands Film Korczak als Janusz Korczaks Zögling Abramek. Drei Jahre später wirkte er in Steven Spielbergs Schindlers Liste mit. 1997 war er in Spellbinder – Im Land des Drachenkaisers zu sehen.

Dem deutschen Fernsehpublikum wurde Siemion auch durch die Hauptrolle des Matti Lamminen in der finnisch-deutsch-polnisch-schweizerischen Kinder- und Jugendserie Ein Rucksack voller Abenteuer (poln. Plecak pelen przygod) bekannt, die 1993 im ZDF ausgestrahlt wurde.

In jüngerer Zeit spielte er Rollen in der Fernsehserie Kopciuszek (2006/2007), im TV-Serial Kryminalni (2004–2008), in Dwie strony medalu (2007) von Grzegorz Warchoł oder in Popiełuszko. Wolność jest w nas (2009) des Regisseurs Rafał Wieczyński.
