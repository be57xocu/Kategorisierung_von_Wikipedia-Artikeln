Georges Gilles de la Tourette (vollständiger Name Georges Albert Édouard Brutus Gilles de la Tourette; * 30. Oktober 1857 in Saint Gervais les Trois Clochers bei Loudun; † 22. Mai 1904 in Prilly bei Lausanne) war ein französischer Neurologe und Rechtsmediziner.

Tourette begann mit 16 Jahren ein Medizinstudium in Poitiers und setzte es in Paris fort. 1885 schloss er das Studium ab, wurde 1893 Krankenhausarzt und 1900 Dozent. Er war einer von Jean-Martin Charcots Lieblingsschülern, sein Hausarzt und selbsternannter Sekretär. Charcot förderte im Gegenzug die akademische Karriere Tourettes.

Tourette beschäftigte sich zunächst intensiv mit neuen Therapieformen wie Vibrationstherapie und Hypnose. Auch Sigmund Freud besuchte Tourettes Vorlesungen.

1896 wurde Tourette von einer paranoiden jungen Frau, einer Patientin am Hôpital Salpêtrière, in den Kopf geschossen. Sie behauptete, wider Willen hypnotisiert worden zu sein und davon ihre Gesundheit verloren zu haben. Der Vorgang erregte großes Aufsehen und nährte die Vorstellung, dass Anstiftung zu kriminellen Handlungen unter Hypnose möglich seien, eine These, die Tourette vehement ablehnte. Er wurde nur leicht verletzt und überlebte das Attentat. In der Folgezeit litt Tourette unter starken Gemütsschwankungen, was auf eine Syphiliserkrankung zurückzuführen war. Ab 1901 war er nicht mehr arbeitsfähig und verstarb in der Schweiz in der psychiatrischen Klinik Cery in Prilly bei Lausanne an den Folgen der Neurosyphilis. 

Tourette erbrachte fundamentale Beiträge zur Hysterie und medizinrechtlichen Aspekten der Hypnose. 1884 begann er eine Studie an neun Patienten mit kompulsiven Tics über das nach ihm benannte Tourette-Syndrom.
