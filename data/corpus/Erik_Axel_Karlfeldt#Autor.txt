Erik Axel Karlfeldt (geboren als Erik Axel Eriksson; * 20. Juli 1864 in Karlbo bei Avesta; † 8. April 1931 in Stockholm) war ein schwedischer Lyriker und Literaturnobelpreisträger (1931).

Karlfeldt wuchs als Sohn des Bauern Erik Eriksson und dessen Frau Anna Jansdotter in Karlbo, Dalarna auf. Als jedoch im Nachbarort Krylbo ein Bahnhof eröffnet wurde, wodurch sich Industrie ansiedelte, begann Eriksson, dubiose Geschäfte zu machen, was 1885 schließlich zu seiner Verhaftung wegen Urkundenfälschung führte. Um sich von ihm zu distanzieren nahm sein Sohn 1889 den Namen Karlfeldt an.

Karlfeldt besuchte ein Gymnasium in Västerås, wo er 1885 sein Abitur erhielt. Danach studierte in Uppsala u.a. Literaturgeschichte und war von 1892 bis 1912 Lehrer und Bibliothekar. In dieser Zeit erschienen auch seine ersten Gedichtbände. 

Der Neuromantik verpflichtet, fand Karlfeldt die Motive und Themen für seine Gedichte in Heimatgeschichte, Sagen, Volksglauben sowie in der Bibel. Bereits als Schüler schrieb er Gedichte, während seines Studiums veröffentlichte er einige unter Pseudonymen in Zeitungen. Größeren Erfolg hatte er jedoch erst 1898 und 1901 mit Fridolins Visor und Fridolins lustgård och dalmålningar på rim. Beeinflusst wurde er von Verner von Heidenstam und Gustaf Fröding.

1904 wurde Karlfeldt, der als einer der talentiertesten Dichter Schwedens galt, Mitglied der Schwedischen Akademie (Svenska Akademien). 1912 wurde er auch zu ihrem Sekretär gewählt. Diesen Posten behielt er bis zu seinem Tod. 1917 verlieh die Universität Uppsala ihm einen Ehrendoktortitel.

Erik Axel Karlfeldt starb am 8. April 1931 in Stockholm an Bronchitis. Zu diesem Zeitpunkt galt er als der beliebteste Dichter Schwedens. Er wurde in seinem Heimatort Krylbo beigesetzt.

Im Oktober 1931 wurde Karlfeld der Nobelpreis für Literatur verliehen. Er ist bis heute der einzige Literaturnobelpreisträger, der postum ausgezeichnet wurde. Die postume Verleihung rief Kritik hervor, widersprach allerdings nicht den Statuten der Nobelstiftung, weil die Nominierung durch Nathan Söderblom bereits vor Karlfeldts Tod erfolgt war.

Karlfeldt war zu Lebzeiten bereits mehrfach, erstmals 1918, für den Nobelpreis vorgeschlagen worden. Er hatte jedoch, da er selbst Mitglied des Komitees war, die Ehrung immer abgelehnt. Vor seinem Tod hatte er jedoch bereits seinen Rücktritt angekündigt, so dass die Grundlage seiner Bedenken entfallen war. Daher entschied sich die Jury, dem Wunsch des im Juli 1931 ebenfalls verstorbenen Friedensnobelpreisträgers Söderblom zu entsprechen.

In deutscher Sprache sind zwei Auswahlbände Gedichte (1938 und 1969) sowie Fridolins Lieder (1944) erschienen. Letzterer enthält die Gedichte aus Fridolins visor und Fridolins lustgård och dalmålningar på rim.

Prudhomme (1901) |
Mommsen (1902) |
Bjørnson (1903) |
F. Mistral/Echegaray (1904) |
Sienkiewicz (1905) |
Carducci (1906) |
Kipling (1907) |
Eucken (1908) |
Lagerlöf (1909) |
Heyse (1910) |
Maeterlinck (1911) |
Hauptmann (1912) |
Tagore (1913) |
nicht verliehen (1914) |
Rolland (1915) |
Heidenstam (1916) |
Gjellerup/Pontoppidan (1917) |
nicht verliehen (1918) |
Spitteler (1919) |
Hamsun (1920) |
France (1921) |
Benavente (1922) |
Yeats (1923) |
Reymont (1924) |
Shaw (1925) |
Deledda (1926) |
Bergson (1927) |
Undset (1928) |
Mann (1929) |
Lewis (1930) |
Karlfeldt (1931) |
Galsworthy (1932) |
Bunin (1933) |
Pirandello (1934) |
nicht verliehen (1935) |
O’Neill (1936) |
Martin du Gard (1937) |
Buck (1938) |
Sillanpää (1939) |
nicht verliehen (1940–1943) |
Jensen (1944) |
G. Mistral (1945) |
Hesse (1946) |
Gide (1947) |
Eliot (1948) |
Faulkner (1949) |
Russell (1950) |
Lagerkvist (1951) |
Mauriac (1952) |
Churchill (1953) |
Hemingway (1954) |
Laxness (1955) |
Jiménez (1956) |
Camus (1957) |
Pasternak (1958) |
Quasimodo (1959) |
Perse (1960) |
Andrić (1961) |
Steinbeck (1962) |
Seferis (1963) |
Sartre (1964) |
Scholochow (1965) |
Agnon/Sachs (1966) |
Asturias (1967) |
Kawabata (1968) |
Beckett (1969) |
Solschenizyn (1970) |
Neruda (1971) |
Böll (1972) |
White (1973) |
Johnson/Martinson (1974) |
Montale (1975) |
Bellow (1976) |
Aleixandre (1977) |
Singer (1978) |
Elytis (1979) |
Miłosz (1980) |
Canetti (1981) |
García Márquez (1982) |
Golding (1983) |
Seifert (1984) |
Simon (1985) |
Soyinka (1986) |
Brodsky (1987) |
Mahfuz (1988) |
Cela (1989) |
Paz (1990) |
Gordimer (1991) |
Walcott (1992) |
Morrison (1993) |
Ōe (1994) |
Heaney (1995) |
Szymborska (1996) |
Fo (1997) |
Saramago (1998) |
Grass (1999) |
Gao (2000) |
Naipaul (2001) |
Kertész (2002) |
Coetzee (2003) |
Jelinek (2004) |
Pinter (2005) |
Pamuk (2006) |
Lessing (2007) |
Le Clézio (2008) |
Müller (2009) |
Vargas Llosa (2010) |
Tranströmer (2011) |
Mo (2012) |
Munro (2013) |
Modiano (2014) |
Alexijewitsch (2015) |
Dylan (2016) |
Ishiguro (2017)
