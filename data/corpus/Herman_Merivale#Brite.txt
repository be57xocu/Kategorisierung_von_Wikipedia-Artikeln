Herman Merivale (* 8. November 1806 in Dawlish, Devon; † 8. Februar 1874) war ein englischer Staatsbeamter und ebenfalls als Autor tätig. Er war der ältere Bruder von Charles Merivale und Vater des Dichters Herman Charles Merivale.

Geboren wurde er in Dawlish, Devonshire. Er besuchte die Harrow Schule. Ab dem Jahre 1823 besuchte er das Oriel College in Oxford. Im Jahre 1825 wurde er Student im Trinity College und gewann den Irischen „scholarship“. Drei Jahre später wurde er vom Balliol College ausgewählt. Er wurde Mitglied des Inner Tempels und praktizierte im westlichen Bezirk.

Von 1837 bis 1842 war er Professor der Volkswirtschaft in Oxford. Im Jahre 1859 wurde er zum „permanent  under-secretaryship“ von Indien ernannt. 1870 wurde Merivale D.C.L von Oxford.
