Petra Fuhrmann (* 19. Oktober 1955 in Wiesbaden) ist eine deutsche SPD-Politikerin und ehemaliges Abgeordnete des Hessischen Landtags.

Fuhrmann studierte Politikwissenschaft und Chemie an der TH Darmstadt. Nach dem Staatsexamen 1982 war sie zunächst in der Erwachsenenbildung tätig. Außerdem arbeitete sie als Mitarbeiterin einer Europaabgeordneten sowie als 2. Frauenbeauftragte der Landeshauptstadt Wiesbaden.

Von 1991 bis 1994 leitete sie das Parlamentsreferat im Hessischen Ministerium für Frauen, Arbeit und Sozialordnung. Von 1994 bis 2014 ist Fuhrmann Mitglied des Hessischen Landtags. Sie wurde jeweils über die Landesliste gewählt. Als Direktkandidatin im Wahlkreis Hochtaunus I unterlag sie jeweils den CDU-Kandidaten. Die Schwerpunkte ihrer Arbeit sind Sozial-, Gleichstellungs- und Finanzpolitik. Fuhrmann ist frauen- und arbeitsmarktpolitische Sprecherin der SPD-Landtagsfraktion.

Darüber hinaus ist sie Mitglied des SPD-Parteirats, des Bezirksvorstands der SPD Hessen-Süd und des Landesvorstands der SPD Hessen und war Vorsitzende der Arbeitsgemeinschaft sozialdemokratischer Frauen (AsF) im SPD-Bezirk Hessen-Süd.

Sie wohnt in Friedrichsdorf.
