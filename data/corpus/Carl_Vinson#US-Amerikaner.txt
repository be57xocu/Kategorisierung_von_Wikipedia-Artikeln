Carl Vinson (* 18. November 1883 im Baldwin County, Georgia; † 1. Juni 1981 in Milledgeville, Georgia) war ein US-amerikanischer Politiker (Demokratische Partei).

Der  Jurist wurde im Alter von 25 in das Repräsentantenhaus von Georgia gewählt. Nachdem er kurze Zeit als Richter tätig gewesen war, kandidierte er für das US-Repräsentantenhaus und wurde als bis damals jüngster Kongressabgeordneter am 3. November 1914 vereidigt.

Während seiner Amtszeit widmete sich Vinson in erster Linie der Landesverteidigung. Er war seit 1920 Mitglied im Ausschuss für Marineangelegenheiten des Kongresses und von 1931 bis 1946 dessen Vorsitzender. Vor allem setzte er sich während der Weltwirtschaftskrise für die Erneuerung der United States Navy ein; der Hauptgrund hierfür war ursprünglich, Arbeitsplätze zu schaffen. Beim Kriegseintritt 1941 profitierten die USA daher sehr von Vinsons Bemühungen. 1944/45 wurde er bekannt dafür, dass er sich massiv für die Beförderung von William F. Halsey zum Fleet Admiral im Kongress einsetzte und die von Admiral Raymond A. Spruance verhinderte, obwohl beide gleichermaßen in der US-Pazifikflotte erfolgreich die 3. bzw. 5. Flotte führten. 

Auch nach dem Zweiten Weltkrieg galt sein Hauptaugenmerk der Modernisierung der US-Streitkräfte. In dem 1946 gebildeten United States House Committee on Armed Services war er Mitglied bis 1949 Mitglied und danach Vorsitzender bis 1965, als er aus dem Kongress ausschied.

Er gehörte zu den Unterzeichnern des Southern Manifesto von 1956, das die weitere Rassentrennung in öffentlichen Schulen verlangte, entgegen einem Grundsatzurteil des Obersten Gerichtshofes der Vereinigten Staaten von 1954 (Brown v. Board of Education).

Vinson wurde 26-mal in Folge in den Kongress gewählt, er stellte damit einen Rekord auf, der erst 1994 von Jamie Lloyd Whitten eingestellt wurde. 1964 verlieh ihm Präsident Lyndon B. Johnson mit der „Presidential Medal of Freedom with Special Distincton“ die höchste Auszeichnung, die in den USA an Zivilpersonen vergeben wird.

Vinson war seit 1921 verheiratet mit Mary Green. Beide hatten keine Kinder. Sein Neffe Sam Nunn war 24 Jahre lang von 1972 bis 1996 Senator für Georgia.

Vinsons Namen tragen 

1. Bezirk: Baldwin |
J. Jones |
Milledge |
Early |
H. Cobb I |
Barnett |
Wilde |
Abbot |
Haynes |
Newnan |
Coffee |
Dawson |
Cooper |
A. Stephens |
King |
J.W. Jackson |
Seward |
Love |
Clift |
Paine |
MacIntyre |
Rawls |
Sloan |
Hartridge |
W.B. Fleming |
Nicholls |
G. Black |
Nicholls |
T. Norwood |
Lester |
Overstreet |
C. Edwards |
Overstreet |
Moore |
C. Edwards |
Parker |
Peterson |
Preston |
Hagan |
Ginn |
Thomas |
Kingston |
Carter • 2. Bezirk: J. Jackson I |
A. Wayne |
Milledge |
Carnes |
Milledge |
Taliaferro |
D. Meriwether |
Troup |
W. Lumpkin |
T. Cobb |
A. Cuthbert |
Floyd |
T. Foster |
Owens |
E. Black |
T. Foster |
Stiles |
S. Jones |
Iverson |
Wellborn |
J. Johnson |
A. Colquitt |
M. Crawford |
Tift |
Whiteley |
Smith |
Turner |
Russell |
Griggs |
Roddenbery |
Park |
Cox |
Pilcher |
O’Neal |
Mathis |
Hatcher |
Bishop • 3. Bezirk: Mathews |
Willis |
Bryan |
Smelt |
Hall |
J. Crawford |
Gilmer |
Cary |
Fort |
H. Lamar |
Schley |
Cleveland |
Alford |
E. Black |
Towns |
J.W. Jones |
A. Owen |
Bailey |
Trippe |
Hardeman |
W. Edwards |
Bethune |
Bigby |
P. Cook |
C.F. Crisp |
C.R. Crisp |
E. Lewis |
Hughes |
C.R. Crisp |
Castellow |
Pace |
Forrester |
Callaway |
Brinkley |
Ray |
M. Collins |
Marshall |
Westmoreland |
Ferguson • 4. Bezirk: S. Hammond |
Mead |
Spalding |
Bibb |
A. Cuthbert |
Z. Cook |
J. Cuthbert |
Tattnall |
Gilmer |
J. Wayne |
J.Y. Jackson |
W. Colquitt |
Holt |
Gamble |
Millen |
Clinch |
Haralson |
Murphey |
Dent |
Warner |
Gartrell |
Gove |
Long |
T. Speer |
Beck |
Harris |
Persons |
Buchanan |
Harris |
Grimes |
Moses |
Adamson |
W. Wright |
E. Owen |
Camp |
Flynt |
MacKay |
Blackburn |
Levitas |
Swindall |
B. Jones |
Linder |
McKinney |
Majette |
McKinney |
H. Johnson • 5. Bezirk: Forsyth |
Reid |
Forsyth |
Wilde |
Sanford |
Glascock |
Cooper |
J.A. Meriwether |
H. Cobb II |
J. Lumpkin |
Hackett |
Chastain |
J. Lumpkin |
A. Wright |
Underwood |
Prince |
Corker |
DuBose |
Freeman |
M. Candler |
N. Hammond |
Stewart |
Livingston |
W.S. Howard |
Upshaw |
Steele |
Ramspeck |
Mankin |
J.C. Davis |
Weltner |
F. Thompson |
A. Young |
Fowler |
J. Lewis • 6. Bezirk: Telfair |
W. Terrell |
W. Thompson |
Gamble |
J. Terrell |
Holsey |
Habersham |
G. Crawford |
Haralson |
H. Cobb II |
Hillyer |
H. Cobb II |
J. Jackson II |
P. Young |
W. Price |
Blount |
Cabaniss |
Bartlett |
Wise |
Rutherford |
Mobley |
Vinson |
Flynt |
Gingrich |
Isakson |
T. Price |
Handel • 7. Bezirk: T. Cobb |
Wilde |
J. Meriwether |
W. Lumpkin |
Clayton |
Towns |
Alford |
Towns |
King |
J. Lamar |
Chappell |
A. Stephens |
D. Reese |
N. Foster |
Hill |
P. Young |
Felton |
Clements |
Everett |
Maddox |
Lee |
Tarver |
Lanham |
Mitchell |
J.W. Davis |
McDonald |
Darden |
Barr |
Linder |
Woodall • 8. Bezirk: Gilmer |
Grantland |
Nisbet |
W. Colquitt |
J. Lumpkin |
Toombs |
A. Stephens |
J.J. Jones |
A. Stephens |
S. Reese |
Carlton |
Lawson |
W.M. Howard |
Tribble |
Rucker |
Brand |
Deen |
W. Gibbs |
F. Gibbs |
Gibson |
Wheeler |
Blitch |
Tuten |
Stuckey |
Evans |
Rowland |
Chambliss |
M. Collins |
Westmoreland |
Marshall |
A. Scott • 9. Bezirk: S. Jones |
Haynes |
Warren |
H. Bell |
E. Speer |
H. Bell |
A. Candler |
Winn |
Tate |
T. Bell |
Wood |
Whelchel |
Wood |
Landrum |
Jenkins |
Deal |
C. Norwood |
Deal |
Graves |
D. Collins • 10. Bezirk: Hardeman |
Barnes |
Watson |
J. Black |
W.H. Fleming |
Hardwick |
Vinson |
Brand |
Brown |
R. Stephens |
Barnard |
C. Johnson |
C. Norwood |
Deal |
C. Norwood |
Broun |
Hice • 11. Bezirk: Turner |
Brantley |
Walker |
Lankford |
McKinney |
Linder |
Gingrey |
Loudermilk • 12. Bezirk: Hughes |
Larsen |
Burns |
Barrow |
Allen • 13. Bezirk: D. Scott • 14. Bezirk: Graves
