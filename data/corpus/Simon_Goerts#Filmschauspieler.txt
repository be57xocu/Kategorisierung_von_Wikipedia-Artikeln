Simon Goerts (* 1973 in Kastl) ist ein deutscher Schauspieler.

Goerts zog nach der Scheidung der Eltern (1974) zusammen mit seiner Mutter nach München. 1979 wurde er Schüler an der dortigen Waldorfschule und machte 1989 Abitur. 1990 erhielt er eine Ausbildung als Grafikdesigner in Freiburg. 

Im selben Jahr zog er nach Hamburg und betreute behinderte Kinder in einem Sozialprojekt. 1991 arbeitete Goerts als Bühnenbauer im Schmidt Theater. 1992 begann er eine Frisörausbildung, die er vorzeitig abbrach. 1993 wohnte er auf einem Bauwagen-Platz in Hamburg und reiste dann für zwei Jahre nach Spanien.

1996 bis 1997 arbeitete er als Vorlagenzeichner bei einem Trickfilmstudio in München, nebenher als Statist in Fernsehproduktionen und als Model für Mode-Shootings. 1998 übernahm er nach einem Casting die Rolle des wuchtigen, rastlosen Koma im Spielfilm Oi!Warning. 1999 betätigte er sich als Fotomodel in Hamburg. Nach Schauspiel- und Sprachunterricht war er unter anderem Nebendarsteller in einer Tatort-Episode.
