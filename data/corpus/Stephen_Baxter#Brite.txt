Stephen Baxter (* 13. November 1957 in Liverpool) ist ein englischer Science-Fiction-Autor, der seit 1995 diese Tätigkeit hauptberuflich ausübt.

Baxter studierte Mathematik in Cambridge und wurde als Ingenieur an der Southampton-University promoviert. Einige Jahre lang lehrte er Mathematik, Physik und Informatik.

Sein Stil als Hard-SF-Schreiber gilt als einzigartig. Er hat viele Preise gewonnen: Den Philip K. Dick Award, den John W. Campbell Memorial Award, den British Science Fiction Association Award, den deutschen Kurd-Laßwitz-Preis und den Seiun Award (Japan). Den Sidewise Award für Geschichten, die sich um alternative Zeitlinien drehen, erhielt er zum einen für seinen Roman Voyage (dt. Mission Ares) und zum anderen für seine Erzählung Brigantia's Angels, für die Erzählung Huddle bekam er den Locus Award. Stephen Baxter stellt in jedem seiner Romane eine Frage und leitet dann Antworten aus dem aktuellen Kenntnisstand der physikalischen Gesetze und Theorien her.

Sein »Xeelee«-Zyklus spielt in einer fernen Zukunft. Die Menschen sind dabei, sich neben den gottähnlichen Xeelee (gespr. Sielie) zur zweitmächtigsten Spezies im Universum zu entwickeln. Die Romane und Kurzgeschichten beschäftigen sich mit Aspekten der modernen Physik, mit Singularitäten und den Konflikten zwischen baryonischem und supersymmetrischem Leben. Speziell in Flux findet man eine literarische Beschreibung für Leben auf einem Neutronenstern.

Sein Zyklus über die »Kinder des Schicksals« fügt sich in den »Xeelee«-Zyklus ein. In diesen Romanen wird beschrieben, wie sich durch diverse Umstände Parallelentwicklungen in der menschlichen Gesellschaft ergeben und dabei auch Unterarten der Gattung Mensch entstehen können.

In dieser Trilogie sind die Mammuts nicht ausgestorben, eine kleine Gruppe von ihnen hat auf einer einsamen Insel im Nordpolarmeer überlebt. Die Bücher beschreiben, wie es dazu kam, und wie sie sich in unserer modernen Welt zurechtfinden.

Die Romane der Multiversum-Trilogie liefern verschiedene mögliche Erklärungen zum Fermi-Paradoxon und auch einen Einblick in die Viele-Welten-Interpretation der Quantenmechanik.

Baxter hat die Bände 1 und 7 zu der zwölfteiligen Jugendbuch-Serie The Web beigetragen, spricht auf seiner Homepage selbst aber vom Web Book 1 + 2.

Saga einer alternativen Urgeschichte
