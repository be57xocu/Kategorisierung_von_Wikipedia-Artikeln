Sir Nevill Francis Mott (* 30. September 1905 in Leeds; † 8. August 1996 in Milton Keynes) war ein englischer Physiker.

Sein Vater Charles Francis Mott und seine Mutter Lillian Mary Mott, geborene Reynolds, waren beide „research students“ am Cavendish-Laboratorium in Cambridge unter Joseph John Thomson. Sein Vater wurde später „Director of Education of Liverpool“. Nevill Francis Mott ging am Clifton College in Bristol zur Schule und studierte Mathematik und Theoretische Physik am St John’s College in Cambridge.

Nach dreijähriger Forschung in Angewandter Mathematik erhielt Mott 1929 eine „lectureship“ an der Universität Manchester. 1930 kehrte er nach Cambridge zurück und wurde Mitglied (Fellow) und „lecturer“ am dortigen Gonville and Caius College. 1933 ging er als Professor für Theoretische Physik an die Universität Bristol. Nach der deutschen Annexion Tschechiens im März 1939 bürgten er und seine Frau Ruth für die in Prag von der Verfolgung bedrohten minderjährigen Lilly und Ilse Spielmann, Töchter des Pianisten Leopold Spielmann und nahmen diese bis Kriegsende bei sich auf. 1948 wurde Mott Henry-Overton-Wills-Professor für Physik und Leiter des Henry Herbert Wills Physical Laboratory in Bristol. 1954 folgte er der Berufung als Cavendish Professor of Physics an die Universität Cambridge; diese Stellung hielt er bis 1971. Von 1959 bis 1966 war er zusätzlich Master des Gonville and Caius College.

Er war seit 1930 mit Ruth Horder verheiratet und hatte zwei Töchter.

Mott wandte neue Ansätze der Wellenmechanik auf Kollisionen atomarer Teilchen an. Die Mott-Streuung und – darauf basierend – der Mott-Detektor sind hier nach ihm benannt. Sein Hauptarbeitsgebiet war die Festkörperphysik, die Theorie der Metalle und Legierungen sowie Halbleiter und Isolatoren. Hier sind Mott-Isolator und Mott-Übergang nach ihm benannt. Von ihm und R. W. Gurney stammt auch eine grundlegende Arbeit über den fotografischen Prozess.[1] Während des Zweiten Weltkrieges forschte er an der Ausbreitung von Radiowellen und der explosiven Zertrümmerung von Bombenhüllen. In seinen letzten Forschungsjahren widmete er sich der Hochtemperatursupraleitung.

Mit Ronald Gurney entwickelte er eine Theorie des photographischen Prozesses.

Mott erhielt 1977 zusammen mit Philip Warren Anderson und John H. van Vleck den Nobelpreis für Physik „für die grundlegenden theoretischen Leistungen zur Elektronenstruktur in magnetischen und ungeordneten Systemen“.

1936 wurde er als Mitglied („Fellow“) in die Royal Society of London gewählt, die ihm 1941 die Hughes-Medaille, 1953 die Royal Medal und 1972 die Copley-Medaille verlieh. 1954 wurde er in die American Academy of Arts and Sciences gewählt. 1961 wurde er zum korrespondierenden Mitglied der Göttinger Akademie der Wissenschaften gewählt.[2] Des Weiteren wurde er im Jahr 1964 zum Mitglied der Deutschen Akademie der Naturforscher Leopoldina - Nationale Akademie der Wissenschaften gewählt.

Ihm zu Ehren wurde 1997 die Mott-Medaille gestiftet.

1901: Röntgen |
1902: Lorentz, Zeeman |
1903: Becquerel, M. Curie, P. Curie |
1904: Rayleigh |
1905: Lenard |
1906: J. J. Thomson |
1907: Michelson |
1908: Lippmann |
1909: Braun, Marconi |
1910: van der Waals |
1911: Wien |
1912: Dalén |
1913: Kamerlingh Onnes |
1914: Laue |
1915: W. H. Bragg, W. L. Bragg |
1916: nicht verliehen |
1917: Barkla |
1918: Planck |
1919: Stark |
1920: Guillaume |
1921: Einstein |
1922: N. Bohr |
1923: Millikan |
1924: M. Siegbahn |
1925: Franck, G. Hertz |
1926: Perrin |
1927: Compton, C. T. R. Wilson |
1928: O. W. Richardson |
1929: de Broglie |
1930: Raman |
1931: nicht verliehen |
1932: Heisenberg |
1933: Schrödinger, Dirac |
1934: nicht verliehen |
1935: Chadwick |
1936: Hess, C. D. Anderson |
1937: Davisson, G. P. Thomson |
1938: Fermi |
1939: Lawrence |
1940–1942: nicht verliehen |
1943: Stern |
1944: Rabi |
1945: Pauli |
1946: Bridgman |
1947: Appleton |
1948: Blackett |
1949: Yukawa |
1950: Powell |
1951: Cockcroft, Walton |
1952: Bloch, Purcell |
1953: Zernike |
1954: Born, Bothe |
1955: Lamb, Kusch |
1956: Shockley, Bardeen, Brattain |
1957: Yang, T.-D. Lee |
1958: Tscherenkow, Frank, Tamm |
1959: Segrè, Chamberlain |
1960: Glaser |
1961: Hofstadter, Mößbauer |
1962: Landau |
1963: Wigner, Goeppert-Mayer, Jensen |
1964: Townes, Bassow, Prochorow |
1965: Feynman, Schwinger, Tomonaga |
1966: Kastler |
1967: Bethe |
1968: Alvarez |
1969: Gell-Mann |
1970: Alfvén, Néel |
1971: Gábor |
1972: Bardeen, Cooper, Schrieffer |
1973: Esaki, Giaever, Josephson |
1974: Ryle, Hewish |
1975: A. N. Bohr, Mottelson, Rainwater |
1976: Richter, Ting |
1977: P. W. Anderson, Mott, Van Vleck |
1978: Kapiza, Penzias, R. W. Wilson |
1979: Glashow, Salam, Weinberg |
1980: Cronin, Fitch |
1981: Bloembergen, Schawlow, K. Siegbahn |
1982: K. Wilson |
1983: Chandrasekhar, Fowler |
1984: Rubbia, van der Meer |
1985: von Klitzing |
1986: Ruska, Binnig, Rohrer |
1987: Bednorz, Müller |
1988: Lederman, Schwartz, Steinberger |
1989: Paul, Dehmelt, Ramsey |
1990: Friedman, Kendall, R. E. Taylor |
1991: de Gennes |
1992: Charpak |
1993: Hulse, J. H. Taylor Jr. |
1994: Brockhouse, Shull |
1995: Perl, Reines |
1996: D. M. Lee, Osheroff, R. C. Richardson |
1997: Chu, Cohen-Tannoudji, Phillips |
1998: Laughlin, Störmer, Tsui |
1999: ’t Hooft, Veltman |
2000: Alfjorow, Kroemer, Kilby |
2001: Cornell, Ketterle, Wieman |
2002: Davis Jr., Koshiba, Giacconi |
2003: Abrikossow, Ginsburg, Leggett |
2004: Gross, Politzer, Wilczek |
2005: Glauber, Hall, Hänsch |
2006: Mather, Smoot |
2007: Fert, Grünberg |
2008: Nambu, Kobayashi, Maskawa |
2009: Kao, Boyle, Smith |
2010: Geim, Novoselov |
2011: Perlmutter, Schmidt, Riess |
2012: Haroche, Wineland |
2013: Englert, Higgs |
2014: Akasaki, Amano, Nakamura |
2015: Kajita, McDonald |
2016: Thouless, Haldane, Kosterlitz |
2017: Barish, Thorne, Weiss
