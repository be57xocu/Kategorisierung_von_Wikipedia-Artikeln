Leo James Rainwater (* 9. Dezember 1917 in Council, Idaho; † 30. Mai 1986 in New York) war ein US-amerikanischer Physiker. Die wichtigsten seiner wissenschaftlichen Arbeiten entwickeln Theorien über das Verhalten und die Struktur von Atomkernen.

Leo James Rainwater wurde 1917 in einer Kleinstadt in Idaho geboren, wo seine Eltern einen kleinen Gemischtwarenladen führten. Bereits ein Jahr später starb sein Vater während einer Grippe-Epidemie. Zusammen mit seiner Mutter und Großmutter zog er anschließend nach Hanford in Kalifornien. Schon in der Schule zeigten sich seine Stärken in den naturwissenschaftlichen Fächern Physik, Chemie und Mathematik. 

Rainwater begann sein Physikstudium am California Institute of Technology, welches er 1939 als Bachelor abschloss. Nach seinem Wechsel an die Columbia University in New York, wo er in den Anfangsjahren u.a. bei den Physikern Isidor Isaac Rabi und Enrico Fermi lernte, war Rainwater wie viele amerikanische Physiker dieser Zeit am Manhattan-Projekt zur Entwicklung der Atombombe beteiligt. 

1946 promovierte er an der Columbia University, wo er auch die weiteren Stationen seiner wissenschaftlichen Karriere durchlief: die Stelle eines Privatdozenten 1947, eine außerordentliche Professur 1949 und die Ernennung zum ordentlichen Professor 1952.[1]
Ab 1949 entwickelte Rainwater eine Theorie, dass Atomkerne auf Grund kollektiver Nukleonenanregungen von der zuvor angenommenen Kugelgestalt abweichen können. Ausgangspunkt waren die experimentellen Ergebnisse bezüglich gemessener Quadrupolmomente von Atomkernen, die Charles H. Townes während eines Kolloquiums vorstellte. Äußerst fruchtbar war dabei die Zusammenarbeit mit dem dänischen Physiker Aage Niels Bohr, der ein Auslandsaufenthalt an der Columbia University verbrachte.

Leo James Rainwater war seit 1942 mit Emma Louis Smith verheiratet. Sie hatten drei Söhne und eine Tochter, die jedoch bereits jung verstarb.

1963 wurde Rainwater von der Atomenergie-Kommission der Ernest Orlando Lawrence Preis für Physik zugesprochen. 1975 erhielt er zusammen mit Aage Niels Bohr und Ben Mottelson den Physik-Nobelpreis „für die Entdeckung der Verbindung zwischen kollektiver und Teilchen-Bewegung in Atomkernen und die Entwicklung der Theorie von der Struktur der Atomkerne basierend auf dieser Verbindung“.

1968 wurde er zum Mitglied der National Academy of Sciences gewählt.[2]
1901: Röntgen |
1902: Lorentz, Zeeman |
1903: Becquerel, M. Curie, P. Curie |
1904: Rayleigh |
1905: Lenard |
1906: J. J. Thomson |
1907: Michelson |
1908: Lippmann |
1909: Braun, Marconi |
1910: van der Waals |
1911: Wien |
1912: Dalén |
1913: Kamerlingh Onnes |
1914: Laue |
1915: W. H. Bragg, W. L. Bragg |
1916: nicht verliehen |
1917: Barkla |
1918: Planck |
1919: Stark |
1920: Guillaume |
1921: Einstein |
1922: N. Bohr |
1923: Millikan |
1924: M. Siegbahn |
1925: Franck, G. Hertz |
1926: Perrin |
1927: Compton, C. T. R. Wilson |
1928: O. W. Richardson |
1929: de Broglie |
1930: Raman |
1931: nicht verliehen |
1932: Heisenberg |
1933: Schrödinger, Dirac |
1934: nicht verliehen |
1935: Chadwick |
1936: Hess, C. D. Anderson |
1937: Davisson, G. P. Thomson |
1938: Fermi |
1939: Lawrence |
1940–1942: nicht verliehen |
1943: Stern |
1944: Rabi |
1945: Pauli |
1946: Bridgman |
1947: Appleton |
1948: Blackett |
1949: Yukawa |
1950: Powell |
1951: Cockcroft, Walton |
1952: Bloch, Purcell |
1953: Zernike |
1954: Born, Bothe |
1955: Lamb, Kusch |
1956: Shockley, Bardeen, Brattain |
1957: Yang, T.-D. Lee |
1958: Tscherenkow, Frank, Tamm |
1959: Segrè, Chamberlain |
1960: Glaser |
1961: Hofstadter, Mößbauer |
1962: Landau |
1963: Wigner, Goeppert-Mayer, Jensen |
1964: Townes, Bassow, Prochorow |
1965: Feynman, Schwinger, Tomonaga |
1966: Kastler |
1967: Bethe |
1968: Alvarez |
1969: Gell-Mann |
1970: Alfvén, Néel |
1971: Gábor |
1972: Bardeen, Cooper, Schrieffer |
1973: Esaki, Giaever, Josephson |
1974: Ryle, Hewish |
1975: A. N. Bohr, Mottelson, Rainwater |
1976: Richter, Ting |
1977: P. W. Anderson, Mott, Van Vleck |
1978: Kapiza, Penzias, R. W. Wilson |
1979: Glashow, Salam, Weinberg |
1980: Cronin, Fitch |
1981: Bloembergen, Schawlow, K. Siegbahn |
1982: K. Wilson |
1983: Chandrasekhar, Fowler |
1984: Rubbia, van der Meer |
1985: von Klitzing |
1986: Ruska, Binnig, Rohrer |
1987: Bednorz, Müller |
1988: Lederman, Schwartz, Steinberger |
1989: Paul, Dehmelt, Ramsey |
1990: Friedman, Kendall, R. E. Taylor |
1991: de Gennes |
1992: Charpak |
1993: Hulse, J. H. Taylor Jr. |
1994: Brockhouse, Shull |
1995: Perl, Reines |
1996: D. M. Lee, Osheroff, R. C. Richardson |
1997: Chu, Cohen-Tannoudji, Phillips |
1998: Laughlin, Störmer, Tsui |
1999: ’t Hooft, Veltman |
2000: Alfjorow, Kroemer, Kilby |
2001: Cornell, Ketterle, Wieman |
2002: Davis Jr., Koshiba, Giacconi |
2003: Abrikossow, Ginsburg, Leggett |
2004: Gross, Politzer, Wilczek |
2005: Glauber, Hall, Hänsch |
2006: Mather, Smoot |
2007: Fert, Grünberg |
2008: Nambu, Kobayashi, Maskawa |
2009: Kao, Boyle, Smith |
2010: Geim, Novoselov |
2011: Perlmutter, Schmidt, Riess |
2012: Haroche, Wineland |
2013: Englert, Higgs |
2014: Akasaki, Amano, Nakamura |
2015: Kajita, McDonald |
2016: Thouless, Haldane, Kosterlitz |
2017: Barish, Thorne, Weiss
