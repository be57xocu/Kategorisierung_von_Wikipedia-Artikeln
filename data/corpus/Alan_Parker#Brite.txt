Sir Alan William Parker (* 14. Februar 1944 in London Borough of Islington, London, Großbritannien) ist ein britischer Drehbuchautor, Autor und Filmregisseur.

Parker war zunächst erfolgreicher Werbefilmer. In den 1970er Jahren gründete er zusammen mit Alan Marshall eine Produktionsfirma für Werbefilme.[1] Marshall war auch bis einschließlich Angel Heart aus dem Jahr 1987 an all seinen Filmprojekten als Produzent beteiligt. Der Filmeditor Gerry Hambling (1926–2013) war für den Schnitt sämtlicher Filmprojekte verantwortlich. Sehr häufig kooperierte er auch mit dem Kameramann Michael Seresin. 

Parker wurde mehrfach für den Oscar nominiert (u. a. für 12 Uhr nachts – Midnight Express) und gewann viermal den British Academy Film Award (u. a. für Bugsy Malone) sowie den „Großen Preis der Jury“ bei den Filmfestspielen von Cannes 1985 (mit Birdy). 1981 führte er Regie bei Pink Floyd – The Wall, der Spielfilmadaption des erfolgreichen Rockalbums The Wall der britischen Rockgruppe Pink Floyd.

Ein Höhepunkt seines Schaffens war Angel Heart (1987) mit Mickey Rourke, Robert De Niro und Lisa Bonet in den Hauptrollen.

Einen Oscar (beste Kameraführung) erhielt der Film Mississippi Burning – Die Wurzel des Hasses (1988), mit Gene Hackman und Willem Dafoe in den Hauptrollen. Das engagierte Werk um Bürgerrechte in den Vereinigten Staaten nach Ende der Rassentrennung beruht auf einer wahren Geschichte.

Mit Die Commitments (1991), einem Film über eine junge Soulband der irischen Arbeiterklasse, zeigte Parker erneut sein Können.

Weitere Werke Parkers: die Komödie Willkommen in Wellville (1994) mit Sir Anthony Hopkins, die Verfilmung des Lloyd-Webber-Musicals Evita mit Madonna (1996), das Drama Die Asche meiner Mutter (1999) und der Thriller Das Leben des David Gale (2002).

Parker ist Gründungsmitglied der Director’s Guild of Great Britain. 1985 ehrte die Britische Akademie Parker für seine herausragenden Verdienste um das britische Kino mit dem Michael Balcon Award. Am Neujahrstag 2002 wurde Parker von der britischen Königin zum Ritter geschlagen. 2013 wurde ihm in Anerkennung seines Lebenswerkes der Ehrenpreis der British Academy of Film and Television Arts, die Academy Fellowship, zugesprochen. Parker ist außerdem seit 2005 Ehrendoktor für Kunst der Universität Sunderland.

Außerdem produzierte er zahlreiche Werbefilme.

Alan Parker erhielt unter anderem den CineMerit Award 2004 des Filmfest München für herausragende Verdienste um die Filmkunst.
