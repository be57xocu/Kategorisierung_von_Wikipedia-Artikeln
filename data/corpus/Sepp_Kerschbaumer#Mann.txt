Sepp Kerschbaumer (* 9. November 1913 in Frangart, Südtirol; † 7. Dezember 1964 in Verona) war ein Südtirolaktivist und Leiter des  Befreiungsausschusses Südtirol (BAS).

Sepp Kerschbaumer wurde als Sohn des Kaufmanns Josef (vom Ritten) und Luise geb. Zelger (aus Aldein) in Frangart bei Bozen geboren. Als Kerschbaumer vier Jahre alt war, fiel sein Vater an der Dolomitenfront; als er neun Jahre alt war, starb seine Mutter.

Nach seiner Ausbildung im Rainerum Bozen und Kloster Neustift absolvierte er 1927 die kaufmännische Vorbereitungsschule in Brixen.

1933 wurde er zum italienischen Militär eingezogen. 

Im Herbst 1934 wurde Kerschbaumer zu zwei Jahren Verbannung bei Potenza wegen Teilnahme an einer verbotenen politischen Veranstaltung verurteilt. Nach Begnadigung durch Benito Mussolini im Herbst 1935 kehrte er nach Südtirol zurück.

Danach wurde Kerschbaumer das elterliche Gemischtwarengeschäft von seinem Vormund übergeben.

Kerschbaumer war ein tiefgläubiger Katholik und besuchte täglich die Heilige Messe in Bozen; im Gefängnis betete er häufig den Rosenkranz.

Am 29. April 1936 heiratete er Maria Spitaler aus Frangart. Das Paar hatte sechs Kinder:

Als Optant entschied Kerschbaumer für sich und seine Familie 1939, in das Deutsche Reich auszuwandern, und warb auch für diese Entscheidung, da er sich wie viele seiner Landsleute wieder gemäß seiner Volkszugehörigkeit kulturell und sprachlich entfalten wollte. Doch im Laufe der Zeit erkannte er, dass aus Deutschland keine Hilfe zu erwarten war. Er wurde zum entschiedenen Gegner des Nationalsozialismus. Er wurde während der deutschen Besatzungszeit 1944 zur Wehrmacht in Bozen eingezogen. 

Als nach dem Krieg die Südtiroler Volkspartei gegründet wurde, trat er ihr bald bei, wurde Ortsobmann, Fraktionsvorsteher und Gemeinderat von Frangart und widmete sich der Lokalpolitik.

Anfang der 1950er Jahre kam jedoch Kritik an der zu konzilianten Haltung der SVP auf. Kerschbaumer war einer dieser Kritiker und begann, Gleichgesinnte um sich zu sammeln. Die so entstehende Organisation wurde Befreiungsausschuss Südtirol kurz BAS genannt. Im September 1956 erfolgten erste Anschläge des BAS. Eine zweite Serie von Anschlägen wurden im Jänner 1957 durchgeführt.

Im selben Jahr wurden die moderaten Stimmen in der Parteiführung der SVP durch radikalere verdrängt. Als der neue Obmann Silvius Magnago am 17. November 1957 bei der sogenannten Großkundgebung von Schloss Sigmundskron das Motto: Los von Trient prägte, war Kerschbaumer dabei und verteilte ein nicht unterzeichnetes Flugblatt, in dem er ein freies Südtirol forderte und dies folgendermaßen begründete: "Deutsch wollen wir bleiben und keine Sklaven eines Volkes werden, welches durch Verrat und Betrug unser Land kampflos besetzt hat und seit 40 Jahren ein Ausbeutungs- und Kolonisationssystem betreibt, welches schlimmer ist als die einstigen Kolonialmethoden in Zentralafrika."[1]
Nach der Attentatswelle der Feuernacht wurden Kerschbaumer sowie 150 weitere BAS-Mitglieder verhaftet und in der Haft von den Ordnungskräften gefoltert. Die schweren Misshandlungen Kerschbaumers und seiner Mitgefangenen, an deren Folgen Anton Gostner und Franz Höfler verstarben, trugen zu einer weiteren Eskalation bei.

Am 16. Juli 1964 wurde Sepp Kerschbaumer als Führer des BAS zu 15 Jahren und 11 Monaten Gefängnis verurteilt. Er entging, wie alle Mitangeklagten, einer drohenden Verurteilung zu zweimal lebenslang, da der Präsident des Schwurgerichts, Gustavo Simonetti, auf Druck der Regierung Aldo Moro die Anklagepunkte „Anschlag auf die Einheit des Staates“ und „Anschlag auf die Verfassung“ fallen ließ. Am 7. Dezember des gleichen Jahres erlitt er in der Haft einen Herzinfarkt und verstarb.

Die Beisetzung wurde zu einer Demonstration: Mehr als 15.000 Menschen kamen zur Beerdigung.

Kerschbaumer hatte mit der Gründung des BAS und dessen Aktionen der internationalen Öffentlichkeit gezeigt, dass es einer beschleunigten Lösung des Südtirolproblems bedurfte. 
Dass Kerschbaumer im 1. Mailänder Prozess nicht die für seine Vergehen mögliche Höchststrafen erhielt, verdankte er sowohl der Intervention der frisch vereidigten Mitte-links-Koalition unter Aldo Moro beim Gericht, als auch einem vollständigen Geständnis seiner Taten und der Akzeptanz der Taktik der Verteidigung mit der Aussage, das Ziel sei die Autonomie und nicht die Selbstbestimmung gewesen (was nicht seinen wahren Absichten entsprach).

Ob die Attentate mehr geschadet oder genutzt haben, war unter Historikern lange umstritten. Heute geht man davon aus, dass die Attentate vor allem der italienischen Seite genutzt haben, da durch sie die Möglichkeit einer Südtiroler Selbstbestimmung, welche die Attentate ja herbeiführen sollten, diskreditiert wurde. Denn kein Südtiroler Politiker wollte oder konnte sich öffentlich mit den Taten der Attentäter und somit auch ihren Zielen einverstanden erklären. 
Trotz einer gespannten Situation in Südtirol, inklusive massiver Militärpräsenz, gelang es den politischen Vertretern des Staates und der Südtiroler, an einem friedlichen Dialog zur Lösung des Problems festzuhalten. Mit der Neunzehner-Kommission erhielt die SVP eine konkrete Möglichkeit für Verhandlungen, die schließlich zum Südtirol-Paket führten und Grundlage der heutigen Autonomie Südtirols sind.
