Milan Uhde (* 28. Juli 1936 in Brünn) ist ein tschechischer Schriftsteller und Politiker.

Milan Uhde wuchs in Brünn auf und studierte dort Russisch und Tschechisch. Bis zu ihrer Auflösung 1970 war er Redakteur der Kulturrevue Host do domu, danach arbeitete er als freier Schriftsteller. 1972 bekam er Publikationsverbot. 1977 unterzeichnete er die Charta 77.

Nach der Samtenen Revolution wurde er 1990 Kulturminister der Tschechoslowakei, 1992 Parlamentspräsident im tschechischen Parlament und 1996 Fraktionsvorsitzender der ODS. Nach der Spaltung der ODS wechselte er 1997 zur Unie svobody – Demokratická unie (Freiheitsunion). Seit 1998 ist er wieder parteilos und freier Schriftsteller.

Milan Uhde schrieb Gedichte, Erzählungen, Drehbücher, Theaterstücke, Hörspiele und Essays.

Auf Deutsch erschienen die beiden Dramen König Vavra und Nonstop-Nonsense (1965). In: Das Gartenfest. (Anthologie des tschechischen Theaters von ca. 1948–1989.)

Milan Uhde |
Miloš Zeman |
Václav Klaus |
Lubomír Zaorálek |
Miloslav Vlček |
Miroslava Němcová |
Jan Hamáček |
Radek Vondráček

