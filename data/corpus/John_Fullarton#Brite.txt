John Fullarton (* um 1780; † 24. Oktober 1849) war ein englischer Wirtschaftswissenschaftler.

Fullarton vertrat gemeinsam mit Thomas Tooke die These, der Geldbedarf einer Volkswirtschaft werde sich ganz von alleine regeln. Gegen diese These konnte sich jedoch später die gegenteilige These Robert Torrens' und David Ricardos durchsetzen, wonach die Basisgeldmenge begrenzt werden müsse.
