Ernst Meister (* 3. September 1911 in Haspe; † 15. Juni 1979 in Hagen) war ein deutscher Dichter und Schriftsteller.

Ernst Meister besuchte die städtische Oberrealschule in Haspe und legte sein Abitur im Jahr 1930 an dieser Schule ab, die zwischenzeitlich als Reformrealgymnasium geführt wurde. Diese Schule, später in Ernst-Meister-Gymnasium umbenannt, ist heute die Gesamtschule Haspe.

In Marburg begann Meister 1930 auf Wunsch seines Vaters ein Studium der Theologie, wandte sich aber dann der Germanistik, Philosophie und Kunstgeschichte zu und studierte seit Ende 1931 in Berlin. Hier veröffentlichte er 1932 seinen ersten Gedichtband Ausstellung.

Nach der Machtergreifung der Nationalsozialisten ging sein Lehrer Karl Löwith ins Exil, so dass Meister die Arbeit an seiner Dissertation nicht beenden konnte. Auch als Autor schrieb Meister von nun an, von drei kleineren Veröffentlichungen in der Frankfurter Zeitung abgesehen, für die Schublade.

Meister nahm als Soldat am Zweiten Weltkrieg teil. Die vielfältigen, vor allem kriegsbedingten Erfahrungen verarbeitete er in lyrischen Beiträgen ebenso wie in Erzählungen, Hörspielen und Theaterstücken. Doch erst Anfang der 1950er Jahre begann Meister wieder, seine Texte zu veröffentlichen, zunächst bei Victor Otto Stomps in der Eremiten-Presse in Stierstadt. Dort erschienen die Bände Unterm schwarzen Schafspelz (1953), Dem Spiegelkabinett gegenüber (1954), Der Südwind sagte zu mir (1955) und Fermate (1957). Es folgten Zahlen und Figuren 1958, Die Formel und die Stätte 1960, Flut und Stein 1962 und Zeichen um Zeichen 1968. Dazwischen erschienen unter anderem die Erzählung Der Bluthänfling 1959 und das 1966 uraufgeführte Drama Ein Haus für meine Kinder. Das Spätwerk, das zugleich auch der Gipfelpunkt des Meisterschen Werkes ist, umfasst die Gedichtbände Es kam die Nachricht (1970), Sage vom Ganzen den Satz (1972), Im Zeitspalt (1976) und Wandloser Raum (1979). Die Zeitschrift Westfalenspiegel druckte zahlreiche seiner Texte ab und würdigte auch Meisters abstraktes bildnerisches Schaffen.[1] 1956 war Meister am „Schmallenberger Dichterstreit“ beteiligt.

Für den Lyriker und Romanautor Nicolas Born spielte der zurückgezogen in Hagen lebende Dichter als Mentor ab Ende der fünfziger Jahre eine entscheidende Rolle. Born sorgte als Mitglied der Jury des Petrarca-Preises Mitte der siebziger Jahre dafür, dass Meisters als hermetisch geltende, fast vergessene Lyrik in ihrer essenziellen Einfachheit und zeitlosen Qualität wiederentdeckt wurde. Andere jüngere Autoren, die Meisters Bedeutung in dieser Zeit erkannten und sich nachhaltig mit seinem Werk auseinandersetzten, waren Hans Bender, Peter Handke, Michael Krüger, Christoph Meckel, Oskar Pastior und Paul Wühr.

Hubertus Heiser schrieb über ihn in der Westfalenpost:

„Seine Dichtung war quasi eine intellektuelle Poesie in Form einer meditativen und gedanklich tief schürfenden Lyrik, die nach den Grundformen und Grundbedingungen menschlicher Existenz in der, wie Meister es sah, 'kosmischen Preisgegebenheit' fragt.
Gleich funkelnden Kristallen blieben vor allem seine Gedichte die auch heute noch wahrgenommenen Schätze, über die der Schöpfer vor Vers-Beginn einmal sagte: 'Ein Gedicht ist ein Ereignis, das durch sich selbst in der Direktheit seiner Existenz wirken muss'.“

Werkausgaben

Lyrik:

In der Reihe Sämtliche Gedichte, hg. und jeweils mit einem Nachwort von Reinhard Kiefer, sind erschienen:

Prosa:

Lesebuch:

Hörspiele:

Medien:
