Jean Joseph Paul Augustin Dessoles oder Jean-Joseph Dessolle (* 3. Oktober 1767 in Auch; † 3. November 1828 im Château de Monthuchet, Saulx-les-Chartreux, Département Essonne) war Marquis und französischer General.

Dessoles entstammte einem alten Adelsgeschlecht. Bereits mit 25 Jahren diente Dessolles 1792 als Kapitän in der Westpyrenäenarmee und kam anschließend als Adjutant von General Jean-Louis-Ebenezer Reynier in den Generalstab. Im Oktober 1793 wurde Dessolles zum Generaladjutanten ernannt und nahm als solcher drei Jahre später an Napoleons italienischen Feldzug teil. Dafür wurde er von Napoleon persönlich ausgezeichnet. 1797 zum Brigadegeneral befördert, befehligte er 1798 ein Reservekorps in Italien und rückte am 7. Dezember mit seiner Division in Turin ein.

Im März 1799 überquerte Dessolles mit 4500 Soldaten das Stilfser Joch, um General Claude-Jacques Lecourbe zu unterstützen. Dieser war nach der Schlacht von Finstermünz im Inntal von österreichischen Truppen bei Glurns und Taufers in Schwierigkeiten geraten und Dessolles konnte am 13. April 1798 das „Treffen von Santa Maria“ für Frankreich entscheiden.

Für diesen Sieg wurde er vom Direktorium zum Divisionsgeneral ernannt. Als Chef des Generalstabs des Oberkommandos der italienischen Armee bewies er bei Novi (16. Juli 1799) die glänzendste Tapferkeit, befehligte gegen Ende des Feldzugs die Truppen in Genua, wurde auf Wunsch von General Jean-Victor Moreau zum Chef des Generalstabs der Rheinarmee ernannt. Dessolles konnte sich in der Schlacht bei Hohenlinden, in der Schlacht bei Linz und beim Überqueren von Inn und Salza auszeichnen.

Als der Friede von Lunéville im Dezember 1801 unterschrieben und in Kraft war, wollte ihn der Erste Konsul zum Staatsrat, Kriegssekretär und Mitglied des Verwaltungsrats ernennen. Dessoles lehnte jedoch aus Anhänglichkeit an Moreau ab. 1803, als es darauf ankam, Moreaus Freunde zu entfernen, wurde er nach Hannover geschickt, um unter dem Befehl von General Édouard Mortier eine Division zu übernehmen.

Als alle Zivil- und Militärbehörden bei Eröffnung des Prozesses gegen Moreau dem Ersten Konsul Glückwunschadressen zusandten, gehörte Dessoles zu den wenigen, die dies unterließen. Bonaparte verzieh ihm dies nie ganz, obwohl er ihn, um ihn an sein Interesse zu fesseln, 1804 zum Großoffizier der Ehrenlegion und 1805 sogar zum Gouverneur des Palastes von Versailles ernannte.

1808 erhielt Dessolles das Kommando über ein Armeekorps in Spanien. Dort zeichnete er sich besonders im August 1809 in der Schlacht bei Toledo, am 18. November bei Ocana und beim Übergang über die Sierra Morena aus. Am 18. Januar 1810 hielt er seinen Einzug in Córdoba, wurde zum Gouverneur von Córdoba, Jaén und Sevilla ernannt und erwarb sich durch Milde das Zutrauen der Spanier.

Mit Napoleons System nicht übereinstimmend, zog er sich abermals aus dem Dienst und aufs Land zurück. 1812 zum Chef des Generalstabs bei der Armee des Vizekönigs von Neapel ernannt, nahm er nach der Eroberung von Smolensk seine Entlassung und lebte bis zur Restauration zurückgezogen auf einem Landsitz bei Paris. Nach der Rückkehr der Bourbonen wurde er von Ludwig XVIII. zum Staatsminister und Pair von Frankreich ernannt und ihm zugleich das Kommando über sämtliche Nationalgarden übertragen. Da er sich aber als entschiedenen Anhänger konstitutioneller Ideen zeigte, legte er das Kommando nieder.

Am 29. Dezember 1818 übernahm er die Verwaltung der auswärtigen Angelegenheiten sowie den Vorsitz in dem von Decazes gebildeten liberal-konstitutionellen Ministerium und wurde vom König zum Marquis erhoben, trat jedoch schon 1819 aus dem Ministerium, da er Decazes' Nachgiebigkeit gegen die Reaktion nicht billigte, aus und wurde unter dem Ministerium Jean-Baptiste de Villèle 1822 auch aus der Liste der Staatsminister gestrichen.

Jean Joseph Paul Dessolles starb am 3. November 1828 in Montluchet und ruht auf dem Friedhof Père-Lachaise in Paris (28. Division).

Sein Name ist am Triumphbogen in Paris in der 15. Spalte eingetragen.
