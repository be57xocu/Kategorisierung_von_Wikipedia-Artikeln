Glenn Close (* 19. März 1947 in Greenwich, Connecticut) ist eine US-amerikanische Schauspielerin und Sängerin.

Ihr Vater William T. Close, ein Arzt und Autor, übernahm im Jahr 1960 die Leitung eines Krankenhauses in der Demokratischen Republik Kongo.[1] Glenn Close verbrachte ihre Jugend demzufolge abwechselnd in dem zentralafrikanischen Land und in Internaten wie der Rosemary Hall (Greenwich) in Neuengland.
Sie besuchte das College of William & Mary in Virginia und machte ihren Abschluss in Schauspiel und Anthropologie.

Glenn Close begann ihre Schauspielkarriere im letzten Jahr ihrer College-Ausbildung am Broadway-Theater. 1980 wurde sie erstmals für den Tony Award nominiert, 1982 erhielt sie die erste von insgesamt drei Auszeichnungen.

Im selben Jahr wurde sie für ihre erste Kinorolle in Garp und wie er die Welt sah für den Oscar als Beste Nebendarstellerin nominiert. In den folgenden sechs Jahren folgten vier weitere Nominierungen, darunter unter anderem als Beste Hauptdarstellerin in Eine verhängnisvolle Affäre und Gefährliche Liebschaften – gewinnen konnte sie die Auszeichnung jedoch bislang nicht. Eine weitere Oscar-Nominierung folgte 2012 für die Titelrolle in dem Historiendrama Albert Nobbs (2011).

Seit den 1990ern nahm sie zunehmend auch Fernsehrollen an.
Für die Hauptrolle in Der Löwe im Winter erhielt sie 2005 eine Emmy-Nominierung und gewann den Golden Globe Award. 2005 stieß sie zur Stammbesetzung der Krimiserie The Shield – Gesetz der Gewalt, 2008 wurde sie erneut für die Fernsehserie Damages – Im Netz der Macht mit einem Golden Globe ausgezeichnet. Für die Rolle der Star-Anwältin Patty Hewes erhielt sie außerdem 2008 und 2009 den Emmy als Beste Hauptdarstellerin in einer Drama-Serie.

Glenn Close wurde am 13. Zurich Film Festival (vom Sonntag, 1. Oktober 2017) in Zürich mit dem "Golden Icon Award" für ihr Lebenswerk geehrt[2]. Sie erschien zusammen mit ihrer Tochter Annie Starke und nahm die Auszeichnung persönlich entgegen.

In der US-amerikanischen Zeichentrickserie Die Simpsons sprach sie in mehreren Folgen Mona J. Simpson. Bemerkenswert ist auch ihr Cameo-Auftritt in Hook von Steven Spielberg. In dem Film spielte sie den männlichen Piraten, der in eine Kiste gesperrt wird.

Close ist seit Februar 2006 zum dritten Mal verheiratet. Ihre Tochter aus einer vorhergehenden Ehe ist die 1988 geborene Annie Starke, mit der zusammen sie 2017 den Film The Wife drehte. Glenn Close spielt die weibliche Hauptrolle der Joan Castleman, und in den Rückblenden wird die junge Castleman von Annie Starke dargestellt.[3]
Glenn Close lebt überwiegend in New York. Close ist eine Hundeliebhaberin und führt ihren eigenen Blog, in dem sie andere Prominente über die Beziehung zu ihren Hunden interviewt.

In den Filmen und Serien wird Glenn Close für die deutschsprachigen Fassungen von verschiedenen deutschen Sprecherinnen und Sprechern synchronisiert. Allerdings sind Kerstin Sanders-Dornseif und Hallgard Bruckhaus seit Beginn immer wieder zu hören. Auch Uta Hallant sowie weitere sieben Sprecherinnen verleihen der Schauspielerin ihre Stimme.

In zwei Fällen, Glenn Close synchronisierte jeweils die Großmutter in den Animationsfilmen Die Rotkäppchen-Verschwörung und Das Rotkäppchen-Ultimatum, wurde ihre Rolle von Hans Werner Olm und Michael Pan übernommen.

Golden Icon Award

Oscar

British Academy Film Award

Blockbuster Entertainment Award

CableACE Award

Drama Desk Award

Emmy

GLAAD Media Award

Golden Globe Award

Goldene Kamera

Gotham Award

Hasty Pudding

Hollywood Walk of Fame

Internationales Filmfestival von Locarno

Los Angeles Film Critics Association

National Board of Review

Obie Award

People’s Choice Award

Festival Internacional de Cine de Donostia-San Sebastián

Satellite Award

Saturn Award

Screen Actors Guild Award

ShoWest Convention

Television Critics Association Award

Tony Award

Women in Film Crystal Award
