Keri Hulme (* 9. März 1947 in Christchurch) ist eine neuseeländische Schriftstellerin.

Keri Hulme wurde als ältestes von sechs Kindern in Christchurch (Neuseeland) geboren. Ihre Vorfahren waren schottisch-englische Einwanderer bzw. mütterlicherseits Māori.

Ihre erste Schule war die North Brighton Primary, anschließend besuchte sie die Aranui High School. Als sie 11 Jahre alt war, starb ihr Vater, John W. 1967/1968 studierte sie an der Universität von Canterbury Jura, musste es aber aus finanziellen Gründen abbrechen. Sie schlug sich mit Gelegenheitsarbeiten durch, als Tabakpflückerin, Köchin und Postangestellte, aber auch mit Arbeiten am Bau und in der Fischerei. Im Alter von 25 Jahren bezog sie ein einsames Haus in Okarito an der Westküste der Südinsel Neuseelands, um dort ungestört schreiben, lesen, malen und fischen zu können.

Für ihren Roman "The Bone People", in der deutschen Ausgabe unter dem Titel Unter dem Tagmond, erhielt sie 1985 den Booker Prize[1]. Außerdem erschien 1987 ein Bändchen mit Erzählungen "The Windeater. Te Kaihau" (deutsch, 1992: "Der Windesser. Te Kaihau") 1967–1968.
