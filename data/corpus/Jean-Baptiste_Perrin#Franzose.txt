Jean-Baptiste Perrin (* 30. September 1870 in Lille; † 17. April 1942 in New York) war ein französischer Physiker und Nobelpreisträger.

Jean Perrin studierte an der École normale supérieure. Nach seinem Abschluss 1894 arbeitete er dort als wissenschaftlicher Assistent und promovierte 1897. Er wechselte anschließend an die Sorbonne in Paris und lehrte dort von 1910 bis 1940 als Professor. Nach dem Einmarsch der Deutschen flüchtete er in die USA, wo er am 17. April 1942 verstarb. Seine sterblichen Überreste wurden 1948 vom Kriegsschiff Jeanne d'Arc nach Frankreich überführt und im Panthéon beigesetzt.

Sein Sohn Francis Perrin (1901–1992) war ebenfalls Physiker.

Die ersten Arbeiten Perrins, die während seiner Promotionszeit begannen, beschäftigten sich mit Kathodenstrahlen und Röntgenstrahlen, wobei er unter anderem zeigen konnte, dass es sich bei den Kathodenstrahlen um negativ geladene Teilchen handelt. Weitere Arbeiten behandelten die Fluoreszenz, den Zerfall von Radium sowie die Schallerzeugung und -ausbreitung. Unter anderem entwickelte er die Perrin-Röhre, mit der er erstmals in einwandfreier Weise die negative Ladung der Kathodenstrahlen nachwies. Darüber hinaus lässt sich mit ihr die Größenordnung der spezifischen Elementarladung bestimmen.

Seine bekanntesten Arbeiten beschäftigen sich mit den Eigenschaften von Kolloiden, mit der Untersuchung der brownschen Bewegung der gelösten Teilchen konnte er die Berechnungen und Vorhersagen Albert Einsteins bestätigen, nach der die gelösten Teilchen den Gasgesetzen gehorchen. Durch eine genaue Analyse konnte er zudem die Avogadro-Konstante bestimmen - das Ergebnis stand im Einklang mit anderen Bestimmungen der Konstante und war ein entscheidender Beleg für die Teilchennatur der Materie.

Er wurde 1926 mit dem Nobelpreis für Physik „für seine Arbeiten über die diskontinuierliche Struktur der Materie, besonders für seine Entdeckung des Sedimentationsgleichgewichts“ ausgezeichnet.

1901: Röntgen |
1902: Lorentz, Zeeman |
1903: Becquerel, M. Curie, P. Curie |
1904: Rayleigh |
1905: Lenard |
1906: J. J. Thomson |
1907: Michelson |
1908: Lippmann |
1909: Braun, Marconi |
1910: van der Waals |
1911: Wien |
1912: Dalén |
1913: Kamerlingh Onnes |
1914: Laue |
1915: W. H. Bragg, W. L. Bragg |
1916: nicht verliehen |
1917: Barkla |
1918: Planck |
1919: Stark |
1920: Guillaume |
1921: Einstein |
1922: N. Bohr |
1923: Millikan |
1924: M. Siegbahn |
1925: Franck, G. Hertz |
1926: Perrin |
1927: Compton, C. T. R. Wilson |
1928: O. W. Richardson |
1929: de Broglie |
1930: Raman |
1931: nicht verliehen |
1932: Heisenberg |
1933: Schrödinger, Dirac |
1934: nicht verliehen |
1935: Chadwick |
1936: Hess, C. D. Anderson |
1937: Davisson, G. P. Thomson |
1938: Fermi |
1939: Lawrence |
1940–1942: nicht verliehen |
1943: Stern |
1944: Rabi |
1945: Pauli |
1946: Bridgman |
1947: Appleton |
1948: Blackett |
1949: Yukawa |
1950: Powell |
1951: Cockcroft, Walton |
1952: Bloch, Purcell |
1953: Zernike |
1954: Born, Bothe |
1955: Lamb, Kusch |
1956: Shockley, Bardeen, Brattain |
1957: Yang, T.-D. Lee |
1958: Tscherenkow, Frank, Tamm |
1959: Segrè, Chamberlain |
1960: Glaser |
1961: Hofstadter, Mößbauer |
1962: Landau |
1963: Wigner, Goeppert-Mayer, Jensen |
1964: Townes, Bassow, Prochorow |
1965: Feynman, Schwinger, Tomonaga |
1966: Kastler |
1967: Bethe |
1968: Alvarez |
1969: Gell-Mann |
1970: Alfvén, Néel |
1971: Gábor |
1972: Bardeen, Cooper, Schrieffer |
1973: Esaki, Giaever, Josephson |
1974: Ryle, Hewish |
1975: A. N. Bohr, Mottelson, Rainwater |
1976: Richter, Ting |
1977: P. W. Anderson, Mott, Van Vleck |
1978: Kapiza, Penzias, R. W. Wilson |
1979: Glashow, Salam, Weinberg |
1980: Cronin, Fitch |
1981: Bloembergen, Schawlow, K. Siegbahn |
1982: K. Wilson |
1983: Chandrasekhar, Fowler |
1984: Rubbia, van der Meer |
1985: von Klitzing |
1986: Ruska, Binnig, Rohrer |
1987: Bednorz, Müller |
1988: Lederman, Schwartz, Steinberger |
1989: Paul, Dehmelt, Ramsey |
1990: Friedman, Kendall, R. E. Taylor |
1991: de Gennes |
1992: Charpak |
1993: Hulse, J. H. Taylor Jr. |
1994: Brockhouse, Shull |
1995: Perl, Reines |
1996: D. M. Lee, Osheroff, R. C. Richardson |
1997: Chu, Cohen-Tannoudji, Phillips |
1998: Laughlin, Störmer, Tsui |
1999: ’t Hooft, Veltman |
2000: Alfjorow, Kroemer, Kilby |
2001: Cornell, Ketterle, Wieman |
2002: Davis Jr., Koshiba, Giacconi |
2003: Abrikossow, Ginsburg, Leggett |
2004: Gross, Politzer, Wilczek |
2005: Glauber, Hall, Hänsch |
2006: Mather, Smoot |
2007: Fert, Grünberg |
2008: Nambu, Kobayashi, Maskawa |
2009: Kao, Boyle, Smith |
2010: Geim, Novoselov |
2011: Perlmutter, Schmidt, Riess |
2012: Haroche, Wineland |
2013: Englert, Higgs |
2014: Akasaki, Amano, Nakamura |
2015: Kajita, McDonald |
2016: Thouless, Haldane, Kosterlitz |
2017: Barish, Thorne, Weiss
