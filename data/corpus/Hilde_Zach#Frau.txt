Hilde Zach (* 25. August 1942 in Hall in Tirol; † 15. Jänner 2011 in Innsbruck[1]) war eine österreichische Kommunalpolitikerin. Sie war von 2002 bis 2010 Bürgermeisterin von Innsbruck.

Nach einer kaufmännischen Ausbildung war Zach zunächst im elterlichen Betrieb, dann als selbständige Unternehmerin tätig. 1991 wurde sie Wirtschaftsbundbezirksobfrau von Innsbruck-Stadt. 1994 wurde sie Gründungsmitglied des Vereins Für Innsbruck, für den sie im selben Jahr erfolgreich als Stadträtin kandidierte. Ab 10. September 2001 war sie die Obfrau des Vereins.
Da am 26. Oktober 2002 der damals amtierende Innsbrucker Bürgermeister Herwig van Staa zum Landeshauptmann Tirols gewählt wurde, wählte der Gemeinderat von Innsbruck in einer Sondersitzung am 30. Oktober 2002 Hilde Zach zur neuen Bürgermeisterin und damit als erste Frau an die Spitze einer Landeshauptstadt[1].
Bei der Gemeinderatswahl am 23. April 2006 kam sie bei einer Wahlbeteiligung von 59,1 Prozent auf 26,8 Prozent der Stimmen und wurde trotz hohen Stimmenverlusts im Amt bestätigt.

Da sie einer Fleischerdynastie entstammte, wurde sie im Volksmund manchmal auch  „Fleischkas-Hilde“ oder kürzer, „Wurscht-Hilde“ genannt. Der Rufname „Wilde Hilde“ entsprang hingegen nicht ihrer Herkunft, sondern war vielmehr auf ihren teilweise als traditionell bezeichneten Umgang im politischen Alltag zurückzuführen[2].

Im März 2010 erklärte Zach aus gesundheitlichen Gründen ihren Rückzug aus der Politik. Ihre Nachfolge trat am 8. März 2010 Christine Oppitz-Plörer an[3]. Am selben Tag beschloss der Gemeinderat Innsbrucks, Hilde Zach die Ehrenbürgerwürde der Stadt zu verleihen[4].
Sie verstarb am 15. Jänner 2011 an den Folgen von Krebs.[1] Ihre Beisetzung erfolgte am 21. Jänner 2011 nach einer Trauerzeremonie im Innsbrucker Dom und einem großen Geleitzug durch die Innenstadt auf dem Westfriedhof.

Hilde Zach war Mitglied der Kammer der Gemeinden des Kongresses der Gemeinden und Regionen des Europarates.
