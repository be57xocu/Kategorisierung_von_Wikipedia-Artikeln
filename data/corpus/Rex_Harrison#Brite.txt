Sir Reginald Carey „Rex“ Harrison (* 5. März 1908 in Huyton, Lancashire, England; † 2. Juni 1990 in New York City, New York) war ein britischer Schauspieler. Er gewann 1965 für My Fair Lady den Oscar als bester Hauptdarsteller.

Rex Harrison begann seine Karriere im September 1924 im Alter von 16 Jahren am Liverpool Repertory Theatre mit dem Stück Thirty Minutes in a Street. 1930 kam er nach London, spielte an verschiedenen Bühnen und galt bereits mit 28 Jahren als der beste Lustspieldarsteller in England. Auf der Bühne wie im Film verkörperte er den Dandy, Charmeur und Womanizer.

Zum Kinostar wurde er erst 1945 als Schriftsteller Charles Condomine in Geisterkomödie. Trotz weiterer Leinwanderfolge blieb Harrison in den 1950er Jahren vorwiegend Bühnenschauspieler. Am Broadway war er ab 1956 als Prof. Higgins in dem Musical My Fair Lady überaus erfolgreich. Bis in die 1980er Jahre war er amerikaweit in dieser Paraderolle zu sehen. Als unerwarteter Bösewicht zeigte er sich 1960 in dem Krimi Mitternachtsspitzen an der Seite von Doris Day. Mitte der 1960er Jahre war er auf dem Höhepunkt seines Schaffens. Als Professor Higgins in der Musical-Verfilmung My Fair Lady und Julius Caesar in Cleopatra spielte er zwei seiner bekanntesten Rollen. Zu einem Misserfolg geriet 1982 sein letzter Film Zeit zu sterben (Originaltitel: A Time to Die, von Matt Cimber), in dem er einen NS-Juristen und Kriegsverbrecher darstellte, der es in der Bundesrepublik fast bis zum Justizminister bringt, aber von einem rachdürstigen Ex-GI erschossen wird.

1948 wurde Harrisons Name in Verbindung gebracht mit dem Selbstmord des Hollywood-Stars Carole Landis, mit der er ein Verhältnis gehabt haben soll. Als „Sexy Rexy“ wurde er zu einer Lieblingsfigur der Klatschkolumnisten.

Harrison war sechs Mal verheiratet:

Seine ersten vier Ehefrauen waren Schauspielerinnen. Mit Lilli Palmer ging er 1946 nach Hollywood und machte auch dort Karriere. Gemeinsam hatten sie auch am New Yorker Broadway Erfolg. 1956 wurde die Ehe geschieden. Mit Kay Kendall war er bis zu deren Tod 1959 verheiratet. Die Ehe mit der Schriftstellerin Mercia Tinker endete mit Harrisons Tod; er starb 1990 an Bauchspeicheldrüsenkrebs. Der Spiegel schrieb in seinem Nachruf: „Ein nobler, stilsicherer Komödiant, beherrschte er die Rolle des englischen Gentleman so perfekt, dass sie ihm auch im Leben zur zweiten Natur wurde. Seine Arroganz trug er wie einen Maßanzug, mit eleganter Ironie salopp unterfüttert […]“ Die Queen hatte ihn ein Jahr zuvor in den Ritterstand erhoben.

Kinofilme

 Fernsehen 
