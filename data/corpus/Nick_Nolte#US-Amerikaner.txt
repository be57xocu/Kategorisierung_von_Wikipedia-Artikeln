Nicholas King „Nick“ Nolte (* 8. Februar 1941 in Omaha, Nebraska) ist ein US-amerikanischer Schauspieler. 

Nolte begann 1960 seine schauspielerische Karriere bei dem amerikanischen Bühnenensemble Actor’s Inner Circle of Phoenix und trat innerhalb der darauffolgenden acht Jahre in etwa 180 Stücken auf.

1968 hatte er Auftritte am Old Log Theater in Minneapolis und mit der Experimentalgruppe La Mama Experimental Theatre in New York City, bis es ihn nach Los Angeles verschlug, wo er sich niederließ. Für kurze Zeit studierte er Schauspiel bei Stella Adler und trat am Pasadena Playhouse in Pasadena auf.

Er konzentrierte sich jedoch bald auf das Film- und Fernsehgeschäft. Einen seiner ersten TV-Auftritte hatte Nolte 1973 in einer Episode der Krimiserie Die Straßen von San Francisco. 1977 gelang ihm der Durchbruch mit der Serie Reich und Arm. Im selben Jahr hatte er auch seinen ersten Kinoerfolg mit dem Film Die Tiefe nach dem Roman von Peter Benchley. Nach einigen erfolgreichen Rollen in den 1980ern stieg er zu einem gefragten Charakterdarsteller auf. 1991 spielte er neben Martin Scorseses Publikumserfolg Kap der Angst die Hauptrolle in dem Drama Herr der Gezeiten an der Seite von Barbra Streisand. Nolte wurde für seine Leistung mit dem Golden Globe und anderen Auszeichnungen geehrt. Die Rolle brachte ihm zudem seine erste Oscarnominierung ein.

Nach einigen mäßig erfolgreichen Projekten konnte er 1997 wieder auf sich aufmerksam machen, als er die Hauptrolle in Oliver Stones Thriller U-Turn übernahm. Für seinen Part in Der Gejagte von Paul Schrader im gleichen Jahr wurde Nolte erneut für den Oscar und diverse weitere Preise nominiert. Ein Jahr später erhielt er eine größere Rolle in Terrence Malicks Kriegsfilm Der schmale Grat. Anfang des neuen Jahrtausends wurde seine Hauptrolle in Der Dieb von Monte Carlo (2003) von der Kritik hochgelobt, im Filmdrama Clean (2004) spielte er an der Seite von Maggie Cheung, die für ihre Rolle in dem Film den Darstellerpreis der Filmfestspiele von Cannes erhielt. Seither ist Nolte überwiegend in Nebenrollen zu sehen. Die aufsehenerregendste war 2011 im Sportdrama Warrior, für die er seine insgesamt dritte Oscarnominierung erhielt. Eine tragende Rolle spielte er 2015 in Picknick mit Bären an der Seite von Robert Redford.

Nolte fiel immer wieder öffentlich durch übermäßigen Alkoholkonsum auf. So wurde er  u. a. von der Polizei beim Autofahren im betrunkenen Zustand gefasst. Nolte gibt jedoch an, inzwischen vom Alkoholismus geheilt zu sein.

Nolte, dreifach geschieden, hat zwei Kinder. Einen Sohn, Brawley Nolte (* 1986), aus einer früheren Ehe, und seit Oktober 2007 eine Tochter mit seiner damals 38 Jahre alten, langjährigen Lebenspartnerin Clytie Lane.

Im Jahr 1992 wurde er vom People Magazine zum Sexiest Man Alive gewählt. Sein Vater Frank Nolte hat deutsche Wurzeln.

Nick Nolte hat einen Stammsprecher, Thomas Danneberg, der ihn fast immer synchronisiert. Wenn dieser nicht zur Verfügung steht, dann übernehmen Kollegen wie Rüdiger Bahr, Hartmut Becker, Tommi Piper, Christian Brückner, Ben Becker, Wolf Frass, Reiner Schöne,Thomas Fritsch oder Jochen Striebeck den Part.
