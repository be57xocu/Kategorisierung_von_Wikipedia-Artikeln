Rudolf Firkušný (* 11. Februar 1912 in Napajedla, Mähren; † 19. Juli 1994 in Staatsburg, New York) war ein tschechischer Pianist.

Firkušný war bereits als Fünfjähriger ein Schüler von Leoš Janáček und trat im Alter von acht Jahren in Prag auf, mit zehn Jahren spielte er Mozarts Krönungskonzert mit den Prager Philharmonikern.

1931 traf er erstmals Bohuslav Martinů. Seine Freundschaft zu diesem Musiker hatte auch nach dem Verlassen seiner Heimat 1939 nach dem deutschen Einmarsch in die Tschechoslowakei Bestand. Er lebte viele Jahre in Frankreich (Paris, Aix-en-Provence, Marseille) und in den USA. 1948 verließ er die Tschechoslowakei erneut und übersiedelte in die USA.
1971 lebte er in Luzern und unterrichtete Viera Janárceková.

Rudolf Firkušný gilt als profilierter Janáček-Interpret, dessen Gesamtwerk für Klavier er mehrfach einspielte. Außerdem wurde er für die Einspielung der Klavierwerke von Bohuslav Martinů mit dem Grammy Award ausgezeichnet.
