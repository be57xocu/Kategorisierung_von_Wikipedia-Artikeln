Manfred Carstens (* 23. Februar 1943 in Molbergen) ist ein deutscher Politiker (CDU).

Er war von 1989 bis 1993 Parlamentarischer Staatssekretär beim Bundesminister der Finanzen, von 1993 bis 1997 beim Bundesminister für Verkehr und von 1997 bis 1998 beim Bundesminister des Innern.

Nach dem Besuch der Handelsschule absolvierte Carstens eine Sparkassenlehre, die er mit der Fachprüfung als Sparkassen-Betriebswirt beendete. Er war dann als Sparkassenangestellter in Cloppenburg, Lohne und Oldenburg tätig. Von 1967 bis 1972 war er Direktor der Landessparkasse Emstek. 

Manfred Carstens ist verheiratet und hat drei Kinder.

Seit 1962 ist er Mitglied der CDU. Von 1980 bis 2003 war er Vorsitzender des CDU-Kreisverbandes Cloppenburg. Von April 1985 bis April 2009[1] war er Vorsitzender des CDU-Landesverbandes Oldenburg.

Von 1972 bis 2005 war er Mitglied des Deutschen Bundestages. Vom 14. Oktober 1982 bis zum 21. April 1989 war er hier Vorsitzender der Arbeitsgruppe Haushalt der CDU/CSU-Bundestagsfraktion.

Von 2002 bis 2005 war Carstens Vorsitzender des Haushaltsausschusses.

Manfred Carstens ist stets als direkt gewählter Abgeordneter des Wahlkreises Cloppenburg bzw. seit 1980 des Wahlkreises Cloppenburg - Vechta in den Deutschen Bundestag eingezogen. Zuletzt erreichte er hier bei der Bundestagswahl 2002 62,2 % der Erststimmen.

Anlässlich einer Kabinettsumbildung wurde Carstens am 21. April 1989 als Parlamentarischer Staatssekretär beim Bundesminister der Finanzen in die von Bundeskanzler Helmut Kohl geführte Bundesregierung berufen. Am 22. Januar 1993 wechselte er in gleicher Funktion zum Bundesminister für Verkehr und am 15. Mai 1997 schließlich zum Bundesminister des Innern. Nach der Bundestagswahl 1998 schied er am 26. Oktober 1998 aus dem Amt.

Seit 1990 ist er Mitglied im wissenschaftlichen katholischen Studentenverein Unitas Rhenania zu Bonn.

Erwin Schoettle (1949–1969) |
Albert Leicht (1969–1977) |
Heinrich Windelen (1977–1981) |
Lothar Haase (1981–1982) |
Helmut Esters (1982–1983) |
Rudi Walther (1983–1994) |
Helmut Wieczorek (1994–1998) |
Adolf Roth (1998–2002) |
Manfred Carstens (2002–2005) |
Otto Fricke (2005–2009) |
Petra Merkel (2009–2013) |
Gesine Lötzsch (seit 2014)
