Omid Nouripour (persisch امید نوری پور [omiːd ɛ nuːriːˈpuːr]; * 18. Juni 1975 in Teheran, Iran) ist ein deutscher Politiker (Bündnis 90/Die Grünen) und Mitglied des Deutschen Bundestages (MdB). Er besitzt neben der deutschen auch die iranische Staatsangehörigkeit.

Im August 1988 verließ Nouripour gemeinsam mit seiner Familie den Iran und ging nach Frankfurt am Main. Hier erlangte er 1996 das Abitur an der Bettinaschule.

Anschließend begann er ein Studium der Deutschen Philologie mit den Nebenfächern Politikwissenschaft und Rechtswissenschaft an der Johannes Gutenberg-Universität Mainz, das er 1997 um ein Zweitstudium der Soziologie, Philosophie und Volkswirtschaftslehre ergänzte. Beide Studiengänge hat er nicht abgeschlossen. Dennoch wurde er u. a. in offiziellen Bundestags-Biografien als „Promovend der Germanistik“ geführt, was ihm durch die Bild-Zeitung in der Ausgabe vom 21. November 2008 den Titel Angeber im Bundestag eintrug.[1] Er selbst widerrief danach in einer persönlichen Erklärung die quasi-akademische Bezeichnung als „Fehler“ und als „veralteten biografischen Eintrag“.

Nouripour ist seit 2002 deutscher Staatsbürger und hat weiterhin auch die iranische Staatsangehörigkeit. In seiner Freizeit tritt er manchmal als Rapper unter dem Namen „MC Omid“ auf.

Seit 1996 ist er Mitglied bei den Grünen. Er engagierte sich zunächst in der Grünen Jugend, deren hessischer Landesvorsitzender er von 1999 bis 2003 war, und bei der Migranteninitiative ImmiGrün. In dieser Zeit gehörte er auch dem Landesvorstand der Grünen in Hessen an. Von 2002 bis 2010 war er Sprecher der Bundesarbeitsgemeinschaft MigrantInnen und Flüchtlinge.  

Auf der Bundesdelegiertenkonferenz am 8. Dezember 2002 wurde Nouripour gemeinsam mit Katja Husen als Beisitzer in den Bundesvorstand von Bündnis 90/Die Grünen gewählt. Ende 2004 wurde er in Kiel wiedergewählt. Am 2. Dezember 2006 trat er nicht wieder an, und Malte Spitz wurde sein Nachfolger.

Nouripour war Mitglied der Rechtsextremismus-Kommission des Bundesvorstands von Bündnis 90/Die Grünen.[2]
Am  1. September 2006 rückte er für den ausgeschiedenen Abgeordneten Joschka Fischer über die Landesliste Hessen in den Bundestag nach. Dort war er zunächst Mitglied im Europaausschuss und von 2008 bis zur Bundestagswahl 2009 Mitglied im Haushalts- und Verteidigungsausschuss.

Von 2009 bis 2013 war Nouripour sicherheitspolitischer Sprecher der Fraktion Bündnis 90/Die Grünen. 
Seit der Bundestagswahl 2013 ist er außenpolitischer Sprecher seiner Fraktion. Zudem ist er ordentliches Mitglied im Auswärtigen Ausschuss und im Ausschuss für Menschenrechte und humanitäre Hilfe sowie stellvertretendes Mitglied im Verteidigungsausschuss und im Unterausschuss für Auswärtige Kulturpolitik.

Im September 2014 sprach sich Nouripour für Luftschläge gegen den Islamischen Staat aus, lehnte hingegen Waffenlieferungen an die Kurden ab.[3]
Nouripour unterstützt im Zusammenhang mit der Ukraine-Krise Wirtschaftssanktionen gegen Russland, die er als sinnvoll und notwendig bezeichnet. Zudem spricht er sich für eine EU-Beitrittsperspektive für die Ukraine aus.[4]
Nouripour ist Mitglied des Vorstandes der Atlantik-Brücke[7] sowie Mitglied der Europa-Union Parlamentariergruppe Deutscher Bundestag. Zudem ist Nouripour Beisitzer im Vorstand der Deutschen Atlantischen Gesellschaft[8]. Bis Februar 2014[9] saß er mehrere Jahre lang im Beirat des Forums für interkulturellen Dialog e. V. (FID),[10] dessen Ehrenvorsitzender der islamische Prediger Fethullah Gülen ist. Er ist Vorsitzender des bundesAdler e. V., des Fanclubs von Eintracht Frankfurt im Deutschen Bundestag, dessen Gründung er maßgeblich initiierte.[11]