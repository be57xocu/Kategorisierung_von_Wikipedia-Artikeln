Rosa Mayreder (geb. Obermayer, Pseudonym: Franz Arnold) (* 30. November 1858 in Wien; † 19. Jänner 1938 ebenda) war eine österreichische Schriftstellerin, Frauenrechtlerin, Kulturphilosophin, Librettistin, Musikerin und Malerin.

Die Tochter des wohlhabenden Wiener Gastwirts vom Winterbierhaus konnte sich von Jugend an als Malerin und Schriftstellerin betätigen. Sie liebte die Wissenschaft und kämpfte früh gegen das männliche Primat in der Bildung. Dies schien ihr im Herkommen begründet, das sie durch „die neuen, besseren Sitten“ ersetzen wollte. Selbst ging sie zunächst von Anthropologie und Physik aus, stieß aber bald auch auf die besondere Bedeutung der Sprache. Mit siebenunddreißig Jahren brachte sie gemeinsam mit Hugo Wolf die Oper „Der Corregidor“ (nach der Novelle „Der Dreispitz“ von Alarcon) heraus, deren Libretto sie verfasst hat; sie gehörte zu Wolfs Förderinnen.

1881 heiratete Rosa ihren Jugendfreund, den Architekten und späteren Rektor der Technischen Hochschule Wien Karl Mayreder. Bei der Frauenrechtlerin Marie Lang lernte sie Anfang der 1890er Jahre Marianne Hainisch kennen und gründete 1893 den Allgemeinen Österreichischen Frauenverein mit, deren Vorstandsmitglied und Vizepräsidentin sie wurde. Ab 1899 gab sie gemeinsam mit Marie Lang und Auguste Fickert die Zeitschrift Dokumente der Frauen heraus.
Sie stand auch mit der Komponistin Mathilde Kralik von Meyrswalden in Kontakt. In einem Brief vom 13. Mai 1936 schreibt die Komponistin ihr einen Brief, hier ein Auszug: „Sehr verehrte Frau, Ich bin entzückt von Ihren herrlichen Sonetten, die gleicherweise formvollendet und gedankentief und so reich an Sprachschönheit sind, dass sie schon die Musik in sich tragen …“ Quelle: Wienbibliothek.

In ihren Büchern, aber auch in Gesprächen, die sie in ihren Tagebüchern niederlegte, versuchte sie als Kulturschaffende, ein gleichwertiges Verhältnis der Geschlechter durchzusetzen, durch das weder der Mann die Frau noch diese den Mann nur körperlich begehrt. Mit ihrem Ansinnen stieß sie in literarischen Kreisen auf Anerkennung und Zustimmung. Ihre Gegner sah sie vor allem unter Vertretern der Medizin, die von Mayreder als ein Hort seelischer Willkür und der Herabwürdigung von Frauen zum Sexualobjekt empfunden wurde. Sie wandte sich gegen die Diskriminierung ihres Geschlechts und die bestehende Doppelmoral. Ihre Werke fanden weite Verbreitung und wurden auch ins Englische übertragen.
Auf der letzten herausgegebenen 500-Schilling-Banknote fand sich neben ihrem Abbild das Zitat „Die beiden Geschlechter stehen in einer zu engen Verbindung, sind voneinander zu abhängig, als dass Zustände, die das eine treffen, das andere nicht berühren sollten“ (1905). Allerdings liebte Rosa Mayreder selbst durchaus auch großbürgerliche Sitten, die sie mit ihren inneren Anliegen in eins zu verschmelzen suchte.

Ambivalent blieb ihr Verhältnis zu Rudolf Steiner: zeigt der gemeinsame Briefwechsel ein echtes Angezogensein, so sind die Tagebucheintragungen von Missfallen an der Ferne des – wenn auch als Schriftsteller für bedeutend gehaltenen – Jugendgefährten vom Praktischen geprägt.

Mayreder, die selbst zuerst als Malerin tätig gewesen war, gründete in den Jahren vor dem Ersten Weltkrieg mit Olga Prager und Kurt Federn die „Kunstschule für Frauen und Mädchen“ (später umbenannt in Wiener Frauenakademie).

Vor und während des Krieges engagierte sie sich gemeinsam mit Bertha von Suttner in der Friedensbewegung und wurde 1919 Vorsitzende der „Internationalen Frauenliga für Frieden und Freiheit“ (IFFF), obwohl sie durch die Pflege ihres psychisch erkrankten Mannes ab 1912 in ihrer Arbeit stark eingeschränkt war. Sie kritisierte alle Formen des Militarismus, den sie als typisch männliches Machwerk sah. In den Jahren nach dem Ersten Weltkrieg konstatierte Mayreder scharfsichtig einen kulturellen Rückschritt ins 19. Jahrhundert.

1928 wurde Rosa Mayreder die Auszeichnung als „Ehrenbürgerin der Stadt Wien“ zugesagt, nachdem sie sich aber öffentlich zu ihrem jüdischen Großvater bekannte, wurde sie aber nur als „Bürgerin ehrenhalber der Stadt Wien“ gewürdigt. 1938 starb sie im Alter von 80 Jahren in Wien. Rosa Mayreder ist in der auf dem Wiener Zentralfriedhof in der ersten Reihe hinter der Friedhofskirche zum heiligen Karl Borromäus gelegenen Familiengrabstätte „Mayreder“ an der Seite ihres Ehegatten Karl, ihrer Schwiegereltern Leopold und Henriette Mayreder (Besitzer des berühmten Hotels Matschakerhof in Wien I, Spiegelgasse 5/Seilergasse 7), ihres Schwagers, des Architekten Julius Mayreder und ihrer Schwägerinnen beerdigt. Diese Grabstätte ist bis heute weder ein Ehrengrab der Stadt Wien noch ein ehrenhalber gewidmetes Grab.

Nach Mayreder wurde das 1999 gegründete Rosa Mayreder College in Wien benannt, das sich der feministischen Bildungsarbeit widmet. Im Jahr 1965 wurde in Wien Donaustadt (22. Bezirk) die Mayredergasse nach ihr benannt. Nahe dem Karlsplatz befindet sich seit 2005 der Rosa-Mayreder-Park.[1]
Ihr Konterfei war auf der 500-Schilling-Banknote von 1997 zu sehen. Die Rückseite der Banknote zeigt Porträtfotos von Rosa und Karl Mayreder (oben) und ein Gruppenbild der Teilnehmerinnen des Bundestags Österreichischer Frauenvereine in Wien 1911.

Essays und Aufsätze

Erzählende Prosa

Libretto

Theaterstücke

Aphorismen, Gedichte

Memoiren, Tagebücher, Briefe
