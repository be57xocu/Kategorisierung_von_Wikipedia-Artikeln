René Monory (* 6. Juni 1923 in Loudun; † 11. April 2009 ebenda) war ein französischer Politiker der UDF[1]. Monory bekleidete oft Ministerposten und war Präsident des französischen Senats.

Monory, ohne weiterführenden Schulabschluss, war als Automechaniker tätig und trug daher den Spitznamen garagiste de Loudun,[1] als er 1959 das erste Mal zum Bürgermeister seiner Heimatstadt Loudun gewählt wurde. Von 1968 bis 1977 und von 1986 bis 2004 war er Mitglied des französischen Senats für Vienne. 1977 berief Premierminister Raymond Barre ihn als Minister für Industrie, Handel und Handwerk, ab 1978 als Minister für Wirtschaft, ein Amt, das er bis 1981 ausübte, wobei zu seinen Leistungen vor allem die Einführung des Volks-Sparkassensystems SICAV zählt. Er wurde am 20. März 1986 als Bildungsminister in das bürgerliche Kabinett von Premierminister Jacques Chirac berufen. Er hatte das Amt bis zum Ende dieser Regierung am 12. Mai 1988 inne.
1992 bis 1998 war er Präsident des französischen Senats. Auf seine Initiative geht der Bau des Futuroscopes der Stadt Poitiers zurück.

Er wurde mit dem Orden des Ritters der Ehrenlegion ausgezeichnet.[1] 1997 erhielt er das Große Goldene Ehrenzeichen am Bande für Verdienste um die Republik Österreich.[2]
Édouard Ramonet |
Jean-Marcel Jeanneney |
Michel Maurice-Bokanowski |
Raymond Marcellin |
Olivier Guichard |
Albin Chalandon |
André Bettencourt |
François-Xavier Ortoli |
Jean Charbonnel |
Yves Guéna |
Michel d’Ornano |
René Monory |
André Giraud |
Pierre Joxe |
Pierre Dreyfus |
Jean-Pierre Chevènement |
Laurent Fabius |
Édith Cresson |
Alain Madelin |
Roger Fauroux |
Dominique Strauss-Kahn |
Gérard Longuet |
José Rossi |
Yves Galland |
Franck Borotra |
Dominique Strauss-Kahn |
Christian Sautter |
Laurent Fabius |
Francis Mer |
Nicolas Sarkozy |
Hervé Gaymard |
Thierry Breton |
Christine Lagarde |
François Baroin |
Arnaud Montebourg |
Emmanuel Macron

Antoine Pinay |
Wilfrid Baumgartner |
Valéry Giscard d’Estaing |
Michel Debré |
Maurice Couve de Murville |
François-Xavier Ortoli |
Valéry Giscard d’Estaing |
Jean-Pierre Fourcade |
Raymond Barre |
René Monory |
Jacques Delors |
Pierre Bérégovoy |
Édouard Balladur |
Pierre Bérégovoy |
Michel Sapin |
Edmond Alphandéry |
Alain Madelin |
Jean Arthuis |
Dominique Strauss-Kahn |
Christian Sautter |
Laurent Fabius |
Francis Mer |
Nicolas Sarkozy |
Hervé Gaymard |
Thierry Breton |
Jean-Louis Borloo |
Christine Lagarde |
François Baroin |
Pierre Moscovici |
Michel Sapin

Jean Berthoin |
André Boulloche |
Michel Debré |
Louis Joxe |
Pierre Guillaumat |
Lucien Paye |
Pierre Sudreau |
Louis Joxe |
Christian Fouchet |
Alain Peyrefitte |
François-Xavier Ortoli |
Edgar Faure |
Olivier Guichard |
Joseph Fontanet |
René Haby |
Christian Beullac |
Alain Savary |
Jean-Pierre Chevènement |
René Monory |
Lionel Jospin |
Jack Lang |
François Bayrou |
Claude Allègre |
Jack Lang |
Luc Ferry |
François Fillon |
Gilles de Robien |
Xavier Darcos |
Luc Chatel |
Vincent Peillon |
Benoît Hamon |
Najat Vallaud-Belkacem |
Jean-Michel Blanquer
