Matilda Shannon Wells Lucid (* 14. Januar 1943 als Matilda Shannon Wells in Shanghai, China) ist eine ehemalige US-amerikanische Astronautin.

Lucid wurde im chinesischen Shanghai in eine Missionarsfamilie geboren. Während der japanischen Besetzung von China wurde ihre Familie interniert, konnte aber 1944 in die USA ausreisen und lebte in Fort Worth. Nach dem Krieg kehrte die Familie wieder nach China zurück, wo Lucid auch in die Grundschule ging. Die High School besuchte sie in Bethany, Oklahoma, danach nahm sie an der University of Oklahoma ein Chemiestudium auf und erwarb 1963 einen Bachelor. 1970 folgte ein Master in Biochemie und drei Jahre später eine Promotion.

Lucid wurde im Januar 1978 für das Astronautenteam der NASA ausgewählt. Dies war die erste Gruppe, in der auch Frauen zugelassen waren.

Ihren ersten Raumflug unternahm Lucid im Juni 1985 an Bord der US-Raumfähre Discovery in der Mission STS-51-G. Weitere Shuttle-Missionen in den Jahren 1989 mit STS-34, 1991 mit STS-43 und 1993 mit STS-58 folgten.

Besondere Bekanntheit erlangte sie durch ihren fünften Raumflug, als sie 188 Tage im Weltraum verbrachte, 179 Tage davon im Rahmen des Shuttle-Mir-Programms auf der russischen Raumstation Mir. Am 22. März 1996 wurde sie mit STS-76 zur Mir gebracht und gelangte am 26. September 1996 mit STS-79 wieder zurück auf die Erde. Beide Missionen wurden mit der Raumfähre Atlantis durchgeführt. Ihr Aufenthalt in der Mir sollte eigentlich nicht so lange sein, aber ihre Rückkehr verzögerte sich zweimal, insgesamt um sechs Wochen. Nach ihrer Mission zur Mir wurde ihr die Congressional Space Medal of Honor verliehen. 

Im Jahr 2002 wurde sie NASA Chefwissenschaftlerin im NASA Headquarters in Washington. Sie kehrte im Sommer 2003 zum Johnson Space Center nach Houston zurück und wurde im Astronautenbüro für technische Aufgaben bestimmt. Sie agierte als (Capcom) im Mission Control Center. 

2005 war sie Verbindungssprecher/Capcom während der STS-114-Mission, sowie im Mai 2011 während der STS-134-Mission und ebenso im Juli 2011 während der letzten Shuttle-Mission STS-135.

Lucid verließ die NASA im Januar 2012.[1]
Sie ist verheiratet und hat drei Kinder.
