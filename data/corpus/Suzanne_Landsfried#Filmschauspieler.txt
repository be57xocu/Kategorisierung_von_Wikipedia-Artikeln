Suzanne Landsfried (* 22. November 1973) ist eine deutsche Schauspielerin.

Suzanne Landsfried begann ihre Karriere 2001 nach der Ausbildung an der Berliner Schule für Schauspiel am Theater. Nach kleineren Rollen am Maxim-Gorki-Theater und der Miss Ping in Doris Dörries Turandot-Inszenierung an der Staatsoper Berlin spielt sie an Berliner Off-Bühnen u. a. die Solange in Jean Genets Die Zofen (Scheselong) und die Anna in Blaubart, das im Berliner Tiyatrom aufgeführt wurde.

Seit 2004 spielt Landsfried eine feste Nebenrolle als freche Capitol-Angestellte in der Fernsehserie Stromberg. Auf kleinere Rollen wie die Sekretärin eines Wissenschaftlers im Fernsehfilm Tsunami folgten neben einer Gastrolle als Schwester Gabi in drei Folgen der Serie Nikola der deutsche Kinofilm Oktoberfest, in dem sie Barbara Rudniks naive Ausschankkollegin Daniela spielt, und der Fernsehfilm Das geteilte Glück, in dem sie sich als resolute Kiosksbesitzerin Biggi in der Ehekrise eines Paares (Petra Schmidt-Schaller und Rüdiger Klink) den zweifelnden Gatten angelt. Als Psychotherapeutin Dr. Gregorius von Christian Ulmen war sie in Serie Die Snobs zu sehen. Als Oberärztin Dr. Söderberg spielte sie in Polizeiruf 110 und neben Henry Hübchen und Florian David Fitz in Da geht noch was als Frau Sitl - eine EDEKA - Kassiererin. 

Zudem hat sie eine eigene Firma, die SLL Medien und Events, die unter anderem bei der Vermittlung, Produktion und Beratung von Imagefilmen, Werbespots und Corporate TV tätig ist und unterschiedliche Events und Veranstaltungen organisiert.[1]
Suzanne Landsfried wohnt in Grünwald.
