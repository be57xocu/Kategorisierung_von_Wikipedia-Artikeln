Mia Farrow (* 9. Februar 1945 als Maria de Lourdes Villiers Farrow in Los Angeles, Kalifornien) ist eine US-amerikanische Schauspielerin. Sie wurde berühmt durch ihre Rolle in Rosemaries Baby sowie durch Filme von und mit Woody Allen. Für ihre schauspielerischen Leistungen erhielt sie Auszeichnungen wie den Golden Globe und den David di Donatello Award sowie drei Nominierungen für den BAFTA Film Award.

Farrow engagiert sich für zahlreiche humanitäre Projekte in Afrika und ist Sonderbotschafterin für UNICEF.

Farrow ist die Tochter des australischen Schriftstellers, Schauspielers und Regisseurs John Farrow und der irischen Schauspielerin Maureen O’Sullivan. Sie wuchs mit ihren sechs Geschwistern größtenteils in Beverly Hills auf und kam schon früh mit dem Filmgeschäft in Berührung.

Im Alter von neun Jahren erkrankte Farrow an Polio.[1][2] Es dauerte Monate bis zu ihrer vollständigen Genesung, und in ihren Memoiren bezeichnet die Schauspielerin diese Zeit als „das Ende meiner Kindheit“.[3]
1958 zog Farrow mit ihrer Familie für ein Filmprojekt des Vaters einige Monate nach England, wo sie mit ihrer jüngeren Schwester Prudence ein Klosterinternat in Surrey besuchte.[4] Im selben Jahr kam Farrows ältester Bruder Michael bei einem Flugzeugabsturz ums Leben.[5] Im Herbst 1959 zog die Familie zurück nach Beverly Hills, wo Farrow im Jahr 1962 die High School abschloss.

Ihr Filmdebüt gab Farrow im Alter von zwölf Jahren in einer kleinen Rolle in dem Film Beherrscher der Meere (1959), bei dem ihr Vater Regie führte. Im Januar 1963 zog Farrow nach New York City, um hier Schauspielunterricht zu nehmen, und erhielt bald darauf die Rolle der Cecily in der Off-Broadway-Inszenierung von Oscar Wildes Theaterstück The Importance of Being Earnest.

Im September 1963 drehte sie den Pilotfilm zur Fernsehserie Peyton Place und danach den Film Schüsse in Batasi (1964) mit Richard Attenborough in der Hauptrolle. Im Anschluss daran widmete sich Farrow wieder den Dreharbeiten zu Peyton Place. Die Serie wurde in den USA von einem Millionenpublikum verfolgt und Farrow zum nationalen TV-Star. 1965 wurde sie mit dem Golden Globe Award als beste Nachwuchsdarstellerin ausgezeichnet.[6] Es folgten sieben weitere Nominierungen für diesen Preis. 1966 verließ Farrow die TV-Serie.

In den Londoner Pinewood Studios drehte Farrow den Film Der Todestanz eines Killers (1968) und bekam kurze Zeit später von Paramount Pictures die Hauptrolle in Roman Polańskis Rosemaries Baby (1968) angeboten. Die Verfilmung des Bestsellers von Ira Levin wurde zum weltweiten Kultfilm und verschaffte Farrow den endgültigen Durchbruch als Schauspielerin. Für die Rolle der Rosemarie Woodhouse erhielt sie noch im gleichen Jahr den David di Donatello Award und den Laurel Award. Außerdem wurde sie als beste Hauptdarstellerin für den Golden Globe Award und den BAFTA Film Award nominiert.[7]
Es folgten die Filme Die Frau aus dem Nichts (1968) mit Elizabeth Taylor und Robert Mitchum sowie John und Mary (1969) an der Seite von Dustin Hoffman. In den 1970er Jahren lebte und arbeitete Farrow überwiegend in England. Zwischen den Dreharbeiten zu Filmen wie Der große Gatsby (1974) an der Seite von Robert Redford und Eine Hochzeit (1978) unter der Regie von Robert Altman spielte sie vor allem Theater mit der Royal Shakespeare Company. 1977 reiste Farrow für die Dreharbeiten der Agatha Christie-Verfilmung Tod auf dem Nil nach Ägypten und im Sommer 1978 nach Bora Bora, um Hurricane (1979) zu drehen. Danach kehrte sie nach New York City zurück und spielte im Theaterstück Romantic Comedy.

In der englischen Originalversion des Zeichentrickfilms Das letzte Einhorn (1982) lieh Farrow dem Einhorn bzw. der Lady Amalthea ihre Stimme.

1982 drehte Farrow erstmals unter der Regie ihres damaligen Lebensgefährten Woody Allen den Film Eine Sommernachts-Sexkomödie. Das Paar drehte von 1983 bis 1992 insgesamt zwölf Filme miteinander, darunter The Purple Rose of Cairo (1985), Hannah und ihre Schwestern (1986), New Yorker Geschichten (1989), Alice (1990) sowie Ehemänner und Ehefrauen (1992).

1995 spielte Farrow an der Seite von Sarah Jessica Parker und Antonio Banderas in dem Kinofilm Miami Rhapsody, wirkte aber nach der privaten und beruflichen Trennung von Allen ansonsten überwiegend in Fernsehproduktionen mit. 2006 kehrte Farrow mit Das Omen und in Luc Bessons Arthur und die Minimoys sowie dessen Fortsetzungen 2009 und 2010 auf die Kinoleinwand zurück.

Im Jahr 2000 wurde Mia Farrow zur Sonderbotschafterin für UNICEF ernannt.[8]
In dieser Funktion setzt sie sich besonders für die Ausrottung von Polio ein.[9] 2001 reiste die Schauspielerin nach Nigeria und 2010 in den Tschad, um großangelegte Impfaktionen gegen die Krankheit zu unterstützen.[10][11]
Weitere Reisen in die Krisenregion Darfur, um auf die sudanesische Flüchtlingssituation und das dortige Notleiden der Menschen aufmerksam zu machen, folgten ab 2004.[12] Im Juni 2006 wurde sie von ihrem Sohn Ronan begleitet. Anschließend reisten beide nach Berlin und besuchten – als Teil einer Wohltätigkeitsveranstaltung und UNICEF-Hilfe für den Sudan – die Ausstellung der United Buddy Bears.[13][14] Im Rahmen der Kampagne Save Darfur trat Farrow am 27. April 2009 in einen zwölftägigen Hungerstreik, den sie auf ihrer Website miafarrow.org dokumentierte.[15]
Farrow war zweimal verheiratet und ist Mutter von vier leiblichen und zehn adoptierten Kindern.[16][17][Anmerkung 1]
Ihre erste Ehe mit Frank Sinatra dauerte von 1966 bis 1968 und blieb kinderlos.

Im September 1970 heiratete sie den Komponisten und Dirigenten André Previn.[18] Das Paar hat drei leibliche Söhne und adoptierte zwei Mädchen aus Vietnam[19][20] sowie im Jahr 1978 die damals achtjährige Soon-Yi aus Südkorea.[21] Farrow und Previn ließen sich 1979 scheiden.[22]
1980 wurde Farrow die Lebensgefährtin von Woody Allen.[23] Gemeinsam adoptierten sie zwei Kinder[24][25] und bekamen 1987 einen leiblichen Sohn, Ronan Farrow. Die Trennung von Allen im Jahr 1992 sorgte für sehr viel Aufsehen und moralische Kontroversen in den Medien aufgrund der neuen Beziehung Woody Allens zu Farrows Adoptivtochter Soon-Yi[26][27] und dem darauffolgenden Sorgerechtsstreit um die drei gemeinsamen Kinder.[28][29]
Am 7. Juni 1993 erhielt Farrow das alleinige Sorgerecht von einem New Yorker Gericht zugesprochen.[30]
In den 1990er Jahren adoptierte Farrow fünf weitere Kinder.

Golden Globe Award

British Academy Film Award

David di Donatello Award

Festival Internacional de Cine de Donostia-San Sebastián

Laurel Award

Goldene Himbeere

National Board of Review Award

Satellite Award

Kurz nach ihrer Scheidung von Sinatra reiste Farrow 1968 mit ihrer Schwester Prudence in ein Ashram im indischen Rishikesh, um an einem mehrwöchigen Meditationskurs teilzunehmen. Zur gleichen Zeit lebten auch die Beatles in dem Ashram und John Lennon schrieb für Mias Schwester den Song Dear Prudence, der 1968 auf dem Album The Beatles veröffentlicht wurde.[31] Von Mia selbst handelt das Lied Beware of Young Girls (1970) der US-amerikanischen Sängerin, Songschreiberin und Dichtern Dory Previn, die den Song nach ihrer Scheidung von André Previn im Jahr 1970 über dessen neue Ehefrau schrieb.[32]
Im August 2010 sagte Mia Farrow vor dem Sondergerichtshof für Sierra Leone in Den Haag gegen den ehemaligen liberianischen Präsidenten und mutmaßlichen Kriegsverbrecher Charles Taylor aus.[33]