Hans Eduard Suess (* 16. Dezember 1909 in Wien; † 20. September 1993 in San Diego) war ein österreichischer physikalischer Chemiker und Kernphysiker.

Hans Eduard Suess' Vater war der Geologe Franz Eduard Suess, sein Großvater der Geologe Eduard Suess. Er promovierte 1935 an der Universität. Über seine Beschäftigung mit chemischen Gleichgewichtsreaktionen wurde er im Zweiten Weltkrieg Berater bei der Abtrennung von Schwerwasser im Wasserkraftwerk der Norsk Hydro in Vemork, Norwegen. Nach dem Krieg war er an der Entwicklung des Kernschalenmodells beteiligt mit Otto Haxel und J. Hans D. Jensen.

1950 emigrierte er in die USA und war zuletzt Professor of Chemistry an der University of California in La Jolla. Er arbeitete auf dem Gebiet der Kalibrierung der Radiokohlenstoffdatierung und der Kosmochemie, wobei er die Elementhäufigkeiten untersuchte. 1972 wurde er in die American Academy of Arts and Sciences gewählt. Nach Suess ist der Suess-Effekt benannt, der den Einfluss der Industrialisierung auf den 14C-Gehalt in der Atmosphäre beschreibt.

Hans Eduard Suess erhielt 1974 den V. M. Goldschmidt Award.

Ironischerweise ist das Mineral Suessit (ein Fe-Ni-Silicid aus Enstatitchondriten) nach ihm benannt und nicht nach seinen berühmten Geologen-Vorfahren.
