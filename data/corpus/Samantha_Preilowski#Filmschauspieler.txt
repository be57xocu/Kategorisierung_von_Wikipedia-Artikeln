Samantha Preilowski (* 24. Februar 1989 in Berlin) ist eine deutsche Schauspielerin.

Bekannt wurde sie durch ihre Rolle in der KI.KA-Serie Schloss Einstein, in der sie von 2002 bis 2005 die Rolle der Antonia Fabri spielte. 2006 folgten einige Rollen in der RTL-Serie Abschnitt 40 und in der KI.KA-Serie Leo – ein fast perfekter Typ. Im selben Jahr kehrte sie für einen Gastauftritt zu Schloss Einstein zurück. 

Preilowski besuchte von 2007 bis 2009 eine Berliner Schauspielschule. Außerdem besuchte sie 2010 die Meisner Technique in Berlin.

Preilowski lebt in Berlin.
