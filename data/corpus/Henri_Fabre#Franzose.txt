Henri Fabre (* 29. Juni 1882 Marseille; † 1. Juli 1984) war ein französischer Luftfahrtpionier, der 1910 den ersten Flug mit einem Wasserflugzeug durchführte.

Henri Fabre war ein Ingenieur, der bereits früh die Flugzeuge von Louis Blériot und Gabriel Voisin untersuchte. Daraufhin entschloss er sich 1909 mit dem Bau des ersten Prototyps eines Wasserflugzeugs zu beginnen. Fabre investierte viel Zeit in den Bau der Schwimmer. Diese Kenntnisse halfen ihm später, fremde Flugzeugtypen mit Schwimmern auszurüsten.

Der Prototyp hatte allerdings nur einen schwachen 18 kW (25 PS) Motor und flog deshalb nicht. Die nächste Maschine erhielt einen leistungsstarken 37 kW (50 PS) Gnôme-Sternmotor. Er nannte das Flugzeug Hydravion.

Fabre war vorher nie geflogen, und es bedurfte einigen Mutes, sich auf sein filigranes Flugzeug zu verlassen. Das Hydravion-Wasserflugzeug war konzeptionell ein Entenflugzeug in Gitterbauweise und besaß einen Druckpropeller.

Am 28. März 1910 begannen die Startversuche in Martigues bei Marseille. Der zweite Start gelang, und das Flugzeug flog rund 600 m. Am nächsten Tag waren es sogar 6 km. Das filigrane Hydravion erwies sich als sehr stabil. Nach einem Absturz bei der Landung wurde das Flugzeug überarbeitet. Im April 1911 startete die neue Version mit dem Piloten Jean Bécue in Monaco. Allerdings wurde das Flugzeug in der starken Brandung am Ufer zerstört.

Da Fabre keine finanzielle Reserven hatte, stellte er den Bau neuer Flugzeuge ein und baute fortan Schwimmer, z. B. für Voisin.

Das Hydravion kann heute im Musée de l’air et de l’espace in Le Bourget besichtigt werden.

Fabre flog rund ein Jahr vor Glenn Curtiss und gilt somit als Erbauer des ersten vollfunktionsfähigen Wasserflugzeugs. Zwar unternahm manchen Quellen zufolge bereits im Jahre 1901 der Österreicher Wilhelm Kress Flugversuche mit einem Wasserflugzeug, dies ist aber bis heute umstritten.
