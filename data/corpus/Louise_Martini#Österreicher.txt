Louise Martini (eigentlich Marie-Louise Chiba, verehelichte Schwarz; * 10. November 1931 in Wien; † 17. Januar 2013 ebenda)[1] war eine österreichische Schauspielerin und Radiomoderatorin.

Die Liebe zum Theater entdeckte Louise Martini, die den Geburtsnamen ihrer Mutter zu ihrem Künstlernamen machte, als sie mit zwölf Jahren in einer Schulaufführung das Lottchen in Ferdinand Raimunds Der Bauer als Millionär spielte. Bereits vor Ende ihrer Schulzeit begann sie am Max-Reinhardt-Seminar ihre Schauspielausbildung, die sie ein Jahr nach ihrer Matura abschloss. Ihr erstes Engagement erhielt sie am Kleinen Theater im Konzerthaus, dann spielte sie am Wiener Volkstheater.

In den 1950er Jahren war sie Mitglied der heute als Namenloses Ensemble bekannten Kabarettgruppe, der auch Gerhard Bronner, Helmut Qualtinger, Carl Merz, Peter Wehle, Georg Kreisler und Michael Kehlmann angehörten. Von Beginn an war Martini ab 1957 Moderatorin der Radiosendung Autofahrer unterwegs.

1962 übersiedelte sie nach München und feierte dort in dem Musical Irma La Douce Erfolge. Später war sie Ensemblemitglied am Deutschen Schauspielhaus in Hamburg, an den Münchner Kammerspielen und am Münchner Residenztheater. Ihre erste Fernsehrolle spielte sie 1963 unter der Regie von Ludwig Cremer in Spiel im Morgengrauen. Einem breiten Publikum wurde Martini vor allem durch ihre zahlreichen Fernsehauftritte bekannt, mit prominenten Rollen in Serien, wie etwa in Traumschiff, Derrick, Der Kommissar, Tatort, Kottan und Ein Fall für zwei. Stets blieb sie aber auch dem Hörfunk treu und wirkte als Sprecherin in weit über 100 Hörspielen mit.

Ab 1968 wohnte sie wieder in Wien im Helmut-Qualtinger-Hof und moderierte 17 Jahre lang im dritten Hörfunkprogramm Ö3 des Österreichischen Rundfunks (ORF) die wöchentlichen Sendungen Mittags-Martini und Martini-Cocktail. Sie war wiederholt auch Gastgeberin der Talkshow Club 2.

Nach langer Wiener Bühnenabsenz holte Felix Dvorak Martini 1997 für die Rolle der Valerie in seiner Inszenierung von Horváths Geschichten aus dem Wiener Wald am Stadttheater Berndorf. Verbunden war Martini dem Theater in der Josefstadt, zu dessen Ensemble sie bis zuletzt gehörte. Ihr 60-Jahre-Bühnenjubiläum feierte die Schauspielerin 2009 unter dem Titel Nylons, Swing und Chesterfield mit Ausschnitten aus ihrer Karriere in den Kammerspielen.

Louise Martini erhielt zahlreiche Auszeichnungen, u. a. die Goldene Kamera (1978), die Goldene Ehrenmedaille der Stadt Wien (1987), den Johann-Nestroy-Ring (1997) und das Goldene Wiener Ehrenzeichen (2006). 1998 erschien ihr Buch Ein O für Louise – Wien in den 50er Jahren.

Daneben machte sich Martini auch als Diseuse einen Namen, so zu hören auf der Schallplatte Frivolitäten – 10 Diseusen – 10 Chansons von Polydor.

Nach ihrer ersten Ehe mit dem Vibraphonisten Bill Grah war sie von 1966 bis zu dessen Tod 2004 mit dem Regisseur Heinz Wilhelm Schwarz verheiratet. Louise Martini verstarb in der Nacht vom 16. zum 17. Januar 2013. Am 4. Februar fand in der Feuerhalle Simmering die Trauerfeier statt; ihre Urne wurde im engsten Familienkreis auf dem Friedhof in Anif beigesetzt.[2]
Im Jahr 2014 wurde in Wien-Landstraße (3. Bezirk) die Louise-Martini-Straße nach ihr benannt.
