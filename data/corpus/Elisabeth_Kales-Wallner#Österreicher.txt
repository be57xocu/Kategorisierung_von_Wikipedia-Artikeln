Elisabeth Kales-Wallner (* 28. April 1951 in Waidhofen an der Ybbs, Österreich; † 23. Oktober 2005 in Wien) war eine österreichische Operettensoubrette und Kammersängerin.

Elisabeth Kales studierte am Grazer Konservatorium und wurde 1975 Mitglied der Grazer Oper. Ihr Debüt an der Wiener Volksoper gab sie am 1. Februar 1978 als „Arsena“ im Zigeunerbaron. Von 1979 an bis zu ihrem Tod blieb sie Ensemblemitglied der Wiener Volksoper, von welcher sie 1995 zur Kammersängerin ernannt wurde. 1996 übernahm sie für vier Jahre die künstlerische Leitung des Stadttheaters Baden bei Wien.

Am 23. Oktober 2005 erlag sie nach jahrelangem Leiden ihrer Krebserkrankung. Sie wurde am Sieveringer Friedhof (Abteilung 1, Gruppe 13, Nummer 5) in Wien bestattet.

Sie war mit dem Manager Leo Wallner verheiratet und hinterließ drei erwachsene Kinder.

