Margaret Munnerlyn Mitchell (* 8. November 1900 in Atlanta, Georgia; † 16. August 1949 ebenda) war eine US-amerikanische Schriftstellerin.

Für ihren 1936 erschienenen Südstaaten-Roman Vom Winde verweht (Gone with the Wind) wurde sie 1937 mit dem Pulitzer-Preis ausgezeichnet. Die gleichnamige Verfilmung aus dem Jahr 1939 mit Vivien Leigh und Clark Gable in den Hauptrollen ist einer der erfolgreichsten Filme aller Zeiten.

Margaret Mitchell wurde in Atlanta als Tochter von Mary Isabelle „Maybelle“ Stephens und dem Rechtsanwalt Eugene Muse Mitchell geboren. Ihr Spitzname, unter dem sie später auch zuweilen in der Öffentlichkeit auftrat, war Peggy. Nachdem Mitchell ihre Schul- und Collegeausbildung beendet hatte, übernahm sie nach dem frühen Tod der Mutter im Jahr 1918 den elterlichen Haushalt und begann, für die Zeitung Atlanta Journal eine wöchentliche Kolumne in der Sonntagsausgabe zu schreiben.

Am 2. September 1922 heiratete Mitchell Berrien „Red“ Upshaw, von dem sie sich 1924 wieder scheiden ließ. Am 4. Juli 1925 heiratete sie Upshaws Freund John Marsh.

Mitchell begann 1926, Vom Winde verweht zu schreiben, als sie wegen Verletzungen und Arthritis längere Zeit ans Bett gefesselt war. Ihr Mann John Marsh kaufte ihr eine gebrauchte Remington-Reiseschreibmaschine, auf der sie innerhalb der folgenden zehn Jahre das mehr als 1000 Seiten umfassende Buch fertigstellte. Der Roman erschien am 30. Juni 1936. Im gleichen Jahr verkaufte Mitchell die Filmrechte an ihrem Buch für 50.000 US-Dollar an den Produzenten David O. Selznick. 1937 wurde Mitchell für Vom Winde verweht mit dem Pulitzer-Preis ausgezeichnet.

Ab den 1940er Jahren arbeitete Mitchell ausschließlich ehrenamtlich für wohltätige Zwecke. Unter anderem sponserte sie die medizinische Ausbildung von unterprivilegierten College-Absolventen, gründete Notfallaufnahmen für Schwarze und Weiße am Grady Hospital in Atlanta, engagierte sich in der Afro-Amerikanischen Bewegung für Rechte und Ausbildung der Schwarzen in Atlanta und half während des Zweiten Weltkriegs in verschiedenen Krankenhäusern.

Am 11. August 1949 wurde Mitchell beim Überqueren der Peachtree Street von einem betrunkenen Taxifahrer angefahren; ihre Verletzungen waren so schwer, dass sie nicht mehr aus dem Koma erwachte. Margaret Mitchell starb am 16. August 1949 im Grady Hospital und ist auf dem Oakland Cemetery in Atlanta begraben.

1965 wurde Mitchell postum der „Shining Light Award“ von der Atlanta Gas Light und der Radiostation WSB für ihre „Verdienste im Sinne der Menschlichkeit“ verliehen. 1989 wurde das Margaret Mitchell House, in dem Vom Winde verweht geschrieben wurde, von Atlantas Bürgermeister zu einem der offiziellen Wahrzeichen der Stadt ernannt. 1997 wurde es für die Öffentlichkeit als Museum freigegeben.

1987 erschien das Buch Stolz und unbeugsam wie Scarlett. Margaret Mitchells Briefe an ihren Jugendfreund Allen Edee aus der Zeit zwischen 1919 und 1921. 1996 folgte das Buch Insel der verlorenen Träume, ein Roman, der 1995 zusammen mit Briefen und Fotos von Margaret Mitchell im Nachlass eines Jugendfreundes entdeckt worden war. Im Jahr 2000 erschien Before Scarlett, eine Sammlung von bis dahin unveröffentlichten Geschichten und Novellen, die Margaret Mitchell im Alter zwischen sieben und 18 Jahren geschrieben hatte.
