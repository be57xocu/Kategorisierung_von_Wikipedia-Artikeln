Jutta Limbach, geborene Jutta Ryneck (* 27. März 1934 in Berlin; † 10. September 2016 ebenda[1]), war eine deutsche Rechtswissenschaftlerin sowie Professorin an der Freien Universität Berlin und Politikerin der SPD. Von 1994 bis 2002 war sie Präsidentin des Bundesverfassungsgerichts und von 2002 bis 2008 Präsidentin des Goethe-Instituts.

Jutta Rynecks Großmutter Elfriede Ryneck war Mitglied der Weimarer Nationalversammlung und Reichstagsabgeordnete für die SPD, ihr Vater Erich Ryneck (1899–1976) war ebenfalls Sozialdemokrat und von 1946 bis 1948 Bürgermeister des Ostberliner Bezirks Pankow, bevor er mit seiner Familie nach West-Berlin umzog und sein Amt niederlegte.[2] Sie besuchte die Mädchenoberschule und war Schulsprecherin. Das Jurastudium schloss sie 1958 mit dem 1. Staatsexamen, das Referendariat 1962 mit dem 2. Staatsexamen ab. In dem Jahr trat sie der SPD bei. Von 1963 bis 1966 war sie Akademische Rätin am Fachbereich Rechtswissenschaft der Freien Universität Berlin. 1966 wurde sie dort mit einer Arbeit über die Theorie und Wirklichkeit der GmbH zum Doktor der Rechte promoviert. Ihr Doktorvater war der Jurist und Rechtssoziologe Ernst Eduard Hirsch.[3] Im Jahr 1970[4] wurde sie Mutter von Benjamin Limbach, welcher nunmehr selbst Aufgaben in der Justiz als Direktor der Fachhochschule für Rechtspflege Nordrhein-Westfalen und Leiter des Ausbildungszentrums der Justiz NRW aufgenommen hat.[5] Kurz darauf folgte 1971 die Habilitation mit einer Arbeit über Das gesellschaftliche Handeln, Denken und Wissen im Richterspruch.

1972 nahm Limbach einen Ruf auf eine Professur für Zivilrecht an der Freien Universität Berlin an. 1982 war sie Gastprofessorin in Bremen. Sie war unter anderem im Beirat des Vereins Kontakte-Контакты e. V. – Verein für Kontakte zu Ländern der ehemaligen Sowjetunion (Berlin) ehrenamtlich tätig.

Sie war mit dem Juristen Peter Limbach verheiratet und hatte drei Kinder.

In den Jahren 1987 bis 1989 gehörte sie als Mitglied dem Wissenschaftlichen Beirat für Familienfragen beim Bundesministerium für Familie, Senioren, Frauen und Jugend an. Ab 1987 war Limbach Vorstandsmitglied der Gesellschaft für Gesetzgebung und zuletzt Mitglied des Beirats. In den Jahren 1992 und 1993 war sie Mitglied der Gemeinsamen Verfassungskommission des Bundesrats und Deutschen Bundestages.

Nach dem Wahlsieg von Walter Momper bei der Wahl zum Abgeordnetenhaus von Berlin 1989 wurde sie zur Senatorin für Justiz in Berlin berufen. Dieses Amt hatte sie bis 1994 inne. Gleich nach ihrem Amtsantritt musste sie sich mit Hungerstreiks von inhaftierten Terroristen der Roten Armee Fraktion auseinandersetzen. Durch ihre Position der Verständigung – sie traf zwei inhaftierte Frauen zum Gespräch – trug sie maßgeblich zur Beilegung bei.[6] Nach der Wende und friedlichen Revolution in der DDR hatte sie die Aufsicht über die Strafverfolgung der früheren DDR-Staatsspitze wegen des Schießbefehls an der innerdeutschen Grenze.[7]
Im März 1994 wurde sie zunächst zur Vizepräsidentin des Bundesverfassungsgerichts und Vorsitzenden des Zweiten Senats berufen; noch im selben Jahr wurde sie vom Bundestag[8] als Nachfolgerin von Roman Herzog zur Präsidentin des Gerichts ernannt. An der Spitze des Bundesverfassungsgerichts stand sie bis zum Erreichen der Altersgrenze 2002.

Von 2002 bis 2008 war sie Präsidentin des Goethe-Instituts.

Seit 2003 war sie Vorsitzende der „Beratenden Kommission im Zusammenhang mit der Rückgabe NS-verfolgungsbedingt entzogener Kulturgüter, insbesondere aus jüdischem Besitz“, auch Limbach-Kommission genannt, die sich als staatliche Institution mit Raubkunst und deren Rückgabe an die Erben befasst.[9]
Sie war Mitglied im Stiftungsrat des Friedenspreises des Deutschen Buchhandels und bis 2007 Vorsitzende des Deutschen Sprachrats, in dessen Auftrag sie das Buch „Ausgewanderte Wörter“ herausgegeben hat. Seit 2009 war Limbach die Vorsitzende des Medienrats der Medienanstalt Berlin-Brandenburg.[10]
Im Juli 2007 wurde Jutta Limbach für sechs Jahre in den Universitätsrat der Ernst-Moritz-Arndt-Universität Greifswald gewählt, seit 2011 war sie Vorsitzende des Hochschulrates der Kunsthochschule Berlin-Weißensee.

Hermann Höpker-Aschoff (1951–1954) |
Josef Wintrich (1954–1958) |
Gebhard Müller (1959–1971) |
Ernst Benda (1971–1983) |
Wolfgang Zeidler (1983–1987) |
Roman Herzog (1987–1994) |
Jutta Limbach (1994–2002) |
Hans-Jürgen Papier (2002–2010) |
Andreas Voßkuhle (seit 2010)

Valentin Kielinger (CDU) |
Wolfgang Kirsch (FDP) |
Hans-Günter Hoppe (FDP) |
Horst Korber (SPD) |
Hermann Oxfort (FDP) |
Jürgen Baumann (FDP) |
Gerhard Moritz Meyer (FDP) |
Rupert Scholz (CDU) |
Hermann Oxfort (FDP) |
Rupert Scholz (CDU) |
Ludwig Rehlinger (CDU) |
Jutta Limbach (SPD) |
Lore Maria Peschel-Gutzeit (SPD) |
Ehrhart Körting (SPD) |
Eberhard Diepgen (CDU) |
Wolfgang Wieland (Bündnis 90/Die Grünen) |
Karin Schubert (SPD) |
Gisela von der Aue (SPD) |
Michael Braun (CDU) |
Thomas Heilmann (CDU) |
Dirk Behrendt (Bündnis 90/Die Grünen)

Kurt Magnus (1951–1962) |
Max Grasmann (1962–1963) |
Peter Pfeiffer (1963–1971) |
Hans-Heinrich Herwarth von Bittenfeld (1971–1977) |
Klaus von Bismarck (1977–1989) |
Hans Heigert (1989–1993) |
Hilmar Hoffmann (1993–2001) |
Jutta Limbach (2002–2008) |
Klaus-Dieter Lehmann (seit 2008)
