Barry B. Longyear (* 12. Mai 1942 in Harrisburg, Pennsylvania) ist ein amerikanischer Science-Fiction-Autor.

Longyear studierte in Detroit und wurde 1977 freiberuflicher Autor, der sich der Science-Fiction zuwandte. Außer unter seinem Namen veröffentlichte er auch unter den Pseudonymen Mark Ringdahl und Frederik Longbeard.

Bekannt wurde Longyear mit der 1979 erschienenen Erzählung Enemy Mine (dt. „Du, mein Feind“), die alle wichtigen Science-Fiction-Preise (Nebula-, Hugo- und Locus Award) gewann. Gleichzeitig erhielt Longyear den John W. Campbell Best New Writer Award als bester neuer Autor des Jahres. Die Erzählung schildert das zwangsweise Zusammenleben zweier Soldaten verfeindeter Rassen, das schließlich zu Freundschaft und Verantwortungsbewusstsein dem anderen gegenüber führt. Die Story wurde 1985 durch Wolfgang Petersen als Enemy Mine – Geliebter Feind verfilmt. Später erschienen drei weitere Erzählungen, die die Handlung fortsetzten.

Longyears Romane und Erzählungen zeichnen sich durch die ihm eigene Dramaturgie, das einfühlsame Schildern seiner Protagonisten und sein humanistisches Gedankengut, gepaart mit Einfühlungsvermögen und Anteilnahme, aus.
