Jean Antoine Dubois, 'Abbé Dubois', (getauft 10. Januar 1766 in Saint-Remèze, Ardèche; † 17. Februar 1848 in Paris), französischer Missionar und Indologe.

Dubois ging in den Wirren der Französischen Revolution 1792 als Priester nach Südindien, wo er 1794 im Auftrag der neuen britischen Herren nach der Niederlage Tippu Sultans die von diesem zwangsbekehrten Christen betreute. Aufgrund der Priesterverfolgungen im französischen Pondichéry nach Mysore ausgewichen, nahm er in den Jahren 1803/04 mit Erfolg Pockenschutzimpfungen vor, engagierte sich beim Bau zahlreicher Kirchen und errichtete eine landwirtschaftliche Versuchskolonie in Sathally. 

Der gute Kontakt zu den Briten - Dubois gehörte zu den wenigen, die die englische Sprache beherrschten - vermittelte ihm den Verkauf seines Buchmanuskripts über "Leben und Riten der Inder" für einen bedeutenden Geldbetrag, und von den dringendsten Existenzsorgen befreit, widmete sich Dubois der Gemeindearbeit, vor allem in Karnataka. Seine indischen Gläubigen missbilligten allerdings den Kontakt zu den als unrein angesehenen feringhis (Engländern), mit denen ihn bis zu seiner Rückkehr nach Frankreich gegenseitige Achtung und Anerkennung verband.

Zum Abschied sammelt die englische Kolonie in Chennai (Madras) für ein von Thomas Hickey verfertigtes Bild, das Dubois in seiner typischen Tracht als christlichen Sannyasin zeigt; es handelt sich dabei um das letzte Werk des über achtzigjährigen britischen Malers[1].

1823 zu den Missions Étrangères in Paris zurückbeordert, verfasste er dort eine missionsskeptische Schrift, die mehrere Entgegnungen von katholischer und protestantischer Seite hervorrief und als Klassiker der Missionskritik gilt. 

Bei seinen Vorgesetzten galt Dubois wegen seiner missionarischen Skepsis und pessimistischen Weltsicht als "wenig standhaft".

1825 veröffentlichte der Abbé weitere religionswissenschaftliche Arbeiten und 1826 eine Übersetzung des Panchatantra in der südindischen Fassung. In den letzten Lebensjahren war Dubois Superior (Leiter) der Missions Étrangères (1836–1839); er starb 1848 in Paris. Sein Vermögen vermachte er der Mission und seiner französischen Heimatgemeinde Saint-Remèze für Schulzwecke. - Ein Museum und mehrere von ihm errichtete Kirchen erinnern bis heute in Indien an diesen Pionier der frühen Indologie.

Dubois beschrieb in seinem Hauptwerk, den Hindu life and manners, anders als frühere Autoren  (Baldaeus, Rogerius) die Kasten-, Religions- und Lebensverhältnisse in Südindien nicht nur unter religiösen, sondern auch unter ethnologisch-soziologischen Aspekten. Das von vielen Anekdoten und Sentenzen durchzogene Hauptwerk, das im Kern auf ein Manuskript des Jesuitenpaters Gaston-Laurent Cœurdoux (1691–1779) zurückgeht, ermöglichte trotz vieler Schwächen der Darstellung und Deutung erstmals einen lebendigen Einblick in Brauchtum und Alltag des bis dahin weitgehend unbekannten Subkontinents. 

Dubois' Werk zählt wegen seines umfassenden wie detailreichen Überblicks bis heute zur Standardlektüre der Indologen.
