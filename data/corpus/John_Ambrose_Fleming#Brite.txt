Sir John Ambrose Fleming (* 29. November 1849 in Lancaster, Lancashire; † 18. April 1945 in Sidmouth, Devon), manchmal auch Ambrose J. Fleming genannt, war ein britischer Elektroingenieur und Physiker. Seine Eltern waren James und Mary Anne Fleming, John hatte noch sechs jüngere Geschwister.

John Ambrose Fleming wurde auf der University College School in London und dem University College London ausgebildet. Er arbeitete als Dozent an verschiedenen Universitäten, darunter die Universität Cambridge, die Universität Nottingham und das University College London. Fleming war Berater der Unternehmen Marconi Wireless Telegraph Company, Swan, Ferranti, Edison Telephone und später der Edison Electric Light Company. 1892 legte er der Institution of Electrical Engineers in London eine wichtige Arbeit zur Theorie des Transformators vor.

Am 16. November 1904 beantragte Fleming in England ein Patent mit dem Titel „Improvements in Instruments for Detecting and Measuring Alternating Electric Currents“, in dem er einen „Zweielektroden-Funkgleichrichter“ beschrieb. Auf der Suche nach einem besseren Detektor für Radiowellen hatte er entdeckt, dass der Edison-Effekt zur Detektion von Signalen genutzt werden konnte.

Er selbst nannte seine Erfindung Oscillation Valve („Schwingungsventil“). Das Patent mit der Nummer GB190424850 wurde ihm am 21. September 1905 zugesprochen. Seine Erfindung war auch unter den Namen Kenotron, Elektronenventil, Vacuumdiode, Elektronenröhre oder Flemingventil bekannt. Vielfach wird diese Erfindung als der Anfang des Elektronikzeitalters betrachtet.

1906 entwickelte der amerikanische Erfinder Lee de Forest daraus einen als Audion (Triode) bezeichneten, verstärkenden Hochfrequenz-Röhrendetektor, indem er ein Steuergitter als dritte Elektrode hinzufügte. Das führte dazu, dass Fleming ihn der Nachahmung bezichtigte. De Forests Triode wurde später zur Verstärkung elektrischer Signale eingesetzt.

Fleming leistete Beiträge auf den Gebieten der Photometrie, der drahtlosen Telegraphie und der elektrischen Messtechnik. 1929 wurde er zum Ritter geschlagen und 1933 erhielt der die Ehrenmedaille (IRE  Medal of Honor) des Institute of Radio Engineers (IRE).
