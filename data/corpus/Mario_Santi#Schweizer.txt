Mario Santi (* 1941; † 16. Oktober 2004 in Kenia) war ein Schweizer Sportmoderator. Er wurde in den 1970er- und frühen 1980er-Jahren durch seine Fussballreportagen im Schweizer Radio DRS einem breiten Publikum bekannt.

Nach einer Ausbildung als Bahnbeamter ging Santi 1966 als Reiseleiter von Kuoni nach Kenia, von 1969 bis 1984 war er Verkaufsdirektor des Reisekonzerns und nebenberuflich als Fussballreporter für das Radio im Einsatz. 1984 wechselte er zum Schweizer Fernsehen in die Sportabteilung.

Er starb an den Folgen eines Hirnschlags während seines Urlaubs in Kenia.[1]