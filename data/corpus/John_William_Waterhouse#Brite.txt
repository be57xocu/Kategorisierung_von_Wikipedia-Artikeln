John William Waterhouse RA (* 6. April 1849 in Rom; † 10. Februar 1917 in London) war ein britischer Maler, der sowohl dem Akademischen Realismus als auch der Gruppe der Präraffaeliten zugerechnet wird.

Waterhouse wurde in Rom als Sohn des Malers William Waterhouse und dessen Ehefrau, der Malerin Isabella geboren. Von der Familie und seinen Freunden wurde er „Nino“ genannt, das Diminutiv des italienischen Vornamens Giovanino. 1854 siedelte seine Familie nach London über. 1857 starben seine Mutter und sein Bruder an Tuberkulose. Sein Vater heiratete 1860 erneut Frederica Mary Jane Perceval, die Enkeltochter des bei einem Attentat ermordeten Britischen Premierministers, Spencer Perceval. Nachdem er zuerst von seinem Vater unterrichtet worden war, wurde er 1871 – nach mehreren vergeblichen Versuchen – Student der Bildhauerei (Sculpture) an den Schulen der Royal Academy[1]. Bereits 1872 stellte Waterhouse in der Royal Society of British Artists aus. Man nimmt an, dass seine Schwester Jessica eines seiner frühesten Modelle war. Im Jahre 1874, im Alter von fünfundzwanzig, reichte Waterhouse die klassische Allegorie „Schlaf und sein Halbbruder Tod“[2] für die Sommerausstellung der Royal Academy of Arts ein. 

Waterhouse war Nachfolger der Generation von Malern, die z. B. von Frederic Leighton, George Frederic Watts, Edward Burne-Jones und Lawrence Alma-Tadema dominiert worden war. Er nahm seine Inspiration von vielen dieser großen viktorianischen Maler und entwickelte daraus einen eigenen Stil, in dem Klassizismus, Romantik, Fantasie und Wirklichkeit miteinander verwoben sind. Er war weniger bedacht auf die Feinheit in Einzelheiten als seine viktorianischen Vorfahren, während er den Präraffaeliten seine Themen und den Reichtum an Farben schuldete, nicht aber den Grad der Vollendung. Einige der Landschaften in seinen Gemälden ähneln denen der Impressionisten.

Bereits seine frühen Werke wurden ausgestellt in den Räumen der Royal Academy of Arts, der Royal Society of British Artists, des Royal Institute of Painters in Water Colours und der Dudley Gallery.

Waterhouse liebte Italien und verbrachte viel Zeit dort zwischen 1876 und 1883, wo er Genrebilder malte.

1883 heiratete er in der Kirche von St. Micheals in Ealing Esther Kenworthy, Tochter eines Lehrers an der Kunstschule. [3] Es wurden 2 Kinder geboren, die jedoch beide früh starben. 1877 hatte der Architekt Alfred Healy eine Reihe von Häusern mit Studios in Primrose Hill für 12 Künstler gebaut. Waterhouse zog dort mit seiner Frau ein, wo er sich mit Mitgliedern der Newlyn School assoziierte, die von Frankreich beeinflusst war. 

Obwohl er hauptsächlich in Öl malte wurde er 1883 in die Royal Institute of Painters in Water Colours gewählt, trat aber 1889 zurück. 

Er verfolgte den konventionellen Weg zum Erfolg, indem er am 4. Juni 1885 zum ARA (Associate) der Royal Academy gewählt wurde und 10 Jahre später am 16. Mai 1895 zum Vollmitglied RA (Royal Academician). Seine Diplomarbeit für den RA war “A Mermaid” (Eine Meerjungfrau). Weil das Gemälde erst 1900 fertiggestellt war, bot Waterhouse seine „Ophelia“ von 1888 als temporäre Einreichung der Royal Academy an.[4]
1899 hatte der Burenkrieg in Südafrika begonnen und im Frühjahr 1900 gaben 350 Künstler ihre Werke zur Unterstützung britischer Truppen an den  „Artists' War Fund“, der eigens dafür gegründet worden war und  Waterhouse zu  den primären Stiftern gehörte. Er schuf hierfür das Gemälde „Schicksal“.  Die dem Kriegsfonds angebotenen Bilder wurden von dem Auktionshaus Christies versteigert und der Erlös an den Kriegsfond überwiesen. [5]
Als sein berühmtestes Bild gilt The Lady of Shalott (Die Dame von Shalott), das von Sir Henry Tate 1888 gekauft wurde; die erste der drei Versionen ist 1888 entstanden. Dazu wurde er durch das gleichnamige Gedicht von Alfred Lord Tennyson inspiriert.

Im Jahr 1901 zog er nach St Johns Wood und schloss sich dem St. Johns Wood Arts Club an, einer sozialen Organisation, der u. a. Lawrence Alma-Tadema und George Clausen (1852–1944) angehörten.[6]
Eine seiner letzten Arbeiten war The Enchanted Garden, unvollendet gelassen auf seiner Staffelei bei seinem Tod,[7] 
und befindet sich jetzt in der Sammlung der „Lady Lever Art Gallery“ in Liverpool.

Im Alter von nahezu 68 Jahren starb John William Waterhouse am 10. Februar 1917 an Krebs in seinem Haus in St. John’s Wood, London. Seine letzte Ruhestätte fand er auf dem Kensal Green Cemetery in London. Seine Ehefrau Esther überlebte ihn um 27 Jahre und starb 1944. Sie wurde neben ihm begraben.

„Über die Ausgeburten von Waterhouse urteilten die Kritiker mal euphorisch, mal vernichtend. Ein Kritiker maulte, das Personal seiner Bilder entstamme weder „Träumen noch dem Tageslicht“; ein anderer schwärmte, die Kunst von Waterhouse lebe „in einer selbsterschaffenen Welt“. In einem Punkt waren sich also alle einig: Der 1847 in Rom geborene Maler hatte eine Welt geschaffen, die sich weder dem Phantastischen noch der Wirklichkeit zuordnen ließ. In seiner Malerei erhielten Hexen, Mischwesen und Märchenfiguren ein neues Zuhause. Er war ein Meister darin, dem Außerordentlichen eine beiläufige, selbstverständliche Gestalt zu geben.“

Dolce far niente (1880)

Eurydike (1880)

Diogenes (1882)

The Favorites of the Emperor Honorius (1883)

Magic Circle (1886)

The Lady of Shalott (1888)

Circe Invidiosa (1892)

La Belle Dame sans Merci (1893)

Cäcilia von Rom (1895)

Hylas und die Nymphen (1896)

Flora und die Zephyre (1898)

Die Meerjungfrau (1900)

Echo and Narcissus (1903)

Die Danaiden (1903)

Boreas (1903)

Decamerone (1916)

Miranda – The Tempest (1916), Inspiration für das Musikvideo Frozen von Madonna
