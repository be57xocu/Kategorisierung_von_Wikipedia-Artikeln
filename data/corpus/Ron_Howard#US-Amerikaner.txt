Ronald William Howard (* 1. März 1954 in Duncan, Oklahoma) ist ein US-amerikanischer Schauspieler, Regisseur und Filmproduzent sowie zweifacher Oscarpreisträger.

Ron Howards Vater Rance Howard, ein bekannter Schauspieler, brachte ihn und seinen Bruder Clint Howard häufig in Kinderrollen unter. Jahrelang wurde er als Ronny Howard geführt. Seit früher Jugend interessierte ihn das Filmemachen. So drehte er mit Vater und Geschwistern als Schauspielern kleinere Filme, die zum Teil schon Auszeichnungen erhielten.

Howard spielte recht früh in der Fernsehserie Andy Griffith Show den Opie Taylor mit Andy Griffith (geführt als Ronny Howard). Im Kino hatte Howard seinen ersten Erfolg 1973 als Schauspieler in der Rolle des „Steve Bollander“ in George Lucas' Kultfilm American Graffiti. Danach spielte er jahrelang in der erfolgreichen Sitcom Happy Days. Dazwischen hatte er 1976 eine tragende Nebenrolle in dem Western Der letzte Scharfschütze, der gleichzeitig der letzte Film mit dem legendären US-Schauspieler John Wayne war. 

Seine Karriere als Regisseur begann 1977 mit der Arbeit an dem Low-Budget-Film Highway 101. Anschließend arbeitete er einige Jahre als Regisseur bei Fernsehserien. Anfang der 1980er-Jahre schaffte Howard den Durchbruch in Hollywood und führte seitdem Regie bei vielen beachteten Produktionen. 1986 gründete er zusammen mit Filmproduzent Brian Grazer die Produktionsfirma Imagine Entertainment, mit welcher er fortan seine Filme produziert. Publikumserfolge und filmtechnische Glanzleistungen wie Splash, Willow und Backdraft begründeten seinen aktuellen Ruf als großer Hollywood-Regisseur. Bisheriger Höhepunkt seiner Laufbahn war 2002 der Gewinn von zwei Oscars in den Kategorien „Beste Regie“ und „Bester Film“ für A Beautiful Mind – Genie und Wahnsinn. Mit The Missing legte er einen künstlerisch beachtlichen Versuch einer Neusicht des Western-Genres vor, der zugleich realistisch und beunruhigend wirkt. Sein Bruder Clint Howard ist häufig in einer Nebenrolle in seinen Filmen zu sehen. 2008 zeichnete Howard für die Verfilmung von Peter Morgans Theaterstück Frost/Nixon verantwortlich mit Frank Langella in der Rolle des Richard Nixon und Michael Sheen als Fernsehmoderator David Frost. Das Drama brachte ihm erneut Regie-Nominierungen für den Golden Globe Award und Oscar ein. Weitere Filmprojekte folgten, darunter auch weitere Literaturverfilmungen.

Im Juni 2017 übernahm er inmitten der Dreharbeiten die Regie von Solo: A Star Wars Story, der 2018 in die Kinos kommen soll.[1]
Howard ist seit 1975 mit der Schriftstellerin Cheryl Howard verheiratet, zusammen haben sie vier Kinder. Seine Tochter Bryce Dallas ist ebenfalls Filmschauspielerin.

Seit dem Film Nightshift – Das Leichenhaus flippt völlig aus arbeitet er beinahe ausschließlich mit dem Filmeditor Mike Hill und dessen Kollegen Daniel P. Hanley zusammen. Als regelmäßiger Kameramann kommt seit 2003 Salvatore Totino zum Einsatz.

In der 4. Staffel der Fernsehserie Arrested Development spielt Howard sich selbst.

Hinzu kommt die Jugendserie Happy Days von 1974 bis 1980 und eine Reihe von Cameo-Auftritten in seinen Regie-Filmen sowie mehrere Auftritte in der Zeichentrickserie Die Simpsons.

Nominierungen:

Nominierungen:

Nominierungen:
