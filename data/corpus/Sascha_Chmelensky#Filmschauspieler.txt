Sascha Chmelensky (* 9. Januar 1993 in Berlin) ist ein deutscher Schauspieler. 

In der vom KIKA produzierten Serie Allein gegen die Zeit bekam Chmelensky mit Lennart „Lenny“ Merz seine bisher größte Rolle.

Chmelensky ist der Bruder des Musikers Dennis Chmelensky.
