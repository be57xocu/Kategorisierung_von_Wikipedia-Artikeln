Rudolf Paul (* 30. Juli 1893 in Gera; † 28. Februar 1978 in Frankfurt am Main) war Jurist, Politiker (DDP, später SED), 1945 Oberbürgermeister der Stadt Gera und von 1945 bis 1947 Landespräsident (Ministerpräsident) von Thüringen.

Rudolf Paul studierte von 1913 bis 1914 in Berlin und Leipzig Staats- und Rechtswissenschaften. Er leistete im Ersten Weltkrieg seinen Militärdienst, setzte 1919 sein Studium in Jena fort und war nach seiner Promotion zunächst Referendar und bis 1923 Gerichtsassessor. 1924 wurde er Staatsanwaltrat in Gera.

In Ausübung seines Amtes beabsichtigte er, Hitler festzusetzen und als „lästigen Ausländer“ abschieben zu lassen. Dazu benötigte er einen Strafantrag des Reichstages, der jedoch vom Reichspräsidenten Friedrich Ebert mit folgenden Worten abgelehnt wurde:

„Ich danke Ihnen, dass Sie das Interesse hatten, das Ansehen des Reichstages zu schützen, jedoch der Dreck kommt nicht bis an meine Sohlen.“[1]
Daraufhin gab er sein Amt als Staatsanwaltrat auf und war von 1924 bis 1934 als Rechtsanwalt und Notar in Gera tätig. In dieser Zeit war er Mitglied der DDP und deren Vorsitzender in Ostthüringen. Nach erteiltem Berufsverbot und Entzug der Rechtsanwaltserlaubnis durch die Nationalsozialisten ging er als Landwirt nach Ulrichswalde bei Stadtroda. 1938 ließ er sich von seiner Frau Lilly, geborene Biermann und Tochter des bekannten Geraer Kaufhausbesitzers Max Biermann, scheiden. Lilly Paul zog nach Berlin, wurde 1942 wegen ihrer jüdischen Herkunft deportiert und starb am 1. Januar 1945 im KZ Stutthof.

Am 7. Mai 1945 wurde Paul vom amerikanischen Stadtkommandanten und Militär-Gouverneur zum Geraer Oberbürgermeister ernannt, zusätzlich wurden ihm alle Behörden unterstellt. Bis zum Juli 1945 wirkte er maßgeblich an der Neuordnung der Stadt Gera mit. Für kurze Zeit fungierte er auch als stellvertretender Vorsitzender der noch nicht wieder zugelassenen DDP im „Thüringen-Ausschuss“, dem vorparlamentarischen Beratungs- und Kontrollgremium unter US-amerikanischer Besatzung.

Am 16. Juli 1945 wurde er nach der kurzen Amtszeit von Hermann Brill in der nunmehrigen Sowjetischen Besatzungszone zum Landespräsidenten des Landes Thüringen gewählt und am 14. August 1945 auf Befehl von Marschall Schukow zum Ministerpräsidenten ernannt.[2] Profilierte Mitglieder dieses provisorischen Kabinetts waren der altkommunistische KPD/SED-Politiker Ernst Busse, 1. Vizepräsident und zuständig für Polizei, Bodenreform und Entnazifizierung, sowie der CDU-Politiker Hans Lukaschek, 3. Vizepräsident und Chef des Landesamtes für Land- und Forstwirtschaft. Lukaschek ersetzte Max Kolter von der CDU, der als Gegner der Bodenreform durch die SMAD abgesetzt, verhaftet und in Haft umgebracht wurde.

Paul war ein überzeugter Befürworter der so genannten Bodenreform in der sowjetischen Besatzungszone. Nach seiner Überzeugung durfte angesichts der ungleich verteilten Kriegsschäden in Deutschland durch Bombenkrieg, Vertreibung usw. das zufällig erhalten gebliebene Privateigentum nicht unantastbar sein. In diesem Sinne verfügte er Ende 1946 auch eine für die gesamte SBZ beispielgebende sozialpolitische Gesetzgebung für Vertriebene (in der SBZ Umsiedler genannt) zur Umverteilung überschüssigen Hausrats und zur Verpachtung von Gartenparzellen. Deren Erfolge blieben jedoch aufgrund der Widerstände in der alteingesessenen Bevölkerung begrenzt.

1946 erreichte Paul bei der Sowjetischen Militäradministration die Rückgabe der Bestände des Nietzsche-Archivs, die bereits verpackt und für den Abtransport vorbereitet waren.

Rudolf Paul bekannte sich zur neu formierten Demokratischen Partei Thüringens (der späteren LDP), ohne ihr offiziell beizutreten.[2] Auf ihrem Thüringer Gründungsparteitag im April 1946 trat Paul der SED bei, um sich parteipolitisch besser abzusichern. Bei der Landtagswahl 1946 wurde er für die SED in den Thüringer Landtag gewählt.
Von den Altkommunisten in der SED beargwöhnt, war er jedoch weiterhin maßgeblich vom Vertrauen der sowjetischen Besatzungsmacht abhängig. Nach Konstituierung des im Herbst 1946 gewählten neuen Thüringer Landtages wurde er im Januar 1947 zum Ministerpräsidenten des Landes Thüringen gewählt, erhielt jedoch im April 1947 mit dem SED-Landesvorsitzenden Werner Eggerath einen altkommunistischen Stellvertreter und „Aufpasser“. Sein Handlungsspielraum wurde in der Folgezeit immer mehr eingeschränkt. Im Juni 1947 nahm Paul an der Münchener Ministerpräsidentenkonferenz teil, die jedoch scheiterte.

Am 1. September 1947 flüchtete er über Berlin-West in die amerikanische Besatzungszone, seines Amtes wurde er offiziell am 9. Oktober 1947 enthoben und durch Eggerath ersetzt.

Er war über 30 Jahre lang als Rechtsanwalt in Frankfurt am Main tätig und Mitbegründer der bedeutenden Anwaltskanzlei Knauthe-Paul-Schmitt.

Robert Fürbringer |
Wilhelm Weber |
Constantin Sorger |
Robert Fischer |
Kurt Alwin Lade |
Ludwig Ernst Huhn |
Kurt Herrfurth |
Walter Arnold |
Walter Kießling |
Otto Zinn |
Rudolf Paul |
Friedrich Giessner |
Friedrich Bloch |
Curt Böhme |
Otto Aßmann |
Willi Weber |
Horst Pohl |
Horst Jäger |
Michael Galley |
Andreas Mitzenheim |
Ralf Rauch |
Norbert Vornehm |
Viola Hahn

1920–1933: 
Arnold Paulssen |
August Frölich |
Richard Leutheußer |
Arnold Paulssen |
Erwin Baum |
Fritz Sauckel
1933–1945: 
Willy Marschler
1945–1952: 
Hermann Brill |
Rudolf Paul |
Werner Eggerath
seit 1990:
Josef Duchač |
Bernhard Vogel |
Dieter Althaus |
Christine Lieberknecht |
Bodo Ramelow
