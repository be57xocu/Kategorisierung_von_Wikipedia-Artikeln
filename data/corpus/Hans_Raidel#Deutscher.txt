Hans Raidel (* 11. Juli 1941 in Lechnitz/Siebenbürgen) ist ein deutscher Politiker (CSU). Er war Leiter der deutschen Delegation zur Euromediterranen Parlamentarischen Versammlung.

Nach der Mittlere Reife machte Raidel zunächst eine Ausbildung in der Verwaltung und besuchte später die Verwaltungs- und Wirtschaftsakademie, die er als Diplom-Verwaltungswirt (FH) beendete. 

Hans Raidel ist verheiratet und hat eine Tochter.

Raidel gehörte von 1974 bis 1990 dem Vorstand des Bezirksverbandes der CSU Schwaben und des Evangelischen Arbeitskreises der CDU/CSU an.

Raidel ist seit 1972 Mitglied des Kreistages des Landkreises Donau-Ries und gehörte von 1974 bis 1990 auch dem Bezirkstag des Bezirks Schwaben an.

Er war von 1990 bis 2009 Mitglied des Deutschen Bundestages und leitete ab 2005 die deutsche Delegation zur Euromediterranen Parlamentarischen Versammlung. Außerdem gehörte Raidel der Interparlamentarischen Union und der Parlamentarischen Versammlung der OSZE an.

Hans Raidel war stets als direkt gewählter Abgeordneter des Wahlkreises Donau-Ries in den Bundestag eingezogen.

Hans Raidel ist Mitglied des Unterausschusses Abrüstung, Rüstungskontrolle und Nichtverbreitung des Deutschen Bundestages. 

In der konservativen Zeitung Junge Freiheit sprach Raidel im April 2003 vom
„Ausbluten der deutschen Rüstungsindustrie“. Auf die Frage, ob es einer Regierungsinitiative zum Schutz des deutschen Rüstungswissens bedarf, antwortete Raidel:

„Natürlich, das Problem dabei ist nur, dass solche Eingriffe im System der freien Marktwirtschaft schwierig sind. Wir müssen also anders ansetzen: Wann ist eine Firma gezwungen, sogar ihr Know-how zu veräußern? Wenn sie nicht mehr werthaltig ist! Das heißt für Deutschland, durch Förderung von Forschung und Entwicklung, aber auch durch entsprechende Produktabnahme unseren wehrtechnischen Firmen das nötige Polster zu verschaffen. Tatsächlich aber läuft die deutsche Rüstungsindustrie Gefahr, auszubluten. Obendrein heißt es dann künftig für die Bundeswehr: ‚Buy American!‘ – ‚Kaufe amerikanisch!‘“[1]
Im September 2006 brachte Hans Raidel im Bundestag einen Antrag ein unter der Überschrift: Gefährliche Streumunition verbieten – Das humanitäre Völkerrecht weiter entwickeln. Inzwischen sieht er seine Wortwahl kritisch:

„Diese Wortwahl ist natürlich schon zwiespältig, das muss man einräumen, weil entweder ist Streumunition gefährlich, oder sie ist nicht gefährlich.“

Seit März 2010 ist er Mitglied des Beirats für Fragen der Inneren Führung.
