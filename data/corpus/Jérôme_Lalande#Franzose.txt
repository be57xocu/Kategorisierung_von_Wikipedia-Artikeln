Joseph Jérôme Lefrançais de Lalande (* 11. Juli 1732 in Bourg-en-Bresse, Frankreich; † 4. April 1807 in Paris) war ein französischer Mathematiker und Astronom in der Zeit der Aufklärung und der Französischen Revolution. In der Revolution ließ er das Adelsprädikat „de“ fallen.

Lalande besuchte eine Jesuitenschule und begann anschließend ein Studium der Rechtswissenschaft. Nachdem er in Paris den Astronomen Joseph Nicolas Delisle kennenlernt hatte, studierte er bei ihm Astronomie und theoretische Physik bei Pierre Lemonnier.

Trotz engagierter Beobachtertätigkeit als Delisles Assistent schloss er 1751 das Rechtsstudium ab und praktizierte in seiner Heimat Bourg-en-Bresse. Nach anregenden Kontakten zu Pierre-Louis Moreau de Maupertuis und Leonhard Euler in Berlin publizierte Lalande einige Reduktionen seiner Messungen in den Mitteilungen der Akademien von Berlin und Paris. Seine Arbeiten führten zur Ernennung als Auswärtiges Mitglied der Preußischen Akademie der Wissenschaften[1] und 1752 zum Direktor der Berliner Sternwarte. 1753 wurde er in die Académie des Sciences gewählt. Danach kam es zu einem Streit mit seinem Lehrer Lemonnier über die korrekte Berechnung der Abplattung der Erde, die für die Berechnung der Mondbahn (Äquatorial-Horizontalparallaxe) gebraucht wurde. Eine Kommission der Pariser Akademie entschied zugunsten Lalandes. Für diesen war das jedoch nur ein „Pyrrhussieg“, da sein Verhältnis zu seinem Lehrer Lemonnier danach erkaltete. 1764 wurde Lalande zum auswärtigen Mitglied der Göttinger Akademie der Wissenschaften gewählt.[2]
Lalande arbeitete danach als Assistent von Alexis-Claude Clairaut an einer besseren Bahnberechnung des Halleyschen Kometen, wobei er sich wie Clairaut, d'Alembert und Euler mit dem Dreikörperproblem beschäftigte. Mit Methoden Clairauts konnte Lalande erfolgreich die Bahnstörungen des Kometen durch große Planeten berechnen. Unterstützt wurde er in der umfangreichen Rechenarbeit durch Nicole-Reine Lépaute (1723–1788), deren Beitrag wie der anderer damaliger Mathematikerinnen, die für ihre männlichen Kollegen „Rechenarbeit“ ausführen durften, weitgehend ignoriert wurde. Lalande würdigte ihre Arbeit so:

Sechs Monate lang rechneten wir von morgens bis nachts .. Die Hilfe Mme. Lépautes war so, daß ich ohne sie die enorme Arbeit überhaupt nicht hätte in Angriff nehmen können. Es war notwendig, die Distanz der beiden Planeten Jupiter und Saturn zum Kometen separat für jeden aufeinanderfolgenden Grad über 150 Jahre hinweg zu berechnen.

Auch später kooperierte Lalande häufig mit Mathematikerinnen. Lalandes Lebensgefährtin Louise du Pierry[3] (geboren 1746) wurde später die erste Astronomie-Professorin.

Für „Halley“ berechnete er wegen der ungewöhnlich starken Bahnstörungen durch Jupiter voraus, dass der Komet bei seiner nächsten Annäherung an die Erdbahn einige Monate später kommen würde als angenommen. Charles Messier hatte den für 1757 erwarteten Kometen noch vergeblich gesucht; er wurde Ende 1758 von Johann Georg Palitzsch erstmals gesichtet und das Perihel im März 1759 stimmte auf vier Wochen genau – was einen großen Erfolg für die neuen störungstheoretischen Methoden darstellte. Lalandes Vorhersage wurde von den Zeitgenossen als Triumph des hauptsächlich mechanistischen Weltbildes der Aufklärung betrachtet.

Heute weiß man durch andere Methoden (vor allem dank der durch Computer möglichen Numerischen Integration kleiner Schritte), dass Halleys Umlaufzeit sich nicht nur damals um ein Jahr veränderte: ihr Wert, der im Mittel bei 76 Jahren liegt, schwankte in den letzten 2000 Jahren zwischen 74 und 79 Jahren.

Erst 40 Jahre nach Lalande gelang es Wilhelm Olbers, schnellere Methoden der Bahnberechnung von Kometen zu entwickeln.

Nach diesem Erfolg wurde Lalande 1762 Nachfolger seines Lehrers Deslisle als Professor für Astronomie am Collège Royale, was er bis zu seinem Tod blieb. 1760 bis 1776 war er außerdem Herausgeber der Connaissance des temps, des wichtigsten astronomischen Jahrbuchs in Frankreich. 1791 wurde er Rektor des College de France und setzte unter anderem die Zulassung von Studentinnen durch.

Als Astronom arbeitete er beispielsweise an der Erarbeitung von „Monddistanzen“ zu Sternen, die damals als eine Methode angesehen wurden, das für die Schifffahrtsnavigation sehr wichtige Längenproblem zu lösen.

Auch an der Bearbeitung der Daten der Venusdurchgänge von 1761 und 1769, unter anderem auch von James Cook in der Südsee durchgeführt (Endeavour-Expedition, Charles Green führte Juni 1769 auf Tahiti die Beobachtungen aus), war Lalande beteiligt. Es gelang Lalande 1771, aus weltweiten Beobachtungen der Venustransite von 1761 und 1769 eine verbesserte Berechnung der Erdbahn vorzulegen. Seine Angabe dieser „Astronomischen Einheit“ von 153 ± 1 Mio. km stimmt mit dem heutigen Wert von 149,6 Mio. km bereits bis auf 2 % überein. Denselben Wert erhielt Johann Franz Encke 1835, während Pingrè auf 142,9 Mio. km kam.

Am 24. November 1763 wurde Lalande in die Royal Society aufgenommen. 1764 erschien sein Lehrbuch der Astronomie, das auch ein Handbuch für Messungen und deren „Reduktion“ und Messinstrumente war. Die darin geschilderten Berechnungsmethoden für Kometenbahnen beeinflussten 1800 den jungen Kaufmann Friedrich Wilhelm Bessel, sich der Astronomie zuzuwenden. Lalande schrieb auch populärwissenschaftliche Bücher wie seine „Astronomie des Dames“.

1765 bis 1766 reiste er nach Italien, wo er bei einer Audienz vom Papst erbat, die Werke von Nicolaus Copernicus und Galilei vom „Index“ zu nehmen. 1769 erschien sein umfangreicher Reisebericht aus Italien. Auf einer weiteren England-Reise sah er in Greenwich auch die berühmten Uhren Harrisons, die in verbesserter Form schließlich das Längenproblem lösten.

1781 wurde Lalande in die American Society of Arts and Sciences gewählt. Ab 1783 war sein begabtester Student Jean-Baptiste Joseph Delambre sein Assistent, den Lalande 1788 (Brief an Bugge) als „zur Zeit fähigsten Astronom weltweit“ bezeichnete. Nachdem Delambre 1789 für seine Berechnung der Uranusbahn den Grand Prix der Akademie erhalten hatte, überschrieb ihm Lalande seine Sternwarte.

Im Mai 1795 wurde Lalande Direktor der Pariser Sternwarte. Es entstand ein erster Sternkatalog mit 30.000 Sternen, 1797 erweitert auf 41.000. Seine angeheiratete Nichte und illegitime Tochter Marie-Jeanne de Lalande (Amélie Harlay, 1768–1832) und sein Cousin Michel, Ehemann von Marie-Jeanne, waren dabei seine Mitarbeiter.

Der Aufklärer Lalande war als Atheist bekannt, was ihm während der Französischen Revolution zum Vorteil gereichte. 1799 erschien sein Wörterbuch für Atheisten, in das er später zu dessen Unwillen[4] auch Napoleon Bonaparte aufnahm. Seine „Hässlichkeit“ war Ziel vieler zeitgenössischer Karikaturen. Er selbst nahm keinen Anstoß daran und ließ sich dadurch auch nicht in seinen Beziehungen zu Frauen stören.

Sein Witz äußert sich beispielsweise in folgender Anekdote. Es war allgemein bekannt, dass Voltaire, zu dem Lalande eine freundschaftliche Beziehung unterhielt, keine Katzen mochte. Als Voltaire lästerte, sie hätten es als Tiere nicht einmal unter die 33 Sternbilder geschafft, benannte der bekennende Katzenliebhaber Lalande ein Sternbild Felis (Katze). Allerdings
fand es bei vielen Astronomen keinen Anklang, und war daher nur auf einigen Himmelskarten zu finden. Hingegen entschied der erste europäische Astronomenkongress 1798 in Gotha, auf dem er auf Einladung von Franz Xaver von Zach mit Johann Elert Bode zusammentraf, dass der von Lalande ebenfalls vorgeschlagene „Heißluftballon“ bleiben solle. Spätestens 1922 wurde auch dieses Sternbild von offiziellen Karten verbannt.

Jérôme Lalande ist unter den 72 Namen hervorragender Personen auf dem Eiffelturm aufgeführt.

Mit seinem Freund Claude Adrien Helvétius gründete der Aufklärer 1776 eine der bedeutendsten Freimaurerlogen im Zeitalter der Aufklärung, die sog. Philosophenloge Neuf Sœurs in Paris. Er selbst war ihr erster Logenmeister. Für die Encyklopädie von Diderot und d'Alembert steuerte er 250 Artikel auf dem Gebiet der Astronomie bei.

Jérôme Lalande starb mit 74 Jahren und wurde auf dem Cimetière du Père Lachaise in Paris bestattet.[5]
Der am 13. Mai 1971 entdeckte Asteroid (9136) Lalande wurde am 2. April 1999 nach ihm benannt.[6] Der Mondkrater Lalande ist ebenfalls nach ihm benannt.
