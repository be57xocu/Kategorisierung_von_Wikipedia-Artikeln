Alma Taylor (* 3. Januar 1895 in London; † 23. Januar 1974 ebenda) war eine britische Kinderdarstellerin und Schauspielerin. Sie gehörte zu den ersten Stars des britischen Films.

Ihr Filmdebüt hatte sie 1908, davor war sie bereits auf Londoner Bühnen zu sehen. Bis 1915 trat sie neben Chrissie White als Sally, die Schwester der Titelheldin Tilly, in einer beliebten Komödienserie des Produzenten Cecil Hepworth auf. Sie war bei Hepworth bis in die 1920er Jahre engagiert und spielte vornehmlich in melodramatischen Rollen. 1923 hatte sie die Hauptrolle in Hepworths ehrgeiziger, aber erfolgloser Neuverfilmung von Comin' Thro the Rye. Mit dem darauffolgenden Zusammenbruch seiner Filmfirma war auch Taylor bis 1926 nicht mehr präsent.

1929 spielte sie dann in zwei deutschen Filmen, darunter Der Hund von Baskerville unter der Regie von Richard Oswald. Sie drehte nur einige wenige Tonfilme und hatte dort lediglich Kleinstrollen, die letzte 1958.

Alma Taylor war mit dem Filmproduzenten Walter West verheiratet. Sie starb an einem Schlaganfall.
