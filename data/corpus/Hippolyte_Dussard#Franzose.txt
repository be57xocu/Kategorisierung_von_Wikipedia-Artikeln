Hippolyte Dussard (* 4. September 1798 in Morez, Jura; † 22. Januar 1876 in Miéry, Jura) war französischer Ökonom.

Dussard trat 1839 in die Redaktion des Repertoire de l'industrie étrangère, das Zeichnungen und Beschreibungen der wichtigsten im Ausland patentierten Maschinen enthielt, ein. Von 1843 bis 1846 Chefredakteur des Journal des économistes, wurde er 1848 zum Präfekten des Départements Seine-Inférieure ernannt.

Er hat weitere Bücher geschrieben und auch Mills Prinzipien der politischen Ökonomie ins Französische übersetzt.
