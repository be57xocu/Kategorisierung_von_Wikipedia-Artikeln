Georg Joachim Göschen (* 22. April 1752 in Bremen; † 5. April 1828 in Grimma) war ein Verleger der Goethe-Zeit.  Zu seinen Autoren zählten u. a. Friedrich Schiller, Johann Wolfgang von Goethe, Christoph Martin Wieland und Friedrich Gottlieb Klopstock.

Göschen war der Sohn eines Kaufmanns, der nach dem Tod seiner Frau wegen finanzieller Schwierigkeiten nach Vlotho zog und dann untertauchte. Göschen wurde von Bekannten wieder nach Bremen geholt und er wohnte beim Pastor Erhard Heeren in Bremen-Arbergen, der das Waisenkind förderte. Göschen machte eine Buchhändlerlehre bei der Cramerschen Buchhandlung in der Obernstraße in Bremen.

1770 zog er nach Leipzig um und war bei der Buchhandlung Siegfried Leberecht Crusius tätig. Von 1783 bis 1788 leitete er die Gelehrtenbuchhandlung in Dessau. Er gründete 1785 in Leipzig  die G. J. Göschen’sche Verlagsbuchhandlung, die unter seiner Leitung zu einem der wichtigsten Verlage der Weimarer Klassik wurde. Er verlegte von 1786 bis 1790 die erste Gesamtausgabe der Werke Goethes in 8 Bänden mit einem finanziellen Misserfolg. Ab 1785 editierte er die Werke Schillers. Goethe und Schiller verlor er später an den Verleger Johann Friedrich Cotta. Seine größte Aufgabe war die Herausgabe der Werke von Christoph Martin Wieland. Die 1802 erschienene Wielandausgabe umfasste 42 Bände.

Einer der Illustratoren des Verlegers war Johann Heinrich Ramberg.[1]
Die Herausgabe der Klassiker brachte keine großen finanziellen Gewinne; er verdiente jedoch mit den Veröffentlichungen von Romanen, Almanachen und Sachbüchern.

Göschen war bestrebt, auch äußerlich ansehnliche Bücher herzustellen. 1793 gründete er auch eine eigene Druckerei in Leipzig. Er verlegte die Druckerei 1797 nach Grimma. Die Leipziger Zunftregeln schrieben vor, dass die Bücher in Frakturschriften erscheinen müssten. Dagegen wurden ihm in Grimma „uneingeschränkte Privilegien“ eingeräumt, dies ermöglichte ihm, moderne Typographien zu verwenden, auch die von ihm geschätzten Antiquaschriften von Bodoni und Baskerville.[2] In Grimma arbeitete sein Freund Johann Gottfried Seume bei ihm als Korrektor. Dessen Autobiographie Mein Leben veröffentlichte er 1813. Göschen selbst schrieb ein heute vergessenes Lustspiel und seine Erinnerungen.

1795 erwarb er ein Landgut in Grimma-Hohnstädt. Hier befindet sich im Göschenhaus heute ein Museum mit einer Seume-Gedenkstätte, welche maßgeblich dem Engagement von Renate Sturm-Francke zu verdanken ist. Die erfolgreiche Verlagshandlung wurde durch seine Söhne weitergeführt, 1838/39 dann jedoch an die Cotta’sche Verlagsbuchhandlung verkauft.

Göschen war der Großvater der britischen Politiker George Joachim Goschen, 1. Viscount Goschen und Edward Goschen, 1. Baronet.

Ehrungen
