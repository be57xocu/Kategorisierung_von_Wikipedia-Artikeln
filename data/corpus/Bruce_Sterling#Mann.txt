Bruce Sterling (* 14. April 1954 in Brownsville, Texas) ist ein US-amerikanischer Science-Fiction-Schriftsteller.

1974 war Sterling Teilnehmer des renommierten Clarion Workshops für angehende Science-Fiction- und Fantasy-Autoren.
Seine erste Veröffentlichung war 1977 der Roman Involution Ocean (dt. „Der Staubplanet“ (1984), später „Der Staubozean“).
Er gilt als Mitbegründer des Cyberpunk. Mehrfach war er für den Nebula Award und den Hugo Award nominiert, letzteren hat er 1999 erhalten. Seit 2007 lebt er in Turin, nachdem er mehrere Jahre in Belgrad gewohnt hatte. Er ist in zweiter Ehe mit der serbischen Autorin und Filmemacherin Jasmina Tešanović verheiratet.

Bekannt wurde Sterling auch durch sein politisches Engagement.
1992 veröffentlichte er sein erstes Sachbuch The Hacker Crackdown mit dem Untertitel Recht und Unordnung im Elektronischen Grenzland; darin beschreibt er die Hacker-Szene der späten 80er Jahre und entscheidende Ereignisse wie etwa den Schlag des US Secret Service gegen Kreditkartenbetrüger, die Razzia bei Steve Jackson Games und die Gründung der Electronic Frontier Foundation. Dieses Buch wurde frei in elektronischer Form im Internet veröffentlicht.

Sterling ist zudem einer der führenden Köpfe hinter der Viridian-Design-Bewegung („Cyber-Grüne“).

Sterling schreibt journalistische Artikel zu Popkultur, Populärwissenschaft und Reise, die unregelmäßig in verschiedenen amerikanischen Magazinen erscheinen.

An zwei Kurzgeschichten in Mirrorshades The Cyberpunk Anthology war er als Koautor beteiligt.

Zu dem Buch The Glass Bees (der amerikanischen Ausgabe von „Gläserne Bienen“ (von Ernst Jünger)) hat er 2000 für den Verlag New York Review Books das Vorwort geschrieben.

Sterling ist einer von 18 Koautoren des 2003 erschienenen Buches „Das Geheimnis der Matrix“, einer Essay-Sammlung zum Kinofilm „Matrix“.

Weiterhin schrieb Sterling einen Beitrag zu DJ Spookys 2008 erschienenem Buch „Sound Unbound“, in dem er sich mit toten Medien auseinandersetzt.
