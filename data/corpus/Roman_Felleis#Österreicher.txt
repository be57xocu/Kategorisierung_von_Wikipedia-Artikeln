Roman Felleis (* 18. März 1903 in Wien; † 24. August 1944 im KZ Buchenwald, Thüringen) war ein österreichischer Politiker. Er gründete zusammen mit Bruno Kreisky die Revolutionäre Sozialistische Jugend.

Roman Felleis' politische Karriere begann in der Sozialistischen Arbeiterjugend (SAJ). Nach dem Ende des Österreichischen Bürgerkrieges wurde die SAJ im Februar 1934 verboten. Daraufhin versammelten sich die ehemaligen Funktionäre der SAJ im Wienerwald und gründeten unter Führung von Roman Felleis und Bruno Kreisky am 18. Februar die Revolutionäre Sozialistische Jugend (RSJ).

Im Spätsommer 1939 gelang es der Gestapo Roman Felleis zu verhaften. Nach seiner Verurteilung wurde er ins KZ Buchenwald überstellt. Er starb am 24. August 1944 während eines amerikanischen Bombenangriffs.

Sein ehrenhalber gewidmetes Grab (Abteilung 2, Ring 3, Gruppe 1, Nummer 18) befindet sich im Urnenhain der Feuerhalle Simmering.

Im Jahr 2004 wurde in Wien Floridsdorf (21. Bezirk) die Roman-Felleis-Gasse nach ihm benannt. Außerdem wurde ein 1927/28 erbauter Gemeindebau im 3. Wiener Gemeindebezirk (Hagenmüllergasse) nach ihm benannt.
