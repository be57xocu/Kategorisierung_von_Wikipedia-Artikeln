Justina Machado (* 6. September 1972 in Chicago, Illinois) ist eine US-amerikanische Schauspielerin mit puerto-ricanischen Wurzeln.

Ihre Familie wanderte aus Puerto Rico ein und ließ sich in Chicago nieder. Dort trafen ihre Eltern das erste Mal aufeinander und heirateten später. Zusammen hatten sie 5 Kinder, Justina ist die Älteste. Während ihrer Schulzeit spielte sie aktiv in der Theatergruppe ihrer Schule mit. 1986 besuchte sie die Lane Tech High School. In ihrer Freizeit spielte sie bei der Latino Chicago Theater Company.

1990 machte sie ihren Abschluss an der High School und zog einige Zeit später nach New York City. Die Schauspielerfahrungen, die sie bei der Latino Chicago Theater Company sammeln konnte, halfen ihr dabei in New York City Fuß fassen zu können. Bald darauf wurde ihr ein Job als professionelle Schauspielerin in Los Angeles angeboten. 1996 hatte sie ihre ersten kleineren Rollen in Fernsehserien.

Zu ihren bekannteren Filmen zählen A.I. – Künstliche Intelligenz mit Haley Joel Osment, Im Zeichen der Libelle und Final Destination 2 mit Michael Landes. In der Fernsehserie Six Feet Under – Gestorben wird immer konnte sie zunächst eine Nebenrolle ergattern, die dann später in eine Hauptrolle umgewandelt wurde.

Neben der Tätigkeit als Film- und Fernsehschauspielerin tritt sie auch in Theaterproduktionen auf. Zu den bekanntesten Stücken zählen Blade to the Heat und Black Butterfly (beide wurden im Mark Taper Forum in Los Angeles aufgeführt).
