Gertrude Pritzi, oft auch Trude Pritzi genannt (* 15. Jänner 1920 in Wien; † 21. Oktober 1968 in Wien), war eine österreichische Tischtennisspielerin. Ihre größten Erfolge erreichte sie zwischen 1936 und 1944 während ihrer Vereinszugehörigkeit zum Post SV Wien. 1937 und 1938 wurde sie Weltmeisterin. Sie lebte in Wien und verdiente ihren Lebensunterhalt als Postbedienstete.

Pritzi begann ihre Tischtennis-Laufbahn beim Verein Badner AC, wechselte 1936 zu Post SV Wien und schließlich 1945 zu Austria Wien. Später spielte sie auch noch für den First Vienna FC 1894. 1937 und 1938 gewann sie die österreichische Meisterschaft und setzte sich dabei gegen die damals vorherrschende Trude Wildam durch. 1938 wurde sie auch Weltmeisterin. 

Bereits bei der Weltmeisterschaft 1937 hatte sie das Finale erreicht, aber dieses wurde wegen Zeitüberschreitung abgebrochen; der Titel wurde damals nicht vergeben – ein Kuriosum in der Geschichte des Tischtennis. Diese Entscheidung hat der Weltverband ITTF inzwischen korrigiert: Seit April 2001 werden Trude Pritzi und die Amerikanerin Ruth Hughes Aarons als „Co-Weltmeisterinnen“ geführt.[1]
Nach dem Anschluss Österreichs an Deutschland 1938 trat sie bei Weltmeisterschaften für Deutschland auf. Dabei wurde sie noch zweimal Weltmeisterin im Doppel. In der deutschen Rangliste wurde sie 1938 auf Rang 1 geführt.

1968 starb Pritzi an Krebs und wurde auf dem Döblinger Friedhof beigesetzt.[2][3]
2010 wurde sie in die ITTF Hall of Fame aufgenommen.[4]
[5]
1926: Mária Mednyánszky |
1928: Mária Mednyánszky |
1929: Mária Mednyánszky |
1930: Mária Mednyánszky |
1931: Mária Mednyánszky |
1932: Anna Sipos |
1933: Anna Sipos |
1934: Marie Kettnerová |
1935: Marie Kettnerová |
1936: Ruth Hughes Aarons |
1937: vakant |
1938: Trude Pritzi |
1939: Vlasta Depetrisová |
1947: Gizella Lantos-Gervai-Farkas |
1948: Gizella Lantos-Gervai-Farkas |
1949: Gizella Lantos-Gervai-Farkas |
1950: Angelica Adelstein-Rozeanu |
1951: Angelica Adelstein-Rozeanu |
1952: Angelica Adelstein-Rozeanu |
1953: Angelica Adelstein-Rozeanu |
1954: Angelica Adelstein-Rozeanu |
1955: Angelica Adelstein-Rozeanu |
1956: Tomi Ōkawa |
1957: Fujie Eguchi |
1959: Kimiyo Matsuzaki |
1961: Qiu Zhonghui |
1963: Kimiyo Matsuzaki |
1965: Naoko Fukazu |
1967: Sachiko Morisawa |
1969: Toshiko Kowada |
1971: Lin Huiqing |
1973: Hu Yulan |
1975: Pak Yung-sun |
1977: Pak Yung-sun |
1979: Ge Xinai |
1981: Tong Ling |
1983: Cao Yanhua |
1985: Cao Yanhua |
1987: He Zhili |
1989: Qiao Hong |
1991: Deng Yaping |
1993: Hyun Jung-hwa |
1995: Deng Yaping |
1997: Deng Yaping |
1999: Wang Nan |
2001: Wang Nan |
2003: Wang Nan |
2005: Zhang Yining |
2007: Guo Yue |
2009: Zhang Yining |
2011: Ding Ning |
2013: Li Xiaoxia |
2015: Ding Ning
