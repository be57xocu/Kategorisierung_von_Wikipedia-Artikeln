Emma Brunner-Traut (* 25. Dezember 1911 in Frankfurt am Main; † 18. Januar 2008 in Tübingen) war eine deutsche Ägyptologin.

Emma Traut hatte eine große musische Begabung und wollte zunächst Konzertpianistin werden. Zur Zeit ihres Abiturs begegnete sie aber dem Ägyptologen Hellmut Brunner (1913–1997), der sie für die Ägyptologie begeisterte und den sie 1937 auch heiratete. Nach ihrem Studium der Ägyptologie wurde sie 1937 mit einer Arbeit über den „Tanz im Alten Ägypten“ promoviert. 1937/38 begleitete sie ihren Ehemann auf dessen Reisestipendium des Deutschen Archäologischen Instituts nach Ägypten und nahm an Ausgrabungen von Hermann Junker in Gizeh und von Günther Roeder in Hermopolis teil. 

Nach dem Zweiten Weltkrieg folgte sie 1951 ihrem Mann nach Tübingen. Hier hat sie neben ihrer wissenschaftlichen Tätigkeit sich in der Stuttgarter Privatstudiengesellschaft, an der zu dieser Zeit auch noch der Verleger Ernst Klett beteiligt war, engagiert. Sie war wissenschaftlich in der Sammlung des Archäologischen Institutes Tübingen tätig und gestaltete unter anderem deren 1981 erschienenen zweibändigen Katalog maßgeblich mit. 

Im Jahr 1963 erschienen erstmals ihre „Altägyptischen Märchen“ (8. Auflage 1989). Parallel veröffentlichte sie die „Altägyptischen Tiergeschichten und Fabeln“ (6. Auflage 1980) mit Rekonstruktionen von auf Reliefs und Zeichnungen überlieferten Erzählungen. 

In ihrem Werk „Frühformen des Erkennens“ (2. Auflage 1992) stellte sie die wesentlichen Aspekte der altägyptischen Denk- und Darstellungsweise der griechisch-abendländischen Kultur gegenüber. 1998 erschien ihr „Alltag unter den Pharaonen“ und sie befasste sich mit der ägyptischen Religionsgeschichte insbesondere der Kopten. 1984 richtete sie unter dem Motto „Osiris - Kreuz - Halbmond“ gemeinsam mit ihrem Mann eine große Ausstellung in Stuttgart aus. Neben ihrer Tätigkeit in Tübingen und Stuttgart war sie auch für die Ägyptischen Museen in München und Berlin tätig. Emma Brunner-Traut lebte bis zu ihrem Tod in Tübingen. Sie fand ihre letzte Ruhestätte auf dem Tübinger Bergfriedhof.

1964 wurde sie vom Deutschen Archäologischen Institut zum Ordentlichen Mitglied ernannt. Sie wurde auch von der International Society for Folk Narrative Research als Mitglied aufgenommen. 1972 wurde ihr der Professorentitel verliehen.

Bekannt wurde sie einer breiteren Öffentlichkeit unter anderem durch den von ihr 1962 in erster Auflage herausgegebenen und danach mehrfach wiederaufgelegten und aktualisierten Reiseführer für Ägyptenreisende.

