Florian Illies (* 4. Mai 1971 in Schlitz) ist ein deutscher Journalist, Kunsthändler, Kunsthistoriker und Buchautor.

Illies’ Vater war der Biologe und Limnologe Joachim Illies. Florian Illies studierte Kunstgeschichte und Neuere Geschichte in Bonn und Oxford. An der Universität Bonn schloss er 1998 sein Studium mit dem Grad eines Magister Artium (M.A.) bei Andreas Tönnesmann mit der Arbeit Gustav Friedrich Waagen in England. Eine Studie über den Import kunsthistorischer Systeme zur Zeit des Viktorianismus ab. Erste journalistische Erfahrungen erwarb er beim Schlitzer Boten, seinem Heimatblatt, später volontierte er bei der Fuldaer Zeitung.

Bereits 1997 wurde er Feuilletonredakteur der  Frankfurter Allgemeinen Zeitung. Seit 1999 betreute er die „Berliner Seiten“ der FAZ und wurde anschließend Feuilletonchef der Frankfurter Allgemeinen Sonntagszeitung. Nach seinem Ausscheiden bei der FAZ gründete Illies 2004 mit seiner Frau Amélie von Heydebreck, der Tochter des Deutsche-Bank-Vorstandes Tessen von Heydebreck, mit der er zwei Kinder hat, Monopol, eine Zeitschrift für Kunst, Literatur und Lifestyle. Bis Ende 2006 war Illies sowohl deren Herausgeber als auch deren Chefredakteur. 2007 übernahm der ehemalige Welt am Sonntag-Kulturredakteur Cornelius Tittel die Chefredaktion, Herausgeber blieben Illies und seine Frau. 2008 wechselte Illies zur Zeit und war zunächst für das Zeit-Magazin tätig.[1] Ab 2009 leitete er dort, zusammen mit Jens Jessen, das Ressort Feuilleton und Literatur.[2] Seit dem Sommer 2011 ist Illies einer der vier Gesellschafter des Berliner Auktionshauses Villa Grisebach[3], wo er vor allem mit der Kunst des 19. Jahrhunderts handelt, um so das bisher auf die Klassische Moderne spezialisierte Auktionshaus thematisch zu erweitern.[4]
Bekannt wurde Illies besonders durch seinen Bestseller Generation Golf (2000), in dem er ein kritisches Bild seiner eigenen, um 1970 geborenen Generation entwirft. In den beiden folgenden Bänden Anleitung zum Unschuldigsein (2001) und Generation Golf zwei (2003) suchte Illies seine Beobachtungen zu bestätigen.[5]
2006 erschien Ortsgespräch, ein Rückblick auf eine Kindheit in der deutschen Provinz, und 2012 ein weiterer Bestseller: 1913: Der Sommer des Jahrhunderts, der im September 2013 vom Theater Oberhausen adaptiert wurde.[6]
als Autor

als Mitautor

als Herausgeber
