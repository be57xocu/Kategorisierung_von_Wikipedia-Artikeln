Heinz Rutishauser (* 30. Januar 1918 in Weinfelden, Schweiz; † 10. November 1970 in Zürich) war ein Schweizer Mathematiker und ein Pionier der modernen numerischen Mathematik und der Informatik avant la lettre.

Heinz Rutishauser studierte ab 1936 Mathematik an der ETH Zürich und schloss 1942 mit dem Diplom ab. Von 1942 bis 1945 war er Assistent von Walter Saxer an der ETH und von 1945 bis 1948 Mathematiklehrer in Glarisegg und Trogen. 1948 promovierte er an der ETH mit einer viel beachteten Dissertation über Funktionentheorie.

1948/49 hielt sich Rutishauser in den USA an den Universitäten  Harvard und  Princeton auf. Von 1949 bis 1955[1] war er wissenschaftlicher Mitarbeiter am kurz zuvor von Eduard Stiefel gegründeten Institut für Angewandte Mathematik der ETH Zürich, wo er zusammen mit Ambros Speiser massgeblich an der Entwicklung des ersten Schweizer Computers ERMETH beteiligt war. Dabei leistete er insbesondere auf dem Gebiet der Compiler Pionierarbeit indem er in seiner Habilitationsschrift an der ETH schon 1951 das Prinzip eines Compilers und der dafür notwendigen zusätzlichen Programmbefehle beschrieb.[2] Auch wirkte er an der Definition der Programmiersprachen Algol 58 + 60 mit.

1951 wurde Rutishauser Privatdozent, 1955 ausserordentlicher und 1962 ordentlicher Professor für Angewandte Mathematik an der ETH. 1968 wurde er Leiter der Fachgruppe für Computerwissenschaften, aus der später das Institut für Informatik und schliesslich das Departement Informatik der ETH Zürich hervorgingen.

Spätestens seit den 1950er Jahren litt Rutishauser an Herzbeschwerden. 1964 erlitt er einen schweren Herzinfarkt, von dem er sich zunächst wieder erholte. Er starb am 10. November 1970 in seinem Büro an akutem Herzversagen. Nach seinem frühen Tod kümmerte sich seine Frau Margrit um die Herausgabe seiner nachgelassenen Werke.

1958 führte er den LR-Algorithmus ein, der auf der LR-Zerlegung basiert, aber heute weitgehend durch den stabileren QR-Algorithmus ersetzt ist.
