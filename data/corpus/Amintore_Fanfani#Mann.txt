Amintore Fanfani (aˈmintore faˈɱfaːni)[1] (* 6. Februar 1908 in Pieve Santo Stefano, Provinz Arezzo; † 20. November 1999 in Rom) war ein italienischer Wirtschaftswissenschaftler und Politiker (DC). Er war einer der wichtigsten Politiker seines Landes während der Nachkriegszeit. Von 1954 bis 1959 und erneut von 1973 bis 1975 war er Segretario (entspricht einem Vorsitzenden) seiner Partei. Sechsmal war er italienischer Ministerpräsident, allerdings jeweils nur für kurze Zeit: 1954, 1958/59, 1960–1963, 1982/83, 1987. Zusammengerechnet war er insgesamt etwa viereinhalb Jahre Regierungschef. Außerdem war er mehrmals Außenminister Italiens und 1965/66 Präsident der UN-Generalversammlung. Letztlich war er 1968–1973, 1976–1982 und 1985–1987 Präsident des italienischen Senats.

Amintore Fanfani wurde in einer Kleinstadt in der zentralen Toskana geboren. Er war der älteste Sohn des Rechtsanwalts und Notars Giuseppe (Beppe) Fanfani, der sich auf lokaler Ebene bei der Partito Popolare Italiano, einer Vorläuferin der Democrazia Cristiana, engagierte, und dessen Frau Anita, die aus Kalabrien stammte.[2] Nach dem Abschluss des Realgymnasiums in Arezzo studierte er Wirtschaftswissenschaft und Handel an der Katholischen Universität vom Heiligen Herzen in Mailand. Während des Studiums gehörte er der Begabtenförderungseinrichtung Collegio Augustinianum sowie dem katholischen Studentenbund FUCI an.

Er schloss das Studium 1930 ab, 1932 wurde er Privatdozent, erhielt einen Ruf auf einen Lehrstuhl für Wirtschaftsgeschichte an der Universität Genua, folgte 1936 aber dem Ruf auf einen ebensolchen Lehrstuhl an der katholischen Universität in Mailand. Zudem war er außerordentlicher Professor am Institut für Wirtschaft und Handel an der Universität Venedig Ca’ Foscari. Er veröffentlichte zahlreiche Abhandlungen und Artikel sowie Bücher über ökonomische und gesellschaftliche Themen, die zum Teil auch in andere Sprachen übersetzt wurden (etwa Cattolicesimo e protestantesimo nella formazione storica del capitalismo von 1934, deutsch „Katholizismus und Protestantismus in der geschichtlichen Entwicklung des Kapitalismus“).[3]
Fanfani hing der Doktrin des Korporatismus an, die er als Mittel zur Umsetzung der katholischen Soziallehre im Gegensatz sowohl zum Wirtschaftsliberalismus als auch zum Sozialismus sah.[4] 1938 unterzeichnete er das antisemitische Manifesto della razza,[5] in den Folgejahren veröffentlichte er unter anderem Beiträge in der faschistischen Zeitschrift La difesa della razza,[4] in der Dottrina fascista (Organ der Parteischule Scuola di mistica fascista) sowie der Zeitschrift Geopolitica des Ministers Giuseppe Bottai. Er rechtfertigte die faschistische Kolonialpolitik einschließlich des Angriffskriegs gegen Äthiopien.[6] Andererseits nahm er ab 1940/41 auch an geheimen Treffen im Haus des Philosophieprofessors Umberto Padovani teil (unter anderem mit dem Kirchenrechtler, späteren Resistenza-Kämpfer und christdemokratischen Politiker Giuseppe Dossetti), bei denen über die sich abzeichnende Krise des Faschismus und die künftige gesellschaftliche Rolle der Katholiken diskutiert wurde.[7] Nach dem Sturz Benito Mussolinis und dem Ausscheiden Italiens aus der Achse am 8. September 1943 entzog sich Fanfani dem ausbrechenden Bürgerkrieg zwischen Faschisten und Antifaschisten, indem er in die Schweiz ging.[4]
Fanfani unterrichtete noch bis 1955 an der Cattolica, auch wenn er sich da bereits hauptsächlich der Politik widmete. Nach dem Umzug nach Rom im Jahr 1948 pendelte er für die Lehrveranstaltungen nach Mailand.[8]
1945 trat er der neugegründeten Democrazia Cristiana bei und wurde 1946 für sie in die Verfassunggebende Versammlung gewählt. Der erste Satz der italienischen Verfassung („Italien ist eine demokratische, auf die Arbeit gegründete Republik“) geht maßgeblich auf eine Formulierung Fanfanis zurück, die er als Kompromiss zwischen dem Vorschlag der Kommunisten und Sozialisten („Italien ist eine Republik der Arbeiter“) und dem der Liberalen („Italien ist eine auf die Freiheiten gegründete Republik“) einbrachte.[4] Von 1946 bis zu seinem Tod gehörte er ununterbrochen dem Italienischen Parlament an – während der ersten fünf Legislaturperioden (bis 1968) als Mitglied der Abgeordnetenkammer, anschließend als Senator.

Nach 1947 war Fanfani mehrfach Kabinettsmitglied, darunter als Außenminister, Innenminister, Landwirtschaftsminister und Arbeitsminister. Mehrfach bekleidete er das Amt des Ministerpräsidenten (1954, 1958/59, 1960–1963, 1982/83, 1987). Er vertrat sozialreformerische Ideen und setzte sich für staatlichen Wohnungsbau und Agrarreformen ein. Während seiner Amtszeit wurden 1962 die italienischen Elektrizitätswerke verstaatlicht, dadurch wurde die nationale Stromversorgungsholding Enel geschaffen. Außerdem führte Fanfanis Regierung eine Schulreform durch, durch die eine für alle Schüler verpflichtende achtjährige Hauptschule geschaffen wurde.[9] Damit bereitete er einer Öffnung der Christdemokraten nach links (apertura à sinistra) den Weg, die sich dann 1963 in der Koalition mit den Sozialisten unter seinem Parteifreund Aldo Moro verwirklichte.[10] 1965/66 war er Präsident der UN-Generalversammlung. Im März 1972 wurde er vom Staatspräsidenten Giovanni Leone zum Senator auf Lebenszeit ernannt.

Im April 1939 heiratete Fanfani Bianca Rosa Provasoli, Tochter eines Textilunternehmers, die er als Studentin in der Universitätsbibliothek kennengelernt hatte. Sein Antrag hielt sie davon ab, Ordensschwester zu werden. Die Ehe wurde vom Rektor der Katholischen Universität, Pater Agostino Gemelli, geschlossen. Die beiden hatten sieben Kinder – zwei Söhne und fünf Töchter.[11] Bianca Rosa starb 1968.[12]
Sieben Jahre nach dem Tod seiner ersten Frau heiratete Fanfani erneut: Mariapia (oder Maria Pia) Tavazzani, die ebenfalls verwitwet war[13] und sich für verschiedene humanitäre Projekte in aller Welt engagierte. Sie war von 1985 bis 1989 Vizepräsidentin der Internationalen Föderation der Rotkreuz- und Rothalbmond-Gesellschaften.[14]
1940er Jahre:
Spaak (BEL) |
Aranha (BRA) |
Arce (ARG) |
Evatt (AUS) |
Rómulo (PHI)
– 1950er Jahre:
Entezam (IRN) |
Nervo (MEX) |
Pearson (CAN) |
Pandit (IND) |
van Kleffens (NED) |
Maza (CHI)  |
Masson (CHI) |
Waithayakon (THA) |
Munro (NZL) |
C. Malik (LIB) |
Belaúnde (PER)
– 1960er Jahre:
Boland (IRL) |
Slim (TUN) |
Khan (PAK) |
Rodríguez (VEN) |
Quaison-Sackey (GHA) |
Fanfani (ITA) |
Pazhwak (AFG) |
Mănescu (ROM) |
Catalán (GUA) |
Brooks (LBR)
– 1970er Jahre:
Hambro (NOR) |
A. Malik (INA) |
Trepczynski (POL) |
Benítes (ECU) |
Bouteflika (ALG) |
Thorn (LUX) |
Amerasinghe (SRI) |
Mojsov (YUG) |
Liévano (COL) |
Salim (TAN)
– 1980er Jahre:
von Wechmar (BRD) |
Kittani (IRQ) |
Hollai (HUN) |
Illueca (PAN) |
Lusaka (ZAM) |
Piniés (ESP) |
Choudhury (BAN) |
Florin (DDR) |
Caputo (ARG) |
Garba (NGR)
– 1990er Jahre:
de Marco (MLT) |
Shihabi (KSA) |
Ganew (BUL) |
Insanally (GUY) |
Essy (CIV) |
Freitas do Amaral (POR) |
Ismail (MAS) |
Udowenko (UKR) |
Opertti (URU) |
Gurirab (NAM)
– 2000er Jahre:
Holkeri (FIN) |
Han (KOR) |
Kavan (CZE) |
Hunte (LCA) |
Ping (GAB) |
Eliasson (SWE) |
Al Chalifa (BAH) |
Kerim (MKD) |
Brockmann (NIC) |
Treki (LAR)
– 2010er Jahre:
Deiss (SUI) |
al-Nasser (QAT) |
Jeremić (SRB) |
Ashe (ANT) |
Kutesa (UGA) |
Lykketoft (DEN)
