Gerald Frank Shove (* 1887 in Faversham, Kent; † 1947 Old Hunstanton, Norfolk) war ein britischer Ökonom.

Nachdem er die Schule von Uppingham besucht hatte,  studierte er Wirtschaft am King's College, Cambridge, wo er 1907/08 Rupert Brooke und James Strachey kennenlernte.

Brooke organisierte 1909 dessen Wahl zu den "Aposteln". Und auch in den späteren Jahren war Shove ein wichtiges Mitglied des Freundeskreises um Rupert Brooke. Unter diesen war er vor allem bekannt, dass er während eines Carbonari-Treffens 1909 den Trinkspruch äußerte: “The king, God damn him!”

1915 heiratete er die Dichterin Fredegond Maitland.
Er veröffentlichte Schriften über Wirtschaft und Artikel über Krieg und Frieden und erhielt eine Fellowship am King's College Cambridge.
