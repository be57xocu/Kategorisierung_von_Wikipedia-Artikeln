Sandra Kalniete [ˈsandra ˈkalniete] (* 22. Dezember 1952 in Togur, Oblast Tomsk, Sowjetunion) ist eine lettische Politikerin. Sie war von 2002 bis 2004 Außenministerin und zwischen dem 1. Mai 2004 und dem 22. November 2004 EU-Kommissarin.

Kalniete wurde im russischen Oblast Tomsk geboren, wohin ihre Eltern als Kinder bzw. Jugendliche 1941 bzw. 1949 von der Sowjetmacht aus dem sowjetisch okkupierten Lettland deportiert worden waren. Sie studierte von 1977 bis 1981 Kunst an der Lettischen Kunstakademie und arbeitete als Kunsthistorikerin. In diesem Beruf brachte sie unter anderem einen Katalog der Bildwirkerei von Rūdolfs Heimrāts (1929–1992) heraus (1987) und ein Buch über Lettische Textilkunst (1989).

Ihr Engagement in der lettischen Unabhängigkeitsbewegung führte sie 1988 in die Politik. Sie war stellvertretende Vorsitzende der Lettischen Volksfront, der größten der nach Unabhängigkeit strebenden politischen Organisationen Sowjetlettlands.

Nach der Wiederherstellung der Unabhängigkeit der Republik Lettland arbeitete sie im Außenministerium und war Botschafterin ihres Landes bei den Vereinten Nationen (1993 bis 1997), in Frankreich (1997 bis 2000) und bei der UNESCO (2000 bis 2002). Im November 2002 wurde sie lettische Außenministerin und behielt dieses Amt bis 2004.

Von März bis November 2004 war sie zusammen mit Franz Fischler Europäische Kommissarin für Landwirtschaft und Fischerei.

Für viele Letten überraschend wurde sie Ende 2004 nicht erneut als EU-Kommissarin ihres Landes nominiert. In der Folge zog sie sich vorübergehend aus der Politik zurück und lehnte die Übernahme der ihr angebotenen minderen diplomatischen Aufgaben ab. 2006 trat Kalniete der Partei Jaunais laiks (JL, zu deutsch: Neue Ära) bei. 2008 wechselte sie mit einem Gutteil der JL-Mitglieder in die neugegründete Partei Pilsoniskā savienība (PS, zu deutsch: Bürgerunion), deren Vorsitzende sie wurde. 2011 ging die PS in der neuen Partei Vienotība (zu deutsch: Einigkeit) auf. Für Pilsoniskā savienība 2009 und Vienotība 2014 ins Europäische Parlament gewählt, gehört sie dort der Fraktion der Europäischen Volkspartei (Christdemokraten) an.

Aufsehen erregte ihr 2001 erschienener Bericht Ar balles kurpēm Sibīrijas sniegos (deutscher Titel: Mit Ballschuhen im sibirischen Schnee, Herbig 2005, Knaur TB 2007) über die bis 1957 dauernde Deportation ihrer Familie nach Sibirien. Ihr in bislang 12 Sprachen übersetztes Buch trug maßgeblich dazu bei, dass die Verschleppung von 35.000 Letten im Jahre 1941 und weiterer 43.000 Letten im März 1949 durch und in die Sowjetunion auch außerhalb Lettlands bekannt wurde.[1]
Bei der Leipziger Buchmesse 2004 hielt Sandra Kalniete die vieldiskutierte Eröffnungsrede.[2]
Als EU-Parlamentarierin war Kalniete in der Periode 2009 bis 2014 Stellvertretende Vorsitzende der Delegation für die Beziehungen zu Japan.
Mitglied ist sie im Ausschuss für Binnenmarkt und Verbraucherschutz.
Als Stellvertreterin ist sie im Ausschuss für Landwirtschaft und ländliche Entwicklung und in der Delegation im Gemischten Parlamentarischen Ausschuss EU-Türkei.[3]
Sie gehört zu den 89 Personen aus der Europäischen Union, gegen die Russland im Mai 2015 ein Einreiseverbot verhängt hat.[4][5]
Übersetzungen von Ar balles kurpēm Sibīrijas sniegos:[7]
Michel Barnier (bis April 2004) |
Frits Bolkestein |
Philippe Busquin |
David Byrne |
Anna Diamantopoulou (bis März 2004) |
Franz Fischler |
Neil Kinnock |
Pascal Lamy |
Erkki Liikanen (bis Juli 2004) |
Mario Monti |
Poul Nielson |
Loyola de Palacio |
Chris Patten |
Romano Prodi |
Viviane Reding |
Michaele Schreyer |
Pedro Solbes (bis April 2004) |
Günter Verheugen |
António Vitorino |
Margot Wallström

Ergänzungen:Joaquín Almunia (ab April 2004)  |
Péter Balázs (ab Mai 2004) |
Jacques Barrot (ab April 2004) |
Joseph Borg (ab Mai 2004) |
Stavros Dimas (ab März 2004) |
Ján Figeľ(ab Mai 2004) |
Dalia Grybauskaitė (ab Mai 2004) |
Danuta Hübner (ab Mai 2004) |
Siim Kallas (ab Mai 2004) |
Sandra Kalniete (ab Mai 2004) |
Márcos Kyprianoú (ab Mai 2004) |
Janez Potočnik (ab Mai 2004) |
Olli Rehn (ab Juli 2004) |
Pavel Telička (ab Mai 2004)

Janis Jurkans |
Georgs Andrejevs |
Valdis Birkavs |
Indulis Bērziņš |
Sandra Kalniete |
Rihards Pīks |
Helena Demakova |
Artis Pabriks |
Helena Demakova |
Māris Riekstiņš |
Aivis Ronis |
Ģirts Valdis Kristovskis |
Edgars Rinkēvičs
