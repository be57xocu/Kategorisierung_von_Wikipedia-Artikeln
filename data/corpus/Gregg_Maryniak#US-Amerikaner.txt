Gregg E. Maryniak (* Februar 1954)[1] ist ein US-amerikanischer Jurist, Unternehmer und stellvertretender Direktor der X-Prize Foundation.

Im Jahr 1974 gründete er die Firma Oak Research Enterprises, die Kontrollsysteme für Tetraplegiker (engl. quadriplegics) entwirft, baut und installiert. Maryniak erlangte 1978 seinen Doktor für Rechtswissenschaften an der Northwestern University. Es folgte eine Karriere als Rechtsanwalt in Chicago, um dann Vizepräsident für Forschung und Entwicklung am Institut für Weltraumstudien in Princeton, New Jersey zu werden. 1985 war er für die Planung und Entwicklung der Raumsonde Lunar Prospector zuständig, die im Juli 1999 die Oberfläche des Mondes untersuchte.

Er war weiterhin wissenschaftlicher Berater für die Firma Futron Corporation in Bethesda, Maryland und unterstützte die NASA bei der Abteilung Transport und Wirtschaftsunternehmen für Weltraumforschung und Telekommunikation.

Maryniak war außerdem Dozent am Adler-Planetarium und im Museum für Naturgeschichte. Vorlesungen hielt er in Nordamerika, Europa und Asien. Das amerikanische Institut für Luft- und Raumfahrt zeichnete ihn 1991 für seinen Vortrag The Harvest of Space (dt. Der Nutzen/Ertrag des Weltraums) aus.

Er ist heute Professor an der International Space University. 1996 erhielt er die Tsiolkovsky-Medaille für seine Arbeit. 1998 wurde er von der Space Frontier Foundation mit dem Vision to Reality Award für seine Arbeit an der Sonde Lunar Prospector ausgezeichnet.
