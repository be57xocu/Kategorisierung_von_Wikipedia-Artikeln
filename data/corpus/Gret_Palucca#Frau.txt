Gret Palucca (eigentlich Margarete Paluka, * 8. Januar 1902 in München; † 22. März 1993 in Dresden) war eine deutsche Tänzerin und Tanzpädagogin.

Gret Paluccas Eltern waren der aus Konstantinopel stammende Apotheker Max Paluka und Rosa  Paluka, die jüdisch-ungarischer Herkunft war.[2] Kurz nach ihrer Geburt in München zog die Familie nach San Francisco, Kalifornien. Im Jahr 1909 kehrte Gret aber mit ihrer Mutter Rosa nach Deutschland zurück und kam nach Dresden, wo sie von 1914 bis 1916 Ballettunterricht bei Heinrich Kröller erhielt.

Schon als Ballettelevin stand Gret Palucca dem klassischen Tanz mit Skepsis gegenüber. Der Besuch einer Dresdner Tanzveranstaltung mit Mary Wigman wurde für sie zu einem Schlüsselerlebnis und Palucca zu einer der ersten Schülerinnen Mary Wigmans. Im Jahr 1921 änderte sie ihren Namen zu Gret Palucca.

Bis 1924 tanzte sie in Wigmans Gruppe. Dann begann Palucca mit ihrer Solokarriere und wurde eine der führenden Ausdruckstänzerinnen. Ihr Stil war fröhlich, unbeschwert und humorvoll, wie etwa die Choreografie von In weitem Schwung oder Tanzfreude vermitteln.

Ab 1924 war Gret Palucca mit Fritz Bienert, dem Besitzer eines Mühlenbetriebes in Dresden, sechs Jahre verheiratet. Er war der Sohn von Ida Bienert, der ersten privaten Kunstsammlerin der Modernen Kunst in Deutschland. In ihrem Hause gingen die Künstler des Dadaismus ebenso ein und aus wie die Architekten des jungen Bauhauses, wo ihre Tochter Ise Bienert, eine Worpswede-Schülerin, studierte. Palucca wurde zum Thema der Neuen Kunst. Mit Fritz Bienert verbrachte sie ihre Sommerurlaube ab 1924 regelmäßig auf der Insel Sylt.

Im Jahr 1925 gründete Gret Palucca ihre eigene Schule. Die Palucca-Schule in Dresden unterschied sich maßgeblich von anderen Schulen ihrer Art. Nicht der körperliche Drill stand im Vordergrund, sondern die geistig-künstlerische Erziehung. Zu ihren bekanntesten Schülerinnen zählen Ruth Berghaus und Lotte Goslar sowie Annerose Schunke, die spätere DDR-Nachrichtensprecherin Annerose Neumann.

Im Jahr 1926 schrieb Wassily Kandinsky zwei beachtete Aufsätze über Palucca, die zu ihrem wachsenden Bekanntheitsgrad beitrugen.

Am 29. April 1927 trat Palucca im Bauhaus in Dessau auf.

„Palucca verdichtet den Raum, sie gliedert ihn: der Raum dehnt sich, sinkt und schwebt – fluktuierend in allen Richtungen.“

Im Jahr 1930 trennte sich Gret Palucca von Fritz Bienert und begann ein Verhältnis mit Will Grohmann. Auch mit ihm war sie häufig auf Sylt. Im Jahr 1935 schrieb er die erste Monographie über sie, unter dem Pseudonym Olaf Rydberg.

Bei den Olympischen Spielen 1936 in Berlin nahm Palucca am Eröffnungsabend an der Seite von Leni Riefenstahl mit eigenen Beiträgen teil. Bis 1939 hatte sie viele Auftritte und konnte ungehindert arbeiten. Im Jahr 1939 erhielt sie dann von den Nationalsozialisten Tanzverbot und musste die Leitung ihrer Schule abgeben. Sie tanzte aber weiterhin auf privaten Veranstaltungen, was ihr jedoch unter Androhung von KZ-Haft später auch verboten wurde.

Gret Palucca hatte im Dritten Reich kein Auftrittsverbot mit Ausnahme bei staatlichen und NSDAP-Veranstaltungen bis zur Schließung aller Theater 1944.[3] Die Legende vom vollständigen Auftrittsverbot habe im Interesse der Kulturverantwortlichen in der DDR gelegen. Durch eine von Grohmann erwirkte Sondergenehmigung 1936 konnte Palucca trotz ihrer Stigmatisierung als sogenannte „Halbjüdin“ weiterhin als Tänzerin auftreten, Auslandsgastspiele wurden ihr anfangs noch gestattet, dann aber verboten.[4] Die Presse durfte ihre Auftritte nicht mehr positiv besprechen. Am 31. März 1939 wurde ihre Schule geschlossen.

Am 1. Juli 1945 eröffnete Gret Palucca wieder ihre Tanz-Schule in der Karcherallee 43 in Dresden. Im Jahr 1949 wurde diese Palucca-Schule verstaatlicht. Der Ausdruckstanz entsprach nicht dem neuen Geist der Zeit. Unter dem Begriff Neuer Künstlerischer Tanz versuchte Gret Palucca weiterhin, ihre Ausrichtung auf dem Lehrplan beizubehalten. Das klassische Ballett beherrschte aber die Ausbildung. Im Jahr 1959 verließ sie die DDR, ging nach Sylt und verhandelte von dort aus über die Bedingungen ihrer Rückkehr.[5] Als Zugeständnis erhielt Gret Palucca die künstlerische Leitung der Tanzschule in Dresden zugesichert und eine Professur, einen Wagen mit Chauffeur sowie ein Grundstück auf Hiddensee.

An der Gründung der Deutschen Akademie der Künste in Berlin (Ost) 1950 war sie beteiligt. Von 1965 bis 1970 war sie Vizepräsidentin.

Zum 75. Geburtstag von Wilhelm Pieck im Jahr 1951 gab sie ihren letzten Soloauftritt. Bis ins hohe Alter blieb Gret Palucca als Tanzpädagogin tätig. 

Nach ihrem Tod 1993 wurde Gret Palucca auf Hiddensee beigesetzt, wo sie alljährlich ihre Sommeraufenthalte im eigenen Haus verbracht hatte. Zu Beginn des 20. Jahrhunderts hatte es hier eine Künstlerkolonie gegeben, in der sie zuweilen zu Gast war.

Ihre langjährigen Gastgeber bei ihren Sommerurlauben in List auf Sylt benannten in den frühen 1960er Jahren ihr erstes Ausflugsschiff nach ihrem Namen „Palucca“; auch alle nachfolgende Schiffe dieser kleinen Reederei trugen die Namen „Palucca“ oder „Gret Palucca“. Noch heute fährt ein Ausflugskutter der Adler-Reederei, die die Linien der alten Palucca-Reederei übernommen hat, unter dem Namen „Gret Palucca“, ein weiterer unter dem Namen ihrer Mutter, „Rosa Paluka“.

In Dresden gibt es die Gret-Palucca-Straße.

Am 8. Oktober 1998 wurde eine Briefmarke im Wert von 4,40 DM mit Paluccas Porträt im Rahmen der Briefmarkenserie Frauen der deutschen Geschichte herausgegeben.

Im Museum Hofmühle Dresden im Stadtteil Plauen befindet sich eine Dauerausstellung über Paluccas Leben und Werk.

An einem Haus im Dresdner Kanzleigäßchen wurde eine Fassade mit Bildern Paluccas im Tanz gestaltet.[6]
Monographien über Gret Palucca

Ausgewählte Aufsätze
