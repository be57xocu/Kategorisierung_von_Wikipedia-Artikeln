Nehemiah Grew (* 26. September 1641 in Mancetter Parish, Warwickshire, England; † 25. März 1712 in London) war ein englischer Arzt, Botaniker und Pflanzenanatom.

Grew studierte zunächst Philosophie an der Universität Cambridge, dann Medizin an der Universität Leiden, wo er 1671 zum Dr. med. promovierte. Er arbeitete nach seinem Studium als Arzt in Coventry, ab 1672 in London. Von 1677 bis 1679 amtierte er als Sekretär der Royal Society in London.

Grew beschäftigte sich sehr erfolgreich mit der Anatomie der Pflanzen, die ihn als Mikroskopiker bekannt machten und zählt zu den Begründern der Pflanzenanatomie. Seine Forschungstätigkeit galt unter anderem der Suche nach strukturellen Gemeinsamkeiten von Pflanze und Tier.[1] Er erkannte, dass Pflanzen aus Zellen aufgebaut sind. Er unterschied parenchymatisches Gewebe von den longitudinal gestreckten Faserformen, beschrieb die echten Gefäße und die saftführenden Kanäle und wies (gleichzeitig, jedoch wesentlich sorgsamer als Marcello Malpighi) die Zusammenlegung dieser Gewebeformen in den verschiedenen Organen der Pflanzen nach. Auch über den Bau der Spiralgefäße hatte er klarere Vorstellungen.

Mit seinem Mikroskop untersuchte er auch die Fortpflanzungsorgane der Pflanzen und beschrieb die von ihnen erzeugten Pollenkörner.

Grew entdeckte im übrigen, dass sich die Menschen in ihren Fingerabdrücken unterscheiden. Hierzu veröffentlichte er 1684 die erste Arbeit weltweit zu diesem Thema, wobei er charakteristische Merkmale wie Hautrillen, Furchen, Täler und Porenstrukturen der Fingerabdrücke erläuterte.

Carl von Linné benannte Grew zu Ehren die Gattung der Sternbüsche (Grewia), die heute in die Pflanzenfamilie der Malvengewächse (Malvaceae) eingeordnet ist.[2][3][4]