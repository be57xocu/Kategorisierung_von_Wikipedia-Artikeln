Lori Anne Loughlin (* 28. Juli 1964 in Queens, New York City) ist eine US-amerikanische Schauspielerin.

Loughlin wuchs in Hauppauge, Long Island auf. Im Alter von elf Jahren begann ihre Karriere als Fotomodell. Während ihrer frühen Jugendzeit erschien ihr Bild in zahlreichen Werbekatalogen und -prospekten. Sie wurde auch für Fernsehwerbespots engagiert.

Von 1989 bis 1996 war Loughlin mit Michael Burns verheiratet. Am 25. November 1997 heiratete sie den Designer Mossimo Giannulli, mit dem sie zwei Kinder hat.[1]
1980 bekam sie die Rolle der Emmeline in Die blaue Lagune. Von dieser trat sie später zurück, und die Rolle ging an Brooke Shields. Im selben Jahr bekam Loughlin die Rolle der 18-jährigen Jody Travis in der ABC-Seifenoper The Edge of the Night, die sie bis 1983 spielte. Es folgten Kino- und Spielfilme wie beispielsweise der Horrorfilm Amityville III. Loughlins bekannteste Rolle ist die Rebecca Donaldson in der Familienserie Full House, in der sie von 1988 bis zum Ende der Sitcom 1995 mitwirkte.

Loughlin spielte bereits neben Jim Carrey, Meg Ryan oder Keanu Reeves. Von 2004 bis 2005 hatte sie beim US-Sender The WB eine eigene Fernsehserie, Summerland Beach. Sie spielte die Hauptrolle und war zudem Drehbuchautorin und Produzentin der Serie. Die Serie handelt von Ava, einer Tante von drei Kindern, deren Eltern bei einem Autounfall ums Leben kamen. 2006 drehte sie an der Seite von Greg Germann eine ABC-Comedy mit dem Titel In Case of Emergency. Von 2008 bis 2011 spielte sie eine Hauptrolle in der Fernsehserie 90210, einem Spin-off von Beverly Hills, 90210.
