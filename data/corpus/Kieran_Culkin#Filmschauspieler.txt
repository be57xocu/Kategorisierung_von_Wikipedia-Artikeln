Kieran Kyle Culkin (* 30. September 1982 in New York City, New York) ist ein US-amerikanischer Film- und Theaterschauspieler.

Kieran Culkin wurde als Sohn des Filmschauspielers Christopher „Kit“ Culkin und Patricia Brentrup geboren. Fünf seiner sechs Geschwister sind ebenfalls Schauspieler, von denen Macaulay besonders bekannt ist. Seine übrigen Geschwister heißen Rory, Quinn, Christian, Shane und Dakota Culkin († 2008).

Parallel zu seiner Arbeit vor der Filmkamera stand er auch auf Theaterbühnen. Eines seiner letzten Engagements fand im März 2005 am Broadway statt. Hier wirkte er an der Seite von Anna Paquin in After Ashley mit. Nach vier Jahren Pause begann er 2006 wieder mit Filmdrehs: Margaret an der Seite von Anna Paquin und Matt Damon.

Nominiert für einen Golden Globe, zwei Young Artist Awards und einen MTV Movie Award, dies ist nur eine Auswahl der Ehrungen mit denen Kieran Culkin bedacht wurde. Gewonnen hat Culkin bisher unter anderem einen Satellite Award.
