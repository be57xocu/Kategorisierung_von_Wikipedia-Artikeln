Kathryn Ann Bigelow [ˈkʰæθɹɪn ʔæn ˈbɪgəloʊ̯] (* 27. November 1951 in San Carlos, Kalifornien) ist eine US-amerikanische Filmregisseurin und Oscar-Preisträgerin.

Kathryn Bigelow ist das einzige Kind einer Bibliothekarin und eines Farbenfabrikmanagers. Sie studierte zunächst zwei Jahre lang am San Francisco Art Institute, bevor sie 1971 ein Stipendium des renommierten Studienprogramms des Whitney Museums of American Art gewann und nach New York zog. Sie war zeitweise Mitglied der Avantgarde-Künstlergruppe Art & Language. An der Columbia University studierte sie Film. Als Abschlussarbeit drehte sie dort 1978 den 20-minütigen Kurzfilm The Set-Up.

1982 folgte ihr erster, gemeinsam mit Monty Montgomery realisierter Langfilm, das Bikerdrama The Loveless mit Willem Dafoe in der Hauptrolle. 1983 übernahm sie eine Hauptrolle in Lizzie Bordens feministischem Science-Fiction-Film Born in Flames. Bigelows nächste Regiearbeit, das düstere Vampirdrama Near Dark, wurde im September 1987 auf dem Toronto Film Festival uraufgeführt. Im Anschluss drehte Bigelow ein Musikvideo für die Band New Order (Touched by the Hand of God) und übernahm eine Rolle als Bikerin in James Camerons Musikvideo für Bill Paxtons Band Martini Ranch.

Bigelows nächste Kinofilme waren der kontroverse Serienkiller-Thriller Blue Steel (1989) mit Jamie Lee Curtis und der Actionfilm Gefährliche Brandung (1991) mit Keanu Reeves und Patrick Swayze. Zwischen ihren Kinofilmen dreht sie auch gelegentlich fürs Fernsehen, so für die Krimiserie Homicide und die von Oliver Stone produzierte Serie Wild Palms.

Im Jahr 2008 arbeitete Bigelow gemeinsam mit dem Drehbuchautor Mark Boal an dem Kriegsdrama Tödliches Kommando – The Hurt Locker zusammen, das von einer Einheit des US-Kampfmittelräumdienstes im Irakkrieg berichtet. Der Film erhielt großes Lob seitens der Fachkritik und brachte Bigelow den Directors Guild of America Award und den British Academy Film Award ein. Bei der Oscarverleihung 2010 erhielt der Film neun Nominierungen. Als erste Frau gewann Kathryn Bigelow den Oscar für die Beste Regie, und einen weiteren in ihrer Funktion als Produzentin in der Kategorie Bester Film. Insgesamt erhielt der Film sechs Oscars, darunter in den Kategorien Bestes Originaldrehbuch, Bester Ton, Bester Tonschnitt und Bester Schnitt.

Nach Tödliches Kommando – The Hurt Locker folgte eine erneute Zusammenarbeit mit Mark Boal an dem Spielfilm Zero Dark Thirty (2012). Der Thriller, der eine junge CIA-Analytikerin (dargestellt von Jessica Chastain) auf der Jagd nach Osama bin Laden in den Mittelpunkt stellt, startete am 19. Dezember 2012 in den US-amerikanischen Kinos und brachte Bigelow nach 2009 abermals den New York Film Critics Circle Award für Film und Regie sowie den National Board of Review Award in den gleichen Kategorien ein.

Bigelow ist eine von wenigen weiblichen Action-Regisseurinnen.

Von 1989 bis 1991 war sie mit James Cameron verheiratet, der auch das Drehbuch für ihren Film Strange Days (1995) schrieb.

Near Dark − Die Nacht hat ihren Preis

Strange Days

Tödliches Kommando – The Hurt Locker 

Zero Dark Thirty
