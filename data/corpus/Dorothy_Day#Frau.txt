Dorothy Day (* 8. November 1897 in Brooklyn, New York; † 29. November 1980 in New York) war eine US-amerikanische christliche Sozialistin und Journalistin.

Drei Jahre nach ihrem Tod wurde ihre Seligsprechung vorgeschlagen. Im Jahre 2000 bevollmächtigte Papst Johannes Paul II. das Erzbistum New York, das Seligsprechungsverfahren zu eröffnen.

Dorothy Day wurde als Tochter eines Sportreporters geboren. Sie graduierte mit 16 Jahren an der Robert Waller High School und gewann ein Stipendium der University of Illinois, wo sie Mitglied der Sozialistischen Partei Amerikas wurde. Sie wurde Journalistin, die für linke Blätter schrieb.[1] In Kalifornien trat sie in die Kommunistische Partei der USA ein und war eine der Pioniere der Partei in diesem Bundesstaat. Bis 1927 blieb sie radikale Anhängerin des Kommunismus und wurde später Vertreterin eines christlichen Anarchismus, als sie 1928 zum Katholizismus konvertierte. Sie ist gemeinsam mit Peter Maurin Gründerin der Catholic-Worker-Bewegung.

Als überzeugte Frauenrechtlerin und Pazifistin wurde sie mehrere Male inhaftiert, weil sie politische Entwicklungen nicht mit ihrem Gewissen und ihrem Glauben vereinbaren konnte. Zuletzt war sie 1973 – im Alter von 75 Jahren – im Gefängnis, weil sie an einer verbotenen Streikpostenkette teilgenommen hatte, um César Chávez und die United Farm Workers in Kalifornien zu unterstützen.

Dorothy Day ist Autorin zahlreicher Bücher, Gründerin des St. Joseph’s House of Hospitality und der Zeitung The Catholic Worker in New York. In ihrer Biographie Gegenwind widmet Dorothee Sölle ihr ein eigenes Kapitel. Day war die Schwester des Osteuropareporters Donald Day, der zwischen 1921 und 1942 für die Chicago Tribune aus Riga berichtete.
