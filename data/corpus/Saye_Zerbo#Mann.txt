Saye Zerbo (* 27. August 1932 in Tougan; † 19. September 2013[1]) war von 1980 bis 1982 Staats- und Regierungschef von Obervolta, dem heutigen Burkina Faso.

Zerbo stammte aus der Provinz Samo im Westen des Landes. Er ging in Mali und Saint-Louis in Senegal zur Schule. Danach trat er in die französische Armee ein und wurde an der Militärakademie von Saint-Cyr ausgebildet. Als Fallschirmjäger nahm er am Indochinakrieg und am Algerienkrieg teil. Nach der Unabhängigkeit Obervoltas von Frankreich am 5. August 1960 wechselte er 1961 in dessen Armee.

In der Militärregierung des seit 1966 regierenden Präsidenten Sangoulé Lamizana war er von 1974 bis 1976 Außenminister. Weitere Funktionen waren Regimentskommandeur in der Hauptstadt Ouagadougou und Leiter des militärischen Nachrichtendienstes. Am 25. November 1980 stürzte er Lamizana, der im Mai 1978 in einer demokratischen Wahl im Amt bestätigt worden war, durch einen Putsch und übernahm selbst die Position des Staatsoberhaupts und Regierungschefs. Die 1977 eingeführte Verfassung wurde suspendiert und der Militärrat Comité Militaire de Redressement pour le Progrès National (CMPRN) etabliert. Gegen seine Machtübernahme opponierten vor allem die Gewerkschaften, die Lamizana eine Zeit lang unterstützt hatten. Ein weiterer Putsch am 7. November 1982 beendete seine Amtszeit. Sein Nachfolger wurde Dr. Jean-Baptiste Ouédraogo als Chef des Militärrates Conseil du Salut du Peuple (CSP).

Nach seinem Sturz wurde Zerbo inhaftiert. Ouédraogo wurde am 4. August 1983 durch Thomas Sankara gestürzt und im Mai 1984 wurden die ehemaligen Staatschefs Lamizana und Zerbo wegen diverser Vergehen während ihrer Amtszeit vor Gericht gestellt. Zerbo erhielt fünfzehn Jahre Haft. Während seiner Haft trat er vom Islam zum Christentum über. Im August 1985 wurde er aus dem Gefängnis entlassen. Sankara verlor am 15. Oktober 1987 Amt und Leben durch den nächsten Putsch unter Führung seines bisherigen Gefolgsmannes Blaise Compaoré. Dieser holte gelegentlich Zerbos Rat ein. Das Urteil des „Volkstribunals“ von 1984 wurde am 18. Februar 1997 vom Obersten Gericht in einem Revisionsverfahren aufgehoben. Zerbo lebte bis zu seinem Tod von seiner Pension als Oberst und ehemaliges Staatsoberhaupt.

Zerbos dritte Tochter Araba Kadiatou Zerbo ist mit dem ehemaligen Premierminister Paramanga Ernest Yonli verheiratet.

Fischer Weltalmanach – Biographien zur Zeitgeschichte seit 1945, Fischer Taschenbuch Verlag 1985, ISBN 3-596-24553-2

Maurice Yaméogo |
Sangoulé Lamizana |
Saye Zerbo |
Jean-Baptiste Ouédraogo |
Thomas Sankara |
Blaise Compaoré |
Honoré Traoré (interim) |
Isaac Zida (interim) |
Michel Kafando (interim) |
Gilbert Diendéré (interim) |
Michel Kafando (interim) |
Roch Marc Kaboré

Daniel Ouezzin Coulibaly |
Maurice Yaméogo |
Gérard Kango Ouédraogo |
Sangoulé Lamizana |
Joseph Conombo |
Saye Zerbo |
Thomas Sankara |
Youssouf Ouédraogo |
Roch Marc Kaboré |
Kadré Désiré Ouédraogo |
Paramanga Ernest Yonli |
Tertius Zongo |
Luc-Adolphe Tiao |
Isaac Zida |
Paul Kaba Thieba
