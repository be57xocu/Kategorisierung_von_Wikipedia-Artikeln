Jacob Joachim „Jack“ Klugman (* 27. April 1922 in Philadelphia, Pennsylvania; † 24. Dezember 2012 in Northridge, Kalifornien[1]) war ein US-amerikanischer Film- und Fernsehschauspieler. Bekannt wurde er vor allem durch die Titelrolle des Dr. R. Quincy in der Fernsehserie Quincy.

Klugman stammt aus einer jüdischen Kleinbürgerfamilie in Philadelphia und begann seine Karriere, nachdem er im Zweiten Weltkrieg bei der United States Army gedient hatte. In seiner Zeit als aufstrebender Schauspieler in New York City in den späten 1940er Jahren teilte sich Klugman zeitweilig ein Apartment mit seinem Kollegen Charles Bronson (damals noch Charles Dennis Buchinsky).

Klugman wurde vor allem durch seine Rolle als Sportjournalist Oscar Madison in der US-Fernsehserie Männerwirtschaft (orig. The Odd Couple) bekannt, die in Deutschland vom ZDF ausgestrahlt wurde. Sein Partner in der Serie war Tony Randall als Felix Unger. Die Serie wurde von 1970 bis 1975 produziert und basierte auf Neil Simons Theaterstück „Ein seltsames Paar“ (1965), das 1968 mit Jack Lemmon und Walter Matthau verfilmt wurde.

Daran anschließend erhielt Klugman weitere Angebote für Rollen in komödiantischen Fernsehserien, entschloss sich aber nach dem Erfolg von Männerwirtschaft bewusst dagegen. Stattdessen nahm er die Rolle des Gerichtsmediziners Dr. R. Quincy in der gleichnamigen Fernsehserie Quincy an, die von 1976 bis 1983 produziert wurde und die für ihn zu einem gleichermaßen großen Erfolg wurde.

Seit dem Tod von Jack Warden am 19. Juli 2006 war Klugman der letzte noch lebende Darsteller der Besetzung aus dem Justizdrama Die zwölf Geschworenen von Sidney Lumet. Ursprünglich sollte Klugman im März 2012 nochmals im Bühnenstück „Die zwölf Geschworenen“ mitspielen. Wenige Tage zuvor sagte er aber aus gesundheitlichen Gründen ab und starb noch im selben Jahr.

Wie Oscar Madison in Männerwirtschaft war Klugman auch im wirklichen Leben ein Fan von Pferderennen. Eines seiner Pferde, Jaklin Klugman, gewann 1980 nach einigen Siegen mehrere Auszeichnungen.

1984 wurde bei Klugman Kehlkopfkrebs diagnostiziert. 1989 wurde er an den Stimmbändern operiert, wobei ein Teil seines Kehlkopfs entfernt werden musste.[2] Danach war Klugman gezwungen, das Sprechen neu zu erlernen.

2005 veröffentlichte Klugman ein Buch über die langjährige Freundschaft mit seinem Kollegen Tony Randall: Tony And Me: A Story of Friendship. Klugman sagt darin über Randall, dass dieser der beste Freund gewesen sei, den er je gehabt habe. Er schrieb über ihre immer funktionierende Beziehung zueinander und darüber, wie gut Randall nach seiner Krebsoperation zu ihm gewesen sei.

Jack Klugman heiratete 1953 Brett Somers, mit der er zwei Söhne bekam. 1974 trennten sich die beiden, ließen sich jedoch nie scheiden.[3]
Am 2. Februar 2008 heiratete er im Alter von 85 Jahren seine langjährige Lebensgefährtin Peggy Crosby, nachdem Somers am 15. September 2007 verstorben war.

Jack Klugman verstarb am 24. Dezember 2012 im Alter von 90 Jahren. Die Zeremonie und Verabschiedung fand auf dem jüdischen Friedhof Mount Sinai Cemetery in Los Angeles statt. Die Urne selber soll sich im Besitz der Familie befinden. Berichten zufolge soll er im Westwood Village Memorial Park Cemetery bestattet worden sein.[4]