Christoph Peters (* 11. Oktober 1966 in Kalkar) ist ein deutscher Schriftsteller.

Christoph Peters war von 1977 bis zum Abitur 1986 Schüler des katholischen Internatsgymnasiums Collegium Augustinianum Gaesdonck, wo Franz Joseph van der Grinten einer seiner Lehrer war. Danach studierte Peters von 1988 bis 1994 Malerei an der Staatlichen Akademie der Bildenden Künste Karlsruhe, unter anderem bei Horst Egon Kalinowski und Günter Neusel, zuletzt als Meisterschüler von Meuser. Von 1995 bis 2000 arbeitete er als Fluggastkontrolleur am Frankfurter Rhein-Main-Flughafen.

Peters war bis 2000 in Mainz ansässig, seitdem wohnt er in Berlin. Mit seiner Ehefrau, der Schriftstellerin Veronika Peters, hat er eine Tochter (* 2003). Sein Roman Das Tuch aus Nacht (2003) wurde ins Amerikanische und ins Chinesische übersetzt.

Peters ist Mitglied des PEN-Zentrums Deutschland.
