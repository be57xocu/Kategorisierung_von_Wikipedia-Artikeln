Monique Laederach (* 16. Mai 1938 in Les Brenets (NE); † 17. März 2004 in Peseux bei Neuenburg) war eine Schweizer Schriftstellerin und Literaturkritikerin.

Laederach schrieb sowohl auf Deutsch als auch auf Französisch. Sie war auch als Übersetzerin tätig und übertrug Werke von Adolf Muschg und Erika Burkart ins Französische.
