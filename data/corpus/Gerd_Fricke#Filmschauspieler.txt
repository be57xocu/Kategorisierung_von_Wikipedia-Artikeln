Gerd Fricke, auch Gert Fricke, (* 10. August 1890; † nach 1960) war ein deutscher Hörspielregisseur, Hörspielsprecher, Hörfunkmoderator und Schauspieler. 

Fricke absolvierte die Schauspielschule des Deutschen Theaters in Berlin unter Leitung von Max Reinhardt. Anschließend begann er zunächst eine Karriere als Theaterschauspieler. Das Deutsche Bühnenjahrbuch verzeichnet in seinen entsprechenden Jahrgängen folgende Stationen seiner Laufbahn:

Bereits im Jahre 1924, als er in Frankfurt am Main auf der Bühne stand, kam er als Sprecher zum dortigen Südwestdeutschen Rundfunkdienst AG (SÜWRAG), dem Vorläufer des Hessischen Rundfunks. Als seine ersten Auftritte in einem Sendespiel, wie die Hörspiele damals noch genannt wurden, verzeichnet die ARD-Hörspieldatenbank Arthur Schnitzlers Abschiedssouper und Frage an das Schicksal, aus dem Anatol-Zyklus mit Fricke in der Hauptrolle. Die Produktionen wurden am 16. September und 1. Oktober 1924 live ohne Aufzeichnung gesendet. Als erste Regiearbeit ist eine Adaption von Goethes Theaterstück Stella  vom 19. März 1925 beim Frankfurter Sender nachgewiesen.

Wenige Jahre später ging er zum Deutschlandsender nach Berlin, wo er zunächst ebenfalls als Sprecher und Regisseur, später dann auch als Oberspielleiter tätig war. Zu seinen großen Erfolgen in dieser Zeit zählen Josef Martin Bauers Das tote Herz, Hans Rothes Verwehte Spuren, Ludwig Tügels Treue, Günter Eichs Weizenkantate und Alfred Karraschs Winke, bunter Wimpel.

In seiner Funktion als Oberspielleiter nahm er Kontakt zu vielen Autoren auf, um sie für das Genre Hörspiel zu gewinnen. Dadurch hatte er maßgeblichen Anteil am Erfolg dieser Kunstform gehabt.

Daneben konzipierte er eine Reihe von legendär gewordenen Unterhaltungssendungen, die er auch selber moderierte, wie beispielsweise Guten Morgen lieber Hörer!, Olle Kamellen, Beliebte Kapellen und Sonntagsmorgen ohne Sorgen. Einen großen Erfolg hatte er mit seiner Weihnachtssendung  Heut soll keiner einsam sein, die er am Heiligen Abend ab 23:00 Uhr gemeinsam mit Barnabás von Géczy gestaltete.

Nach dem Zweiten Weltkrieg hatte er einen schweren Start, da seine Karriere zunächst nicht richtig in Gang kommen wollte. Erste Sendungen machte er beim Bayerischen Rundfunk und gelegentlich beim HR in Frankfurt. Dann wechselte er zum Süddeutschen Rundfunk in Stuttgart. Dort entwickelte er Sendungen wie Die Fahrt ins Blaue und Meine Freunde – Deine Freunde.

Eine kleine Sensation war Anfang der 1950er Jahre die Sendereihe Vom Hundertsten ins Tausendste. Er verließ dafür die Funkhausstudios und besuchte die Menschen in ihren Heimen, die er erzählen ließ, so wie sie sich einen Abend zusammenfanden. Zu den Prominenten zählten unter anderen der Schriftsteller Walther von Hollander, der Bühnenkünstler Waldemar Staegemann und der langjährige Oberbürgermeister von Ulm Emil Schwamberger.

Für den Nordwestdeutschen Rundfunk Hamburg entwickelte er 1952 die Hörspielreihe Das Gericht zieht sich zur Beratung zurück. Geschildert wurden Kriminalfälle aus dem täglichen Leben. Am Schluss jeder Folge fand eine Diskussionsrunde mit einigen Hörern statt, in der das Für und Wider des Urteils erörtert wurde.  Die Reihe umfasste 79 Folgen, bei denen Gerd Fricke jeweils die Regie führte. Die letzte Folge wurde am 14. Mai 1956 urgesendet.

In der Folgezeit sind bis 1961 noch mehrere Hörspielproduktionen u. a. beim Südwestfunk und beim SR nachgewiesen, bei denen er als Sprecher tätig war.

In den 1920er Jahren trat er auch in einigen Stummfilmen und ab Mitte der 1950er Jahre in verschiedenen Fernsehfilmen auf.
