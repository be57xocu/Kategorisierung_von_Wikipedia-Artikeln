Gabrielle Beaumont (* 7. Juli 1942 in London) ist eine britische Film- und Fernsehregisseurin, Filmproduzentin und Drehbuchautorin.

Gabrielle Beaumont führte bei zahlreichen US-amerikanischen Fernsehserien Regie. Auch übernahm sie eigenständige Film- und TV-Produktionen, so unter anderem 1998 Diana – Königin der Herzen, ein Film, der die Beziehung von Lady Diana mit Dodi Fayed beleuchtet, und ihren Unfalltod im August 1997. Bei diesem Film führte Beaumont nicht nur Regie, sondern schrieb auch das Drehbuch. Außerdem führte sie 1996 Regie über den Fantasyfilm Beastmaster – Das Auge des Braxus.
