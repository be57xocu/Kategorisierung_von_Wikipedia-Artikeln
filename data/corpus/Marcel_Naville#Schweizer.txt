Marcel Alfred Naville (* 12. August 1919 in Genthod; † 8. Oktober 2003 in Meyrin) war ein Schweizer Bankier. Nach einem Studium der Klassischen Philologie arbeitete er während des Zweiten Weltkrieges zunächst für das Eidgenössische Politische Department und für die Rechtsabteilung des Internationalen Komitees vom Roten Kreuz (IKRK). Nach dem Krieg wechselte er in den Bankdienst. 1969 wurde er vom IKRK zu dessen Präsidenten gewählt und hatte dieses Amt bis 1973 inne.

Marcel Naville wurde am 12. August 1919 in Genthod geboren. Er studierte an der Universität Genf Klassische Philologie und beendete das Studium mit dem Lizenziat (Licencié ès Lettres classiques), einem dem Magister vergleichbaren Abschluss. Während des Zweiten Weltkrieges war er für das Eidgenössische Politische Department und für die Rechtsabteilung des Internationalen Komitees vom Roten Kreuz tätig. Nach dem Ende des Krieges hielt er sich zu weiteren Studien in Rom und Paris auf. Er begann anschließend eine Karriere im Bankdienst und war ab 1965 Direktor der Genfer Filiale der Schweizerischen Nationalbank.

Naville war verheiratet mit Béatrice Naville geb. Vernet. Zusammen hatten sie zwei Töchter und einen Sohn.

Im Jahr 1967 wurde Naville als Mitglied des Internationalen Komitees vom Roten Kreuz kooptiert. Bereits sein Großvater Edouard Naville, der von 1898 bis 1922 dem Komitee angehörte und während des Ersten Weltkrieges dessen Internationale Zentralstelle für Kriegsgefangene geleitet hatte, war von 1916 bis 1920 Vizepräsident sowie zeitweise Interimspräsident des Komitees gewesen. Im Januar 1969 wurde Naville einstimmig zum Präsidenten des IKRK und damit zum Nachfolger von Samuel Gonard als  gewählt, der das Amt aus Altersgründen aufgegeben hatte.

Während seiner Amtszeit kam es zum Einsatz des IKRK im Biafra-Krieg, einer Auseinandersetzung um die Unabhängigkeit des Gebietes Biafra von Nigeria. Dieser Konflikt stellte das IKRK vor seine größte Herausforderung seit dem Zweiten Weltkrieg. Seine Arbeit war durch die traditionelle stille Diplomatie und strikte Neutralität gekennzeichnet. Unzufriedenheit mit diesem Vorgehen führte 1971 zur Gründung der Hilfsorganisation Médecins Sans Frontières (Ärzte ohne Grenzen) durch französische Ärzte um Bernard Kouchner. Auch zeigte sich, dass insbesondere das Französische und das Schwedische Rote Kreuz teilweise einseitig die Bevölkerung in Biafra unterstützten. Dies war bedingt durch Versuche der Rebellen in Biafra, die öffentliche Meinung in Europa zu ihren Gunsten zu beeinflussen. Der Einsatz des IKRK in Biafra offenbarte durch diese Entwicklungen auch Probleme innerhalb der Führungsspitze des IKRK bei der Koordinierung des Einsatzes sowie hinsichtlich des Führungsanspruchs des Komitees innerhalb der Internationalen Rotkreuz-Bewegung.

Zu den weiteren Aktivitäten Navilles in seiner Funktion als IKRK-Präsident gehörten Besuche in Polen, in der Sowjetunion sowie in den USA, wo er mit dem damaligen Präsidenten Richard Nixon über die kriegsbedingten humanitären Probleme in Vietnam sprach. Im September 1971 reiste er nach China für Gespräche mit Vertretern der Regierung und der nationalen Rotkreuz-Gesellschaft des Landes über Probleme bei der Akzeptanz des Roten Kreuzes, die infolge der Kulturrevolution entstanden waren.

Im Juli 1973 wurde der Genfer Arzt Eric Martin zum Nachfolger von Naville im Amt des Präsidenten des IKRK gewählt. Diese Entscheidung galt als überraschend, da bisher fast alle Präsidenten des Komitees auf eigenen Wunsch aus dem Amt ausgeschieden sind. Zudem gehört es zu den Gepflogenheiten innerhalb des Komitees, einem amtierenden Präsidenten eine zweite Amtsperiode zu gewähren, wenn dies seinem Wunsch entspricht.

Guillaume-Henri Dufour (1863–1864) |
Gustave Moynier (1864–1910) |
Gustave Ador (1910–1928) |
Max Huber (1928–1944) |
Carl Jacob Burckhardt (1945–1948) |
Paul Ruegger (1948–1955) |
Léopold Boissier (1955–1964) |
Samuel Gonard (1964–1969) |
Marcel Naville (1969–1973) |
Eric Martin (1973–1976) |
Alexandre Hay (1976–1987) |
Cornelio Sommaruga (1987–1999) |
Jakob Kellenberger (2000–2012) |
Peter Maurer (seit 2012)
