Claude Simon (* 10. Oktober 1913 in Tananarive, Madagaskar; † 6. Juli 2005 in Paris) war ein französischer Schriftsteller. Er erhielt 1985 den Nobelpreis für Literatur.

Simon wurde auf der damaligen französischen Kolonie Madagaskar als Sohn eines Offiziers der Marineinfanterie geboren. 1914 kehrte die Familie nach Frankreich zurück, der Vater fiel zu Beginn des Ersten Weltkriegs in Flandern. Claude Simon wuchs bei Perpignan, der Heimat seiner Mutter, auf.

Als 1924 die Mutter starb, wurde einer ihrer Vettern, ein Kavallerieoffizier im Ruhestand, Vormund des Elfjährigen. Er kam auf das katholische Collège Stanislas in Paris. Nach Beendigung der Schule besuchte Simon Kurse für Malerei. 1934/35 leistete er Militärdienst im 31e régiment de dragons. Nach seiner Entlassung aus dem Militär erhielt er durch einen Freund einen Mitgliedsausweis der Kommunistischen Partei Frankreichs. Er reiste damit nach Barcelona ins Hauptquartier der republikanischen Seite im Spanischen Bürgerkrieg (später Thema seines Romans Le Palace), kehrte aber nach zwei Wochen nach Frankreich zurück, weil er dort keine sinnvolle Aufgabe für sich fand. In Frankreich gehörte er zu einer Gruppe, die einen Waffentransport als Nachschub für die Republikaner organisierte. 1937 begab er sich auf eine große Europareise (Berlin, Warschau, Sowjetunion, Istanbul, Griechenland, Norditalien). Er unternahm erste, von ihm als unbedeutend eingestufte literarische Versuche.

Angesichts des heraufziehenden Zweiten Weltkriegs wurde Simon Ende August 1939 wieder in seine alte Kavallerieeinheit eingezogen. Im Krieg wurde seine Schwadron in Belgien aufgerieben, er selbst gefangen genommen und ins Kriegsgefangenenlager Stalag IV B bei Mühlberg/Elbe deportiert. Seine Kriegserfahrungen verarbeitete er später in verschiedenen Romanen (La Route des Flandres, Les Géorgiques, L'Acacia). Simon gelang es, in ein anderes Kriegsgefangenenlager für Kolonialfranzosen in Frankreich verlegt zu werden, von wo aus er flüchten konnte. Er gelangte ins im von den Deutschen unbesetzten Teil Frankreichs gelegene Perpignan. Als die Behörden der Vichy-Regierung auf ihn aufmerksam geworden waren, begab er sich 1944 nach Paris, um seiner Verhaftung zu entgehen.

In dieser Zeit entstand sein erster Roman Le Tricheur (Der Betrüger), der 1945 veröffentlicht wurde und genauso unbeachtet blieb wie die drei folgenden, die bis Mitte der 1950er Jahre entstanden. 1956 lernte er Alain Robbe-Grillet kennen, der für den Verlag Éditions de Minuit arbeitete und ihm vorschlug, dort zu veröffentlichen. Der Verlag war die Keimzelle einer literarischen Bewegung, die später als Nouveau Roman bekannt wurde. In der Folgezeit wuchsen Simons Bekanntheit und die ihm gezollte künstlerische Anerkennung, seine Texte wurden nun auch in andere Sprachen übersetzt. 1961 erhielt er für La Route des Flandres den Preis der Zeitschrift L’Express, 1967 den Prix Médicis für Histoire. 1985 wurde ihm zur Überraschung weiter Teile der Fachwelt und der Öffentlichkeit der Nobelpreis für Literatur verliehen. 1987 wurde Simon in die American Academy of Arts and Sciences gewählt. 1996 erhielt er das Österreichische Ehrenzeichen für Wissenschaft und Kunst.

Simons literarisches Werk besteht hauptsächlich aus Romanen. Als Teil der literarischen Bewegung des Nouveau roman weichen seine Werke deutlich von den klassischen Beispielen dieser Gattung aus dem 19. Jahrhundert ab, typisch ist z. B. das Fehlen eines Allwissenden Erzählers, der den Leser durch eine spannende oder zumindest interessante Geschichte mit einer chronologischen, durchgehenden Handlung führt. Auch der Erzähler scheint keine tiefere Einsicht in das Geschehen zu besitzen, er beschränkt sich weitgehend auf äußerliche Beschreibung, ohne dass die Einzelheiten deutlich erkennbar in einen übergeordneten Handlungsstrang eingeordnet erscheinen. Eine psychologische Charakterisierung der Romanfiguren bzw. Erklärung ihrer Handlungen findet nicht statt, stattdessen erfährt man in umfangreichen Passagen aus Innerem Monolog und Erlebter Rede etwas über Gedanken und Motive einiger Protagonisten. Der Roman insgesamt ist aus einer Aneinanderreihung zahlreicher fragmentierter Handlungsstränge aufgebaut, die unvermittelt abbrechen (bisweilen mitten im Satz) und später im Text wieder aufgenommen werden können. Die Stränge sind inhaltlich durch ein dichtes Beziehungsgeflecht miteinander verbunden, ohne dass sich allerdings so etwas wie eine chronologische Handlung ergibt.

Wiederkehrende Themen in Simons Romanen sind Geschichte einschließlich der eigenen Familiengeschichte, Krieg und die darin gemachte Erfahrung extremer Gewalt.

Prudhomme (1901) |
Mommsen (1902) |
Bjørnson (1903) |
F. Mistral/Echegaray (1904) |
Sienkiewicz (1905) |
Carducci (1906) |
Kipling (1907) |
Eucken (1908) |
Lagerlöf (1909) |
Heyse (1910) |
Maeterlinck (1911) |
Hauptmann (1912) |
Tagore (1913) |
nicht verliehen (1914) |
Rolland (1915) |
Heidenstam (1916) |
Gjellerup/Pontoppidan (1917) |
nicht verliehen (1918) |
Spitteler (1919) |
Hamsun (1920) |
France (1921) |
Benavente (1922) |
Yeats (1923) |
Reymont (1924) |
Shaw (1925) |
Deledda (1926) |
Bergson (1927) |
Undset (1928) |
Mann (1929) |
Lewis (1930) |
Karlfeldt (1931) |
Galsworthy (1932) |
Bunin (1933) |
Pirandello (1934) |
nicht verliehen (1935) |
O’Neill (1936) |
Martin du Gard (1937) |
Buck (1938) |
Sillanpää (1939) |
nicht verliehen (1940–1943) |
Jensen (1944) |
G. Mistral (1945) |
Hesse (1946) |
Gide (1947) |
Eliot (1948) |
Faulkner (1949) |
Russell (1950) |
Lagerkvist (1951) |
Mauriac (1952) |
Churchill (1953) |
Hemingway (1954) |
Laxness (1955) |
Jiménez (1956) |
Camus (1957) |
Pasternak (1958) |
Quasimodo (1959) |
Perse (1960) |
Andrić (1961) |
Steinbeck (1962) |
Seferis (1963) |
Sartre (1964) |
Scholochow (1965) |
Agnon/Sachs (1966) |
Asturias (1967) |
Kawabata (1968) |
Beckett (1969) |
Solschenizyn (1970) |
Neruda (1971) |
Böll (1972) |
White (1973) |
Johnson/Martinson (1974) |
Montale (1975) |
Bellow (1976) |
Aleixandre (1977) |
Singer (1978) |
Elytis (1979) |
Miłosz (1980) |
Canetti (1981) |
García Márquez (1982) |
Golding (1983) |
Seifert (1984) |
Simon (1985) |
Soyinka (1986) |
Brodsky (1987) |
Mahfuz (1988) |
Cela (1989) |
Paz (1990) |
Gordimer (1991) |
Walcott (1992) |
Morrison (1993) |
Ōe (1994) |
Heaney (1995) |
Szymborska (1996) |
Fo (1997) |
Saramago (1998) |
Grass (1999) |
Gao (2000) |
Naipaul (2001) |
Kertész (2002) |
Coetzee (2003) |
Jelinek (2004) |
Pinter (2005) |
Pamuk (2006) |
Lessing (2007) |
Le Clézio (2008) |
Müller (2009) |
Vargas Llosa (2010) |
Tranströmer (2011) |
Mo (2012) |
Munro (2013) |
Modiano (2014) |
Alexijewitsch (2015) |
Dylan (2016) |
Ishiguro (2017)
