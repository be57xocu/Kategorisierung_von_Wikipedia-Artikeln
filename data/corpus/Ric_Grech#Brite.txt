Richard Roman Grechko (* 1. November 1946 in Bordeaux; † 17. März 1990) war ein britischer Rockmusiker (Bass, Violine). Als Künstler trat er unter den Namen Ric Grech bzw. Rick Grech auf.

Ric Grech schloss sich 1965 der Band Farinas an, die 1962 von Charlie Whitney gegründet worden war. Nachdem Roger Chapman hinzustieß, benannten sie sich 1967 in Family um.

Im Februar 1969 verließ Grech Family, um als viertes Bandmitglied neben Eric Clapton, Steve Winwood und Ginger Baker die – nach der Auflösung von Cream gegründete – neue Supergroup Blind Faith zu komplettieren. Nach dem frühen Ende von Blind Faith im September 1969 nahm Grech zusammen mit Clapton, George Harrison, Denny Laine und Trevor Burton einige Songs auf, die jedoch nicht zur Veröffentlichung gelangten.

Grech schloss sich daher Ginger Baker’s Air Force an, die damit quasi zur Nachfolgeband von Blind Faith wurde. Im Sommer 1970 spielte er mit John Mayall, Peter Green und Aynsley Dunbar beim Bath Festival. Kurz darauf, im August 1970, wurde Grech Mitglied von Winwoods Band Traffic, bei der er bis Dezember 1971 blieb.

Im Januar 1973 spielte Grech bei einer kurzlebigen Band namens The Palpitations, in der u. a. Eric Clapton, Ron Wood, Pete Townshend und Steve Winwood mitspielten. Im gleichen Jahr spielte er mit den legendären Crickets, der früheren Band von Buddy Holly.

1975 sah die Geburt einer neuen Supergroup mit dem Namen KGB. Neben Grech gehörten ihr Ray Kennedy, Mike Bloomfield, Barry Goldberg und Carmine Appice an. Der erhoffte große Erfolg blieb jedoch aus. 1976 ging Grech mit der Band Square Dancing Machine auf Tour, die aus The Lentones entstanden war.

Danach wurde es ruhig um Ric Grech, der sich aus dem Musikgeschäft zurückgezogen hatte. Er starb 1990 an den Folgen einer Hirnblutung. In anderen Quellen ist von Leber- und Nierenversagen die Rede. Zuvor hatte er in Leicester als Teppichverkäufer gearbeitet.
