Johannes Zweytick (* 24. Mai 1961 in Wagna, Steiermark) ist Winzer und Politiker. Zweytick war von 1994 bis 2008 Abgeordneter zum Nationalrat (ÖVP) und 1999 bis 2014 Bürgermeister von Ratsch an der Weinstraße.

Nach Pflichtschulbesuch in Ratsch und Gamlitz erfolgt die weitere Ausbildung an der landwirtschaftlichen Fachschule Silberberg für Wein- und Obstbau 1976–1979, die Meisterprüfung zum Weinbau- und Kellermeister erfolgt 1983.

1999 wurde er Bürgermeister der Gemeinde Ratsch. 
In der XIX. Legislaturperiode trat Zweytick per 15. Dezember 1994   in den Nationalrat ein. Mit 27. Oktober 2008 zog er sich aus der Parlamentarbeit zurück.
Das Amt eines Bürgermeisters übte er bis zur Auflösung der Gemeinde Ratsch im Rahmen der steiermärkischen Gemeindestrukturreform per Ende 2014 aus. Seither ist er Vizebürgermeister der neuen Gemeinde Ehrenhausen an der Weinstraße.[1]
Johannes Zweytick war Mitglied im
