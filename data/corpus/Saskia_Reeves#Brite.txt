Saskia Reeves (* August 1961 in Paddington, London) ist eine britische Schauspielerin. 

Saskia Reeves wurde in London von ihrer niederländischen Mutter und ihrem englischen Vater aufgezogen, der ebenfalls ein etablierter Schauspieler ist. Schon früh interessierte sie sich für die Schauspielerei, besuchte die Guildhall School of Music and Drama und hat schon mit namhaften Regisseuren wie Mike Leigh, Stephen Poliakoff und Nicholas Hytner gearbeitet.
