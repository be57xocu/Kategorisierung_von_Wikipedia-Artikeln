Eugen Egner (* 10. Oktober 1951 in Ingelfingen) ist ein deutscher Schriftsteller, Zeichner und Musiker.

Egner lebt seit 1955 in Wuppertal. In den 1970er- und 1980er Jahren arbeitete er als Grafiker und Illustrator unter anderem für Die Sendung mit der Maus und betätigte sich als Rockmusiker hauptsächlich in der Band Armutszeugnis unter dem Pseudonym Eugen Euler zusammen mit R.M.E. Streuf sowie als Mensch Oyler gemeinsam mit Tim Buktu.

Die ersten Veröffentlichungen seiner Zeichnungen (Comic-Serie) hatte er 1971 bei Hörzu. Erste Cartoons erschienen 1978 in der Wuppertaler Stadtillustrierten „Kulturmagazin“. 1986 veröffentlichte der Wuppertaler Sisyphos-Verlag den Band „Als die Erlkönige sich Freiheiten herausnahmen“ (Zeichnungen und eine Bildergeschichte). Loriot zeigte das Buch Gerd Haffmans, der Egner daraufhin zur Mitarbeit an der Literaturzeitschrift Der Rabe einlud. Im „Rausch-Raben“ wurde 1989 der fiktive Tagebuchtext „Das letzte Jahr“ veröffentlicht, aus dem später das „Tagebuch eines Trinkers“ hervorging. Ab 1992 erschienen dann auch Egners Bücher im Haffmans Verlag.

Ab 1988 fanden sich seine Zeichnungen und Texte u. a. in den Zeitschriften Titanic (hier wird er seit 1989 als ständiger Mitarbeiter geführt), taz, Der Rabe, Frankfurter Rundschau, Kowalski, Eulenspiegel, Italien, Frankfurter Allgemeine Sonntagszeitung, Die Zeit, Junge Welt, Rolling Stone. Für den Westdeutschen Rundfunk schrieb Egner seither zahlreiche Kurztexte und Hörspiele. Seine Bücher sind bis heute bei etwa einem Dutzend verschiedener Verlage erschienen.

Egners Texte sind vorwiegend im Bereich des Surrealen und Groteske angesiedelt, wobei seine kürzeren Texte oft noch absurder als seine Romane und Erzählungen sind. Häufig werden Realitätswahrnehmung und Bewußtseinsveränderung (Traum, Rausch, Wahn) thematisiert, das „Wunderbare“ und „Andere“.

Auch kann man eine deutliche Beeinflussung durch Kafka, verschiedene Schriftsteller der Romantik (vor allem E.T.A. Hoffmann), Robert Aickman, Thomas Ligotti, Lewis Carroll, Donald Barthelme, Charles Fort sowie Arbeiten von Psychiatriepatienten (z.B. der Prinzhorn-Sammlung und Gugginger Künstler) erkennen.

In seinem 2001 erschienenen Erzählband „Die Eisenberg-Konstante“ deutet sich eine stärkere Hinwendung zur Unheimlichen Phantastik an, die dann mit „Gift Gottes“ (2003) vollzogen war. Egners kurze Texte weisen zwar ebenfalls diese Merkmale auf, sind aber insgesamt nach wie vor komischer und absurder als die Erzählungen.

2006 wandte sich Egner als Gitarrist wieder aktiv der Musik zu. Gründung des Improvisationstrios Gorilla Moon mit Dietrich Rauschtenberger (dr) und Dietmar Wehr (b).

2014 wurde im Wuppertaler Opernhaus nach Egners Romanvorlage die Oper „Der Universums-Stulp,“ eine musikalische Bildgeschichte in drei Heften, uraufgeführt. Die Musik komponierte Stephan Winkler, die Inszenierung besorgte Thierry Bruehl.
