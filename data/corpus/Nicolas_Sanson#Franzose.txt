Nicolas Sanson, der Ältere, auch: Nicolas Sanson d’Abbeville (* 20. Dezember 1600 in Abbeville, Frankreich; † 7. Juli 1667 in Paris) war ein französischer Kartograph. Er gilt als der Vater der Geographie in Frankreich.[1]
Seine Vorfahren stammten aus Schottland. In den Jahren 1618 bis 1667 schuf Sanson etwa 300 einzelne Karten sowie Atlanten und illustrierte Texte. Er wurde zum Geographe Ordinaire du Roy (königlicher Kartenmacher) ernannt und unterrichtete die Könige Ludwig XIII. und Ludwig XIV. in Geographie.[2]
Sein Sohn Nicolas Sanson (1626–1648) starb während der Fronde. Nach des Vaters Tod, führten zwei weitere Söhnen Adrien Sanson († 1708) und Guillaume Sanson († 1703) zusammen mit seinem Neffen Pierre Duval (1619–1683), dem Enkel Pierre Moulard Sanson († 1730) sowie Hubert Jaillot[2] und Gilles Robert de Vaugondy (1688–1766) die Arbeit fort.

1650, Karte Nordamerikas

1681, Karte der Grafschaft Mark

Innerösterreich
