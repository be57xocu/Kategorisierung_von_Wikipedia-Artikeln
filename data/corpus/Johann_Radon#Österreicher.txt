Johann Radon (* 16. Dezember 1887 in Tetschen; † 25. Mai 1956 in Wien) war ein österreichischer Mathematiker.

Radon promovierte 1910 an der Universität Wien zum Doktor der Philosophie. Das Wintersemester 1910/11 verbrachte er auf Grund eines Stipendiums an der Universität Göttingen, wo er u. a. Vorlesungen von David Hilbert hörte. Danach war er Assistent an der Deutschen Technischen Hochschule Brünn und von 1912 bis 1919 Assistent an der Lehrkanzel Mathematik II der Technischen Hochschule in Wien. 1913/14 habilitierte er sich an der Universität Wien: sein Habilitationsantrag ging am 17. Dezember 1913 im Dekanat der Philosophischen Fakultät ein, die Lehrbefugnis für Mathematik wurde ihm am 26. August 1914 erteilt.[1] Der Titel seiner Habilitationsschrift lautete: "Theorie und Anwendung der absolut additiven Mengenfunktionen". Während des Krieges war er vom Militärdienst wegen seiner starken Kurzsichtigkeit befreit.

1919 wurde er als außerordentlicher Professor an die neu gegründete Universität Hamburg berufen, danach wurde er 1922 ordentlicher Professor an der Universität Greifswald und 1925 in Erlangen. Von 1928 bis 1945 war er Ordinarius an der Universität Breslau.

Wegen der drohenden Belagerung durch die rote Armee musste er mit seiner Familie im Januar 1945 Breslau verlassen; sie gelangten auf Umwegen nach Innsbruck, wo eine Schwester seiner Frau lebte. Nach einem Zwischenspiel an der Universität Innsbruck wurde er am 1. Oktober 1946 zum Ordinarius am Mathematischen Institut der Universität Wien ernannt. Im Studienjahr 1954/55 war er Rektor der Universität Wien. Zur feierlichen Inauguration seines Rektorats hielt er am 18. November 1954 eine Ansprache zum Thema "Mathematik und Naturerkenntnis".[2]
Radon wurde 1939 korrespondierendes Mitglied, 1947 wirkliches Mitglied der Österreichischen Akademie der Wissenschaften; von 1952 bis 1956 war er Sekretär der mathematisch-naturwissenschaftlichen Klasse dieser Akademie. Im Jahr 1947 gründete er die Monatshefte für Mathematik neu. Von 1948 bis 1950 war er Präsident der Österreichischen Mathematischen Gesellschaft.

Im Jahr 1916 heiratete Johann Radon Marie Rigele, eine Hauptschullehrerin, die naturwissenschaftliche Fächer unterrichtete. Sie bekamen drei Söhne, die allerdings in jungem Alter starben. Ihre Tochter Brigitte, 1924 geboren, studierte in Innsbruck Mathematik und wurde dort promoviert. 1950 heiratete sie den österreichischen Mathematiker Erich Bukovics.

Radon, wie ihn Curt Christian 1987 anlässlich der Enthüllung der Gedenkbüste beschrieb, war ein liebenswerter, gütiger, bei Schülern und Kollegen in hohem Maße beliebter Mann, eine vornehme Persönlichkeit. Er machte zwar den Eindruck eines stillen Gelehrten, war aber dennoch von geselliger Natur, nicht abgeneigt, Feste zu feiern. Er liebte die Musik und pflegte die Hausmusik, war selbst ein hervorragender Geiger und hatte eine schöne Baritonstimme; seine Liebe zur klassischen Literatur dauerte bis zuletzt an.

Radon war ein äußerst vielseitiger und produktiver Wissenschafter. Mit seinem Namen sind vor allem die Radon-Transformation, die in der Computertomographie verwendet wird, die Radonzahlen, der Satz von Radon, sowie der in der Maßtheorie bedeutsame Satz von Radon-Nikodým und der Satz von Radon-Riesz verbunden.

1921 erhielt er den Richard-Lieben-Preis. 

Die Österreichische Akademie der Wissenschaften hat eine Radon-Medaille initiiert, die an Personen für Beiträge zu Gebieten vergeben werden kann, auf denen Radon arbeitete. Sie wurde 1992 erstmals an Prof. Fritz John (Courant-Institut, New York) vergeben.

Im Jahre 2003 gründete die Österreichische Akademie der Wissenschaften in Linz ein Institute for Computational and Applied Mathematics und benannte es nach Johann Radon (siehe Weblink unten).
