Andy Gibb (* 5. März 1958 als Andrew Roy Gibb in Manchester, England; † 10. März 1988 in Oxford, England) war ein britisch-australischer Sänger.

Andy Gibb wurde als Sohn von Hugh und Barbara Gibb geboren. Seine Mutter war Sängerin, während der Vater ein kleines Orchester leitete.[1] Seine älteren Brüder Barry, Robin und Maurice Gibb wurden als Bee Gees bekannt.

1979 hatte Andy Gibb einen Auftritt mit den Bee Gees (Spirits Having Flown Tour), jedoch war er selbst nie Mitglied der Gruppe. Stattdessen feierte er als Solosänger große Erfolge. Seinen ersten Nummer-eins-Hit in den USA hatte er 1977 mit dem von seinem Bruder Barry geschriebenen Titel I Just Want to Be Your Everything. Zwischen Mai 1977 und April 1978 schaffte er es mit insgesamt drei Liedern an die Spitze der US-Charts. Daneben war er 1978 als Gastsänger auf dem Album Thoroughfare Gap von Stephen Stills zu hören.

In den folgenden Jahren hatte er weitere musikalische Erfolge und kam mit seiner Beziehung zu der Schauspielerin Victoria Principal in die Medien. Mitte der 1980er Jahre trennten sich die beiden, und der mittlerweile drogenabhängige Gibb wurde in die Betty-Ford-Klinik eingeliefert.

Gibb schloss Anfang 1988 einen neuen Vertrag mit der Plattenfirma Island Records. Zu den schon geplanten Aufnahmen für seine neue Platte kam es jedoch nicht mehr, da er Anfang März 1988 ins Krankenhaus eingeliefert wurde, wo er am 10. März 1988 an den Folgen einer Myokarditis starb. Er wurde auf dem Forest-Lawn-Friedhof in Hollywood beigesetzt.

Bevor Gibb Australien verließ, hatte er seine Freundin Kim Reeder geheiratet. Als die gemeinsame Tochter, Peta Jaye, am 25. Januar 1978 geboren wurde, lebte Gibb bereits von seiner Ehefrau getrennt und ließ sich im Laufe des Jahres von ihr scheiden.[2][3] Gibb traf seine Tochter nur einmal im Jahr 1981.[4] Seine Tochter Peta J. Reeder-Gibb züchtet Staffordshire Bull Terrier und ist eine anerkannte Jurorin bei Hundeschauen in New South Wales, Australien.[5]
weitere Kompilationen

weitere Singles
