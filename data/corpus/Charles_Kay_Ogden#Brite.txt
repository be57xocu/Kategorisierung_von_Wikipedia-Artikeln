Charles Kay Ogden (* 1. Juni 1889 in Fleetwood, Lancashire; † 21. März 1957 in London) war ein britischer Sprachwissenschaftler und Schriftsteller.

Ogden studierte Sprachen in Cambridge. Im Jahre 1912 gründete er eine wöchentliche Zeitschrift The Cambridge Magazine mit Literaten als Autoren wie Thomas Hardy, George Bernard Shaw, H. G. Wells etc. In seinem Leben übersetzte er insgesamt 15 Bücher aus der deutschen und französischen Sprache. Die bekannteste Übersetzung war ein philosophisches Werk: Ogden übersetzte, mit Hilfe von George Edward Moore, Frank P. Ramsey und Ludwig Wittgenstein selbst, im Jahre 1922 den Tractatus Logico-Philosophicus von Wittgenstein ins Englische.

Ogden ist auch der Erfinder der konstruierten vereinfachten Sprache (Welthilfssprache) Basic English, mit 850 Wörtern und einer vereinfachten Grammatik, entwickelt von 1926 bis 1930.
Seine Intention war es, ein standardisiertes, uniformes Hilfsmittel für die internationale Kommunikation, als internationale zweite Fremdsprache gedacht, zu erschaffen.
Dafür bekam er Unterstützung von den Premierministern Arthur Neville Chamberlain und Winston Churchill sowie vom amerikanischen Präsidenten Franklin D. Roosevelt.
Es wurde eigens eine Stiftung (Orthological Institute) zur Verbreitung des Basic English gegründet, deren Vorsitzender und Direktor Ogden war.
Das Ziel des Instituts war unter anderem die Ausbildung von Lehrern.
Diese Aktivitäten wurden ab Ende des Zweiten Weltkriegs nicht weitergeführt.

Bekannt ist Ogden auch für seine in Zusammenarbeit mit I. A. Richards 1923 entwickelte Theorie über die Bedeutung der Bedeutung (The Meaning of Meaning), welche das so genannte semiotische Dreieck beschreibt und den berühmten grammatikalisch korrekten, aber inhaltlich sinnlosen Satz “The gostak distims the doshes” (deutsch: Der Gostak distimmt die Doschen.) enthält.
Dieser besagt nur, dass der Gostak ein Distimmer von Doschen ist, dass die Doschen vom Gostak distimmt werden, solange nicht weiter bekannt ist, was die Wörter Gostak, distimmen und Doschen eigentlich bedeuten.
