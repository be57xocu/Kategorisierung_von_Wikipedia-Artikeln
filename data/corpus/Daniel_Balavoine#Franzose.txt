Daniel Balavoine (* 5. Februar 1952 in Alençon; † 14. Januar 1986 in Mali) war ein französischer Chansonnier.

Balavoine wurde als jüngstes von sechs Kindern als Sohn eines Ingenieurs und einer Antiquitätenhändlerin geboren. Die Familie lebte im Süd-Westen Frankreichs und Balavoine besuchte verschiedene Internate. Im Alter von sechzehn Jahren begann er zu singen und beendete das Gymnasium ohne Abschluss. Bis zu seinem 20. Geburtstag sang er in verschiedenen Gruppen und trat in ganz Frankreich auf. Anschließend unternahm er erste Solo-Versuche im Beiprogramm von Patrick Juvet. Der Durchbruch gelang ihm jedoch erst durch die Zusammenarbeit mit Michel Berger, als er in dessen Rock-Oper Starmania (1978) mitspielte, deren Titel „Quand on arrive en ville“ ein Hit wurde. Im selben Jahr hatte Balavoine seinen größten Verkaufserfolg mit „Le Chanteur“, einem Lied, das den Abstieg eines großen Chansonniers (in der Ich-Form gesungen, aber möglicherweise Anspielung auf Polnareff) zum Thema hat. Das Album „Le Chanteur“ wurde ebenso ein großer Erfolg.

1980 kritisierte Balavoine den damaligen Präsidentschaftskandidaten François Mitterrand in einer Diskussionsrunde vor laufenden Kameras heftig. Die Politik nehme die Probleme der Jugend nicht ernst. 
Sein fünftes Solo-Album „Un autre monde“ (1980) enthielt das Lied „Mon fils, ma bataille“ (Mein Sohn, meine Schlacht), in dem Balavoine den Kampf eines Vaters um das Sorgerecht für seinen Sohn schildert. Das Chanson wurde durch den Film „Kramer gegen Kramer“ und auch durch die Scheidung von Balavoines Gitarristen inspiriert. Auch die Lieder „Je ne suis pas un héros“ und „La vie ne m’apprend rien“ wurden fester Bestandteil von Balavoines Repertoire.

Nach der Veröffentlichung von „Vendeur de larmes“ (1982) nahm Balavoine als passionierter Motorsportler an der Rallye Paris-Dakar teil. Als er wegen einer Panne schon nach der ersten Etappe aufgeben musste, nutzte er die Gelegenheit, die Umgebung zu erkunden. Inspiriert von der Armut und Hungersnot des afrikanischen Kontinents, entstand „Loin des yeux de l’Occident“ (1983) in Schottland. Im selben Jahr nahm Daniel Balavoine mit der ABBA-Sängerin Frida ein Duett auf: „Belle“. 

Balavoines politisches Interesse wurde immer stärker, am 23. Oktober 1983 war er erneut als Gast einer Nachrichtensendung geladen und sagte wortwörtlich „Ich scheiße auf die Kriegsveteranen“ („J’emmerde les anciens combattants!“). Anlass zu dieser Brandrede gegen den Krieg war der Anschlag von Drakkar, ein Selbstmordattentat auf eine amerikanische Militärbasis im Libanon, wo auch Balavoines Bruder Yves seinen Dienst versah. Mehrmals machte sich Balavoine öffentlich für humanitäre Angelegenheiten stark.

Im Juli 1984 kam Balavoines erstes Kind Jérémie zur Welt, eine Erfahrung, die Balavoine zur Basis seines Liedes „Dieu que c’est beau“ macht. Eine erfolgreiche Tour schloss Daniel Balavoine 1984 mit einem Konzert im Pariser Palais des Sports ab, der Auftritt erschien als Doppelalbum unter dem Namen „Balavoine au Palais des sports“.

Im Oktober 1985 kam Balavoines letztes Album „Sauver l’amour“ auf den Markt, in welchem er sich provokativ mit politischen Themen beschäftigt. „L’aziza“, ein Lied für seine jüdisch-marokkanische Frau, erhielt den Preis der französischen Anti-Rassismus-Organisation S.O.S. Racisme. „Tous les cris les S.O.S.“, „Sauver l’amour“ und „Aimer est plus fort que d’être aimé“ werden Balavoines letzte Hits. 

Balavoines Freund Coluche startete im Dezember 1985 ein bis heute existierendes Projekt namens „Les restaurants du cœur“ („Les restos du cœur“), eine Organisation, die Obdachlose und Arme gratis speist. Balavoine nahm an der Eröffnung teil. Er selbst reiste anschließend erneut zur Rallye Paris-Dakar, um persönlich Wasserpumpen für die Wüstenbewohner abzuliefern. Sein Hubschrauber stürzte am 14. Januar 1986 in der Nähe des Sees Gossi ab. Bei diesem Absturz starben außerdem der Rallye-Direktor Thierry Sabine, der Schweizer Pilot François-Xavier Bagnoud, die französische Journalistin Nathalie Odent und der französische Fernsehtechniker Jean-Paul Fur.

Balavoine galt – nach dem Verschwinden von Michel Polnareff – Mitte der 1970er bis Mitte der 1980er Jahre als enfant terrible der französischen Chanson- und Variété-Szene. 
Seine Lieder waren stets provokativ und von linker Politik geprägt – der damalige Präsident François Mitterrand war ihm nicht sozialistisch genug, weder in der Innen- noch in der Außenpolitik. So versuchte Balavoine, mit Hilfe seiner großen Popularität und dem im Showgeschäft verdienten Geld selbst „Politik“ zu machen.

Balavoines Musik ist gekennzeichnet durch die helle Klangfarbe seiner Stimme (voix de „cristal“) und der angelsächsisch geprägten, elektronischen Popmusik, die er als einer der ersten nach Frankreich brachte.  

Nach seinem Tod wurde die „Association Daniel Balavoine“ gegründet, die sein humanitäres Engagement fortführen soll. Balavoine hinterließ zwei Kinder: Jérémie (* 1984) und Joana (* 1986), die erst nach seinem Tod zur Welt kam. Er ist auf dem Friedhof von Biarritz beerdigt.

30 Jahre nach seinem Tod erschien als Hommage das Album Balavoine(s), in dem aktuelle Interpreten wie Zaz, Nolwenn Leroy, Florent Pagny und Shy’m 17 seiner Lieder neu interpretierten.

Goldene Schallplatte

2× Goldene Schallplatte

Platin-Schallplatte
