Steve Martin [ˌstiːvˈmɑɹtn̩] (* 14. August 1945 in Waco, Texas; eigentlich Stephen Glenn Martin) ist ein US-amerikanischer Komiker, Schriftsteller, Musiker, Produzent und Schauspieler. 2014 wurde ihm der Ehrenoscar verliehen.

Nach dem Abschluss der Garden Grove High School im Jahr 1963 studierte Martin an der California State University einige Semester Philosophie und Theaterwissenschaften. Nebenbei arbeitete er im Magic Shop von Disneyland, wo er seine Fähigkeiten im Jonglieren, Zaubern, Banjospielen und Ballontiereformen entwickelte. Ende der 1960er Jahre schrieb er sein erstes Bühnenprogramm, mit dem er durch zahlreiche kleinere Clubs in Los Angeles tourte.

Martin machte sich in der Branche schnell einen Namen. Bereits 1969 gewann er als Autor der Smothers Brothers Comedy Hour einen Emmy. 1971 war er festes Ensemblemitglied der Sonny & Cher Show. Mit zahlreichen Auftritten in der Tonight Show mit Johnny Carson wurde er einem breiten Publikum bekannt. Dass er in dem Bruce-Lee-Film Todesgrüße aus Shanghai sein Leinwanddebüt gegeben haben soll, ist eine verbreitete Legende. Tatsächlich sieht ihm ein Polizist in diesem Film nur sehr ähnlich.

1976 moderierte er erstmals die erfolgreiche Comedy-Sendung Saturday Night Live, für die er in den folgenden zehn Jahren u. a. mit Dan Aykroyd, John Belushi, Chevy Chase, Eddie Murphy, Bill Murray und Martin Short vor der Kamera stand.

In dem Film Reichtum ist keine Schande, zu dem er auch das Drehbuch schrieb, spielte Martin 1979 seine erste Hauptrolle. Für den Kurzfilm The Absent-Minded Waiter erhielt er im selben Jahr eine Oscar-Nominierung. In den 1980er Jahren war Martin u. a. in der Film-noir-Parodie Tote tragen keine Karos, den jeweils Golden-Globe-nominierten Komödien Solo für zwei und Roxanne sowie im Oscar-nominierten Horror-Musical Der kleine Horrorladen zu sehen. Mit diesen Filmen wurde er auch in Europa bekannt.

1991 wirkte er in L.A. Story und Vater der Braut, einem Remake des Spencer-Tracy-Klassikers, mit. Das Drama Grand Canyon – Im Herzen der Stadt, in dem er neben Kevin Kline und Danny Glover die Hauptrolle spielte, gewann auf der Berlinale den Goldenen Bären. Viel Beachtung fand seine Darstellung in dem Thriller Die unsichtbare Falle, in dem er in einer seiner wenigen ernsthaften Rollen zu sehen ist. Für Housesitter und Schlaflos in New York stand Martin 1992 und 1999 gemeinsam mit Goldie Hawn vor der Kamera.

2001 und 2003 moderierte er die Oscarverleihung und wurde dafür für mehrere Emmys nominiert. Auch als Autor ist Martin nach wie vor aktiv. Das Theaterstück Picasso at the Lapin Agile wurde 1993 in Chicago uraufgeführt und verfilmt. Seine Bücher, die Kurzgeschichtensammlung Pure Drivel und der Roman Shopgirl (2005 verfilmt), waren Ende der 90er ebenfalls sehr erfolgreich. 2003 spielte er die Rolle als Vater von zwölf Kindern in Im Dutzend billiger. 2005 übernahm Steve Martin in einer Neuverfilmung von Der rosarote Panther die Rolle des Inspektor Clouseau und spielte erneut den Vater in Im Dutzend billiger 2.

2009 war er mit Der rosarote Panther 2 erneut in dieser Rolle zu sehen, zudem an der Seite von Meryl Streep und Alec Baldwin in der Liebeskomödie It’s Complicated von Nancy Meyers. Im selben Jahr erhielt Martin für seinen Gastauftritt als Gavin Volure in der gleichnamigen Episode der Serie 30 Rock seine fünfte Emmy-Nominierung.

Von 1986 bis 1994 war er mit der Schauspielerin Victoria Tennant verheiratet. Am 28. Juli 2007 heiratete er die Journalistin Anne Stringfield, mit der er seit Dezember 2012 ein Kind hat.

Martin wurde 2005 mit dem Mark-Twain-Preis für amerikanischen Humor und 2007 mit dem Kennedy-Preis ausgezeichnet.

2010 erhielt er den Grammy in der Kategorie Bestes Bluegrass-Album für The Crow / New Songs for the Five-String Banjo. Im selben Jahr wurde er in die American Academy of Arts and Sciences gewählt.

Am 7. März 2010 führte Martin zusammen mit Alec Baldwin im Kodak Theatre noch einmal durch die Oscarverleihung. 2014 erhielt er den Ehrenoscar für sein Lebenswerk.

Das American Film Institute (AFI) ehrte ihn am 4. Juni 2015 in Los Angeles mit dem AFI Life Achievement Award für sein Lebenswerk.[1]
Weitere Veröffentlichungen:

Die deutsche Synchronstimme von Steve Martin sprechen unter anderem Norbert Gescher und Eckart Dux.
