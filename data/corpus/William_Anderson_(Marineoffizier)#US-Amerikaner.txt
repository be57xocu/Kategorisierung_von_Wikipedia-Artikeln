William Robert Anderson (* 17. Juni 1921 in Bakerville, Humphreys County (Tennessee); † 25. Februar 2007 in Leesburg (Virginia)) war ein Offizier der United States Navy. 

Er durchlief die militärische Ausbildung und stieg zum Fregattenkapitän auf. Als Spezialist für Atom-U-Boote war er von 1957 bis 1959 Kommandant des ersten Atom-U-Boots, der USS Nautilus (SSN-571). Mit ihr gelang ihm am 3. August 1958 die erste Unterquerung des Nordpols.

Nach seinem Abschied von der Marine engagierte er sich in der Politik. Als unabhängiger Kandidat belegte er bei den Gouverneurswahlen in Tennessee 1962 mit 32,8 Prozent der Stimmen den zweiten Platz hinter dem siegreichen Demokraten Frank G. Clement. Er trat 1964 selbst den Demokraten bei und saß vom 3. Januar 1965 bis zum 3. Januar 1973 für den 6. Kongresswahlbezirk von Tennessee im Repräsentantenhaus der Vereinigten Staaten. Nach der Wahlniederlage gegen den republikanischen Herausforderer Robin Beard zog er sich aus der Öffentlichkeit zurück. Er lebte zuletzt in Alexandria (Virginia).

Delegierter aus dem Südwest-Territorium (1794–1796)White

Abgeordnete aus dem Bundesstaat Tennessee (seit 1796)1. Bezirk: Jackson |
W. Claiborne |
Dickson |
Wharton |
Miller |
Grundy |
Cannon |
T. Claiborne |
R. Allen |
Blair |
Carter |
Arnold |
A. Johnson |
B. Campbell |
N. Taylor |
Watkins |
Nelson |
Bridges |
N. Taylor |
R. Butler |
McFarland |
Randolph |
R. Taylor |
Pettibone |
R. Butler |
A. Taylor |
W.C. Anderson |
Brownlow |
Massey |
Sells |
B. Reece |
Lovette |
B. Reece |
Phillips |
B. Reece |
L. Reece |
Quillen |
Jenkins |
D. Davis |
Roe • 2. Bezirk: G. Campbell |
Weakley |
Sevier |
Henderson |
Hogg |
Bryan |
Alexander |
J. Cocke |
P. Lea |
Arnold |
Bunch |
McClellan |
Senter |
W. Cocke |
Watkins |
Churchwell |
Sneed |
Maynard |
Thornburgh |
L. Houk |
J. Houk |
Gibson |
Hale |
Austin |
J.W. Taylor |
Jennings |
H. Baker Sr. |
I. Baker |
Duncan Sr. |
Duncan Jr.

3. Bezirk: Rhea |
Powell |
F. Jones |
Blair |
J.C. Mitchell |
Standifer |
L. Lea |
J. Williams |
Blackwell |
Crozier |
J. Anderson |
Churchwell |
S. Smith |
Brabson |
Clements |
Stokes |
A. Garrett |
Crutchfield |
Dibrell |
Neal |
Evans |
H. Snodgrass |
F. Brown |
Moon |
J. Brown |
McReynolds |
Kefauver |
Frazier |
Brock |
L. Baker |
Lloyd |
Wamp |
Fleischmann • 4. Bezirk: Bowen |
Reynolds |
Marr |
Cannon |
S. Houston |
Isacks |
Standifer |
Stone |
Blackwell |
T. Campbell |
A. Cullom |
Hill |
Savage |
W. Cullom |
Savage |
Stokes |
E. Cooper |
Mullins |
Tillman |
Bright |
Fite |
Riddle |
McMillin |
C. Snodgrass |
Fitzpatrick |
M. Butler |
Hull |
Clouse |
Hull |
J.R. Mitchell |
Gore Sr. |
Evins |
Gore Jr. |
Jim Cooper |
Hilleary |
L. Davis |
DesJarlais

5. Bezirk: T. Harris |
I. Thomas |
Rhea |
Isacks |
R. Allen |
Desha |
Hall |
Forester |
Turney |
G. Jones |
Ready |
Hatton |
W. Campbell |
Trimble |
Prosser |
Golladay |
Harrison |
Bright |
Warner |
Richardson |
W. Houston |
E. Davis |
Byrns |
Atkinson |
Byrns Jr. |
Priest |
McCord |
Earthman |
Evins |
Priest |
Loser |
Fulton |
C. Allen |
Boner |
Clement |
Jim Cooper • 6. Bezirk: Humphreys |
Sevier |
Blount |
J. Cocke |
J. Polk |
B. Peyton |
W. Campbell |
A. Brown |
Martin |
J. Thomas |
W. Polk |
G. Jones |
J. Thomas |
Arnell |
Whitthorne |
House |
A. Caldwell |
Washington |
Gaines |
Byrns |
Turner |
Courtney |
Priest |
Sutton |
Bass |
W. Anderson |
Beard |
Gore Jr. |
B. Gordon |
Black

7. Bezirk: Reynolds |
S. Houston |
Bell |
Caruthers |
Dickinson |
Gentry |
Bugg |
Wright |
Hawkins |
R. Caldwell |
Atkins |
Whitthorne |
Ballentine |
Whitthorne |
Cox |
Padgett |
Turner |
Salmon |
E. Eslick |
W. Eslick |
Browning |
Pearson |
Courtney |
Sutton |
Murray |
Blanton |
E. Jones |
Sundquist |
Bryant |
Blackburn • 8. Bezirk: Sandford |
Marable |
C. Johnson |
Dickinson |
Maury |
Gentry |
J. Peyton |
E. Ewing |
Barrow |
A. Ewing |
W. Cullom |
Zollicoffer |
Quarles |
Leftwich |
Nunn |
W. Smith |
Vaughan |
Nunn |
Atkins |
J.M. Taylor |
Enloe |
McCall |
Sims |
Scott |
Browning |
Jere Cooper |
Murray |
Jere Cooper |
Everett |
E. Jones |
Kuykendall |
Ford Sr. |
E. Jones |
Tanner |
Fincher |
Kustoff 

9. Bezirk: Standifer |
Alexander |
D. Crockett |
Fitzgerald |
J. Polk |
Watterson |
C. Johnson |
Chase |
I. Harris |
Etheridge |
Atkins |
Etheridge |
Lewis |
W. Caldwell |
Simonton |
Pierce |
Glass |
Pierce |
McDearmon |
Pierce |
F. Garrett |
Jere Cooper |
Crump |
Chandler |
C. Davis |
Jere Cooper |
C. Davis |
Grider |
Kuykendall |
Ford Sr. |
Ford Jr. |
Cohen • 10. Bezirk: Inge |
Shields |
A. Brown |
Ashe |
Stanton |
Rivers |
Avery |
Maynard |
Young |
Moore |
Young |
Z. Taylor |
Phelan |
J. Patterson |
Carmack |
M. Patterson |
G. Gordon |
McKellar |
Fisher |
Crump |
C. Davis

11. Bezirk: C. Johnson |
Cheatham |
C. Johnson |
M. Brown |
Haskell |
C. Williams • 12. Bezirk: D. Crockett |
Huntsman |
J. Crockett |
M. Brown • 13. Bezirk: Dunlap |
C. Williams
