Natalie Curtis (* 26. April 1875 in New York; † 23. Oktober 1921 in Paris) war eine amerikanische Ethnographin und Musikethnologin, die durch ihre Forschungen bei den Indianern berühmt wurde.

Curtis genoss als Arzttochter eine gute Erziehung und wurde zunächst zur Pianistin ausgebildet. Mit 25 Jahren begleitete sie ihren Bruder auf einer Reise in den Südwesten der Staaten, die ihr Schicksal bestimmte, als sie Indianern begegnete.

Bis ans Ende ihres Lebens sammelte Curtis von nun an Gedichte, Lieder, Märchen, Mythen und Erzählungen verschiedener Indianerstämme als wertvolle Zeugnisse der einheimischen amerikanischen Kultur. Ihr Werk ist dem der Brüder Grimm vergleichbar, die in Deutschland die Märchen vor dem Vergessen bewahrten. Curtis setzte sich gegenüber Theodore Roosevelt engagiert für die Rechte der Indianer ein.

Curtis fand internationale Anerkennung, so etwa hielt sie im Oktober 1921 eine Vorlesung an der Sorbonne in Paris. Kurz darauf wurde sie von einem Taxi überfahren und starb.
