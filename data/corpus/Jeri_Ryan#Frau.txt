Jeri Lynn Ryan, geborene Zimmermann (* 22. Februar 1968 in München) ist eine US-amerikanische Schauspielerin, die vor allem durch ihre Rolle als Seven of Nine in der Fernsehserie Star Trek: Raumschiff Voyager bekannt wurde.

Jeri Ryan wurde in München als Jeri Lynn Zimmermann, Tochter von U.S. Army Master Sergeant Gerhard „Jery“ Florian Zimmermann und Sharon Zimmermann, geboren. Sie hat einen älteren Bruder namens Mark. 

Als Ryan elf Jahre alt war, entschied die Familie, sich in Paducah (Kentucky) niederzulassen. Dort besuchte sie später die Lone Oak High School und studierte schließlich an der Northwestern University in Chicago, wo sie der Studentenverbindung Alpha Phi angehörte. In Chicago nahm sie an Schönheitswettbewerben teil und wurde „Miss Northwestern Alpha Delta Phi“. 1989 wurde sie Miss Illinois und später Vierte bei den Wahlen zur Miss America.

Mit einem Bachelor-Abschluss in Theaterwissenschaften ging sie nach Los Angeles, wo sie in verschiedenen Fernsehserien in Gastrollen auftrat. Im Jahr 1991 heiratete sie den Politiker Jack Ryan, mit dem sie einen Sohn hat. Die Ehe wurde 1999 geschieden. Nach Bekanntwerden von Details aus dem Scheidungsverfahren zog Jack Ryan seine Kandidatur um das Amt eines US-Senators gegen Barack Obama zurück. Nach der Ehe war Jeri Ryan mit Brannon Braga zusammen, der neben Rick Berman einer der Produzenten von Star Trek: Raumschiff Voyager war.[1]
Nach dem Ende von Raumschiff Voyager im Jahr 2001 übernahm Ryan verschiedene Rollen in US-Serien. So war sie von 2001 bis 2004 als Ronnie Cooke in der Serie Boston Public zu sehen. Von 2006 bis 2008 spielte sie eine der Hauptrollen der Serie Shark. Von 2011 bis 2013 war sie als Dr. Kate Murphy in der Serie Body of Proof zu sehen. Ebenso hatte sie wiederkehrende Rollen in den Serien O.C., California, Two and a Half Men und Leverage. 

Ryan wird hauptsächlich von Anke Reitzenstein synchronisiert.[2]
Von Februar 2005 bis Ende 2010 besaß sie mit Christophe Emé das Restaurant „Ortolan“ in Los Angeles.[3] Seit 2007 ist Ryan mit Emé verheiratet und wurde am 2. März 2008 Mutter einer Tochter.
