Peter Bieri (* 21. Juni 1952 in Winterthur; heimatberechtigt in Hünenberg ZG und Romoos) ist ein Schweizer Politiker (CVP). Von 1995 bis 2015 war er Ständerat, im Jahr 2007 Präsident.

Bieri studierte Agronomie an der ETH Zürich, wo er 1978 als Ing. Agr. diplomierte und 1982 am Institut für Nutztierwissenschaften als Dr. sc. techn. promovierte. Er war von 1982 bis 2009 Lehrer am kantonalen Landwirtschaftlichen Bildungs- und Beratungszentrum Schluechthof in Cham. Er präsidiert den Informationsdienst für den öffentlichen Verkehr LITRA,[1] seit 2016 präsidiert er die Kommission der Schweizerischen Nationalbibliothek.[2]
Von 1987 bis 1994 war er Gemeinderat, von 1987 bis 1991 war er Präsident der CVP Hünenberg, von 1995 bis 2015 vertrat er den Kanton Zug im Ständerat. Am 4. Dezember 2006 wurde er zum Ständeratspräsidenten gewählt. In seiner Tätigkeit als Ständerat präsidierte er die Geschäftsprüfungskommission 1998/99, die Kommission für Wissenschaft, Bildung und Kultur 2002/03, die Kommission für Verkehr und Fernmeldewesen 2008/09, die EFTA-EU-Delegation 1998/99 und die Delegation bei der interparlamentarischen Union 2004/05.

Peter Bieri ist verheiratet, Vater von vier erwachsenen Kindern und wohnt in Hünenberg. In der Schweizer Armee war er bis 2003 Major. 

Nationalräte:
Thomas Aeschi |
Gerhard Pfister |
Bruno Pezzatti

Ständeräte:
Peter Bieri |
Rolf Schweiger

Liste der Mitglieder des Schweizer Nationalrats in der 49. Legislaturperiode |
Liste der Mitglieder des Schweizer Ständerats in der 49. Legislaturperiode
