Haiducii (* 14. Juni 1977 in Bukarest; eigentlich Paula Monica Mitrache) ist eine rumänische Sängerin. Ihr Künstlername leitet sich von den Heiducken ab.

Nach der Wahl zur Miss Bukarest startete Mitrache in Rumänien eine Karriere als Sängerin, Schauspielerin und Fernsehmoderatorin. 2003 siedelte sie nach Rom über. In Italien veröffentlichte sie ihre erste Single Dragostea din tei, eine Coverversion eines Songs der moldawischen Band O-Zone, jedoch ohne deren Erlaubnis. Infolgedessen musste Haiducii eine Strafe von umgerechnet 10.000 Euro zahlen. Bereits eine Woche nach Veröffentlichung erreichte die Single Platz 1 der italienischen Verkaufscharts. Insgesamt belegte Haiducii 20 Wochen die Spitze der italienischen Dance-Charts. Einen maßgeblichen Beitrag an ihrem Erfolg hatte Gabry Ponte, ein bekannter italienischer DJ und Musikproduzent, der ihre Songs mitproduzierte und entsprechende Remixe beisteuerte.

Im Mai 2004 erreichte sie mit der Single auch in Österreich Platz 1 der Charts, in der Schweiz Platz 2. In Deutschland belegte sie im Sommer 2004 Platz 2 der Singlecharts und konnte sich 15 Wochen in den Top-10 halten, gleich hinter O-Zone.

Ende 2004 brachte sie ihre zweite Single Mne S Toboy Horosho heraus, allerdings nur mit mäßigem Erfolg. 
Die dritte Single More’n’More wurde Anfang 2006 veröffentlicht.
