Carl Martin Rudolf Möhner, auch Carl Mohner (* 11. August 1921 in Wien, Österreich; † 14. Januar 2005 in McAllen, Texas) war ein österreichischer Schauspieler und Maler.

Möhner besuchte ab 1937 die Theaterschule seiner Heimatstadt und arbeitete an verschiedenen österreichischen und deutschen Theaterbühnen, bevor der Zweite Weltkrieg seine Karriere unterbrach. Danach gelang ihm in den 1950er und 1960er Jahren eine Filmkarriere mit zahlreichen Auftritten in internationalen Produktionen. Zu seinen bekanntesten Filmen gehören der französische Gangsterfilmklassiker Rififi (1955), sowie Die letzte Fahrt der Bismarck (1960), Die Geierwally (1956), Weißer Holunder (1957) oder Wo die alten Wälder rauschen (1956).

Mit Beginn der 1960er Jahre begann Möhner, sich der Malerei zu widmen. Schon nach kurzer Zeit konnte er in zahlreichen europäischen Städten ausstellen; 1963 gewann er die Goldmedaille des „IX Premio Internazionale de Pittura San Vitto Romano“. Auch in den 1990er Jahren wurde er mehrfach ausgezeichnet.[1] Sein Stil wird als „einfach und von kindlicher Unschuld geprägt“ bezeichnet.[2]
1978 zog sich Möhner weitgehend vom Filmgeschäft zurück und zog nach Texas. Er starb an der Parkinsonschen Krankheit.

Seine Söhne Gunther Möhner[3] und Gernot Möhner[4] waren ebenfalls als Schauspieler tätig, ersterer nur im Zeitraum 1970 bis 1973 in deutschen Sexfilmen.
