Ekathotsarot (Thai: สมเด็จพระเอกาทศรถ - Somdet Phra Ekathotsarot, anderer Name: Sanphet III., สมเด็จพระเจ้าสรรเพชญ์ที่3 - Somdet Phrachao Sanphet Thi Sam; * nach 1556 in Phitsanulok/Thailand; † 1610 in Ayutthaya) war von 1605 bis 1610 König des Königreiches Ayutthaya im heutigen Thailand[1].

Laut Jeremias Van Vliet, Direktor des VOC-Kontors in Ayutthaya und Chronist, war sein Name Phra Anuchathirat Phra Ramesuan (Thai: พระอนุชาธิราช พระราเมศวร). Der Sohn von Maha Thammaracha war der 21. König von Ayutthaya, er bestieg den Thron im Alter von 45 Jahren und regierte sechs Jahre lang.

Als Nachfolger des Königs Naresuan der Große, seines Bruders, erbte er ein Reich, das die größte je erreichte Ausdehnung Siams repräsentierte. Hatte er zu Zeiten der Regentschaft von Naresuan eng an dessen Seite die jahrzehntelangen Schlachten gegen Birmanen und Kambodschaner gefochten, konzentrierte er sich in seiner Zeit auf die Festigung der politischen Organisation Siams und auf die Außenbeziehungen.

Ekathotsarot sandte als erster König Thailands 1608 eine Gesandtschaft nach Europa an Maurits (Moritz von Oranien) in den Haag. Dies war der Beginn einer mehr als 160 Jahre währenden Beziehung mit der Holländischen Ostindienkompanie. Schon früh gelangten so Teleskope und Ferngläser nach Siam, eine damals neue Erfindung in Europa.

Nachfolger von Ekathotsarot wurde sein Sohn Songtham.

Ramathibodi I. |
Ramesuan |
Borommaracha I. |
Thong Lan |
Ramracha |
Intharacha |
Borommaracha II. |
Borommatrailokanat | 
Borommaracha III. |
Ramathibodi II. |
Borommaracha IV. |
Ratsada |
Chairacha |
Yot Fa |
Khun Worawongsa |
Chakkraphat |
Mahin |
Maha Thammaracha |
Naresuan d.Gr. |
Ekathotsarot |
Si Saowaphak |
Songtham |
Chetthathirat |
Athittayawong |
Prasat Thong |
Chai  |
Si Suthammaracha |
Narai d.Gr. |
Phetracha |
Phrachao Suea |
Thai Sa |
Borommakot |
Uthumphon |
Ekathat

Taksin d.Gr.

Rama I. d.Gr. |
Rama II. |
Rama III. |
Mongkut |
Chulalongkorn d.Gr. |
Vajiravudh |
Prajadhipok |
Ananda Mahidol |
Bhumibol Adulyadej d.Gr. |
Maha Vajiralongkorn
