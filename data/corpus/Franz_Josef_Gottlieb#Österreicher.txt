Franz Josef Gottlieb, auch F. J. Gottlieb (* 1. November 1930 in Semmering, Niederösterreich; † 23. Juli 2006 in Verden) war ein österreichischer Regisseur.

Gottlieb besuchte die Akademie für Musik und darstellende Kunst in Wien, war Schauspieler am Akademietheater und schloss 1953 mit seinem Regie-Diplom ab. Danach betätigte er sich bei mehreren Filmproduktionen als Regieassistent wie zuletzt bei Mit Himbeergeist geht alles besser und inszenierte 1960 mit Meine Nichte tut das nicht seinen ersten Spielfilm.

Seitdem gehörte Gottlieb zu den meistbeschäftigten, aber auch meistkritisierten Regisseuren des deutschsprachigen Nachkriegskinos, dem immer wieder routinemäßiges, einfallsloses Herunterkurbeln vorgeworfen wurde. Seine Auftraggeber schätzten an ihm das genaue Einhalten des Drehbuchs und des Zeitplans, was andererseits als mangelnde Kreativität ausgelegt wurde. Aufsehen erregte er lediglich, als er bei den Dreharbeiten zu dem Karl-May-Film Durchs wilde Kurdistan von Produzent Artur Brauner entlassen wurde. Der anschließende Rechtsstreit endete 1968 mit einem Vergleich.

Zu seinen bekanntesten Regiearbeiten gehörten Filme wie Tante Trude aus Buxtehude oder Zärtliche Chaoten. Auch für Fernsehserien wie Mandara, Manni, der Libero, Der Landarzt, Ein Schloß am Wörthersee oder Unser Charly und bei zahlreichen Synchronisationen führte er Regie.

Im Jahr 2002 wurde Gottlieb mit dem Scharlih ausgezeichnet, dem ältesten Preis, der mit dem Namen Karl May verbunden ist.

Franz Josef Gottlieb war 12 Jahre mit der Schauspielerin Doris Kirchner verheiratet. Seine zweite Ehe schloss er 1974 mit der aus Norwegen stammenden Schauspielerin Elisabeth Krogh. Ihre gemeinsame Tochter ist die Schauspielerin Viktoria Gottlieb. Franz Josef Gottlieb starb im Alter von 75 Jahren an einem Gehirntumor.

„Ein Erfolgsrezept gibt es in dieser Branche nicht. Mir geht es vorwiegend darum, das Publikum gut zu unterhalten. Film ist in erster Linie eine Unterhaltungsindustrie.“
