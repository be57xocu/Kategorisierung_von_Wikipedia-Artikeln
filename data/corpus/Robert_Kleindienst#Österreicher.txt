Robert Kleindienst (* 4. März 1975 in Salzburg) ist ein österreichischer Schriftsteller.

Robert Kleindienst wuchs in Radstadt im Pongau auf, studierte Germanistik, Politikwissenschaft und Pädagogik an der Universität Salzburg und verfasste seine Diplomarbeit über Paul Celan im Kontext von Roland Barthes' Autorkonzept. Kleindienst ist Gründungsmitglied des Literaturportals die flut, Mitglied der Grazer Autorenversammlung, der IG Autorinnen Autoren sowie der Salzburger Autorengruppe.
