Otto Flake (* 29. Oktober 1880 in Metz; † 10. November 1963 in Baden-Baden) war ein deutscher Schriftsteller.

Flake wurde am 29. Oktober 1880 in Metz geboren. Er besuchte das Gymnasium in Colmar und studierte anschließend Germanistik, Philosophie und Kunstgeschichte in Straßburg. Dort gehörte er zur Künstlergruppe Das jüngste Elsaß (auch Der Stürmerkreis).

Seine ersten beruflichen Stationen waren Paris und Berlin, wo er als regelmäßiger Mitarbeiter der Neuen Rundschau tätig war und später zu den auflagenstärksten Autoren der Weimarer Republik gehörte. In dieser Zeit unternahm er zahlreiche Reisen, über die er in seiner Essaysammlung Das Logbuch berichtet hat (publiziert 1917 bei S. Fischer). Unter anderem traf er bei einem Besuch in Konstantinopel auf Friedrich Schrader und Max Rudolf Kaufmann, beide damals bekannt für ihre Übersetzungen moderner türkischer Literatur und zahlreiche Beiträge über osmanische Kultur im Feuilleton der Frankfurter Zeitung[1].

Während des Ersten Weltkrieges arbeitete Otto Flake in der Zivilverwaltung in Brüssel. Dort hatte er im Haus von Carl und Thea Sternheim Kontakt mit den ebenfalls dort stationierten Schriftstellern Gottfried Benn, Friedrich Eisenlohr und Carl Einstein, dem Kunsthistoriker Wilhelm Hausenstein, dem Verleger Hans von Wedderkop sowie dem Kunsthändler Alfred Flechtheim[2]. Anfang 1918 war er kurze Zeit für die neu gegründete Deutsche Allgemeine Zeitung in Berlin als Chef des Feuilletons tätig. Stellvertretender Chefredakteur der DAZ war bis 1920 Max Rudolf Kaufmann, den Flake aus Konstantinopel kannte. Gegen Kriegsende ließ Flake sich in Zürich nieder und schloss sich dem Kreis der Dadaisten an.

1920 legte er die erste deutsche Übersetzung des berühmtesten Romans von Honoré de Balzac, Verlorene Illusionen, vor.[3] Im selben Jahr erkannte er zeitgleich mit Eduard Korrodi Hermann Hesses Urheberschaft des Demian.[4] Seit 1928 lebte er nach seiner Ausweisung aus Südtirol (wo er am Ritten lebte) mit seiner Familie in Baden-Baden.

Tucholsky schrieb über seinen Mitarbeiter an Die Weltbühne:

„Flake, unser bedeutendster Essayist neben Heinrich Mann, ein deutscher Wegbereiter, eine geistige Wohltat […].“[5]
Stefan Zweig stellte fest:

„Ganz fremd ist Flake, ich weiß es, ganz isoliert mit dieser seiner Art in unserer neueren Literatur, aber notwendig, sehr notwendig, denn er beweist den Deutschen, denen Dichtung fast immer eins ist mit Dämmerung, am besten, dass Kunst auch Klugheit sein kann und zwar Klugheit mit Kraft.“

1933 unterschrieb Flake wie 87 weitere deutsche Schriftsteller eine Ergebenheitsadresse an Adolf Hitler, das Gelöbnis treuester Gefolgschaft, worum ihn sein Verleger Samuel Fischer ersucht hatte, um dessen Verlag zu unterstützen (Fischer galt nach den Kategorien der Nazis als Jude). Zudem war Flakes fünfte Ehefrau in der Terminologie der Nazis eine „Halbjüdin“, und er glaubte, auch sie dadurch zu schützen. Für diese Unterschrift wurde er unter anderem von Thomas Mann, Bertolt Brecht und Alfred Döblin scharf kritisiert.

Nach Kriegsende 1945 wurde Flake von der französischen Besatzungsmacht in den Kulturrat von Baden-Baden berufen, der mit der Durchführung von Ausstellungen und Vorträgen betraut war. Als gebürtiger Lothringer setzte er sich für die Aussöhnung von Deutschen und Franzosen ein. Als Autor wurde er nach 1945 zunächst kaum mehr wahrgenommen und schrieb unter Pseudonym. 1954 erhielt Otto Flake den Johann-Peter-Hebel-Preis des Landes Baden-Württemberg. 1958 legte Bertelsmann mehrere Titel des verarmten und depressiven Autors neu auf und verkaufte in 28 Monaten rund 1 Million Exemplare davon, was für Überraschung sorgte.

Am 10. November 1963 starb Otto Flake in Baden-Baden. Sein Nachlass befindet sich in der Stadtbibliothek Baden-Baden. Otto Flake war fünfmal verheiratet, davon zweimal mit der Mutter seiner Tochter. Die Tochter Eva Maria Seveno starb am 21. Februar 2010 im Alter von 89 Jahren in der Nähe von Lübeck.[6]
Friedrich Sieburg beschreibt Flake als Moralisten mit dem Drang, die menschliche Natur zu bilden, [...] als Prophet, dem man nicht glaubte, Lehrer, dem man nicht folgte.[7] In der DDR wurde Flakes Das Ende der Revolution (1920) auf die Liste der auszusondernden Literatur gesetzt.[8]