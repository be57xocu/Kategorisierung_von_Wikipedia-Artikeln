Ludwig Laher (* 11. Dezember 1955 in Linz) ist ein österreichischer Schriftsteller.

Ludwig Laher studierte Germanistik, Anglistik und Klassische Philologie in Salzburg und schloss mit einem Dr. phil. ab. Danach arbeitete er zunächst als Gymnasiallehrer am Christian-Doppler-Gymnasium in Salzburg.

1993 zog er nach St. Pantaleon in Oberösterreich und ist seit 1998 als freier Schriftsteller tätig. Laher veröffentlichte Prosa, Lyrik, Essays, Übersetzungen, wissenschaftliche Arbeiten, Hörspiele, Drehbücher und erhielt zahlreiche Literaturpreise und Stipendien.
