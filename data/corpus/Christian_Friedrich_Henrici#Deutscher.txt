Christian Friedrich Henrici (Pseudonym Picander; * 14. Januar 1700 in Stolpen bei Dresden; † 10. Mai 1764 in Leipzig) war ein produktiver Gelegenheitsdichter des späten Barock und der wichtigste Textdichter Johann Sebastian Bachs.

Christian Friedrich Henrici studierte ab 1719 an der Universität Wittenberg Jura und setzte sein Studium 1720 an der Universität Leipzig fort. Da er anschließend nur geringe Einnahmen als Hauslehrer hatte, begann er seine Karriere als Dichter 1721 in Leipzig und verfasste anfangs derb erotische Gedichte und Dramen. Die ersten Kontakte zu Bach waren vermutlich eher zufälliger Natur.

1725 schrieb Picander, so sein Pseudonym, die Texte zu Bachs weltlichen Kantaten
Entfliehet, verschwindet, entweichet, ihr Sorgen (BWV 249a), Vorbild für das Oster-Oratorium (BWV 249), und Zerreißet, zersprenget, zertrümmert die Gruft (BWV 205).
Bereits 1723 hatte er mit seinem Strophengedicht „Weg ihr irdischen Geschäfte“ die Textvorlage zu Bachs geistlicher Kantate Bringet dem Herrn Ehre seines Namens (BWV 148) geliefert. Die Bachkantate Es erhub sich ein Streit (BWV 19) von 1726 basierte auf einem ähnlichen Werk Henricis.

Beide Gedichte erschienen in der 1724–1725 veröffentlichten Gedichtsammlung „Sammlung erbaulicher Gedancken“, die dem Grafen Franz Anton von Sporck gewidmet war. Dieser war auch mit Bach bekannt und könnte den Kontakt zwischen Komponist und Dichter angeregt haben.

Dieser Kontakt wuchs bald zur Freundschaft, im Zuge derer Bach und Henrici auch ihre künstlerische Zusammenarbeit vertieften. So enthalten alle fünf Bände von Picanders Ernst- schertzhafften und satyrischen Gedichten (Leipzig, 1727–51) Texte, die von Bach vertont wurden. Dazu gehören die Matthäus-Passion (BWV 244) und die Markus-Passion (BWV 247), die Trauerode Klagt, Kinder, klagt es aller Welt (BWV 244a), die Kantate Sehet! Wir gehn hinauf gen Jerusalem (BWV 159), aber auch die populäre Kaffeekantate (BWV 211) und ein Dramma per musica zum Namenstag Augusts des Starken am 3. August 1727, Ihr Häuser des Himmels, ihr scheinenden Lichter (BWV 193a). Wahrscheinlich verfasste er auch das Himmelfahrtsoratorium Lobet Gott in seinen Reichen (BWV 11) und die Kantate Singet dem Herrn ein neues Lied (BWV 190). 1742 schrieb er die Bauernkantate Mer hahn en neue Oberkeet (BWV 212).

Henricis dichterische Begabung wurde auch zum Ausgangspunkt einer Beamtenlaufbahn. So brachte ihm ein Bittgedicht an August den Starken 1727 die Stelle eines Aktuars beim Oberpostamt in Leipzig ein. Kurz darauf wurde er Postsekretär, 1734 Oberpostkommissar und 1740 wurde er dazu Kreissteuer- und Stadttranksteuereinnehmer der Weininspektion. 

Seine Frau Johanna Elisabeth war Taufpatin von Johanna Carolina Bach (1742–1809), der zweitjüngsten Tochter Johann Sebastian Bachs.

