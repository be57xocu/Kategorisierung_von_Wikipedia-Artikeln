Friedrich Ris (* 1867 in Glarus; † 30. Januar 1931 in Rheinau) war Direktor der Irrenanstalt Rheinau und ein bedeutender Schweizer Libellenforscher. 

Sein gleichnamiger Vater Friedrich Ris war ein Kaufmann aus Fluntern. Seine Mutter war Maria Schmid. Er besuchte das Gymnasium Zürich. Danach studierte er ab dem Wintersemester 1885 an der Universität Zürich und schloss das Studium am 3. Mai 1890 ab. Seine Promotion zum Dr. med. folgte bereits am 24. Juni 1890. Das Thema der Dissertation war: Klinischer Beitrag zur Nierenchirurgie... (VZU 725). Er arbeitete dann zunächst als Psychiater und Entomologe in Mendrisio und war zwischen 1898 und 1931 Direktor der Irrenanstalt Rheinau. Seit 1930 war er Ehrenmitglied der Naturforschenden Gesellschaft Schaffhausen. Sein Autorkürzel ist Ris.

Ris war Erstbeschreiber bzw. Einrichter 
