Johann Friedrich Wilhelm Adolf (seit 1885 Ritter von) Baeyer [ˈbaiɐ] (* 31. Oktober 1835 in Berlin; † 20. August 1917 in Starnberg) war ein deutscher Chemiker.
Von Baeyer entwickelte die erste Indigosynthese; er synthetisierte das Phenolphthalein, das Fluorescein und war Wegbereiter für die Alizarin-Synthese. 1905 erhielt er den Nobelpreis für Chemie für seine Verdienste um „die Entwicklung der organischen Chemie und der chemischen Industrie durch seine Arbeiten über die organischen Farbstoffe und die hydroaromatischen Verbindungen“.

Baeyer ist der Sohn des Offiziers und Geodäten Johann Jacob Baeyer und der Eugenie Hitzig, Tochter des Verlegers und Schriftstellers Julius Eduard Hitzig.

Nach Besuch des Friedrich-Wilhelms-Gymnasiums in Berlin studierte er zunächst an der Friedrich-Wilhelms-Universität Berlin Mathematik und Physik, dann Chemie bei Robert Bunsen an der Ruprecht-Karls-Universität Heidelberg.[1] Er promovierte 1858 bei Friedrich Kekulé mit einer Dissertation De arsenici cum methylo conjunctionibus (Über Methylarsin-Verbindungen).[2] 1859 wurde er Privatdozent an der Friedrich-Wilhelms-Universität in Berlin, folgte für seine weiteren Forschungen aber Kekulé nach Gent.[3]
1860 habilitierte sich Baeyer in Berlin und nahm eine Lehrtätigkeit für Organische Chemie am Gewerbeinstitut in Berlin an.[4]  1866 wurde er außerordentlicher Professor an der Friedrich-Wilhelms-Universität Berlin.

1867 gehörte er zu den Gründern der Deutschen Chemischen Gesellschaft zu Berlin, die als Mitteilungsblatt die Fachzeitschrift „Berichte der Deutschen Chemischen Gesellschaft“ zu Berlin.[5] veröffentlichten. Mit sehr geringem Vorsprung gewann A. W. Hofmann die Wahl zum ersten Vorstand 1868 vor Baeyer. In den Jahren 1871, 1881, 1893 und 1903 wurde er dagegen mit Mehrheit zu deren Vorstand gewählt.

Ab 1872 war er Professor für Chemie an der Universität Straßburg, ab 1875 in München als Nachfolger von Justus von Liebig, wo nach seinen Vorgaben ein neues Laboratorium gebaut wurde. Von 1887 bis 1917 war er, wie zuvor auch Justus von Liebig (dieser zwischen 1852 und 1873) Mitglied der Zwanglosen Gesellschaft München.[6]
Zwischen 1870 und 1900 kam es in der Gesellschaft Deutscher Chemiker zu Diskussionen über Art und Inhalte des Chemiestudiums. Die chemische Industrie wünschte vereinheitlichte Lern- und Prüfungsordnungen in Deutschland. Die Inhalte des Chemiestudiums sollten stärker die Bedürfnisse der chemischen Industrie berücksichtigen. Dagegen trat Adolf von Baeyer neben Wilhelm Ostwald und A. W. Hofmann für eine hochschulinterne Prüfung und eine zweckfreie Forschung ein.
Von Baeyer glaubte, dass Wissenschaft nur in Unabhängigkeit von äußeren, wirtschaftlichen Einflüssen gedeihen könnte.[7]
Seit 1911 wird die Adolf-von-Baeyer-Denkmünze in unregelmäßigen Abständen verliehen.

Sein Name findet sich wieder in verschiedenen „Namensreaktionen“ wie der Baeyer-Villiger-Oxidation und der Baeyer-Probe, außerdem in der Von-Baeyer-Nomenklatur sowie in der Strukturchemie bei der Baeyer-Spannung von alicyclischen Verbindungen.

Er wurde in verschiedene wissenschaftliche Akademien gewählt, so 1877 in die Bayerische Akademie der Wissenschaften,[9] 1884 in die Königlich-Preußische Akademie der Wissenschaften[10] und 1885 als auswärtiges Mitglied in die Royal Society,[11] 1898 in die National Academy of Sciences.

2009 wurde der Mondkrater von Baeyer nach ihm benannt.

Um 1860 erhielt Baeyer von Adolf Schlieper (1825–1887) Präparate der Pseudoharnsäure, Harnsäure und Alloxan.
Baeyer synthetisierte eine Reihe von verschiedenen Alloxanderivaten und konnte deren Konstitution bestimmen.[12]
1864 hatte Baeyer die Barbitursäure entdeckt und chemisch bestimmt.
Strecker hatte basierend auf Baeyers Ausarbeitungen in seinem Lehrbuch (5. Auflage) im Jahr 1868 die Strukturen von Alloxan, Barbitursäure, Hydantoin angegeben.
Bisher waren diese Verbindungen nur durch Abbaureaktionen hergestellt worden, Baeyer synthetisierte das Hydantoin aus Harnstoff. Eduard Grimaux entwickelte dann weiterführende Synthesen für Allantoin und Barbitursäure.

Als Baeyer mit seinen Synthesen zum Indigo begann, erkannte er die Ähnlichkeit des Isatin zu Alloxan.
Baeyer erkannte, dass mit Zinkstaub der Sauerstoff in einem Aromaten reduziert werden konnte. Er konnte aus Oxyindol das Indol erhalten.
Mit Emmerling entwickelte Baeyer nun eine Synthese des Indols (aus Nitrozimtsäure mit Kaliumhydroxid und Eisenspänen).[13]
Dann versuchte Baeyer einen ersten Strukturvorschlag für Isatin (das Oxidationsprodukt von Indigo), der danach jedoch von Kekulé korrigiert wurde.[14]
Im Jahr 1878 erhielten Baeyer und Emmerling durch Reduktion von o-Nitrophenylessigsäure über eine Oxyindol-Zwischenstufe das Isatin.[15] Noch war dieses Verfahren aufgrund der Nebenreaktionen nicht günstig.
Eine verbesserte Synthese von Indigo wurde etwas später von Baeyer ausgehend von o-Nitropropiolsäure durchgeführt.[16] Dieses Verfahren wurde von ihm patentiert[17] und an die Badischen Anilin- und Sodafabrik (BASF) zur industriellen Herstellung von Indigo (Heinrich Caro) abgetreten. Die Herstellungskosten waren aber im Vergleich zum Naturfarbstoff zu hoch, so dass dieser Syntheseweg wieder aufgegeben werden musste.

Später entwickelten Baeyer und Viggo Beutner Drewsen noch eine industriell unbedeutende Indigo-Synthese aus Nitrobenzaldehyd[18]
Im Jahr 1883 gelang Baeyer die korrekte Strukturaufklärung für das Indigo.[19]
Erst im Jahr 1900 entwickelte Karl Heumann eine wirtschaftliche Indigosynthese.[20]
Ein weiterer wirtschaftlich wichtiger Naturfarbstoff war damals das Alizarin.
Zur Stoffanalyse riet Baeyer seinen Assistenten Carl Graebe und Liebermann, diesen Stoff mit Zinkstaub zu reduzieren. Sie erhielten das Anthracen.

Graebe und Liebermann entwickelten nun eine neue Anthrachinonsynthese mit Kaliumdichromat und Schwefelsäure. Durch Behandlung des Anthrachinons mit Brom bei 100 °C und anschließender Behandlung mit Kaliumhydroxid konnte das Alizarin auch synthetisch dargestellt werden.
Die Stellung der Hydroxygruppen im Alizarin wurde durch eine Arbeit von Baeyer und Caro ermittelt.[21]
Baeyer entdeckte noch eine weitere Farbstoffgruppe. Durch das Erhitzen von Phthalsäureanhydrid mit Phenol entstand das Phenolphthalein, mit Resorcin das Fluorescein.[22] Unter dem Namen Eosin brachte Heinrich Caro den letzteren Farbstoff (als Tetrabromverbindung) bei der Anilin- und Sodafabrik in den Handel.
Baeyer konnte auch die Struktur des Phenolphthaleins aufklären.[23]
1872 beschrieb er erstmals die Polykondensation von Phenol und Formaldehyd (Phenoplast).[24]
Mit seinem Mitarbeiter Victor Villiger untersuchte von Baeyer die Konstitution der Terpene. Adolf von Baeyer stellte das Reagenzglas als das wichtigste Werkzeug für Chemiker heraus.
