Beatrix Alice Lehmann (geboren am 1. Juli 1903 in Bourne End, Buckinghamshire, England, gestorben am 31. Juli 1979 in London) war eine britische Theater- und Filmschauspielerin, sowie Theaterregisseurin und Romanautorin.

Lehmann war die jüngste Tochter von Rudolph Chambers Lehmann und dessen Frau Alice Marie Davis. Ihre Familie hatte Wurzeln unter anderem in Deutschland, Schottland und Amerika und war jüdisch geprägt. Ihr Vater war Journalist (Punch, The Daily News, Granta) und von 1906 bis 1910 Mitglied des britischen Unterhauses; ihr Urgroßvater Leo Lehmann und ihre Großonkel Rudolf Lehmann und Henri Lehmann waren bekannte Maler; ihr Großvater war der Verleger Robert Chambers. Ihre älteren Schwestern war Helen Chambers Lehmann (1899–1985) und die spätere Schriftstellerin Rosamond Lehmann, ihr Bruder war der spätere Dichter und Verleger John Lehmann.[1]
Sie wurde an der Royal Academy of Dramatic Art ausgebildet und hatte ihr Theaterdebüt 1924 in dem Stück The Way of the World in London. Am Theater verkörperte sie weitere Rollen unter anderem in Adaptionen von Schuld und Sühne und Staircase. 1946 wurde sie Regisseurin in der Arts Council Midland Theatre Company.[2] Sie trat neben Spielfilmrollen auch in britischen Fernsehserien auf, darunter auch Doctor Who[3].

Sie starb 1979 in Camden, London.

Sie schrieb mehrere Kurzgeschichten und zwei Romane. Davon bekannt geworden ist:
