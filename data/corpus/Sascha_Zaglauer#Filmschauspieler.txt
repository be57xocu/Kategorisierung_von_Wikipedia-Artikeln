Sascha Zaglauer (* 1968) ist ein deutscher Fernsehschauspieler.

Sascha Zaglauer absolvierte eine Schauspielausbildung. Danach konnte man in vielen Filmen und Fernsehserien sehen. Er wurde vor allem bekannt durch seine Rolle als Rajan Rai in der ARD-Soap Verbotene Liebe, die er ein Jahr lang spielte.
