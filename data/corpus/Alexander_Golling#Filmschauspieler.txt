Alexander Golling (* 2. August 1905 in München; † 28. Februar 1989 in Rottach-Egern/Oberbayern) war ein deutscher Schauspieler.[1]
Alexander Golling besuchte in seiner Heimatstadt München die Schauspielschule von Max Bayrhammer. Nach einem Debüt in Rudolstadt (1924) und Engagements in Erfurt, Heidelberg und Leipzig (Intendant Douglas Sirk), wo er bereits den Mephisto spielte, kam er 1934 zur Berliner Volksbühne. Alternierend mit Heinrich George gab er dort unter anderem den Franz Moor in Friedrich Schillers Drama Die Räuber. Seit 1934 sah man ihn als Charakterdarsteller in Filmen wie Der Kurier des Zaren, Der Tiger von Eschnapur und Das indische Grabmal. Neben einer Hauptrolle in Herbert Selpins U-Boot-Drama Geheimakte WB 1 (1941/42) hatte er größere Auftritte in den Filmen 90 Minuten Aufenthalt (1936), Dreizehn Mann und eine Kanone (1938) und Gold in New Frisco (1939).

Nach seinem Erfolg als Richard III. am Bayerischen Staatsschauspiel in München 1937 und der darauf folgenden Ernennung zum Staatsschauspieler wurde dem 32-Jährigen 1938 die vakante Intendanz dieses Theaters übertragen, ein Posten, den er bis zum Ende des Krieges innehatte. Kurz bevor die Bomben das Gebäude zerstörten, ließ Golling die Innenausstattung aus der Zeit des Rokoko ausbauen und in Sicherheit bringen. Durch diese Initiative konnte nach dem Krieg das Cuvilliés-Theater in alter Pracht wiedereröffnet werden. Auf der Münchner Bühne gab Golling etwa den Peer Gynt, den Macbeth und Papst Gregor VII. Letztere Rolle brachte ihm den Spitznamen der „der Theaterpapst“ ein. Daraus wurde nach dem Krieg, wegen seiner Nähe zum Nationalsozialismus, „der braune Theaterpapst“. Diese Nähe verhinderte nach dem Ende des Zweiten Weltkrieges eine nahtlose Fortsetzung seiner Filmkarriere. Der Prozess seiner Entnazifizierung von einer Münchener Spruchkammer ging durch drei Instanzen. In der ersten wurde er freigesprochen, in der zweiten als „Belasteter“ eingestuft. Das endgültige Urteil in dritter Instanz klassifizierte ihn 1948 dann als Mitläufer und verurteilte ihn zu einer Zahlung von 500 Mark.[2]
Ab 1948 spielte er bei Saladin Schmitt in Bochum erneut Theater. 1950 stand er wieder vor der Kamera und blieb zunächst auf Engagements bei Regisseuren wie Veit Harlan, Wolfgang Liebeneiner und Karl Ritter angewiesen, die in der Zeit des Nationalsozialismus ebenfalls auf der Seite des Regimes gestanden hatten. Bis Ende der 1970er Jahre trat er in Nebenrollen in 21 weiteren Filmen auf. Seit Mitte der 1960er Jahre war er daneben häufig auch in Fernsehproduktionen zu sehen.

Golling besetzte 1969 im Film Die Lümmel von der ersten Bank - Hurra die Schule brennt die Rolle des mürrischen Studienprofessors Blaumeier.

Alexander Golling war von 1937 bis 1941 mit der Schauspielerin Annie Markart verheiratet. Aus einer späteren Ehe stammt die Schauspielerin Claudia Golling (* 1950).

Seine Grabstätte befindet sich auf dem Friedhof in Rottach-Egern.

Joseph Anton von Seeau (1776-1799) |
Josef Marius Babo (1799-1810) |
Carl August  Delamotte (1810-1820) |
Joseph von Stich (1820-1823) |
Clemens von Weichs (1823-1824) |
Johann Nepomuk von Poißl (1824-1833) |
Karl Theodor von Küstner (1833-1842) |
Eduard Graf Yosch (1842-1844) |
August von Frays (1844-1847 /1848-1851 /1857-1860) |
Franz von Dingelstedt (1851-1857) |
Wilhelm Schmitt (1860-1867) |
Karl von Perfall (1867-1893) |
Ernst von Possart (1894-1905) |
Albert von Speidel (1905-1912) |
Clemens von Franckenstein (1912–1918) |
Viktor Schwanneke (1918) |
Karl Zeiss (1919–1924) | 
Clemens von Franckenstein (1924–1934) |
Oskar Walleck (1934–1938) |
Alexander Golling (1938-1945) |
Paul Verhoeven (1945-1948) |
Alois Johannes Lippl (1948-1953) |
Kurt Horwitz (1953-1958) |
Helmut Henrichs (1958-1972) |
Kurt Meisel (1972-1983) |
Frank Baumbauer (1983-1986) |
Günther Beelitz (1986-1993) |
Eberhard Witt (1993-2001) |
Dieter Dorn (2001-2011) |
Martin Kušej (seit 2011)
