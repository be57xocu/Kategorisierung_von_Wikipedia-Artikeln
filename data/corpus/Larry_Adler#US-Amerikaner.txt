Lawrence Cecil „Larry“ Adler (* 10. Februar 1914 in Baltimore, Maryland; † 7. August 2001 in London) war ein US-amerikanischer Mundharmonikaspieler und Autor.

Adler begann, ebenso wie sein jüngerer Bruder Jerry früh, Mundharmonika zu lernen. Bereits seit den 1920er Jahren wurde Adler für seine Virtuosität auf der Mundharmonika ausgezeichnet. Er spielte in den 1930er Jahren Kompositionen von Gershwin ein, arbeitete mit Django Reinhardt und dessen Quintette du Hot Club de France, mit John Kirbys Sextett sowie Tanz- und Showorchestern zusammen. In den 1930er Jahren war er als Schauspieler in einigen Filmen zu sehen, 1944 folgte eine weitere Rolle in Musik für Millionen. 1948 absolvierte er einen Auftritt als er selbst in Drei kleine Biester.

Für die Mitarbeit an dem Soundtrack für den Film Die feurige Isabella erhielt er 1955 eine Oscar-Nominierung, obgleich sein Name wegen des McCarthyismus nicht auf dem Filmabspann auftauchte. Später verfasste er für Harper’s Queen eine Lebensmittel-Kolumne. Seine Autobiographie, die nach der Komposition It Ain’t Necessarily So von Gershwin betitelt ist, erschien 1985.

Durch seine Mitarbeit an der CD The Glory of Gershwin, die bis auf Platz zwei der britischen Albumcharts stieg, erhielt er Anfang der 1990er Jahre einen Eintrag in das Guinness-Buch der Rekorde als ältester Musiker, der die Top Ten erreichte. Die ausgekoppelte Single The Man I Love von Kate Bush mit Larry Adler erreichte Platz 27 der britischen Hitparade.[1]
Einer seiner letzten Arbeiten war die Zusammenarbeit mit Frank Sinatra,
der ihn 1994 für sein 2. Album Duets engagierte, für das Lied For Once in My Life,
an dessen Anfang er noch einmal seine Mundharmonika-Kunst präsentierte.

Larry Adler starb am 7. August 2001 in London im Alter von 87 Jahren.

Filmmusik

Schauspieler
