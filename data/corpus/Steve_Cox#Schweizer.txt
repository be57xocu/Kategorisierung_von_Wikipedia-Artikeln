Steve Cox (* 15. September 1967) ist ein Schweizer Gleitschirmpilot aus Brugg.

Nach vielen zweiten Plätzen in internationalen Wettbewerben erreichte er 2005 den Weltmeistertitel an den Gleitschirmweltmeisterschaften in Valadares (Brasilien).
