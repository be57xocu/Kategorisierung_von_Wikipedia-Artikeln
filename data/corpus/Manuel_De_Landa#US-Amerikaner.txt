Manuel De Landa (* 1952 in Mexiko-Stadt) ist ein mexikanischer Schriftsteller, Künstler und Philosoph.

De Landa wurde als Professor an die „Graduate School of Architecture, Planning and Preservation“ der Columbia University in New York berufen. Er unterrichtet außerdem am jesuitischen Canisius College in New York.[1]
Seit 1975 lebt Manuel De Landa in New York.

Seine Arbeiten beziehen sich auf die Theorien von Gilles Deleuze und Félix Guattari. Es geht um die Anwendung dieser Theorien auf die moderne Wissenschaft, künstliche Intelligenz und Leben, Ökonomie, Architektur, Chaostheorie, nonlineare Wissenschaften und zelluläre Automaten. Seine Grundlagenforschung zur Morphogenesis − die Entstehung von semi-stabilen Strukturen aus den Materialflüssen −, die unsere natürliche und soziale Welt definieren, rief großes Interesse in verschiedenen Disziplinen und Fachbereichen hervor.
