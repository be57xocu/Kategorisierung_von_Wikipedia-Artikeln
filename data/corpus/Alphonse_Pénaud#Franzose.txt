Alphonse Pénaud (* 31. Mai 1850 in Paris; † 22. Oktober 1880 ebenda durch Suizid) war ein französischer Luft-Techniker.

Durch seine Arbeiten erlangte er umfangreiche Kenntnisse der Flugtechnik. Er erfand den noch heute im Modellsport verwendeten Gummimotor. Im Jahr 1871 flog sein von einem Gummimotor angetriebenes Flugmodell „Planophore“ mit 45 cm Flügelspannweite und 16 g schwer 60 m weit. Pénauds Planaphore regte nicht zuletzt die Gebrüder Wright an, die vor 1899 eins der Modelle besaßen und davon berichteten.

Pénaud veröffentlichte zahlreiche Arbeiten in der Zeitschrift L’Aéronaute, unter anderem seine Erklärung des „Thermischen Aufwinds“. 1876 ließ er sich ein erfolgreiches Schwingenflugzeug mit einziehbarem Fahrwerk, gewölbten Tragflächen und Einrichtungen zur Kompensation der Ruderkräfte patentieren.

Vermutlich wegen der zögerlichen Umsetzung seines Flugzeugprojekts nahm sich Pénaud 1880 das Leben.
