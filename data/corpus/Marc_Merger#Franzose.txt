Marc Merger (* 5. Mai 1961) ist der erste Mensch, dessen beschädigtes Rückenmark durch ein elektronisches Implantat überbrückt bzw. in seiner Funktion ersetzt wurde.

Merger zog sich eine Querschnittlähmung zu, als er 1990 am Steuer seines Wagens einschlief und eine Böschung hinabfuhr. Er erklärte sich bereit, sich für das Projekt Lève-toi et marche (bzw. Stand up and walk, seit 1997 ein europäisches Gemeinschaftsprojekt) zur Verfügung zu stellen.

Geplant war, ihm ein Steuerungsgerät in Gestalt eines Mikrochips unter die Bauchdecke zu legen, ferner pro Bein acht Kabel zu implantieren, die die Impulse an die Muskeln weiterleiten sollten. Die Fernbedienung - für jeden Schritt muss ein Kommando per Tastendruck erteilt werden - sollte in ein Gehgestell oder später in die Krücken Mergers integriert werden.

Der erste Versuch, das durch die Verletzung unterbrochene Nervensystem zu überbrücken, schlug fehl; das in den Bauchraum des Probanden eingesetzte Implantat arbeitete nicht wie gewünscht. In einem zweiten Anlauf gelang es im Jahr 2000, einen Teil seiner Beinmuskulatur an das Steuerungsgerät anzuschließen. Dies bedeutet allerdings nicht, dass Merger eine normale Gehfähigkeit zurückerlangt hätte, da längst nicht alle Muskeln in der Perfektion kontrolliert werden können, die ein natürliches Nervensystem zustande bringt, und da auch das Gleichgewicht durch die körperfremde Steuerung nicht kontrolliert werden kann. Merger ist aber in der Lage, aufzustehen und an einem Gehgestell, das die Tastatur für sein künstliches Nervensystem enthält, einige Schritte zu gehen.

In den Monaten nach der zweiten Operation äußerte er sich überaus positiv über das Ergebnis des Experimentes. Seine Erfahrungen hat er in dem Buch Lève-toi et marche (Edition Robert Laffont) niedergeschrieben; auch eine Verfilmung wurde geplant. In den letzten Jahren wurde es allerdings wieder stiller um Merger und das Experiment.

Merger arbeitete als Professor für Volkswirtschaft bereits an den Universitäten von Nancy, Metz und Straßburg. Er ist verheiratet und hat zwei Kinder. Er sitzt im Straßburger Bürgerrat.
