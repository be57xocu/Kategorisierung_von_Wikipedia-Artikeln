Karl Merkatz (* 17. November 1930 in Wiener Neustadt) ist ein österreichischer Schauspieler. Er wirkte in zahlreichen österreichischen Filmproduktionen und Theaterstücken mit.

Der Sohn eines Feuerwehrmannes absolvierte zunächst eine Tischlerlehre. Später nahm er in Salzburg, Wien und Zürich Schauspielunterricht und machte am Mozarteum seine Abschlussprüfung. Es folgten Theaterengagements in Heilbronn, Nürnberg, Salzburg, Köln, Hamburg, München und Wien (Theater in der Josefstadt).

In Heilbronn lernte er seine spätere Ehefrau Martha Metz kennen. Die beiden sind seit 1956 verheiratet und leben in Irrsdorf bei Straßwalchen. Der Ehe entstammen zwei Töchter, Gitta (* 2. Juli 1958) und Josefine (* 13. April 1962). 

Karl Merkatz spielte zahlreiche Rollen in Film und Fernsehen. Einem großen Publikum wurde er als „Mundl“ Edmund Sackbauer in der Fernsehserie Ein echter Wiener geht nicht unter (1975–1979) bekannt, einer Milieustudie, die das Leben einer Wiener Arbeiterfamilie zeigt. Großen Erfolg hatte er auch in Franz Antels Filmen um den Bockerer als Wiener Fleischhauer Karl Bockerer, der im Kampf gegen den Nationalsozialismus, im besetzten Nachkriegsösterreich, während des Ungarischen Volksaufstandes und im Prager Frühling gezeigt wird. Für den ersten Teil erhielt Merkatz den Preis als bester Schauspieler bei den Filmfestspielen Moskau sowie das Filmband in Gold.

Seit 2005 spielt er die Rolle des Benesch von Diedicz in König Ottokars Glück und Ende – die Inszenierung lief bei den Salzburger Festspielen und steht seit Oktober 2005 auf dem Programm des Wiener Burgtheaters.
Ebenfalls bei den Salzburger Festspielen 2005 spielte Karl Merkatz im Theaterstück Jedermann mit.

In den 1970er Jahren hat Karl Merkatz auch gesungen und einige Schallplatten auf den Markt gebracht.

Karl Merkatz engagiert sich für die Menschenrechtsplattform SOS Mitmensch, deren Vorsitzender er von 1999 bis 2001 war.

Im November 2005 übernahm Karl Merkatz eine Patenschaft um 4.800 Euro pro Jahr für einen Koala im Tiergarten Schönbrunn.
