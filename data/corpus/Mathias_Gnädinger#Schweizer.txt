Mathias Gnädinger (* 25. März 1941 in Ramsen, Kanton Schaffhausen; † 3. April 2015 in Zürich) war ein Schweizer Schauspieler und Synchronsprecher. Er lebte in Stein am Rhein.

Gnädinger erlernte den Beruf eines Schriftsetzers, bevor er in Zürich am Bühnenstudio seine Schauspielausbildung begann. Grossen Einfluss auf diesen "Kurswechsel" hatte sein Onkel, der Maler Josef Gnädinger, genannt Seppel[1]. In der Folge arbeitete er an verschiedenen Bühnen. Das letzte feste Engagement hatte er in Berlin an der Schaubühne. Ab 1988 war er freischaffender Schauspieler.

In seiner Laufbahn wirkte er in 130 Theaterstücken und über 70 Filmen für Kino und Fernsehen mit. 

Gnädinger starb am 3. April 2015 im Alter von 74 Jahren in Zürich.[2]