Sir Derek George Jacobi, CBE, (* 22. Oktober 1938 in Leytonstone, London, England) ist ein britischer Schauspieler.

Derek Jacobi ist das einzige Kind von Daisy Gertrude, geborene Masters und Alfred George Jacobi, der einen Süßigkeitenladen betrieb und in Chingford als Tabakwarenhändler tätig war. Sein Urgroßvater war im 19. Jahrhundert von Deutschland nach England eingewandert.

Jacobi wurde in Leytonstone im Osten Londons geboren und studierte einige Zeit an der Universität Cambridge, bevor er seine Leidenschaft für die Schauspielerei entdeckte. Nach der Rolle des Edward II in Cambridge wurde Jacobi 1960 direkt nach seinem Abschluss Mitglied im Birmingham Repertory Theatre. Laurence Olivier war von seinen Fähigkeiten beeindruckt und besetzte ihn als Cassio in seiner 1965er-Verfilmung von Shakespeares Othello. Der wirkliche Durchbruch gelang ihm jedoch erst 1976 mit I, Claudius, einer vielbeachteten Fernsehverfilmung des Bestsellers des Autors Robert Graves. Im Jahr 1994 wurde er für seine langjährigen Verdienste um das britische Theater zum Knight Bachelor ernannt.

Jacobi trat in vielen Shakespeare-Inszenierungen auf, unter anderem in mehreren Verfilmungen von Hamlet. Er wurde 1994 einem breiten Fernsehpublikum bekannt durch die Rolle des „Bruder Cadfael“ in den Verfilmungen der historischen Krimis von Ellis Peters. 1995 wurde Jacobi zusammen mit Duncan Weldon künstlerischer Leiter des Chichester Festival Theatre und spielte dort 1996 auch erstmals die Rolle des Onkel Wanja im Drama von Anton Tschechow. In Ridley Scotts Gladiator (2000) verkörperte er den Senator Gracchus. Nach einem Emmy als Nebendarsteller 1989 spielte Jacobi in seiner ersten Gastrolle im amerikanischen Fernsehen 2001 in einer Episode der Fernsehserie Frasier den schlechtesten Shakespeare-Darsteller der Welt. Das brachte ihm prompt eine weitere Emmy-Auszeichnung ein. Anfang 2006 war Jacobi in Len Wisemans Underworld: Evolution als Alexander Corvinus, der Urvater aller Vampire und Werwölfe, zu sehen.

In dem BBC-Webcast Doctor Who: Scream of the Shalka war Derek Jacobi als der Master zu hören. 2007 spielte er den Master in der Fernsehserie Doctor Who, Folge Utopia.

Derek Jacobi lebt seit 1977 mit seinem Partner Richard Clifford zusammen, der ebenfalls Schauspieler ist. 2006 gingen die beiden eine eingetragene Partnerschaft ein. Sie leben in Primrose Hill,  London.[1]
Theater

Fernsehen

Film
