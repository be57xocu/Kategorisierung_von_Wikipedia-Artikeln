Andrea Marie Ferrell (* in Salt Lake City, USA) ist eine US-amerikanische Schauspielerin.

Ihren ersten Auftritt im Fernsehen hatte die Schauspielerin 1996 in dem Film Ed McBain's 87th Precinct: Ice. Auch in der Fortsetzung Ed McBain's 87th Precinct: Heatwave war sie vertreten. Einem großen Publikum wurde sie in der Rolle der Heather Cain in der Fernsehserie Eine himmlische Familie bekannt, die sie zwischen 1997 und 2006 in 20 Folgen verkörperte.[1]
Ferrell ist von Geburt an gehörlos und spielt ausschließlich gehörlose Charaktere.
