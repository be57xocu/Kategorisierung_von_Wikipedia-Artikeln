Roswitha Haring (* 9. Oktober 1960 in Leipzig) ist eine deutsche Schriftstellerin.

Roswitha Haring absolvierte eine Kleidungsfacharbeiterlehre in Görlitz und legte dort ihr Abitur ab. Sie studierte Kulturwissenschaften in Leipzig und lebt in Köln. Für ihr Erstlingswerk, die Novelle Ein Bett aus Schnee, erhielt sie den Aspekte-Literaturpreis 2003. 2006 erhielt Haring den Kammweg Literaturpreis des Kulturraumes Erzgebirge.

diverse Anthologien und Beiträge in Literaturzeitschriften
