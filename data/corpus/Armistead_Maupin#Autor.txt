Armistead Maupin (* 13. Mai 1944 in Washington, D.C.) ist ein US-amerikanischer Schriftsteller.

Armistead Maupin studierte Englisch an der University of North Carolina und arbeitete als Reporter und für eine Nachrichtenagentur. Er lebt zurzeit in San Francisco.

Maupin wurde bekannt durch seine Stadtgeschichten („Tales of the City“), die zunächst ab 1974 als Fortsetzungsgeschichten im San Francisco Chronicle und der Pacific Sun erschienen und ab 1978 als (bisher neunbändige) Romanreihe veröffentlicht wurden.

Weitere Werke von Maupin neben den Stadtgeschichten sind „The Night Listener“ und „Maybe the Moon“.

Im Mittelpunkt der neunbändigen Romanreihe steht ein Haus in der Barbary Lane in San Francisco. Die Vermieterin Anna Madrigal kümmert und sorgt sich um die Mieter wie um ihre eigenen Kinder. Neben dem schwulen Michael Tolliver leben die esoterisch angehauchte Mona Ramsey, die aus Cleveland (Ohio) stammende Mary-Ann Singleton und der Kellner Brian Hawkins in ihrem Haus. Jeder der Bewohner ist auf der Suche nach dem ganz großen Glück.

Während Michael den Mann seines Lebens finden will, verfolgt Mary-Ann ihre ehrgeizigen beruflichen Ziele. Auch Brian und Mona sind auf der Suche nach der Erfüllung ihrer Lebensträume. Die Schicksale der vielen Figuren sind dabei eng miteinander verknüpft, und die Episoden der Stadtgeschichten nehmen häufig überraschende Wendungen. Selbst vor Tabuthemen macht Maupin in seinen Romanen nicht halt – so tauchen beispielsweise auch die Kulthandlungen einer Kannibalensekte auf.

Die Handlung erstreckt sich über mehr als zwei Jahrzehnte. Die einzelnen Geschichten sind eine Art Spiegelbild der amerikanischen Gesellschaft von den 1970ern bis in die achtziger Jahre hinein, da auch zeitgeschichtliche Themen wie die Kampagnen von Anita Bryant gegen die Gleichberechtigung von Lesben und Schwulen, der Ausbruch von AIDS oder der Massenselbstmord in Jonestown (Guyana) in die Handlung integriert wurden. Die ersten drei Romane wurden in einer Mini-Serie mit Laura Linney als Mary-Ann und Olympia Dukakis in der Rolle der Mrs. Madrigal für das Fernsehen verfilmt.

„The Night Listener“ handelt von dem Schriftsteller Gabriel Noone, der seine Geschichten nachts im Radio vorliest. Eines Tages erhält er von einem schwerkranken Fan einen Brief – es handelt sich um einen kleinen Jungen, der mit Hilfe der nächtlichen Radiosendungen seinem Leiden ein Stück weit entfliehen kann. Als sich zwischen den beiden über das Telefon eine Freundschaft aufbaut, zweifelt der Schriftsteller mehr und mehr an der Echtheit des Jungen und macht sich schließlich auf die Suche nach der geheimnisvollen Stimme. 2006 wurde der Roman unter demselben Titel verfilmt; Regisseur ist Patrick Stettner, die Hauptrolle spielt Robin Williams. Der deutsche Filmtitel ist The Night Listener – Der nächtliche Lauscher.

„Maybe the Moon“ handelt von der kleinwüchsigen Schauspielerin Cadence Roth, die die Rolle des Waldgnoms Mr. Woods in einem Hollywoodfilm gespielt hat. Mit Galgenhumor und Selbstironie lässt Maupin seine Heldin in ihrem Tagebuch berichten, mit welchen Schwierigkeiten und Absurditäten eine Zwergin in Hollywood zu kämpfen hat. Die Heldin des Romans hat es wirklich gegeben. Sie hieß Tamara De Treaux und spielte den Außerirdischen E.T. in Steven Spielbergs gleichnamigem Film.
