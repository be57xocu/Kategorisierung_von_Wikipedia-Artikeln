Glen David Brin (* 6. Oktober 1950 in Glendale, Kalifornien) ist ein US-amerikanischer Science-Fiction-Autor. 

Seine Werke wurden mit zahlreichen Preisen ausgezeichnet – unter anderem erhielt er den Hugo, den Nebula und den Locus Award. Er lebt in Südkalifornien, studierte Astronomie am California Institute of Technology und erwarb einen Doktorgrad in Astrophysik an der University of California, San Diego. 

Er war als NASA-Berater und Physik-Professor tätig. 1994 wurde der Asteroid (5748) Davebrin nach ihm benannt.

1983 schlug David Brin eine erweiterte Drake-Gleichung vor.[1][2]
Die Idee zum Uplift-Universum geht auf die Hypothese Erich von Dänikens zurück, der zufolge die Götter Außerirdische waren. Die ursprünglichen »Progenitoren« müssen immer schon intelligent gewesen sein, aber jede andere potenziell intelligente Lebensform wurde erst seitens einer „Patronatsrasse“ auf galaktisches Niveau „geliftet“ (Steigerung der Intelligenz und eventuell auch der körperlichen Fähigkeiten mittels Genmanipulation). Die Menschheit ist eine Anomalie, eine „Wölflingsrasse“, weil sie keine Patrone besitzt oder diese zumindest unbekannt sind. Daher fehlt den Menschen auch ein grundlegendes Verständnis für die Feinheiten der galaktischen Kultur.

Es gibt außerdem einen „Uplift“-Zusatz zu dem Rollenspiel GURPS, der es Rollenspielern erlaubt, Abenteuer in dem Universum zu erleben, in dem diese Romane spielen. Dieser Zusatz wurde nicht von David Brin geschrieben, basiert aber auf seinem Werk.

Eine andere Arbeit von David Brin ist sein Roman, der in Isaac Asimovs Foundation-Universum spielt. Es handelt sich um den dritten, abschließenden Band des zweiten Foundation-Zyklus.

„Physikalische Mehrkörperprobleme tendieren dazu, unlösbar zu sein, und soziale sind es. Nach neunhundertfünfzig Seiten hat David Brin den Weltgeist zum Leben erweckt, während ein Wissenschaftler mehr zufällig zum Schöpfer eines neuen Universums geworden ist. Kleiner ging es offenbar nicht. Mutter Erde selbst vernetzt sich mit dem globalen Netz und übernimmt, weise Gaia, das Regime, allen Menschen ein Wohlgefallen (na ja, den meisten). Und wieder einmal rettet sich ein ansonsten respektabel auf den Brettern des wissenschaftliches Weltbildes festgezimmerter und kompetent geschriebener SF-Roman letztlich ins Spirituelle, entschärft seine eigenen Fragen und legt die Axt der Transzendenz an seine eigenen Wurzeln. Wenn eh der Weltgeist das Regiment übernimmt, können wir ruhig weiterwursteln. Bedenklicher Fakt: Autoren, die mit weit offenen Augen die Misere dieser Welt wahrnehmen, können sich nur mit Hilfe einer mythischen Weltmutter Gaia in ein zweifelhaftes Happy End retten.“
