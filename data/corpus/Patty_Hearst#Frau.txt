Patty Hearst (* 20. Februar 1954 als Patricia Campbell Hearst in San Francisco, Kalifornien) ist eine Enkeltochter des einflussreichen US-amerikanischen Medienmoguls William Randolph Hearst. Sie wurde 1974 durch eine spektakuläre Entführung durch die linksradikale Symbionese Liberation Army (SLA) bekannt. Sie schloss sich dieser an, wurde nach ihrer Verhaftung zu einer Gefängnisstrafe wegen Bankraubs verurteilt und später begnadigt.

Patty Hearst wuchs in einem wohlhabenden Umfeld in San Francisco als dritte von fünf Töchtern auf. Ihr Vater war der jüngste Sohn von William Randolph Hearst, Randolph Apperson Hearst.[1] Sie besuchte die Santa Catalina School in Monterey.

Die Symbionese Liberation Army (SLA) entführte die damals 19-jährige Millionenerbin am 4. Februar 1974. Hearst wurde eigenen Aussagen zufolge während der Entführung 57 Tage lang in einen Schrank von 1 × 1,7 Meter Größe gesperrt und misshandelt. Die SLA erpresste von der Familie Hearst mehrere Millionen US-Dollar, von denen die Entführer Lebensmittel kauften, die in den Armenvierteln von San Francisco, Oakland und Berkeley verteilt wurden.

Am 3. April 1974, zwei Monate nach ihrer Entführung, gab sie per Audionachricht bekannt, dass sie sich der SLA angeschlossen habe. Die Hintergründe für diese Handlung sind bis heute nicht geklärt. Bei der SLA nahm sie den Namen „Tania“ (inspiriert von der Guerrillera Tamara „Tania“ Bunke) an und beteiligte sich an mehreren Überfällen. Später ließ sie über ihren Anwalt eine Stellungnahme abgeben, der zufolge sie von der SLA unter LSD gesetzt und gegen ihren Willen zur Teilnahme an den Überfällen gezwungen worden sei. Zudem sei sie sexuell missbraucht worden.

Am 18. September 1975 wurde Patty Hearst, neben anderen SLA-Mitgliedern, verhaftet und 1976 von einem Gericht in San Francisco zu 35 Jahren Haft verurteilt. In einem Berufungsprozess vor dem Bundesbezirksgericht für Nordkalifornien wurde die Strafe auf sieben Jahre herabgesetzt. Nach 22 Monaten wurde Hearst vom damaligen US-Präsidenten Jimmy Carter begnadigt und am 1. Februar 1979 aus dem kalifornischen Bundesgefängnis bei San Francisco entlassen. Am 20. Januar 2001 erhielt sie ein „presidential pardon“ (vollständige Begnadigung) durch Bill Clinton am letzten Tag seiner Amtszeit.

Patty Hearst heiratete nach ihrer Freilassung ihren ehemaligen Leibwächter Bernard Shaw, der 2013 verstarb. Sie hat zwei Töchter, Gillian und Lydia, und lebt in New York.

Nach ihrer Begnadigung wirkte Patty Hearst in mehreren Filmen als Schauspielerin mit, unter anderem in John Waters’ Komödien Serial Mom – Warum läßt Mama das Morden nicht? (1994) und in A Dirty Shame (2004).

Patty Hearsts Autobiografie Every Secret Thing wurde 1988 von Regisseur Paul Schrader als Patty verfilmt. Ihre Entführung war auch Thema von Dokumentarfilmen wie Zorro und Patty Hearst (2004).[2]
Anthony Davis und Michael John LaChiusa schrieben 1991 die Oper Tania.

Die Berliner Band Stereo Total widmete Hearst auf ihrem 2007er Album Paris<>Berlin einen nach ihr benannten Song und produzierte für den Bayerischen Rundfunk das Hörspiel Patty Hearst – Princess and Terrorist.[3]
Die amerikanische Punkrock-Band Smoke or Fire befasste sich in dem Lied The Patty Hearst Syndrome auf ihrem Album The Sinking Ship (2007) mit Patty Hearst.
