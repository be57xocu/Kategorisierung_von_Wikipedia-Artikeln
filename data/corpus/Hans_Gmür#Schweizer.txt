Hans Gmür (* 1. Februar 1927 in Chur; † 15. April 2004 in Nottwil) war ein Schweizer Autor.

Gmür war Autor, Regisseur und Produzent von Komödien, Schwänken, Cabaretstücken und Musicals. Schon als Student an der Universität Zürich schrieb er Texte für das Cabaret Fédéral. Von 1953 bis 1959 führte er Regie bei Werbe- und Dokumentarfilmen. 1960 entstand sein erstes Stück, Schön ist die Jugend.

Nach einem Abstecher in die Werbung war Gmür bis 1964 bei Radio Zürich (dem heutigen Radio SRF) zuständig für das Ressort Unterhaltung. Nebenbei schrieb er ab 1964 Unwahre Geschichten für die Weltwoche. Er war von 1966 bis 1970 neben Eva Maria Borer Co-Chefredaktor der Frauenzeitschrift Annabelle und machte sich 1972 selbständig.

Im Schweizer Radio machte er in den 60er- und 70er-Jahren Geschichte mit Sketchen am Samstagnachmittag wie «Mini Meinig – Dini Meinig», «Oder» und «Barbier von Seldwyla»; er verfasste über 50 Musicals, darunter «Helden, Helden» (zusammen mit Udo Jürgens), «Bibi Balù», «Z wie Züri» und «Hotel Happy End», daneben präsentierte er ab 1974 jahrelang den «Bernhard-Apéro» im Zürcher «Bernhard-Theater». 1975 produzierte Gmür zusammen mit dem Trio Eugster und Ines Torelli deren Hit Gigi von Arosa. Von 1977 bis 1993 führte er durch die deutsche Fernsehratesendung Ich trage einen großen Namen.

Hans Gmür arbeitete mit bekannten Schweizern wie Werner Wollenberger, Max Rüeger und Charles Lewinsky zusammen. In seinen Stücken, darunter viele heitere Zürcher Musicals, spielten unter anderem Schauspieler wie Ruedi Walter, Margrit Rainer, Stephanie Glaser, Jörg Schneider, Ursula Schaeppi, Ines Torelli und Paul Bühlmann. Der Höhepunkt seiner Karriere war die Goldene Rose von Montreux 1969 für die Fernsehfassung der Revue Holiday in Switzerland. Insgesamt schrieb er über 50 Stücke.

Seine Stimme und sein Bündner Dialekt prägten viele Jahrzehnte Schweizer Bühnen-, Hörspiel- und Radioproduktionen.
