George Francis Abbott (* 25. Juni 1887 in Forestville, New York; † 31. Januar 1995 in Miami Beach, Florida) war ein US-amerikanischer Theaterproduzent, Filmproduzent, -regisseur und Drehbuchautor.[1]
George Abbott studierte ab 1907 zunächst Journalismus an der University of Rochester, wandte sich aber bereits dort der Dramatik zu. 1911 begann er ein Theaterstudium an der Harvard University, wo er an George Pierce Bakers Dramakurs „47 Workshop“ teilnahm.

1913 debütierte Abbott am Broadway und blieb dort bis Mitte der 1920er Jahre als Schauspieler tätig. Zudem fand er 1915 bei dem Theaterproduzenten John Golden eine Anstellung als „assistant casting director“ und „associate playwright“. 1923 wurde er für seine Rolle in der Komödie Zander the Great unter die zehn besten Schauspieler gewählt. 1924 spielte er eine Hauptrolle in dem Pulitzer-Preis-prämierten Schauspiel Hell-bent Fer Heaven von Hatcher Hughes.

Seinen ersten großen Erfolg als Autor und Regisseur hatte Abbott 1926 mit dem Schauspiel Broadway, dass er zusammen mit Philip Dunning schrieb. Hauptsächlich in der Zeit zwischen 1928 und 1932 arbeitete er auch als Regisseur und Drehbuchautor auch in Hollywood. Hier arbeitete er unter anderem am Drehbuch des Filmklassikers Im Westen nichts Neues (1930) mit. Sein erstes erfolgreiches selbst produziertes Theaterstück war die Komödie Twentieth Century im Jahr 1932.  

George Abbott war in seiner mehr als siebzigjährigen Broadway-Laufbahn anfänglich als  Schauspieler, später als Autor (Dramatiker), Regisseur und Produzent tätig. Darüber hinaus machte er sich einen Namen als „show doctor“, wobei er, als Ko-Regisseur, -Produzent oder -Autor, an der Überarbeitung bzw. Umgestaltung von Shows, die während der Tryouts oder Previews nicht befriedigten, mitwirkte.[2]
Bekannte Theaterregisseure wie Jerome Robbins, Bob Fosse und Harold Prince erlernten bei Abbott das Regiehandwerk. 

Abbott war dreimal verheiratet, aus der ersten Ehe (1914) mit Frau Ednah Levis ging Tochter Judith hervor, die Schauspielerin wurde und 1946 den Schauspieler Tom Ewell heiratete. Ednah starb 1930 und Abbott heiratete 1946 Mary Sinclair von der er im April 1951  geschieden wurde. Am 21. November 1983, fünf Monate nach seinem 96. Geburtstag, heiratete er Joy Valderrama.

Abbott starb an einem Schlaganfall in Miami Beach, vier Monate und 25 Tage vor seinem 108. Geburtstag.[3]
Broadway-Produktionen (Auswahl)

Musical-Produktionen (Auswahl) 

Drehbuchautor
