Bernard Kouchner (* 1. November 1939 in Avignon) ist ein französischer Politiker und Arzt. Er ist Mitgründer von Médecins sans Frontières (MSF, Ärzte ohne Grenzen), Médecins du Monde (MDM, Ärzte der Welt) und war vom 18. Mai 2007 bis 14. November 2010 französischer Außenminister und Minister für Europäische Angelegenheiten in der Regierung von François Fillon. In der zweiten Jahreshälfte 2008 war er außerdem Präsident des Rats der Europäischen Union.

Kouchner ist ein Gastroenterologe. Der Sohn eines jüdischen Vaters und einer protestantischen Mutter begann seine politische Karriere als Mitglied der Kommunistischen Partei, aus der er 1966 ausgeschlossen wurde. 1968 arbeitete er als Arzt für Secours médical français (SMF) in der nigerianischen Provinz Biafra. Der dortige Bürgerkrieg wurde zu einem Schlüsselerlebnis für Kouchner. Angesichts des Leidens und des Hungers der Bevölkerung und der Grausamkeit der Soldateska wollte sich der junge Arzt nicht an das vom SMF geforderte Schweigegebot halten. Dies war für ihn mit dem Eid des Hippokrates nicht zu vereinbaren. „Unparteilichkeit ja, Neutralität nein.“ So diente ihm die humanitäre Aktion auch dazu, die Unterdrückung und die Verbrechen sichtbar zu machen.

1971 gründete er zusammen mit anderen engagierten Medizinern die nichtstaatliche Organisation Médecins sans Frontières, die aus der französischen Secours médical français hervorging. Darüber hinaus geriet Kouchner mit dem Direktor von MSF Claude Malhuret in Meinungsverschiedenheiten und trat aus MSF aus, um 1980 die zweite Hilfsorganisation Médecins du Monde (MDM) zu gründen. Kouchners „french doctors“ wurden bald in den Konflikt- und Krisengebieten rund um den Erdball zu einem Begriff, ebenso wie sein Credo: Das Recht, ja, die Pflicht, sich einzumischen, um das Elend der Menschen in aller Welt zu bekämpfen. „Das Recht auf humanitäre Intervention (droit d’ingérence humanitaire) geht vor. Im Zweifelsfall sogar vor staatliche Souveränität.“

Er trat für ein Langzeitkonzept humanitärer Einmischung ein. 1993 gründete er deshalb die Stiftung Fondation pour l’action humanitaire. Obwohl er zu diesem Zeitpunkt nicht Mitglied einer politischen Partei war, kandidierte er 1994 auf der Liste von Michel Rocard erfolgreich zum Europäischen Parlament. 1995 trat er der Parti radical de gauche bei, deren Pressesprecher er wurde. Gleichzeitig trat er für die Reformen der Sozialen Sicherheit des gaullistischen Ministers Alain Juppé ein. Er leitete den Club Réunir (Vereinigung) und stand sowohl Michel Rocard als auch Lionel Jospin nahe.

Von 1999 bis 2001 entsandte ihn der Generalsekretär der Vereinten Nationen Kofi Annan als Sondergesandten und Leiter der UNMIK ins Kosovo. Kouchner war persönlich befreundet mit dem brasilianischen Diplomaten und UN-Hochkommissar für Menschenrechte Sérgio Vieira de Mello.

Er ist Autor einer Reihe von Büchern mit hauptsächlich medizinisch-humanitären und politischen Themen. Sein Bruder Jean Kouchner ist Direktor des kommunistischen Pariser Radiosenders TSF. In erster Ehe war Bernard Kouchner mit Évelyne Pisier verheiratet, die nach ihrer Scheidung den Europäischen Abgeordneten der sozialistischen Partei und Verfassungsrechtler Olivier Duhamel heiratete. Die aktuelle Lebensgefährtin Kouchners ist die Journalistin Christine Ockrent.

Das so genannte loi Kouchner (Kouchner-Gesetz), offiziell Suspension de peine pour raison médicale, sieht einen Antrag auf Haftaussetzung für Gefangene vor, deren Gesundheitszustand mit der Haft nicht vereinbar ist. Die Regelung führte insbesondere durch die Anwendung im Fall des Kriegsverbrechers Maurice Papon zu einer öffentlichen Kontroverse.

Unmittelbar nachdem Bernard Kouchner von Präsident Nicolas Sarkozy im Mai 2007 zum Außenminister seiner konservativen Regierung ernannt worden war, erklärte die Parti Socialiste Kouchners Ausschluss: „Wer in diese Regierung eintritt“, verkündete Parteichef François Hollande, „ist ein rechter Minister und kann nicht gleichzeitig den Sozialisten angehören. Kouchner ist nicht mehr Mitglied der Sozialistischen Partei.“ PS-Fraktionschef Jean-Marc Ayrault kritisierte, die von Sarkozy versprochene „Öffnung“ beschränke sich auf vereinzelte Abwerbungen und Kouchner, der eine „Persönlichkeit ohne Grenzen“ sei und nun „sorgfältig unter die direkte Oberaufsicht des Elysée gestellt wurde“.

Seit Mai 2015 ist Kouchner als Workstream-Leader für die Agentur zur Modernisierung der Ukraine (AMU) tätig. Sein Ziel ist die Ausarbeitung eines Modernisierungsprogramms für medizinisch-humanitäre Themen.[1]
François Fillon |
Alain Juppé |
Bernard Kouchner |
Hervé Morin |
Michèle Alliot-Marie |
Brice Hortefeux |
Rachida Dati |
Jean-Louis Borloo |
Éric Woerth |
Xavier Bertrand |
Christine Boutin |
Christine Lagarde |
Xavier Darcos |
Valérie Pécresse |
Christine Albanel |
Roselyne Bachelot

François Fillon |
Jean-Louis Borloo |
Bernard Kouchner |
Hervé Morin |
Michèle Alliot-Marie  |
Brice Hortefeux  |
Christine Lagarde |
Éric Woerth |
Xavier Darcos |
Valérie Pécresse |
Roselyne Bachelot |
Éric Besson |
Luc Chatel |
Bruno Le Maire |
Frédéric Mitterrand |
Michel Mercier

Maurice Couve de Murville |
Michel Debré |
Maurice Schumann |
André Bettencourt |
Michel Jobert |
Jean Sauvagnargues |
Louis de Guiringaud |
Jean François-Poncet |
Claude Cheysson |
Roland Dumas |
Jean-Bernard Raimond |
Roland Dumas |
Alain Juppé |
Hervé de Charette |
Hubert Védrine |
Dominique de Villepin |
Michel Barnier |
Philippe Douste-Blazy |
Bernard Kouchner |
Michèle Alliot-Marie |
Alain Juppé |
Laurent Fabius |
Jean-Marc Ayrault |
Jean-Yves Le Drian

Bernard Kouchner |
Hans Hækkerup |
Michael Steiner |
Harri Holkeri |
Søren Jessen-Petersen |
Joachim Rücker |
Lamberto Zannier |
Farid Zarif |
Zahir Tanin
