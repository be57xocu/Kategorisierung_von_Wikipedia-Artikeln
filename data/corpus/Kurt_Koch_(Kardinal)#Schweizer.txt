Kurt Kardinal Koch (* 15. März 1950 in Emmenbrücke, Kanton Luzern) ist ein Schweizer Theologe, Kurienkardinal der römisch-katholischen Kirche und ehemaliger Bischof von Basel.

Koch studierte Theologie an der Ludwig-Maximilians-Universität München und der Universität Luzern und wurde 1975 diplomiert. Zunächst arbeitete er als Laientheologe in Sursee. Am 20. Juni 1982 empfing er die Priesterweihe und wirkte daraufhin drei Jahre als Vikar in der Pfarrei St. Marien in Bern. Nachdem er 1986 Dozent für Dogmatik und Moraltheologie am Katechetischen Institut in Luzern geworden war, wurde er 1987 aufgrund einer Arbeit über Wolfhart Pannenberg promoviert. 1989 habilitierte er sich. Koch wurde zum Honorarprofessor für Dogmatik, Ethik, Liturgiewissenschaft und Ökumenische Theologie an der Theologischen Fakultät der Universität Luzern ernannt.

1995 wurde Kurt Koch vom Basler Domkapitel als Nachfolger von Hansjörg Vogel zum Bischof von Basel gewählt. Die Bischofsweihe spendete ihm am 6. Januar 1996 Papst Johannes Paul II. Sein bischöflicher Wahlspruch lautet: «Christus hat in allem den Vorrang» und stammt aus Kol 1,18 EU.

Durch einen Konflikt mit Franz Sabo, der als Pfarrer in Röschenz im Kanton Basel-Landschaft arbeitet und sich in den Medien ab 2003 kritisch zu Koch sowie Generalvikar Roland-Bernhard Trauffer und deren Amtsführung äusserte, kam das Bistum in die Schlagzeilen. Koch machte in diesem Zusammenhang insbesondere von sich reden, als er einen Entscheid eines weltlichen Gerichts gegen ihn mit der Forderung nach Trennung von Kirche und Staat beantwortete. Der Konflikt wurde im September 2008 in persönlichen Gesprächen beigelegt.[1]
Kurt Koch war in den Jahren 2007 bis 2009 Präsident der Schweizer Bischofskonferenz. Er hat über 60 Bücher und Schriften verfasst, darunter Mut des Glaubens (1979) und Eucharistie (2005).

Papst Benedikt XVI. ernannte ihn am 1. Juli 2010 zum Präsidenten des Päpstlichen Rates zur Förderung der Einheit der Christen[2] und verlieh ihm aus diesem Anlass den Titel eines Erzbischofs ad personam.[3] Papst Franziskus bestätigte diese Ernennung am 19. Februar 2014.[4] Kurt Koch folgte in diesem Amt Walter Kardinal Kasper nach. Er stand dem Bistum Basel bis zur Amtseinführung seines Nachfolgers Felix Gmür am 16. Januar 2011 als Apostolischer Administrator vor.[5]
Im Konsistorium vom 20. November 2010 nahm ihn Benedikt XVI. als Kardinaldiakon mit der Titeldiakonie Nostra Signora del Sacro Cuore in das Kardinalskollegium auf.[6]
Nach dem Rücktritt Benedikts XVI. nahm Kardinal Koch am Konklave 2013 teil.

Kurt Koch ist Mitglied folgender Dikasterien der römischen Kurie:

Über Kurt Koch

Pierre-François de Preux |
Etienne Marilley |
Carl Johann Greith |
Eugène Lachat |
Gaspard Mermillod |
Adrien Jardinier |
Augustin Egger |
Johannes Fidelis Battaglia |
Jules-Maurice Abbet |
Jakob Stammler |
Georg Schmid von Grüneck |
Aurelio Bacciarini |
Viktor Bieler |
Angelo Jelmini |
Johannes Vonderach |
François-Nestor Adam |
Anton Hänggi |
Pierre Mamie |
Otmar Mäder |
Henri Schwery |
Otmar Mäder |
Pierre Mamie |
Henri Salina |
Amédée Grab |
Kurt Koch |
Norbert Brunner |
Markus Büchel |
Charles Morerod (seit 2016)
