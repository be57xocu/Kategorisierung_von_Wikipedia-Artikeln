Paul Grimault (* 23. März 1905 in Neuilly-sur-Seine; † 29. März 1994 in Mesnil-Saint-Denis) war einer der bedeutendsten französischen Zeichentrickfilmer und Regisseure.

Bekannt wurde er ab 1937 durch seine Kinder- und Märchenfilme, die zunächst als Kurzfilme, später als Trickfilme produziert wurden.

Sein Kurzfilm Der kleine Soldat (1947) nach einem Märchen von Hans Christian Andersen,  zu dem Jacques Prévert das Drehbuch schrieb, bekam den Preis des besten Zeichentrickfilms beim Festival von Venedig 1948, gemeinsam mit Melody Time von Walt Disney.

Sein bekanntestes und auch persönlichstes Werk ist Der König und der Vogel.
Der französische Meister des Trickfilmes arbeitete seit 1947 an Andersens Die Hirtin und der Schornsteinfeger. Nachdem sein Werk 1953 in einer, laut Grimault, verunstalteten Fassung aufgeführt worden war, erwarb er 1966 die Rechte am Zeichentrickfilm und gestaltete als 75-Jähriger seine persönliche Fassung. Die Neufassung, welche Dialoge und Chansontexte von Jacques Prévert beinhaltet, wurde mehrfach international ausgezeichnet. 

1979 gewann Paul Grimault den Louis-Delluc-Preis für Der König und der Vogel. 1989 bekam er den Ehrenpreis des französischen Filmpreises César verliehen. 
