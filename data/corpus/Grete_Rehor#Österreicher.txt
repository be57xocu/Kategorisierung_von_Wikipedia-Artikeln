Grete Rehor (* 30. Juni 1910 als Grete Daurer in Wien; † 28. Januar 1987 ebenda) war eine österreichische Politikerin (ÖVP) und erste österreichische Ministerin.

Grete Rehor wurde 1910 als zweites von drei Kindern einer diplomierten Krankenschwester und eines Beamten in Wien geboren. Bereits früh musste sie einen schweren Schicksalsschlag hinnehmen: Ihr Vater kehrte aus dem Ersten Weltkrieg nicht mehr zurück. Nach der fünfjährigen Volksschule in Wien-Josefstadt besuchte sie die Bürgerschule und ein einjähriges Lehrerseminar. Allerdings konnte sie ihren Berufswunsch, Lehrerin zu werden, wegen der schweren wirtschaftlichen Zeiten in den Nachkriegsjahren nicht verwirklichen. So begann sie als Textilarbeiterin zu arbeiten, um sich den Besuch einer Handelsschule zu ermöglichen. „Ihr war schon damals bewusst, dass nur eine qualifizierte Berufsausbildung dem sozialen und wirtschaftlichen Aufstieg der Arbeitnehmer in Österreich förderlich sein kann.“[1] Doch auch sozialpolitischen Abendkursen galt ihr Interesse, in denen sie sich „...das geistige Rüstzeug für ihre spätere gewerkschaftliche und politische Tätigkeit“ holte.[1]
„Ihre gewerkschaftliche Tätigkeit begann im Jahre 1927 als hauptamtliche Sekretärin im Zentralverband der christlichen Textilarbeiter Österreichs.“[2] Auch in dieser Zeit musste Rehor einen weiteren Schicksalsschlag verkraften. Ihre Mutter starb, und so war Grete Rehor im Alter von 19 Jahren Vollwaise. Von 1928 bis 1938 war sie das erste weibliche Mitglied im Jugendbeirat der Arbeiterkammer Wien. Sie konnte sich mit den Problemen der arbeitenden Jugend identifizieren und ihnen als deren Sprecherin in vielen Bereichen Beratung und Hilfe angedeihen lassen.[1] So hatte sie einen maßgeblichen Einfluss in den Aktionen „Jugend am Werk“, „Jugend in Not“ und „Jugend in Arbeit“.[3]
Im Jahr 1935 heiratete sie den christlichen Gewerkschafter und späteren Stadtrat Karl Rehor. Ihr Mann gründete zusammen mit dem späteren Bundeskanzler Josef Klaus die christliche Jugendbewegung Junge Front im Arbeiterbund. Nach der Machtübernahme der Nationalsozialisten wurde Karl Rehor zuerst inhaftiert, wenig später zur Wehrmacht eingezogen und fiel 1943 in Stalingrad. Grete Rehor war von nun an Kriegswitwe und alleinerziehende Mutter einer Tochter, der sie sogar ein akademisches Studium ermöglichen konnte.

1945, kurz nach Kriegsende, entstand der Österreichische Gewerkschaftsbund. Unmittelbar darauf wurde Grete Rehor als Fachgruppensekretärin der Weber in der Gewerkschaft der Textil-, Bekleidungs-, und Lederarbeiter tätig. Am 16. April 1948 wurde sie Vorsitzenden-Stellvertreterin und als Bundesvorsitzende der FCG in diese Fachgewerkschaft gewählt, die zu einer der größten Gewerkschaften in dieser Zeit zählte.[1] Somit war der erste grundlegende Schritt für eine spätere Ministerkarriere getan, denn: „um für die Position als SozialministerIn überhaupt in Frage zu kommen, war – den bisherigen politischen Gepflogenheiten und der Realverfassung zufolge – eine hohe Funktion in einem Verband Voraussetzung.“[4] Zudem war Rehor auch noch Bundesvorsitzende der Fraktion Christlicher Gewerkschafter in dieser Fachgewerkschaft. „Kein Weg war ihr zu beschwerlich und kein Betrieb zu entfernt, um den Kontakt mit allen Gewerkschaftern unter schwierigsten Bedingungen im ganzen Land aufrechtzuerhalten.“[5] Viele Wege mussten damals zu Fuß gemacht werden, und die Aufteilung Österreichs in vier Zonen tat ihr Übriges. Zudem standen einige Betriebe auch unter der Führung von Besatzungstruppen.[1]
Bereits an den ersten Lohn- und Tarifverhandlungen beteiligte sich Grete Rehor maßgeblich und erfolgreich. Durch ihren Einsatz wurde sie von beiden Sozialpartnern hoch geschätzt, und ihrem Bemühen ist es zu verdanken, dass die Lohnunterschiede zwischen männlichen und weiblichen Arbeitern aufgehoben wurden. Grete Rehor verfolgte das politische Ziel Gleicher Lohn für gleiche Arbeit.

„Im Rahmen des Frauenreferats des ÖGB übte sie die Funktion einer Vorsitzendenstellvertreterin aus und als Mitglied des Bundesvorstandes des ÖGB arbeitete sie intensiv an der Schaffung eines einheitlichen Gewerkschaftsbundes.“[5]
1949 wurde sie von der Österreichischen Volkspartei als erste Frau für den größten Wahlbezirk, Wien-West, nominiert. So nahm sie von 1949 bis 1970 ihre Verantwortung als Abgeordnete zum Nationalrat wahr.[5] Vor allem setzte sie sich für berufstätige Frauen und Mütter ein, und so gründete sie 1957 das Frauenreferat des ÖAAB, das sie bis 1975 leitete. Grete Rehor versuchte, Frauen für politische und gewerkschaftliche Tätigkeiten in Betrieben und Dienststellen zu motivieren und machte die „Frauen im ÖAAB“ zur stärksten Frauengruppierung der ÖVP.[5]
Am 6. März 1966 errang die ÖVP die absolute Mehrheit. Koalitionsverhandlungen mit der SPÖ scheiterten, daher wurde eine Alleinregierung gebildet. Grete Rehor wurde die erste Bundesministerin der Republik Österreich. (Eine Unterstaatssekretärin gab es schon 1945 8 Monate lang mit Helene Postranecky.)  Klaus erzählt: „In einem Gespräch mit dem Obmann des ÖAAB, Alfred Maleta, teilte ich ihm meine Absicht, Dich [Rehor] zum Sozialminister vorzuschlagen – und fand sofort seine lebhafte Zustimmung: Eine ehemalige Textilarbeiterin, Kriegswitwe, Gewerkschaftssekretärin, erprobte Parlamentarierin, jahrzehntelanges Mitglied des Sozialausschusses – und noch dazu eine Frau, die Bundesleiterin der „Frauen im ÖAAB“, eine Wienerin mit Charme, Witz und Schlagfertigkeit, das wäre die beste Lösung! Für Karl Kummer mag es eine herbe Enttäuschung gewesen sein, aber er hat mir gegenüber nie etwas merken lassen.“[6]
Die Medien stürzten sich auf die zierliche Frau, die geduldig ein Interview nach dem anderen gab. „Es ist wichtig und richtig, wenn Frauen auch in höchste Positionen vordringen. Dies entspricht nicht nur der Bevölkerungs- und Beschäftigungsstruktur, sondern auch der Wählerstruktur“, sagte sie an ihrem ersten Amtstag zur Neuen illustrierten Wochenschau im Mai 1966.

„In ihrer Amtsperiode setzte sie Meilensteine für die Arbeitnehmer: Arbeitsmarktförderungsgesetz, Hausbesorgergesetz, die Weiterführung der Kodifikation des Arbeitsrechts und die Einführung eines neuen Feiertages, des 8. Dezembers, sind alleinige Entscheidungen ihrer Persönlichkeit.“[7] Unter Rehor stieg das Sozialbudget von 1965 bis 1970 um 66 %, und die reale Erhöhung der Pensionen betrug 22 %, ein Ausmaß, das seither nicht mehr erreicht wurde; ebenso setzte sie ein neues Lebensmittelgesetz durch.[8] Insgesamt wurden mehr als hundert Sozialgesetze während ihrer Amtszeit verabschiedet, dies brachte ihr den durchaus wohlwollenden Spitznamen „schwarze Kommunistin“ beim Volk ein.

Nach der Wahlniederlage der ÖVP im Jahre 1970 fand ihre Karriere als Sozialministerin ein jähes Ende. Sie verließ das politische Feld, blieb jedoch nicht untätig. „Bis ins hohe Alter setzte sie sich im sozialen Bereich ein. Sie war Vizepräsidentin der ARGE – Dachorganisation für 61 Behindertenverbände und Obfrau der Jugendfreunde sowie in der Liga für Menschenrechte.“

Am 28. Januar 1987 starb die auch von politischen Gegnern sehr geschätzte Politikerin im Alter von 76 Jahren in Wien. Zur Erinnerung an ihre Person beschloss der Wiener Gemeinderat einstimmig, den Park am Schmerlingplatz in Grete-Rehor-Park umzubenennen. Zudem besteht der Grete-Rehor-Hilfsfonds, der sich um behinderte Jugendliche kümmert.

Josef Klaus |
Fritz Bock /
Hermann Withalm

Carl Heinz Bobleter |
Hans Bürkle |
Karl Gruber |
Johann Haider |
Franz Hetzenauer |
Hans Klecatsky |
Stephan Koren |
Vinzenz Kotzina |
Roland Minkowitsch |
Otto Mitterer |
Alois Mock |
Heinrich Neisser |
Theodor Piffl-Perčević |
Karl Pisa |
Georg Prader |
Grete Rehor |
Karl Schleinzer |
Wolfgang Schmitz |
Franz Soronics |
Josef Taus |
Lujo Tončić-Sorinj |
Kurt Waldheim |
Ludwig Weiß

Erste Republik:Hanusch |
Heinl |
Resch |
Pauer |
Schmitz |
Resch |
Innitzer |
Schmitz |
Resch |
Ender |
Resch |
Kerber |
Schmitz |
Neustädter-Stürmer |
Dobretsberger |
Resch |
Jury

Zweite Republik:Böhm |
Maisel |
Proksch |
Rehor |
Häuser |
Weißenberg |
Salcher |
Dallinger |
Lacina |
Geppert |
Hesoun |
Hums |
Hostasch |
Sickl |
Haupt |
Haubner |
Buchinger |
Hundstorfer |
Stöger |
Hartinger-Klein
