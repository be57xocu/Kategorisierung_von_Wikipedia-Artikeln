George Stevens (* 18. Dezember 1904 in Oakland; † 8. März 1975 in Lancaster, Kalifornien) war ein US-amerikanischer Filmregisseur, Drehbuchautor, Filmproduzent und Kameramann. Zu seinen bekanntesten Filmen gehören Ein Platz an der Sonne, Mein großer Freund Shane, Giganten und Das Tagebuch der Anne Frank. Für Ein Platz an der Sonne und Giganten gewann er jeweils den Oscar in der Kategorie Beste Regie.

George Stevens wurde als Sohn zweier Bühnenschauspieler geboren und gab sein eigenes Debüt im Alter von fünf Jahren. Bereits 1921 begann er als Kameraassistent in Hollywood seine Karriere. 1927 wechselte er zu Hal Roach und arbeitete an zahlreichen Streifen des beliebten Komikerduos Stan Laurel und Oliver Hardy. Ab 1930 wechselte Stevens hinter die Kamera als Regisseur vieler Kurzfilme. Auch nach seinem Wechsel zu Universal und später zu RKO blieb er zunächst auf die Arbeit an unambitionierten Streifen beschränkt. Seinen Durchbruch als ernstzunehmender Regisseur hatte er 1935, als er Katharine Hepburn ihren größten Erfolg seit Vier Schwestern gab: die stilvolle und elegant inszenierte Adaption von Booth Tarkingtons Alice Adams schildert die Erlebnisse einer jungen Frau, die alles versucht, um aus ihren bescheidenen Verhältnissen aufzusteigen. Hepburn wurde für ihre Darstellung mit einer Oscarnominierung belohnt und Stevens wurde im Folgejahr mit der Aufgabe betraut, RKOs wertvollste Stars zu betreuen. Mit Swing Time gab er Fred Astaire und Ginger Rogers nach Meinung vieler Kritiker ihr bestes Vehikel überhaupt. Der Film war ein uneingeschränkter künstlerischer und finanzieller Erfolg. 1939 vermochte er mit Gunga Din einen der besten Abenteuerfilme der Zeit zu realisieren und der Streifen machte über $ 1.5 Mio. an Profit. Mit dem Film Penny Serenade begann 1941 seine künstlerisch erfolgreichste Zeit. Der Film, der das beliebte Leinwandpaar Cary Grant und Irene Dunne wieder vereinte, schilderte die dramatischen Erlebnisse zweier junger Eheleute, die am Tod des einzigen Kindes fast zerbrechen. Kurz danach drehte er für Columbia zwei erfolgreiche Komödien mit Jean Arthur. The Talk of the Town war eine ambitionierte Geschichte um Kleinstadtvorurteile und bürgerliche Freiheiten, die neben Arthur noch Cary Grant und Ronald Colman vor die Kamera brachte. Im Folgejahr hatte Stevens mit Immer mehr, immer fröhlicher seinen bis dahin größten finanziellen Erfolg. Die Komödie spielt im völlig übervölkerten Washington der Kriegstage. Jean Arthur spielt eine junge Regierungsangestellte, die eine Hälfte ihres Hauses an einen freundlichen Herren (Charles Coburn) vermietet, der seine Hälfte wiederum an Joel McCrea untervermietet, ohne der Hausherrin darüber Bescheid zu geben. Die humorvollen Ereignisse schildern den Weg von Arthur und McCrea bis zum Happy-End. 

Kurze Zeit später ging Stevens mit dem Rang eines Major zu einer Aufklärungseinheit und drehte unter anderem Filme über das befreite Deutschland. Er wurde 1945 im Rang eines Lieutenant-Colonel aus der Armee entlassen und begann nahtlos an die bisherigen Erfolge anzuknüpfen. Mit I Remember Mama gab er Irene Dunne 1948 den letzten Erfolg ihrer Karriere. Die Neuverfilmung von A Place in the Sun von Theodore Dreiser, der 1951 nach einer über 18-monatigen Nachbearbeitung im Studio in den Verleih kam, wurde von den Kritikern zwar als Verbesserung gegenüber der inadäquaten Verfilmung durch Josef von Sternberg angesehen. Im Gedächtnis blieben jedoch die sinnlichen Nahaufnahmen von Elizabeth Taylor und Montgomery Clift. Auch die nächsten Filme, darunter Mein großer Freund Shane und Giganten wurden Erfolge an der Kinokasse. Nach der intelligent in Szene gesetzten Adaption von The Diary of Anne Frank 1959 drehte Stevens nur noch zwei kommerziell weniger erfolgreiche Filme.

Stevens war zweimal verheiratet. Sein einziges Kind, George Stevens Jr., ist ein erfolgreicher Film- und Theaterproduzent sowie Mitgründer des American Film Institute.

George Stevens war ein ausgesprochener Perfektionist, der jede Szene sorgfältig plante und die Schauspieler teilweise mit endlosen Wiederholungen der Einstellungen in die Erschöpfung trieb. Vor dem Dreh wurde jede mögliche Kameraeinstellung und jeder denkbare Beleuchtungswinkel durchgespielt, um die bestmöglichen Ergebnisse zu bekommen.

Douglas Fairbanks |
William C. de Mille |
Mike C. Levee |
Conrad Nagel |
Theodore Reed |
Frank Lloyd |
Frank Capra |
Walter Wanger |
Bette Davis |
Walter Wanger |
Jean Hersholt |
Charles Brackett |
George Seaton |
George Stevens |
B. B. Kahane |
Valentine Davies |
Wendell Corey |
Arthur Freed |
Gregory Peck |
Daniel Taradash |
Walter Mirisch |
Howard W. Koch |
Fay Kanin |
Gene Allen |
Robert Wise |
Richard Kahn |
Karl Malden |
Robert Rehme |
Arthur Hiller |
Robert Rehme |
Frank Pierson |
Sid Ganis |
Tom Sherak |
Hawk Koch |
Cheryl Boone Isaacs

King Vidor |
Frank Capra |
George Stevens |
Mark Sandrich |
John Cromwell |
George Marshall |
Joseph L. Mankiewicz |
George Sidney |
Frank Capra |
George Sidney |
Delbert Mann |
Robert Wise |
Robert Aldrich |
George Schaefer |
Jud Taylor |
Gilbert Cates |
Franklin J. Schaffner |
Gene Reynolds |
Jack Shea |
Martha Coolidge |
Michael Apted |
Taylor Hackford
