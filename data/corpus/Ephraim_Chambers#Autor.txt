Ephraim Chambers (* um 1680 in Kendal; † 15. Mai 1740 in Islington (heute London)) war ein englischer Schriftsteller der Frühaufklärung. Er war der Herausgeber und größtenteils auch Verfasser eines der ersten enzyklopädischen Wörterbücher der Künste und Wissenschaften im Zeitalter der Aufklärung.  Nach diesem Aufklärer ist das populäre englische Wörterbuch Chambers Dictionary benannt.

Chambers wurde in Kendal, Westmorland geboren und besuchte die Heversham Grammar School. Er absolvierte von 1714 bis 1721 eine Lehre zum Globus-Macher bei John Senex (1678–1740) einem Kartographen und Graveur aus London.
Chambers fasste schon als Handwerkslehrling den Plan zu seiner Cyclopaedia, or an universal dictionary of arts and sciences, die zuerst zu London in 2 Bänden erschien und Geografie und Geschichte ausschloss. Die Cyclopaedia basiert auf John Harris' Lexicon technicum von 1704 und war ihrerseits Vorbild und erst Grundlage für die von Diderot und d'Alembert geschaffene Encyclopédie. Er veröffentlichte sie 1728; sie gilt als eine der ersten englischsprachigen Enzyklopädien.

Nach Beginn der Cyclopaedia verließ er John Senex und widmete sich ganz dem Enzyklopädie-Projekt. Er nahm Unterkunft in Gray’s Inn, wo er für den Rest seines Lebens blieb.

Chambers fand Anerkennung, man ernannte ihn zum Mitglied der Royal Society of London, und er erlebte noch drei Auflagen seines Buches.

Außerdem hatte er an dem Litterary Magazine teil und an der abgekürzten Übersetzung der Memoiren der Akademie der Wissenschaften zu Paris: Philosophical history and memoirs of the Royal Academy of Sciences at Paris (1742, 5 Bde.).

Chambers starb 1740 im Canonbury House bei Islington. Er wurde im Nordkloster der Westminster Abbey begraben.[1]
In Anbetracht der Schwierigkeiten, welche Chambers damals bei der alphabetischen Zusammenstellung aller Gegenstände des menschlichen Wissens zu überwinden hatte, ist sein Verdienst nicht gering anzuschlagen. (Meyers)
