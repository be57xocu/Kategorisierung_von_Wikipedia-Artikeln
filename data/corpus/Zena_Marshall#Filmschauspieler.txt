Zena Moyra Marshall (* 1. Januar 1925 in Nairobi, Kenia; † 10. Juli 2009 in London) war eine britische Schauspielerin.

Sie besuchte das katholische Internat St Mary's School Ascot und wurde an der Royal Academy of Dramatic Art (RADA) ausgebildet. Mit der Ensa (Entertainments National Service Association) arbeitete sie während des Zweiten Weltkrieges.[1]
Zena Marshalls Filmkarriere reicht bis 1945 zurück, als sie eine kleine Rolle in Caesar und Cleopatra an der Seite von Claude Rains und Vivien Leigh hatte. Ihr exotisches Aussehen führte oft zu Auftritten in ethnischen Rollen, zum Beispiel als Italienerin oder Asiatin. Ihren bekanntesten Auftritt hatte sie 1962 an der Seite von Sean Connery als Miss Taro im ersten James- Bond- Film James Bond jagt Dr. No.
