Maria Holst (* 2. April 1917[1] in Wien; † 8. Oktober 1980 in Salzburg) war eine österreichische Theater- und Filmschauspielerin.

Sie besuchte die Theaterschule in Prag und das Max-Reinhardt-Seminar in Wien. 1935 debütierte sie am Landestheater Linz und wechselte ein Jahr später an das Theater an der Wien. 1937 spielte sie am Stadttheater in Brünn, ab 1938 gehörte sie zum Ensemble des Wiener Burgtheaters. In dieser Zeit wurde sie eine bedeutende Theaterschauspielerin. Sie war die Gloria in George Bernard Shaws Man kann nie wissen (1939), Wlasta in Franz Grillparzers Libussa (1941), Elisabeth in Friedrich Schillers Don Carlos (1942), Prothoe in Heinrich von Kleists Penthesilea (1943) und Portia in Shakespeares Der Kaufmann von Venedig (1943).

Bereits mit 19 Jahren spielte sie in ersten Filmen, z. B. in Lumpacivagabundus mit Heinz Rühmann die Amorosa. Berühmt wurde sie aber erst 1940, als sie unter der Regie von Willi Forst in dem Historienfilm Operette die Sängerin Marie Geistinger darstellte. Einen ähnlichen Erfolg feierte sie im Jahr darauf, als sie, wieder unter Forsts Regie, im Mittelpunkt des Operettenfilms Wiener Blut stand.

In den 50er Jahren erhielt sie wichtige Nebenrollen in mehreren zeittypischen Produktionen dieser Jahre wie Grün ist die Heide und Die Trapp-Familie.

Maria Holst heiratete 1944 den Maler und Graphiker Eugen Graf Ledebur. Nach der Scheidung war sie mit dem Berliner Arzt Rudolf Röttger (1919–1976) verheiratet. Aus dieser Ehe ging die Tochter Elisabeth (* 1957) hervor.

1980 erstickte sie tragischerweise beim Essen. Sie wurde in Berlin auf dem Friedhof Heerstraße im Berliner Ortsteil Westend beigesetzt.
