Elsa Beskow, geborene Elsa Maartman, (* 11. Februar 1874 in Stockholm; † 30. Juni 1953 in Djursholm) war eine schwedische Kinderbuchautorin, Malerin und Illustratorin.

Elsa Beskow wurde als Tochter des Geschäftsmannes Bernt Maartman (1841–1889) und der Lehrerin Augusta Fahlstedt (1850–1915) geboren. Sie hatte fünf jüngere Geschwister. Als der Vater Konkurs anmelden musste und kurz danach starb (1889), zog die Mutter mit ihren Kindern in den Haushalt ihrer Schwestern. Diese leiteten eine Vorschule im Sinne der Reformpädagogik. Die Tanten standen später Porträt für Elsa Beskows Buch Tante Braun, Tante Grün und Tante Lila.

Elsa Beskow studierte von 1890 bis 1895 Zeichnen an der Konstfack in Stockholm, dort lernte sie Natanael Beskow kennen, den sie 1897 heiratete. Das Paar bekam sechs Söhne, darunter Bo Beskow. 

Ab 1894 publizierte Elsa Beskow in der Kinderzeitschrift „Jultomten“, die damals eine sehr hohe Auflage erreichte und wesentlich zu einem beginnenden Bekanntheitsgrad der Arbeiten Elsa Beskows beitrug. 1897 veröffentlichte sie ihr erstes Buch Sagan om den lilla, lilla gumman / Das Märchen von der kleinen, kleinen Frau; mehr als 30 Bücher sollten folgen, die in zahlreiche Sprachen übersetzt wurden.

Elsa Beskows Stil war den Werken von Hans Christian Andersen und Zacharias Topelius verbunden. 
Ihre zahlreichen Kinderbücher hat sie selbst illustriert. Daher wird sie als erste schwedische Bilderbuchautorin angesehen. Alle Bücher sind auf phantasievolle Weise kindgerecht, aber wenig realistisch. Viele von ihnen zählen zur klassischen schwedischen Kinderliteratur und werden auch weiterhin gelesen. Die Namen einiger Figuren sind in Schweden überall bekannt und als Begriffe in den schwedischen Wortschatz eingeflossen. Beispiele dafür sind: „Tant Grön, Tant Brun och Tant Gredelin“ (Tante Grün, Tante Braun und Tante Lila).

Viele Bücher sind in Gedichtform abgefasst, und bei einigen sollen die Kinder selbst den passenden „Endreim“ der zweiten Zeile finden, wie z.B. in „Hattstugan“ (Huthäuschen). Solche Wortspiele lassen sich natürlich schlecht in eine andere Sprache transferieren. Andere Bücher sind aber auch in Deutschland bekannt, z.B. „Puttes äventyr i blåbärsskogen“ (Hänschen im Blaubeerwald) das 1903 in deutscher Übersetzung publiziert wurde. In vielen Büchern kommen kleine Wichtelmännchen bzw. Wichtelkinder und andere Sagenwesen vor.
