Johannes Buridan, oder auch Jean Buridan (geboren um 1300 in Béthune, Grafschaft Artois; gestorben kurz nach 1358), war ein scholastischer Philosoph, Physiker und Logiker.

Buridan gehörte zum Pariser Ockhamistenkreis. Er war Professor an der Pariser Universität und leitete von 1325 bis 1348 das Rektorat. Er gilt als der bedeutendste unter Ockhams unmittelbaren Schülern und gehörte ebenso wie sein Lehrer dem Nominalismus an.

Als Physiker lieferte er mit seiner Impetustheorie einen Beitrag zur Weiterentwicklung der Dynamik.

Buridan analysierte ausführlich die unmittelbaren modalen Schlüsse und konstruierte eine entsprechende mnemotechnische Figur.
Er entwickelte auch eine Theorie der Eliminierung semantischer Antinomien.

Bekannt ist er heute noch durch den Ausdruck Buridans Esel: Ein Esel steht genau in der Mitte zwischen zwei völlig gleichartigen und gleich weit entfernten Heuhaufen. Er verhungert, da es bei gleichen Motiven keinen vernünftigen Grund gibt, sich für einen der beiden Heuhaufen zu entscheiden. Dieses Bild ist allerdings nicht in seinen Schriften zu finden. In der Literatur wird die Ansicht vertreten, dass dieses Gleichnis fälschlicherweise Buridan zugeordnet wird, da bereits Aristoteles und Dante eine ähnliche Situation beschreiben.

Allerdings drückt es einen zentralen Aspekt der Auffassungen Buridans aus: Er reduziert die Freiheit auf die Wahl zwischen mehreren Möglichkeiten (libertas oppositionis).

Im weiteren Sinne drückt dieses Gleichnis Buridans Ansichten über die Wechselbeziehungen von Wille und Verstand aus. Wenn der Verstand zu dem Schluss kommt, dass er gleichwertige Möglichkeiten vor sich hat, verliert der Wille seine Wirkung.

Der Name Buridans ist mit zwei historisch widerlegten Legenden verknüpft. Die eine behauptet, er habe eine Affäre mit Königin Johanna (1326–1360), zuerst Gemahlin Philipps des Schönen von Burgund (1323–1346) – seit 1350 in zweiter Ehe Gemahlin Johanns II. von Frankreich (1319–1364), gehabt. Er habe deswegen nach Wien flüchten müssen und sei an der Gründung der dortigen Universität beteiligt gewesen. Allerdings war diese schon 1237 von Friedrich II. gegründet worden.[1]
Die andere Legende, ebenfalls eine unglücklich endende Affäre mit einer Königin, diesmal der Königin Margarete von Burgund (1292–1315), ist durch François Villons Ballade des dames du temps jadis (Ballade von den edeln Frauen vergangener Zeiten) in seinem Grant Testament von 1489 bekannt geworden. Dort heißt es in der zweiten Strophe: »Semblablement ou est la royne / Qui commanda que Buridan / Fust geté en ung sac en Saine? / Mais ou sont les neiges d'antan?« (Ebenso, wo ist die Königin, die befahl, Buridan in einem Sack in die Seine zu werfen? Aber wo ist der Schnee vom vergangenen Jahr?) Allerdings war Buridan noch ein Kind, als Margarete 1315 starb.[2]