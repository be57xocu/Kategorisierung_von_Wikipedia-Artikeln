Evariste François René Mertens (* 1846 in Breda; † 1907 in Zürich) war ein Schweizer Gartenarchitekt belgischer Herkunft.

Der 1846 geborene Evariste Mertens stammte aus Belgien. Nach Abschluss der Studien am Institut horticole in Gent und gärtnerischer Praxis in England und Frankreich kam er im Alter von 24 Jahren in die Schweiz, wo er mit seinem Studienkollegen Arnold Neher 1870 eine Gartenbaufirma in Schaffhausen gründete.

Mit dem Sohn von Leopold Karl Theodor Fröbel, dem Gartenbauarchitekten Otto Karl Fröbel, war er an der Ausführung der Zürcher Quaianlagen maßgeblich beteiligt. 1889 gründete er eine eigene Gartenbaufirma in Zürich, die später von seinen Söhnen, den Gebrüdern Walter und Oskar Mertens, weitergeführt wurde. Neben öffentlichen Anlagen entwarf Mertens auch zahlreiche Privatgärten.
