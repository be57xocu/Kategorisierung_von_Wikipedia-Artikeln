Bernhard Villinger (* 13. Dezember 1889 in Mannheim; † 12. Februar 1967 in Freiburg im Breisgau) war ein deutscher Arzt, ärztlicher Standespolitiker, Sportler, Filmpionier und Forscher.

Bernhard Villinger wurde 1889 in Mannheim geboren. Er absolvierte bis 1909 den Militärdienst in München als Einjährig-Freiwilliger beim bayerischen 2. Infanterie-Regiment Kronprinz. Von 1910 bis 1914 studierte er in Freiburg Medizin. Im Jahr 1919 wurde er an der Universität Freiburg zum Dr. med. promoviert.

In den Jahren 1904 bis 1914 war Villinger mehrfach erfolgreich im norwegischen Holmenkollenrennen bei Oslo.
Villinger schloss sich im Sommer 1913 zusammen mit Sepp Allgeier, Rudolf  Biehler und Gerhard Graetz der von Theodor Lerner geleiteten Rettungsexpedition an. In zwei jeweils 60 Tage dauernden Suchaktionen sollte der bei Spitzbergen verschollenen Expedition des Polarforschers Herbert Schröder-Stranz Hilfe gebracht werden, vergeblich. Der einzige Lichtblick war die filmische Ausbeute. Am 24. Dezember 1913 hatte der Film Mit der Kamera im ewigen Eis (Dokumentation über Lerners Hilfsexpedition) in Freiburg Premiere.

Nach dem Ersten Weltkrieg war er Darsteller in Arnold Fancks Film Das Wunder des Schneeschuhs (1919/1920), bei dem sein Freund Sepp Allgeier die Kamera führte. 

Am 8. Mai 1920 gründete er, vom Film und von den Bergen gleichermaßen begeistert, zusammen mit Arnold Fanck, Odo Deodatus Tauern und Rolf Bauer in Freiburg im Breisgau die Berg- und Sportfilm GmbH. Von März bis Oktober 1926 leitete Villinger die Grönland-Expedition der UFA. Dabei entstand der Film Milak, der Grönlandjäger mit Helmer Hanssen in der Hauptrolle.

Unter Leitung von Hubert Wilkins war Villinger 1931 als Arzt und Wissenschaftler Mitglied der gescheiterten Nautilus-Expedition zum Nordpol. 

Ab 1933 war Villinger in Freiburg als niedergelassener Arzt tätig. Von 1936 bis 1944 war er Präsident des Freiburger FC.
In dieser Zeit wurde Villinger NSDAP-Mitglied.

Von 1956 bis 1963 war er Präsident der Landesärztekammer Baden-Württemberg.

1959 wurde er mit dem Großen Bundesverdienstkreuz ausgezeichnet. Der Deutsche Ärztetag ehrte 1965 Villinger mit der Paracelsus-Medaille.
