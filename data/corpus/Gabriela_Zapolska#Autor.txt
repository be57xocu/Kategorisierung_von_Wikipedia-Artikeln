Gabriela Zapolska, auch Gabryela Zapolska (* 30. März 1857 in Podhajce bei Łuck (heute Ukraine); † 17. Dezember 1921 in Lemberg), eigentlich Maria Korwin-Piotrowska, war eine polnische Schriftstellerin.

Zapolska war die Tochter eines wohlhabenden Gutsbesitzers, die sich, gegen den Willen ihrer Eltern, 1879 einer wandernden Schauspielertruppe anschloss. Sie trat in vielen polnischen Städten auf und gastierte zwischen 1890 und 1895 in Paris. 1902 gründete sie eine Schauspielschule in Krakau, die sie auch einige Jahre leitete. Außerdem machte sie sich einen Namen als Theaterkritikerin.

Als führende Vertreterin des polnischen Naturalismus war sie auch von Émile Zola beeinflusst und kritisierte besonders in ihren Romanen die Stellung der Frau in der Gesellschaft. Einen Teil ihres Werkes verfasste sie unter den Pseudonymen Józef Maskoff und Walery Tomicki. Besondere Bedeutung erlangte sie durch ihre Theaterstücke, ihr wichtigstes, die Tragikomödie Moralność pani Dulskiej (Die Moral der Frau Dulski) von 1906 wird auch heute häufig gespielt.

Eine neunbändige Ausgabe ihrer Romane erschien 1924 im Berliner Verlag Oesterheld & Co., was von ihrer enormen Popularität auch in Deutschland zeugt.
