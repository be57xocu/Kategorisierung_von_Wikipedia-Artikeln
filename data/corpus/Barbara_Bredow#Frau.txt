Barbara Bredow (* 18. April 1937 in Karlsruhe) ist eine deutsche bildende Künstlerin.

Sie studierte von 1956 bis 1959 bei Oskar Holweck und Boris Kleint am Hochschulinstitut für Kunst- und Werkerziehung der Werkkunstschule Saarbrücken,[1] zugleich an der Universität des Saarlandes und anschließend an der Akademie der Bildenden Künste München. In den Jahren 1962 bis 1964 absolvierte sie ein Referendariat als Kunsterzieherin in Mannheim. Seit 1968 lebt und arbeitet sie in Darmstadt.

Barbara Bredow ist Mitglied in der Darmstädter Sezession, im Rhein-Neckar-Bund Mannheim und im Hessischen Werkbund.
