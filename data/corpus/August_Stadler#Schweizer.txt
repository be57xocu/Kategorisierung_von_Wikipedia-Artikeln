August Stadler (* 24. August 1850 in Zürich; † 16. Mai 1910 ebenda) war ein Schweizer Philosoph und Professor für Philosophie am Polytechnikum Zürich.

August Stadler entstammte einer alteingesessenen Zürcher Familie. Er begann seine Studien am Polytechnikum seiner Vaterstadt unter Gottfried Semper und Friedrich Albert Lange, wechselte dann nach Berlin, wo er Schüler von Hermann von Helmholtz wurde. Nach weiteren Studien in England übernahm er eine Dozentur am Polytechnikum, wo er sich 1877 habilitieren konnte. 1892 wurde er in Zürich als Nachfolger Ludwig Steins Professor für Philosophie und Pädagogik. Durch seine Publikationen hat er sich einen Namen als profunder Kenner der kantschen Philosophie gemacht.

August Stadler starb an den Folgen einer Lungenentzündung im Alter von 59 Jahren.
