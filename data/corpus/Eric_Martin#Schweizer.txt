Eric Martin (* 13. August 1900 in Cologny; † 6. Januar 1980 in Genf) war ein Schweizer Arzt und Wissenschaftler. Von 1936 bis 1970 war er Direktor der Poliklinik der Universität Genf. Darüber hinaus wirkte er zeitweise als Rektor der Universität und Dekan der Medizinischen Fakultät. Bereits während seiner Tätigkeit an der Hochschule begann er sich im Schweizerischen Roten Kreuz (SRK) zu engagieren. Von 1973 bis 1976 war er Präsident des Internationalen Komitees vom Roten Kreuz (IKRK).

Eric Martin wurde 1900 in Cologny im Kanton Genf als Sohn eines Notars geboren. Er besuchte die Schule in Genf und studierte anschließend Medizin in Genf, Straßburg, Paris sowie Wien und schloss das Studium mit der Promotion zum Dr. med. ab. Im Jahr 1929 ließ er sich mit einer eigenen Praxis in Genf nieder, entschied sich dann jedoch für eine Tätigkeit als Wissenschaftler.

Er wurde Professor an der Medizinischen Fakultät der Universität Genf und wirkte dort als Internist. Während seiner wissenschaftlichen Laufbahn veröffentlichte er rund 300 Publikationen zum Diabetes mellitus, zum Rheumatismus, zur Geriatrie und zu sozialmedizinischen Themen. Zwischen 1936 und 1970 war er Leiter der Poliklinik der Universität Genf. Von 1956 bis 1958 und von 1965 bis 1966 war er darüber hinaus als Dekan der Medizinischen Fakultät tätig. In den Jahren von 1960 bis 1962 wirkte er als Rektor der Universität.

Neben seiner Tätigkeit an der Universität war er auch im Schweizerischen Roten Kreuz aktiv, unter anderem als Präsident der Genfer Sektion und später als Mitglied im Zentralkomitee des SRK. Für das SRK nahm er im Jahr 1948 an der Internationalen Rotkreuz-Konferenz in Stockholm teil.

Eric Martin war verheiratet mit Gisèle Martin, geb. Morsier. Er starb 1980 in Genf.

Im Juli 1973 wurde Eric Martin Nachfolger von Marcel Naville als Präsident des Internationalen Komitees vom Roten Kreuz. Die Versammlung des Komitees traf bei der Wahl von Navilles Nachfolger eine ungewöhnliche Entscheidung, indem sie Eric Martin zum Präsidenten wählte, jedoch gleichzeitig Roger Gallopin zum Präsidenten des Versammlungsrates ernannte. In der auf diese Weise entstandenen De-facto-Doppelpräsidentschaft kamen Eric Martin eher repräsentative Aufgaben zu, während Roger Gallopin die Entscheidungsbefugnis für die meisten Bereiche wahrnahm.

Schwerpunkte der Präsidentschaft von Martin waren die Friedensarbeit des Roten Kreuzes und der Einsatz gegen die Folter. In seine Amtszeit fiel die Veröffentlichung des nach dem kanadischen Experten für Entwicklungshilfe Donald Tansley benannten Tansley Reports im Jahr 1975. In diesem durch eine internationale Gruppe von unabhängigen Gutachtern unter Leitung Tansleys erstellten Bericht wurde die Tätigkeit des IKRK und der Liga der Rotkreuz-Gesellschaften kritisch untersucht. Er enthielt Empfehlungen für die zukünftige Ausgestaltung der Zusammenarbeit zwischen beiden Organisationen sowie zwischen dem IKRK und den nationalen Rotkreuz- und Rothalbmond-Gesellschaften. Eric Martin nannte den Bericht eine „schonungslose Aufdeckung“ („pitiless inquisition“). Viele der Vorschläge des Tansley Reports wurden, wenn auch teilweise mit erheblicher Verzögerung, in den folgenden Jahren vom IKRK umgesetzt.

Eric Martin trat 1976 aus Altersgründen vom Amt des IKRK-Präsidenten zurück. Neben der Amtszeit seines Vorgängers Marcel Naville gilt seine Präsidentschaft in der Geschichte des Komitees als eine Zeit ohne herausragende Ereignisse oder Erfolge, sein Wirken blieb weitestgehend ohne bleibenden Einfluss für die weiteren Aktivitäten des Komitees. Zu seinem Nachfolger wurde Alexandre Hay gewählt.

Guillaume-Henri Dufour (1863–1864) |
Gustave Moynier (1864–1910) |
Gustave Ador (1910–1928) |
Max Huber (1928–1944) |
Carl Jacob Burckhardt (1945–1948) |
Paul Ruegger (1948–1955) |
Léopold Boissier (1955–1964) |
Samuel Gonard (1964–1969) |
Marcel Naville (1969–1973) |
Eric Martin (1973–1976) |
Alexandre Hay (1976–1987) |
Cornelio Sommaruga (1987–1999) |
Jakob Kellenberger (2000–2012) |
Peter Maurer (seit 2012)
