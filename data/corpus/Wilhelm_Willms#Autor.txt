Wilhelm Willms (* 4. November 1930 in Rurdorf an der Rur, heute Linnich; † 25. Dezember 2002 in Heinsberg) war ein deutscher Priester und Verfasser geistlicher Lieder und Lyrik.

Nach dem Krieg wuchs Willms in Düsseldorf-Oberkassel auf. Nach einigen Semestern Studium der Kunstgeschichte wechselte er zur Theologie (Bonn und München) und 1957 wurde er in Aachen zum Priester geweiht. Er wirkte zunächst als Kaplan in Viersen St. Peter, dann in Aachen und in St. Dionysius in Krefeld, dann Propst in St. Gangolf in Heinsberg.

1972 gewann er beim Wettbewerb „Kiel oben – Kiel unten“ zum Neuen Geistlichen Lied anlässlich der Segelolympiade 1972 in Kiel den ersten, zweiten und dritten Preis für seine Liedtexte (vertont von Hans-Jörg Böckeler). 1983 brachte er zusammen mit Pater Stephan Reimund Senge von der Abtei Himmerod den Psalmenband Spiel-Räume zum Lobpreis heraus.

1985 textet Wilhelm Willms ein Musical über das Leben des Gründers der Schönstatt-Bewegung und Entwickler der Kentenich-Pädagogik P. Josef Kentenich - Wagnis und Liebe ist ein Musical des Komponisten Ludger Edelkötter sowie des Texters Wilhelm Willms.

Von 1980 bis 1990 wirkte er an der Theresienkirche in Aachen (Personalgemeinde). Seit einem Schlaganfall während einer Messe am ersten Adventssonntag 1990 war er für freie Mitarbeit in seiner Diözese (unter anderem im Bildungswerk Aachen) freigestellt. Sein Wahlspruch lautete „Geht hin zu allen geschöpfen predigend die frohe botschaft... denn die schöpfung liegt in wehen harrend auf erlösung, auf befreiung“.

Willms schrieb zahlreiche Bücher und Texte Neuer geistlicher Lieder (über 200). Er war Mitglied der Oekumenischen Textautoren- und Komponistengruppe der Werkgemeinschaft Musik e.V. und der AG Musik in der Ev. Jugend e.V., heute Textautoren- und Komponistengruppe TAKT.
