Marie-Joseph Blaise de Chénier, (* 11. Februar 1764 in Konstantinopel; † 10. Januar 1811 in Paris), war ein französischer Schriftsteller. Er gilt als bedeutendster Dramatiker der Französischen Revolution, ist heute aber so gut wie vergessen.

Marie-Joseph Chénier (wie er in den Literaturgeschichten meistens heißt) war der knapp anderthalb Jahre jüngere Bruder des heute bekannteren und als bedeutender betrachteten Autors André Chénier. Er wurde geboren in Konstantinopel (heute Istanbul) als Sohn eines dort zum Geschäftsmann gewordenen französischen Adeligen und einer Angehörigen der damals noch starken griechischen Minderheit der Stadt. 1765 ging die Familie nach Frankreich, zunächst nach Paris. Während der Vater wenig später den Posten des französischen Konsuls in der marokkanischen Hafenstadt Salé antrat, blieb die Mutter mit den beiden älteren Kindern in Paris. Die beiden jüngeren, Marie-Joseph und André, wurden einem Onkel in Carcassonne anvertraut. 1773 kamen beide ebenfalls nach Paris, wo sie das Collège de Navarre besuchten und im Salon ihrer geistig interessierten Mutter Literaten, Künstler und Gelehrte kennenlernten.

1781, also eben fünfzehnjährig, begann Chénier eine militärische Karriere als Kadett in einem Dragoner-Regiment in Niort. Zwei Jahre später gab er sie jedoch auf, ging nach Paris zu seiner Familie zurück und folgte seinen literarischen Neigungen, ganz wie sein Bruder André, der im selben Jahr einen noch kürzeren Versuch bei einem Regiment in Straßburg abgebrochen hatte.

Schon 1785 wurde, dank der Vermittlung des mit der Familie befreundeten Dramatikers Palissot, ein erstes Stück von ihm angenommen und aufgeführt, die „heroische Komödie“ Edgar, roi d'Angleterre, ou Le Page supposé. Das Stück fiel aber durch, ebenso 1787 sein zweites, die Voltaire imitierende Tragödie Azémire.

1788 stellte Chénier sein nächstes Stück fertig: die in der berüchtigten Bartholomäusnacht 1572 spielende historische Tragödie Charles IX ou La Saint-Barthélemy (1790 gedruckt und 1797 umgearbeitet zu Charles IX ou L'École des rois). Es wurde zwar von der Comédie Française angenommen, jedoch von der Zensur nicht freigegeben. Denn die Figuren (ein wankelmütiger König, eine ihn manipulierende Königin, ein machtgieriger hochadeliger Höfling, ein skrupellos die Interessen der Kirche verfolgender Kardinal, ein rechtschaffener, aber machtloser Kanzler bürgerlicher Herkunft und ein am Ende ermordeter Philosoph und Reformer) verkörperten gar zu offenkundig politische Akteure im Frankreich der Zeit, d. h. unmittelbar vor Ausbruch der Revolution. Erst Ende 1789, nach langen in der Öffentlichkeit geführten Diskussionen über die Freiheit des Theaters, in die Chénier selbst mit zwei Streitschriften eingriff, und dank einer stark veränderten politischen Situation konnte das Stück aufgeführt werden. Es erlebte nun, auch an Provinztheatern, einen triumphalen Erfolg und wurde zum meistgespielten, allerdings auch heftig angefeindeten französischen Drama der frühen 1790er Jahre. In der Titelrolle begründete der Schauspieler François Talma seinen Ruhm.

Ähnlich wie dem Charles IX erging es Anfang 1789 einem weiteren Stück Chéniers, Henri VIII ou La Tyrannie, das ebenfalls von der Comédie Française angenommen wurde, aber keine Freigabe erhielt. Es kam erst 1791 im neuen pro-revolutionären Théâtre de la République zur Aufführung, das kurz zuvor von Talma und einem Teil seiner Comédie Française-Kollegen gegründet worden war.

Hier wurden denn auch, teils mehr, teils weniger erfolgreich, die weiteren historischen Tragödien uraufgeführt, die Chénier in den nächsten Jahren verfasste. Sie waren sämtlich ebenfalls politisch motiviert und bezogen sich kaum verschlüsselt auf jeweils aktuelle Ereignisse und Entwicklungen: 1792 Jean Calas, ou L’École des juges und Caius Gracchus, 1793 Fénelon ou Les Religieuses de Cambrai und 1794 Timoléon.

Politisch intendiert wie seine Stücke war auch der größte Teil der umfangreichen Gelegenheits- bzw. Gebrauchslyrik, die Chénier in diesen Jahren zu vielerlei Anlässen verfasste, insbes. für die öffentlichen Feiern und Feste, an deren Organisation er mitwirkte. Hierzu gehören z.B. eine Ode sur la mort de Mirabeau (1791), die Strophes qui seront chantées au Champ de la Fédération le 14 juillet 1792, eine Hymne sur la translation du corps de Voltaire, eine Hymne à l'Être suprême (1793), ein Chant des Sections de Paris (1793), eine Hymne à la liberté, pour l'inauguration de son temple dans la commune de Paris (1793), die Hymne du 10 août (1794) usw.

Der bekannteste dieser Texte, die von verschiedenen Komponisten vertont wurden, wurde der Chant du départ (Lied des Aufbruchs/Abschieds, Melodie von Étienne Nicolas Méhul), den Chénier 1793 anlässlich des Ausrückens der Revolutionsarmeen gegen Österreich und Preußen verfasste.

Spätestens 1792 entwickelte sich Chénier auch zum aktiven Politiker. Er wurde Mitglied im „Club“ (einer Art Partei) der Jakobiner und dann Abgeordneter im Nationalkonvent, wo er dem Ausschuss für Volksbildung angehörte. Auf seinen Antrag wurde 1792 die Einrichtung von Primarschulen beschlossen. 1793 war er maßgeblich beteiligt an der Auflösung der königlichen Akademien (u.a. der Académie Française). Naturgemäß gehörte er 1792 zur Mehrheit der Abgeordneten, die das Todesurteil für König Ludwig XVI. bestätigten.

Während der anschließenden Radikalisierung der Revolution im diktatorischen Terrorregime 1793/94 geriet Chénier ins politische Abseits. Sein Stück Timoléon wurde vom Diktator Robespierre als gegen ihn gerichtet erachtet und verboten. Auch hatte er nicht mehr den nötigen Einfluss, um für seinen im März 1794 inhaftierten Bruder André eintreten und dessen Köpfung (25. Juli) verhindern zu können.

Nach dem Sturz Robespierres und der Etablierung des Nachfolgeregimes, des Direktoriums (1795), wurde Chénier zum Mitglied des Rates der Fünfhundert ernannt, einer der beiden Kammern eines neugeschaffenen Pseudoparlaments. Bei der Gründung der Nachfolgeorganisation der aufgelösten ehemaligen Akademien, des Institut de France, gelang es ihm, einen Platz in dessen dritter „Klasse“ (Literatur und schöne Künste) zu erhalten.

Als 1795 der Timoléon wieder aufgenommen wurde, sahen Gegner Chéniers darin das verschlüsselte Eingeständnis einer Schuld am Tod seines Bruders André. Chénier wehrte sich mit den leidenschaftlichen Versen der Épître sur la calomnie (1796), die vielen als sein Meisterwerk gilt.

Unter dem Regime des Konsulats, das 1799 auf das Direktorium folgte, wurde er zum Mitglied im Tribunat berufen, einer Kammer des nächsten neuen Pseudoparlaments. Eine bedeutendere politische Karriere blieb ihm allerdings wiederum versagt, weil er die restaurativen Tendenzen zu bekämpfen versuchte, die unter dem Direktorium eingesetzt und in den Folgejahren an Kraft gewonnen hatten.

So machte er 1801 Front gegen das Wiedererstarken des Katholizismus und attackierte dessen Galionsfiguren Chateaubriand, Jean-François de La Harpe und Madame de Genlis mit den satirischen Schriften Le docteur Pancrace und Les nouveaux saints. 1802 attackierte er mit der Petite épître à Jacques Delille diesen ebenfalls aus der Emigration zurückgekehrten Lyriker, der sich ihm den neuen Herren allzu opportunistisch anzudienen schien.

Trotz seiner wachsenden Distanz zu dem neuen starken Mann Napoleon Bonaparte wurde Chénier 1803 zum Generalinspekteur der „Université“ ernannt, d.h. des unter diesem Namen neu geschaffenen Gesamtsystems des französischen Bildungswesens.

Sein 1804 von Napoleon zu dessen Kaiserkrönung bestelltes und in diesem Rahmen aufgeführtes Drama Cyrus gefiel weder dem Publikum noch dem neuen Kaiser selbst, der Chéniers verdecktes Plädoyer für eine republikanische Verfassung wenig goutierte. Es wurde nur einmal aufgeführt.

Nachdem Chénier sich 1805 in seiner Elegie La Promenade (1805) erneut als Republikaner gezeigt und 1806 in einer Épître à Voltaire Napoleon indirekt vorgeworfen hatte die Ideale der Revolution zu verraten, wurde er seines Amtes als Inspecteur enthoben. Immerhin wurde ihm eine auskömmliche Pension gewährt.

In den Folgejahren schrieb er weitere Stücke, die aber weder aufgeführt noch gedruckt wurden: die Tragödien Philippe II, Œdipe roi und Œdipe à Colone (nach Sophokles), das Drama Nathan le Sage (nach Lessing) und die Komödie Ninon.

Daneben hielt er (1806/07) am Pariser Athénée eine Vorlesungsreihe über die Literatur seiner Zeit, das Tableau historique de l'état et du progrès de la littérature française depuis 1789, in dem er die Ideale der Aufklärung verfocht und die beginnende Romantik als restaurativ kritisierte.

1811 wurde das historische Stück Tibère, wo er in der Figur des römischen Kaisers Tiberius Napoleon kritisiert, sein letztes Werk.

Sein freigewordener Sessel im Institut de France fiel an Chateaubriand, der ihn in seiner Laudatio praktisch unerwähnt ließ.

Da fast alle Texte Chéniers in einem bestimmten weltanschaulichen Sinne zweckbestimmt waren, d.h. die zeitgenössischen Zuschauer/Hörer/Leser gegen die Monarchie und für die Republik einzunehmen versuchten, wurden sie noch zu Lebzeiten des Autors durch den Gang der politischen Entwicklung obsolet. Auch die spätere Literaturgeschichtsschreibung hat Chénier trotz dieses oder jenen Versuchs einer Ehrenrettung nicht den Platz in der Literaturgeschichte gewährt, den er aufgrund seiner großen Bedeutung zu einem gewissen Zeitpunkt verdient.

Theaterstücke

Lyrik und Anderes
