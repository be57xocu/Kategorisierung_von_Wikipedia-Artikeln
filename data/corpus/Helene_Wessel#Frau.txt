Helene Wessel (* 6. Juli 1898 in Hörde (jetzt Dortmund); † 13. Oktober 1969 in Bonn) war eine deutsche Politikerin. Vom 17. Oktober 1949 bis zum Januar 1952 war sie Vorsitzende des Zentrums, danach gründete sie unter anderem mit Gustav Heinemann die Gesamtdeutsche Volkspartei und schloss sich schließlich mit dieser der SPD an. Sie wurde in den Parlamentarischen Rat gewählt und ist damit eine der „Mütter des Grundgesetzes“.

Helene Wessel wurde am 6. Juli 1898 in Hörde als jüngstes von vier Kindern des Reichsbahnbeamten Heinrich Wessel und seiner Ehefrau Helene Wessel, geborene Linz, geboren. Die Eltern waren tief katholisch geprägt, der Vater Mitglied der Deutschen Zentrumspartei. Er starb 1905 an den Folgen eines Arbeitsunfalls.

Helene Wessel besuchte erst die Volks- und Handelsschule, absolvierte dann eine kaufmännische Lehre und nahm im November 1915 eine Stelle als Sekretärin im Hörder Parteibüro des Zentrums an. Dort traf sie auf Johannes Gronowski, einen Bekannten des Vaters, der zu ihrem politischen Ziehvater wurde. Im März 1923 begann sie einen einjährigen Lehrgang an der Staatlichen Wohlfahrtsschule in Münster zur Jugend- und Sozialfürsorgerin, den sie aus eigenen Mitteln – unter anderem durch Auflösung ihrer Briefmarkensammlung – bezahlte. Seit 1919 engagierte sie sich im Zentrum und wurde im Mai 1928 in den Preußischen Landtag gewählt. Damit gab sie ihre beiden Berufe als Parteisekretärin und Fürsorgerin der katholischen Kirche auf. Ab dem Oktober 1929 ließ sie sich an der Berliner Deutsche Akademie für soziale und pädagogische Frauenarbeit zur Diplom-Wohlfahrtspflegerin weiterbilden.

Nach der nationalsozialistischen Machtergreifung 1933 wurde Helene Wessel als „politisch unzuverlässig“ eingestuft. Bei der Abstimmung zum Ermächtigungsgesetz im Landtag hatte sie sich nach eigenen Angaben der Stimme enthalten.[1] Sie arbeitete zunächst in der Verwaltung des St.-Johannes-Hospitals, führte ab 1935 ein Forschungsprojekt zum „Zusammenhalt der katholischen Familie durch die Religion“ durch und war dann kurze Zeit Sekretärin beim Katholischen Frauenbund. Ab April war sie erst Sekretärin, dann Fürsorgerin beim Katholischen Fürsorgeverein. 
Als zunächst ehrenamtliche, dann wieder berufliche Fürsorgerin[2] in der „Gefährdetenfürsorge“ referierte und publizierte sie und setzte sich als Befürworterin der Zwangsverwahrung für ein Bewahrungsgesetz und die Sterilisation von „Asozialen“ ein.[3]
Nach dem Zweiten Weltkrieg betätigte sie sich wieder politisch, zunächst erneut im Zentrum, dann in der Gesamtdeutschen Volkspartei (GVP). Als diese bei der Bundestagswahl 1953 an der Fünf-Prozent-Hürde scheiterte, verlor sie ihr Mandat und arbeitete vorübergehend als freie Mitarbeiterin beim Deutschen Gewerkschaftsbund. Sie trat der Sozialdemokratischen Partei Deutschlands (SPD) bei, für die sie 1957 erneut in den Bundestag gewählt wurde, dem sie dann bis zu ihrem Tod angehörte.

Helene Wessel starb am 13. Oktober 1969 im Alter von 71 Jahren in Bonn[4] und wurde auf dem dortigen Südfriedhof beigesetzt.

Helene Wessel, die seit 1915 für die Deutsche Zentrumspartei arbeitete und ihr seit 1919 angehörte, engagierte sich zunächst im Windthorstbund. Sie wurde 1922 dessen westfälische Landesvorsitzende und gehörte ab 1930 dem Bundesvorstand an. Über diese Tätigkeit erreichte sie auch führende Funktionen im Zentrum. Im Jahr 1924 wurde sie in den Reichsparteiausschuss delegiert, das höchste Gremium zwischen den Parteitagen. Ein Jahr später wurde sie Mitglied im Reichsparteivorstand. Im Mai 1928 erfolgte schließlich die Wahl in den Preußischen Landtag für den Wahlbezirk Westfalen-Süd. Sie war damit die jüngste Abgeordnete ihrer Fraktion und war Fachsprecherin für Fürsorgefragen. Bei den Wahlen im April 1932 und im März 1933 konnte sie ihr Mandat verteidigen. Sie lehnte eine Zusammenarbeit mit den Nationalsozialisten streng ab, befand sich mit dieser Position innerhalb ihrer Fraktion jedoch in der Minderheit. Mit Auflösung des Zentrums und des Landtags endete zunächst Wessels politische Karriere.

Nach dem Ende der nationalsozialistischen Diktatur knüpfte sie an ihre politische Arbeit wieder an. Sie schloss sich erneut dem Zentrum an und wurde auf dem ersten Parteitag im März 1946 dessen stellvertretende Vorsitzende. Im Gegensatz zu vielen ehemaligen Parteifreunden trat sie nicht der neu gegründeten Christlich-Demokratischen Union Deutschlands (CDU) bei, da sie diese nicht in der sozial-fortschrittlichen Tradition des politischen Katholizismus sah und ihrer Meinung nach sich in ihr zu viele reaktionäre Kräfte und Steigbügelhalter Hitlers befanden.
Im September 1946 wurde sie Lizenzträgerin des Neuen Westfälischen Kuriers, weswegen sie eigens an den Verlagsort Werl zog. Bereits im Februar 1946 war sie Mitglied des Zonenbeirats geworden, ab April war sie Abgeordnete im westfälischen Provinziallandtag und ab Oktober auch im ernannten Landtag Nordrhein-Westfalens.[5]
Sie beteiligte sich an Diskussionen um die Erneuerung Deutschlands im Rahmen der Gesellschaft Imshausen. Im September 1948 wurde sie mit Johannes Brockmann als Vertreterin des Zentrums in den Parlamentarischen Rat gewählt, in dem sie als Schriftführerin wirkte. Sie war damit neben Friederike Nadig, Elisabeth Selbert und Helene Weber eine der vier Mütter des Grundgesetzes, dem sie allerdings wegen ihrer Meinung nach mangelnder demokratischer und sozialer Grundrechte die Zustimmung verweigerte.[6]
Nach dem Tod von Fritz Stricker Anfang Juli 1949 übernahm Helene Wessel im Oktober den Bundesvorsitz des Zentrums, den sie bis zu ihrem Parteiaustritt innehatte. Sie war damit die erste weibliche Vorsitzende einer Partei in Deutschland. Bei der Wahl 1949 wurde sie für die Zentrumspartei in den ersten Deutschen Bundestag gewählt. Dort übernahm sie auch  den Fraktionsvorsitz, den sie nach dem Zusammenschluss mit der Bayernpartei zur Föderalistischen Union (FU) zunächst beibehielt. Eine weibliche Fraktionsvorsitzende gab es im Bundestag erst Jahrzehnte später wieder. Außerdem war sie bis zum 13. Februar 1953 Vorsitzende des Ausschusses für Fragen der öffentlichen Fürsorge.

Wessels vehemente Ablehnung der Wiederbewaffnung und ihr Engagement in der Notgemeinschaft für den Frieden in Europa stießen in weiten Teilen der Partei auf Ablehnung. Im Januar 1952 trat sie vom Parteivorsitz zurück, am 12. November dann ganz aus der Partei aus. Gemeinsam mit Gustav Heinemann, Hans Bodensteiner, Thea Arnold, Hermann Etzel, Diether Posser und Johannes Rau gründete sie am 29./30. November die GVP. Diese scheiterte jedoch bei der nächsten Wahl an der neu eingeführten Fünf-Prozent-Hürde. Nach der Auflösung der GVP wechselte sie wie die meisten Mitglieder zur SPD und erreichte 1957 über deren Landesliste wieder ein Bundestagsmandat, das sie bis zu ihrem Tod innehatte. Bis 1965 leitete sie den Petitionsausschuss, anschließend war sie bis zu ihrem Tod stellvertretende Vorsitzende dieses Ausschusses. Weitere Spitzenämter übernahm sie jedoch aufgrund ihrer angeschlagenen Gesundheit nicht mehr. Sie engagierte sich noch in der Bewegung Kampf dem Atomtod, setzte sich für Völkerverständigung ein und stimmte 1968 mit der Begründung, sie habe die Auswirkungen des Ermächtigungsgesetzes erlebt, gegen die Notstandsgesetze.[4]
Wilhelm Hamacher (1945–1946) |
Johannes Brockmann (1946–1948) |
Carl Spiecker (1948–1949) |
Fritz Stricker (1949) |
Helene Wessel (1950–1953) |
Johannes Brockmann (1953–1969) |
Gerhard Ribbeheger (1969–1974) |
Gerhard Woitzik (1974–1986) |
Adelgunde Mertensacker (1986–1987) |
Gerhard Ribbeheger (1987–1996) |
Gerhard Woitzik (1996–2009) |
Alois Degler oder Gerhard Woitzik (2009–2011, umstritten) |
Gerhard Woitzik (seit 2011)

Luise Albertz (SPD) |
Helene Wessel (SPD) |
Maria Jacobi (CDU) |
Lieselotte Berger (CDU) |
Gero Pfennig (CDU) |
Christa Nickels (Grüne) |
Heidemarie Lüth (PDS) |
Marita Sehn (FDP) |
Karlheinz Guttmacher (FDP) |
Kersten Steinke (Linke)
