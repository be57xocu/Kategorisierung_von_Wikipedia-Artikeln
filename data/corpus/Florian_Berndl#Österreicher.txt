Florian Berndl (* 10. Mai 1856 in Großhaselbach; † 30. November 1934 in Wien) war ein österreichischer Naturheilkundler.

Berndl wurde im Waldviertel als Sohn eines Schneiders und einer Hebamme geboren. Von seiner Mutter erwarb er auch jene Kenntnisse, die ihn später berühmt machen sollten. Zunächst erlernte er wie sein Vater das Schneiderhandwerk. Nach seinem Militärdienst, den er als Sanitäter verbrachte, wurde er Krankenpfleger im Wiener Allgemeinen Krankenhaus. Anschließend arbeitete er als Masseur und Pedikeur.

Bei Wanderungen fiel ihm das Gänsehäufel, eine Insel in der Alten Donau auf, das  er 1900 um jährlich 15 Gulden pachten konnte. Er zog mit seiner Frau und seinen Söhnen dort in eine Hütte und begann, seine Vorstellungen von einer natürlichen Lebensweise zu verwirklichen. Das Luft- und Sonnenbad Gänsehäufel wurde kurz darauf Anziehungspunkt für viele Wiener, darunter auch so prominente wie Hermann Bahr. Seine Ansichten brachten Berndl aber oft in Konflikte mit der evidenzbasierten Medizin. Angegriffen wurde er auch von konservativen Journalisten, denen das gemeinsame Baden von Frauen und Männern ein Dorn im Auge war. Der Pachtvertrag wurde 1905 annulliert, da Berndl für die Kantine im Bad keine Konzession besaß.

Berndl gründet dann nördlich vom Gänsehäufel die Kolonie Neu-Brasilien. Er kehrte allerdings auch an seine alte Wirkungsstätte zurück und wurde zunächst Oberbadewärter im Gänsehäufel, später auch Aufseher über die dort errichtete Kindererholungsstätte. Trotz Verbotes fuhr er mit seinen Naturheilverfahren fort, was 1913 zu seiner Entlassung und Delogierung führte.

Danach wollte er den Bisamberg zu einem Kurort für ärmere Bevölkerungsschichten machen, hatte damit aber nur wenig Erfolg. Trotzdem verbrachte er seine letzten 27 Lebensjahre hier. Das Schwimmbad Bisambergs, das "Florian Berndl Bad" wurde nach ihm benannt. 

Er ruht in einem ehrenhalber gewidmeten Grab auf dem Wiener Zentralfriedhof (43C-1-21). In Wien Donaustadt (22. Bezirk) wurde die Florian-Berndl-Gasse nach ihm benannt.
