Egon Kapellari (* 12. Jänner 1936 in Leoben, Steiermark, Österreich) ist ein emeritierter österreichischer römisch-katholischer Bischof und Jurist. Er war von 1982 bis 2001 Diözesanbischof der Diözese Gurk-Klagenfurt, von 2001 bis 2015 Diözesanbischof der Diözese Graz-Seckau und gleichzeitig stellvertretender Vorsitzender der österreichischen Bischofskonferenz.[1]
Kapellari ging in Leoben zur Schule und maturierte dort 1953. Danach studierte er bis 1957 an der Universität Graz Rechtswissenschaften und wurde zum Doktor der Rechtswissenschaften promoviert. Anschließend studierte er Theologie an der Universität Salzburg und Universität Graz.

Am 9. Juli 1961 empfing er durch Bischof Josef Schoiswohl in Graz die Priesterweihe. Von 1962 bis 1964 war Kapellari Kaplan in der Grazer Kalvarienbergpfarre. Anschließend war er bis 1981 Hochschulseelsorger der Katholischen Hochschulgemeinde und Leiter des Afro-Asiatischen Instituts in Graz. Ab 1968 arbeitete er außerdem bei der Leitung des Grazer Priesterseminars mit und am 15. September 1973 wurde er zum Monsignore (Kaplan seiner Heiligkeit) ernannt.

Am 7. Dezember 1981 wurde Kapellari zum Bischof der Diözese Gurk-Klagenfurt ernannt. Die Bischofsweihe spendete ihm Erzbischof Karl Berg am 24. Januar 1982. Mitkonsekratoren waren die Bischöfe Johann Weber und Maximilian Aichern. Sein Wahlspruch lautet Omnia vestra, vos autem Christi (Alles ist Euer, Ihr aber gehört Christus).

Kapellari wurde 1986 Mitglied im Päpstlichen Rat für die Kultur und war von 1996 bis 1998 Mitglied im Rat der Europäischen Bischofskonferenzen (CCEE). Zusätzlich wurde er 1997 Konsultor der Päpstlichen Kommission für die Kulturgüter der Kirche in der österreichischen Bischofskonferenz.

Am 14. März 2001 übernahm er die Diözese Graz-Seckau. Er war ab dem 4. April 2001 stellvertretender Vorsitzender der österreichischen Bischofskonferenz. In der Bischofskonferenz leitet er gemeinsam mit Erzbischof Franz Lackner OFM und Bischof Klaus Küng die Referate Liturgie (mit „Theologie und Ordnung der Weiheämter und Dienste“) sowie Kultur und Medien.[2]  Sein Rücktrittsgesuch als Diözesanbischof zum Erreichen der Altersgrenze von 75 Jahren wurde 2011 abgelehnt und ihm eine Verlängerung von zwei Jahren gewährt.[3]
Neben seinen vielfältigen Aufgaben als Bischof fand er immer wieder Zeit für das Schreiben theologisch und spirituell anspruchsvoller Bücher,[4] unter anderem über das Kirchenjahr und die Theologie der Symbole, die ihn im gesamten deutschsprachigen Raum bekannt machten.  Besonders beschäftigt sich Kapellari mit dem Thema Kunst.[5] Bekannt als Medien-Bischof ist er zudem Präsident der Katholischen Medien-Akademie Wien.[6]
Anlässlich der 54. Pfarrerwoche im September 2011 in Schloss Seggau sprach sich Kapellari für eine Geldstrafe für Priester aus, welche den Zölibat nicht einhalten.[7]
Zum Thema Pfarrerinitiative schrieb Kapellari 2012 in einem Hirtenbrief, er wolle alles tun, damit reformorientierte Katholiken „im großen Schiff der Diözese und der Weltkirche verbleiben“ könnten, wandte sich jedoch dagegen, dass „einige […] eigenmächtig das Steuerrad dieses Schiffes Kirche ergreifen“ wollten. Das führe zur „Spaltung“.[8]
Im März 2013 verhängte Kapellari ein Predigtverbot über Ostern gegen den Pfarrer von St. Veit am Vogau, Karl Tropper, wegen wiederholter verbaler Ausfälle gegen den Islam und Homosexuelle. Bei weiteren „Exzessen“ behielt sich der Bischof weiteres Einschreiten vor.[9]
Im September 2014 verbot Kapellari den „Gottesdienst am Rand“, eine Messfeier für Homosexuelle, wiederverheiratete Geschiedene und Alleinerziehende, die der Pfarrer des Pfarrverbandes Kirchberg an der Raab und St. Margarethen an der Raab, Bernhard Preiß, zum zweiten Mal feiern wollte, und bezeichnete sie als „nicht zu Ende gedachten Alleingang“. Er wolle jedoch mit Preiß im Gespräch bleiben. Aus Kirchberg verlautete, man wolle überlegen, wie man diese Gottesdienste in anderer Form weiterführen könne.[10]
Am 24. Jänner 2015 gab Kapellari in einem Hirtenbrief seinen in wenigen Tagen folgenden Rücktritt bekannt.[11] Sein Schritt ist bemerkenswert: Kapellari trat nach mehr als vierjährigem erfolglosen Warten auf seine altersbedingte Abberufung und auf einen Nachfolger an der Spitze der Diözese ab und wartete nicht mehr auf eine Entscheidung aus Rom.[12] Der Rücktritt wurde am 28. Jänner 2015 kirchenrechtlich nach Can. 401 §1 des Codex Iuris Canonici wirksam, nachdem die entsprechende Verlautbarung seines Rücktrittes in einem Bulletin des Heiligen Stuhls bekanntgegeben wurde.[13]
Das Grazer Domkapitel übernahm am 28. Jänner 2015 gemäß Can. 419 des Codex Iuris Canonici (CIC) als Konsultorenkollegium interimistisch die Leitung der Diözese und wählte für die Zeit der Sedisvakanz der Diözese Graz-Seckau gemäß Can. 421 §1 CIC den bisherigen Generalvikar Heinrich Schnuderl zu deren Diözesanadministrator.[14][15][16][17][18]
Die Ernennung des Nachfolgers Kapellaris und neuen Bischofs von Graz-Seckau, Wilhelm Krautwaschl, durch Papst Franziskus erfolgte am 16. April 2015 und wurde am selben Tag durch ein Bulletin des Heiligen Stuhls bekanntgegeben.[19] Bischof Kapellari fungierte bei der Weihe Bischof Krautwaschls am 14. Juni 2015 im Grazer Dom neben dem Hauptkonsekrator Erzbischof Franz Lackner als Mitkonsekrator.[20]
Bei Styria Verlag veröffentlicht:

Bischöfe (Fürstbischöfe) mit Sitz KlagenfurtFranz II. Xaver von Salm-Reifferscheidt-Krautheim |
Jakob Peregrin Paulitsch |
Georg Mayer |
Franz Anton Gindl |
Adalbert Lidmansky |
Valentin Wiery |
Peter Funder |
Josef Kahn |
Balthasar Kaltner |
Adam Hefter |
Andreas Rohracher (Kapitularvikar) |
Joseph Köstner |
Egon Kapellari |
Alois Schwarz

Bistum Seckau (bis 1782 in der Abtei Seckau)Philipp Renner (Administrator) |
Petrus Percic |
Georg IV. Agricola |
Sigmund von Arzt |
Martin Brenner |
Jakob I. Eberlein |
Johann IV. Markus von Altringen |
Max Gandolf von Kuenburg |
Wenzel Wilhelm Freiherr von Hofkirchen |
Johann V. Ernst Graf von Thun und Hohenstein |
Rudolf Josef von Thun |
Franz Anton Adolph von Wagensperg |
Josef I. Dominikus Graf von Lamberg |
Karl II. Josef Graf Kuenburg |
Leopold II. Anton Freiherr von Firmian |
Jakob II. Ernst von Liechtenstein-Kastelkorn |
Leopold III. Ernst Graf Firmian |
Josef II. Philipp Franz Graf von Spaur

Bischofssitz Graz (seit 1786)Josef III. Adam Graf Arco |
Johann VI. Friedrich Graf von Waldstein-Wartenberg |
Simon Melchior de Petris (als Apostolischer Vikar) |
Roman Sebastian Zängerle |
Josef IV. Othmar von Rauscher |
Ottokar Maria Graf von Attems (seit 1859 inklusive Diözese Leoben) |
Johann VII. Baptist Zwerger |
Leopold IV. Schuster |
Ferdinand Stanislaus Pawlikowski |
Josef V. Schoiswohl

Diözese Graz-Seckau (seit 1963)Josef V. Schoiswohl |
Johann VIII. Weber |
Egon Kapellari |
Wilhelm Krautwaschl
