Anna Helene Paquin (* 24. Juli 1982 in Winnipeg, Manitoba) ist eine kanadisch-neuseeländische Schauspielerin. 1994 erhielt sie im Alter von elf Jahren den Oscar als Beste Nebendarstellerin im Filmdrama Das Piano und war somit die zweitjüngste Oscargewinnerin der Geschichte. Bekanntheit erlangte sie aber vor allem durch die X-Men-Filme und die Serie True Blood. Paquin wurde bereits fünfmal für den Golden Globe nominiert.

Paquin wurde in Winnipeg, Kanada geboren. Ihre Mutter ist Englischlehrerin, ihr Vater Sportlehrer. Sie hat einen älteren Bruder namens Andrew (* 1977) und eine ältere Schwester namens Katya (* 1980). Ihre Kindheit verbrachte sie überwiegend in Neuseeland, nach der Scheidung ihrer Eltern 1995 zog sie mit ihrer Mutter nach Los Angeles. Sie spielte schon als Kind Viola, Cello und Piano und nahm Ballettunterricht. Nach ihrem Schulabschluss 2000 studierte sie ein Jahr lang an der Columbia University, entschied sich dann aber doch für eine Schauspielkarriere.

Ihre Schauspielkarriere begann 1991 in Neuseeland, als die Regisseurin Jane Campion eine junge Hauptdarstellerin für das Drama Das Piano suchte. Paquins Schwester Katya ging nach einer Anzeige in einer Zeitung zum Casting. Anna Paquin ging ursprünglich nur als Begleitung mit, wurde dann aber für die Rolle engagiert. Sie setzte sich damit gegen 5000 Bewerberinnen durch. Der Film war ein internationaler Erfolg und Paquin erhielt für ihre Rolle den Oscar als beste Nebendarstellerin. Sie ist nach Tatum O’Neal die zweitjüngste jemals mit diesem Preis ausgezeichnete Schauspielerin. Anschließend hatte sie zunächst nicht vor, Schauspielerin zu werden und lehnte neue Rollenangebote ab. Erst 1996 war sie wieder als junge Jane Eyre im gleichnamigen Film zu sehen. Im gleichen Jahr spielte sie die Hauptrolle in dem Drama Amy und die Wildgänse. In ihren Teenagerjahren war sie unter anderem auch in den Komödien Hurlyburly und Eine wie keine zu sehen, sowie in dem Drama Almost Famous – Fast berühmt an der Seite von Kate Hudson.

Zu erneutem, weltweitem Ruhm gelangte sie 2000 durch ihre Rolle in der Comic-Verfilmung X-Men, in der sie die Mutantin Rogue spielte. Im gleichen Jahr war sie an der Seite von Sean Connery in dem Drama Forrester – Gefunden! zu sehen. 2003 spielte sie erneut die Rogue in X-Men 2 und wiederholte diese Rolle auch 2006 im dritten Teil X-Men: Der letzte Widerstand. Für ihre Arbeit an dem Fernsehfilm Bury My Heart at Wounded Knee im Jahr 2007 wurde sie für den Golden Globe Award und den Emmy Award nominiert.

Von 2008 bis 2014 spielte Paquin die Hauptrolle in der Vampir-Serie True Blood. Die Serie basiert auf der Sookie-Stackhouse-Buchreihe von Charlaine Harris. Für diese Rolle erhielt sie 2009 den Golden Globe als beste Serien-Hauptdarstellerin und wurde auch 2010 in dieser Kategorie nominiert. Einen weiteren Erfolg im Fernsehen hatte sie 2009 mit der Titelrolle des Films The Courageous Heart of Irena Sendler, die ihr ebenfalls eine Nominierung für den Golden Globe einbrachte. 2010 war sie an der Seite von Katie Holmes in der Komödie The Romantics zu sehen und hat einen Cameo-Auftritt in Scream 4 übernommen.

Der Scream Award für den besten Gruselschauspieler wurde Anna Paquin von Paul Wesley, Nina Dobrev und Ian Somerhalder verliehen.

Paquin ist seit 2009 mit ihrem Serienkollegen Stephen Moyer liiert.[1] In der Serie True Blood spielen die beiden ein Paar. Am 21. August 2010 heiratete das Paar in Malibu.[2] Außerdem outete Paquin sich 2010 als bisexuell.[1] Paquin ist Stiefmutter eines Sohnes (* 2000) und einer Tochter (* 2002). Im September 2012 bekam das Paar Zwillinge.[3]
Academy Awards

Academy of Science Fiction, Fantasy & Horror Films (Saturn Award)

Emmy Awards

Golden Globe Awards

Los Angeles Film Critics Association Awards

MTV Movie Awards

Online Film Critics Society Awards

Satellite Awards

Screen Actors Guild Awards

Scream Awards

Teen Choice Awards

Young Artist Awards
