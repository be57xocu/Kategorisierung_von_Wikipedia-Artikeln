Dirk Erik Fischer (* 29. November 1943 in Bevensen) ist ein deutscher Politiker (CDU).

Er war von 1989 bis 2014 verkehrspolitischer Sprecher der CDU/CSU-Bundestagsfraktion. Seit November 2007 ist er Präsident des Hamburger Fußball-Verbandes (HFV) und Mitglied im Vorstand des Deutschen Fußball-Bundes (DFB).

Nach dem Abitur 1964 am Ostsee-Gymnasium Timmendorfer Strand leistete Fischer seinen Wehrdienst ab und durchlief auch die Ausbildung zum Reserveoffizier (letzter Dienstgrad: Oberleutnant d.R.). Anschließend absolvierte er ab 1966 ein Studium der Rechtswissenschaft an der Universität Hamburg, welches er 1975 mit dem ersten Staatsexamen beendete. Nach dem Referendariat bestand er 1978 auch das zweite Staatsexamen und war bis November 1986 als Syndikus und Revisor bei dem Hamburger Unternehmen Fa. Möller + Förster angestellt.[1] Seit 1982 ist er als Rechtsanwalt zugelassen.

Fischer trat 1967 in die Junge Union und die CDU ein. Er war von 1970 bis 1977 Landesvorsitzender der Jungen Union in Hamburg und gehört seit 1972 dem Hamburger CDU-Landesvorstand an. Von 1974 bis 1996 war Fischer auch Vorsitzender des CDU-Kreisverbandes Hamburg-Nord. Fischer war ab 1976 zunächst stellvertretender Vorsitzender und von 1992 bis 2007 Vorsitzender des CDU-Landesverbandes Hamburg. Er gehörte außerdem dem CDU-Bundesvorstand an.

Fischer ist Ehrenvorsitzender der Hamburger CDU.[2]
Erstmals errang Fischer durch die Bürgerschaftswahl in Hamburg 1970 einen Sitz in der Hamburgischen Bürgerschaft, den er über drei Legislaturperioden bis zur Mandatsniederlegung am 5. Februar 1981 wahrnahm.

Von 1980 bis 2017 war er Mitglied des Deutschen Bundestages. Dort war er von 1989 bis 2014 Vorsitzender der Fraktionsarbeitsgruppe Verkehr (1998 bis 2005: Verkehr, Bau- und Wohnungswesen bzw. seit 2005 Verkehr, Bau und Stadtentwicklung, Aufbau Ost) und damit verkehrspolitischer Sprecher der CDU/CSU-Bundestagsfraktion. In der 18. Wahlperiode war er ordentliches Mitglied im Ausschuss für Verkehr und digitale Infrastruktur sowie stellvertretendes Mitglied im Sportausschuss[3]. Von 1994 bis 2014 war er außerdem Vorsitzender der Landesgruppe Hamburg in der CDU/CSU-Fraktion.

Dirk Fischer war 1987, 1990, 1994, 2009 und 2013 direkt gewählter Abgeordneter im Bundestagswahlkreis Hamburg-Nord und zog sonst stets über die Landesliste Hamburg in den Bundestag ein. Sein Nachfolger wurde 2017 Christoph Ploß.[4]
September 1994: Bundesverdienstkreuz 1. Klasse[5]
Franz Beyrich (Oktober 1945–November 1945) |
Johannes Speckbötel (1945) |
Otto Wilhelm Wendt (1945–1946) |
Max Ketels (1946–1948) |
Hugo Scharnberg (1948–1954) |
Josef von Fisenne (1954–1956) |
Hugo Scharnberg (1956–1958) |
Erik Blumenfeld (1958–1968) |
Dietrich Rollmann (1968–1973) |
Jürgen Echternach (1974–1992) |
Dirk Fischer (1992–2007) |
Michael Freytag (2007–2010) |
Frank Schira (2010–2011) |
Marcus Weinberg (2011–2015) |
Roland Heintze (seit 2015)
