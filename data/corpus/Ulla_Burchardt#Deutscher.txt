Ursula „Ulla“ Burchardt (* 22. April 1954 in Dortmund) ist eine deutsche Politikerin (SPD). Sie ist Lehrbeauftragte an der TU Dortmund und arbeitet als Wissenschafts- und Politikberaterin. Von 1990 bis 2013 war sie direkt gewählte Abgeordnete des Deutschen Bundestages. 2005 bis 2013 war sie Vorsitzende des Ausschusses für Bildung, Forschung und Technikfolgenabschätzung. 2014 wurde sie in das Kuratorium der TU Berlin gewählt.[1]
Nach dem Abitur 1972 absolvierte Ulla Burchardt ein Studium der Pädagogik, Sozialwissenschaften und Psychologie in Bochum und Bielefeld, welches sie 1977 als Diplom-Pädagogin beendete. Danach war sie bis 1978 als Jugendbildungsreferentin und seit 1979 als Referentin in der Erwachsenenbildung tätig.

Ulla Burchardt lebt in Dortmund und Berlin und hat zwei Töchter.

Seit 1976 ist sie Mitglied der SPD. Sie gehört dem Forum Bildung des SPD-Parteivorstandes sowie den Wissenschaftsforen der Bundes-SPD und NRW-SPD an.

Von 1990 bis 2013 und als erste Frau aus Dortmund war Ulla Burchardt Mitglied des Deutschen Bundestages. Hier war sie von 1998 bis 2002 Vorsitzende der Querschnittsgruppe Nachhaltige Entwicklung der SPD-Bundestagsfraktion. Von 1998 bis 2005 war sie stellvertretende Vorsitzende des Ausschusses für Bildung, Forschung und Technikfolgenabschätzung. Von November 2005 an war sie Vorsitzende dieses Ausschusses. Ulla Burchardt war außerdem mehr als zehn Jahre Vorsitzende der deutsch-italienischen Parlamentariergruppe.

Bis 1998 wurde Ulla Burchardt im Wahlkreis Dortmund III direkt in den Deutschen Bundestag gewählt. Nach der Wahlkreisreform errang sie 2002 ihr Mandat im neugebildeten Wahlkreis Dortmund II. Bei der Bundestagswahl 2005 erreichte sie hier 57,9 % der Erststimmen und wurde erneut als Abgeordnete des Deutschen Bundestags bestätigt. Das Direktmandat ihres Wahlkreises konnte sie bei der Bundestagswahl 2009 mit 42,4 % der abgegebenen Stimmen verteidigen. Burchardt gab im Sommer 2011 bekannt, nicht mehr für eine weitere Legislaturperiode im Bundestag zu kandidieren.
Zur Bundestagswahl 2013 trat Burchardt nicht erneut an.[2]
Der Tunnel Berghofen (B236n) in Dortmund wird aufgrund des Einsatzes der Bundestagsabgeordneten Burchardt für den Bau dieser Unterführung im Volksmund „Ulla-Tunnel“ genannt. Ulla Burchardt ist zudem Tunnelpatin.
