Sr. Suzanne Toolan RSM (* 24. Oktober 1927 in Lansing, Michigan) ist eine US-amerikanische Lehrerin, Musikerin und Komponistin.

Sie kam durch Jugendtreffen in Nordamerika mit Taizé in Kontakt. Im Alter von 17 Jahren zog sie nach Hollywood, wo sie Studentin von Richard Keys Biggs wurde. Seit ihrem Ordenseintritt ("Sisters of Mercy") lebt und arbeitet Suzanne Toolan als Lehrerin, Musikerin und Komponistin im Mercy Center, Burlingame, Kalifornien.

Für Taizé schrieb sie "When the night becomes dark", das "Kyrie A" und das "Alleluia 16". International bekannt wurde sie jedoch mit einem schon viel früher entstanden Neuen Geistlichen Lied, "I Am the Bread of Life" (Copyright 1971 G.I.A. Publications Inc., 7407 S.Mason, Chicago, Ill 60638). Dieses Lied findet sich unter den Titeln "Ich bin das Brot des Lebens" oder "Und er wird auferstehn" auch in zahlreichen deutschsprachigen Liederbüchern.

2007 erschien ihre Autobiographie:
