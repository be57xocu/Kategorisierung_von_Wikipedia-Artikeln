Robert Adame Beltran (* 19. November 1953 in Bakersfield, Kalifornien) ist ein US-amerikanischer Schauspieler mexikanischer Abstammung.

Robert Beltran studierte an der California State University, Fresno. Er wuchs mit zwei Schwestern und sieben Brüdern auf. Weltweit bekannt wurde er in der Fernsehserie Star Trek: Raumschiff Voyager als Commander Chakotay.

Beltran genoss seine Ausbildung am Fresno State College, wo er einen Abschluss in Theaterkunst machte. 1981 bekam er die erste Filmrolle in Zoot Suit und war danach in verschiedenen weiteren Filmen zu sehen, wie zum Beispiel Gaby – Eine wahre Geschichte (1987), Kiss me a Killer – Tödliche Begierde (1991), Bugsy (1991) oder auch Nixon – Der Untergang eines Präsidenten (1995). Neben seiner Arbeit beim Film ist er auch im Theater aktiv und gründete die East LA Classic Theater Group.

Beltran war für mehrere Preise nominiert und gewann 1997 den Nosotros Golden Eagle Award als „Bester Darsteller in einer Fernsehserie“. Die Nosotros-Gesellschaft fördert spanisch sprechende  Künstler.
