Clemens Alexander Winkler (* 26. Dezember 1838 in Freiberg; † 8. Oktober 1904 in Dresden) war ein deutscher Chemiker. Er ist der Entdecker des chemischen Elements Germanium.

Clemens Winkler war der Sohn des Metallurgen Kurt Alexander Winkler, Neffe des Mineralogen August Breithaupt und Cousin des Geologen Hermann Theodor Breithaupt. Sein Pate war der Chemiker Ferdinand Reich. Der Chemiker Ferdinand Bischoff war sein Schwager.

Nach seinen Studien an der Chemnitzer Gewerbeschule (1853–1856) und an der Freiberger Bergakademie (1857–1859) arbeitete Clemens Winkler zunächst in den Blaufarbenfabriken Oberschlema und Niederpfannenstiel. Im Jahr 1864 promovierte er an der Universität Leipzig über das kristalline Silicium und dessen Verbindungen; im gleichen Jahr wurde er Hüttenmeister im Blaufarbenwerk Niederpfannenstiel.
Dort hatte Winkler auch Zeit für eigene Versuche. Er untersuchte die Reaktionen des Elementes Indium – das sein früherer Lehrer Ferdinand Reich entdeckt hatte – bestimmte das Atomgewicht, ermittelte die Zweiwertigkeit und stellte eine Vielzahl von Salzen Indiums dar. Er bestimmte die Atomgewichte von Nickel und Cobalt.

Auf Empfehlung Hermann Kolbes wurde Clemens Winkler im Jahr 1873 der Nachfolger von Theodor Scheerer als Professor für anorganische Chemie an die Bergakademie Freiberg berufen. Seine ersten Arbeiten handelten von der Mineralanalyse, für die Gewichtsanalyse von Metallarten wendete Winkler die Elektroanalyse an. Winkler führte 1898 die Drahtnetzelektrode aus Platin ein[1] und gilt als Mitbegründer der Elektroanalyse.

Bereits 1831 hatte in England Peregrine Phillips ein Patent zur Erzeugung von Schwefelsäure aus Schwefeldioxid durch Platinkontakt. Winkler entwickelte zu diesem Zweck platinierten Asbest und erhielt eine sehr hohe Ausbeute an Schwefelsäure. 1876 entstand eine Schwefelsäurefabrik bei Freiberg. Winkler war ein Mitbegründer des Kontaktverfahrens und erkannte Arsen als Kontaktgift, das die Wirkung des Katalysators verschlechterte.
Später entwickelte der BASF-Chemiker Rudolf Knietsch das Verfahren weiter, es fand weltweite Anerkennung.

Winkler verbesserte die gasanalytischen Methoden von Robert Bunsen und vereinfachte sie so weit, dass sie weite Anwendung in der Industrie fanden; er gilt gemeinsam mit dem Chemiker Walther Hempel als ein Begründer der technischen Gasanalyse.

Die bedeutendste Leistung Winklers war die Entdeckung des Elementes Germanium am 6. Februar 1886. Bei der Analyse des seltenen Minerals Argyrodit fand er heraus, dass dieses zu ca. 75 Prozent aus Silber, zu 17 Prozent aus Schwefel und zu geringen Anteilen (insgesamt ca. 1 Prozent) aus Eisen, Quecksilber und Zink bestand. Nun fehlten noch sieben Prozent zum Ganzen. Nach mehrmonatiger Arbeit konnte Winkler schließlich ein neues Element mithilfe des Freiberger Aufschlusses isolieren, das er – in Anlehnung der Namensgebung der Elemente Gallium und Scandium – als Germanium bezeichnete.[2] Seine Entdeckung bestätigte die theoretische Vorarbeit von Dmitri Iwanowitsch Mendelejew, der die Existenz eines Elementes, das er Eka-Silicium nannte, mit diesen Eigenschaften vorausgesagt hatte.

Im Jahre 1894 traf er sich erstmals mit Mendelejew, mit dem er seit 1886 Briefkontakte pflegte. Von 1896 bis 1899 wirkte Winkler als Direktor der Bergakademie; Berufungen an andere Universitäten lehnte er ab. Er starb im Jahr 1904 im Alter von 65 Jahren an Krebs.

Clemens Winkler war Angehöriger des Weinheimer Corps Franconia Freiberg. Er ist auf dem Trinitatisfriedhof in Dresden begraben.

Das Werk Clemens Winklers umfasst 141 wissenschaftliche Veröffentlichungen. Die folgende Liste stellt eine Auswahl seiner Publikationen dar.
