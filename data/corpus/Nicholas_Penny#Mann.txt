Sir Nicholas Penny FBA FSA (* 21. Dezember 1949) ist ein englischer Kunsthistoriker und Museumskurator. 

Nicholas Penny studierte am St Catharine’s College, Cambridge und am Londoner  Courtauld Institute of Art, wo er 1975 promovierte. Von 1975 bis 1982 lehrte er Kunstgeschichte an der Universität Manchester. Danach wirkte er am King’s College in Cambridge. Von 1980 bis 1981 war er „Slade Professor of Fine Art“ an der Universität Oxford. Er war Kurator der Abteilung für Westliche Kunst am Ashmolean Museum.

Seit 2000 ist er „Andrew W. Mellon Professor“ am National Gallery of Art’s Center for Advanced Study in the Visual Arts. An der National Gallery of Art in Washington, D.C. wurde er 2002 zum „Senior Curator of Sculpture“ ernannt. Von 2008 bis 2015 war er Direktor der Londoner Nationalgalerie.[1]
Seit 2007 ist er Mitglied der American Academy of Arts and Sciences.
2015 erhielt er den Ritterschlag als Knight Bachelor.[2]