Hyacinthe Laurent Théophile Aube (* 22. November 1826 in Toulon; † 31. Dezember 1890 ebenda) war französischer Marineminister und Gouverneur von Martinique zwischen 1879 und 1881.

Aube wurde 1854 Fregattenkapitän. Im Deutsch-Französischen Krieg 1870/71 organisierte er die Verteidigung der Carentanlinie. 1879 erhielt er den Posten des französischen Gouverneurs von Martinique und kehrte 1881 als Konteradmiral nach Frankreich zurück und wurde mit der Leitung des Torpedowesens betraut. Er bekleidete das Amt des französischen Marineministers vom 7. Januar 1886 bis zum 30. Mai 1887.[1] 1886 wurde er zum Vizeadmiral befördert. 

Aube begründete die Denkrichtung der „Jeune École“, die den Bau von kleineren Schiffs-Einheiten zu Lasten der Schlachtschiffe vertrat.

Volkmar Bueb lieferte folgende Einschätzung Aubes: „Er war ein echter Provençale von freundlichem, schlichtem, aber temperamentvollem Wesen, diskussionsfreudig und allem Neuen aufgeschlossen. In der Stille provinzieller Zurückgezogenheit, weit ab vom Getriebe der Großstadt und auf dem Schiffe fühlte er sich am wohlsten. Sein Aufstieg zum Vizeadmiral und Marineminister war mühsam und voller Rückschläge.“[2]
Aube war Großoffizier der Ehrenlegion.
