Martha Grimes (* 2. Mai 1931 in Pittsburgh, Pennsylvania, Vereinigte Staaten) ist eine amerikanische Schriftstellerin. Sie ist bekannt durch ihre erfolgreichen Romane um Inspektor Jury und gilt als Meisterin des klassischen Kriminalromans.

Grimes wurde als Tochter eines Rechtsanwalts geboren und studierte Anglistik an der Universität von Maryland. Sie schrieb zuerst Gedichte, bis dann 1981 ihr erster Roman mit einer Auflage von 3000 Exemplaren erschien. Die Inspektor-Jury-Reihe setzte sie seitdem konsequent fort.

Heute lebt Martha Grimes in Santa Fe (New Mexico) und in Washington, D.C. Sie ist Professorin für Literatur und Kreatives Schreiben und lehrte lange Zeit an der Johns-Hopkins-Universität und am Montgomery College in Tahoma Park, Maryland. Sie ist aktive Tierschützerin und spendete den größten Teil des Erlöses ihres Buches „Das Mädchen ohne Namen“ an Tierschutzvereine.

Ihre Romane gelten als Werke bester Krimi-Tradition und werden häufig mit denen Agatha Christies verglichen.

In ihren traditionellen englischen Kriminalromanen um Inspektor Richard Jury beschreibt Martha Grimes die sich in komplizierten Verstrickungen verlierenden Fälle des melancholischen Protagonisten. Der Inspektor bzw. Superintendent von Scotland Yard wird dabei von seinem humorvollen Freund Melrose Plant unterstützt, der sich ob seines ererbten Reichtums erlauben kann, seine Adelstitel abzulegen, sowie von dem stets kränkelnden Assistenten Sergeant Wiggins.

Um Inspektor Jury haben sich zwei Mikrokosmen gebildet:

Zum anderen existiert auch um Jurys Freund Melrose Plant solch ein Mikrokosmos.

Die Inspektor-Jury-Kriminalromane stecken voller amüsanter Nebengeschichten, die sich mit trockenem Humor rund um die (amourösen) Abenteuer des ganz normalen Lebens ranken. Die Haupthandlungsstränge, die Mordgeschichten, sind im Gegensatz dazu düster, blutig und grausam. Oft geht es um Kindesmissbrauch, seelisch oder körperlich. Es gibt in den Inspektor-Jury-Romanen viele sehr einsame Kinder, oft Mädchen, die Schreckliches durchmachen oder mit ansehen mussten und die sich nun tapfer bemühen, trotz allem in dieser kalten Welt zu überleben.

Ein Echo finden diese heroischen, verlorenen kleinen Gestalten in dem Einzelgänger Richard Jury selbst, der als Kind seine Mutter bei einem Bombenangriff im Zweiten Weltkrieg verlor. Jurys Liebesgeschichten, die alle von einer Art Helfersyndrom bestimmt sind, enden - bis auf „The Winds of Change“ - mit Trennung, oder schlimmer, durch Mord. Als sein Counterpart und den Plot vorantreibenden Dialogpartner gibt es seit Mitte der Serie den Divisional Commander Brian Macalvie, einen verstandesbetonten Kollegen à la Mr. Spock oder Tatort-Prof. Boerne.

Die Freunde in Long Piddleton sind ebenso - mit all ihren guten Eigenschaften und ihrem Witz - im Herzen Nichterwachsene geblieben, die es nicht fertigbringen, sich gegen die entsetzliche Tante Agatha zu wehren, die alle tyrannisiert. Selbst unverheiratet, sitzen sie die ganze Zeit im Pub zusammen und hintertreiben völlig kindisch die Hochzeit von Vivian Rivington, indem sie deren Verlobten vergraulen.

Inspektor Jury und sein Umfeld machen im Fortschreiten der Romanserie eine Entwicklung durch. Während die Eskapaden seiner Long Piddletoner Gefährten immer alberner werden, tritt Jury menschlich immer hilfloser auf der Stelle, bis er im äußerst düsteren Krimi „The Blue Last“, der auch noch einmal die Kriegstraumata thematisiert, verraten wird und fast ums Leben kommt. Erst nach dieser Katastrophe gelingt es ihm, sich aus der seelischen Erstarrung zu lösen, wie der englische Krimititel „The Winds of Change“ auch andeutet.

Alle „Inspector-Jury“-Romane sind im Original nach real existierenden Pubs benannt. Der MDR produzierte zwischen 1995 und 2003 zwölf der Romane als Hörspiele.[1]
Die Romane „Inspektor Jury schläft außer Haus“, „Inspektor Jury lichtet den Nebel“ und "Inspektor Jury spielt Katz und Maus" wurden 2013, 2015 und 2017 im Auftrag von ZDF und ORF unter den Titeln „Der Tote im Pub“, „Mord im Nebel“ und "Inspektor Jury spielt Katz und Maus" verfilmt [2].
