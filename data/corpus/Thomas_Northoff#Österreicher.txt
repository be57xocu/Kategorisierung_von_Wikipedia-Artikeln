Thomas Northoff (* 18. November 1947 in Wien) ist ein österreichischer Schriftsteller. 
Er veröffentlichte unter anderem Romane und lyrilk. Als fortlaufende Projekte betreibt er das Österreichische GraffitiArchiv für Literatur, Kunst und Forschung und das literarische Projekt StadtLeseBuch/Letztes VolksBuch. Thomas Northoff lebt in Wien.

