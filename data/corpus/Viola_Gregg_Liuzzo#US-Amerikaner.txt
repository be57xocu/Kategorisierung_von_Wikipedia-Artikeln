Viola Fauver Gregg Liuzzo (geborene Gregg; * 11. April 1925 in California, Pennsylvania; † 25. März 1965 in Selma, Alabama) war eine US-amerikanische Bürgerrechtlerin.

Viola Gregg wuchs, als europäischstämmige Tochter einer Lehrerin und eines arbeitslosen Bergarbeiters, in Tennessee und Georgia in ärmlichen Verhältnissen auf. Die Südstaaten waren in dieser Zeit von Rassentrennung, Diskriminierung und Hass geprägt, aber die Familie Gregg fühlte sich den unterdrückten Schwarzen stärker verbunden als den wohlhabenden Weißen.

Während des Zweiten Weltkriegs zog die Familie nach Michigan, und Gregg nahm eine Arbeit als Kellnerin in einer Cafeteria an. Sie heiratete und bekam zwei Töchter; ihre beste Freundin Sarah Evans war eine Schwarze. 1949 geschieden, heiratete sie zwei Jahre später den Gewerkschaftsaktivisten Anthony Liuzzo, mit dem sie drei weitere Kinder hatte. Während sie 1961 eine Ausbildung zur Medizinisch-Technischen Assistentin (MTA) im Carnegie Institute in Detroit aufnahm, arbeitete Evans für die Familie als Haushälterin und Kinderfrau. 1963 bildete sie sich an der Wayne State University weiter.

Neben ihrer Ausbildung und dem durchaus bewegten Familienleben nahm Liuzzo an lokalen Aktionen zur Verbesserung von Ausbildung und sozialer Gerechtigkeit teil. Zweimal wurde sie verhaftet, plädierte auf schuldig und bestand auf einer Gerichtsverhandlung, um größere Publizität zu erreichen.

1964 wurde sie Mitglied der unitarisch-universalistischen Gemeinde in Detroit.

Anfang 1965 sah Viola Liuzzo wie Millionen anderer Amerikaner im Fernsehen die Nachrichten über den ersten der Selma-nach-Montgomery-Märsche, den „Blutigen Sonntag“, bei dem 500 friedliche Bürger auf einer Wahlrechtskundgebung in Selma, Alabama von der Polizei mit Gasgranaten auseinander getrieben wurden. Innerhalb der Unitarischen Gemeinden wurde dazu aufgerufen, nach Selma zu reisen und Martin Luther King zu unterstützen. Sie entschloss sich, dort mindestens eine Woche zu bleiben und mitzuarbeiten.

Unter anderem half sie mit, Demonstranten in ihrem Auto zu fahren. Während eines dieser Transporte näherte sich ihrem Fahrzeug ein Wagen, aus dem heraus sie erschossen wurde. Ihr Auto fuhr in den Graben, ihre Mitfahrer stellten sich tot und retteten damit wohl ihre Leben.

Ihre vier Mörder, Mitglieder des Ku-Klux-Klan, wurden angeklagt und vor Gericht gestellt. Obwohl einer der Männer ihre Tat zugab, als Kronzeuge aussagte und damit Immunität erlangte, wurden die anderen drei nicht wegen Mordes verurteilt. Sie erhielten zehn Jahre Gefängnis wegen Verletzung von Liuzzos Bürgerrechten. Im Anschluss an dieses Verfahren wurde Liuzzo von KKK-Sympathisanten in der Öffentlichkeit politisch und moralisch verleumdet, was zeitweise durchaus regionalen Erfolg hatte.

Nachdem ihre Kinder von 1979 bis 1983 in einem Gerichtsprozess vergeblich versucht hatten, die Komplizenschaft des FBI nachzuweisen, wurde Viola Gregg Liuzzo erst 2002 vollständig rehabilitiert und offiziell mit einer Gedenkplakette in Boston geehrt.

Im Film Selma (2014) wird Liuzzo dargestellt von Tara Ochs.
