Leon Marcus Uris (* 3. August 1924 in Baltimore, Maryland; † 21. Juni 2003 in Shelter Island, New York) war ein US-amerikanischer Schriftsteller.

Leon Uris war der Sohn einer jüdisch-polnischen Familie. Mit 17 Jahren trat er dem United States Marine Corps bei. Während des Zweiten Weltkriegs war er im Südpazifik eingesetzt. In dieser Zeit lernte er seine erste Frau Betty kennen, die ebenfalls im Marine Corps diente.

Seine Kriegserlebnisse verarbeitete er in seinem ersten Roman Schlachtruf oder Urlaub bis zum Wecken (original: Battle cry). Anregungen zu seinem erfolgreichsten Roman Exodus erhielt er durch seinen Einsatz als Korrespondent in Israel. Dieser Roman machte ihn 1958 international bekannt. Da Uris im Roman Exodus den Namen des polnischen Arztes Władysław Alexander Dering genannt und ihn mit Menschenversuchen im Konzentrationslager verknüpft hatte, verklagte ihn dieser vor einem Londoner Gericht. Uris verarbeitete diese Geschichte im Roman QB VII, der auch als Film und als Serie erfolgreich war.

Uris’ Bekanntheitsgrad wurde 1969 durch Alfred Hitchcocks Verfilmung des Romans Topas weiter gesteigert. 

Leon Uris war dreimal verheiratet. Von 1945 bis 1965 mit Betty Katherine Beck, aus dieser Ehe entstammen drei Kinder. Seine zweite Ehe mit Margery Edwards dauerte nur wenige Monate bis zu ihrem Tod 1969. Aus seiner dritten Ehe mit Jill Peabody 1970 bis 1989 hat er zwei weitere Kinder.

Als Joseph Heller 1953 mit seinem Buch Catch-22 begann, nannte er es Catch-18. Bei seiner Veröffentlichung im Oktober 1961 hatte er den Titel angepasst um eine Ähnlichkeit/Verwechselung zu Uris' kurz zuvor erschienener Novelle Mila 18 zu vermeiden.[1][2]