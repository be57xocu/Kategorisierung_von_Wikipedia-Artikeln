Kimila „Kim“ Anna Basinger [ˈbeɪsɪŋɡəɹ] (* 8. Dezember 1953 in Athens, Georgia) ist eine US-amerikanische Schauspielerin.

Basinger wurde als drittes von fünf Kindern des Jazzmusikers Don Basinger und seiner Frau Ann, einer Synchronschwimmerin, die in Esther-Williams-Filmen mitwirkte, geboren. So war ihr das Showbusiness von Kindheit an vertraut. Als junge Frau erhielt sie Ballettunterricht und gewann Schönheitskonkurrenzen in Georgia. Anfang der 1970er Jahre ging sie nach New York. Neben der Arbeit als gut verdienendes Fotomodell nahm sie privaten Schauspielunterricht und spielte an kleinen Bühnen in Greenwich Village Theater.

1976 ging Basinger nach Hollywood und trat zunächst in einigen Fernsehserien wie Drei Engel für Charlie auf. Der Durchbruch als Schauspielerin gelang ihr 1983 als Bond-Girl Domino Petachi in dem James-Bond-Streifen Sag niemals nie und an der Seite von Mickey Rourke in 9½ Wochen.

1992 schlug sie die Hauptrolle in Basic Instinct aus, die dann Sharon Stone übernahm. 1997 kam sie mit dem Film L.A. Confidential zurück, für den sie einen Oscar erhielt. Damit war sie das erste ehemalige Bond-Girl, das diesen Preis gewann.
Sie gehörte zu den bestbezahlten Hollywoodstars. Für den Film Ich träumte von Afrika erhielt sie eine Gage von 5 Millionen US-Dollar.

2010 spielte sie an der Seite von Jungdarsteller Zac Efron in dem Drama Wie durch ein Wunder.

Sie war von 1980 bis 1989 mit dem Visagisten Ron Snyder verheiratet, den sie 1980 bei Dreharbeiten zu dem Film Jodie – Irgendwo in Texas kennengelernt hatte, und von 1993 bis 2002 mit dem US-amerikanischen Schauspieler Alec Baldwin; mit ihm hat sie die Tochter Ireland Baldwin (* 1995), die in den USA ein bekanntes Model ist.
