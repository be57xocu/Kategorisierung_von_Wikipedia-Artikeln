George Ellery Hale (* 29. Juni 1868 in Chicago, Illinois; † 21. Februar 1938 in Pasadena) war ein US-amerikanischer Astronom. Mit ihm verbindet man die Ära der großen Spiegelteleskope und wesentliche Fortschritte in der Sonnenforschung und bei Modellen der Sternentwicklung. Er arbeitete eng mit dem Teleskopbauer George Willis Ritchey zusammen.[1]
Hale, geboren 1868 in Chicago und aus wohlhabender Familie stammend, stellte bei der Beobachtung des Sonnenspektrums die Aufspaltung von Absorptionslinien (Zeeman-Effekt) fest und wies nach, dass die Sonnenflecken Orte starker Magnetfelder sind.[2]
Bereits als 21-Jähriger entwickelte er als Student am Massachusetts Institute of Technology (MIT) um 1890 (gleichzeitig mit Henri-Alexandre Deslandres) den Spektroheliografen. Dieses Instrument wurde bald weltweit zur Untersuchung der Sonne in engen Spektralbereichen eingesetzt.

Er studierte am MIT, war 1889/90 am Observatorium des Harvard College und 1893/94 in Berlin. 1890 leitete er sein eigenes Observatorium (Kenwood Astrophysical Observatory) auf dem Familiensitz im Chicagoer Stadtteil Kenwood. 1891 bis 1893 war er Professor für Astrophysik am Beloit College in Beloit (Wisconsin).

Hale wurde 1892 an die Universität Chicago berufen, wo er bis 1897 Associate Professor und danach bis 1905 Professor war. Da er im selben Jahr von einem neuen 40-Zoll-Objektiv hörte, bedingte er sich den Bau einer großen Sternwarte aus. Es gelang Hale, dafür einen reichen Sponsor aus Chicago zu finden: Charles Yerkes. So wurde der riesige Refraktor mit 102-cm-Öffnung 1897 am neuen Yerkes-Observatorium das damals größte Teleskop der Welt. Es ist noch heute das größte jemals gebaute Linsenfernrohr.

Hale war ein äußerst energiegeladener Mann, der für seine Forschungen noch weitere Sternwarten gründete, die mit immer größeren Teleskope und der neuesten Ausrüstung ausgestattet waren. In Pasadena hatte er sein eigenes Sonnenobservatorium (Hale Solar Laboratory). Er spielte eine führende Rolle beim Aufbau des Caltech.

Als Sekretär für Auslandsbeziehungen der National Academy of Sciences nominierte Hale 1914 und 1916 ungeachtet des Ersten Weltkrieges den deutschen Physiker Johannes Stark für den Nobelpreis für Physik.[3] Auf Hales Betreiben wurde 1916 der National Research Council gegründet, für den dann Hale bis 1917 zusammen mit Robert Andrews Millikan wirkte. Hales Ziel war, dass Wissenschaftler statt des normalen Kriegsdienstes entsprechend ihrer Qualifikation  als Wissenschaftler eingesetzt wurden.

Für den Bau eines riesigen Teleskop-Spiegels begann Hale seit dem Jahr 1927 zu werben. 1928 konnte er den Präsidenten der Rockefeller-Stiftung von dieser gewaltigen Investition in die Wissenschaft überzeugen. Das Spiegelteleskop auf dem Mount Palomar wurde 1947 fertiggestellt. Mit einem Hauptspiegel von 200 Zoll (5 Meter) war es bis 1975 das größte Teleskop der Welt. Noch heute ist es das größte Teleskop mit äquatorialer Montierung.

Mount Wilson besitzt ein Spiegelteleskop mit einem Hauptspiegel von 2,5 Metern Durchmesser, das unter der Leitung von George Willis Ritchey erbaut wurde. Edwin Hubble konnte damit um 1923 die ersten Einzelsterne im Andromedanebel nachweisen und die Expansion des Universums entdecken. Bis zum Bau des Palomar-Teleskops war dieses Teleskop das weltweit größte.

Hale hatte psychische Probleme (Schübe von Schizophrenie), die ihn auch dazu zwangen, seinen Direktorposten am Mount Wilson aufzugeben.

1894 gründete er das Astrophysical Journal, dessen Herausgeber er ab 1895 war. 1892 bis 1895 war er Mitherausgeber von Astronomy and Astrophysics.

Er war auswärtiges Mitglied der Académie des sciences (1919) und der Royal Society (1909).[4]
Die beiden Hale-Observatorien auf dem Mount Wilson und dem Mount Palomar wurden 1970 zu seinen Ehren nach ihm benannt.

Der Mondkrater Hale ist nach ihm und dem Erfinder William Hale benannt. Seit 1973 ist auch der Marskrater Hale nach ihm benannt. Auch der Asteroid (1024 Hale) ist nach ihm benannt.
