Johann Kurzbauer (* 11. Juni 1943 in Raipoltenbach, Niederösterreich) war von 1994 bis 2006 Abgeordneter zum Nationalrat für die Österreichische Volkspartei (ÖVP) und von Frühling 1995 bis 23. Oktober 2007 Bürgermeister von Neulengbach. Er ist Vereinspräsident der Wienerwald Initiativ Region.

Kurzbauer ist verheiratet und hat 6 Kinder. Nach Schul- und landwirtschaftlicher Ausbildung übernahm er die elterliche Landwirtschaft im Jahr 1963. Im zweiten Bildungsweg Ausbildung im Bankenbereich ab 1971. Ab 1982 Geschäftsleiter der Raiffeisenbank Böheimkirchen-Kasten-Pyhra.

Bis zur 22. Legislaturperiode war Johann Kurzbauer Mitglied in folgenden Ausschüssen:

Weiters war er Ersatzmitglied in folgenden Ausschüssen:

Als Bürgermeister verwirklichte Johann Kurzbauer unter anderem in Neulengbach folgende Projekte:
