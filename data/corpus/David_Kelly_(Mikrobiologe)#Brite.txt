David Kelly CMG (* 17. Mai 1944 in Rhondda, Wales; † 17. Juli 2003 in Abingdon, Oxfordshire) war ein britischer Mikrobiologe, Biowaffenexperte und Berater des britischen Verteidigungsministeriums. Er wurde vor allem aufgrund seiner Todesumstände einer größeren Öffentlichkeit bekannt.

Der auf Viren spezialisierte Wissenschaftler forschte über die Abwehr von biologischen und chemischen Kampfstoffen. Von 1991 bis 1994 untersuchte er das russische Biowaffenprogramm. Kelly war als UN-Beauftragter an der ersten Kontrollmission im Irak beteiligt. Zwischen 1994 und 1997 war er insgesamt 37 Mal am Persischen Golf.

Er soll die Hauptquelle (Whistleblower) für einen Bericht der BBC gewesen sein, in dem der britischen Regierung vorgeworfen wurde, Geheimdienst-Berichte über irakische Massenvernichtungswaffen aufgebauscht zu haben; vor allem Alastair Campbell, der Kommunikationsdirektor des Premierministers Tony Blair, soll darauf bestanden haben, in das Dossier gegen den Widerstand des Secret Intelligence Service den Satz irakische ABC-Waffen könnten innerhalb von 45 Minuten gefechtsbereit sein einzubringen. Deshalb wurde Kelly am 15. Juli 2003 von einem Untersuchungsausschuss des britischen Parlaments vernommen. Dabei soll er stark unter Druck gesetzt worden sein. Kelly gab zu, sich mit dem BBC-Reporter Andrew Gilligan getroffen zu haben. Er bestritt jedoch, dass die Informationen zu dem Artikel von ihm stammten.

David Kelly verließ am 17. Juli 2003, zwei Tage nach der Anhörung vor dem Untersuchungsausschuss, um 15 Uhr sein Haus in Abingdon, um spazieren zu gehen. Einen Tag später wurde er ein paar Kilometer entfernt von seinem Wohnsitz mit durchschnittener Ellenarterie tot aufgefunden. Die Arterie war mit seinem Taschenmesser durchtrennt worden, das jedoch keine Fingerabdrücke aufwies. Neben ihm lag eine leere Schachtel mit Schmerztabletten seiner Frau. Die in seinem Körper gefundene Menge des Schmerzmittels Coproxamol entsprach etwa einem Drittel der letalen Dosis.

Nach Angaben der Polizei, deren Untersuchung 'Operation Mason' offiziell bereits begann, bevor Kelly zu seinem Spaziergang aufbrach[1], gab es keinen Hinweis auf Fremdeinwirkung.

Fünf Jahre nach seinem Tod zitierte die Daily Mail eine Freundin Kellys mit der Aussage, er habe niemals jemanden töten können, geschweige denn sich selbst.[2]
Der am 22. Oktober 2010 vom Ministry of Justice veröffentlichte Bericht des Pathologen sowie das ebenfalls veröffentlichte toxikologische Gutachten belegen die These des Selbstmordes.[3][4]
Nach dem Tod von Kelly wurden Rücktrittsforderungen gegenüber Premierminister Tony Blair von Seiten der Opposition und aus den Reihen der eigenen Partei laut. Die Hauptquelle für den umstrittenen Artikel war nach Aussage der BBC vom 20. Juli 2003 David Kelly. Dieser wurde am 6. August in Longworth beigesetzt. An der Beerdigung nahmen unter den etwa 160 Trauergästen auch Lordrichter Brian Hutton, der die Untersuchung zu den näheren Todesumständen leitete, sowie der stellvertretende Premierminister John Prescott teil.

Am 11. August 2003 begann im High Court die richterliche Untersuchung der Todesumstände. Die Ergebnisse wurden im so genannten Hutton-Bericht veröffentlicht.

Der Fernsehsender ARTE sendete im Jahr 2005 unter dem Titel David Kelly - Der Waffeninspekteur eine Fernsehspiel-Dokumentation der Lebensgeschichte.

