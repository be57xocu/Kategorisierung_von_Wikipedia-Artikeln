Herbert Sausgruber (* 24. Juli 1946 in Bregenz) ist ein ehemaliger  österreichischer Politiker (ÖVP), der von 1997 bis 2011 Landeshauptmann des Bundeslands Vorarlberg war. Davor war Sausgruber unter anderem von 1989 bis 1990 Landesrat und in den Jahren von 1990 bis 1997 Landesstatthalter in der Vorarlberger Landesregierung.

Herbert Sausgruber wurde am 24. Juli 1946 als Sohn des Chemikers Kurt Sausgruber und dessen Ehefrau Luise in der Vorarlberger Landeshauptstadt Bregenz geboren. Anschließend wuchs Sausgruber in seinem Elternhaus in der Bodenseegemeinde Höchst auf, wo er auch die Volksschule besuchte. Daran anknüpfend wechselte er in die Unterstufe des Bregenzer Bundesgymnasiums Blumenstraße, wo er in weiterer Folge auch die Oberstufe besuchte und maturierte. Nach der Matura inskribierte Herbert Sausgruber zunächst an der Leopold-Franzens-Universität Innsbruck für die Studien der Rechtswissenschaften und der katholischen Theologie, wobei er zweiteres nach einem Jahr wieder abbrach. Im Jahr 1970 wurde er in Innsbruck zum Doktor der Rechte promoviert.

Direkt im Anschluss an sein Studium leistete Sausgruber seinen Präsenzdienst bei der 6. Jägerbrigade des österreichischen Bundesheers in Absam und Innsbruck ab. In den Jahren 1971 und 1972 absolvierte er daraufhin das Gerichtsjahr am Bezirksgericht Bregenz sowie am Landesgericht Feldkirch. 1972 trat er in den Landesdienst ein, wo er zuerst in den Bezirkshauptmannschaften Bludenz, Feldkirch und Bregenz in unterschiedlichen Funktionen tätig wurde. Bei der BH Bregenz wurde er zuletzt als Leiter des Jugendamtes eingesetzt. Ab 1975 war er beruflich beim Amt der Vorarlberger Landesregierung tätig.

Der Österreichischen Volkspartei gehört Sausgruber bereits seit dem Jahr 1964 an, als er zunächst in der Jungen Volkspartei und anschließend im Arbeitnehmer- und Angestelltenbund tätig wurde. Im Jahr 1975 wurde Herbert Sausgruber erstmals für die ÖVP zum Mitglied der Gemeindevertretung in seiner Heimatgemeinde Höchst gewählt. Von 1978 bis 1986 war er zudem Gemeinderat für Finanzen in Höchst. Bei der Landtagswahl in Vorarlberg 1979 wurde er erstmals als Abgeordneter des Vorarlberger Landtags gewählt, wobei seine erste Landtagssitzung als Abgeordneter am 20. November 1979 stattfand. Als Abgeordneter folgte Sausgruber dem in die Landesregierung berufenen Konrad Blank auf dessen Platz nach. In weiterer Folge war er von 1981 bis 1989 Klubobmann der ÖVP-Fraktion im Landtag. Am 18. Oktober 1986 wurde er auch zum Landesparteiobmann der ÖVP Vorarlberg gewählt.

Drei Jahre später, am 23. Oktober 1989, wurde Sausgruber nach der Landtagswahl in Vorarlberg 1989 von Landeshauptmann Martin Purtscher als Landesrat für Inneres, Verkehrspolitik, Verkehrsrecht und Wohnbauförderung in die Vorarlberger Landesregierung berufen. Ein Jahr darauf, am 9. Mai 1990 übernahm Sausgruber von seinem Parteikollegen Siegfried Gasser, der in das Amt des Bregenzer Bürgermeisters wechselte, den Posten des Landesstatthalters, wie der Landeshauptmann-Stellvertreter in Vorarlberg genannt wird. Als solcher unterstanden ihm die Ressorts Innere Angelegenheiten, Gesetzgebung, Feuerpolizei, Hilfs- und Rettungswesen, Katastrophenbekämpfung und Wohnbauförderung, ab 1994 auch Finanzangelegenheiten, Vermögensverwaltung, Gebarungskontrolle. Schließlich wählten die Abgeordneten des Vorarlberger Landtags ihn am 2. April 1997 als Nachfolger von Martin Purtscher zum vierten Landeshauptmann Vorarlbergs nach dem Zweiten Weltkrieg.

Am 7. Oktober 2011 gab Sausgruber bekannt, dass er plane, sein Amt als Landeshauptmann Anfang Dezember 2011 zurückzulegen.[2] In einer Sondersitzung des Vorarlberger Landtags am 7. Dezember 2011 wurde Sausgruber schließlich feierlich verabschiedet und zugleich sein Nachfolger, der bisherige Landesstatthalter Markus Wallner, gewählt.

Herbert Sausgruber wurde 2005 die Verdienstmedaille des Landes Baden-Württemberg verliehen.[3] Zudem ist Sausgruber kraft seines Amtes als Landeshauptmann Träger des Ehrenzeichens des Landes Vorarlberg in Gold seit 1997 und Ehrenbürger seiner Heimatgemeinde Höchst seit 2004. 2008 wurde er zum Ehrensenator der Universität Innsbruck ernannt.[4] 2012 erhielt er das Große Ehrenzeichen des Landes Oberösterreich sowie das Große Goldene Ehrenzeichen für Verdienste um das Land Wien mit dem Stern.

Herbert Sausgruber ist verheiratet mit Ilga Sausgruber und hat zwei Söhne sowie eine Tochter. Er wohnt in Höchst und ist römisch-katholisch.

Sausgruber ist Mitglied der Katholischen Mittelschulverbindung K.M.V. Kustersberg Bregenz im MKV. Er ist seit Studientagen Urmitglied der katholischen Studentenverbindung A.K.V. Tirolia Innsbruck im ÖKV.

Monarchie: Sebastian Ritter von Froschauer |
Anton Jussel |
Karl Graf von Belrupt-Tissac |
Adolf Rhomberg

Erste Republik:
Otto Ender |
Ferdinand Redler |
Otto Ender |
Ernst Winsauer

Deutsches Reich (Gauleiter):
Anton Plankensteiner |
Franz Hofer

Zweite Republik:
Ulrich Ilg |
Herbert Keßler |
Martin Purtscher |
Herbert Sausgruber |
Markus Wallner

Ferdinand Redler (1923–1930) |
Martin Schreiber (1930–1931) |
Ferdinand Redler (1931–1934) |
Alfons Troll (1934–1938) |
Martin Schreiber (1945–1954) |
Ernst Kolb (1954–1959) |
Eduard Ulmer (1959–1963) |
Gerold Ratz (1964–1973) |
Martin Müller (1973–1974) |
Rudolf Mandl (1974–1984) |
Siegfried Gasser (1984–1990) |
Herbert Sausgruber (1990–1997) |
Hans-Peter Bischof (1997–1999) |
Hubert Gorbach (1999–2003) |
Dieter Egger (2003–2004) |
Hans-Peter Bischof (2004–2006) |
Markus Wallner (2006–2011) |
Karlheinz Rüdisser (seit 2011)
