Alfred Auerbach (* 9. Juni 1873 in Stuttgart; † 31. Januar 1954 ebenda) war ein deutscher Schauspieler und Schriftsteller.

Alfred Auerbach wurde als Sohn des Kaufmanns Benjamin Auerbach in Stuttgart geboren und besuchte dort das Realgymnasium. Mütterlicherseits war er mit Berthold Auerbach verwandt.

Um 1883 zog die Familie nach Frankfurt am Main. Auerbach wurde nach seiner Schulzeit gezwungen, eine kaufmännische Lehre zu absolvieren, um das elterliche Geschäft zu übernehmen. Ab 1895 konnte Auerbach dem ungeliebten Beruf entfliehen und studierte an Dr. Hoch’s Konservatorium. 

Nach erfolgreich abgelegter Prüfung im dramatischen Fach engagierte ihn 1898 Emil Claar für das Schauspielhaus Frankfurt. Den Durchbruch schaffte Auerbach 1902, als er nach dem Tod von Klemens Grunwald dessen Charakterfach übernahm. Bis 1923 war er Mitglied des Schauspielhauses in Frankfurt.

Von 1906 bis 1933 leitete er die Theaterabteilung des Dr. Hoch’s Konservatorium. 1908 heiratete er und bekam eine Tochter. Im Juni 1940 emigrierte er in die USA und arbeitete in Chicago für einen deutschsprachigen Radiosender. Vergeblich versuchte er in dieser Zeit, auch in Hollywood Fuß zu fassen.

Nach seiner Rückkehr nach Deutschland ließ sich Auerbach in Stuttgart nieder und verfasste seine Memoiren Ein Schwabe studiert Amerika. Alfred Auerbach starb am 31. Januar 1954 in Stuttgart.
