Elfriede Jelinek (geboren am 20. Oktober 1946 in Mürzzuschlag) ist eine österreichische Schriftstellerin, die in Wien und München lebt. Im Jahr 2004 erhielt sie den Literaturnobelpreis für „den musikalischen Fluss von Stimmen und Gegenstimmen in Romanen und Dramen, die mit einzigartiger sprachlicher Leidenschaft die Absurdität und zwingende Macht der sozialen Klischees enthüllen“.[1]
Elfriede Jelinek schreibt gegen Missstände im öffentlichen, politischen, aber auch im privaten Leben der österreichischen Gesellschaft. Dabei benutzt sie einen sarkastischen, provokanten Stil, der von ihren Gegnern („Nestbeschmutzer“-Diskussion), aber auch von ihr selbst mitunter als obszön, blasphemisch, vulgär oder höhnisch beschrieben wird.

Wurde Jelinek in den ersten Jahren verbreitet als Provokateurin angesehen, werden ihre Arbeiten in letzter Zeit zunehmend als zeitpolitisch höchst sensible Sprachkunst wahrgenommen.

Elfriede Jelinek wurde am 20. Oktober 1946 in Mürzzuschlag geboren. Ihre Mutter Olga, geb. Buchner, stammte aus dem Wiener Großbürgertum und ernährte die Familie längere Zeit durch ihre Tätigkeit als Buchhalterin. Ihr Vater Friedrich Jelinek war Chemiker, Absolvent der TU Wien[2] und jüdisch-tschechischer Abstammung. Sein „kriegsdienlicher“ Beruf bewahrte ihn vor Verfolgung unter dem NS-Regime; ihm wurde ein Arbeitsplatz in der Rüstungsindustrie zugewiesen. Friedrich Jelinek erkrankte während der 50er Jahre psychisch; während der sechziger Jahre lebte er in zunehmend verwirrtem Zustand zu Hause. Er starb 1969 in einer psychiatrischen Klinik in völliger geistiger Umnachtung.

Um Jelineks Erziehung kümmerte sich die Mutter. Jelinek kam in einen katholischen Kindergarten und danach in eine Klosterschule, die sie als äußerst restriktiv empfand (Essay „In die Schule gehen ist wie in den Tod gehen“). Ihr auffälliger Bewegungsdrang brachte sie auf Anraten der Nonnen in die Kinderpsychiatrie, auf die heilpädagogische Abteilung der Kinderklinik der Wiener Universität, die von Hans Asperger geleitet wurde, obwohl ihr Verhalten aus medizinischer Sicht im Bereich der Norm blieb. Abgesehen davon plante die Mutter die Karriere ihrer Tochter als musikalisches Wunderkind, und Jelinek erhielt bereits in der Volksschule Klavier-, Gitarren-, Flöten-, Geigen- und Bratschenunterricht. Im Alter von 13 Jahren wurde sie ins Konservatorium der Stadt Wien aufgenommen und studierte dort Orgel, Klavier, Blockflöte und später auch Komposition. Parallel dazu absolvierte sie die Mittelschulausbildung am Wirtschaftskundlichen Realgymnasium Wien-Feldgasse.

In der Tradition der Wiener Gruppe führte Jelinek für sich zunächst die Kleinschreibung ein, die sie aber später wieder aufgab.

Nach der Matura erfolgte der erste psychische Zusammenbruch. Sie belegte dennoch für einige Semester Kunstgeschichte und Theaterwissenschaft an der Universität Wien, bis sie 1967, durch Angstzustände gezwungen, das Studium abbrach und ein Jahr lang zu Hause in völliger Isolation verbrachte. Während dieser Zeit begann sie zu schreiben; ihre ersten Gedichte wurden in Zeitschriften und kleinen Verlagen gedruckt. Im gleichen Jahr erschien ihr Gedichtband Lisas Schatten. Der erste Roman bukolit (1968) blieb allerdings bis 1979 unveröffentlicht. Nach dem Tod ihres Vaters begann sie sich zu erholen; sie engagierte sich im Umfeld der 68er-Bewegung und lebte für einige Monate in einer linken Wohngemeinschaft u. a. mit Robert Schindel und Leander Kaiser.

1971 legte sie die Orgelprüfung am Konservatorium bei Leopold Marksteiner ab. Maßgeblich für ihr weiteres literarisches Schaffen war in dieser Zeit die Auseinandersetzung mit den Theorien von Roland Barthes, welche sie in dem Essay Die endlose Unschuldigkeit[3] verarbeitete. 1972 lebte sie mit Gert Loschütz in Berlin, kehrte im Jahr darauf aber wieder nach Wien zurück. 1974 trat sie der KPÖ bei und engagierte sich beim Wahlkampf sowie bei Kulturveranstaltungen, wie z. B. im Rahmen der Autorenlesungen unter dem Titel Linkes Wort beim Volksstimmefest.[4] Im selben Jahr heiratete sie Gottfried Hüngsberg, der zu dieser Zeit Filmmusik für Rainer Werner Fassbinder schrieb und seit Mitte der 1970er Jahre in München als Informatiker tätig ist.

Seit der Heirat lebte Elfriede Jelinek abwechselnd in Wien und München. Der literarische Durchbruch gelang ihr 1975 mit dem Roman die liebhaberinnen, der marxistisch-feministischen Karikatur eines Heimatromans. Vor allem in den 70ern entstanden zahlreiche Hörspiele; Anfang der 80er erschien Die Ausgesperrten als Hörspiel, Roman und schließlich auch als Film mit Paulus Manker (Vorbild ist ein realer Wiener Mordfall kurz vor Weihnachten 1965, der anlässlich des Urteils um den 10. Mai 1966 von den Medien ausführlich kommentiert wurde).

Der erste große Skandal um Jelinek wurde 1985 durch die Uraufführung von Burgtheater heraufbeschworen. Das Drama setzt sich mit der mangelhaften NS-Vergangenheitsbewältigung in Österreich auseinander, mit der Vergangenheit der Schauspielerin Paula Wessely im Mittelpunkt. In der öffentlichen Wahrnehmung erschien der Text jedoch reduziert auf persönliche Anspielungen auf damalige prominente Mitläufer. Jelineks Ruf als Nestbeschmutzerin begann sich zu festigen. 1983 erschien der Roman Die Klavierspielerin. In den Rezensionen überwog jedoch die biografische Deutung; die Auseinandersetzung mit dem Text trat in den Hintergrund.

1989 folgte mit Lust das nächste aufsehenerregende und zugleich Jelineks meistverkauftes Werk. Jelineks Auseinandersetzung mit den patriarchalischen Machtverhältnissen auch im Bereich der Sexualität wurde im Vorfeld als „weiblicher Porno“ skandalisiert.

Jelinek setzte sich gemeinsam mit Erika Pluhar, Ernest Bornemann und weiteren Intellektuellen für den wegen Mordes verurteilten „Häfenliteraten“ Jack Unterweger ein, der im Jahre 1990 entlassen wurde und – wieder in Freiheit – neun weitere Morde beging.

1991 trat Jelinek, mit den beiden Vorsitzenden Susanne Sohn und Walter Silbermayer, wieder aus der KPÖ aus.[4] Gleichzeitig steht sie bis heute in kooperativem Zusammenhang bspw. mit der pluralistisch-marxistischen Wissenschaftszeitschrift „Das Argument“, die von Wolfgang Fritz Haug u. a. herausgegeben wird.

Nachdem das Theaterstück Raststätte eine ähnliche Rezeption wie Lust erfuhr und nach persönlichen Angriffen auf die Autorin auf Wahlplakaten der Wiener FPÖ 1995 gab Jelinek ihren Rückzug aus der österreichischen Öffentlichkeit bekannt und erließ ein Aufführungsverbot ihrer Stücke für Österreich.[5]
So wurde Stecken, Stab und Stangl, das vier 1995 im Burgenland begangene Morde an dort lebenden Roma thematisiert, in Hamburg von Thirza Bruncken inszeniert und am 12. April 1996 im Malersaal des Deutschen Schauspielhauses uraufgeführt.[6] Die Rückkehr Elfriede Jelineks nach Wien[7] wurde am 23. Januar 1997 im Burgtheater mit der Premiere dieses Stücks unter der Regie von George Tabori begangen.[8]  1998 folgte dort dann die Uraufführung der ganze sechs Stunden dauernden Kurzfassung von Ein Sportstück in der Inszenierung von Einar Schleef.[9] Die Langfassung des Stücks unter Mitwirkung der Autorin feierte am 14. März 1998 ebenda Premiere.[10]
Auch nach dem neuerlichen Aufführungsverbot, das Jelinek im Jahr 2000 anlässlich der schwarz-blauen Regierungsbildung in Österreich erließ, nahm sie konkret auf die aktuelle Tagespolitik Bezug; bei einer regierungskritischen Donnerstagsdemonstration im Jahr 2000 wurde auf dem Wiener Ballhausplatz Das Lebewohl. Ein Haider-Monolog mit dem Schauspieler Martin Wuttke uraufgeführt. Im selben Jahr entstand im Kontext der Schlingensief-Aktion Bitte liebt Österreich, als deren „Schirmherrin“ sie u. a. gemeinsam mit Daniel Cohn-Bendit und Gregor Gysi fungierte,[11] die Kaspertheater–Montage „Ich liebe Österreich“,[12] die den Umgang mit Asylbewerbern in Österreich kritisiert.[13]
2003 schließlich kam am Akademietheater des Burgtheaters Das Werk in der Regie von Nicolas Stemann zur Uraufführung. Die Inszenierung wurde zum Berliner Theatertreffen eingeladen und gewann den Mülheimer Dramatikerpreis. Im selben Jahr inszenierte Christoph Schlingensief am Burgtheater Bambiland. Ebenfalls 2003 hatte Olga Neuwirths Musiktheater Lost Highway Premiere, dessen Libretto von Elfriede Jelinek stammt.

2004 wurde in Wien das Elfriede Jelinek-Forschungszentrum gegründet, eine Dokumentations-, Informations- und Kommunikationsstelle zur Autorin, die ihren Sitz am Institut für Germanistik der Universität Wien hat. In diesem Jahr erhielt Jelinek auch den Nobelpreis für Literatur.

2005 fand im Wiener Burgtheater die Uraufführung von Babel statt, einer monumentalen Meditation über den Irakkrieg und den Folterskandal in Abu Ghraib, in der Regie von Nicolas Stemann, der im Oktober 2006 auch Jelineks RAF-Drama Ulrike Maria Stuart und im Frühjahr 2009 ihre Wirtschaftskomödie Die Kontrakte des Kaufmanns inszenierte.

In einem Interview mit dem Magazine littéraire (2007) aus Anlass der wegen ihrer Drastik umstrittenen französischen Übersetzung von Die Kinder der Toten wiederholte Jelinek die Liste ihrer großen Themen: eine bedrückende Kindheit, ihre Polemik gegen „Natur“ und „Unschuld“, ihren Hass auf das verdrängte Nazi-Erbe des Landes. Sie meinte, ein großer Teil der Literatur Österreichs kreise um das „schwarze Loch“ Hitler.

Vom Frühjahr 2007 bis zum Frühjahr 2008 veröffentlichte sie auf ihrer Website nacheinander die Kapitel ihres „Privatromans“ Neid.[14] Diesen Roman, der nicht als Buch erscheinen, sondern ein reiner Online-Text bleiben soll, stellte Jelinek im Mai 2008 fertig. In ausgedruckter Form würde er rund 900 Seiten umfassen. Jelinek setzt damit ihr „Todsündenprojekt“ fort, das sie 1989 mit Lust begonnen und 2000 mit Gier ergänzt hatte.

Ebenfalls 2008 erschien das Theaterstück Rechnitz (Der Würgeengel), das unter der Regie von Jossi Wieler am 28. November bei den Münchner Kammerspielen uraufgeführt wurde.[15]
Ende 2009 protestierte Jelinek gegen die mittlerweile zurückgenommene Ehrung des Holocaust-Leugners Walter Lüftl durch die Technische Universität Wien mittels eines „Goldenen Ingenieurdiploms“ für „besondere wissenschaftliche Verdienste“ und das „hervorragende berufliche Wirken“ des Mannes.[16] In diesem Zusammenhang gab sie auch etwas von ihrer Familiengeschichte preis, nämlich die Anfeindungen und Erschwernisse durch Antisemiten, unter denen ihr Vater als „Halbjude“ (nach Globkes Definition) sein Ingenieursexamen abgelegt hat; weiter, dass er bei Semperit eine Art wissenschaftliche Zwangsarbeit leisten musste und dass diese Tatsache ihn sein Leben lang belastet hat.

2012 wurde im Schauspielhaus der Münchner Kammerspiele ihr Werk Die Straße. Die Stadt. Der Überfall in der Regie von Johan Simons uraufgeführt, ein Auftragswerk zum 100. Gründungsjahr der Kammerspiele. Es behandelt den Mythos der Münchener Maximilianstraße.[17]
Am 10. Dezember 2013, dem Tag der Menschenrechte, ist Jelinek eine der fünf Literaturnobelpreisträger von 560 Schriftstellern, die im Rahmen der Kampagne Stop Watching Us mit einem internationalen Aufruf gegen die systematische Überwachung im Internet durch Geheimdienste protestierten.[18]
Im November 2015 wollte der polnische Kulturminister Piotr Gliński eine Aufführung von Jelineks Der Tod und das Mädchen am Teatr Polski we Wrocławiu, dem Polnischen Theater in Breslau, verhindern. Grund dafür waren angebliche sexuelle Handlungen auf der Bühne, die Inszenierung verstoße gegen „Prinzipien des gesellschaftlichen Zusammenlebens“. Auch der Sprecher des Erzbistums Breslau protestierte gegen den Auftritt „ausländischer Pornodarsteller“ (für das Stück waren tschechische Schauspieler engagiert worden). Die Premiere fand dennoch statt, einige Demonstranten versuchten, den Zutritt zum Theater zu blockieren. Der Direktor des Theaters, Krzysztof Mieszkowski, forderte den Rücktritt des Kulturministers und warf ihm einen präzedenzlosen Zensurversuch vor. Den Job verlor jedoch der Theaterdirektor, der im August 2016 abgelöst wurde, und suspendiert wurde eine Moderatorin des staatliche Fernsehsender TVP, nachdem sie dem Minister kritische Fragen zur Causa gestellt hatte.[19]
Am Abend, an dem Donald Trump zum Präsidenten der Vereinigten Staaten gewählt wurde, begann Elfriede Jelinek, ihr Stück Am Königsweg zu schreiben. Vor Trumps Amtseinführung hatte sie eine erste Fassung des Textes abgeschlossen. Der Bayerische Rundfunk produzierte die Hörspielfassung von Am Königsweg als deutsche Erstinszenierung in zwei Varianten: Eine Fassung in drei Teilen, die den ungekürzten Text enthält, und eine eigenständige, das gesamte Material komprimierende Kurzfassung. 

Französisch:

Englisch:

Rumänisch:

Russisch:

Hörspiel

Film

Nobelpreis

Die Liebhaberinnen (1975) • Die Klavierspielerin (1983) • Lust (1989) • Die Kinder der Toten (1995) • Gier (2000) • Neid (2007/2008)

Was geschah, nachdem Nora ihren Mann verlassen hatte oder Stützen der Gesellschaft (1979) • Totenauberg. Ein Stück (1991) • Ein Sportstück (1998) •  Der Tod und das Mädchen (2002/2003) • Ulrike Maria Stuart. Königinnendrama (2006) • Rein Gold. Ein Bühnenessay (2000) • Bambiland (2003) • Winterreise. Ein Theaterstück (2011) • Die Schutzbefohlenen (2014) • Wut (2016)

Malina (1991)

Sportchor (2006) • Die Schutzbefohlenen (2014)

Theodor Mommsen (1902) |
Rudolf Eucken (1908) |
Paul Heyse (1910) |
Gerhart Hauptmann (1912) |
Carl Spitteler (1919) |
Thomas Mann (1929) |
Hermann Hesse (1946) |
Nelly Sachs (1966) |
Heinrich Böll (1972) |
Elias Canetti (1981) |
Günter Grass (1999) |
Elfriede Jelinek (2004) |
Herta Müller (2009)

Prudhomme (1901) |
Mommsen (1902) |
Bjørnson (1903) |
F. Mistral/Echegaray (1904) |
Sienkiewicz (1905) |
Carducci (1906) |
Kipling (1907) |
Eucken (1908) |
Lagerlöf (1909) |
Heyse (1910) |
Maeterlinck (1911) |
Hauptmann (1912) |
Tagore (1913) |
nicht verliehen (1914) |
Rolland (1915) |
Heidenstam (1916) |
Gjellerup/Pontoppidan (1917) |
nicht verliehen (1918) |
Spitteler (1919) |
Hamsun (1920) |
France (1921) |
Benavente (1922) |
Yeats (1923) |
Reymont (1924) |
Shaw (1925) |
Deledda (1926) |
Bergson (1927) |
Undset (1928) |
Mann (1929) |
Lewis (1930) |
Karlfeldt (1931) |
Galsworthy (1932) |
Bunin (1933) |
Pirandello (1934) |
nicht verliehen (1935) |
O’Neill (1936) |
Martin du Gard (1937) |
Buck (1938) |
Sillanpää (1939) |
nicht verliehen (1940–1943) |
Jensen (1944) |
G. Mistral (1945) |
Hesse (1946) |
Gide (1947) |
Eliot (1948) |
Faulkner (1949) |
Russell (1950) |
Lagerkvist (1951) |
Mauriac (1952) |
Churchill (1953) |
Hemingway (1954) |
Laxness (1955) |
Jiménez (1956) |
Camus (1957) |
Pasternak (1958) |
Quasimodo (1959) |
Perse (1960) |
Andrić (1961) |
Steinbeck (1962) |
Seferis (1963) |
Sartre (1964) |
Scholochow (1965) |
Agnon/Sachs (1966) |
Asturias (1967) |
Kawabata (1968) |
Beckett (1969) |
Solschenizyn (1970) |
Neruda (1971) |
Böll (1972) |
White (1973) |
Johnson/Martinson (1974) |
Montale (1975) |
Bellow (1976) |
Aleixandre (1977) |
Singer (1978) |
Elytis (1979) |
Miłosz (1980) |
Canetti (1981) |
García Márquez (1982) |
Golding (1983) |
Seifert (1984) |
Simon (1985) |
Soyinka (1986) |
Brodsky (1987) |
Mahfuz (1988) |
Cela (1989) |
Paz (1990) |
Gordimer (1991) |
Walcott (1992) |
Morrison (1993) |
Ōe (1994) |
Heaney (1995) |
Szymborska (1996) |
Fo (1997) |
Saramago (1998) |
Grass (1999) |
Gao (2000) |
Naipaul (2001) |
Kertész (2002) |
Coetzee (2003) |
Jelinek (2004) |
Pinter (2005) |
Pamuk (2006) |
Lessing (2007) |
Le Clézio (2008) |
Müller (2009) |
Vargas Llosa (2010) |
Tranströmer (2011) |
Mo (2012) |
Munro (2013) |
Modiano (2014) |
Alexijewitsch (2015) |
Dylan (2016) |
Ishiguro (2017)
