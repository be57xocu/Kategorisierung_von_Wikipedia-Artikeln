Andrea Maria Dusl (* 12. August 1961 in Wien) ist eine österreichisch-schwedische Filmregisseurin, Autorin und Zeichnerin.

Andrea Maria Dusl wurde in eine österreichisch-schwedische Architekten- und Unternehmerfamilie geboren. Sie ist die Schwester von Peter Dusl und Großnichte von Theodor Scheimpflug. Sie wuchs in Schweden, Bad Aussee und Wien auf. 1980 maturierte sie im Wiener Gymnasium Wasagasse.

Von 1981 bis 1985 studierte sie bei Lois Egg an der Akademie der bildenden Künste in Wien in der Meisterklasse für Bühnenbild, wo ihr 1985 der Grad Magistra Artium verliehen wurde. 1993 bis 1997 folgte ein Studium der Medizin an der Universität Wien.

1982 bis 1989 Engagements an Burgtheater, Akademietheater unter George Tabori, Theater an der Wien, Theater in der Josefstadt und der Wiener Staatsoper.

Seit 1985 schreibt und zeichnet sie für österreichische Publikationen wie FORVM (Erste Teile der Endloszeichnung Das Unendliche Panorama), profil, News, Format, Der Standard, Kurier und Salzburger Nachrichten. Ab 1996 erscheinen die wöchentlichen Kolumnen Comandantina Dusilova und Fragen Sie Frau Andrea in der Wiener Stadtzeitung Falter.[1]
1989 bis 2006 reiste sie für Recherchen zu ihrem Film Blue Moon durch Tschechien, die Slowakei, Polen, die Ukraine, Russland und Frankreich.

Zahlreiche Vorträge, Lesungen, Präsentationen, Podiumsdiskussionen und Moderationen auf österreichischen und internationalen Konferenzen, Symposien zu den Themen Film, Drehbuch, politische und kulturelle Kommunikation ergänzen ihre filmische und literarische Arbeit.

Dusl moderierte 2006 die monatliche Diskursveranstaltung Redezeit im Wiener Rabenhof Theater.[2]
Seit 2003 ist Dusl Vorstandsmitglied des Verbands Filmregie Österreich. 2007 war sie dessen Obfrau. Von 2005 bis 2009 war Dusl für den Bereich Regie Mitglied der Auswahlkommission des Österreichischen Filminstitutes.

2009 begann Dusl bei Ernst Strouhal an der Universität für angewandte Kunst in Wien ein Dissertationsstudium der Philosophie. Im Winter 2013 reichte sie ihre Dissertation ein, im Jänner 2014 promovierte sie mit ausgezeichnetem Erfolg zur Doktorin der Philosophie. Der Titel der Dissertation war: Aufnahme und Auswahl. Strategien fotografischer Praxis. In dieser kulturwissenschaftlichen Arbeit beschäftigt sich Dusl am Beispiel von sieben eigens für die Arbeit geführten Filminterviews - interviewt wurden die Fotografen und Fotografinnen Lukas Beck, Hertha Hurnaus, Manfred Klimek, Erich Lessing, Ingo Pertramer, Lisl Ponger und Peter Rigaud - mit dem holistischen Prozess von Aufnahme und Auswahl in der professionell-künstlerischen Fotografie. Dusl interpoliert aus dem hermeneutisch ausgewerteten Material sechs Strategien fotografischer Praxis: „Das Schütteln des Kaleidoskops“, „Die Jagd“, „Der Fang“, „Die Inszenierung“, „Serendipity“ und „Das Glöckchen des Kairos“.
