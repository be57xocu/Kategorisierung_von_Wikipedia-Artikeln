Rudolf Mellinghoff (* 25. November 1954 in Langenfeld, Rheinland) ist ein deutscher Rechtswissenschaftler. Er war 2001 bis 2011 Richter des Bundesverfassungsgerichts. Seit dem 31. Oktober 2011 ist er Präsident des Bundesfinanzhofes.

Mellinghoff studierte von 1975 bis 1980 Rechtswissenschaften an der Universität Münster und legte nach seiner Referendarzeit 1984 das 2. juristische Staatsexamen ab. Danach war er zunächst wissenschaftlicher Mitarbeiter an der Universität Heidelberg, ehe er 1987 als Richter auf Probe an das Finanzgericht Düsseldorf berufen wurde. Noch im Jahr 1987 wurde er als wissenschaftlicher Mitarbeiter an das Bundesverfassungsgericht abgeordnet. Während der Zeit der Abordnung wurde er 1989 zum Richter am Finanzgericht ernannt.

Nach Beendigung seiner Tätigkeit als wissenschaftlicher Mitarbeiter wechselte Mellinghoff im Juli 1991 als Referatsleiter in das Justizministerium des Landes Mecklenburg-Vorpommern, wo er für den Aufbau der öffentlich-rechtlichen Gerichtsbarkeit zuständig war.

Im Juli 1992 wurde er zum Richter am Finanzgericht Mecklenburg-Vorpommern ernannt, im Jahr 1996 zum Vorsitzenden Richter am Finanzgericht. In der Zeit von 1992 bis 1996 war er zudem im zweiten Hauptamt Richter am Oberverwaltungsgericht, in den Jahren 1995–1996 überdies Richter am Landesverfassungsgericht.

Vom Wintersemester 1992/1993 an bis zum Sommersemester 1997 nahm Mellinghoff einen Lehrauftrag an der  Rechts- und Staatswissenschaftlichen Fakultät der Universität Greifswald wahr.

Zum 1. Januar 1997 wurde Mellinghoff zum Richter am Bundesfinanzhof berufen. Im Januar 2001 wurde er als Nachfolger von Klaus Winter Richter am zweiten Senat des Bundesverfassungsgerichts, dem er bis Oktober 2011 angehörte.

Seit Sommersemester 2001 ist er Lehrbeauftragter an der Juristischen Fakultät der Eberhard Karls Universität Tübingen. 

Am 17. November 2006 wurde ihm die Ehrendoktorwürde der Ernst-Moritz-Arndt-Universität Greifswald verliehen.

Im November 2007 wurde Mellinghoff zum Honorarprofessor an der Eberhard Karls Universität Tübingen bestellt.

Mellinghoff ist seit 2009 Vorsitzender des Präsidiums der Deutschen Sektion der Internationalen Juristenkommission. Außerdem ist er Vorsitzender des wissenschaftlichen Beirats der Berliner Steuergespräche e.V. Seit 2011 ist er außerdem Vorsitzender der Deutschen Steuerjuristischen Gesellschaft.[1]
Zusammen mit Paul Kirchhof und Hartmut Söhn gibt er einen Großkommentar zum Einkommensteuergesetz heraus.

Am 31. Oktober 2011 verlieh ihm der Bundespräsident das Große Verdienstkreuz mit Stern und Schulterband des Verdienstordens der Bundesrepublik Deutschland und überreichte ihm gleichzeitig die Ernennungsurkunde zum Präsidenten des Bundesfinanzhofs.

Am 31. Oktober 2011 schied Mellinghoff vor Ablauf seiner regulären Amtszeit aus dem Bundesverfassungsgericht aus, um Präsident des Bundesfinanzhofs zu werden. Während seiner Amtszeit beim Bundesverfassungsgericht war er unter anderem bei folgenden Verfahren Berichterstatter[2]:

Heinrich Schmittmann |
Hans Müller |
Ludwig Heßdörfer |
Wolfgang Mersmann |
Hugo von Wallis |
Heinrich List |
Franz Klein |
Klaus Offerhaus |
Iris Ebling |
Wolfgang Spindler |
Rudolf Mellinghoff
