Alfred Mittermeier (* 1964 in Dorfen) ist ein deutscher Schauspieler und Kabarettist.

Mittermeier studierte bis 1988 Betriebswirtschaft an der Berufsakademie Heidenheim. Nach Kurzprogrammen als Kabarettist ab 1994 war er Schauspieler in komischen Rollen, gefolgt von einer Tätigkeit im Management einer Künstleragentur.  
Seit 2004 ist er mit seinen Programmen in Deutschland, Österreich und in der Schweiz auf Tour.
Alfred Mittermeier ist der ältere Bruder von Michael Mittermeier.
