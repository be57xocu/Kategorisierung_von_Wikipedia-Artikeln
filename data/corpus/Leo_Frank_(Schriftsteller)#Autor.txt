Leo Frank (* 29. Juni 1925 in Wien; † 19. März 2004 in Bad Ischl; gebürtig/eigentlich Leo Maier) war ein österreichischer Kriminalautor.

Leo Frank begann seine berufliche Laufbahn unter seinem Geburtsnamen Leo Maier als Probegendarm in Braunau (Oberösterreich). 1948 wechselte er zur Staatspolizei nach Linz. Im Jahr 1961 wurde er für einige Monate nach Jerusalem abgesandt, um als  offizieller Berichterstatter  den Eichmann-Prozess zu beobachten. Zurück in Linz war er wieder als Kriminalbeamter tätig. In dieser Funktion wurde er 1967 in eine Informationsaffäre  um den Voest-Konzern verwickelt. Man verdächtigte ihn, vertrauliches Material an ausländische Nachrichtendienste geliefert zu haben, und er geriet unter dem Namen „James Bond von Linz“ in die Medien. Es folgte eine Strafversetzung nach Wien, wo er nach wenigen Monaten ein Angebot zur Versetzung nach Zypern annahm.

Zwischen 1967 und 1974 war Leo Maier Kripo-Chef der österreichischen UN-Truppe in Nikosia. In Zypern begann er seine ersten Kriminalromane zu schreiben – Leo Frank war geboren. Doch dauerte es noch einige Jahre, bis 1976 sein erster Roman Die Sprech-Puppe publiziert wurde.

1974 kehrte er – in der Voest-Affäre inzwischen voll rehabilitiert – nach Linz zurück. Er leitete verschiedene Referate (Gewaltreferat, Sittenreferat, Mordreferat), bevor er 1980 zum obersten Krimalisten der Stadt ernannt wurde.

Mit 59 Jahren ging er in Pension und zog in seine Wahlheimat Bad Ischl, wo er 2004 verstarb. Leo Frank-Maier (wie er sich auch nannte) war verheiratet und hatte zwei Kinder.

Die vielfältigen beruflichen Erfahrungen und Kenntnisse Franks fließen in seine Kriminalromane ein, die sich durch Detailreichtum und einen trockenen Humor auszeichnen. Auch seine  Auslandserfahrungen finden ihren literarischen Niederschlag, so in seinem Zypern-Krimi von 1977 Die Zikaden. Sein Israel-Aufenthalt schlägt sich in seinem 1979 veröffentlichten Roman Der programmierte Agent nieder, in dem ein Journalist zum Eichmann-Prozess geschickt wird und sein Gewissen befragt.

Trotz seines schriftstellerischen Erfolgs sah sich Leo Frank nicht als Literat und schloss sich keiner Autorenvereinigung an. 1993 erschien seine Autobiographie Geständnis. Das Leben eines Polizisten, ein Jahr später, 1994, wurde sein letzter Roman Gold und Tribadie veröffentlicht. In den letzten zehn Jahren seines Lebens hat er keine neuen Bücher mehr publiziert.

Leo Frank schrieb insgesamt 15 Bücher, von denen einige Bestseller wurden. Sieben Romane wurden als Tatort und Eurocops verfilmt, zu den bekanntesten zählen Nachtstreife (ORF, Erstausstrahlung 1985) und Das Archiv (ORF, Erstausstrahlung 1986).
