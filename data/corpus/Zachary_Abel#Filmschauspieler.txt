Zachary Burr Abel (* 4. September 1980 in Middletown, Indiana) ist ein US-amerikanischer Schauspieler.

Zachary Abel wurde im September 1980 in Middletown im US-Bundesstaat Indiana geboren und wuchs dort auch auf. Danach studierte er Finance und International Business an der Washington University in St. Louis.[1]
Er begann seine Karriere 2007 in einer Episode von der beliebten Krimiserie CSI: Vegas. Danach erhielt er eine Hauptrolle in der Webserie My Alibi als Jonah Madigen. 2009 war er als Max Enriquez in einem mehrteiligen Handlungsbogen von The Secret Life of the American Teenager zu sehen. Danach folgten Gastauftritte in The Big Bang Theory und 90210. Außerdem spielte er 2009 in dem Horrorfilm Forget Me Not mit.

Sein Durchbruch als Schauspieler gelang ihm mit der Rolle des Carter Anderson in der ABC-Family-Jugendserie Make It or Break It, die er seit 2009 verkörperte. Dafür bekam er bei den Teen Choice Awards 2010 eine Nominierung als Bester männliche Schauspieler in einer Sommerfernsehserie, die er jedoch an Ian Harding verlor. Im Jahr 2011 absolvierte er Gastauftritte in The Secret Circle und Awkward – Mein sogenanntes Leben.

Im April 2012 bekam Abel die männliche Hauptrolle des Mitchell Taylor in der Pilotfolge Shelter des Senders The CW, die von J. J. Abrams und Mark Schwahn erdacht wurde.[2]