Stanley Baldwin, 1. Earl Baldwin of Bewdley, KG, PC, (* 3. August 1867 in Bewdley, Worcestershire; † 14. Dezember 1947 in Astley Hall bei Stourport-on-Severn) war einer der einflussreichsten konservativen Politiker im Großbritannien der Zwischenkriegszeit. In den Jahren von 1923 bis 1937 bekleidete er unter anderem dreimal das Amt des Premierministers.

Im Jahr 1908 wurde der Industrielle Stanley Baldwin als Nachfolger seines Vaters Alfred Baldwin (1841–1908) zum Abgeordneten des Wahlkreises Bewdley im britischen Unterhaus gewählt. Bei den Tories machte er sich einen Namen als Vertreter eines strikt konservativen, industriefreundlichen Kurses.

Erste Regierungserfahrungen sammelte er seit 1917 als stellvertretender Schatzkanzler in der Koalitionsregierung des letzten liberalen Premiers David Lloyd George. 1921 wurde er zum Handelsminister ernannt. Im Jahr darauf war Baldwin maßgeblich am Sturz Lloyd Georges beteiligt. Dessen konservativer Nachfolger Andrew Bonar Law machte ihn zu seinem Schatzkanzler. Als Bonar Law sein Amt im Mai 1923 wegen einer Krebserkrankung niederlegte, wurde Baldwin als führender Politiker der Tories zum neuen Premierminister gewählt. In Verhandlungen mit der US-Regierung versuchte er damals vergeblich, einen Erlass von britischen Schulden aus der Zeit des Ersten Weltkriegs zu erreichen.

Nach einer Unterbrechung von Januar bis November 1924 durch die Amtszeit des ersten Labour-Premiers Ramsay MacDonald war Baldwin bis zur Unterhauswahl 1929 Premierminister.
In diese Zeit fiel der Generalstreik vom 4. Mai bis 30. November 1926.
Anders als zu Beginn seiner politischen Karriere setzte er sich nun für eine gemäßigte Politik des Interessenausgleichs mit der Arbeiterschaft ein. Gegen den Widerstand von Teilen seiner eigenen Partei (etwa Winston Churchills) versuchte er, der Arbeitslosigkeit durch Schutzzölle Herr zu werden und trat für die Zusammenarbeit zwischen Arbeitgebern und Gewerkschaften in Schlichtungskommissionen ein.

Außenpolitisch kam es zu Beginn seiner Amtszeit im November 1924 zu einer schweren Krise mit dem Königreich Ägypten, welches Großbritannien 1922 in die staatliche Unabhängigkeit entlassen hatte, um den Status des Anglo-Ägyptischen Sudan.

Nach der Unterhauswahl am 30. Mai 1929 zunächst Oppositionsführer, trat er nach der vorgezogenen Unterhauswahl am 27. Oktober 1931 als Lord President of the Council in das zweite Kabinett MacDonald ein. Baldwin galt als graue Eminenz dieses Kabinetts, dessen Politik er als „heimlicher Premier“ wesentlich mitbestimmte. Nach der Unterhauswahl am 14. November 1935 übernahm er das Amt zum dritten Mal offiziell.

In der Abdankungskrise um König Edward VIII. trat Baldwin nachdrücklich für dessen Abdankung ein. Ausschlaggebend dafür waren nicht seine Vorbehalte gegen Edwards geplante Hochzeit mit der geschiedenen Amerikanerin Wallis Simpson. Baldwin lehnte die unkritische Haltung des Königs gegenüber Hitlerdeutschland ab sowie dessen autokratische Neigungen. Nach seinem Ausscheiden aus dem Amt im Jahr 1937 erwies sich Baldwin als scharfer Kritiker der Appeasement-Politik seines Nachfolgers Chamberlain.

Bei seinem Ausscheiden aus dem Amt wurde Baldwin als Earl Baldwin of Bewdley in den erblichen Adelsstand erhoben. Seine Urne wurde in der Worcester Cathedral beigesetzt.

Am 8. Dezember 1938 startete Stanley Baldwin über die BBC einen Aufruf an die britische Öffentlichkeit, durch den er für den „Lord Baldwin Fund for Refugees“ die sehr beträchtliche Summe von £ 500.000 einwarb. Ein Großteil dieser Gelder kam den Kindertransporten zugute. Sie dienten aber auch der Finanzierung der Arbeit britischer Flüchtlingskomitees wie dem Germany Emergency Committee (GEC).[1]
„Baldwin wusste nur wenig über Europa und das wenige, das er wusste, missfiel ihm.“

Tonaufnahmen:

Walpole |
Wilmington |
Pelham |
Newcastle |
Devonshire |
Newcastle |
Bute |
Grenville |
Rockingham |
Pitt |
Grafton |
North |
Rockingham |
Shelburne |
Portland |
Pitt der Jüngere |
Addington |
Pitt der Jüngere |
Grenville |
Portland |
Perceval |
Liverpool |
Canning |
Goderich |
Wellington |
Grey |
Melbourne |
Wellington |
Peel |
Melbourne |
Peel |
Russell |
Derby |
Aberdeen |
Palmerston |
Derby |
Palmerston |
Russell |
Derby |
Disraeli |
Gladstone |
Beaconsfield |
Gladstone |
Salisbury |
Gladstone |
Salisbury |
Gladstone |
Rosebery |
Salisbury |
Balfour |
Campbell-Bannerman |
Asquith |
Lloyd George |
Bonar Law |
Baldwin |
MacDonald |
Baldwin |
MacDonald |
Baldwin |
Chamberlain |
Churchill |
Attlee |
Churchill |
Eden |
Macmillan |
Douglas-Home |
Wilson |
Heath |
Wilson |
Callaghan |
Thatcher |
Major |
Blair |
Brown |
Cameron |
May

Spencer Perceval |
Robert Banks Jenkinson, 2. Earl of Liverpool |
George Canning |
Frederick John Robinson, 1. Viscount Goderich |
Arthur Wellesley, 1. Duke of Wellington |
Robert Peel |
Edward Geoffrey Smith Stanley, 14. Earl of Derby |
Benjamin Disraeli |
Robert Gascoyne-Cecil, 3. Marquess of Salisbury |
Arthur Balfour |
Andrew Bonar Law |
Austen Chamberlain |
Stanley Baldwin |
Neville Chamberlain |
Winston Churchill |
Anthony Eden |
Harold Macmillan |
Alec Douglas-Home |
Edward Heath |
Margaret Thatcher |
John Major |
William Hague |
Iain Duncan Smith |
Michael Howard |
David Cameron |
Theresa May
