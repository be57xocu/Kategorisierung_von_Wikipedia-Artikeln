Peter Hille (* 11. September 1854 in Erwitzen, Kreis Höxter, Westfalen; † 7. Mai 1904 in Berlin) war ein deutscher Schriftsteller.

Peter Hille wurde als Sohn eines Rentmeisters und Lehrers geboren. Von 1869 bis 1872 besuchte er das Gymnasium Marianum Warburg, danach das Gymnasium Paulinum in Münster, wo er Mitglied der geheimen Schülerverbindung Satrebil wurde. Die Gruppe las Karl Marx, August Bebel, Charles Darwin, auch Johann Georg Hamann, Pierre-Joseph Proudhon, Karl Gutzkow und Ludwig Büchner. 1874 musste Hille wegen ungenügender Leistungen das Gymnasium ohne Abschluss verlassen und arbeitete kurze Zeit als Protokollschreiber beim Staatsanwalt in Höxter und als Korrektor in einer Leipziger Druckerei.

1877 schrieb er an der Zeitschrift Deutsche Dichtung mit, die von den Brüdern Heinrich und Julius Hart gegründet worden war. Hier erschienen seine ersten Gedichte. Für die Deutschen Monatsblätter schrieb er literaturwissenschaftliche Beiträge. Eine Zeit lang arbeitete er auch in Bremen an der sozialdemokratischen Zeitung Bremer Tageblatt. 1880 lebte er in den Londoner Elendsvierteln, lernte Sozialisten und Anarchisten kennen.

1884 finanzierte er mit seiner Erbschaft eine niederländische Schauspielertruppe, die ihn mit in den Ruin riss. Er lebte zeitweilig als Obdachloser, spielte jedoch trotzdem eine wichtige Rolle in der naturalistischen Bewegung.

1888 kam seine Tuberkulose zum Ausbruch. Der Schriftsteller Karl Henckell rettete ihn vor dem Verhungern und nahm ihn mit nach Zürich. Doch bald war Hille wieder unterwegs, diesmal nach Südeuropa. 1891 suchte er Zuflucht bei seinem Freund Julius Hart. Die Polizei verfolgte ihn als angeblichen Sozialdemokraten, er flüchtete durch ganz Deutschland, bis er 1895 nach Berlin zurückkehrte. Dort findet ab 1899 Else Lasker-Schüler durch die Freundschaft mit ihm Anschluss an die literarische Szene.

Die naturalistischen Schriftsteller sorgten für ihn. Die Neue Gemeinschaft bestritt seinen Lebensunterhalt. 1902 eröffnete er ein Kabarett. Bald darauf, am 7. Mai 1904, erlag er seinem chronischen Lungenleiden im Alter von 49 Jahren. 

Lasker-Schüler hat Hille in ihrem 1906 erschienenen Erstlingsprosawerk Das Peter Hille-Buch postum enthusiastisch gewürdigt.[1]
Ein Teilnachlass von Peter Hille befindet sich in der Handschriftenabteilung der Stadt- und Landesbibliothek Dortmund.
