Henry B. Ollendorff (auch: Ollendorf; * 14. März 1907 in Esslingen; † 10. Februar 1979 in Cleveland, Ohio) war ein deutschstämmiger US-amerikanischer Jurist und Sozialarbeiter.

Als Sohn eines zum christlichen Glauben konvertierten jüdischen Augenarztes in Esslingen geboren, wuchs er in Darmstadt auf, studierte Jura in Berlin und promovierte 1929 in Heidelberg. 

Durch die nationalsozialistische Gesetzgebung wurde er stark behindert, kam für 13 Monate in Haft und emigrierte 1938 in die USA. Seine Frau folgte ihm 1939. Seine Stiefmutter wurde im KZ Auschwitz ermordet.

In Cleveland studierte er Sozialarbeit und arbeitete hierauf für unterprivilegierte Kinder in seiner neuen Heimat. 

Er wurde von der US-Regierung 1954 nach Deutschland gesandt, um im Rahmen des Umerziehungsprogramms Kurse für deutsche Jugendleiter und Sozialarbeiter zu leiten. Die sehr erfolgreiche Tätigkeit führte dann zu einem - mittlerweile weltweiten - Austausch von Fachleuten aus dem sozialen Bereich und zur Gründung des Council of International Fellowship - CIF.[1]
Seit 2005 ist ein Platz in Darmstadt nach ihm benannt.[2]