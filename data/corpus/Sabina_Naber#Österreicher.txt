Sabina Naber (* 17. Dezember 1965 in Tulln in Niederösterreich) ist eine österreichische Schauspielerin, Regisseurin, Kriminal- und Drehbuchautorin.

Nach dem Abschluss ihres Studiums 1991 mit dem Magister in den Fächern Theaterwissenschaft, Germanistik, Geschichte und Philosophie sammelte Naber journalistische Erfahrungen unter anderen beim Österreichischen Rundfunk und den Niederösterreichischen Nachrichten. 

Seit 1987 absolvierte sie eine Theaterausbildung mit Kursen, war zuerst als Schauspielerin tätig, dann als Regisseurin. Zu dieser Zeit veröffentlichte sie auch Liedtexte und Musicals. 

1996 begann sie mit der Ausbildung als Drehbuchautorin. Es folgten Aufträge von ORF, Terra-Film, Wega-Film, Prisma-Film und Dor-Film sowie zahlreiche Industriefilme. Zugleich veröffentlichte sie Kurzgeschichten in Anthologien. Ihr erster Kriminalroman Die Namensvetterin erschien 2002.
