Steve Case (* 21. August 1958) ist der Gründer von America Online. Er trat im Januar 2003 auf Druck der Anteilseigner in der Folge von finanziellen Schwierigkeiten und Abrechnungsskandalen als Vorstandsvorsitzender (CEO) zurück und fungierte fortan nur noch als Director und Co-Chair im „Strategic Committee“.

Er gründete ursprünglich Quantum Link, einen Onlinedienst für den Commodore 64. Seit 1988 wurden Apple und IBM-kompatible Computer unterstützt. 1991 erfolgte die Umbenennung zu America Online. 1994 stieg die Anzahl der Nutzer auf eine Million.

Nach einem Jahrzehnt extremen Wachstums fusionierte AOL Anfang 2000 mit Time Warner und firmierte fortan eine Zeit lang unter der Bezeichnung „AOL Time Warner“, bevor es sich wieder in „Time Warner“ umbenannte.

Im April 2005 gründete Steve Case das Unternehmen „Revolution“, das sich auf die Fahnen geschrieben hat, „Konsumenten mehr Wahlmöglichkeiten, Komfort und Kontrolle einzuräumen“. Im Oktober 2005 hat  Case Time Warner endgültig verlassen, um sich ausschließlich seiner neuen Firma widmen zu können.

Steve Case ist der Cousin von Ed Case, der zwischen 2002 und 2007 den Staat Hawaii im US-Repräsentantenhaus vertrat.
