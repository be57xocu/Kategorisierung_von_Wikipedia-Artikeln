Sara Lidman (* 30. Dezember 1923 in Missenträsk; † 17. Juni 2004 in Umeå) war eine schwedische Schriftstellerin.

Sie erzielte 1953 mit ihrem Roman „Tjärdalen“ (Das Teertal) ihren ersten großen Erfolg. Darin und in den darauf folgenden drei Romanen schildert die Autorin mit großem Pathos die harten Lebensbedingungen der armen Bauern im Inland der nördlichen schwedischen Provinz Västerbotten im 19. Jahrhundert.

Ihr innovativer Sprachgebrauch ist stark geprägt von dialektalen Einflüssen und von der Bibelsprache.

Im Anschluss an die vier ersten Romane schrieb Sara Lidman eine Reihe von Texten mit starkem politischem Inhalt. Sie engagierte sich im Protest gegen den Vietnamkrieg, gegen das südafrikanische Apartheidregime und für den Streik der Grubenarbeiter in Nordschweden und beeinflusste die öffentliche Meinung auf entscheidende Weise. Ab 1977 erschienen fünf Romane, in denen die Kolonisierung Nordschwedens durch den Eisenbahnbau ihre literarische Gestaltung findet. Diese fünf Romane sind in der englischen Übersetzung auch als „Railway-Suite“ bekannt und umfassen die Bücher Din Tjänare Hör, Vredens Barn, Nabots Sten, Den Underbare Mannen und Järnkronan. Die späteren Titel Lifsens Rot und Oskuldens Minut sind ebenfalls Eisenbahn-Romane, bilden aber keine inhaltliche Fortsetzung zu der Romanserie der Jahre 1977 bis 1985.

Sara Lidman sind zahlreiche Preise verliehen worden, darunter 1980 der renommierte Literaturpreis des Nordischen Rates für Vredens barn. Unter den Ehrungen finden sich auch ein Ehrendoktortitel[1] und die Verleihung des Professorentitels.
