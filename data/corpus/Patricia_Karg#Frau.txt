Patricia Karg (* 7. Dezember 1961 in Innsbruck) ist eine österreichische Bildhauerin und Malerin.

Patricia Karg ist die Tochter des Baumeisters Ludwig Karg, von dem sie den Bezug zum Handwerklichen lernte, und seiner Frau Gertraud. Von ihrer Großmutter, einer Handarbeitslehrerin, die ihre Kreativität weckte, lernte Patricia das Nähen. Eine von einer Freundin aus Amerika geschenkte Barbie-Puppe (in Europa gab es diese noch kaum) kleidete die elfjährige Patricia mit eigenen Kreationen aus Stoff und Häkeleien ein. So entstand aus zwei Meter Stoff ein Faltenkleid, das sie „die spanische Nacht“ nannte.

Nach der Pflichtschule besuchte Karg von 1976 bis 1980 die Fachschule für Holz- und Steinbildhauerei an der HTL für Bau und Kunst in Innsbruck und schloss diese 1981 mit der Gesellenprüfung ab. Von 1980 bis 1987 studierte sie Bildhauerei an der Münchener Akademie der Bildenden Künste und war Meisterschülerin von Hans Ladner. Mit Diplom beendete sie das Studium. Seit 1987 ist sie freischaffend tätig.

Karg arbeitet mit Holz, Stein, Kunststein, Bronzeguss, Eisen und Glas. Sie malt mit Acryl, Öl, Aquarell und macht Grafik.[1][2] Ihr künstlerisches Motto ist: „Durch meine Arbeit möchte ich den Geist der Menschen erbauen und weiten. Meine Werke sollen Nahrung für die Seele sein.“

Patricia Karg lebt mit ihren Töchtern in Thaur in Tirol. Ihr „Atelierhaus“ genanntes und 1995 bezogenes Haus hat sie selbst entworfen und vom Innsbrucker Architekten Wolfgang Martin Miess umsetzen lassen.
