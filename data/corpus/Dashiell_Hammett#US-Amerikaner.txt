Samuel Dashiell Hammett (* 27. Mai 1894 in Maryland; † 10. Januar 1961 in New York) war ein US-amerikanischer Schriftsteller. Er veröffentlichte auch unter dem Pseudonym Peter Collinson. Hammett gilt noch vor Raymond Chandler als der Begründer des amerikanischen Kriminalromans (hardboiled novel).

Dashiell Hammetts Eltern waren Richard Thomas Hammett und Annie Bond Dashiell (vom französischen De Chiel).
Er verließ die Schule mit 13 Jahren und wurde nach einer Reihe von Aushilfsarbeiten Angestellter der Detektivagentur Pinkerton.

Nach Teilnahme am Ersten Weltkrieg als Mitglied einer motorisierten Lazaretteinheit, während der er an Tuberkulose erkrankte, begann er, in der Werbeindustrie zu arbeiten, ab 1915 dann als Angestellter bei Pinkerton. 1922 wurde er Schriftsteller. Seine literarischen Arbeiten basieren zu einem großen Teil auf persönlichen Erfahrungen, die er als Mitarbeiter bei der Detektivagentur Pinkerton gemacht hatte. Er veröffentlichte hauptsächlich in der Zeitschrift Black Mask. Herausgeber war Joseph Thompson Shaw.

In seinen Kriminalromanen beschrieb er den amerikanischen Privatdetektiv als Antihelden. Die realistische Darstellung des Verbrechermilieus sowie die Verquickung von Verbrecher und Detektiv setzen Hammett von den klassischen Kriminalautoren, die eine oft dualistische Einteilung der Gesellschaft in „gut“ und „böse“ machen, ab. Seine bekannteste Figur ist Sam Spade aus dem Roman Der Malteser Falke; erfolgreich verfilmt als Die Spur des Falken. 

1920 heiratete Dashiell Hammett Josephine Annas Dolan. Ihr widmete er später den Roman Der Malteser Falke („Für Jose“). Aus der Ehe gingen zwei Töchter hervor, Mary Jane (1921) und Josephine („Jo“) (1926). Obwohl Hammett schon sehr bald nicht mehr bei seiner Familie lebte, wurde die Ehe erst 1937 geschieden.

Dashiell Hammetts große Liebe war die Dramatikerin Lillian Hellman, mit der er von 1931 an liiert war. Die Beziehung hielt bis zu seinem Tode. Im Zweiten Weltkrieg meldete Hammett sich freiwillig und verbrachte seinen Militärdienst als Herausgeber einer Armeezeitung auf den Aleuten. Nach drei Jahren Dienst verließ Hammett 1945 die Armee im Rang eines Stabsfeldwebels.

Neben seinen Romanen verfasste er eine Reihe von Kurzgeschichten und war auch an Drehbüchern (u. a. des Film Noir) beteiligt. Nach seinem letzten Roman The Thin Man (Der dünne Mann) (1934) widmete er sich links-politischen und antifaschistischen Aktivitäten. Er trat 1937 der Kommunistischen Partei bei und wurde 1946 Präsident der im selben Jahr gegründeten Bürgerrechtsbewegung Civil Rights Congress.

Im Zusammenhang mit diesem politischen Engagement wurde Hammett während der McCarthy-Ära 1951 wegen Missachtung des Gerichts zu einer sechsmonatigen Haftstrafe verurteilt, von der er 5 Monate verbüßte, da er unter Berufung auf das Fifth Amendment von seinem Zeugnisverweigerungsrecht Gebrauch machte. 

Nachdem Hammett im Gefängnis inhaftiert war, verlangte die amerikanische Einkommensteuerbehörde eine Nachzahlung von 111.000 Dollar von ihm und beschlagnahmte seine Tantiemen. Diese „Sperre“ wurde laut Hellmans Darstellung für die restlichen 10 Jahre seines Lebens nie mehr aufgehoben. Hellman unterstützte ihn finanziell.[1] Damals setzte NBC Hammetts Spade-Serie im Rundfunk ab. Auch die Veröffentlichung von A Man Named Thin wurde vom Verlag gestoppt. 1953 musste er vor der McCarthy-Kommission aussagen. Die Befragung wurde im Fernsehen übertragen.

1955 erlitt Hammett einen schweren Herzanfall und lebte seitdem weitgehend zurückgezogen. Seit 1959 erhielt er eine monatliche Pension in Höhe von 131 Dollar von der Veteranenbehörde. Er starb 1961 als armer Mann an Lungenkrebs und ist als Kriegsveteran beider Weltkriege auf dem Nationalfriedhof Arlington bei Washington beigesetzt.

Postum fungierte er als Namensgeber für den spanischen Premio Hammett und nordamerikanischen Hammett Prize, Literaturpreisen für Kriminalliteratur. Im Jahre 2011 verlieh ihm die US-amerikanische Nero Wolfe Society (The Wolfe Pack) in Anerkennung seines literarischen Lebenswerkes postum den Archie Goodwin Award.

Zahlreiche Hörspiele v.a. der Kurzgeschichten mit dem anonymen „Continental Op“. Die fünf „großen“ Romane wurden 1996 bis 1998 von Norbert Schaeffer und Walter Adler für SWF/HR produziert[3]:
