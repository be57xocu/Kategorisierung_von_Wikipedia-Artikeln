Pierre Koenig (* 17. Oktober 1925 in San Francisco; † 4. April 2004 in Los Angeles) war ein US-amerikanischer Architekt. Er zählt zu den Vätern der kalifornischen Moderne und wurde in den Nachkriegsjahren mit seinen Stahl- und Glaskonstruktionen bekannt.

Koenig erhielt sein Architekturdiplom 1952 an der University of Southern California, wo er unter anderem bei Raphael Soriano studierte. 1953 heiratete Koenig Merry Thompson, im folgenden Jahr wurde sein Sohn Randall Francis geboren. Nach fünf Jahren Ehe folgte die Scheidung von seiner ersten Frau. Seine zweite Frau Gail Carson ehelichte Koenig 1960, ein Jahr später kam sein zweiter Sohn Jean Pierre zur Welt. 1964 wurde Koenig Assistenzprofessor an der Architekturfakultät der University of Southern California, Los Angeles, wo er vier Jahre später zum außerordentlichen Professor berufen wurde. 1971 wurde Koenig Mitglied des College of Fellows des American Institute of Architects (AIA). Die Scheidung von Gaile Carson folgte 1975. 1985 heiratete er Gloria Kaufman, die zwei Söhne mit in die Ehe brachte. Zum ordentlichen Professor an der Architekturfakultät der University of Southern California wurde Koenig 1996 berufen.

„[Pierre Koenig ist die] Schlüsselfigur einer Generation, die dazu beigetragen hat, aus Los Angeles eines der großen Laboratorien der Architektur des 20. Jahrhunderts zu machen.“

Koenig praktizierte hauptsächlich an der Westküste und wurde vor allem bekannt durch die Entwürfe zu den Stahltragwerkhäusern Case Study Houses No. 21 (1956–1958) und 22 (1960). Beide Konstruktionen Nr. 21 (auch Bailey House genannt) und Nr. 22 (das Stahl House) wurden auf zuvor für unbebaubar gehaltenen Grundstücken errichtet.

„Man muss verstehen, dass damals freudige Aufbruchstimmung herrschte. Der Krieg war vorbei, alle waren äußerst idealistisch und wollten das Problem der Wohnungsnot lösen. Alle beschäftigten sich mit Massenproduktion, Bausystemen und der Lösung sozialer Missstände. Es war eine spannende Zeit, in der alles Mögliche ausprobiert wurde.“

1989: Kulturpreis der Stadt Los Angeles

1996: 25 Year Award des AIA, California Council

1996: Auszeichnung mit dem Maybeck Award for Lifetime Achievement des AIA, California Council Los Angeles, Kalifornien

1998: Distinguished Alumni der Architekturfakultät der University of Southern California

1999: Goldmedaille des AIA, Sektion Los Angeles

1999: Star of Design for Lifetime Achievement des Pacific Design Center, Los Angeles, Kalifornien

1999: Distinguished Professor of Architecture der University of Southern California

2000: Ehrenmitglied des Royal Institute of British Architects

2000: Denkmalpflegepreis der Stadt Los Angeles

2000: Distinguished Alumni Award des Pasadena City College

2000: Goldmedaille für das Lebenswerk von der Tau Sigma Delta Society of Architects and Landscape Architects

2001: Nationaler Designpreis, Finalist Sparte Architektur, Smithsonian/Cooper-Hewitt National Design Museum

2001: Auszeichnung mit dem 25 Year Award des AIA, California Council
