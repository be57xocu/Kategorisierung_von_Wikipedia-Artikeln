Zora Vesecká (* 1967 in Olomouc) ist eine tschechische Filmschauspielerin. 

Sie spielte in tschechischen Fernsehserien und -filmen und auch in dem deutschsprachigen Fernsehfilm Ein Stück Himmel. 

Pavel Brycz beschreibt Zora Vesecká als einen Kinderstar im Fernsehen der 1970er Jahre in seinem Werk I, CITY und schreibt ihr zu, dass sie sich der Liebe verschrieben hätte:

„Love is a Magnetic Mountain. If it gets hold of you, it doesn't let go. For ten years, on the sidewalk under the windows of the boarding scool I LOVE YOU had been painted in red by an unknown author. All the girls living behind those windows could confirm this. Speaking for them all there is for example Zora Vesecká – child star of '70s television – who, during her short stay in the city, observed herself as the object of this declaration too.“
