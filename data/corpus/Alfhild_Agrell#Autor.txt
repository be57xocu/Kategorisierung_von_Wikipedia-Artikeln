Alfhild Teresia Agrell, geborene Martin (* 13. Januar 1849 in Härnösand; † 8. November 1923 in Flen) war eine schwedische Schriftstellerin.

Agrell wurde 1849 als Tochter des Konditors Erik Johan Martin und seiner Frau Karolina Margareta Adolphson geboren. Von 1868 bis 1895 war sie mit dem Stockholmer Kaufmann A. Agrell verheiratet.

Agrell benutzte zeitweilig auch die Pseudonyme Thyra, Lovisa Petterqvist und Stig Stigson.
