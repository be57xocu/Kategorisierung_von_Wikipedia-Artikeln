Jean Stapleton, bürgerlich Jeanne Murray (* 19. Januar 1923 in New York City; † 31. Mai 2013 ebenda) war eine US-amerikanische Schauspielerin.[1]
Stapleton begann ihre Karriere im Showgeschäft in den 1950er und 1960er Jahren als Broadway-Schauspielerin, zudem spielte sie in der gleichen Zeit vor allem in verschiedenen Fernsehproduktionen kleinere Gastrollen. 

Stapleton wurde vor allem durch ihre Rolle der Edith Bunker an der Seite von Carroll O’Connor in der 1970er Jahre-Sitcom All in the Family berühmt, für die sie mit drei Emmys und zwei Golden Globes ausgezeichnet wurde.

1986 spielte sie die Rolle der Ariadne Oliver an der Seite von Peter Ustinov als Hercule Poirot in dem Fernsehkrimi Mord mit verteilten Rollen von Agatha Christie. In ihrer Karriere war sie insgesamt in rund 90 Film- und Fernsehproduktionen zu sehen. 

1957 heiratete sie William Putch, mit dem sie zwei Kinder hatte. Putch starb 1983.
