Thomas Wittmann (* 1963 in München) ist ein deutscher Schauspieler und Sprecher.[1]
Thomas Wittmann erhielt seine Schauspielausbildung am Mozarteum in Salzburg.[2]
Sein erstes Theaterengagement führte ihn 1985 ans Burgtheater in Wien. Von 1986 bis 1995 war er während der Intendanz von 
Frank Patrick Steckel am Schauspielhaus Bochum, 1996 kam er unter der Leitung von Anna Badora ans Düsseldorfer Schauspielhaus 
und blieb dort bis 2006.

Im selben Jahr gastierte er am Berliner Ensemble unter der Regie von Claus Peymann in Schillers Jungfrau von Orleans 
als König Karl der Siebte. Danach spielte er zwei Jahre am Schauspiel Köln während der Intendanz von Karin Beier 
und wechselte 2009 als festes Mitglied ans Berliner Ensemble.

Unter der Leitung von Wilfried Schulz ist er seit 2016 wieder fest am Düsseldorfer Schauspielhaus engagiert.

Er arbeitete u.a. mit Regisseuren wie Andrea Breth, Thomas Langhoff, Matthias Hartmann, Andreas Kriegenburg, Nicolas Stemann und 
vor allem mit Jürgen Gosch, so auch 2005 in dessen "Macbeth"-Inszenierung am Düsseldorfer Schauspielhaus. 

Außerdem arbeitet er bei Film und Fernsehen und tritt als Sprecher mit verschiedenen Orchestern, insbesondere mit den Berliner Philharmonikern, auf.
