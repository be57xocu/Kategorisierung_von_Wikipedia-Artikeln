Mia Aegerter (* 9. Oktober 1976 als Myriam Aegerter in Freiburg im Üechtland) ist eine Schweizer Pop-Sängerin und Schauspielerin.

Mia Aegerter wuchs in einer Künstlerfamilie auf. Sowohl ihre Mutter (Gitarrenlehrerin) als auch ihr Vater (Drummer) spielen in einer Band. Mia spielte von klein auf Gitarre und schrieb eigene Songs. Nach ihrer Matura zog sie nach München, wo sie an der Musicalakademie Gesang, Tanz und Schauspiel studierte.

1998 gewann sie den Imitatorenwettbewerb in der von Thomas Ohrner moderierten Sendung Lass dich überraschen und trat in der Soundmixshow mit dem Titel Torn von Natalie Imbruglia auf. Ausserdem war sie als Tänzerin im Videoclip Doubledecker der Band Liquido zu sehen.

Bekannt wurde Aegerter durch zahlreiche Auftritte als Xenia di Montalban in der Serie Gute Zeiten, schlechte Zeiten. Wegen einer spät entdeckten Lungenkrankheit musste sie bei GZSZ eine sechsmonatige Pause machen. 2003 wurde sie mit dem Prix Walo in der Kategorie Newcomer 2003 geehrt und erhielt am 1. April 2004 für den Song Hie u jetzt in der Schweiz den VIVA COMET in der Kategorie Best CH Act 2003. Sie moderierte ausserdem im Jahr 2004 das Jugendmagazin Bravo TV des ZDF. Am 8. Januar 2005 wurde sie mit dem renommierten SwissAward in der Kategorie „Showbusiness“ geehrt. Ausserdem war sie neben Detlef „D!“ Soost und Chris von Rohr in der Jury der Ausgabe 2004/05 der TV-Show MusicStar auf SF DRS.

Im Februar 2005 war Aegerter zusammen mit Ronan Keating in Deutschland auf Tour. Am 12. März 2005 nahm sie mit dem Titel Alive an der Deutschen Vorentscheidung zum Eurovision Song Contest 2005 teil. Im April 2005 tourte sie mit ihrer Band, bestehend aus Julian Feifel (Guitar / Background Vocals), Benno Richter (Bass) und Athanasios „Zacky“ Tsoukas (Drums), durch diverse Schweizer Clubs. Im Sommer trat sie an verschiedenen Schweizer Festivals auf.

Am 21. März 2006 trat Aegerter mit der Ankündigung eines neuen Albums und einer Single wieder in die Öffentlichkeit. Die Single Meischterwärk/Masterpiece Of Humankind erschien am 12. Mai 2006, das Album Vo Mänsche U Monster/Of Humans and Monsters folgte am 9. Juni 2006. Mit einer neuen Band trat Mia in verschiedenen Schweizer Städten auf.

Die meisten Radiostationen weigerten sich 2006, ihre neue Single Meischterwärk abzuspielen, weil der Song angeblich zu rockig war. Sie klassifizierte sich nur zwei Wochen in der Hitparade und kam nicht einmal unter die Top 50. Das zugehörige Album Vo Mänsche u Monschter / Of Humans and Monsters konnte sich besser behaupten. Ihr drittes Album Chopf oder Buuch wurde  am 30. Januar 2009 veröffentlicht. Im Jahr 2012 folgte mit Gränzgängerin ihr viertes Album.

Nach fünf Jahren Pause veröffentlichte Mia Aegerter 2017 ihr fünftes Album Nichts für Feiglinge in Eigenregie. Sie singt erstmals auf Hochdeutsch und hat alle Songs selbst geschrieben. Gesang und Gitarre stehen dabei im Mittelpunkt.
