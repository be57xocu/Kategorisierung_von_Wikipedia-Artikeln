Arnold Siemens, ab 1888 von Siemens (* 13. November 1853 in Berlin; † 29. April 1918 ebenda) war ein deutscher Industrieller aus der Familie Siemens, Mitinhaber der Firma Siemens & Halske und Politiker.

Er entstammte dem alten Goslarer Stadtgeschlecht Siemens (1384 urkundlich erwähnt) und war der älteste Sohn des Erfinders und Unternehmers Werner von Siemens (1816–1892) und dessen erster Ehefrau Mathilde Drumann (1824–1865) aus Königsberg (Preußen). Vater Werner Siemens wurde mit seinen Nachkommen am 5. Mai 1888 in Charlottenburg in den preußischen Adelsstand erhoben.

Siemens heiratete am 10. November 1884 in Berlin die Malerin Ellen von Helmholtz (* 24. April 1864 in Heidelberg; † 27. November 1941 in Berlin), die Tochter des Physikers Hermann von Helmholtz und seiner Frau Anna, einer Tochter des Robert von Mohl. Aus der Ehe gingen fünf Kinder hervor, unter anderem der spätere Firmenchef Hermann von Siemens. Das Paar lebte in der von ihnen erbauten Siemens-Villa am Kleinen Wannsee bei Berlin.

Zusammen mit seinem Bruder Wilhelm von Siemens wurde ihm von seinem Vater im Jahr 1879 die Leitung des Wiener Zweiggeschäftes, seit 1890 die Leitung der Berliner Firma Siemens & Halske übertragen. Er war seit 1897 Vorsitzender des in eine Aktiengesellschaft umgewandelten Unternehmens. 

Die von Arnold von Siemens aufgebaute Wiener Filiale nahm 1883 eine eigene Produktion auf. 1892 wurde die erste Siemens-Niederlassung in Übersee, die Siemens & Halske Japan Agency in Tokio, gegründet. Eine von Arnold ebenfalls 1892 mit zwei amerikanischen Partnern errichtete Fabrik für Eisenbahnmotoren und Dynamomaschinen in Chicago, die General Electric Konkurrenz machen sollte, wurde im August 1894 durch Brand völlig zerstört.[1] Bei Ausbruch des Ersten Weltkrieges bestanden Produktionsstätten in Großbritannien, Russland, Österreich-Ungarn, Frankreich, Belgien und Spanien. Insgesamt besaß Siemens in 49 Ländern 168 Vertretungsbüros.

Arnold von Siemens war Mitglied des Preußischen Herrenhauses.  
