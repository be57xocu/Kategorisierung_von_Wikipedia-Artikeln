Carolyn Gold Heilbrun, bekannt auch unter dem Pseudonym Amanda Cross, (* 13. Januar 1926 in East Orange; New Jersey; † 9. Oktober 2003 in Manhattan, New York City, N.Y.) war eine amerikanische Schriftstellerin und Frauenrechtlerin.

Carolyn Gold studierte an der renommierten Frauen-Universität Wellesley College bei Boston und englische Literatur an der Columbia University, New York City. Sie heiratete James Heilbrun, Professor für Wirtschaftswissenschaften, und hatte mit ihm drei Kinder. 1986 wurde sie die erste Direktorin des Forschungsinstitutes für Frauenfragen an der Columbia University.

Heilbrun verfasste 9 feministische Werke und zahlreiche Essays und Presseartikel, in denen sie Literatur, vor allem die literarischen Werke von Frauen, aus dem Blickwinkel einer Feministin interpretierte.

Unter dem Pseudonym „Amanda Cross“ veröffentlichte Heilbrun 13 Kriminalromane um die Literaturprofessorin Kate Fansler.

Sie war Fellow der American Academy of Arts and Sciences.[1]
Heilbrun starb am 9. Oktober 2003 durch Selbsttötung. Sie hatte sich bereits 1997 in dem Essay-Band The Last Gift of Time: Life Beyond Sixty („Das letzte Geschenk: Leben jenseits der 60er“, nicht ins Deutsche übersetzt) mit dem Thema Selbsttötung befasst.

Amanda Cross
