Gerald Gersham Jampolsky (* 11. Februar 1925 in Long Beach, Kalifornien)[1] ist ein US-amerikanischer Arzt und esoterischer Autor, der sich vor allem mit der seelischen Heilung befasst und dessen Hauptbotschaft ist: „Liebe heilt am besten“.

Jampolsky studierte und promovierte an der medizinischen Fakultät in Stanford und arbeitete danach als Psychiater am medizinischen Zentrum der University of California in San Francisco.

1974 begegnete er Swami ‚Baba‘ Muktananda, der ihn – wie Jampolsky sagt – bis dahin unbekannte Bewusstseinszustände erleben habe lassen und ihn wieder näher zu Gott gebracht habe. Jampolsky beschreibt diese Begegnung als Wendepunkt in seinem Leben, das bis dahin vom beruflichen Erfolg aber auch von der Scheidung seiner langjährigen Ehe und schweren Alkoholproblemen gekennzeichnet war.

1975 gründete er das Center for Attitudinal Healing (etwa „Zentrum für Heilung über die geistige Einstellung“) in Tiburon (Kalifornien), wo Kinder und Jugendliche mit Behinderungen oder schwer heilbaren Krankheiten geholfen wird, seelischen Frieden und, wenn möglich, Heilung zu finden. Nach diesem Vorbild wurden mehr als 150 Zentren oder tätige Gruppen auf der ganzen Welt gegründet.

Heute schreibt Jampolsky weitere Bücher, widmet sich seinen Patienten im Zentrum, bereist die Zentren für subjektive Heilung und hält Vorträge oder gibt Seminare.

Seine Lehre bezieht sich meist auf Dinge, die für alle Menschen fühlbar oder nachvollziehbar sind. Seine Anliegen sind Überwindung von Furcht, Misstrauen, und sonstiger selbsteinschränkender Einstellungen. Er befürwortet eine selbstbestimmte Hinwendung zu einem spirituellen Wachstum in liebender Beziehung zu Gott und seinen Mitgeschöpfen.
