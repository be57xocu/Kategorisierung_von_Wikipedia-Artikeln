Hans Peter Dieter Udelhoven (* 1944) ist ein deutscher Wissenschaftsjournalist und Sachbuchautor.

Udelhoven verfasste zahlreiche deutschsprachige Publikationen auf dem Feld der Humanmedizin. Die Schwerpunkte seiner publizistischen Tätigkeit liegen in den Bereichen Patientenaufklärung, Doping im Sport, ärztliche Fortbildung und kritische Aufarbeitung des weitreichenden Lobbyismus der Pharmaindustrie. Auch als Autor eines realistischen Doping-Thrillers (Rage, 1999) ist er in Erscheinung getreten.

Beim Zahnpasta-Doping-Skandal um Dieter Baumann war Udelhoven an der Offenlegung beteiligt. Er dokumentierte die in der Dopingforschung nachgewiesene Möglichkeit der effektiven Substanzaufnahme über die Mundschleimhaut.

Udelhoven war Anfang der siebziger Jahre Gründer und verantwortlicher Verleger des Wissenschaftsverlags in Köln. Auch die Akademie für Medizin in den Medien geht auf seine Initiative zurück. Als freier Publizist fungierte er später als Herausgeber von Patientenratgebern zu den unterschiedlichsten Gesundheitsthemen (Lübbes Ärztlicher Ratgeber, Patienten verstehen Medizin, Patient) Der Große Familien-Ratgeber der Gesundheit aus dem Jahre 1986 wurde unter seiner verantwortlichen wissenschaftlichen Leitung erarbeitet und erwies sich als bemerkenswerter Longseller, der ein breites Publikum erreichte und noch heute in vielen Haushalten steht. Die umfangreiche Enzyklopädie gilt als Standardwerk einer populärwissenschaftlichen Patientenaufklärung im besten Sinne. In den 1990er Jahren war Udelhoven Ressortleiter Wissenschaft der medizinischen Fachblätter Der Kassenarzt, Medical Post und Chefredakteur der Fortbildungsmagazine Therapiewoche und Praxisberater.

Als erfolgreicher Seniorensportler errang Udelhoven zahlreiche Medaillen bei Europa- und Weltmeisterschaften für Seniorensportler.

Auch in gänzlich fachfremden Bereichen wie der katholischen Kirchenarbeit und dem politischen Engagement multikultureller Prägung hat Udelhoven seit Jahrzehnten ein breites Tätigkeitsfeld erschlossen.
