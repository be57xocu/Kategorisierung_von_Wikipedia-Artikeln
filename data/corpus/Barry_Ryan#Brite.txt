Barry Ryan (* 24. Oktober 1948 in Leeds, Yorkshire, England als Barry Sapherson) ist ein britischer Popmusik-Sänger, der ab Mitte der 1960er Jahre gemeinsam mit seinem Zwillingsbruder Paul Ryan, ab 1968 als Solointerpret bis Anfang der 1970er Jahre weltweit Hitparaden-Erfolge feiern konnte. Sein größter Hit ist das von Paul Ryan geschriebene Eloise.

Die eineiigen Zwillinge waren Kinder der in den 1950er Jahren recht erfolgreichen Sängerin Marion Ryan und ihres ersten Ehemannes Lloyd Sapherson. Mit 15 Jahren traten sie selbst ins Showgeschäft ein; sie übernahmen von da ab den Mädchen- und Künstlernamen ihrer Mutter – zunächst als The Ryan Twins.

Die Ryan-Brüder veröffentlichten in den 1960er Jahren bei Decca Records mehrere erfolgreiche Hit-Singles, darunter Don't Bring Me Your Heartaches (1965), Have Pity On The Boy (1966) und I Love Her (1966), die sich in der britischen Hitparade in den Top 20 platzieren konnten. Ihr von Top-Designern durchgestyltes photogenes Aussehen, die eingängigen Songs und Barrys unverwechselbare Stimme verschafften ihnen zahlreiche Tourneen, Auftritte in TV-Shows und eine hohe Medienpräsenz in den damaligen Pop-Jugendzeitschriften.

Der Starrummel und der damit verbundene Stress brachten Paul jedoch angeblich an den Rand eines Nervenzusammenbruchs. 1967 begannen die Brüder deshalb, sich auf neue Wege zu konzentrieren. Sie unterschrieben Ende 1967 einen Plattenvertrag beim Label MGM Records. Nach Veröffentlichung von zwei weiteren gemeinsamen Singles (Heartbreaker und Pictures Of Today) verlegte sich Paul auf das Schreiben und Produzieren der Songs, während Barry sie als Interpret vortrug.

Mit der zweiten von MGM Records veröffentlichten Solo-Single Eloise gelang Barry Ryan 1968 ein weltweiter Hit. In sechs Ländern belegte der dramatische, reich orchestrierte Song wochenlang die Nummer 1 der Hitparaden. In Ryans Heimatland Großbritannien kam Eloise auf Platz 2. Die Single verkaufte sich insgesamt über drei Millionen Mal. Die Inspiration zum Schreiben dieses Songs kam Paul Ryan, als er McArthur Park von Jimmy Webb, gesungen von Richard Harris, hörte. Von dieser Aufnahme war er so begeistert, dass er etwas im selben Musikstil schreiben wollte.

Paul Ryan schrieb noch weitere Hits für seinen Bruder, z. B. Love Is Love, The Hunt und Kitsch, die jedoch nicht an den Welterfolg von Eloise anknüpfen konnten. Kitsch beschreibt die Wirkung dieses auch im Englischen gebräuchlichen Wortes: “Kitsch is a beautiful word, it's a beautiful word, it's a beautiful lullaby…”

Wie die Beatles und Mary Hopkin sang Ryan auch Lieder in Deutsch, z. B. Zeit macht nur vor dem Teufel halt, das in Deutschland 1972 ein Top-10-Hit wurde.

Im Jahr 1969 erhielt Ryan den Bronzenen und 1970 den Silbernen Bravo Otto der Jugendzeitschrift Bravo. Bei Fotoaufnahmen für die Bravo gab es im Jahr 1969 einen Unfall, bei dem Ryan angeblich schwere Verbrennungen erlitt; es wurde vermutet, dass der Unfall zu Publicity-Zwecken stark dramatisiert wurde.

Nach dem Jahr 1972 blieben weitere Charterfolge aus. Dies sowohl bei den Solo-Veröffentlichungen von Barry Ryan, wie auch bei den in den Jahren 1973 und 1974 gemeinsamen produzierten Singles mit Bruder Paul Ryan. Im Jahr 1976 unternahm Barry Ryan unter dem Pseudonym Matayo mit der gleichnamigen Single einen weiteren, jedoch erfolglosen Versuch, an seine alten Erfolge anzuknüpfen.

Nachdem Barry 1978 geheiratet hatte, zog er sich ins Privatleben zurück. Er verlegte seine künstlerische Tätigkeit fortan vermehrt in die Fotografie. Einzig 1989 veröffentlichte er auf Drängen seines Bruders Paul noch einmal zwei Singles, welche jedoch beide floppten. Zu dieser Zeit wurde bei Paul eine Krebserkrankung diagnostiziert, an der er erst 44-jährig am 29. November 1992 verstarb.

Mit seinem Hobby, der Fotografie, erarbeitete sich Barry Ryan ab Mitte der 1980er Jahre einen Ruf als Fotograf. Seine Fotos hängen u. a. in der National Portrait Gallery in London und im Museum of Modern Art in New York.

Gelegentlich tritt Barry Ryan noch in Oldie-Shows im Fernsehen auf und präsentiert dort seine größten Hits (insbesondere Eloise). Im Jahr 1999 erschien gemeinsam mit der Formation Klaus & Klaus eine Neueinspielung von Zeit macht nur vor dem Teufel halt.

Eloise wurde 1981 von der Band The Teens und 1985 von der Punkband The Damned gecovert. Ein Remix des Originals kam 1990 in Deutschland noch einmal in den Verkauf.

1995 erschien in England die CD "Eloise", die neben Neuaufnahmen alter Stücke auch sechs von Barry Ryan  verfasste neue Titel enthielt. Ähnlich konzipiert war die 2005 erschienene CD "Hello...again.". Auf Ihr war der Anteil neuer Songs aber wesentlich größer und mit  "Kitsch" und Today" nur noch zwei Neueinspielungen alter Titel vorhanden. Der Vertrieb dieser CD erfolgte primär bei seinen Auftritten und über E-Bay.

Im Jahr 2014 nahm er zusammen mit dem australischen Opern-Bariton Barry Ryan und dem Sydney Lyric Orchestra eine neue Version von Eloise auf, die auf diversen Downloadportalen erworben werden konnte.

Für 2017 ist auf dem französischen  Label Rakel Productions ein Album mit dem Namen "This is now" angekündigt, das neben einem Eurodisco Remake von Eloise im Wesentlichen neues Material enthält. Bei zwei Stücken singt Barry Ryan  in französischer Sprache. Inzwischen teilte die Firma mit, dass es die neuen Songs nur über i-tunes und die Portale Deezer und Spotify  in Form von Downloads geben soll.

Im Oktober erschien das Material  in Frankreich dann doch auch als CD bei CDMC Music - Eine Veröffentlichung in Deutschland und England ist für 2018 geplant. Einen seiner seltenen Auftritte wird Barry Ryan im Rahmen des 2. Superfestes der Oldie Stars im Februar 2018 in Hamburg absolvieren.

sowie „Best-of“-Veröffentlichungen.
