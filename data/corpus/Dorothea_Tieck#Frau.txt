Dorothea Tieck (* März 1799 in Berlin; † 21. Februar[1]1841 in Dresden) war eine deutsche Übersetzerin. Zusammen mit ihrem Vater Ludwig Tieck und Wolf Heinrich Graf von Baudissin fertigte sie zahlreiche Übersetzungen von Werken William Shakespeares an, übersetzte aber auch anderes aus dem Spanischen und Englischen.

Dorothea Tieck wurde 1799 als älteste Tochter des Schriftstellers Ludwig Tieck und der Tochter des Theologen Julius Gustav Alberti, Amalie Alberti, in Berlin geboren. Bereits 1805 trat Dorothea Tieck unter dem Einfluss ihrer Mutter zum katholischen Glauben über. 

Schon in jungen Jahren zeigte sich Dorothea Tiecks Wissbegierde und Talent für Sprachen. Sie lernte Französisch, Englisch, Italienisch und Spanisch, aber auch Griechisch und Latein, sodass sie Shakespeares Werke, aber auch Calderón, Homer, Livius, Vergil, Dante und Horaz im Original lesen konnte. Im Jahr 1819 ging die Familie nach Dresden. Dorothea Tieck wurde in den folgenden Jahren die Gehilfin des Vaters und unterstützte ihn bei seinen Studien und Arbeiten. Sie übersetzte Werke Shakespeares, aber auch mehrere Übersetzungen aus dem Spanischen sind von ihr. Dorothea Tiecks Name wurde dabei nie genannt und oft durch den ihres Vaters ersetzt.

Der Tod ihrer Mutter 1837 stürzte Dorothea Tieck in Depressionen. Besonders litt das erklärte Lieblingskind Ludwig Tiecks[2] unter der neuen Beziehung des Vaters zur Gräfin  Finkenstein. Schwermütige Gedanken und Lebensunlust führten zu einem ständigen Kampf mit sich selbst, sodass sie sogar erwog, in ein Kloster zu gehen. Das Gefühl, den Vater als älteste Tochter umsorgen zu müssen, hielt sie davon ab. Neben ihrer literarischen Arbeit war die tiefreligiöse Dorothea Tieck auch in einem katholischen Frauenverein tätig und unterrichtete in einer Armenschule Mädchen aus den untersten Ständen in Handarbeiten.

Sie erkrankte an Masern und starb an einem hinzugetretenen Nervenfieber im Februar 1841 unverheiratet in Dresden. Sie wurde wie ihre Mutter auf dem Alten Katholischen Friedhof in Dresden beerdigt, beide Gräber sind jedoch nicht erhalten.[3] An Dorothea Tieck erinnert jedoch eine Gedenktafel auf dem Friedhof.

Ludwig Tieck hatte sich bereits sehr zeitig mit William Shakespeare beschäftigt. Schon 1796 fasste er den Plan, Shakespeares Gesamtwerk ins Deutsche zu übertragen. Sein Plan wurde durch August Wilhelm Schlegels Übersetzung von 14 Werken Shakespeares durchkreuzt, die ab 1797 erschienen. Ludwig Tieck wandte sich daher zuerst englischen Werken zu, deren Übersetzung 1823 unter dem Titel Shakespeare's Vorschule erschienen. Die Übersetzungen von Robert Greenes Die wunderbare Sage von Pater Baco und dem anonymen Arden von Feversham stammten dabei von Dorothea Tieck. 

Das nächste größere Projekt wurden die Sonette Shakespeares, deren Übersetzung durch die feste Strophenform jedoch ungleich schwerer war. In Ludwig Tiecks Aufsatz Über Shakespeares Sonette einige Worte, nebst Proben einer Übersetzung derselben, der 1826 in der Zeitschrift Penelope erschien, gab Ludwig Tieck zu, dass die Übersetzung der Sonette durch einen jüngeren Freund hergestellt worden sei.[4] Bei diesem handelte es sich um seine Tochter Dorothea, die ab 1820 sämtliche Sonette Shakespeares übersetzt hatte, von denen 25 in der Zeitschrift Penelope abgedruckt wurden.

Der Plan August Wilhelm Schlegels, eine Gesamtübersetzung der Werke Shakespeares zu liefern, war 1810 nach 14 Dramen abgebrochen. Ab 1825 übernahm Ludwig Tieck das Shakespeare-Projekt, der zu dem Zeitpunkt bereits an einer Übersetzung des Macbeth und des Stücks Love’s Labour’s Lost gearbeitet hatte. Doch schon 1830 beschrieb Ludwig Tieck seine Arbeit an der Shakespeare-Übersetzung deutlich passiver: 

„Der Verleger (Georg Andreas Reimer) hat mich aufgefordert, die damals angekündigte Ausgabe insofern zu besorgen, daß ich die Übersetzungen jüngerer Freunde, die ihre ganze Muße diesem Studium widmen können, durchsehe, und, wo es nötig ist, sie verbessere, auch einige Anmerkungen den Schauspielen zufüge.“

Neben Wolf Heinrich Graf von Baudissin handelte es sich bei den jungen Freunden auch um Dorothea Tieck. Diese Form der Arbeitsteilung entsprach jedoch weniger einem geplanten und durch den Verleger angeordneten Vorgehen, als vielmehr einem spontanen und unter Zeitdruck notwendigen Entschluss. Ludwig Tieck hatte von Reimer Zahlungen für die noch fehlenden Übersetzungen erhalten und war bereits regelmäßig gemahnt worden, dafür auch Ergebnisse zu liefern. Ludwig Tieck war jedoch durch zahlreiche Krankheiten und auch gesellschaftliche Verpflichtungen nicht in der Lage, die Übersetzungen der Werke vorzunehmen.

„Da faßten Tiecks älteste Tochter Dorothea und ich uns ein Herz und taten ihm den Vorschlag, viribus unitis die Arbeit zu übernehmen; […] Das Unternehmen hatte raschen Fortgang: im Verlauf von drittehalb Jahren wurden von meiner Mitarbeiterin Macbeth, Cymbeline, die Veroneser, Coriolanus, Timon von Athen und das Wintermärchen, von mir die noch übrigen dreizehn Stücke übersetzt. Tag für Tag von halb zwölf bis ein Uhr fanden wir uns in Tiecks Bibliothekszimmer ein: wer ein Stück fertig hatte, las es vor, die zwei andern  Mitglieder unseres Collegiums verglichen den Vortrag mit dem Original, und approbierten, schlugen Änderungen vor, oder verwarfen.“

In Zusammenarbeit mit Wolf Heinrich Graf von Baudissin übersetzte Dorothea Tieck zudem die Stücke Viel Lärm um Nichts und Der Widerspenstigen Zähmung und steuerte zu seiner Übersetzung des Stücks Verlorene Liebesmüh Sonette bei.

Macbeth hatte Ludwig Tieck bereits 1819 zu übersetzen begonnen. Dorothea Tieck beendete die fragmentarische deutsche Version 1833.

Dorothea Tieck fertigte auch Übersetzungen aus dem Spanischen und Englischen an, die jedoch anonym oder unter dem Namen Ludwig Tiecks erschienen. Im Jahr 1827 erschien Vicente Espinels Biografie Leben und Begebenheiten des Escudero Marcos Obrégon, die den Untertitel Aus dem Spanischen zum ersten Male in das Deutsche übertragen, und mit Anmerkungen und mit einer Vorrede begleitet von Ludwig Tieck trug. Ihre Übersetzung von Cervantes' Leiden des Persiles und der Sigismunda erschien 1838 anonym mit einem Vorwort von Ludwig Tieck. Friedrich von Raumer veranlasste Dorothea Tieck schließlich zur Übersetzung des Werkes Leben und Briefe George Washingtons von Jared Sparks, die 1841 erschien.

Dorothea Tieck hielt sich bei ihrer Arbeit stets im Hintergrund. Über ihre Arbeit als Übersetzerin äußerte sie sich 1831 in einem Brief an Friedrich von Uechtritz.

„Ich glaube, das Übersetzen ist eigentlich mehr ein Geschäft für Frauen als für Männer, gerade weil es uns nicht gestattet ist, etwas eigenes hervorzubringen.“

Dorothea Tieck blieb zeitlebens diesem Frauenbild verhaftet und veröffentlichte trotz ihres literarischen Talents keine eigenen Schriften. Sie akzeptierte das Zurücktreten hinter den Namen ihres Vaters und unterstützte die Geheimhaltung ihrer literarischen Tätigkeit sogar. 

Auch in ihrer Übersetzungsarbeit wurde Dorothea Tieck im Gegensatz zu August Wilhelm Schlegels „poetischen“ Übersetzungen nicht selbst kreativ tätig, sondern setzte die originalgetreue Wiedergabe des Textes an vorderste Stelle.

Bei allen Werken handelt es sich um Übersetzungen Dorothea Tiecks ins Deutsche.
