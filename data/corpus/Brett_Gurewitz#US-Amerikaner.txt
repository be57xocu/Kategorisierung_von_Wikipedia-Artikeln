Brett Gurewitz alias Mr. Brett (* 12. Mai 1962 in Los Angeles) ist Gitarrist und Songwriter der US-amerikanischen Band Bad Religion. Er ist Gründer und Besitzer des Independent-Labels Epitaph Records.

In Los Angeles gründete er 1979 zusammen mit seinen High-School-Mitschülern Greg Graffin, Jay Bentley und Jay Ziskrout die Band Bad Religion, in der er Gitarrist und Songwriter ist und die bis heute eine Institution im Punkrock darstellt. 1994 verließ er wegen bandinternen Streitigkeiten die Band, um sich vollständig seinem Label zu widmen. Auch Probleme mit Drogen sollen eine Rolle gespielt haben. Er gründete eine neue Band namens Daredevils (unter anderem mit Josh Freese von den Vandals und dem später erfolgreichen Regisseur Gore Verbinski), die jedoch nicht mehr als die Single Hate You (1996) veröffentlichte.

Nachdem er im Jahre 2000 für das Bad Religion-Album The New America wieder ein Lied mitschrieb (Believe It), kam er 2001 schließlich zu seiner alten Band zurück, bei der er nun zwar wieder am Songwriting beteiligt ist, diese aber aufgrund der Arbeit mit seinem Label nicht regelmäßig auf Tour begleiten kann. Da Brian Baker als sein damaliger Ersatz im Bad-Religion-Lineup verblieb, konnten die Gitarrenparts jedoch auch live weiterhin problemlos umgesetzt werden. 2002 erschien, nun mit drei Gitarristen im Studio, das „Reunion“-Album The Process of Belief und im Jahre 2004 das hochpolitische Album The Empire Strikes First.

2003 gründete er zusammen mit den Brüdern Atticus und Leopold Ross sowie Greg Puciato die Band Error, die allerdings überwiegend elektronische Musik macht.

How Could Hell Be Any Worse? (1982) • Into the Unknown (1983) • Suffer (1988) • No Control (1989) • Against the Grain (1990) • Generator (1992) • Recipe for Hate (1993) • Stranger Than Fiction (1994) • The Gray Race (1996) • No Substance (1998) • The New America (2000) • The Process of Belief (2002) • The Empire Strikes First (2004) • New Maps of Hell (2007) • The Dissent of Man (2010) • True North (2013)

Bad Religion • Back to the Known
