Bertha Dudde (* 1. April 1891 in Liegnitz, Schlesien, heute Legnica; † 18. September 1965 in Leverkusen) war eine Schneiderin, sie selbst bezeichnete sich als Prophetin. 

Bertha Dudde wuchs mit sechs Geschwistern als zweitälteste Tochter eines Kunstmalers in ärmlichen Verhältnissen auf. Sie genoss lediglich die damals übliche Volksschulbildung und übte bereits früh ihre Neigung zum Schneidern praktisch aus, um die Familie finanziell zu unterstützen. Dieser Tätigkeit ging sie noch bis ins hohe Alter nach, um sich den Lebensunterhalt dadurch selbst zu erwirtschaften.

Ihre Eltern gehörten unterschiedlichen christlichen Konfessionen an und erzogen diesbezüglich ihre Kinder in relativer religiöser Freizügigkeit. Sie hatte daher kaum nennenswertes konfessionelles Wissen, denn sie hielt sich selbst von kirchlichem Einfluss fern, weil sie diesen Lehren ablehnend gegenüberstand. Am 15. Juli 1937 behauptete sie erstmals, eine innere Stimme wahrzunehmen.[1]
Ihr Gesamtwerk der aufgenommenen Offenbarungen besteht aus 9030 fortlaufend nummerierten und datierten Einzelkundgaben, mit jeweils unterschiedlichen und abgeschlossenen Inhalten, die sich erst nachträglich zu Themenschwerpunkten zusammenfassen lassen.

Die Kundgabentexte erheben inhaltlich den Anspruch, göttlichen Ursprungs zu sein und werden von Befürwortern den Neuoffenbarungen zugerechnet. 

Die Weltanschauung aus der Gesamtheit der Kundgaben entwickelt ein weitreichendes, deutlich christozentrisches Weltbild, das in Details auch gnostische, buddhistische und hinduistische Einfärbungen aufweist. 

Das daraus folgende Gottes- und Menschenbild weicht wesentlich von dem propagierten des traditionellen Christentums ab.

Aus geisteswissenschaftlicher Sicht existieren derzeit einige Abhandlungen über unterschiedliche Einzelaspekte, eine erschöpfende inhaltliche Untersuchung und Bewertung des Werkes in seiner Gesamtheit steht noch aus.

Ihre Gegner bezweifeln ihre Behauptung, als eine Offenbarung göttlichen Ursprungs zu wirken. Sie werfen ihr Unkonkretheit, Ähnlichkeiten zu Prophezeiungen anderer Seher und teilweise existentielle Widersprüche zur Bibel vor.
Dudde führt in ihrem Werk auch zahlreiche "Erklärungen" zu naturwissenschaftlichen Fragen an, insbesondere der Astronomie. Diese halten jedoch oft einer näheren wissenschaftlichen Überprüfung nicht stand, so wird beispielsweise der Gasplanet Saturn als Planet aus durchsichtigem Metall beschrieben[2] und es wird behauptet, dass andere Sterne ohne unsere Sonne nicht existieren könnten.

Obwohl sich noch zu Lebzeiten Bertha Duddes ein kleiner Kreis von Anhängern um sie bildete, ist daraus bislang keine eigenständige, nach außen hin abgegrenzt wahrnehmbare Bewegung oder Gemeinschaft hervorgegangen, was wohl auch in Zukunft nicht zu erwarten sein wird.

Die Begründung dafür findet sich in den inhaltlichen Aussagen des Werkes, die sämtlich auf formelle Dogmatik, sakramentale Handlungen und liturgische Rituale hin auslegbare Argumentation verzichten. Dadurch ergibt sich eine Universalität der geistigen Aussagen, die somit überkonfessionell oder konfessionsübergreifend, bzw. konfessionell ungebunden verstanden werden können.
In der Folge finden sich Anhänger des Werkes in allen christlichen Religionsgemeinschaften, wie auch unter konfessionell nicht organisierten Menschen, was eine Einordnung in übliche theologische Kategorien erschwert, weil offensichtlich das Hauptgewicht nicht auf einer formell nach außen hin sichtbaren Erscheinungsform, sondern einer innerlich wirksamen, geistigen Entwicklung liegt.
