Friedrich Adolf Muschg (* 13. Mai 1934 in Zollikon, Kanton Zürich; heimatberechtigt in Zollikon und Männedorf) ist ein Schweizer Dichter, Schriftsteller und Literaturwissenschaftler. 

Adolf Muschg wurde 1934 als Sohn von Adolf Muschg senior (1872–1946) und dessen zweiter Frau geboren. Sein Halbbruder Walter Muschg war damals bereits Mitte dreissig. Seine Halbschwester Elsa (* 1899) war eine erfolgreiche Kinderbuchautorin.[1]
Von 1946 bis 1953 besuchte Adolf Muschg ein Gymnasium in Zürich. Er verbrachte zwei Jahre auf einem Internat in Schiers und legte schließlich die Matura am Literargymnasium Rämibühl in Zürich ab. Anschließend studierte er Germanistik, Anglistik sowie Philosophie in Zürich und Cambridge und promovierte über Ernst Barlach.

Von 1959 bis 1962 unterrichtete er als Hauptlehrer für Deutsch an der Kantonalen Oberrealschule, dann folgten verschiedene Stellen als Hochschullehrer, unter anderem in Deutschland (Universität Göttingen), Japan und den USA. 1967 heiratete er Hanna Meyer.

1969 gehörte er mit Max Frisch, Friedrich Dürrenmatt, Peter Bichsel und anderen zu den Sezessionisten, die aus dem Schweizer Schriftstellerverband austraten und die Gruppe Olten ins Leben riefen, die formal am 25. April 1971 in Biel gegründet wurde.  

1970 bis 1999 war er Professor für deutsche Sprache und Literatur an der Eidgenössischen Technischen Hochschule Zürich. 

1975 war Muschg Kandidat der Zürcher Sozialdemokratischen Partei für den Ständerat. Er wurde zwar nicht gewählt, äusserte sich aber nach wie vor regelmässig zu politischen Zeitfragen.

Von 1988 bis 1993 moderierte er im Fernsehprogramm Südwest 3 die Sendung Baden-Badener Disput; danach übernahm Gertrud Höhler die Moderation.

1991 heiratete er in dritter Ehe die Japanerin Atsuko Kanto.[2]
1997 hielt er die Rede zur Eröffnung des Deutschen Germanistentages in Bonn.[3]
Seit 1976 ist er Mitglied der Akademie der Künste in Berlin, daneben ist er Mitglied der Akademie der Wissenschaften und der Literatur in Mainz[4] sowie der Deutschen Akademie für Sprache und Dichtung in Darmstadt und der Freien Akademie der Künste Hamburg. 2003 wurde er zum Präsidenten der Akademie der Künste in Berlin gewählt. Von diesem Amt trat er am 15. Dezember 2005 überraschend zurück. Grund für diese Entscheidung seien «unüberbrückbare Differenzen mit dem Senat der Akademie». Er befand, der Umzug in den Neubau am Pariser Platz sei nicht dafür genutzt worden, die Aktivitäten der Akademie stärker in die Öffentlichkeit zu tragen. Im Februar 2009 erklärte Muschg nach 35 Jahren das Ende seiner Zusammenarbeit mit dem Suhrkamp Verlag und seinen Wechsel zum Verlag C. H. Beck.[5]
Muschg lebt in Männedorf bei Zürich. Seit 2014 ist er Ehrenbürger der Gemeinde. Sein Archiv befindet sich im Schweizerischen Literaturarchiv in Bern.

Am 15. März 2010 veröffentlichte er in der Berliner Tageszeitung Der Tagesspiegel und im Zürcher Tages-Anzeiger einen Beitrag, in dem er Gerold Becker, den ehemaligen Direktor der Odenwaldschule, gegen den Vorwurf des Missbrauchs mehrerer seiner Schüler verteidigte. Die Berichterstattung über die Missbrauchsvorwürfe bezeichnete er als „Kampagne“ und „Heuchelei“. Er stellte vielmehr eine Verbindung zwischen dem von dem griechischen Philosophen Platon formulierten „pädagogischen Eros“ und den von Becker nicht bestrittenen sexuellen Handlungen an mehreren seiner Schüler her.[6] Am 16. März distanzierte sich der Chefredakteur des Tagesspiegels, Lorenz Maroldt, von Muschgs Beitrag in dem Leitartikel der Zeitung.[7]
Schon früh litt Muschg unter einer extremen Form der Hypochondrie, die so weit ging, dass er sich einmal einer Gehirnoperation unterzog, um von einem nicht vorhandenen Tumor geheilt zu werden, und einmal einer Blinddarmoperation, zu der er aus den Vereinigten Staaten nach Zürich flog.[8]
Mit Selbstironie erzählt er von seiner nachgeholten Hochzeitsreise 1968 auf einem Frachtschiff, wie er zum Schrecken des Kapitäns wurde, als er, einige Tage vom nächsten Hafen (und Krankenhaus) entfernt, behauptete, er habe einen vereiterten Blinddarm, der sofort operiert werden müsste. Der Erste Offizier ließ sich die Symptome schildern, gab sie per Funk an eine Klinik in Danzig weiter und kam dann mit der beruhigenden Mitteilung zu Muschg, es sei kein Blinddarmdurchbruch, sondern ein Magenkrebs, und der ließe noch viel Zeit für eine Operation. Selbstverständlich war Muschg kerngesund.[9]
Das Thema der Hypochondrie erscheint auch in seinem Werk, zum Beispiel in der Erzählung Ihr Herr Bruder [10]. In den „Frankfurter Vorlesungen“ wird die Entstehung der Geschichte unter der Überschrift analysiert: „Wie ich Raimund für mich sterben ließ.“ [11] Im Theaterstück Rumpelstilz (uraufgeführt 1968) hat der Protagonist, der Gymnasialprofessor Viktor Leu, Schluckbeschwerden und bildet sich Kehlkopfkrebs ein.

Sonstige Schriften
