Karl Blömer (* 1937) ist ein deutscher Bodybuilder und Unternehmer, der die Titel des Mr. Germany und Mr. Universum gewann.

Karl Blömer wuchs in einem Dorf in der Nähe von Köln auf und erlernte einen handwerklichen Beruf. Bis zu seinem 18. Geburtstag betrieb er Turnen und Boxen, bevor ihn ein Wochenschau-Bericht über eine Mr.-Universum-Wahl dazu brachte, mit dem Bodybuilding zu beginnen. 1963 stellte ihn eine Kölner Judo-Schule als Trainer in der Bodybuilding-Abteilung ein. 1966 erwarb er das Studio, ein Jahr später nahm zum ersten Mal an einem Wettkampf teil: Er wurde Vierter bei der Wahl zum Mr. Universum der Amateure. Den Titel des Mr. Germany gewann er 1970. Ein Jahr später wurde er Mr. Universum in der "tall class" des IFBB.

Mit 41 Jahren gelang ihm ein Comeback, als er mit Jusup Wilkosz und Reinhard Heyd in der Mannschaftswertung des Mr.-Universum-Wettbewerbs den fünften Platz belegte.

Außer in Köln betrieb Blömer noch Fitnessstudios in Essen und Düsseldorf, von denen er die letzten um die Jahrtausendwende aus Altersgründen verkauft hat.

In den siebziger und achtziger Jahren wirkte Blömer vereinzelt in Kino- und Fernsehfilmen mit.
