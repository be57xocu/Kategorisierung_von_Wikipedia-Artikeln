Monique Truong (* 1968 in Saigon) ist eine US-amerikanische Schriftstellerin vietnamesischer Abstammung.

Monique Truong zog im Alter von sechs Jahren in die USA. Sie studierte an der Yale University und der Columbia University School of Law.

2004 erhielt Truong für The Book of Salt den Stonewall Book Award und wurde zusammen mit zwei weiteren Autoren mit dem Robert-Bingham-Stipendium des amerikanischen PEN-Zentrums ausgezeichnet, um sie bei der Arbeit an ihrem zweiten Buch zu unterstützen.
