Percival Lowell (* 13. März 1855 in Boston; † 12. November 1916 in Flagstaff[1]) war ein US-amerikanischer Astronom. Er begann als Amateurforscher, wurde aber zu einem der bekanntesten Astronomen seiner Zeit und zum Gründer des Lowell-Observatoriums.

Er entstammte einer der reichsten und angesehensten Patrizierfamilien Bostons. Seine Schwester Amy Lowell brachte es als Dichterin, sein Bruder Abbott Lawrence Lowell als Präsident der Harvard-Universität zu Ruhm.[2] Sein Cousin Abbott Lawrence Rotch gründete das meteorologische Blue-Hill-Observatorium Boston und führte den Wetterdrachen in die Aerologie ein.

Lowells stets vorhandenes Interesse an der Astronomie wurde durch die Planeten-Beobachtungen von Giovanni Schiaparelli erhöht, insbesondere durch dessen Entdeckung der „Marskanäle“ bei der günstigen Opposition des Jahres 1877. Ausgelöst durch jahreszeitliche Verfärbungen am Mars entstand dadurch in Lowell und Camille Flammarion die Vorstellung, die „Canali“ könnten künstlich gebaute Kanäle zur Bewässerung des austrocknenden Roten Planeten sein.

Diese Meinung traf auf die schon damals für jede Sensation offenen Massenmedien, und es entstanden die „Marsianer“-Manie und eine Reihe von Science-Fiction-Literatur. Erst durch die Raumsonden der 1960er-Jahre wurden die Canali als reale Struktur widerlegt, lassen sich aber teilweise als visuelle Linienverstärkung von Terrassenrändern und Lichtkanten, Kraterreihen oder Canyons wie das Valles Marineris erklären.

Schon bald machte Lowell aus seiner Liebhaberei eine ernste und erfolgreiche Wissenschaft. Um die Planetologie zu fördern, gründete der begüterte Hobbyastronom Lowell im Jahre 1894 das Lowell-Observatorium in Flagstaff, Arizona. Es gelang ihm, ausgezeichnete Fachleute (z. B. A. E. Douglass) für sein gut ausgestattetes Institut zu gewinnen.

Es wurden intensive Studien des Mars, der Venus und anderer Planeten sowie der Milchstraße durchgeführt. 1930 wurde dort der lange als Planet eingestufte Pluto entdeckt und erhielt – aus doppeltem Grund – die Abkürzung „PL“. Bis heute hat das Institut einen ausgezeichneten Ruf und widmet sich derzeit der Asteroiden-Forschung.

Der Asteroid (1886) Lowell ist nach ihm benannt.[3]