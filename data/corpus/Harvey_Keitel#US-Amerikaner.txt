Harvey Keitel ['hɑːvɪ kaɪ'tɛl] (* 13. Mai 1939 in Brooklyn, New York) ist ein US-amerikanischer Schauspieler. Er gilt  als herausragender Vertreter des Method Acting.

Harvey Keitel ist ein Sohn jüdischer Emigranten, sein Vater Harry Keitel kam aus Polen und seine Mutter Miriam aus der Maramuresch in Rumänien.[1] Keitel wurde in Brighton Beach, Brooklyn geboren und wuchs gemeinsam mit seiner Schwester Renee und seinem Bruder Jerry in ärmlichen Verhältnissen auf. Nach der High School ging er sofort für ein Jahr zu den Marines, um sich finanziell über Wasser halten zu können. Er wurde kurz vor Beginn des Vietnamkriegs entlassen und gilt seitdem als überzeugter Pazifist.

1967 nahm Keitel Schauspielunterricht bei Frank Corsaro und Stella Adler und lernte von ihnen die Technik des Method Acting. Als Mitglied des renommierten Actors Studio war er ab Mitte der 1960er Jahre auf vielen New Yorker Bühnen zu sehen. Sein Debüt am Broadway gab er in einem Stück von Arthur Miller.
1968 wirkte Keitel in Wer klopft denn da an meine Tür?, der Abschlussarbeit des damaligen Regie-Studenten Martin Scorsese, mit und freundete sich mit diesem an. Scorsese besetzte ihn später auch für seine Erfolgsfilme Hexenkessel (1973), Alice lebt hier nicht mehr (1974), Taxi Driver (1976) und Die letzte Versuchung Christi (1988). Keitel wurde von Regisseur Francis Ford Coppola als Hauptdarsteller von Apocalypse Now (1979) verpflichtet, aber nach zwei Wochen entlassen, da Coppola mit seiner Darstellung unzufrieden war (Martin Sheen übernahm die Rolle).

1993 erhielt Keitel einen Independent Spirit Award in der Kategorie Bester Schauspieler für seine Darstellung in Abel Ferraras Low-Budget-Film Bad Lieutenant. Er spielt darin einen abgehalfterten, korrupten Polizisten, der in einer Art Passionsgeschichte seinen Seelenfrieden und Erlösung findet, kurz bevor er in seinem Auto erschossen wird.
In diesem Film wie auch in Ridley Scotts Debüt Die Duellisten (1976) zeigte Keitel, dass er zu den führenden amerikanischen Charakterdarstellern zählt.
Den endgültigen internationalen Durchbruch schaffte Keitel dann wiederum mit dem Erstlingswerk eines Regisseurs, Reservoir Dogs von Quentin Tarantino. Bereits sein nächster Film Bugsy brachte ihm 1991 eine Oscar-Nominierung als Bester Nebendarsteller ein.

In den folgenden Jahren war er unter anderem in den Kassenschlagern Thelma & Louise, Sister Act, Bad Lieutenant, Jane Campions Das Piano und Die Wiege der Sonne zu sehen. Kultstatus erlangte er durch Pulp Fiction (Quentin Tarantino) und From Dusk Till Dawn (Robert Rodriguez/Quentin Tarantino).

Keitel erwarb sich während seiner langen Karriere eine Reputation als herausragender Vertreter des Method Acting, zählte aber nie zu den kassenträchtigen Hollywood-Stars (wie etwa Al Pacino oder Robert De Niro). Allerdings war der Darsteller in zahlreichen Kassenhits in markanten Nebenrollen zu sehen. Profilierte Hauptrollen spielte er in der Regel in künstlerisch ambitionierten Produktionen mit geringem Budget.
Der Erfolg einiger Filme mit ihm war allerdings vorher nicht abzusehen. Keitel förderte mit der Übernahme vieler Rollen vielmehr Talente wie Quentin Tarantino oder Kunstfilmer wie Abel Ferrara. Für den Film Smoke erhielt er 1995 auf der 
Berlinale zusammen mit Regisseur Wayne Wang den Spezialpreis der Jury, den Silbernen Bären.

Keitel ist auf Deutsch von Christian Brückner, Fred Maire und Joachim Kerzel synchronisiert worden.[2]
Harvey Keitel ist seit 2001 mit der Schauspielerin Daphna Kastner verheiratet, mit der er einen Sohn hat. Aus einer früheren Beziehung mit Lorraine Bracco hat er eine Tochter.
