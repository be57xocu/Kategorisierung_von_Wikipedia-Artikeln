Adrian R’Mante (* 3. Februar 1978 in Florida, USA) ist ein US-amerikanischer Schauspieler, der wohl am besten als Esteban Julio Ricardo Montoya de la Rosa Ramirez in der TV-Serie Hotel Zack & Cody bekannt ist.

R’Mante machte seinen Abschluss an der University of Central Florida. Danach hatte er kleinere Theaterauftritte. Erste Bekanntheit erlangte R’Mante jedoch erst als Showmaster in der Gameshow Game Lab in Orlando. Er trat auch in anderen TV-Shows bei Nickelodeon und im Discovery Channel als Showmaster auf.

In den späten 1990er Jahren moderierte R’Mante Teile der Game Show Network, die ihn auch bekannt machte. Er ist auch bekannt für seine Arbeit in der einst populären TV-Serie Summerland, die aber nicht mehr läuft.

Neben seinem Auftritt in Hotel Zack & Cody hatte R’Mante noch zahlreiche Gastauftritte in berühmten TV-Serien wie in CSI: Las Vegas, CSI: NY, Alias – Die Agentin und 24.

Filmauftritte hatte R’Mante nur wenige. Er trat eher überwiegend in TV-Serien auf. Seine wenigen Filme waren unter anderem Furz – Der Film, All or Nothing und S1m0ne.

R'Mante lebt zurzeit in Kalifornien, wo er gelegentlich als Ersatzlehrer an der Culver City High School arbeitet.
