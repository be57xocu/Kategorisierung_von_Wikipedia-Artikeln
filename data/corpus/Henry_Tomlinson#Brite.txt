Henry Major Tomlinson (* 21. Juni 1873 in London; † 5. Februar 1958 in London) war ein britischer Schriftsteller und Journalist. Er schrieb Anti-Kriegs- und Reise-Bücher, Romane und Kurzgeschichten, speziell für das Leben auf dem Meer. In Deutschland ist sein Werk weniger bekannt.

Tomlinson wurde in Poplar (London) geboren, wo er auch seine Jugend verbrachte. Zunächst arbeitete er als Frachtangestellter und später als Reporter für die Zeitung "Morning Leader". Hierfür reiste er entlang des Amazonas.

Im Ersten Weltkrieg wurde er in Frankreich als offizieller Berichterstatter der britischen Armee eingesetzt. Im Jahre 1917 wandte er sich wieder der Organisation "The Nation" mit H. W. Messingham zu. "The Nation" war eine strikte Anti-Kriegs-Organisation. 1923 beendete er seine dortige Tätigkeit, als sich Messingham aufgrund von Besitz- und politischem Linienwechsel der Organisation ebenfalls zurückzog.
