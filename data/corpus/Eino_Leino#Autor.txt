Eino Leino [ˈɛinɔ ˈlɛinɔ] (eigentlich Armas Eino Leopold Lönnbohm; * 6. Juli 1878 in Paltamo; † 10. Januar 1926 in Tuusula) war ein finnischer Schriftsteller.

Leino gab 1895 sein Studium in Helsinki auf und wurde Literaturkritiker und Feuilletonist bei verschiedenen finnischen Zeitungen. Zwischen 1909 und 1910 unternahm Leino Reisen durch Deutschland, Italien und Schweden. Seine ersten lyrischen Anthologien wurden von Heinrich Heine und Johan Ludvig Runeberg inspiriert. Auch dem Kalevala stand seine Lyrik nahe.

Unter dem Eindruck der Werke von Gerhart Hauptmann, Maurice Maeterlinck und Gabriele D’Annunzio wurde Leino veranlasst, das finnische Theater wieder zur Heimstätte reiner Dichtung zu machen. Mit seinem Schaffen wurde Leino eine der repräsentativsten Persönlichkeiten der literarischen Neuromantik Finnlands. Als Lyriker gilt Leino inzwischen als Klassiker, auf den sich die Moderne immer wieder beruft. Auch als Übersetzer hat Leino sich umfangreich betätigt: Werke von Johan Ludvig Runeberg, Zacharias Topelius, Dante, Johann Wolfgang von Goethe, Friedrich Schiller, Jean Racine, Pierre Corneille oder Anatole France sind von ihm übersetzt worden.

Eino Leino starb am 10. Januar 1926 in Tuusula und wurde auf dem Friedhof Hietaniemi in Helsinki beigesetzt.
[1]
Zu seinem Gedenken ist der 6. Juli in Finnland als Eino Leinon päivä / runon ja suven päivä („Eino-Leino-Tag / Tag der Dichtung und des Sommers“) seit 1992 Flaggentag.
