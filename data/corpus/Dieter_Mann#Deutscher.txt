Dieter Mann (* 20. Juni 1941 in Berlin-Tiergarten) ist ein deutscher Theater- und Filmschauspieler.

Mann besuchte die Grundschule in Berlin-Pankow. Er absolvierte eine Lehre als Dreher im VEB Kühlautomat Berlin. Von 1955 bis 1957 war er als Facharbeiter im VEB Schleifmaschinenwerk Berlin tätig. Nach dem Abitur an der Arbeiter-und-Bauern-Fakultät Friedrich Engels in Berlin begann er 1962 ein Studium an der Staatlichen Schauspielschule in Berlin.

Noch während des Studiums wurde Mann 1964 von Friedo Solter an das Deutsche Theater (DT) verpflichtet und hatte seinen ersten großen Erfolg als Wolodja in Unterwegs von Wiktor Rosow.

Mann war von 1964 bis 2006 festes Mitglied im Ensemble des Deutschen Theaters in Berlin. Von 1984 bis 1991 war er Intendant des Deutschen Theaters und holte unter anderem Frank Castorf und Heiner Müller als Regisseure an das Haus. 

Am Deutschen Theater spielte er eine Vielzahl von Rollen in zeitgenössischen und klassischen Stücken, u. a. den Tempelherren im Nathan, den Clavigo, den Edgar Wibeau in Die neuen Leiden des jungen W., den Truffaldino in Goldonis Diener zweier Herren, den Ariel im Sommernachtstraum, den Wehrhahn in Hauptmanns Biberpelz, den Kreon in der Antigone von Sophokles und den Odysseus in Ithaka von Botho Strauß. Außerdem trat er mit literarischen Soloabenden auf.

Als Gast ist Mann nach Ausscheiden aus dem festen Engagement 2006 dem Deutschen Theater weiterhin verbunden geblieben und tritt zudem in diversen Rollen am Berliner Ensemble und am Staatsschauspiel Dresden auf. Daneben arbeitet er für Kino, Fernsehen und Hörfunk. Er ist Honorarprofessor an der Hochschule für Schauspielkunst „Ernst Busch“ Berlin.

Engagements als Gastschauspieler führten ihn an das Deutsche Schauspielhaus Hamburg, das Schauspiel Frankfurt, die Wiener Festwochen, die Sächsische Staatsoper Dresden sowie an das Staatsschauspiel Dresden, die Bregenzer Festspiele, das Düsseldorfer Schauspielhaus, das Wiener Burgtheater und das Theater Basel. Daneben hat er an vielen Kino-, Fernseh- und Hörspielproduktionen mitgewirkt, im Fernsehen u.a. in der Serie Der letzte Zeuge.

Mann lebt mit seiner Frau bei Königs Wusterhausen am Krüpelsee.
