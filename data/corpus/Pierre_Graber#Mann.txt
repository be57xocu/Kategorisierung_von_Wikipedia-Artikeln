Pierre Graber (* 6. Dezember 1908 in La Chaux-de-Fonds; † 19. Juli 2003 in Lausanne; heimatberechtigt in La Chaux-de-Fonds und Langenbruck) war ein Schweizer Politiker (SP) aus dem Kanton Neuenburg. Als Mitglied des Bundesrates stand er während acht Jahren dem Politischen Departement vor und war damit für die schweizerische Aussenpolitik zuständig. Er bekleidete 1975 das Amt des Bundespräsidenten und unterzeichnete für die Schweiz die KSZE-Schlussakte von Helsinki. Graber war drei Mal verheiratet: 1931 heiratete er Margarete Gawronsky, diese Ehe wurde 1936 geschieden; 1939 verheiratete er sich mit Lina Pierrette Meilland, die 1974 verstarb; in dritter Ehe war er ab 1977 vermählt mit Renée Noverraz (geborene Renée Avilon)[1].

Pierre Grabers Vater Ernest-Paul Graber war Politiker der Sozialdemokratischen Partei (SP) und Publizist. Pierre Graber studierte Rechts-, Handels- und Verwaltungswissenschaften an der Universität Neuenburg und wurde 1933 Rechtsanwalt in Lausanne.  Er gehörte der  (SP) an.  Er übte seinerzeit die folgenden politischen Mandate aus:

Er wurde am 10. Dezember 1969 in den Bundesrat gewählt und trat sein Amt am 1. Februar 1970 an. Während seiner Amtszeit stand er dem Politischen Departement vor. Graber war Bundespräsident im Jahre 1975 und Vizepräsident im Jahre 1974. Am 31. Januar 1978 trat er als Bundesrat zurück. 2003 starb er in Lausanne an Herzversagen.[2]
Anfang 2016 wurde aufgrund Recherchen der Neuen Zürcher Zeitung der Vermutung geäussert, dass Graber 1970 unter Vermittlung von Jean Ziegler mit der damals offen terroristisch agierenden Palästinensischen Befreiungsorganisation PLO ein geheimes Stillhalteabkommen – ohne Wissen seiner Bundesratskollegen – schloss: Die Schweiz sollte demnach von weiteren terroristischen Aktionen verschont bleiben, dafür unterstützte sie die PLO in ihrem Bemühen um diplomatische Anerkennung am Uno-Sitz in Genf. Kurz darauf verzichtete man aus unbekannten Gründen auf die Anklageerhebung gegen einen palästinensischen Verdächtigen des Anschlages auf den Swissair-Flug 330 mit 47 Toten.[3][4] Diese Darstellung wiederum wird durch andere Zeitzeugen und Quellen in Zweifel gezogen.[5][6] Eine Arbeitsgruppe der Bundesverwaltung fand keine Hinweise, welche die These eines Geheimabkommens bestätigt hätten.[7]
Jonas Furrer |
Henri Druey |
Josef Munzinger |
Wilhelm Matthias Naeff |
Friedrich Frey-Herosé |
Jakob Stämpfli |
Constant Fornerod |
Josef Martin Knüsel |
Jakob Stämpfli |
Jakob Dubs |
Karl Schenk |
Josef Martin Knüsel |
Emil Welti |
Paul Cérésole |
Johann Jakob Scherer |
Joachim Heer |
Bernhard Hammer |
Numa Droz |
Simeon Bavier |
Louis Ruchonnet |
Adolf Deucher |
Adrien Lachenal |
Eugène Ruffy |
Eduard Müller |
Walter Hauser |
Ernst Brenner |
Josef Zemp |
Robert Comtesse |
Marc Ruchet |
Ludwig Forrer |
Eduard Müller |
Arthur Hoffmann |
Gustave Ador |
Felix Calonder |
Giuseppe Motta |
Marcel Pilet-Golaz |
Max Petitpierre |
Friedrich Traugott Wahlen |
Willy Spühler |
Pierre Graber |
Pierre Aubert |
René Felber |
Flavio Cotti |
Joseph Deiss |
Micheline Calmy-Rey |
Didier Burkhalter |
Ignazio Cassis

Samuel Jacques Hollard |
Charles-Marc Secretan |
Édouard Dapples |
Victor Gaudard |
Édouard Dapples |
Louis Joël |
Samuel Cuénoud |
Berthold van Muyden |
Louis Gagnaux |
Berthold van Muyden |
André Schnetzler |
Paul Maillefer |
Arthur Freymond |
Paul Rosset |
Paul Perret |
Emmanuel Gaillard |
Arthur Maret |
Jules-Henri Addor |
Pierre Graber |
Jean Peitrequin |
Georges-André Chevallaz |
Jean-Pascal Delamuraz |
Paul-René Martin |
Yvette Jaggi |
Jean-Jacques Schilt |
Daniel Brélaz
