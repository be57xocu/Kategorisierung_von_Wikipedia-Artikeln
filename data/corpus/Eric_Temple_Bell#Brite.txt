Eric Temple Bell (* 7. Februar 1883 in Peterhead; † 21. Dezember 1960 in Watsonville) war ein schottisch-amerikanischer Mathematiker, der Bücher zur Geschichte der Mathematik und zahlreiche Arbeiten zur Zahlentheorie, Kombinatorik und Analysis veröffentlichte.

Bell lebte seit 1903 in den USA. Er besuchte die Stanford University und die Columbia University. Seit 1912 lehrte er Mathematik an der University of Washington und später am California Institute of Technology. 
Im Jahre 1924 bekam er den Bôcher Memorial Prize für seine Abhandlung Arithmetical paraphrases.

Bell ist einem breiteren Publikum vor allem durch seine Bücher bekannt geworden, insbesondere für seine klassische Sammlung von Mathematiker-Biographien Men of Mathematics (1937), das heute noch nachgedruckt wird. Weitere Veröffentlichungen sind Algebraic Arithmetic (1927), The Development of Mathematics (1940) und Mathematics, Queen and Servant of Science (1951). 

Nach ihm wurde die Bellsche Zahl Bn{\displaystyle B_{n}} benannt, die die Anzahl der Partitionen einer n-elementigen Menge beschreibt.

Unter dem Pseudonym John Taine schrieb Bell auch Science-Fiction-Romane.
