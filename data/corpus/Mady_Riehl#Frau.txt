Mady Riehl, eigentlich Helga Riehl, geb. Weinert (* 30. September 1940 in Berlin) ist eine deutsche ehemalige Schlagersängerin, Schauspielerin und Fernsehmoderatorin.

Mady Riehl wuchs in Leipzig auf. Im Jahr 1955 siedelte sie in die Bundesrepublik über. Dort durchlief sie eine kaufmännische Lehre und nahm anderthalb Jahre Gesangs- und Sprechunterricht. Mit 17 Jahren versuchte sie sich kurzzeitig als Schlagersängerin und später auch als Schauspielerin. Im Jahr 1959 debütierte sie als Filmdarstellerin in der Komödie Und noch frech dazu …, im Jahr darauf wirkte sie unter der Regie von Paul Martin in O sole mio mit. Nachdem sie im Filmgeschäft nicht richtig Fuß fassen konnte, verdiente sich die Berlinerin ihren Lebensunterhalt als Sekretärin.

1963 wurde Riehl neben Victoria Voncampe und Renate Oelschläger eine der ersten Ansagerinnen des neugegründeten ZDF. Bekannt wurde sie vor allem durch die Spielshow Dalli Dalli von Hans Rosenthal, in der Riehl zwischen 1971 und 1986 neben Brigitte Xander und Ekkehard Fritsch (später: Christian Neureuther) der Jury angehörte. Besonders bekannt wurde sie durch ihren Standardsatz „… wir müssen leider einen Punkt abziehen …“.[1]
Mady Riehl war auch als Programmansagerin tätig, so ab 1971 im Hörfunk des Hessischen Rundfunks.

Sie war neben ihrer Arbeit als ZDF-Programmansagerin von 1986 bis 1991 auch als Sprecherin der heute-Nachrichten tätig.

2000 erkrankte Riehl an Krebs. Die Krankheit konnte geheilt werden.[1]
In erster Ehe war Mady Riehl mit ihrem ZDF-Kollegen, dem Nachrichtensprecher Heinz Wrobel, verheiratet. Von 1971 bis zu dessen Tod im März 2015 war sie mit dem Fernsehjournalisten, Drehbuchautor und Regisseur Hajo Schedlich verheiratet. Sie lebt heute in Mainz.[1]
Ihre bekanntesten Schlager waren:
