Andrew Jackson (* 15. März 1767 in den Waxhaws; † 8. Juni 1845 nahe Nashville, Tennessee) war ein US-amerikanischer Politiker und von 1829 bis 1837 der siebte Präsident der Vereinigten Staaten. Ferner ist er gemeinsam mit Martin Van Buren der Gründer der Demokratischen Partei der USA.

Jackson entstammte sehr einfachen Verhältnissen und begann sich durch seine Teilnahme im Krieg von 1812 und später in diversen Indianerfeldzügen das Ansehen seiner Landsleute zu erwerben. Politisch trat er zunächst als Senator und Militärgouverneur von Florida in Erscheinung. Sein Ruhm als militärischer Befehlshaber ließ ihn 1824 als Demokratischer Republikaner erstmals für das Präsidentenamt kandidieren. Obwohl er von insgesamt vier Kandidaten derselben Partei eine relative Mehrheit an Stimmen im Popular Vote sowie an Wahlmännern erhielt, reichte es im Wahlmännergremium nicht für die zum Sieg erforderliche absolute Mehrheit, und das Repräsentantenhaus bestimmte unter der Vermittlung Henry Clays John Quincy Adams zum Präsidenten.

Schon wenig später bereitete er sich für die Wahl von 1828 auf eine erneute Bewerbung um das höchste Staatsamt vor. Während dieser Zeit widmete er sich intensiv dem Aufbau der neugegründeten Demokratischen Partei, die er von der damaligen Demokratisch-Republikanischen Partei loslöste. Nach einem äußerst heftig ausgetragenen Wahlkampf konnte er Adams souverän besiegen und das Präsidentenamt im März 1829 antreten. Im Herbst 1832 wurde er ohne Probleme für eine zweite Amtsperiode bestätigt.

Jackson ging als einer der prägenden Präsidenten in die Geschichte der USA ein: Zum einen war er der erste Präsident, der nicht aus der Elite des Amerikanischen Unabhängigkeitskrieges stammte, zum anderen nahm er in seiner Regierungszeit umfangreiche Änderungen an der Staatsorganisation vor. Dazu gehören das Etablieren des spoils system sowie die Zerschlagung der amerikanischen Nationalbank, die sich unter seinem Nachfolger Van Buren während der Wirtschaftskrise von 1837 jedoch negativ auswirkte. In seine Amtszeit fällt außerdem die gewaltsame Vertreibung der „fünf zivilisierten Indianernationen“ mit ungezählten Todesopfern. Nach Beendigung seiner Präsidentschaft 1837 zog Jackson sich ins Privatleben zurück. Bis zu seinem Tod 1845 blieb er jedoch innerhalb der Demokratischen Partei eine einflussreiche Größe.

Andrew Jackson wurde am 15. März 1767 in den Waxhaws geboren; damals ein zwischen North und South Carolina umstrittenes Gebiet. Seine Eltern, Andrew Jackson senior (* ca. 1730; † Februar 1767) und Elizabeth „Betty“ Hutchinson (* ca. 1740; † November 1781), emigrierten 1765 aus Carrickfergus im heutigen Nordirland in die USA. Jacksons Vater starb zwei Monate vor seiner Geburt. Da Jackson nach dem Willen seiner Familie zunächst für ein geistliches Amt bestimmt war, konnte er eine grundlegende Schulbildung genießen. Durch den Unabhängigkeitskrieg war dies ab seinem zehnten Lebensjahr nicht mehr möglich. Mit seiner Mutter und zwei Brüdern geriet er 1781 eine Zeitlang in britische Gefangenschaft; einer seiner Brüder überlebte das Martyrium nicht. Auch seine Mutter starb wenig später.

Jackson genoss eine nur dürftige Erziehung und Ausbildung und arbeitete seit seinem 18. Lebensjahr bei einem Rechtsanwalt. 1786 wurde er zur Praxis zugelassen und ließ sich in Nashville, Tennessee, nieder. Ab 1788 wurde er Staatsanwalt für das westliche North Carolina, das heißt für das Gebiet des späteren Tennessee. 1791 heiratete er Rachel Donelson Robards; da die Scheidung von ihrem ersten Mann noch nicht vollzogen gewesen war, heirateten sie 1794 erneut. Nachdem er Mitglied der verfassunggebenden Versammlung des neuen US-Staats Tennessee geworden war, zog er 1796 als einer der ersten Vertreter des Staates in das US-Repräsentantenhaus und im Jahr darauf in den Senat der Vereinigten Staaten ein. Ab 1798 wurde er bis 1804 Richter am Tennessee Supreme Court. Auseinandersetzungen mit dem amtierenden Präsidenten Thomas Jefferson führten zu seinem zeitweiligen Rückzug aus dem politischen Leben. Zwischen 1805 und 1812 bewirtschaftete er seine Farm an der West Cabin. Jackson tötete am 30. Mai 1806 in einem Duell Charles Dickinson, einen regional bekannten Schützen, wurde aber auch selbst schwer verletzt: eine Kugel durchschlug zwei Rippen, verfehlte das Herz knapp und blieb lebenslang im Brustkorb stecken. Dickinson hatte Jacksons Frau verleumdet, was Anlass für das Duell war. Jackson setzte sich weiter häufig Zweikämpfen aus, eine weitere Schussverletzung führte zu dauerhaften Bauchschmerzen, wahrscheinlich durch eine Bleivergiftung.[1][2]
1812 erhielt Jackson den Oberbefehl über alle Milizen des Staates Tennessee und vertrieb die ansässigen Muskogee-Indianer nach Florida. Als die Engländer 1815 New Orleans bedrohten, erhielt Jackson dort den Befehl über die Linientruppen und wurde zum Generalmajor befördert.

Jackson wurde zu einem damals nur noch von George Washington übertroffenen Nationalhelden, nachdem er die Briten in der Schlacht von New Orleans am 8. Januar 1815 besiegt hatte.[2] Jackson und seinen Gegnern war nicht bekannt, dass zu diesem Zeitpunkt bereits der Friede von Gent geschlossen worden war. Seine Popularität stieg, als er die Seminolen im ersten Seminolen-Krieg (1817/18) besiegte.[3]
Jackson kehrte Anfang der 1820er-Jahre ins politische Leben zurück. Er wurde zum Militärgouverneur von Florida ernannt, das gerade von den Spaniern abgetreten worden war, und war von 1823 bis 1825 erneut US-Senator für den Staat Tennessee. Bei der Präsidentschaftswahl von 1824 kandidierte Jackson erstmals für das Amt des US-Präsidenten und erreichte landesweit die meisten Stimmen im Popular Vote. Da jedoch dieses Mal insgesamt vier politisch schwergewichtige Kandidaten aus der Demokratisch-Republikanischen Partei antraten, gelang es keinem Bewerber, die notwendige Mehrheit an Stimmen im Wahlmännergremium auf sich zu vereinen. Verfassungsgemäß hatte nun das amerikanische Repräsentantenhaus den Präsidenten und der Senat den Vizepräsidenten zu wählen. Als sich der viertplatzierte Kandidat Henry Clay für John Quincy Adams aussprach, konnte sich dieser letztlich durchsetzen und wurde zum Präsidenten gewählt. Jackson bezichtigte beide der Korruption, da Adams Clay zum Außenminister ernannt hatte. Bekannt wurde in diesem Zusammenhang die von Jackson gewählte Bezeichnung Corrupt Bargain.[4][5]
In der Folge spaltete sich die damals stärkste Partei in die Demokraten um Jackson und Van Buren und die National Republican Party um Adams.

Vier Jahre nach seiner Niederlage gegen John Quincy Adams stellte die neu gegründete Demokratische Partei mit Jackson erstmals einen eigenen Präsidentschaftskandidaten auf. Auf dem ersten Nominierungsparteitag im Frühjahr 1828 wurde er einstimmig zum Kandidaten gewählt. Jackson, der Adams’ Wahl im Jahr 1824 als illegitim betrachtete, begann bereits um 1825 mit der Vorbereitung für eine Neuauflage des Duells. Dazu gehörte auch der Aufbau seiner neuen Partei. Der Wahlkampf gegen Adams, der für die Nationalrepublikaner antrat, wurde mit äußerster Härte ausgefochten. Beide Kontrahenten und ihre Parteien sparten auch nicht mit schweren persönlichen Angriffen auf den Gegner. So verunglimpften die Demokraten den amtierenden Präsidenten als abgehoben und unehrenhaft. Auch machte er die Indianer-Umsiedlung zu einem zentralen Wahlkampfthema. Das Interesse der weißen Siedler am Land der Indianer war, besonders in den Südstaaten, möglicherweise einer der Hauptgründe für seinen Wahlsieg. Die Kampagne der Nationalrepublikaner verunglimpfte Jackson als machtbesessen; auch die Tatsache, dass seine Frau Rachel bereits einmal verheiratet war, wurde von Adams thematisiert. Heute besteht unter Historikern Einigkeit, dass es sich 1828 um einen der „schmutzigsten“ Wahlkämpfe in der amerikanischen Geschichte handelte. Doch die heftigen politischen Auseinandersetzungen führten zu einer bis dato nie dagewesenen Politisierung der Bevölkerung.

Nachdem im Dezember 1828 das Endergebnis feststand, konnte Jackson mit einem Stimmenanteil von 56 Prozent Adams deutlich besiegen, der etwas mehr als 43 Prozent auf sich vereinte. Auch im Electoral College fiel Jacksons Sieg deutlich aus: 178 Wahlmänner waren auf ihn entfallen, Adams erhielt 83 Stimmen.

Nach seinem politischen Triumph musste Jackson jedoch am 22. Dezember 1828 mit dem Tod seiner Frau einen schweren privaten Rückschlag hinnehmen. Der neugewählte Präsident machte für das Ableben seiner Frau in erster Linie seine politischen Gegner verantwortlich, die durch ihre persönlichen Angriffe auf Rachel Jackson zu deren Tod beigetragen hätten.

Das Präsidentenamt übernahm er gemäß der damaligen gesetzlichen Regelung am 4. März 1829. Vizepräsident während seiner ersten Amtsperiode war John C. Calhoun, der auch schon als Adams’ Stellvertreter amtiert hatte, sich von diesem jedoch abwandte. Andrew Jackson war der erste Präsident, der nicht aus dem Kreis der amerikanischen Revolution kam. George Washington, John Adams, Thomas Jefferson, James Madison und James Monroe waren anerkannte Persönlichkeiten, die im Amerikanischen Unabhängigkeitskrieg gekämpft und die Verfassung der Vereinigten Staaten geprägt hatten, John Quincy Adams war der Sohn von John Adams. Die Wahl Andrew Jacksons bedeutete einen Bruch mit der Vergangenheit. Jackson stammte nicht aus der großagrarischen Gründerelite der USA, sondern vertrat die kleinbürgerlichen Interessen der neuen Immigranten.

Bei der Präsidentschaftswahl 1832 stellte Jackson sich erfolgreich zur Wiederwahl und trat im März 1833 seine zweite Amtsperiode an. Er besiegte dabei seinen politischen Intimfeind, den Senator Henry Clay, mit einem Stimmenanteil von 54,7 Prozent und 219 Elektoren deutlich. Clay, der Jackson in seinem Wahlkampf als „König Andrew“ verspottete, erhielt als Kandidat der Nationalrepublikaner nur 36,9 Prozent der Stimmen und 49 Wahlmänner. Weitere 7,8 Prozent und sieben Wahlmänner entfielen auf den Bewerber der Anti-Masonic Party William Wirt. In 16 der damals 23 Bundesstaaten konnte der Amtsinhaber eine Mehrheit der Stimmen erringen. Neuer Vizepräsident wurde Jacksons früherer Außenminister und enger Vertrauter Martin Van Buren. Der bisherige Vizepräsident John C. Calhoun, der sich in vielen politischen Fragen mit dem Präsidenten uneinig war, wurde von der Demokratischen Partei nicht erneut aufgestellt. Calhoun trat dann im Dezember 1832 kurz vor Ablauf seiner Amtszeit zurück.[6]
Die Jahre von Jacksons Präsidentschaft waren ökonomisch von einem weitreichenden Wandel geprägt. Schon die Zeit vor seiner Machtübernahme war von einem beispiellosen Ausbau der Infrastruktur geprägt, besonders Straßen und Kanäle, später kam auch die Eisenbahn hinzu, was den Grundstein zur Industrialisierung legte. Während dieser Zeit setzten sich in den USA die kapitalistischen Marktkräfte durch, was unter Historikern heute als Marktrevolution bekannt ist. Ausdruck davon waren besonders die Orientierung von Industrie und Landwirtschaft am Weltmarkt.[7]
Andrew Jackson galt während seiner gesamten militärischen und politischen Karriere als „Indianerhasser“. Darin unterscheidet er sich von den Gründungsvätern der USA, die zumindest den „fünf zivilisierten Nationen“ noch Rechte zugebilligt hatten, und auch von seinem direkten Vorgänger John Quincy Adams.

Bereits während des Britisch-Amerikanischen Kriegs von 1812 hatte Jackson die Erfahrung gemacht, dass die Briten mit indianischen Verbündeten gegen die USA gekämpft hatten. Dies bestärkte ihn wohl in seiner Haltung, den Ureinwohnern Gleichberechtigung zu verweigern und sie wo immer möglich zugunsten von weißen Siedlern zu vertreiben. Nach seinem Sieg als General der Tennessee-Miliz über die Muskogee (Creek) in der Schlacht am Horseshoe Bend im Jahr 1814 wurde er im Mai des gleichen Jahres zum Major General der United States Army ernannt.[2] Später erzwang er im Vertrag von Fort Jackson die Abtretung eines Großteils des Indianerlands, darunter auch Gebiete von Indianern, die auf der Seite der USA gekämpft hatten.

1817–1818 marschierte Jackson nach Florida ein, das damals unter spanischer Herrschaft stand. Als Begründung gab er das Ziel an, entlaufene schwarze Sklaven, die sich zum Stamm der Seminolen geflüchtet hatten, einzufangen.[8] Die Seminolen wurden besiegt und zunächst nach Süd- und Mittel-Florida in eine Reservation umgesiedelt, aus der sie aber später auch wieder vertrieben wurden.

Unter Jacksons Präsidentschaft verabschiedeten der Senat und das Repräsentantenhaus 1830 den Indian Removal Act. Formal ermächtigte ihn das Gesetz nur, Verhandlungen über den Tausch der Indianergebiete im fruchtbaren südöstlichen Waldland der Südstaaten gegen Gebiete im kargen, trockenen sogenannten Indianer-Territorium (heute Oklahoma) zu führen. Jackson nutzte diese Vollmacht jedoch, um Umsiedlungs-Verträge zu erzwingen, bei denen die Indianer alles verloren. Weigerten sich die gewählten Häuptlinge, wurde mit anderen, zumeist nicht legitimierten Vertretern verhandelt. Die Umsiedlung selbst führte durch Krankheiten, schlechte Verwaltung, unzureichende Versorgung sowie die völlige Rechtlosigkeit der Indianer zu unzähligen Todesopfern auf dem Pfad der Tränen. Der einzige Stamm, der sich weigerte, die Seminolen in Florida, wurde in einem jahrelangen und auch für das US-Militär äußerst verlustreichen Krieg fast vernichtet.[9]
Ein bedeutendes Ereignis seiner Präsidentschaft war die Nullifikationskrise von 1832/33. Vorausgegangen war ein politischer Streit mit dem Bundesstaat South Carolina, der mit der Zollpolitik der Bundesregierung unzufrieden war. Am 24. November 1832 erklärte der Staat, ihm missfallende Zollgesetze der Regierung in Washington für nichtig erklären zu können („nullifizieren“). Verbunden war diese Ankündigung mit der Drohung, aus den Vereinigten Staaten auszutreten, sollte die Bundesregierung versuchen, die Zollgesetze mit Waffengewalt durchzusetzen. Jackson wies dies entschieden zurück. Er betrachte das Vorgehen South Carolinas als verfassungswidrig. Bestärkt durch die Tatsache, dass sich South Carolina kein weiterer Bundesstaat anschloss, ersuchte der Präsident den Kongress um ein Gesetz, das es ihm erlauben würde, Bundesgesetze notfalls auch mit Gewalt in South Carolina durchsetzen. Die Parlamentarier entsprachen Jacksons Wunsch zwar mit großer Mehrheit, gleichzeitig wurde jedoch ein neues Zollgesetz mit niedrigeren Zöllen verabschiedet. Letzteres war als Kompromissangebot an den Bundesstaat gedacht. Am 2. März 1833 unterzeichnete Jackson beide Gesetze, und die Krise wurde abgewendet. Heute wird von vielen Historikern positiv bewertet, dass sich Jackson im Zuge der Nullifikationskrise dem Gedanken zur Abspaltung eines Staates entschieden widersetzt und damit die Einheit des Landes bewahrt hatte.[10][11]
Nachdem Jackson im Jahr um 1819 erlebt hatte, wie die Zins- und Kreditpolitik der Bank of the United States (Nationalbank) amerikanische Farmer in den finanziellen Ruin trieb, wurde die Zerschlagung der Bank zu einem seiner zentralen politischen Anliegen. Sein Vorgehen gegen die Nationalbank wurde rasch als Bank War („Bankkrieg“) bekannt.[12] Nachdem Jackson das Präsidentenamt angetreten hatte, trieb er dieses Vorhaben entschieden voran. Dies führte schon bald zu scharfen Auseinandersetzungen mit Senator Henry Clay und dem Bankdirektor Nicholas Biddle; sowohl Biddle als auch Clay trennte eine gegenseitige persönliche Abneigung von Jackson. Im Sommer 1832, als der Wahlkampf zwischen Jackson und Clay bereits in vollem Gange war, entschloss sich der Senator zu einem riskanten politischen Schachzug, mit dem er hoffte, den Präsidenten entscheidend schwächen zu können. Clay brachte ein Gesetz in den Kongress ein, um die noch bis 1836 laufende Lizenz der Nationalbank vorzeitig zu verlängern. Der Entwurf wurde in beiden Kongresskammern angenommen. Jackson durchschaute jedoch Clays Bestrebungen und legte am 10. Juli 1832 sein Veto gegen das Gesetz ein. In seiner Begründung ließ der Präsident verlautbaren, er sehe durch die Bank eine erhebliche Gefahr für die Bürger und für die Wirtschaft des Landes, da sich bereits ein Viertel der Bankaktien in ausländischem Besitz befand. Ferner verwies er darauf, dass „Reiche und Mächtige bereits zu oft Regierungsmaßnahmen für ihre selbstsüchtigen Zwecke missbraucht“ hätten.[13] Jackson wertete das Scheitern einer Zweidrittelmehrheit im Kongress zur Überstimmung des Vetos als vollen Erfolg. Wenige Monate später musste Clay mit seiner klaren Wahlniederlage gegen Jackson im Präsidentschaftswahlkampf einen weiteren politischen Rückschlag hinnehmen. Binnen weniger Jahre hatte Jackson sein Ziel erreicht; im Jahr 1836 wurde die Nationalbank privatisiert. Zuvor hatte der Präsident den Finanzminister angehalten, die Goldreserven der Bundesregierung auf die Banken der Einzelstaaten zu verteilen.[10]
Die Etablierung des sogenannten spoils system in der amerikanischen Politik geht auf Jacksons Präsidentschaft zurück. Es bezeichnet die Praxis, dass die Unterstützer eines Wahlsiegers mit Arbeitsstellen in der öffentlichen Verwaltung belohnt werden. Das spoils system ist auch ein Anreiz für die Unterstützer, weiterhin für die Partei des Wahlgewinners zu arbeiten. Der Begriff leitet sich von Senator William L. Marcys Spruch „to the victor belong the spoils“ ab, auf Deutsch etwa „dem Sieger gehört die Beute“.[14] Nach seiner Amtsübernahme belohnte Präsident Jackson seine Träger und Anhänger auf systematische Weise mit Regierungsstellen. Er dachte, die erfolgreiche Wahl durch das Volk gebe der siegreichen Partei das Mandat, staatliche Amtsträger aus den Rängen der eigenen Partei zu ernennen. Befürworter dieser Praxis erklärten, dass in dieser Weise die Bürger so in der Lage seien, über die Abwahl der Exekutive auch die Personen im öffentlichen Dienst abwählen zu können. Gegner des spoils system erwiderten, dieses System sei anfällig für Inkompetenz und ungezügelte Korruption – und weil schlussendlich Gefolgsleute des Wahlsiegers ernannt werden, würde dies dem republikanischen Gedankengut widersprechen.[15]
Die Außenpolitik spielte während Jacksons Präsidentschaft eine untergeordnete Rolle. In seiner Amtszeit war das Verhältnis zu Frankreich kurzzeitig angespannt, nachdem die Franzosen sich geweigert hatten, Entschädigungszahlungen für die Plünderungen amerikanischer Schiffe zu Napoleons Zeiten zu leisten, obwohl dies 1831 in einem bilateralen Abkommen vereinbart wurde. Im Jahr 1835, nachdem auch die britische Regierung sich dafür aussprach, beglich Frankreich seine Schulden an die USA, womit der Konflikt beigelegt wurde.[16]
Jackson sympathisierte bereits früh mit einem Anschluss der Republik Texas an die USA. Texas wurde 1836 von Mexiko in die Unabhängigkeit entlassen; Bedingung war aber, dass Texas nicht Teil der Vereinigten Staaten wird. Die Anerkennung von Texas als eigenständigem Staat zögerte der Präsident am Ende seiner Amtszeit hinaus, um das Aufkommen von Debatten zu diesem Thema im Wahlkampf von 1836 zu vermeiden. Erst Anfang März 1837, kurz vor Ende seiner Regierungszeit, bestätigte Jackson die Anerkennung von Texas als souveränem Staat. Nach Ende seiner Amtszeit sprach sich Jackson dann wiederholt für einen Anschluss an die Vereinigten Staaten aus. Dies wurde dann 1845 vollzogen und führte wenig später zum Krieg zwischen den USA und Mexiko.[16]
Andrew Jackson war der erste US-Präsident, auf den ein Attentat verübt wurde: Am 30. Januar 1835 verließ Jackson das Kapitol, als der arbeitslose englische Anstreicher Richard Lawrence zwei Pistolen auf ihn richtete. Beide Pistolen ließen sich jedoch nicht abfeuern, und angeblich konnte Jackson selbst den Attentäter mit seinem Spazierstock verprügeln. Der unzurechnungsfähige Täter wurde später in die Psychiatrie eingewiesen und nie wegen des Attentats angeklagt.

Im Jahre 1836 verzichtete Jackson auf eine erneute Wiederwahl für eine dritte Amtszeit und sprach sich für seinen Stellvertreter Martin Van Buren als Nachfolger aus. Nachdem Van Buren von den Demokraten einstimmig nominiert worden war und sich bei der Präsidentschaftswahl 1836 durchgesetzt hatte, löste er Jackson turnusgemäß am 4. März 1837 ab. Mit knapp 70 Jahren war Jackson der bis dato älteste amtierende Präsident. Bis zur Wiederwahl von Abraham Lincoln im Jahr 1864 blieb Jackson auch der letzte Präsident, der im Amt bestätigt wurde. Nachdem Lincoln allerdings in seiner zweiten Amtszeit ermordet worden war, war erst Ulysses S. Grant (1869–1877) der nächste Präsident, der tatsächlich zwei komplette Wahlperioden absolvierte.

Während Andrew Jacksons Präsidentschaft wurden zwei Bundesstaaten in die USA aufgenommen:

Jackson ernannte in seiner Zeit als Präsident sechs Richter an den Obersten Gerichtshof der USA:

Weitere Berufungen erfolgten an niedrigere Bundesgerichte.

Nachdem Jackson das Weiße Haus verlassen hatte, zog er sich aus der Politik weitestgehend zurück. Die Wirtschaftskrise von 1837 hatte ihn jedoch finanziell schwer getroffen. Im Jahr 1840 sprach er sich – vergeblich – für die Wiederwahl seines Vertrauten und Nachfolgers Martin Van Buren aus, der dem Whig-Kandidaten William Henry Harrison unterlegen war. Jackson verfolgte Harrisons Wahlsieg mit großer Besorgnis, nachdem sich der neu gewählte Präsident für die Neugründung der Nationalbank ausgesprochen hatte. Als Harrison im April 1841 aber unerwartet verstarb, blockierte dessen Nachfolger, der bisherige Vizepräsident John Tyler, dieses Vorhaben mit seinem Veto. Nachdem dieser als Konsequenz von der Whig-Party ausgeschlossen worden war, forderte der innerparteilich immer noch einflussreiche Jackson die Demokraten auf, das ehemalige Parteimitglied Tyler freundlich zu empfangen. Obwohl Tyler parteilos blieb, unterstützte er Jacksons Anliegen, Texas in die Vereinigten Staaten aufzunehmen, was im März 1845 vollzogen wurde. Zuvor unterstützte Jackson 1844 die Kandidatur des Demokraten James K. Polk. Polk war während der Endphase von Jacksons Präsidentschaft Sprecher des Repräsentantenhauses gewesen und hatte die Politik Jacksons, den er als Vorbild betrachtete, loyal mitgetragen. Eine erneute als aussichtsreich geltende Nominierung Martin Van Burens lehnte Jackson zuvor ab, nachdem dieser sich gegen eine Angliederung der Republik Texas ausgesprochen hatte. Zuvor bat Jackson Präsident John Tyler in einem Brief darum, seine Kandidatur für eine Splittergruppe der Demokraten zurückzuziehen, um so Polk bessere Chancen auf einen Wahlsieg gegen Jacksons politischen Intimfeind Henry Clay zu ermöglichen, der nach dem Parteiausschluss und weiteren Bemühungen, Tylers Regierung mit Rücktritten der Whig-Minister zu stürzen, auch Tylers Feind war. Im Sommer 1844 zog Tyler seine Bewerbung dann tatsächlich zurück und unterstützte Polk; der wurde dann mit knapper Mehrheit zum neuen Präsidenten gewählt. Jackson sollte jedoch nur die ersten drei Monate von dessen Amtsperiode miterleben.[17]
Andrew Jackson starb am 8. Juni 1845 im Alter von 78 Jahren an Herzversagen und wurde neben seiner 1828 verstorbenen Frau Rachel bei Nashville beigesetzt.[18]
Wie schon zu Lebzeiten hat Andrew Jackson bis heute eine polarisierende Wirkung. Heutige Kritiker urteilen vor allem über seine restriktive Indianerpolitik sehr negativ, die neben Vertreibung zu zahlreichen Toten geführt hatte. Einige Historiker versuchen dies mit der Herkunft und der Epoche Jacksons zu relativieren. Jackson sei hier von verschiedenen Einflüssen geleitet worden, denen er sich ebenso wenig wie viele seiner Zeitgenossen entziehen konnte. Sein Krisenmanagement im Zuge der Nullifikationskrise findet im historischen Kontext eine positive Bewertung, da er sich erfolgreich der Sezession eines Bundesstaates widersetzte und damit die Einheit des Landes bewahrte. Besonders blieb seine Präsidentschaft auch durch seinen Erfolg bei der Auflösung der Nationalbank in Erinnerung, was viele Historiker als Eintreten des Präsidenten für das einfache Volk ansehen. Viele Historiker und Politiker haben später Jacksons Zeit als Jacksonian Democracy bezeichnet. Damit sollte Jacksons Aufstieg aus dem einfachen Volk in das höchste Staatsamt ebenso charakterisiert werden wie sein Eintreten für „die Sache des einfachen Mannes“ und damit die Selbstbestimmung des amerikanischen Volkes auf der Basis demokratischer Prozesse (die Bezeichnung Jacksonian Democracy ist übrigens die einzige Epoche in der Geschichte der USA, die nach einer Person benannt wurde). Auch seine Rolle bei der Gründung der Demokratischen Partei hat historisch eine herausragende Bedeutung.[19]
Eine biografische Darstellung des Lebens von Andrew und Rachel Jackson basiert auf dem Roman von Irving Stone The President’s Lady, der 1953 in die Kinos kam mit Susan Hayward, Charlton Heston, John McIntire und Carl Betz in den Hauptrollen, Regie: Henry Levin.

Washington |
J. Adams |
Jefferson |
Madison |
Monroe |
J. Q. Adams |
Jackson |
Van Buren |
W. Harrison |
Tyler |
Polk |
Taylor |
Fillmore |
Pierce |
Buchanan |
Lincoln |
A. Johnson |
Grant |
Hayes |
Garfield |
Arthur |
Cleveland |
B. Harrison |
Cleveland |
McKinley |
T. Roosevelt |
Taft |
Wilson |
Harding |
Coolidge |
Hoover |
F. Roosevelt |
Truman |
Eisenhower |
Kennedy |
L. Johnson |
Nixon |
Ford |
Carter |
Reagan |
G. Bush |
Clinton |
G. W. Bush |
Obama |
Trump

Klasse 1: 
Cocke |
A. Jackson |
Smith |
J. Anderson |
Campbell |
Eaton |
Grundy |
Foster |
Grundy |
Nicholson |
Foster |
Turney |
Jones |
Johnson |
Patterson |
Brownlow |
Johnson |
Key |
Bailey |
H. Jackson |
Whitthorne |
Bate |
Frazier |
Lea |
McKellar |
Gore Sr. |
B. Brock |
Sasser |
Frist |
Corker

Klasse 2: 
Blount |
J. Anderson |
Cocke |
Smith |
Whiteside |
Campbell |
Wharton |
Williams |
A. Jackson |
White |
A. Anderson |
Jarnagin |
Bell |
Nicholson |
Fowler |
Cooper |
Harris |
Turley |
Carmack |
Taylor |
Sanders |
Webb |
Shields |
Tyson |
W. Brock |
Hull |
Bachman |
Berry |
Stewart |
Kefauver |
Walters |
Bass |
Baker |
Gore Jr. |
Mathews |
Thompson |
Alexander

Militärgouverneur (1821):Jackson

Florida-Territorium (1822–1844):Duval |
Eaton |
Call |
Reid |
Call |
Branch

Bundesstaat Florida (seit 1844):Moseley |
Brown |
Broome |
M. Perry |
Milton |
Allison |
Marvin |
Walker |
Reed |
Hart |
Stearns |
Drew |
Bloxham |
E. Perry |
Fleming |
Mitchell |
Bloxham |
Jennings |
Broward |
Gilchrist |
Trammell |
Catts |
Hardee |
Martin |
Carlton |
Sholtz |
Cone |
Holland |
Caldwell |
Warren |
McCarty |
Johns |
Collins |
Bryant |
Burns |
Kirk |
Askew |
Graham |
Mixson |
Martinez |
Chiles |
MacKay |
Bush |
Crist |
Scott

Delegierter aus dem Südwest-Territorium (1794–1796)White

Abgeordnete aus dem Bundesstaat Tennessee (seit 1796)1. Bezirk: Jackson |
W. Claiborne |
Dickson |
Wharton |
Miller |
Grundy |
Cannon |
T. Claiborne |
R. Allen |
Blair |
Carter |
Arnold |
A. Johnson |
B. Campbell |
N. Taylor |
Watkins |
Nelson |
Bridges |
N. Taylor |
R. Butler |
McFarland |
Randolph |
R. Taylor |
Pettibone |
R. Butler |
A. Taylor |
W.C. Anderson |
Brownlow |
Massey |
Sells |
B. Reece |
Lovette |
B. Reece |
Phillips |
B. Reece |
L. Reece |
Quillen |
Jenkins |
D. Davis |
Roe • 2. Bezirk: G. Campbell |
Weakley |
Sevier |
Henderson |
Hogg |
Bryan |
Alexander |
J. Cocke |
P. Lea |
Arnold |
Bunch |
McClellan |
Senter |
W. Cocke |
Watkins |
Churchwell |
Sneed |
Maynard |
Thornburgh |
L. Houk |
J. Houk |
Gibson |
Hale |
Austin |
J.W. Taylor |
Jennings |
H. Baker Sr. |
I. Baker |
Duncan Sr. |
Duncan Jr.

3. Bezirk: Rhea |
Powell |
F. Jones |
Blair |
J.C. Mitchell |
Standifer |
L. Lea |
J. Williams |
Blackwell |
Crozier |
J. Anderson |
Churchwell |
S. Smith |
Brabson |
Clements |
Stokes |
A. Garrett |
Crutchfield |
Dibrell |
Neal |
Evans |
H. Snodgrass |
F. Brown |
Moon |
J. Brown |
McReynolds |
Kefauver |
Frazier |
Brock |
L. Baker |
Lloyd |
Wamp |
Fleischmann • 4. Bezirk: Bowen |
Reynolds |
Marr |
Cannon |
S. Houston |
Isacks |
Standifer |
Stone |
Blackwell |
T. Campbell |
A. Cullom |
Hill |
Savage |
W. Cullom |
Savage |
Stokes |
E. Cooper |
Mullins |
Tillman |
Bright |
Fite |
Riddle |
McMillin |
C. Snodgrass |
Fitzpatrick |
M. Butler |
Hull |
Clouse |
Hull |
J.R. Mitchell |
Gore Sr. |
Evins |
Gore Jr. |
Jim Cooper |
Hilleary |
L. Davis |
DesJarlais

5. Bezirk: T. Harris |
I. Thomas |
Rhea |
Isacks |
R. Allen |
Desha |
Hall |
Forester |
Turney |
G. Jones |
Ready |
Hatton |
W. Campbell |
Trimble |
Prosser |
Golladay |
Harrison |
Bright |
Warner |
Richardson |
W. Houston |
E. Davis |
Byrns |
Atkinson |
Byrns Jr. |
Priest |
McCord |
Earthman |
Evins |
Priest |
Loser |
Fulton |
C. Allen |
Boner |
Clement |
Jim Cooper • 6. Bezirk: Humphreys |
Sevier |
Blount |
J. Cocke |
J. Polk |
B. Peyton |
W. Campbell |
A. Brown |
Martin |
J. Thomas |
W. Polk |
G. Jones |
J. Thomas |
Arnell |
Whitthorne |
House |
A. Caldwell |
Washington |
Gaines |
Byrns |
Turner |
Courtney |
Priest |
Sutton |
Bass |
W. Anderson |
Beard |
Gore Jr. |
B. Gordon |
Black

7. Bezirk: Reynolds |
S. Houston |
Bell |
Caruthers |
Dickinson |
Gentry |
Bugg |
Wright |
Hawkins |
R. Caldwell |
Atkins |
Whitthorne |
Ballentine |
Whitthorne |
Cox |
Padgett |
Turner |
Salmon |
E. Eslick |
W. Eslick |
Browning |
Pearson |
Courtney |
Sutton |
Murray |
Blanton |
E. Jones |
Sundquist |
Bryant |
Blackburn • 8. Bezirk: Sandford |
Marable |
C. Johnson |
Dickinson |
Maury |
Gentry |
J. Peyton |
E. Ewing |
Barrow |
A. Ewing |
W. Cullom |
Zollicoffer |
Quarles |
Leftwich |
Nunn |
W. Smith |
Vaughan |
Nunn |
Atkins |
J.M. Taylor |
Enloe |
McCall |
Sims |
Scott |
Browning |
Jere Cooper |
Murray |
Jere Cooper |
Everett |
E. Jones |
Kuykendall |
Ford Sr. |
E. Jones |
Tanner |
Fincher |
Kustoff 

9. Bezirk: Standifer |
Alexander |
D. Crockett |
Fitzgerald |
J. Polk |
Watterson |
C. Johnson |
Chase |
I. Harris |
Etheridge |
Atkins |
Etheridge |
Lewis |
W. Caldwell |
Simonton |
Pierce |
Glass |
Pierce |
McDearmon |
Pierce |
F. Garrett |
Jere Cooper |
Crump |
Chandler |
C. Davis |
Jere Cooper |
C. Davis |
Grider |
Kuykendall |
Ford Sr. |
Ford Jr. |
Cohen • 10. Bezirk: Inge |
Shields |
A. Brown |
Ashe |
Stanton |
Rivers |
Avery |
Maynard |
Young |
Moore |
Young |
Z. Taylor |
Phelan |
J. Patterson |
Carmack |
M. Patterson |
G. Gordon |
McKellar |
Fisher |
Crump |
C. Davis

11. Bezirk: C. Johnson |
Cheatham |
C. Johnson |
M. Brown |
Haskell |
C. Williams • 12. Bezirk: D. Crockett |
Huntsman |
J. Crockett |
M. Brown • 13. Bezirk: Dunlap |
C. Williams
