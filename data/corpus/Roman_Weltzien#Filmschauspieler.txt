Roman Weltzien (* 12. April 1978 in Karl-Marx-Stadt) ist ein deutscher Schauspieler.

Roman Weltzien absolvierte eine Ausbildung zum Schauspieler an der Hochschule für Musik und Theater „Felix Mendelssohn Bartholdy“ Leipzig. Während der Studienzeit konnte er praktische Erfahrungen sammeln. Er war unter anderem am Carrousel-Theater Berlin, Ulmer Theater und der Neuen Bühne Senftenberg engagiert. Zudem war er an mehreren Sommertheaterproduktionen in Wunsiedel und Mayen beteiligt. Neben einigen Rollen für Film und Fernsehen ist Roman Weltzien als Sprecher für Hörspielproduktionen tätig.

Seit 2006 ist Roman Weltzien am Südthüringischen Staatstheater Meiningen (Das Meininger Theater) engagiert. 
Dort spielt er die Rolle des Mephistopheles in der Inszenierung des Faust von Johann Wolfgang Goethe unter der Regie von Ansgar Haag.

Er war in Fernsehproduktionen des Tatort tätig.
