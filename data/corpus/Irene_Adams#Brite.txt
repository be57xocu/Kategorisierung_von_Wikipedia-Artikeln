Katherine Patricia Irene Adams, Baroness Adams of Craigielea (* 27. Dezember 1947) ist eine britische Politikerin und Mitglied des House of Lords. 

Zuvor repräsentierte sie 1990 bis 2005 den Wahlkreis Paisley North im House of Commons. Der Sitz gehörte ursprünglich ihrem verstorbenen Ehemann Allen Adams, der ihn von 1979 bis 1990 innehatte. Nach seinem Tod trat sie zur Wahl an und gewann. Sie ist Mitglied der Labour Party und zählt dort zum linken Flügel.
Adams ist verwitwet. Sie hat einen Sohn und zwei Töchter.
