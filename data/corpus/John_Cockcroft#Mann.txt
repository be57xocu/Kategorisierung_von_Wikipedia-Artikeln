Sir John Douglas Cockcroft (* 27. Mai 1897 in Todmorden, England; † 18. September 1967 in Cambridge) war ein englischer Atomphysiker und Nobelpreisträger.

John Cockcroft wurde am 27. Mai 1897 als Sohn eines Fabrikanten in Todmorden, einer kleinen Stadt in der Nähe von Manchester in England, geboren. Dort absolvierte er auch seine Grundschulausbildung und die Secondary School. Anschließend begann er 1914 ein Mathematikstudium an der Universität Manchester; ab 1915 diente er in der Royal Field Artillery. Nach seiner Dienstzeit kehrte er nach Manchester zurück und studierte Elektrotechnik. Nach seiner Ausbildung absolvierte er eine zweijährige Lehre bei der Metropolitan Vickers Electrical Company, um dann anschließend sein Mathematikstudium fortzusetzen, das er 1924 erfolgreich abschloss. Anschließend wechselte er an das Cavendish-Laboratorium, das von Ernest Rutherford geleitet wurde. 1934 übernahm er die Leitung des Royal Society Mond Laboratory in Cambridge. Er wurde 1939 zum Professor für Naturphilosophie ernannt und wurde im September 1939 stellvertretender Direktor für wissenschaftliche Forschung im Ministry of Supply. In dieser Position begann er mit der Erforschung von Radar zur Küsten- und Luftverteidigung. Er war im Herbst 1940 an der Tizardmission beteiligt, die unter anderem das Magnetron in die Radartechnik einbrachte, und wurde anschließend zum Leiter des Air Defence Research and Development Establishment ernannt. 1944 wurde er Mitglied des kanadischen Atomenergieprojekts und leitete die Montreal and Chalk River Laboratories. Er kehrte 1946 im Austausch gegen Wilfrid Bennett Lewis als Direktor des Forschungsinstituts für Atomenergie in Harwell nach England zurück. Er wurde 1954 wissenschaftliches Mitglied der britischen Atomenergiebehörde, 1959 führte er diese Aufgabe nur noch auf Teilzeitbasis fort, da er zum Vorsitzenden des Churchill Colleges in Cambridge ernannt wurde.

Er heiratete 1925 Eunice Elizabeth Crabtree, mit der er fünf Kinder hatte. 
John Douglas Cockcroft starb im Jahr 1967.

Cockcroft arbeitete am Cavendish-Laboratorium zunächst mit Pjotr Kapiza an der Erzeugung starker Magnetfelder und tiefer Temperaturen.

Ab 1928 forschte er zusammen mit seinem irischen Physikerkollegen Ernest Walton auf dem Gebiet der Beschleunigung von Protonen. Mit dem von ihnen entwickelten ersten Teilchenbeschleuniger (siehe Cockcroft-Walton-Beschleuniger) konnten sie als Erste die Auslösung von Kernreaktionen an leichten, mit beschleunigten Protonen beschossenen Atomkernen nachweisen. Damit war die vielleicht wichtigste experimentelle Methode der Kernphysik geschaffen worden.

1951 wurden Cockcroft und Walton mit dem Nobelpreis für Physik „für ihre Pionierarbeit auf dem Gebiet der Atomkernumwandlung durch künstlich beschleunigte atomare Partikel“ ausgezeichnet.

1936 wurde er als Mitglied („Fellow“) in die Royal Society gewählt, die ihm 1938 die Hughes-Medaille und 1954 die Royal Medal verlieh. 1948 wurde er zum Ritter geschlagen. 1950 wurde er in die American Academy of Arts and Sciences gewählt. 1951 erhielt er (zusammen mit Ernest Thomas Sinton Walton) den Nobelpreis für Physik. 1953 wurde er zum Knight Commander des Order of the Bath (KCB) ernannt, 1957 schließlich zum Mitglied des prestigereichen Order of Merit. Am 6. April 1961 wurde ihm der Atoms for Peace Award, sowie in Wien die Wilhelm Exner-Medaille verliehen.

1901: Röntgen |
1902: Lorentz, Zeeman |
1903: Becquerel, M. Curie, P. Curie |
1904: Rayleigh |
1905: Lenard |
1906: J. J. Thomson |
1907: Michelson |
1908: Lippmann |
1909: Braun, Marconi |
1910: van der Waals |
1911: Wien |
1912: Dalén |
1913: Kamerlingh Onnes |
1914: Laue |
1915: W. H. Bragg, W. L. Bragg |
1916: nicht verliehen |
1917: Barkla |
1918: Planck |
1919: Stark |
1920: Guillaume |
1921: Einstein |
1922: N. Bohr |
1923: Millikan |
1924: M. Siegbahn |
1925: Franck, G. Hertz |
1926: Perrin |
1927: Compton, C. T. R. Wilson |
1928: O. W. Richardson |
1929: de Broglie |
1930: Raman |
1931: nicht verliehen |
1932: Heisenberg |
1933: Schrödinger, Dirac |
1934: nicht verliehen |
1935: Chadwick |
1936: Hess, C. D. Anderson |
1937: Davisson, G. P. Thomson |
1938: Fermi |
1939: Lawrence |
1940–1942: nicht verliehen |
1943: Stern |
1944: Rabi |
1945: Pauli |
1946: Bridgman |
1947: Appleton |
1948: Blackett |
1949: Yukawa |
1950: Powell |
1951: Cockcroft, Walton |
1952: Bloch, Purcell |
1953: Zernike |
1954: Born, Bothe |
1955: Lamb, Kusch |
1956: Shockley, Bardeen, Brattain |
1957: Yang, T.-D. Lee |
1958: Tscherenkow, Frank, Tamm |
1959: Segrè, Chamberlain |
1960: Glaser |
1961: Hofstadter, Mößbauer |
1962: Landau |
1963: Wigner, Goeppert-Mayer, Jensen |
1964: Townes, Bassow, Prochorow |
1965: Feynman, Schwinger, Tomonaga |
1966: Kastler |
1967: Bethe |
1968: Alvarez |
1969: Gell-Mann |
1970: Alfvén, Néel |
1971: Gábor |
1972: Bardeen, Cooper, Schrieffer |
1973: Esaki, Giaever, Josephson |
1974: Ryle, Hewish |
1975: A. N. Bohr, Mottelson, Rainwater |
1976: Richter, Ting |
1977: P. W. Anderson, Mott, Van Vleck |
1978: Kapiza, Penzias, R. W. Wilson |
1979: Glashow, Salam, Weinberg |
1980: Cronin, Fitch |
1981: Bloembergen, Schawlow, K. Siegbahn |
1982: K. Wilson |
1983: Chandrasekhar, Fowler |
1984: Rubbia, van der Meer |
1985: von Klitzing |
1986: Ruska, Binnig, Rohrer |
1987: Bednorz, Müller |
1988: Lederman, Schwartz, Steinberger |
1989: Paul, Dehmelt, Ramsey |
1990: Friedman, Kendall, R. E. Taylor |
1991: de Gennes |
1992: Charpak |
1993: Hulse, J. H. Taylor Jr. |
1994: Brockhouse, Shull |
1995: Perl, Reines |
1996: D. M. Lee, Osheroff, R. C. Richardson |
1997: Chu, Cohen-Tannoudji, Phillips |
1998: Laughlin, Störmer, Tsui |
1999: ’t Hooft, Veltman |
2000: Alfjorow, Kroemer, Kilby |
2001: Cornell, Ketterle, Wieman |
2002: Davis Jr., Koshiba, Giacconi |
2003: Abrikossow, Ginsburg, Leggett |
2004: Gross, Politzer, Wilczek |
2005: Glauber, Hall, Hänsch |
2006: Mather, Smoot |
2007: Fert, Grünberg |
2008: Nambu, Kobayashi, Maskawa |
2009: Kao, Boyle, Smith |
2010: Geim, Novoselov |
2011: Perlmutter, Schmidt, Riess |
2012: Haroche, Wineland |
2013: Englert, Higgs |
2014: Akasaki, Amano, Nakamura |
2015: Kajita, McDonald |
2016: Thouless, Haldane, Kosterlitz |
2017: Barish, Thorne, Weiss
