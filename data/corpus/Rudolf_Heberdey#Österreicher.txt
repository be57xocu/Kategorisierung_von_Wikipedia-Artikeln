Rudolf Heberdey (* 10. März 1864 in Ybbs an der Donau; † 7. April  1936 in Graz) war ein österreichischer Klassischer Archäologe.

Heberdey wurde am 13. Jänner 1887 Sub auspiciis Imperatoris promoviert und erhielt 1894 an der Universität Wien die Lehrbefugnis.[1] Ab 1898 war er Leiter der Zweigstelle des Österreichischen Archäologischen Instituts in Smyrna und von 1898 bis 1913 Leiter der Ausgrabung in Ephesos, ab 1904 Leiter der Zweigstelle Athen. Von 1909 bis 1911 war er Professor für Klassische Archäologie in Innsbruck, danach bis 1934 in Graz. Sein Nachlass ist im Besitz der Universitätsbibliothek Graz.

Emil Reisch (1890–1898) |
Franz Winter (1899–1905) |
Hans Schrader (1905–1908) |
Rudolf Heberdey (1909–1911) |
Heinrich Sitte (1912–1945) |
Otto Walter (1948–1951) |
Alfons Wotschitzky (1951–1969) |
Bernhard Neutsch (1972–1982) |
Elisabeth Walde (1984–2008) |
Erich Kistler (seit 2010)

Wilhelm Gurlitt (1890–1905) |
Franz Winter (1905–1908) |
Hans Schrader (1908–1910) |
Rudolf Heberdey (1911–1933) |
Arnold Schober (1940–1945) |
Erna Diez (1963–1983) |
Thuri Lorenz (1984–1999) |
Peter Scherrer (seit 2008)
