David Blunkett, Baron Blunkett (* 6. Juni 1947 in Sheffield) ist ein britischer Politiker der Labourpartei. Er war von 2001 bis 2004 Innenminister des Vereinigten Königreichs, 2005 Arbeitsminister und von 1987 bis 2015 Mitglied des House of Commons. Seit 2015 ist er Mitglied des House of Lords. 

Blunkett wurde in Sheffield geboren und wuchs in Armut auf, nachdem sein Vater bei einem Industrieunfall getötet wurde. Blind seit der Geburt, ging er auf Schulen für Blinde in Sheffield und Shrewsbury. Seine beruflichen Chancen im Leben schienen gering. Trotzdem errang er einen Platz an der Universität Sheffield und wurde im Alter von 22 das jüngste Mitglied, das jemals im Stadtrat von Sheffield saß. Er wurde als Figur des linken Flügels der Partei während seiner Führung des Stadtrates in den Achtzigern bekannt und in das nationale Führungskomitee der Labourpartei gewählt.

Bei den Parlamentswahlen 1987 wurde er zum Vertreter für Sheffield Brightside gewählt. Er wurde Sprecher der Partei für die Belange der Kommunalverwaltung, Mitglied des Schattenkabinetts 1992 als Schattengesundheitsminister und Schattenbildungsminister 1994. Als Kombination aus Reformbegeisterung und sozialem Konservatismus wurde er ein Günstling des neuen Parteiführers Tony Blair.

Nach dem Erdrutschsieg von Labour bei den Parlamentswahlen 1997 wurde er der erste blinde Minister des Vereinigten Königreichs und war für den Bereich Bildung und Beschäftigung zuständig. Der Bildungsminister spielte eine wichtige Rolle in der Regierung, deren Premierminister seine Prioritäten als "Bildung, Bildung, Bildung" beschrieb und als Schlüsselversprechen die Reduzierung der Größe von Schulklassen abgab. Letztlich ging es für Blunkett um bessere Bildungsbedingungen, als er sich für die Einführung von Studiengebühren an öffentlichen Universitäten aussprach, die traditionell gebührenfrei waren.

Beim Start der zweiten Regierungsperiode 2001 wurde Blunkett zum Innenminister befördert. Immigration und Asyl wurden für Blunkett im Innenministerium zentrale Themen. Im Dezember 2001 appellierte er an Immigranten, ein größeres Zugehörigkeitsgefühl zu Großbritannien zu entwickeln. Im April 2002 stellte er einen Antrag auf mehr Macht zum harten Durchgreifen bei illegaler Immigration und unbegründeten Ansprüchen auf politisches Asyl.

Ein weiteres umstrittenes Thema waren die Bürgerrechte. Er bezeichnete sie als Luftgespinste (siehe airy fairy libertarians). 2003 gab er eine Ausweitung der Untersuchungsbefugnisse bekannt, außerdem reduzierte ein neues Gesetz den Schutz des Einzelnen, etwa das Recht auf ein Geschworenengericht. Er versuchte nationale Personalausweise (ID cards, "entitlement cards") verpflichtend einzuführen; dies wurde jedoch fallen gelassen. Die Maßnahmen brachten ihm den Spitznamen Big Blunkett ein, in Bezug auf Orwells "Big Brother".

Blunketts Blindenhunde - Teddy, Offa, Lucy und Sadie - wurden vertraute Gestalten in Westminster, und waren Anlass für gelegentliche geistreiche Kommentare von Blunkett und seinen Parlamentskollegen auf beiden Seiten des Hauses. Bei einem denkwürdigen Vorfall erbrach sich Lucy im Parlament während einer Rede des Oppositionsmitglieds David Willetts. Generell ist Blunketts Blindheit jedoch kein Thema.

Es wurde in der Presse berichtet, dass Blunkett eine Beziehung zu der verheirateten, in Amerika geborenen Kimberly Fortier, der Herausgeberin des rechtsgerichteten Politmagazins The Spectator, hatte. Die dreijährige Beziehung solle im Sommer 2004 geendet haben, nachdem Fortier sich entschlossen hatte, zu ihrem Ehemann Stephen Quinn zurückzukehren. Berichte sagten, dass Blunkett gerichtliche Schritte einleiten wolle, um zu beweisen, dass er der Vater von Fortiers Sohn William und des im Januar 2005 erwarteten Kindes sei.

Im Zusammenhang mit dieser Liebesaffäre wurde Blunkett Amtsmissbrauch vorgeworfen. So soll er Leoncia "Luz" Casalme, dem philippinischen Kindermädchen von Kimberly Fortier, eine Aufenthaltsgenehmigung beschafft haben. Gegen ihn wurde aufgrund dieser Vorwürfe eine offizielle Untersuchung eingeleitet. Am 15. Dezember 2004 zog Blunkett die Konsequenzen und trat als Minister zurück.

Doch bereits nach den Unterhauswahlen 2005 berief ihn Tony Blair erneut ins Kabinett zurück, diesmal als Minister für Arbeit und Pensionen, was er jedoch nur bis zum November desselben Jahres blieb. 

Am 20. Juni 2014 gab Blunkett bekannt, das er bei den Unterhauswahlen 2015 nicht mehr für das House of Commons kandidieren werde. Er schied damit am 30. März 2015 nach über 27 Jahren Mitgliedschaft aus dem Unterhaus aus. 

Am 28. September 2015 wurde Blunkett zum Life Peer mit dem Titel Baron Blunkett, of Brightside and Hillsborough in the City of Sheffield ernannt. Damit wurde er auch Mitglied des House of Lords.[1]
Blunkett über eine Bemerkung des englischen Fußballtrainers Glenn Hoddle, Behinderte hätten ihre Behinderung selbst zu verantworten, weil sie in einem vorherigen Leben eine Sünde begangen hätten:

"If Glenn Hoddle is right then I must have been a failed football manager in a previous existence." (Wenn Glenn Hoddle recht hat, dann muss ich in einem vorherigen Leben ein gescheiterter Fußballmanager gewesen sein.)

Tony Blair |
John Prescott |
Gordon Brown |
Robin Cook |
Jack Straw |
Derry Irvine |
Ivor Richard |
Margaret Jay |
Ann Taylor |
David G. Clark |
Alistair Darling |
Stephen Byers |
Alan Milburn |
Andrew Smith |
Frank Dobson |
Jack Cunningham |
Nick Brown |
George Robertson |
Geoff Hoon |
Harriet Harman |
David Blunkett |
Margaret Beckett |
Peter Mandelson |
Chris Smith |
Clare Short |
Mo Mowlam |
Donald Dewar |
John Reid |
Helen Liddell |
Ron Davies |
Alun Michael |
Paul Murphy |
Gavin Strang

Tony Blair |
John Prescott |
Gordon Brown |
Jack Straw |
David Blunkett |
Derry Irvine |
Charles Falconer |
Gareth Wyn Williams |
Robin Cook |
Andrew Smith |
Paul Boateng |
Margaret Beckett |
Alan Milburn |
Stephen Byers |
Geoff Hoon |
Alistair Darling |
Alan Johnson |
Estelle Morris |
Ruth Kelly |
Patricia Hewitt |
Tessa Jowell |
Clare Short |
Valerie Amos |
Hilary Benn |
John Reid |
Helen Liddell |
Paul Murphy |
Peter Hain |
Charles Clarke |
Hilary Armstrong
