Shalyn Casar (* 7. Februar 1988 in Lüdinghausen) ist eine deutsche Schauspielerin.

Ihr Abitur machte sie 2007 auf dem Max-Born-Berufskolleg Recklinghausen mit einer zusätzlichen Ausbildung zur Gestaltungstechnischen Assistentin. Sie dreht unter anderem als Kellnerin Lina für die ARD-Serie Verbotene Liebe. Weitere Rollen besetzte sie u.a. in dem ProSieben-Eventmovie Schlagwetter und in der Sat-1-Primetime-Serie R. I. S. – Die Sprache der Toten.
