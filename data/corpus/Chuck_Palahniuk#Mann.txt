Chuck Palahniuk [ˈpɔːlənɪk], bürgerlich Charles Michael Palahniuk (* 21. Februar 1962 in Pasco, Washington), ist ein US-amerikanischer Schriftsteller und Journalist. Er wohnt derzeit in Vancouver und ist vor allem durch sein Erstlingswerk Fight Club (1996) bekannt geworden, das von David Fincher mit Brad Pitt und Edward Norton verfilmt wurde (siehe auch Fight Club). Sein Roman Choke (2008) wurde von Clark Gregg mit Anjelica Huston und Sam Rockwell ebenfalls verfilmt.

Vor seiner Karriere als Schriftsteller besuchte Palahniuk die School of Journalism an der University of Oregon, arbeitete als freier Journalist sowie Nutzfahrzeugmechaniker und war ehrenamtlich in Obdachlosenheimen und Jugendherbergen tätig.

Auf seiner Internetpräsenz bietet Palahniuk „Premiumnutzern“ unter anderem Schreibseminare an und beantwortet einen großen Teil der Leseranfragen persönlich. Seine Internetpräsenz gilt als eine der größten zentralen Anlaufstellen für einen einzelnen Autor.

Palahniuk ist Mitglied der Cacophony Society, einer anarchistischen Gesellschaft, die dem Suicide Club of San Francisco entsprungen ist. Das sogenannte Project Mayhem aus seinem Roman Fight Club basiert auf den Grundgedanken der Cacophony Society.

Palahniuks Idee zu Fight Club entstand, als er eines Morgens von einer Rauferei gezeichnet zur Arbeit erschien und keiner seiner Kollegen ihn darauf ansprach. Er überlegte, ob es möglich sei, solch ein Doppelleben zu führen, ohne dass von der Gesellschaft unangenehme Fragen gestellt werden. Obwohl Fight Club Palahniuks erste Veröffentlichung ist, hat er zuvor bereits Invisible Monsters und Insomnia: If You Lived Here, You'd Be Home Already geschrieben. Invisible Monsters fand zunächst aufgrund seines „verstörenden Inhalts“ keinen Verleger, und mit Insomnia: If You Lived Here, You'd Be Home Already war Palahniuk nicht zufrieden genug, um es veröffentlichen zu lassen; es bildete jedoch die Grundlage für Fight Club.

Die Kolonie ist ein aus 24 Kurzgeschichten in einer Rahmenhandlung zusammengesetzter Roman. Die Kurzgeschichte Guts (deutscher Titel: Vorfall) sorgte auf Palahniuks Lesungen für Aufregung, da bei diesen insgesamt 73 Leute ohnmächtig wurden.[1] Die Geschichte ist als Sonderbeilage zum Time Magazine erschienen.

Die 2003 produzierte Dokumentation Postcards from the Future befasst sich mit Palahniuk und seinem Status als Kultautor in den USA.
