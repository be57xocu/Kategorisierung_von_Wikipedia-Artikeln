Erich Hoepner (* 14. September 1886 in Frankfurt (Oder); † 8. August 1944 in Berlin-Plötzensee) war ein deutscher Heeresoffizier (seit 1940 Generaloberst) und Widerstandskämpfer des Attentats vom 20. Juli 1944.

Erich Hoepner wurde als Sohn des Arztes Kurt Hoepner geboren. 1890 siedelte seine Familie nach Berlin-Charlottenburg um. Ab 1893 besuchte er das Kaiserin-Augusta-Gymnasium,[1] wo er 1905 sein Abitur machte. Im Juni 1909 trat Hoepner als Fahnenjunker in das Schleswig-Holsteinische Dragoner-Regiment Nr. 13 (Festung Metz) ein. 1910 heiratete er Irma Gebauer. Aus dieser Ehe gingen zwei Kinder hervor. Im Oktober 1913 wurde er an die Kriegsakademie in Berlin kommandiert.

Ab August 1914 war Hoepner Ordonnanzoffizier des XVI. Armee-Korps. Ab 1916 diente er an der Front. Im Verlauf des Ersten Weltkrieges wurde Hoepner zum Rittmeister befördert und mehrfach ausgezeichnet, unter anderem mit beiden Klassen des Eisernen Kreuzes, dem Ritterkreuz des Königlichen Hausordens von Hohenzollern mit Schwertern sowie dem Ritterkreuz 2. Klasse mit Schwertern des Württembergischen Friedrichordens.[2]
1920 wurde Erich Hoepner Eskadronchef im Reiter-Regiment 2 (Allenstein in Ostpreußen). Dieser Aufgabe schloss sich ab 1921 der Dienst als Hauptmann im Generalstab der Inspektion der Kavallerie in Berlin an. Ab 1923 war er Generalstabsoffizier der 1. Kavallerie-Division in seiner Geburtsstadt Frankfurt (Oder). Hoepner wurde 1927 Major im Generalstab und Erster Generalstabsoffizier (Ia) des Wehrkreiskommandos I in Königsberg (Ostpreußen). Ab 1930 diente er als Bataillonskommandeur im Infanterie-Regiment Nr. 17 in Braunschweig. Im Jahre 1932 wurde Hoepner zum Oberstleutnant befördert. Er war zu dieser Zeit Kommandeur des Reiter-Regimentes Nr. 4 in Potsdam.

Hoepner wurde 1933 zum Chef des Generalstabes des Wehrkreises I in Königsberg ernannt. 1933 erfolgte auch seine Beförderung zum Oberst. Zu diesem Zeitpunkt war er Stabschef des Gruppenkommandos I in Berlin. 1937 erfolgte die Ernennung zum Generalmajor. Erich Hoepner war Kommandeur der 1. Leichten Division in Wuppertal. Schon 1938 wurde er zum Generalleutnant befördert. In den letzten Monaten vor Ausbruch des Zweiten Weltkrieges diente Hoepner im Rang eines General der Kavallerie als Kommandierender General des XVI. Armeekorps in Berlin.

Im Polenfeldzug war Hoepner Kommandierender General des XVI. Armeekorps (mot.), das zwei Panzer- und zwei Infanterie-Divisionen umfasste und der 10. Armee angehörte. Am 27. Oktober 1939 erhielt er das Ritterkreuz des Eisernen Kreuzes.[2] In gleicher Stellung nahm er am Westfeldzug teil und war in den Schlachten von Hannut und Gembloux kommandierender Offizier. Er wurde am 19. Juli 1940 zum Generaloberst befördert.

Hoepner formulierte am 2. Mai 1941 in der „Aufmarsch- und Kampfanweisung Barbarossa“ seine Ansichten zum kommenden Krieg:

„Der Krieg gegen Rußland ist ein wesentlicher Abschnitt im Daseinskampf des deutschen Volkes. Es ist der Kampf der Germanen gegen das Slawentum, die Abwehr des jüdischen Bolschewismus. Dieser Kampf muß die Zertrümmerung des heutigen Rußland zum Ziele haben und deshalb mit unerhörter Härte geführt werden. Jede Kampfhandlung muß in Anlage und Durchführung von dem eisernen Willen zur erbarmungslosen, völligen Vernichtung des Feindes geleitet sein. Insbesondere gibt es keine Schonung für die Träger des heutigen russisch-bolschewistischen Systems.“

Das Korps wurde Anfang des Jahres 1941 in Panzergruppe 4 umbenannt. Als Befehlshaber unterstanden Hoepner zu Beginn des Russlandfeldzuges im Juni 1941 das XXXXI. Armeekorps und das LVI. Armeekorps (mot.) mit insgesamt sieben Divisionen. Mit seinen Truppen kämpfte er in der Doppelschlacht bei Wjasma und Brjansk. Schließlich wurde die Panzergruppe im Dezember 1941 zur 4. Panzerarmee aufgewertet. Dieser Großverband bestand nun aus fünf Armeekorps mit zwölf Divisionen. Hoepner galt zu dieser Zeit neben Guderian, Hoth, von Kleist und Rommel als einer der erfolgreichsten und bekanntesten Panzerführer.

Beim Angriff auf die Sowjetunion im Jahre 1941 ordnete Hoepner als Kommandeur des XXXXI. Armeekorps und des LVI. Armeekorps die „Erschießung russischer Kommissare in Uniform“ und zusätzlich auch „die gleiche Behandlung von Zivilkommissaren“ an. Dieser völkerrechtswidrige Befehl ging damit über den Kommissarbefehl hinaus, der die unterschiedslose Tötung der sowjetischen Partei- und Verwaltungsfunktionäre nicht verlangte.

Bereits ab 1935 knüpfte Hoepner durch seinen vorgesetzten Offizier Ludwig Beck Kontakte zum deutschen Widerstand. Im September 1938 während der Sudetenkrise stellte er sich und seine Division der Widerstandsgruppe um Beck und Franz Halder für einen geplanten Staatsstreich gegen Hitler zur Verfügung. Das Münchner Abkommen vereitelte die Pläne jedoch, und der Staatsstreich kam nicht zur Ausführung.[4]
Am 8. Januar 1942 wurde Erich Hoepner von Adolf Hitler wegen „Feigheit und Ungehorsams“ unehrenhaft aus der Wehrmacht entlassen, nachdem er in der sowjetischen Winteroffensive einen Durchhaltebefehl ignoriert und den taktischen Rückzug seiner Einheiten angeordnet hatte. Dies hatte den Verlust aller Orden und Ehrenzeichen zur Folge sowie die Aberkennung des Rechtes zum Tragen einer Uniform.[5][6] Hoepner begründete gegenüber Feldmarschall von Kluge seine Entscheidung: „Herr Generalfeldmarschall, ich habe Pflichten, die höher stehen als die Pflichten Ihnen gegenüber und die Pflichten gegenüber dem Führer. Das sind die Pflichten gegenüber der mir anvertrauten Truppe.“[7] Am 12. Januar kehrte er nach Berlin zurück, um sich gegen Hitlers Willkür zu wehren, denn Hitler hätte sich strenger formaljuristischer und beamtenrechtlicher Verfahren bedienen müssen, um Hoepner zu entlassen. Deshalb erließ der Großdeutsche Reichstag am 26. April 1942 einen Beschluss, wonach der „Führer“ ermächtigt wurde, auch ohne Einhaltung bestehender Rechtsvorschriften jeden Deutschen aus seinem Amte zu entfernen.[8]
Im Sommer 1943 siedelte er mit der Familie zur Schwester nach Bredereiche (heute Fürstenberg/Havel) über. Im Herbst 1943 wurde er in die Pläne zum Attentat eingeweiht und um Unterstützung gebeten. Am 19. Juli 1944 kehrte Erich Hoepner nach Berlin zurück. Während des gescheiterten Attentats- und Umsturzversuchs des 20. Juli 1944 befand sich Hoepner, der bei Gelingen des Planes als „Oberbefehlshaber im Heimatkriegsgebiet“ vorgesehen war, im Bendlerblock. Dort wurde er in den frühen Morgenstunden des 21. Juli verhaftet.

Hoepner wurde im ersten Prozess am 7. und 8. August 1944 beim Volksgerichtshof wegen Verrats am Volke vor Gericht gestellt. Mitangeklagt war auch unter anderem Generalfeldmarschall Erwin von Witzleben, den Vorsitz führte während der Dauer der Prozesse der Präsident des Volksgerichtshofes, Roland Freisler, persönlich. Am 8. August verurteilte dieser Hoepner zum Tod. Das Urteil wurde noch am selben Tag in der Hinrichtungsstätte des Strafgefängnisses Berlin-Plötzensee auf ausdrücklichen Befehl Hitlers durch Hängen vollstreckt.[9]
Generaloberste des HeeresAdam |
von Arnim |
Beck |
Blaskowitz |
Dietl |
Dollmann |
von Falkenhorst |
Frießner |
von Fritsch |
Fromm |
Guderian |
Haase |
Halder |
von Hammerstein-Equord |
Harpe |
Heinrici |
Heitz |
Hilpert |
Hoepner |
Hollidt |
Hoth |
Hube |
Jaenecke |
Jodl |
Lindemann |
von Mackensen |
Raus |
Reinhardt |
Rendulic |
Ruoff |
von Salmuth |
Schmidt |
von Schobert |
Strauß |
von Viettinghoff |
Weiß |
Zeitzler

Generaloberste der LuftwaffeDeßloch |
Grauert |
Jeschonnek |
Keller |
Korten |
Loerzer |
Löhr |
Rüdel |
Student |
Stumpff |
Udet |
Weise

Generaladmirale der KriegsmarineAlbrecht |
Boehm |
Carls |
von Friedeburg |
Kummetz |
Marschall |
Saalwächter |
Schniewind |
Schultze |
Warzecha |
Witzell

SS-Oberst-Gruppenführer und Generaloberste der Waffen-SSDietrich |
Hausser
