Gottfried Biegelmeier (* 19. Juli 1924 in Wien; † 8. Juli 2007 in Lunz am See) war ein österreichischer Physiker.

Er studierte Physik an der Wiener Universität und promovierte 1949. Beruflich begann er bei Prüfstellen der österreichischen Elektrizitätswerke und im Wiener Arsenal.

Ab 1957 leitete er die Entwicklung der ehemaligen Firma Felten & Guilleaume, der heutigen Eaton Industries, in Schrems, wo er auch den Fehlerstrom-Schutzschalter und den Spannungsschutzschalter weiterentwickelte. Seine Spezialgebiete waren Schutztechnik und Schutzmaßnahmen.

In der Folge arbeitete er auf dem Gebiet der Elektropathologie, bei der er unter anderem Selbstversuche bei direkter Berührung von Spannung führenden Teilen durchführte. Bei seinen Studien erlangte er wertvolle Kenntnisse über Gefahrengrenzen beim Herzkammerflimmern.

Für seine Forschungen erhielt er zahlreiche in- und ausländische Ehrungen, wie 1978, als er den Berufstitel Professor[1] erhielt oder 1993 das Ehrenkreuz für Wissenschaft und Kunst I. Klasse und den amerikanischen „IEEE-Power Life Award“.

In seinem Bestreben zur Sicherheit in der Elektrotechnik errichtete er die Gemeinnützige Privatstiftung Elektroschutz (ESF)[2], die alle zwei bis drei Jahre an verdiente Persönlichkeiten die Alvensleben-Jellinek-Ehrenmedaille verleiht.

Am 8. Juli 2007 verstarb Biegelmeier kurz vor Vollendung seines 83. Lebensjahrs in Lunz am See.[3]