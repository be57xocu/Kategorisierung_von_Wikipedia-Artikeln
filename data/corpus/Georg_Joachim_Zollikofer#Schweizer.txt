Georg Joachim Zollikofer (* 5. August 1730 in St. Gallen; † 22. Januar 1788 in Leipzig) war ein schweizerisch-deutscher Theologe und Kirchenliederdichter.

Zollikofer war Sohn des Rechtsgelehrten David Anton Zollikofer aus dem Geschlecht der Zollikofer von Altenklingen auf Schloss Altenklingen und dessen Ehefrau Anna Elisabeth Högger. Nach dem Besuch der Gymnasien von St. Gallen und Frankfurt am Main begleitete er den Sohn des Frankfurter Buchhändlers Bernus auf dessen Grand Tour durch die Niederlande. Ab 1751 studierte Zollikofer an den Universitäten in Bremen, Hanau und Utrecht und kehrte 1753 wieder in seine Heimatstadt zurück.

1754 übernahm er eine Predigerstelle in der reformierten Gemeinde Murten im heutigen Kanton Freiburg, verließ die Gemeinde jedoch bereits nach einer einjährigen Tätigkeit. Von dort aus wurde er in die Gemeinde Monsheim (Rheinhessen) versetzt. Dort lernte er im Hause der Familie von La Roche seine spätere Ehefrau Susanna Regina Le Roy kennen.

Am 6. März 1758 wurde Zollikofer Pfarrer in einer Gemeinde in der 1699 von den Hugenotten gegründeten Exulantenstadt Neu-Isenburg. Im gleichen Jahr wechselte er Ende April nach Leipzig, wo er seine Lebensstellung als Pfarrer der reformierten Gemeinde fand. Am 13. August 1758 hielt er in Leipzig seine Antrittspredigt.

Als seine Ehefrau nach einigen Jahren starb, verehelichte sich Zollikofer bald darauf 1780 mit Henrike Sechehaye.

Da die Tätigkeit in einer nicht übermäßig großen Gemeinde ihm Zeit für andere Tätigkeiten ließ, bildete er bald eine besondere Begabung für die Ausbreitung aufklärerischen Gedankengutes heraus. Als Denis Diderot im Jahre 1773 nach Sankt Petersburg fuhr, um einer Einladung der russischen Zarin Katharina die Große nachzukommen, traf er am 2. September 1773 in Leipzig mit Zollikofer zusammen.

In der vom Luthertum geprägten Stadt Leipzig wurde Zollikofer ein gefeierter Prediger, und immer wieder soll eine große Zahl von Theologiestudenten der Universität in seine Gottesdienste gekommen sein, um seine berühmten Predigten zu hören. 

Zollikofer ist der Textdichter des Kirchenliedes Laß mich, o Herr, in allen Dingen (EG 414). 

Im Alter von fast 58 Jahren starb Georg Joachim Zollikofer am 22. Januar 1788 in Leipzig, im Ortsteil Volkmarsdorf wurde ihm zu Ehren eine Straße benannt.

