Aramis Knight (* 3. Oktober 1999, in Woodland Hills, Kalifornien) ist ein amerikanischer Schauspieler. Er wurde vor allem durch die Rolle als Bean in Ender’s Game bekannt.

Knight wurde in Woodland Hills (Stadtteil von Los Angeles) geboren. Im Jahr 2005 fing er an, Werbespots und kleine Filmrollen zu drehen. Seitdem hat er Gastrollen und tritt als wiederkehrender Charakter in mehreren Serien auf, darunter NCIS, Psych, Lost, Dexter und Boston Legal. Sein Durchbruch gelang im schließlich im Jahr 2013 in dem Science-Fiction Film Ender’s Game, wo er den Rekruten Bean verkörperte.
