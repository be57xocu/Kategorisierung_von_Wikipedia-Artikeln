Alan Mackenzie Howard CBE (* 5. August 1937 in London; † 14. Februar 2015 ebenda) war ein britischer Theater- und Filmschauspieler.

Alan Howard entstammt einer bekannten Schauspielerfamilie. Die Familie seiner Mutter Jean Compton Mackenzie hatte bereits seit dem frühen 19. Jahrhundert mehrere Generationen von Schauspielern hervorgebracht. Sein Vater Arthur John Howard war der jüngere Bruder des britischen Filmstars Leslie Howard, der unter anderem die Rolle des Ashley Wilkes im Film Vom Winde verweht spielte.

Howard, der niemals eine Schauspielschule besucht hatte, begann seine Karriere 1958 am Belgrade Theatre in Coventry. Von 1967 bis 1982 war er Mitglied der Royal Shakespeare Company und spielte dort in zahlreichen Stücken eine der Hauptrollen. Neben seiner Arbeit am Theater wirkte Alan Howard seit 1961 auch in vielen Kinofilmen und Fernsehproduktionen mit. In Peter Jacksons Verfilmung des Herrn der Ringe verleiht er dem dunklen Herrscher Sauron seine Stimme.

Für seine Theaterrollen erhielt er zahlreiche Auszeichnungen. 1998 ernannte ihn Königin Elisabeth II. zum Commander of the Order of the British Empire.

2004 heiratete Howard seine langjährige Partnerin, die Journalistin und Autorin Sally Beauman, mit der er einen gemeinsamen Sohn hatte. Er starb am 14. Februar 2015 in seiner Heimatstadt London im Alter von 77 Jahren an den Folgen einer Lungenentzündung.[1]