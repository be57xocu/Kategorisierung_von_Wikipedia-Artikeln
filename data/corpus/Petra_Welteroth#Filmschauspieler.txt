Petra Welteroth (* 1959 in Eitorf, Nordrhein-Westfalen) ist eine deutsche Theaterschauspielerin und Jazz-Sängerin.

Petra Welteroth wurde von 1978 bis 1981 im Theater der Keller in Köln ausgebildet. Von 1981 bis 1984 arbeitete sie am Stadttheater Ingolstadt. Danach arbeitete sie an verschiedenen deutschsprachigen Theatern, unter anderem am Millowitsch-Theater. Von 1997 bis 2000 arbeitete sie beim Theater Aachen mit und erhielt 2004 den Kurt-Sieder-Preis. Seit 2005 ist sie freiberuflich unterwegs, vor allem als Jazz-Sängerin, aber auch als Autorin.

Welteroth lebt in Aachen.
