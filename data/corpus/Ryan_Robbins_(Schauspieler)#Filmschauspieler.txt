Ryan Robbins (* 26. November 1972 in Victoria, British Columbia) ist ein kanadischer Schauspieler.

Sein Debüt hatte Ryan Robbins im Jahr 1997 im kanadischen Spielfilm Horsey, gefolgt von bislang über 100 weiteren Rollen in Fernsehserien, Fernseh- und Kinofilmen, darunter The L Word – Wenn Frauen Frauen lieben und in Battlestar Galactica. Einem breiteren deutschsprachigen Publikum dürfte er durch seine Rollen als Henry Foss in Sanctuary – Wächter der Kreaturen (2008−2011) und als Tector in Falling Skies bekannt sein.
