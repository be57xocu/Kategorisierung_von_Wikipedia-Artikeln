Loris Azzaro (* 9. Februar 1933 in Tunis; † 20. November 2003 in Paris) war ein französischer Modeschöpfer und Parfümeur.

Er stattete als Modemacher unter anderem Filmstars wie Raquel Welch, Sophia Loren oder Isabelle Adjani mit Kleidern und Hosenanzügen aus.
Er entwickelte die Herrenpflegeserie Azzaro pour homme, deren Lizenz-Hersteller Clarins ist.
