Friedrich Peter (* 13. Juli 1921 in Attnang-Puchheim, Oberösterreich; † 25. September 2005 in Wien) war ein österreichischer Politiker und von 1958 bis 1978 Parteiobmann der FPÖ.

Der Sohn eines sozialdemokratischen Lokomotivführers und einer bürgerlichen Bäckermeisterstochter trat im November 1938 der NSDAP bei und meldete sich im Alter von knapp 17 Jahren freiwillig zur Waffen-SS. Im Zweiten Weltkrieg war er an der West- und Ostfront eingesetzt, zuletzt als SS-Obersturmführer beim Infanterie-Regiment 10 der 1. SS-Infanteriebrigade. Teile dieser Einheit waren im Sommer 1941 der Einsatzgruppe C zugeordnet. Die Einsatzgruppen erschossen hinter der Front systematisch hunderttausende Juden. Obwohl seine Einheit fast ausschließlich in solche Aktionen involviert war, leugnete Peter nach dem Kriege, dass er an diesen Vorgängen beteiligt war oder davon gewusst habe. Nach dem Krieg war er ein Jahr lang in einem amerikanischen Anhaltelager Glasenbach (heute Alpensiedlung) inhaftiert.

Nach seiner Haft wurde er Volks- und Sonderschullehrer, später auch Landesschulinspektor.
Von 1955 bis 1966 gehörte er als Abgeordneter dem Oberösterreichischen Landtag an, zuerst als Vertreter des VdU, dann der FPÖ, deren Bundesparteiobmann er ab 1958 war. 1966 wurde er in den Nationalrat gewählt und wurde 1970 Klubobmann.

Bereits 1962/1963 kam es zu einer Annäherung der FPÖ an die SPÖ, was Teile des rechtsextrem-nationalen Flügels der Partei vor den Kopf stieß und dazu führte, dass große Teile dieses Spektrums sich abspalteten. Während Peter Parteivorsitzender war, versuchte die FPÖ allmählich koalitionsfähig zu werden und bemühte sich nach außen hin, liberaler zu wirken. Auf dem Parteitag 1964 erklärte Peter erstmals, dass „Nationale und Liberale in der FPÖ gemeinsam Platz haben“. Die „Liberalisierung“ der Partei in dieser Phase führte innerparteilich vereinzelt zu Widerstand, auf den Peter mit Parteiausschlüssen reagierte. Obwohl die FPÖ im Wahlkampf 1970 noch „Kein roter Kanzler“ beteuert hatte, tolerierte sie danach die Minderheitsregierung von Bruno Kreisky, der sich seinerseits durch eine Wahlrechtsreform revanchierte, die für die FPÖ eine starke Aufwertung bedeutete.

Simon Wiesenthal, zu diesem Zeitpunkt Leiter des jüdischen Dokumentationszentrums in Wien, veröffentlichte nach der Nationalratswahl 1975 einen Bericht über die Nazivergangenheit des damaligen FPÖ-Chefs Friedrich Peter. Aus diesem Bericht ging hervor, dass Peter als Obersturmführer in einer mit Massenmorden in Verbindung stehenden SS-Einheit gedient hatte. Bundeskanzler Kreisky, selbst Verfolgter des Nazi-Regimes, verteidigte jedoch Friedrich Peter und beschuldigte Simon Wiesenthal, mit „Mafiamethoden“ zu arbeiten, und unterstellte ihm sinngemäß Kollaboration mit der Gestapo. Diese öffentliche Auseinandersetzung wird heute unter dem Begriff Kreisky-Peter-Wiesenthal-Affäre subsumiert.

1978 kandidierte Peter nicht mehr als Bundesparteiobmann. Sein Nachfolger wurde der Grazer Bürgermeister Alexander Götz. Im Hintergrund zog Peter aber nach wie vor die Fäden in der Partei. Nachdem die SPÖ 1983 die absolute Mehrheit verloren hatte, handelte er mit Bruno Kreisky die kleine Koalition unter Bundeskanzler Fred Sinowatz und Vizekanzler Norbert Steger aus. Das Angebot, in Anerkennung seiner Verdienste zum dritten Nationalratspräsidenten gewählt zu werden, musste er nach heftigen Protesten in der Öffentlichkeit ablehnen, auch um die kleine Koalition nicht zu gefährden.

Zu Jörg Haider hatte er immer ein gespanntes Verhältnis. Der endgültige Bruch kam 1992 nach Haiders Aussage über die „ordentliche Beschäftigungspolitik im dritten Reich“. Peter sprach von einer „beschämenden Entgleisung Haiders“ und meinte, dass ihn diese Äußerung zwinge, sein „selbst auferlegtes Schweigen zu brechen und die Führungsorgane der Freiheitlichen Partei Österreichs an ihre staatspolitischen und satzungsgemäßen Pflichten in aller Öffentlichkeit zu erinnern“.

Friedrich Peter starb am 25. September 2005 im Wiener Hanusch-Krankenhaus, wo er mehrere Wochen wegen eines Nierenleidens behandelt worden war.

Anton Reinthaller |
Friedrich Peter |
Alexander Götz |
Norbert Steger |
Jörg Haider |
Susanne Riess-Passer |Mathias Reichhold |
Herbert Haupt |
Ursula Haubner |
Hilmar Kabas |
Heinz-Christian Strache
