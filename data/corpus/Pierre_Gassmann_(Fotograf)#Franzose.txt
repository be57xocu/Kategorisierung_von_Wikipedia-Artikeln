Pierre Gassmann (* 1913 [nach anderen Quellen 1914] in Breslau; † 5. Juli 2004 in Paris) ist der Gründer von Picto und ein berühmter Fotolaborant (Printer).[1][2]
Gassmann wurde 1913 in Breslau geboren, wo seine Mutter ein Röntgenlabor betrieb. Er begann im Alter von zwölf Jahren mit dem Fotografieren und wurde beeinflusst von Künstlern wie Paul Klee, Oskar Kokoschka oder László Moholy-Nagy, die zum Bekanntenkreis seiner Mutter gehörten.

Er begann in Berlin zu studieren, floh jedoch als Jude und Kommunist unmittelbar nach der Machtergreifung durch die Nationalsozialisten nach Paris in den Montparnasse. Dort traf er neben zahlreichen anderen Künstlern auch seine spätere Geliebte, Gerda Taro, und 1935 den Ungarn Endre Friedmann; Friedmann, der sich jetzt Robert Capa nannte, hatte eine Affäre mit Gerda Taro, die Freundschaft zwischen Gassmann und Capa hielt jedoch bis zu Capas Tod.

Seinen Lebensunterhalt bestritt Gassmann als „Laborant“, zunächst noch im heimischen Badezimmer; seine Geschicklichkeit im Fotolabor sprach sich rasch herum, obwohl es zu dieser Zeit noch keine Fachlabore gab: Wer beruflich fotografierte, entwickelte seine Bilder selbst.[3]
Nicht so jedoch der Franzose Henri Cartier-Bresson, der zu einer neuen Generation von Fotojournalisten gehörte und weder Geschick noch Interesse für die Laborarbeit aufbrachte. Gassmann wurde so zur rechten Hand Cartier-Bressons; zu seinen weiteren namhaften Kunden und teilweise auch Freunden zählten seit Ende der 40er Jahre u. a. Jacques-Henri Lartigue, André Kertész, Robert Doisneau, Martin Munkácsi, Robert Capa und Emmanuel Rudnitzky, besser bekannt als Man Ray.

Neben seiner Laborarbeit fotografierte er in Paris; als 1947 die Agentur Magnum Photos gegründet wurde, zählte er zu den Gründungsmitgliedern. Nach Ausbruch des Krieges tauchte Gassmann unter und arbeitete für die Résistance.

Nach Kriegsende arbeitete Gassmann noch kurz als Fotoreporter, bis er 1949 feststellte, „überall, wo er hinkam, war Robert Capa vor ihm gewesen, und er hatte die besseren Bilder gemacht“ (Michael Ebert in: Der König des Schattenreichs). Danach gab er das Fotografieren auf und konzentrierte sich vollständig auf das, was er am besten konnte: Die Laborarbeit.

1950 gründete er daher das erste Fachlabor Europas, Picto (Pictorial Service), in der Rué de la Comète. Bestärkt durch den raschen Erfolg richtete er noch ein Mietstudio ein, ein Konzept, das später F. C. Gundlach mit seinem Professional Photo Service (PPS) in Hamburg nachmachte.

Gassmann und das Labor Picto erwarben sich einen ausgezeichneten Ruf; Harper’s Bazaar ließ dort Aufträge aus Übersee verarbeiten, zwischen 1956 und 1978 stellte Picto zahlreiche Prints für die Photokina-Bilderschauen her und für Magnum Photos war Picto schlicht „unser Labor“. Picto betreibt heute 12 Filialen in Frankreich und eine in Südafrika (Kapstadt).

Gassmann blieb bis ins hohe Alter aktiv, arbeitete im Labor und setzte sich mit der Digitaltechnik auseinander; um 1997 schaffte er einen Apple Macintosh an und erlernte noch mit 84 Jahren die digitale Bildbearbeitung: „Mit der Gelassenheit des Mannes, der sich selbst erfunden hatte, saß er am Mac, scannte und druckte. Anachronistisches Verhalten passte nicht zu dem Pragmatiker. Die Art der Herstellung war sekundär, für ihn zählte das Ergebnis, das gute Bild – und dem gehörte seine ganze Leidenschaft“ (Michael Ebert in: Der König des Schattenreichs).

Im Sommer 2004 starb Gassmann im Alter von 91 Jahren.

Pierre Gassmann erklärte 1995 in einem Interview sein Selbstverständnis: „Der Fotograf ist der Komponist, er schreibt die Noten, und ich bin der Dirigent, der seine Partitur interpretiert“.

Mit seinen eigenen Fotografien gelang es Gassmann nie, aus dem Schatten von Capa oder Cartier-Bresson herauszutreten; mit seinen hochwertigen Prints prägte er aber – quasi aus dem Hintergrund – das, was die Öffentlichkeit von diesen Fotokünstlern zu sehen bekam: die Vergrößerungen ihrer Bilder; so fertigte er beispielsweise die autorisierten Abzüge für Man Ray an, die heute in Ausstellungen gezeigt werden.
