Anton Webern (* 3. Dezember 1883 in Wien; † 15. September 1945 in Mittersill, Salzburg, Österreich; vollständiger Name: Anton Friedrich Wilhelm von Webern; das „von“ musste er 1919 aufgrund des Adelsaufhebungsgesetzes vom 3. April 1919 ablegen) war ein österreichischer Komponist und Dirigent. Als einer der ersten Schüler von Arnold Schönberg gehörte er zum inneren Kreis der Wiener Schule.

Webern, Sohn von Karl Freiherr von Webern, einem erfolgreichen Bergbauingenieur, wuchs in Graz und Klagenfurt auf. Die Familie war 1574 in den Adelsstand erhoben worden. Durch seine Mutter erhielt Anton Webern früh Klavierunterricht, später erteilte ihm Edwin Komauer Privatunterricht in Musiktheorie, außerdem erlernte Webern das Violoncello-Spiel. Von Herbst 1902 bis 1906 studierte er an der Universität Wien Musikwissenschaft. Er promovierte dort mit einer Edition des Choralis Constantinus II von Heinrich Isaac, die 1909 als Band 32 der Denkmäler der Tonkunst in Österreich erschien.[1]
Von 1904 bis 1908 erhielt Webern Kompositionsunterricht von Arnold Schönberg.

In den folgenden Jahren arbeitete Webern zeitweise als Kapellmeister in Bad Ischl, Teplitz, Danzig, Stettin und Prag – eine Tätigkeit, über die er in vielen Briefen klagte.

Nach dem Ersten Weltkrieg war Webern u. a. Leiter des Wiener Schubertbundes (bis 1922), der Wiener Arbeiter-Sinfoniekonzerte sowie Chormeister des Wiener Arbeiter-Singvereins. 1927 wurde er ständiger Dirigent beim österreichischen Rundfunk. 1924 und 1932 erhielt Anton Webern den Musikpreis der Stadt Wien. Er gab Gastspiele in der Schweiz, in England, Spanien und Deutschland.

Weberns Verhältnis zur NS-Ideologie und zum NS-Staat ist in der Forschung umstritten. „Ungeachtet partieller Übereinstimmungen mit dem ‚Nationalsozialismus‘ hatte (und wollte) er nach dem „Anschluss Österreichs“ 1938 als ‚Kulturbolschewist‘ keine Chance im offiziellen Musikleben“, schreibt das Lexikon Komponisten der Gegenwart. Ab 1939 stellte er für die Universal Edition Klavierauszüge her und zog sich zunehmend aus der Öffentlichkeit zurück. Eine authentische Sicht auf den Komponisten in dieser Zeit geben die Erinnerungen Karl Amadeus Hartmanns, der Webern im November 1942 in Maria Enzersdorf bei Wien besuchte, um Unterricht zu nehmen.[2]
Am 15. September 1945 wurde Anton Webern in Mittersill bei Zell am See von einem US-amerikanischen Soldaten versehentlich erschossen. Während einer Razzia im Haus Weberns – sein Schwiegersohn wurde des Schwarzmarkthandels verdächtigt – trat Webern vor die Tür, um eine Zigarre zu rauchen, und stieß mit einem der Soldaten, die das Haus umstellt hatten, zusammen, woraufhin die tödlichen Schüsse fielen.

1990 wurde der Asteroid (4529) Webern nach ihm benannt,[3]
und im Jahr 1998 wurde in Wien Landstraße (3. Bezirk) ihm zu Ehren der Anton-von-Webern-Platz, vor der Universität für Musik und darstellende Kunst Wien, nach ihm benannt.

Bereits aus der „Klagenfurter Periode“ (1899) sind zwei Stücke für Cello und Klavier aus Weberns Hand bekannt. Weberns frühe, zu Lebzeiten nicht aufgeführte Stücke (Im Sommerwind, 1904; Langsamer Satz, 1905) stehen noch deutlich in der Tradition der Spätromantik. Darauf folgte, beginnend 1908/1909 mit den Liedern nach Stefan George, eine lange atonale Phase, die Weberns Ruf als ein Vertreter des musikalischen Expressionismus begründete. Bis 1914 entstanden Stücke von aphoristischer Kürze. 1924/1925 wendete Webern dann erstmals Schönbergs Zwölftontechnik an. Während Schönberg und Alban Berg diese Technik für große Formen anwendeten, vollendete sich die Kunst Weberns in der kleinen, hochkonzentrierten Form und er begann „seine Diamanten zu schleifen, seine blitzenden Diamanten, von deren Minen er eine so vollkommene Kenntnis hatte“ – wie Igor Strawinski es einmal ausdrückte.

Von da an konzentrierte sich Webern auf die Organisation der Struktur – neben der Ordnung der Tonhöhen auch die der Dauern und der Dynamik. Eine konsequente Durchformung der Parameter (so der Ausdruck für die elementaren Ebenen der Musik) nahmen nach dem Zweiten Weltkrieg die Komponisten der Darmstädter Schule vor, am prominentesten Pierre Boulez und Karlheinz Stockhausen, die Weberns Verfahren allererst erkannten und zur seriellen Musik ausarbeiteten.

Zu seinen Lebzeiten veröffentlichte Werke:

Der größte Teil des Nachlasses von Anton Webern befindet sich heute in der Paul-Sacher-Stiftung in Basel.

Julius Bittner (1926–1933) |
Anton Webern (1933–1938) |
Zwangsauflösung (1938–1945) |
Anton Webern (1945) |
Hans Erich Apostel (1946–1949) |
Friedrich Wildgans (1949–1961) |
Josef Polnauer (1961–1968) |
Friedrich Cerha (1968–1975) |
Peter Keuschnig (1975–1983) |
Dieter Kaufmann (1983–1988) |
Wilhelm Zobl (1988–1991) |
Lothar Knessl (1992–2000) |
Wolfgang Liebhart (2000–2004) |
Maria Skodak (2004–2008) |
Bruno Strobl (seit 2008)
