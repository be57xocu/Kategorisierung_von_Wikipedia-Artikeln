Rudolf Hagelstange (* 14. Januar 1912 in Nordhausen; † 5. August 1984 in Hanau) war ein deutscher Schriftsteller.

Hagelstange besuchte das humanistische Gymnasium in Nordhausen, studierte von 1931 bis 1933 Philologie und Leibesübungen in Berlin und unternahm 1933 und 1936 zwei längere Reisen auf den Balkan. Von 1936 bis 1938 volontierte er bei der Nordhäuser Zeitung, wo er ab 1939 als Feuilletonredakteur fungierte. 1938 wurde er Mitteldeutscher Meister im Stabhochsprung.

1939 besuchte er die Reichspresseschule. Während des Zweiten Weltkrieges, der ihn 1940 nach Frankreich führte, war er bei der Nachrichtentruppe, 1944 Kriegsberichterstatter in Frankreich und Italien. Als er aus amerikanischer Kriegsgefangenschaft zurückkam, veröffentlichte er seinen ersten Gedichtband mit 35 Sonetten: Venezianisches Credo. Nach einem Jahr in Nordhausen übersiedelte er nach Westfalen (Hemer im Sauerland) und 1948 dann an den Bodensee (Wohnsitz in Unteruhldingen). 1968 zog er ins Elsass. Von 1971 bis zu seinem Tod im Jahr 1984 lebte er in Erbach (Odenwald) und ist dort auch beerdigt worden.[1]
Sein Werk ist breit gefächert; er war als Herausgeber, Lyriker, Romancier und Essayist tätig.

Daneben vertrat er auf vielen Auslandsreisen die deutsche Nachkriegsliteratur. Zusammen mit Hans Erich Nossack war Hagelstange 1961 als Repräsentant der bundesdeutschen Schriftsteller auf der Feier zum 100. Geburtstag von Rabindranath Tagore in Neu-Delhi. Als Chronist der Olympischen Spiele fuhr er 1960 nach Rom und 1964 nach Tokio. In den 1980er Jahren stand er dem Bundesverband Deutscher Autoren vor. Seit 1992 trägt die Stadtbibliothek in Nordhausen seinen Namen.

In Hanau, wo er bei einem Aufenthalt im Gartenhaus einer Freundin starb, wurde ein Weg nach ihm benannt.[2]