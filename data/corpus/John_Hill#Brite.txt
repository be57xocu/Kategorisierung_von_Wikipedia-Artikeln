John Hill (* um 1716 vermutlich in Peterborough, Northamptonshire; † 21. November 1775 in London) war ein englischer Apotheker, Arzt, Botaniker und Schriftsteller. Sein offizielles botanisches Autorenkürzel lautet „Hill“.

Hills erste Veröffentlichung war eine Übersetzung von Theophrasts Geschichte der Steine (1746). Seither schrieb er unermüdlich, seine Veröffentlichungen umfassen über 70 Werke. Er war Herausgeber des British Magazine (1746–1750) und schrieb zwischen 1751 und 1753 unter dem Pseudonym The Inspector eine tägliche Kolumne für die Zeitung London Advertiser and Literary Gazette, womit Hill als weltweit erster Zeitungskolumnist gilt. Neben wissenschaftlichen Werken schrieb er auch Romane und Schauspiele und wirkte in großem Umfang am Supplement von Ephraim Chambers' Cyclopaedia mit. Im Jahr 1761 wurde er zum Mitglied der Leopoldina gewählt.

Nach ihm wurde die Pflanzengattung Hillia Jacq. benannt.
