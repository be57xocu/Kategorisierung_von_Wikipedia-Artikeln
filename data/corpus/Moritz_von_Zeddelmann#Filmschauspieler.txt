Moritz von Zeddelmann (* 11. Mai 1988 in Ostercappeln) ist ein deutscher Schauspieler.

Moritz von Zeddelmann wuchs in Niedersachsen auf, seine ersten schauspielerischen Erfahrungen machte er am Theater Osnabrück und in freien Theatergruppen. Von 2006 bis 2009 absolvierte er ein Schauspielstudium, welches er 2009 mit Diplom abschloss.[1]
Schon während seiner Ausbildung spielte er an verschiedenen Theatern in Hamburg, so zum Beispiel 2008 an der Komödie Winterhuder Fährhaus (Amphitryon 38 von Jean Giraudoux) und am Thalia Theater (2009 in Radio Rhapsodie von Anja Hilling unter der Regie von Andreas Kriegenburg). Nach dem Abschluss ging er mit Friedrich Schillers Schauspiel Die Räuber für fast ein Jahr auf Tournee in Deutschland und der Schweiz.

Seit 2008 wirkt Moritz von Zeddelmann in verschiedenen Film- und Fernsehproduktionen mit und gewann den Deutschen Jugendvideopreis in Ludwigsburg für den Kurzfilm Blind Date.[2]
2011 übernahm er die Hauptrolle des Damon Miller im englischen Spielfilm 51 Degrees North, dessen Soundtrack von Queen und Brian May geschrieben wurde.[3]
Seit 2014 verkörpert Moritz von Zeddelmann die Rolle des Robbie Schulz in der ARD-Vorabendserie Unter Gaunern.[4]
Moritz von Zeddelmann lebt in Hamburg.
