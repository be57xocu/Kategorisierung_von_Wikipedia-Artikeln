Peter Mark Roget (* 18. Januar 1779 in London; † 12. September 1869 in West Malvern, Worcestershire) war ein englischer Arzt und  Lexikograph. 

Von 1834 bis 1837 war er Fullerian Professor of Physiology an der Royal Institution.

Am 29. April 1852 erschien sein Werk Thesaurus of english words and phrases [1] das als Roget’s Thesaurus für die englische Sprache rasch Bedeutung erlangte. Die Erstauflage umfasste 15.000 Wörter. Der Wortschatz war in sechs Klassen eingeteilt:

Roget erfand 1815 den log-log Rechenschieber. Weiters entwickelte er um das Jahr 1835 die nach ihm benannte Rogetsche Spirale, einen Form von Selbstunterbrecherkontakt.[2]
Nach Roget sind in der Antarktis das Kap Roget im Viktorialand und die Roget Rocks, Felsklippen vor der Küste der Antarktischen Halbinsel, benannt.
