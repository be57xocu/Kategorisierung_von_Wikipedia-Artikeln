Alice Perrers (* etwa 1325, sicher vor 1351; † nach 1377) war verheiratet mit William of Windsor (William de Wyndesore), Lord Lieutenant of Ireland.

Sie war Hofdame bei Philippa von Hainault, der Frau von Eduard III., und nach Philippas Tod in den 1360er Jahren die Geliebte von Eduard III., von dem sie drei uneheliche Kinder hatte, John de Southeray, Joan und Jane. Man geht davon aus, dass sie mit Älterwerden und Erkrankung von Edward III. großen Einfluss hatte und diesen unter anderem zugunsten von John of Gaunt, 1. Duke of Lancaster, zumindest anfangs, einsetzte. Nachgesagt wird ihr erhebliche Korruption.

Außerdem galt lange Zeit Nicholas Lytlington als Sohn der beiden. Dieser wurde jedoch bereits 1333 Mönch (also zu einem Zeitpunkt als Edward III. erst 21 Jahre alt war). Lytlington wurde 1352 Prior von Westminster und war von 1362 bis 1386 Abt von Westminster.[1]
Bekanntheit als geschichtliche Person erlangte sie durch den 1997 veröffentlichten historischen Roman Das Lächeln der Fortuna von Rebecca Gablé.
