Olivia Mary Manning, verheiratet als Olivia Smith (* 2. März 1908 in Portsmouth; † 23. Juli 1980 in Ryde, Isle of Wight), war eine britische Schriftstellerin.

Olivia Manning wurde 1908 in Portsmouth geboren und wuchs in Nordirland auf. Nach einer Ausbildung als Malerin an der Portsmouth School of Art zog sie nach London und war als Schriftstellerin tätig. Ihr erster Roman unter eigenem Namen, The Wind Changes, erschien 1937. Während des Zweiten Weltkriegs flüchtete sie mit ihrem Ehemann nach Griechenland und danach nach Jerusalem. Ihre dortige Zeit bis 1945 und ihre Erfahrungen im britischen Protektoratsgebiet Palästina verarbeitete sie in ihrem ersten großen Erfolg School for Love von 1951 (dt. erst 2013 als Abschied von der Unschuld).

Mannings bekanntestes Werk ist Fortunes of War, das aus zwei Trilogien besteht (The Balkan Trilogy und The Levant Trilogy – deutsch: Im Fluss der Zeit (mit den Teilen Der Reichtum und Stadt ohne Moral), Wie Blätter im Wind und Auf neuen Wegen); es zeichnet die Kriegserfahrungen einer Gruppe von Engländern auf, die während des Zweiten Weltkrieges zwischen Rumänien, Griechenland, Ägypten und Palästina hin- und herwandern. 

Verfilmt wurden Teile des Inhalts unter dem Titel Auf Wiedersehen in Kairo mit Emma Thompson und Kenneth Branagh in den Hauptrollen.
