Lilly Stepanek (* 18. Juli 1912 in Wien; † 24. Mai 2004 in Baden bei Wien) war eine österreichische Schauspielerin.

Stepanek war etwa 50 Jahre Mitglied des Wiener Burgtheaters. Dort spielte sie in klassischen und modernen Stücken Charakterrollen und komische Rollen, u. a. unter den Regisseuren Adolf Dresen, Berthold Viertel und Leopold Lindtberg. Lilly Stepanek, ab 28. März 1969 Kammerschauspielerin,[1] starb nach langer Krankheit in dem vom Verein „Künstler helfen Künstlern“ geführten Heim in Baden bei Wien.
