Sir Frederick Arthur Montague Browning, KBE, CB, DSO (* 20. Dezember 1896 in Kensington; † 14. März 1965 in Cornwall), der auch „Boy“ Browning genannt wurde, war ein britischer Lieutenant-General (Generalleutnant). Er erlangte als Kommandeur der 1. Alliierten Luftlandearmee während der Operation Market Garden Berühmtheit. 

Browning war ab 1932 mit der Autorin Daphne du Maurier verheiratet.

Brownings militärische Karriere begann im Ersten Weltkrieg, wo er Winston Churchill traf, der ihm später das Kommando über die britische 1. Luftlandedivision übergab. Er wurde diverse Male versetzt, bis er 1935 Kommandeur des 2. Bataillons der Grenadier Guards wurde. In dieser Position blieb er bis zum Beginn des Zweiten Weltkrieges, als er Kommandant der Small arms School wurde. 1940 erhielt er den Befehl über die 24. Guards Brigade.

Im Jahr 1941 wurde Browning von Churchill zum Kommandeur 1. Britischen Luftlandedivision ernannt, der er bis 1943 blieb.

Er wurde 1943 Kommandierender General des britischen 1. Luftlandekorps, das 1944 der neu aufgestellten 1. Alliierten Luftlandearmee zugeteilt wurde. Der amerikanische Lieutenant-General (Generalleutnant) Lewis H. Brereton bekam den Oberbefehl über diese Luftlandearmee und Browning wurde sein Stellvertreter. Das Kommando über das 1. Luftlandekorps behielt jedoch Browning selbst.

Browning kommandierte die Luftlandestreitkräfte der 1. Alliierten Luftlandearmee während der Operation Market Garden, wo er mit seinem Stab nahe Nimwegen landete. Die Verstreuung der Truppen erschwerte jedoch die Befehlsgebung.

Obwohl Browning von Bernard Montgomery nicht für den Fehlschlag der Operation Market Garden verantwortlich gemacht wurde, musste er auf weitere Beförderungen verzichten und blieb Lieutenant General. Er wurde nach Südostasien versetzt und führte sein letztes größeres Kommando als Generalsekretär im Kriegsministerium.

1928 nahm Browning als Teil des Bob-Teams des Vereinigten Königreiches an den Olympischen Winterspielen in St. Moritz teil. Er belegte mit seinen Kollegen Rang 10.

In dem Film Die Brücke von Arnheim wurde Browning von Dirk Bogarde gespielt.
