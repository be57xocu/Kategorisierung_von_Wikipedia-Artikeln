Jean Dalibard (* 8. Dezember 1958) ist ein französischer Physiker.

Dalibard arbeitete gemeinsam mit Alain Aspect und Gérard Roger am Laboratoire Charles Fabry in Orsay. 1982 wiesen sie zusammen in einem experimentellen Test des EPR-Effekts nach, dass die Bellschen Ungleichungen verletzt werden.

1986 promovierte er unter der Anleitung von Claude Cohen-Tannoudji über die Manipulation von Atomen durch Laserstrahlen. Dalibard arbeitet am CNRS an der Untersuchung von Bose-Einstein-Kondensaten und ist gleichzeitig Professor an der École polytechnique (Laboratoire Kastler-Brossel). 

Neben der Entwicklung von Methoden der Laserkühlung und von Atomfallen befasst er sich mit Bose-Einstein-Kondensaten (BEC), zum Beispiel dem Verhalten von Wirbeln in rotierenden BEC. Außerdem befasst er sich mit niedrigdimensionalen Vielteilchensystemen wie dem Berezinski-Kosterlitz-Thouless Übergang bei atomaren Gasen in zwei Dimensionen.

Er ist Mitglied der Akademie der Wissenschaften in Frankreich und seit 2011 ordentliches Mitglied der Academia Europaea. 1992 erhielt er den Prix Mergier-Bourdeix. 2012 wurde er mit dem Davisson-Germer-Preis ausgezeichnet, 2000 erhielt er den Prix Jean Ricard, 2010 den Prix des trois physiciens und 2009 die Blaise-Pascal-Medaille. 2012 erhielt er den Max Born Award.

2010 war er Gastwissenschaftler am Trinity College in Cambridge. 1991 war er am NIST bei William D. Phillips.
