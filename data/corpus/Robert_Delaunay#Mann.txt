Robert Victor Félix Delaunay (* 12. April 1885 in Paris; † 25. Oktober 1941 in Montpellier) war ein französischer Maler der Avantgarde, er verfasste auch kunsttheoretische Schriften. Nach einem Beginn als autodidaktischer Maler im Stil des Neoimpressionismus gilt er als Hauptvertreter des orphischen Kubismus, auch Orphismus genannt, der 1912 als Begriff von Guillaume Apollinaire für Delaunays Fensterbilder, Fenêtres, geprägt wurde. In den 1930er Jahren setzte er seine Arbeit nach einer langen Unterbrechung an den abstrakten Kreisformen in der Serie Rythmes, die die Themen der Scheibe von 1913 und der Kreisform erneut aufgreift, fort. Seine Ehefrau, Sonia Delaunay, war ebenfalls Malerin sowie Designerin und arbeitete mit ihm in künstlerischem Austausch zusammen.

Robert Delaunay war der Sohn des Geschäftsmannes Georges Delaunay und der Gräfin Berthe-Félice de Rose. Die Eltern ließen sich 1889 scheiden und Robert wurde von der älteren Schwester seiner Mutter, Marie, und ihrem Ehemann Charles Damour auf deren Landsitz nahe Bourges aufgenommen und sah seine Eltern kaum.[1]
Delaunay verließ aufgrund mangelnder Leistungen die Oberschule und absolvierte von 1902 bis 1904 eine Lehre in Bühnen- und Dekorationsmalerei bei der Bühnenbildwerkstatt Ronsin in Belleville. 1903 begann er in der Bretagne zu malen, hatte eine Verbindung zur Gruppe von Pont-Aven, woraufhin er neo-impressionistische Meeresansichten „vor dem Motiv“ zu malen begann. Ab 1904 fanden seine Gemälde Aufnahme in Ausstellungen: in diesem Jahr und 1906 im Salon d’Automne und von 1904 bis 1914 im Salon des Indépendants. Im Salon d’Automne besichtigte er 1905 den Saal der Fauves, deren Stil ihn zu farbkräftigen Bildern inspirierte. Zwischen 1905 und 1907 schloss er Freundschaft mit Jean Metzinger und Henri Rousseau und studierte die Farbtheorien von Michel-Eugène Chevreul.[2] Zudem setzte er sich mit der Arbeit Paul Cézannes auseinander. Ab 1907 diente er beim Militär als Bibliothekar in Laon und wurde 1908 wegen „Herzfunktionsstörungen“ untauglich geschrieben. Er kehrte nach Paris zurück und sah in der Galerie Kahnweiler kubistische Arbeiten von Georges Braque und Pablo Picasso, worauf er sich etwa ab 1909 dem Kubismus zuwandte.[3][4]
Die bekannten Fenêtre (Fenster)-Bilder, die als Hauptmotiv den durch ein Fenster gesehenen, von 1887 bis 1889 erbauten Eiffelturm aufweisen, entstanden seit 1909 und wurden in den 1920er Jahren fortgesetzt.

Le rocher devant la mer, um 1904

Marché Breton, 1905

L’homme à la tulipe (Portrait de Jean Metzinger), 1906

Nature morte au vase de fleurs, um 1907

La tour aux rideaux (Durchblick auf den Eiffelturm), 1910

1908 lernte Delaunay über den Galeristen Wilhelm Uhde auch dessen Ehefrau − die Künstlerin Sonia Uhde-Terk – kennen und heiratete sie nach deren Scheidung von Uhde 1910. Die gemeinsame Pariser Wohnung 3 rue des Grands Augustins wurde zum Treffpunkt zahlreicher Künstler. Im Januar 1911 wurde ihr Sohn, der spätere Jazz-Autor und Produzent Charles Delaunay, geboren.

In diesem Jahr lernte Delaunay über Elisabeth Epstein, die seit 1904 in Paris lebte, Wassily Kandinsky kennen und schloss sich im selben Jahr den Malern um die Redaktionsgemeinschaft des Blauen Reiters an. Deren erste Ausstellung fand im Dezember 1911 in der Modernen Galerie Heinrich Thannhauser in München statt; Delaunay war der erfolgreichste Künstler, der drei von vier ausgestellten Bildern verkaufen konnte. Darunter war ein Gemälde aus der Eiffelturmserie, das Bernhard Koehler erwarb. Es gilt als verschollen. Insgesamt fanden neun der ausgestellten Bilder in der Galerie ihre Käufer. Delaunay vermittelte auch die Bilder von Henri Rousseau für die Ausstellung, einige befanden sich in seinem Besitz.[5] Es folgten Ausstellungen in der Sturm-Galerie in Berlin, im Gereonsklub in Köln sowie in der Künstlervereinigung Karo-Bube in Moskau.[3]
→ Hauptartikel: Orphismus

Ab 1912 wandte sich Delaunay der reinen Farbmalerei zu, und er schuf einige der durch Kreisformen geprägten Synchromie-Werke. Im Februar und März des Jahres hatte er seine erste Einzelausstellung in der Pariser Galerie Barbazanges, auf der auch Werke von Marie Laurencin gezeigt wurden. Am 11. April besuchte Paul Klee Delaunay in dessen Pariser Atelier. Im Dezember des Jahres erhielt Klee Delaunays Aufsatz Über das Licht (La Lumière) zur Übersetzung für Herwarth Waldens Kunstzeitschrift Der Sturm in Berlin, den Franz Marc für ihn aus Paris mitgebracht hatte, und der im Januar des folgenden Jahres in der Kunstzeitschrift erschien.[6] Klees Werk wurde in der Folge durch Delaunays Fensterbilder geprägt. Gegen Ende des Jahres war der Schriftsteller Guillaume Apollinaire zu Besuch bei Delaunays und kreierte den Begriff des „Orphismus“ angesichts der Fensterbilder des Künstlers. Er bezeichnet eine Gruppe von Werken, die zum Ungegenständlichen, Abstrakten tendieren.[7] Delaunay selbst lehnte den Begriff „Orphismus“ aufgrund der bei Apollinaire mitschwingenden lyrischen Komponente ab, zumal er seine Malerei vielmehr als „Cubisme écartelé (zerteilter Kubismus)“ bezeichnet wissen wollte.[8]
Zwei seiner Gemälde wurden in der Armory Show Anfang 1913 in New York gezeigt, ein drittes, La Ville de Paris, wurde wegen Übergröße von Arthur B. Davies abgelehnt. Der Maler Samuel Halpert forderte im Auftrag von Delaunay die Abhängung aller seiner Bilder, doch es verblieben Les Fenêtres sur la ville (Window on the City, No. 4) und Route de Laon. Sie nahmen aber nicht mehr an der Fortsetzung der Armory Show in Chicago teil.[9] Im September 1913 nahmen Delaunay und seine Frau an der Ausstellung des Ersten Deutschen Herbstsalons in Berlin teil. Delaunay war mit 21 Gemälden vertreten, seine Frau stellte 25 Werke aus, hauptsächlich bemalte Bucheinbände.[10]
La Ville de Paris, 1910/12

Tour Eiffel, 1911

Les trois grâces, 1912

Les Fenêtres sur la ville, 1910/11, gezeigt auf der Armory Show

Fensterbild (Les Fenêtres simultanées sur la ville), 1912

Disque simultané, 1912/13

L’Équipe de Cardiff, 1913, gezeigt im Ersten Deutschen Herbstsalon

Bei Kriegsausbruch 1914 hielt sich das Ehepaar Delaunay in Spanien auf und beschloss, nicht nach Frankreich zurückzukehren. Ihr Wohnsitz war in Hondarribia, Spanien, von 1915 bis 1917 in Vila do Conde, Portugal.[11] Zunächst einberufen, dann zum Fahnenflüchtigen erklärt, wurde Delaunay am 3. Juni 1916 im französischen Konsulat im spanischen Vigo kriegsuntauglich geschrieben. 1917 lebte das Paar in Madrid und Barcelona. Sie freundeten sich mit Sergei Djagilev, Léonide Massine, Diego Rivera und Igor Stravinski an. Im folgenden Jahr entwarf Delaunay das Dekor für das Ballett Cléopâtre für die von Djagilev geleiteten Ballets Russes. Seine Frau war für die Kostüme zuständig. 1919 hatte das Ehepaar eine gemeinsame Ausstellung in der „Asociaciòn de Artistas Vascos“ in Bilbao. Der französische Modeschöpfer Paul Poiret verweigerte 1920 eine Partnerschaft mit Sonia Delaunay und gab als einen Grund an, sie sei mit einem Deserteur verheiratet.[2][12] In dieser Zeit entstanden Motive von der iberischen Halbinsel und die einzigen Aktgemälde des Künstlers wie Femme nue lisant.

Portugiesische Frau, 1915 (Studie)

Lesende/Femme nue lisant, 1915

Lesende/Femme nue lisant, 1920

Nach dem Ersten Weltkrieg kehrte das Ehepaar im Jahr 1921 nach Paris zurück und bezog eine Wohnung am Boulevard Malesherbes 19. Delaunay knüpfte Kontakte zu dem noch jungen Kreis der Dadaisten und späteren Surrealisten wie André Breton, Louis Aragon und dem Dada-Dichter Tristan Tzara, die er porträtierte. 1922 wurden in der Galerie Paul Guillaume seine Werke ausgestellt, und er begann seine zweite Serie mit Darstellungen des Eiffelturms. 1924 folgten seine Läuferbilder, und 1925 schuf er Fresken für das Palais de l’Ambassade de France zur Exposition internationale des arts décoratifs in Paris. Ab 1929 hielt er sich zusammen mit Hans Arp, Sophie Taeuber-Arp und Tristan Tzara in der Bretagne auf.[13]
Ab dem Anfang der 1930er Jahre setzte er seine Arbeit nach einer langen Unterbrechung an den abstrakten Kreisformen fort. Es entstand die Serie Rythmes, die die Themen der Scheibe von 1913 und der Kreisform erneut aufgreift.[14] 1931 schloss er sich zusammen mit seiner Frau der Gruppe Abstraction-Création an. Für die Pariser Weltausstellung 1937 fertigte er Dekorationen für den Palais de l’Aéronautique und den Pavillon des Chemins de Fer an. Für den Pavillon malte er ein Leinwandbild mit dem großen Format 10 × 15 m, das den Titel Luft, Eisen und Wasser trägt und Bestandteil der Sammlung des Centre Georges Pompidou ist. Sein letztes Werk war 1938 die Anfertigung von Dekorationen für die Skulpturenhalle des Salon des Tuileries.[15][16]
Im Jahr 1940 zog das Ehepaar Delaunay aufgrund der deutschen Besetzung Frankreichs in die Auvergne und im Winter nach Mougins in Südfrankreich. Robert Delaunay starb im folgenden Jahr in Montpellier an einem Krebsleiden. 1952 wurde sein Grab zum Friedhof von Gambais bei Paris verlegt, wo die Delaunays einen Bauernhof besaßen; dort wurde 1979 Sonia Delaunay an der Seite ihres Mannes bestattet.[17]
Le Poète Philippe Soupault, 1922

Portrait Tristan Tzara, 1923

Les coureurs, 1924

La Tour Eiffel, 1928

Rythme, Joie de vivre, 1930

Rythme, um 1932

Relief; Rhythms, 1932

Rythme I, 1934

Vor allem Delaunays Frau Sonia und der Amerikaner Patrick Henry Bruce, ein Schüler von Henri Matisse, waren in ihren Arbeiten vom Orphismus beeinflusst. Zudem sollen Arbeiten von Marc Chagall, Raymond Duchamp-Villon sowie der Section d’Or vom Orphismus inspiriert worden sein.[18]
Hinzu kam, dass Delaunays Werk für die deutschen expressionistischen Maler Franz Marc und August Macke von zentraler Bedeutung war. Sie begegneten sich nur zweimal persönlich; im Oktober 1912 suchten sie Delaunay in Paris auf, und im September 1913 stellten sie gemeinsam im Ersten Deutschen Herbstsalon aus. Die farbigen Facettierungen der Fensterbilder sowie die Formes Circulaires, Farbkreise in geometrisch-flächigen Strukturen angeordnet, inspirierten Marc und Macke zu abstrakten Kompositionen.[19] Ebenfalls geprägt durch die Fensterbilder wurde Paul Klee.

Unter dem Titel Pariser Visionen zeigte das Berliner Museum Deutsche Guggenheim 1997 zu seiner Eröffnung Robert Delaunays Serien. Im Jahr 1999 folgte mit Leihgaben des Pariser Centre Georges Pompidou, das ein umfangreiches Werkensemble durch Schenkungen von Sonia und Charles Delaunay besitzt, eine Würdigung des Künstlerehepaars mit einer gemeinsamen Ausstellung ihrer Werke, die von der Hamburger Kunsthalle unter dem Titel Robert Delaunay – Sonia Delaunay: Das Centre Pompidou zu Gast in Hamburg gezeigt wurde.[20][21]
Die Stadt Paris benannte eine Straße, die Rue Robert et Sonia Delaunay, nach dem Ehepaar, und in Versailles wurde die Rue Delaunay nach dem Künstler benannt.

postum

Delaunay, Robert. Du Cubisme à l’ Art Abstrait. Les Cahiers inédits de Robert Delaunay.
S.E.V.P.E.N., Paris 1957.
Erste und einzige Ausgabe des Catalogue raisonné und bis dato unveröffentlichten Schriften Delaunays in vier Teilen: Robert Delaunay vu Robert Delaunay; Notes historiques sur la peinture; Les Entretiens de Robert Delaunay und L’Art et l’état: Projet d’un musée inobjectif. Einleitung von Pierre Francastel. Das Werkverzeichnis verzeichnet 758 Werke, eine Liste der verlorenen Werke, chronologische Liste der Ausstellungen (1904–1957) samt ausgestellter Werke und Bibliographie. 
