Ulrich Knellwolf (* 17. August 1942 in Niederbipp, Kanton Bern) ist ein Schweizer Pfarrer und Kriminalschriftsteller.

Ulrich Knellwolf wuchs in Zürich und Olten auf, besuchte die Kantonsschule in Solothurn und studierte dann Evangelische Theologie an der Universität Basel, der Rheinischen Friedrich-Wilhelms-Universität in Bonn und der Universität Zürich. 1990 wurde er mit einer Arbeit über Jeremias Gotthelfs erzählende Theologie (Gleichnis und allgemeines Priestertum. Zum Verhältnis von Predigtamt und erzählendem Werk bei Jeremias Gotthelf) in Zürich zum Dr. theol. promoviert.

Seit 1969 wirkte er als reformierter Pfarrer, zunächst in Urnäsch und Zollikon und schliesslich von 1984 bis 1996 an der Predigerkirche in Zürich. Danach wurde er Mitarbeiter der Stiftung Diakoniewerk Neumünster Zollikerberg. Er lebt in Zollikon. 2006/2007 sprach er das „Wort zum Sonntag“ im Schweizer Fernsehen (SF).

Knellwolfs Bücher wurden in verschiedene Sprachen übersetzt.
