Gerhard Lojen  (* 28. Dezember 1935 in Graz; † 17. Dezember 2005 ebenda) war ein österreichischer Architekt, Maler und Pädagoge.

Lojen studierte an der TU Graz von 1954 bis 1962 Architektur, Zeichnen und Malen bei Kurt Weber. Von 1958 bis 1977 war er Mitglied der Grazer Sezession und 1977 Mitbegründer der Künstlergruppe 77 in Graz. Studienreisen führten ihn u. a. nach Italien, Griechenland, Deutschland, Tunesien, Israel, in die Schweiz und in den Iran. Ab 1987 war er bis 2000 Leiter der Meisterschule für Malerei an der Grazer Ortweinschule. Sei Atelier befand sich in der Plüddemanngasse.[1] Am 16. Dezember 2005 starb er nach kurzer, schwerer Krankheit.

Lojen war Mitglied der K.A.T.V. Norica Graz im ÖKV.
