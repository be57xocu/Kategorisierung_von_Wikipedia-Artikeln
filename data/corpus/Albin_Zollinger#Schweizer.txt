Albin Zollinger (* 24. Januar 1895 in Zürich; † 7. November 1941 in Zürich) war ein Schweizer Schriftsteller.

Albin Zollinger wurde 1895 als Sohn eines Feinmechanikers geboren und wuchs in Rüti ZH und in Argentinien auf, wo sich seine Eltern vergeblich eine neue Existenz aufbauen wollten. Er besuchte das Küsnachter Lehrerseminar und erhielt nach vielen Stellenwechseln in Oerlikon eine feste Anstellung, die er bis zu seinem Tod behielt. 1921 erschien sein erster Roman. 

Alles, was Zollinger schrieb − Romane, Erzählungen, Gedichte, Aufsätze, Artikel, Rezensionen, Briefe – entstand neben seiner Arbeit als Lehrer, dem Aktivdienst, öffentlichen Engagements im Schweizerischen Schriftstellerverband, seiner Arbeit als verantwortlicher Redaktor zunächst bei der Berner Kulturzeitschrift «Die Zeit», dann beim Wochenblatt «Die Nation» und trotz familiärer Krisen und Depressionen. Seine Ehe wurde nach wenigen Jahren geschieden.

Bevorzugt schrieb er in Zürcher Kaffeehäusern, wohin er jeweils von Oerlikon nach der Schule mit dem Tram fuhr. Fast legendär war in den 30er Jahren sein Marmortischchen im Café Terrasse. Dort war er oft in Gesellschaft von weiteren Zürcher Literaten und Kulturschaffenden anzutreffen, etwa dem Literaturprofessor Fritz Ernst, dem Literaturkritiker Bernhard Diebold, seinem Freund Traugott Vogel oder Rudolf Jakob Humm. Auch mit Ludwig Hohl war er befreundet.

Drei Wochen vor seinem Tod im Alter von erst 46 Jahren begegnete Zollinger auf dem Pfannenstiel dem jungen Schriftsteller Max Frisch, der diese Begegnung in seinem Tagebuch 1946–1949  festhielt.

Zollinger ist in einem Ehrengrab auf dem Friedhof Nordheim bestattet. Sein Nachlass wird von der Zentralbibliothek Zürich verwaltet. In Oerlikon gibt es seit 1980 einen Albin-Zollinger-Platz.
