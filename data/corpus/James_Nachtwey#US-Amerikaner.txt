James Nachtwey (* 14. März 1948 in Syracuse, N.Y.) ist ein US-amerikanischer Dokumentarfotograf, Kriegsberichterstatter und Fotojournalist.

Nachtwey zählt zu den bedeutendsten Vertretern der zeitgenössischen Dokumentarfotografie, insbesondere der Kriegsfotografie.

Nachtwey wurde in Syracuse, New York, geboren. Er wuchs auf in Leominster, Massachusetts, USA und studierte von 1966 bis 1970 Kunstgeschichte und Politikwissenschaft am Dartmouth College. Nach dieser Zeit arbeitete er auf Handelsschiffen und als Lastwagenfahrer; etwa 1972 entschloss er sich – unter Einfluss der Studentenbewegung und des Vietnamkriegs – Fotograf zu werden. Dieses Ziel verfolgte er über nahezu zehn Jahre, jedoch vorerst ohne Bilder zu publizieren; während dieser „Vorbereitungszeit“ brachte sich Nachtwey die Fotografie autodidaktisch durch das Studium von Fotobüchern und Dunkelkammerarbeit bei; zu den Fotografen, deren Arbeiten er studierte, gehören unter anderem Henri Cartier-Bresson, William Eugene Smith, Josef Koudelka und Don McCullin.

Zunächst lernte er den Alltag eines Reporters als Assistent eines Nachrichten-Redakteurs bei der US-Fernseh-Gesellschaft NBC in New York kennen. Ab 1976 fotografierte er selbst erstmals regionale Ereignisse in New Mexico; ab 1980 arbeitete er dann als freier Fotograf in New York City.

Seine erste Fotoreportage entstand 1981 in Nordirland, wo er die Unruhen in Belfast dokumentierte. Nachtwey arbeitet seit dieser Zeit fast ausschließlich in den jeweiligen Krisengebieten der Welt unter dem Motto des „(Augen-) Zeugen“:

„I have been a witness, and these pictures are my testimony. The events I have recorded should not be forgotten and must not be repeated.“

 Was ihn antreibt, ist nach eigenen Aussagen die Suche nach einem Antidot gegen Krieg und die Hoffnung darauf, etwas verändern zu können.

In den 1980er Jahren bereiste er dann als Dokumentarfotograf die lateinamerikanischen Länder (El Salvador, Nicaragua, Guatemala, Brasilien u. a.), den Nahen Osten (Libanon, Israel und die besetzten Gebiete (West Bank), Kurdistan u. a.) sowie Afrika (Ruanda, Somalia, Südafrika (1994) u. a.).

Nachtwey war in diesen Jahren mit Christiane Breustedt liiert, der damaligen Chefredakteurin der Magazin-Reihe GEO-International und heutigen Ehefrau von Peter-Matthias Gaede.

Anschließend folgten zahlreiche Reisen in das Gebiet der ehemaligen Sowjetunion und die Länder des Ostblocks wie Bosnien, Rumänien und Tschetschenien. Auch in Afghanistan entstand 1996 eine Fotodokumentation des Krieges. In Asien entstanden zunehmend Reportagen, die sich mit der Armut und den Lebensumständen der dortigen Bevölkerung beschäftigten (Indonesien, Thailand, Indien, Sri Lanka, Philippinen, Südkorea u. a.)

Seit 1984 steht Nachtwey unter Vertrag beim Time Magazine; zwischen 1980 und 1985 kooperierte er mit der New Yorker Agentur Black Star[1]. Zwischen 1986 und 2001 war James Nachtwey Mitglied der Fotoagentur Magnum; ab 2001 arbeitete er für die Agentur VII, zu deren Gründungsmitgliedern er gehört und die er 2011 wieder verließ. [2]
Nachtwey wohnt am Rand von Manhattan, in Sichtweite der Brooklyn Bridge. Als Augenzeuge der Terroranschläge auf das New Yorker World Trade Center am 11. September 2001 begleitete er die Einstürze der Türme und die Rettungsarbeiten am Ground Zero mit der Kamera.

Nachtwey wurde bei seiner Arbeit viele Male verwundet, so zum Beispiel am 10. Dezember 2003 im Irak bei einer Reportage des Time Magazine zum Thema „Person of the Year“.

Er steht unter Vertrag beim New Yorker und fotografiert inzwischen auch kommerzielle Aufträge wie die Filmfestspiele in Cannes[3].

James Nachtwey fotografiert nach der Maxime, möglichst dicht am Motiv zu sein; er ist „within a rifle butt's reach, sitting upright, his finger poised above the shutter“ (Chris Wright). Nachtwey vermeidet Teleobjektive und spektakuläre Aufnahmestandpunkte; stattdessen bevorzugt er Weitwinkelobjektive und begibt sich auf Augenhöhe der von ihm fotografierten Subjekte, er geht „nah ran“, ganz nach Robert Capas − eigentlich im übertragenen Sinn gemeinten − „goldener Regel“ des Fotojournalismus: „If your pictures aren't good enough, you’re not close enough.“

Nachtweys Methodik hat sich im Laufe seiner drei Jahrzehnte umfassenden Tätigkeit als Fotograf verändert; in den 80er Jahren versuchte er noch, archetypische Einzelbilder zu finden, die komplexe Geschehnisse in ein einziges Bild komprimieren konnten. Heute arbeitet er dagegen überwiegend in Serien, die auch den Kontext zeigen, und nur noch selten in alleinstehenden Einzelbildern.

Er fotografiert sowohl digital als auch mit analogem Film, bevorzugt jedoch Negativfilm. Digitale Fotos werden für ihn jedoch zunehmend notwendig, um knappe Termine für Auftragsarbeiten erfüllen zu können. Nachtwey betont jedoch die überragende Bedeutung des geschulten Auges gegenüber der Technik: „documentary photography and photojournalism are based on perception, not on technology.“

Nachtwey ist derzeit einer der erfolgreichsten Kriegsberichterstatter; seine Bilder werden in zahlreichen Zeitungen, Magazinen und Illustrierten veröffentlicht. Nachtwey fühlt sich der concerned photography verpflichtet und seine Bilder zeigen Mitgefühl für die Opfer von Kriegen und sozialer Ungerechtigkeit; er interessiert sich zunehmend für Armut und sozialkritische Themen. Er behauptet, „ein Journalist, der sich in ein Kriegsgebiet begibt, übt die höchste Form von Journalismus aus“. Nach seiner Auffassung hat die Presse die Möglichkeit, Ereignisse wirkungsvoll zu beeinflussen; Kriegsreportagen sollten daher nicht unterhalten oder nur informieren, sondern „Instinkte berühren und Politiker zum Handeln antreiben“.

Seine Arbeit ist jedoch auch umstritten. So kritisiert Richard B. Woodward in The Village Voice, Nachtwey bilde den Schrecken von Krieg und Tod als „ästhetisches Wunder“ ab; Nachtwey sei eben so anti-war wie der Modefotograf Herb Ritts anti-fashion sei und Nachtwey habe – im Gegensatz zu Robert Capa – kein Anliegen. Er wirft dem Fotografen vor, bei dem sensationslüsternen Publikum den „Appetit“ auf immer grauenvollere Bilder zu befriedigen und sich dabei in der Rolle des „Heiligen“ zu gefallen.[4]
Im Rahmen des Aufstandes in Syrien sorgte ein „wohlwollender“ Vogue-Artikel (erschienen im Februar 2011) über die Familie Assad mit dem Titel „Eine Rose in der Wüste“ für Irritationen, da der Artikel erst nach dem Beginn des Konfliktes, als schon Menschenleben und auch tote Journalisten zu beklagen waren, erschien. In dem Artikel wurde die Herrscherfamilie und vor allem Baschar al-Assads Frau Asma positiv dargestellt („glamourös, jung und sehr schick“, hieß es darin: „Eine schlanke, langbeinige Schönheit mit einem geschliffenen, analytischen Verstand.“). Der Artikel war mit Assad-Familienbildern James Nachtweys illustriert. Der Artikel verschwand wieder von der Website der Vogue.[5]
Nachtweys Werk ist vielfach publiziert, ausgestellt und ausgezeichnet worden.

Auswahl einiger Ausstellungen:

Der Autor und Regisseur Christian Frei drehte den Dokumentarfilm War Photographer über die Arbeit von James Nachtwey (Schweiz 2001, 96 Minuten, 35 mm, Dolby Stereo), der 2001 für den Academy Award in der Kategorie „Bester Dokumentarfilm“ (Best Documentary Feature) nominiert wurde.
