Charles Édouard Dutoit (* 7. Oktober 1936 in Lausanne) ist ein Schweizer Dirigent mit Wohnsitz in Kanada.

Dutoit erhielt in den Konservatorien von Lausanne und Genf eine umfassende musikalische Ausbildung in den Fächern Violine, Viola, Klavier, Schlagzeug, Komposition, Instrumentation, Theorie und Dirigieren. Hernach vervollständigte er seine Ausbildung in Siena bei Alceo Galliera und in Tanglewood (Massachusetts) bei Charles Münch.

Ab 1953 leitete er verschiedene Chöre und Amateurorchester. Dann lud ihn Herbert von Karajan nach Wien ein, um an der Wiener Staatsoper die Premiere von Manuel de Fallas Ballett El sombrero de tres picos zu dirigieren, was ihn ins internationale Rampenlicht rückte. 1964 wurde er zum Vize- und 1967 zum Chefdirigenten des Berner Symphonieorchesters ernannt, das er bis 1973 leitete. Von 1967 bis 1970 war er neben Rudolf Kempe auch Dirigent des Tonhalle-Orchesters Zürich. In den Jahren 1973 bis 1975 leitete er das Nationalorchester von Mexiko und von 1975 bis 1978 jenes von Göteborg.

1977 wurde Dutoit zum Musikdirektor des Symphonieorchesters von Montreal ernannt, eine Position, die er bis 2002 bekleidete. Unter seiner Leitung wurde dieses Orchester nach Meinung der Fachkritik zu einem der weltbesten Ensembles. Mit ihm unternahm Dutoit zahlreiche Auslandstourneen und machte Plattenaufnahmen, von denen viele mit internationalen Preisen ausgezeichnet wurden. 1983 bis 1986 war Dutoit erster Gastdirigent des Minnesota Orchestra. 1984 gab er sein Debüt an der Covent Garden Opera in London und 1987 an der Metropolitan Opera in New York. 1990 wurde er zum Chefdirigenten des Orchestre de Paris gewählt, und von 1991 bis 2001 war er auch musikalischer Leiter des Orchestre National de France. 1996 wurde er zum Chefdirigenten und 1998 zum Musikdirektor des Tokyo Symphony Orchestra NHK ernannt, mit welchem er Tourneen nach Europa, den Vereinigten Staaten, China und Südostasien unternahm. Daneben dirigierte er insgesamt über 150 Orchester in Amerika und Europa.

2003 begann Dutoit damit, im Teatro Colón in Buenos Aires einen Zyklus von Wagner-Opern zu dirigieren (Der Fliegende Holländer, Der Ring des Nibelungen). 2005 kehrte er ans Saratoga Performing Arts Center zurück und leitete dort das Philadelphia Orchestra, dem er von 2008 bis 2012 auch als Chefdirigent vorstand. Im September 2009 übernahm er in der Nachfolge Daniele Gattis als Principal Conductor und Artistic Director die Leitung des Royal Philharmonic Orchestra in London.

Da Dutoit ein besonderes Interesse zeigt für die Zusammenarbeit mit Orchestern von Musikstudenten, hat er häufig mit dem Orchestra of Curtis Institute in Philadelphia, dem Julliard Orchestra in New York, dem Civic Orchestra in Chicago und dem UBS Verbier Festival Orchester in der Schweiz zusammengearbeitet. Während drei Jahren war er der Direktor des Sapporo Pacific Music Festival in Japan. Zurzeit wirkt Dutoit auch als künstlerischer Leiter und Dirigent am Miyazaki International Music Festival in Japan und an der Canton International Summer Music Academy (CISMA) in China.

Ausserdem realisierte Dutoit für NHK-Television in Japan zehn Dokumentarfilme für eine Serie mit dem Titel Cities of Music, die zehn Musikzentren der Welt porträtiert.

Dutoit liegen besonders die französische Musik und die Klassiker des 20. Jahrhunderts am Herzen. Er hat immer versucht, eine breite Öffentlichkeit durch veränderte Abonnements und neue Programme in die Konzertsäle zurückzubringen. So liess er Strawinskis Geschichte vom Soldaten in Parks und auf Plätzen Montreals spielen, gab Beethovens Neunte Sinfonie in einer Hockey-Halle und initiierte in der Notre-Dame-Kirche von Montreal ein Festival, das unter dem Namen Mozart plus zu einem Begriff wurde.

1987 wurde Dutoit in Kanada zum Künstler des Jahres gewählt und mit den Ehrendoktorwürden der Universitäten von Montreal und Laval gewürdigt. Dutoit machte etwa 85 Platteneinspielungen und wurde mit über 40 internationalen Preisen ausgezeichnet. 1991 wurde er Ehrenbürger von Philadelphia.

Im Dezember 2017 wurde Dutoit von seinen Konzertverpflichtungen für das Royal Philharmonic Orchestra[1] vorübergehend entbunden. In den USA kündigten Orchester in Boston, San Francisco[2], Cleveland[3] und New York[4] nach Bekanntwerden der Vorwürfe die Zusammenarbeit mit ihm auf. Im Zuge der #MeToo-Debatte wird Dutoit vorgeworfen, zwischen 1985 und 2010 Sängerinnen und eine klassische Musikerin sexuell bedrängt zu haben.[5][6]
Von 1969 bis 1973 war er mit der Pianistin Martha Argerich verheiratet.

Heinrich Hammer (1905–1907) |
Wilhelm Stenhammar (1907–1922) |
Ture Rangström (1922–1925) |
Tor Mann (1925–1939) |
Issay Dobrowen (1941–1953) |
Dean Dixon (1953–1960) |
Sergiu Comissiona (1967–1973) |
Sixten Ehrling (1974–1976) |
Charles Dutoit (1976–1979) |
Neeme Järvi (1982–2004) |
Mario Venzago (2004–2007) |
Gustavo Dudamel (seit 2007)
