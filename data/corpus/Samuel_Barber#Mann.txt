Samuel Barber (* 9. März 1910 in West Chester, Pennsylvania; † 23. Januar 1981 in New York) war ein US-amerikanischer Komponist.

Barber begann im Alter von sieben Jahren zu komponieren. Mit neun erklärte er in einer Mitteilung an seine Mutter:

“[…] I was meant to be a composer, and will be I'm sure. I'll ask you one more thing.—Don't ask me to try to forget this unpleasant thing and go play football. […]”

„[…] Meine Bestimmung ist, Komponist zu sein, und ich bin sicher, dass ich das werde. Um eines möchte ich Dich noch bitten: Verlange nicht von mir, diese unerfreuliche Sache zu vergessen und Football spielen zu gehen! […]“[1]
Barber studierte am Curtis Institute of Music in Philadelphia, bevor er 1935 Mitglied der American Academy in Rom wurde. Im Jahr darauf schrieb er sein Quartett in B-dur, dessen bekannten zweiten Satz er für Streichorchester als Adagio for Strings (UA. 1938 unter Arturo Toscanini) arrangierte. 1961 wurde er in die American Academy of Arts and Sciences gewählt.

Die Popularität des Adagio hat das restliche Schaffen des Komponisten überstrahlt. Er wird jedoch als einer der talentiertesten amerikanischen Komponisten des 20. Jahrhunderts angesehen. Er vermied den Experimentalismus einiger anderer Komponisten seiner Generation und bevorzugte relativ traditionelle Harmonien und Formen. Sein Werk ist melodiös und wurde oft als die neo-romantische Periode in der Musik beschrieben. Keines seiner anderen Werke kam der Popularität des Adagio nahe, aber einige werden noch immer aufgeführt und aufgenommen.

Barbers Lebensgefährte war der Komponist Gian Carlo Menotti.[2]
Neben dem Adagio for Strings ist sein bekanntestes Werk die Oper Vanessa nach einem Libretto von Gian Carlo Menotti, uraufgeführt am 15. Januar 1958 in der Metropolitan Opera in New York. Gian Carlo Menotti verfasste auch das Libretto zu Samuel Barbers neunminütiger Kurzoper A Hand of Bridge (1959).

Die neue Met im Lincoln Center wurde am 16. September 1966 mit der Uraufführung seiner Oper nach der gleichnamigen Tragödie Antonius und Cleopatra von William Shakespeare eingeweiht.

Von seinen Klavier-Kompositionen ist die Sonate op. 26 die bedeutendste, nicht nur, weil sie seine einzige Auseinandersetzung mit der Zwölftonmusik darstellt, sondern auch aufgrund ihrer enormen Schwierigkeiten für den Pianisten – so vor allem in der auf Wunsch von Vladimir Horowitz (der diese Sonate am 9. Dezember 1949 uraufführte) hinzugefügten vierstimmigen Schluss-Fuge über ein mit zahlreichen Intervall-Sprüngen im schnellen Tempo gespicktes Thema.
