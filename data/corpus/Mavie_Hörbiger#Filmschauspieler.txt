Mavie Hörbiger (* 14. November 1979 in München) ist eine deutsch-österreichische Schauspielerin, die derzeit am Wiener Burgtheater verpflichtet ist.

Mavie Hörbiger ist die Tochter von Thomas Hörbiger und dessen Ehefrau Gaby und somit die Enkelin von Paul Hörbiger, Nichte zweiten Grades von Christiane Hörbiger und Cousine von Christian Tramitz. 

Im Dezember 2006 heiratete sie in Basel ihren Schauspielkollegen Michael Maertens, nachdem sie sich im selben Jahr von ihrem langjährigen Partner, dem Regisseur Christopher Roth, getrennt hatte. 2009 wurden Hörbiger und Maertens Eltern einer Tochter, im August 2012 eines Sohnes. Die Familie lebt in  Wien.

Mavie Hörbiger besuchte die Realschule im bayerischen Aichach und machte dort die Mittlere Reife. Bereits während ihrer Schulzeit stand sie 1996 für Michael Gutmanns TV-Film Nur für eine Nacht vor der Kamera. Sie begann eine Schauspielausbildung im Schauspielstudio von Christa Willschrei in München, die sie aber nicht beendete. Nach ihrer Schulzeit lehnte sie für ein Jahr alle Rollenangebote ab und arbeitete in einer Videothek in Berlin. Danach konzentrierte sie sich wieder auf ihre Schauspielkarriere.

Seit 2001 spielt sie parallel zu ihren Film- und Fernsehrollen auch Theater, beginnend mit einem Gastengagement am Schauspielhaus Hannover. In dem Stück Komödie der Verführung stand sie in der Spielzeit 2002/2003 parallel auch im Schauspielhaus Bochum auf der Bühne, wo sie 2004 in Lulu zum ersten Mal eine Hauptrolle in einem Theaterstück übernahm. 2006 begann ihr zunächst auf zwei Jahre angelegtes festes Engagement am Theater Basel mit ihrer Rolle als „Roxane“ in dem Stück Cyrano, gefolgt von „Stella“ in Endstation Sehnsucht.

2004 erreichte Mavie Hörbiger bei der Wahl der 100 Sexiest Women in the World des Männermagazins FHM Platz 3 hinter Britney Spears und Heidi Klum. Insgesamt gelangte sie in den Jahren 2002, 2004, 2005, 2006 und 2008 unter die ersten 100.

Seit 2005 ist die Schauspielerin durch ihre Rolle als Lilo Gabriel in der TV-Serie Arme Millionäre (RTL und ORF) auch einem größeren Publikum bekannt. Im darauf folgenden Jahr wurde sie in einer Zuschauerwahl für die ZDF-Sendung Unsere Besten auf Platz 47 der 50 besten deutschsprachigen Schauspieler aller Zeiten gewählt und war damit die Jüngste, die es in die Liste geschafft hat.

Seit 2006 hat sie in verschiedenen Projekten mit ihrem Ehemann Michael Maertens zusammengearbeitet, so zum Beispiel in der Aufführung von Ein Sommernachtstraum am Schauspielhaus Zürich und bei den Salzburger Festspielen im zweiten Halbjahr 2007, bei der Lesung Briefe einer Freundschaft im Mai 2007 in Wien und bei der Lesung In Eigenregie: Theodor Storm – Briefe an eine Geliebte, inszeniert von Christian Papke, im Februar 2008, ebenfalls in Wien.

Am Wiener Burgtheater stand Hörbiger 2009/2010 in Alfred de Mussets Lorenzaccio und in Peter Handkes Nachdichtung der Helena von Euripides auf der Bühne, 2010/2011 spielte sie in Michael Laubs Burgporträts.[1] In der Saison 2011/2012, in der sie festes Ensemblemitglied des Burgtheaters wurde, folgten die Rollen der Tinkerbell in J. M. Barries Peter Pan und der Sian in Simon Stephens' Wastwater. 2012/2013 verkörperte sie die Marie in Molnárs Liliom.[2]
2013 war sie in der Tatort-Folge Willkommen in Hamburg, der ersten mit Til Schweiger als Hauptkommissar, als dessen Ex-Freundin Sandra Bieber zu sehen.

Ab 2013 spielt Hörbiger in Johann Nestroys Zauberposse Der böse Geist Lumpazivagabundus in einer Koproduktion des Burgtheaters mit den Salzburger Festspielen die Fee Amorosa, die Wirtstochter Nannette, die Anastasia Hobelmann, die Magd Reserl sowie die Camilla Palpiti.[3] In der Spielzeit 2013/2014 wirkte sie auch bei der Zeitzeugenproduktion Die letzten Zeugen von Doron Rabinovici und Matthias Hartmann am Burgtheater mit; die Produktion bezog sich auf die Novemberpogrome 1938, erlangte hohe Wertschätzung seitens Publikum und Presse und wurde zum Berliner Theatertreffen 2014 eingeladen.

2015 übernahm sie Hauptrollen in Tolstojs Die Macht der Finsternis, inszeniert von Antú Romero Nunes, und in Sophocles’ Antigone, inszeniert von Jette Steckel.
