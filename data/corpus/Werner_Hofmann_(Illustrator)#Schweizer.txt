Werner Hofmann (* 11. Oktober 1935 in Zug; † 27. Oktober 2005[1] in Luzern) war ein Schweizer Grafiker. 

Hofmann lebte in Luzern und arbeitete seit 1961 als freischaffender Illustrator und Holzschneider sowie als Lehrer an der Luzerner Schule für Gestaltung. Sein Schaffen ist beeinflusst durch Aufenthalte in Italien, Frankreich, Finnland, Spanien und New York. Er schuf Illustrationen zu Werken der Weltliteratur von George Moore, James Joyce, William Faulkner, Aleksis Kivi und Anton Tschechow.
