Max Mauermann (* 22. Juli 1868 in Tarnowitz; † 1. Juli 1929 in Wien) war ein österreichischer Ingenieur und Erfinder des rostbeständigen Stahls.

Max Mauermann kam aus dem oberschlesischen Tarnowitz in die Steiermark und gehörte seit 1899 zu den Mitarbeitern des Labors der Bleckmann Stahlwerke (vormals Phönix Stahlwerke) in Mürzzuschlag (Österreich), wo er bald zum Laborleiter ernannt wurde.

Er entwickelte dort in zahlreichen Versuchen im Jahre 1912 den ersten rostbeständigen Stahl und stellte dieses Produkt auf der Wiener Adria-Ausstellung 1913 vor. Mauermann stieg zum Direktor der Stahlwerke Bleckmann auf.

Ihn ereilte wie so vielen Erfindern das Schicksal, nicht den Ruhm seiner großartigen Erfindung sorgenfrei genießen zu können. Ein Stahlkonzern im Ruhrgebiet erzeugte später ebenfalls rostfreien Stahl. Es kam 1924 zu einem Patentprozess, den Mauermann in einem Ersturteil gewann. Den Ruhm und den Gewinn schöpfte allerdings der Ruhr-Konzern ab.

Der Prozess entwickelte sich in der Folge zu einem jahrelangen Rechtsstreit über Max Mauermanns Tod hinaus. Erst drei Tage nach seinem Ableben wird die Ersterfindung des rostfreien Stahls ausdrücklich bestätigt.

1955 wurde die Max-Mauermann-Gasse in Wien-Favoriten nach ihm benannt.
