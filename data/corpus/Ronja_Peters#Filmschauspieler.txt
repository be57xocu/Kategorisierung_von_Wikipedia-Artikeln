Ronja Peters (* 1. September 1989 in Schwerin) ist eine deutsche Schauspielerin und Model.

Peters spielte von Januar 2008 bis Oktober 2009 die Hauptrolle der Karla Bussmann in der Kinder- und Jugendserie Schloss Einstein, die seit dem 5. Januar 2008 im KiKA ausgestrahlt wird. Es ist ihre erste Fernsehproduktion. Am 17. April 2010 kehrte Peters noch einmal als Gastauftritt in die Serie zurück.

Sie lebte von August 2007 bis Mitte 2009 in Erfurt und machte dort ihr Abitur. Inzwischen ist sie nach Berlin gezogen und spielte von März bis September 2010 in der Serie Eine wie keine in der Hauptbesetzung mit. 2011 war sie für den German Soap Award nominiert in der Kategorie „Bester Newcomer“.
