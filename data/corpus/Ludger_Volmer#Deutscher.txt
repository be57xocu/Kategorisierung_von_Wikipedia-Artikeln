Ludger Volmer (* 17. Februar 1952 in Gelsenkirchen-Ückendorf) ist ein deutscher Politiker (Bündnis 90/Die Grünen).
Er war von 1998 bis 2002 Staatsminister im Auswärtigen Amt.

Nach dem Abitur 1971 am humanistischen Schalker Gymnasium absolvierte Volmer ein Studium der Sozialwissenschaften, der Pädagogik und der Philosophie in Bochum und Gießen, welches er 1978 an der Ruhr-Universität Bochum als Diplom-Sozialwissenschaftler abschloss. Er unterbrach das Studium für seinen Zivildienst im Krankenhaus. Nach dem Studium war er beruflich als wissenschaftlicher Mitarbeiter an der Universität, in der Wohnumfeldplanung beim Kommunalverband Ruhrgebiet, in der Erwachsenenbildung bei der Volkshochschule Gelsenkirchen und in der empirischen Sozialforschung tätig. 1998 wurde er bei Wilhelm Bleek an der Ruhr-Universität Bochum mit der Arbeit Die Grünen und die Außenpolitik – ein schwieriges Verhältnis zum Dr. rer. soc. promoviert. 1978 veröffentlichte er (mit Karl-Heinz Lehnardt) das Buch „Politik zwischen Kopf und Bauch“, in dem er aus der Sicht eines 78ers das Selbstverständnis der 68er, Studentenbewegung und APO analysierte.

Volmer war später als freiberuflicher Unternehmensberater im Bereich internationales Marketing und strategische Unternehmensentwicklung tätig. In diesem Zusammenhang ist er Mitgesellschafter einiger Beratungsunternehmen, u. a. der in Bad Honnef ansässigen Synthesis GmbH. Seit 2006 ist er zudem Dozent an der Freien Universität Berlin. Er lehrt am Otto-Suhr-Institut Außen- und Sicherheitspolitik.

Heute ist Volmer freiberuflicher Publizist, Dozent und Politikberater.[1]
Volmers Vater Günter Volmer war von 1969 bis 1983 Mitglied des Deutschen Bundestages (CDU). Seine Mutter Maria-Theresia und seine Großväter Peter Volmer und Bernhard Saager waren Gründungsmitglieder der Katholischen Arbeitnehmer-Bewegung und der CDU. Ludger Volmer hat einen Sohn.

Volmer war seit 1969 in meist selbst mitgegründeten (Bürger-)Initiativen aktiv, etwa in der freiwilligen Sozialarbeit in Obdachlosensiedlungen. An der Universität gehörte er seit 1974 zu den Initiatoren der „undogmatisch-linken“ Basisgruppen, die er in nahezu allen Uni-Gremien vertrat. In der Folge zählte er 1979 zu den Mitbegründern zunächst der Sonstigen Politischen Vereinigung SPV Die Grünen, dann der Partei Die Grünen. In den Zeiten der Spaltung der Partei in Fundis und Realos war er lange Zeit führender Repräsentant der gemäßigten Linken, gründete das „Linke Forum“ und machte sich für die Integration der zerstrittenen Partei stark. Auf dieser Basis war er von 1991 bis 1994 Parteivorsitzender („Sprecher des Bundesvorstandes“). Er organisierte den Wiedereinzug in den Bundestag 1994, aus dem die westdeutschen Grünen (anders als das ostdeutsche Gegenstück) 1990 ausgeschieden waren. Zu diesem Zweck managte er die Fusion mit Bündnis 90 – Bürgerrechtlern der ehemaligen DDR – und den ostdeutschen Grünen und wurde erster Vorsitzender der fusionierten Partei Bündnis 90/Die Grünen. Neben einer tiefgreifenden Reform der Parteistrukturen förderte er die Gründung eines eigenständigen Verbandes der grünen Jugend, initiierte er die Neuordnung des grün-nahen Stiftungswesens (Heinrich-Böll-Stiftung) und rief er die grüne Parteizeitung „Schräg/strich“ ins Leben. Zudem eröffnete er offizielle Beziehungen zum Bundesvorstand des Deutschen Gewerkschaftsbundes und verschiedener Einzelgewerkschaften und bereitete so den Boden für die spätere Rot-Grüne Koalition im Bund. Bei den Koalitionsgesprächen 1998 verhandelte er als Mitglied der grünen Verhandlungskommission die Außenpolitik. Er war maßgeblich an zahlreichen, auch heute noch gültigen Programmschriften beteiligt.

Am 10. April 1985 rückte Volmer wegen des damals bei den Grünen geltenden Rotationsprinzips in den Bundestag nach, dem er zunächst bis 1990 angehörte. Seine Fraktion entsandte ihn in den Ausschuss für wirtschaftliche Zusammenarbeit, Auswärtigen Ausschuss und Finanzausschuss. Zudem war er u. a. Mitglied der deutschen Parlamentsdelegation bei Internationaler Währungsfonds und Weltbank. Am 1. Februar 1986 wurde Volmer als bisher jüngster Abgeordneter zu einem der Fraktionsvorsitzenden („Sprecher“) gewählt. Am 18. Juli legte er das Amt nieder, weil er bei der Aufstellung der Landesliste für die nächste Bundestagswahl nur einen unsicheren Platz bekommen hatte.

Inhaltlich übernahm Volmer von seiner Vorgängerin Gabriele Gottwald die Lateinamerika-Politik. Er engagierte sich besonders für die Befreiungs-, Emanzipations- und Friedensprozesse in Nicaragua, El Salvador und Chile.

Sein wichtigstes Anliegen aber war die Neudefinition der Entwicklungspolitik. Gegen die hilfsorientierte Projektpolitik setzte er einen weltwirtschaftskritischen Ansatz, der die ungerechten Handelsbeziehungen zwischen Nord und Süd sowie die internationale Schuldenkrise ins Zentrum rückte. So gehörte er 1985 zu den Initiatoren des ersten „Gegengipfels“ samt Großdemonstration gegen das G7-Treffen in Bonn. Anschließend organisierte er mit einem immer breiter werdenden Bündnis von NGOs eine weltwirtschaftskritische Kampagne, die 1988 anlässlich der Jahrestagung von IWF und Weltbank in West-Berlin in einen international viel beachteten Alternativkongress, eine Großdemonstration und zahlreiche dezentrale Aktionen mündete. In der Folge beteiligte er sich an der Gründung der Koordinierungsstelle WEED (World economy ecology and development) und der Herausgabe des „Infobriefs Weltwirtschaft & Entwicklung“. In der Fraktion verantwortete er als Sprecher für Weltwirtschaftsfragen die Erarbeitung der Programmschrift „Auf dem Weg zu einer ökologisch-solidarischen Weltwirtschaft“. Volmer und seine Mitstreiter versuchten mit der Kampagne zudem, die Dritte-Welt-Bewegung, die Umweltbewegung und die Friedensbewegung, die bis dahin nebeneinander agiert hatten, zu einer einheitlichen Bewegung zu integrieren. Zahlreiche von Volmer propagierte Ideen und Positionen wurden später von globalisierungskritischen Bewegungen wie attac weitergetragen.

1994 zog Volmer erneut in den Deutschen Bundestag ein und konzentrierte sich auf den Auswärtigen Ausschuss. Er wurde Mitglied der deutschen Delegation bei der Parlamentarischen Versammlung der OSZE. Dort gründete er die grün-alternative Gruppierung, deren Vorsitz er übernahm. Zudem wurde er stellvertretender Vorsitzender der deutsch-US-amerikanischen und der deutsch-russischen Parlamentariergruppe. Ende 2002 wurde er außenpolitischer Sprecher seiner Fraktion.

Volmer wandte sich in dieser Funktion von seinen früheren pazifistischen Positionen ab und befürwortete die deutsche Kriegsbeteiligung in Kosovo und Afghanistan im Rahmen des NATO-Militärbündnisses, für dessen Abschaffung er und seine Partei noch bis 1994 eingetreten waren. Eine deutsche Kriegsbeteiligung am Amerikanischen Krieg im Irak lehnte er wie die Bundesregierung ab.

Volmer analysierte die Ideen-, Programm- und Ereignisgeschichte der grünen Außen- und Friedenspolitik 1998 in seiner Doktorarbeit.

Am 11. Februar 2005 legte Ludger Volmer im Zuge der sog. Visa-Affäre auf einer Landesvorstandssitzung der Grünen Nordrhein-Westfalen seinen Sitz im Auswärtigen Ausschuss und als außenpolitischer Sprecher nieder. Zudem kündigte er an, für die Dauer seines Bundestagsmandats seine Mitarbeit in der Firma Synthesis ruhen und Gesellschafteranteile von einem Treuhänder verwalten zu lassen. Mit diesem Schritt wollte er erreichen, dass die mediale Berichterstattung zu seiner wirtschaftlichen Tätigkeit keine negativen Auswirkungen für seine Partei bei den Landtagswahlen in Nordrhein-Westfalen und Schleswig-Holstein habe. In der ersten öffentlichen Anhörung eines Untersuchungsausschusses wurde Volmer später fast zehn Stunden lang zur Visumpolitik befragt. Der Bericht des Ausschusses schreibt Volmer keinerlei Verschulden zu. Auch das abweichende Minderheitsvotum der Opposition wiederholt keinen persönlichen Vorwurf. Zudem hat Volmer seitdem eine Reihe gerichtlicher Auseinandersetzungen wegen falscher Behauptungen in einigen Medien gewonnen.

Bei der Bundestagswahl 2005 kandidierte er wegen der von ihm so empfundenen mangelnden Solidarität seines Landesverbandes nicht wieder für den Deutschen Bundestag.

Nach der Bundestagswahl 1998 wurde er am 27. Oktober 1998 als Staatsminister im Auswärtigen Amt in die von Bundeskanzler Gerhard Schröder geführte Bundesregierung berufen.
Nach der Bundestagswahl 2002 schied er am 22. Oktober 2002 wieder aus der Regierung aus.

Im Jahre 2002 wurde Volmer von einigen Medien vorgeworfen, dienstlich erworbene Bonusmeilen der Lufthansa für private Flüge von Frau und Sohn genutzt zu haben (siehe Bonusmeilen-Affäre). Er konnte aber nachweisen, dass seine Frau mit Einverständnis und im Auftrage des Auswärtigen Amtes geflogen war und für seinen Sohn private Meilen zur Verfügung standen.

1980–1993: Die GrünenAugust Haußleiter |
Petra Kelly |
Norbert Mann |
Dieter Burgmann |
Manon Maren-Grisebach |
Wilhelm Knabe |
Rainer Trampert |
Rebekka Schmidt |
Jutta Ditfurth |
Lukas Beckmann |
Christian Schmidt |
Regina Michalik |
Verena Krieger |
Ralf Fücks |
Ruth Hammerbacher |
Renate Damus |
Hans-Christian Ströbele |
Heide Rühle |
Ludger Volmer |
Christine Weiske

ab 1993: Bündnis 90/Die GrünenMarianne Birthler |
Ludger Volmer |
Krista Sager |
Jürgen Trittin |
Gunda Röstel |
Antje Radcke |
Renate Künast |
Fritz Kuhn |
Claudia Roth |
Angelika Beer |
Reinhard Bütikofer |
Cem Özdemir |
Simone Peter

Fraktionssprecher Die Grünen (1983–90):Marieluise Beck-Oberdorf |
Petra Kelly |
Otto Schily |
Annemarie Borgmann |
Waltraud Schoppe |
Antje Vollmer |
Sabine Bard |
Hannegret Hönes |
Christian Schmidt |
Annemarie Borgmann |
Hannegret Hönes |
Ludger Volmer |
Willi Hoss |
Thomas Ebermann |
Bärbel Rust |
Waltraud Schoppe |
Helmut Lippelt |
Regula Schmidt-Bott |
Christa Vennegerts |
Helmut Lippelt |
Jutta Oesterle-Schwerin |
Antje Vollmer |
Willi Hoss |
Waltraud Schoppe |
Marianne Birthler |
Antje Vollmer

Gruppensprecher Bündnis 90/Die Grünen (1990–94):Werner Schulz 

Fraktionsvorstände Bündnis 90/Die Grünen (seit 1994):Joschka Fischer |
Kerstin Müller |
Rezzo Schlauch |
Krista Sager |
Katrin Göring-Eckardt |
Fritz Kuhn |
Renate Künast |
Jürgen Trittin |
Katrin Göring-Eckardt |
Anton Hofreiter
