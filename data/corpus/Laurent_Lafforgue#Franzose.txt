Laurent Lafforgue (* 6. November 1966 in Antony bei Paris) ist ein französischer Mathematiker und Träger der Fields-Medaille.

Er gewann den ersten Preis bei den allgemeinen Wettbewerben um die Zulassung an den Eliteuniversitäten in Frankreich (Concours General) und war dann 1984 bis 1986 an Vorbereitungskursen am Lycée Louis-le-Grand. 1986 bis 1990 studierte er an der École normale supérieure, wo er 1988 seine Agrégation in Mathematik erwarb. 1988 bis 1991 forschte er dabei über Arakelov-Theorie bei Christophe Soulé, ab 1990 als Wissenschaftler (Chargé de Recherche) der CNRS an der Universität Paris-Süd in Orsay. 1991/92 leistete er seinen Wehrdienst als Lehrer an der Militärschule Saint-Cyr. 1994 promovierte er auf dem Gebiet der arithmetischen Algebraischen Geometrie an der Universität Paris-Süd bei Gérard Laumon (D-chtoukas de Drinfeld). Er ist seit 2000 Forschungsdirektor der CNRS und seit 2000 Professor der Mathematik am Institut des Hautes Études Scientifiques (IHES) in Bures-sur-Yvette.

Er bewies die Langlandskorrespondenz für lineare Gruppen über Funktionenkörpern (von algebraischen Kurven über endlichen Körpern), woran er 1994 bis 2000 in Anschluss an seine Arbeiten über die Shtuka´s von Wladimir Drinfeld arbeitete, die ein wesentliches Element des Beweises waren. Damit gelang ihm ein großer Durchbruch auf einem zentralen Forschungsgebiet der Zahlentheorie und arithmetischen algebraischen Geometrie, an dem seit 30 Jahren viele Mathematiker arbeiteten. Das Ziel ist dabei der Beweis für Zahlkörper, wie auch in anderen Fällen gilt aber der Beweis im einfacheren Fall für Funktionenkörper als wichtige Vorstufe. Die Langlands-Korrespondenz liefert eine (die jeweiligen L-Funktionen aufeinander abbildende) Bijektion zwischen den l-adischen r-dimensionalen irreduziblen Darstellungen der Galoisgruppe der Funktionenkörper und den Darstellungen im Raum der automorphen Spitzenformen der linearen Gruppe vom Rang r (mit Koeffizienten im Adele-Ring des Funktionenkörpers). Der Fall r=1 entspricht dem Artin´schen Reziprozitätsgesetz der Zahlentheorie und entspricht dem kommutativen Teil der Galoisgruppe. Der Fall r=2 wurde in den 1970er Jahren von Drinfeld bewiesen.

Ab 2001 befasste er sich mit projektiver Geometrie und den Konfigurationsräumen von Matroiden, wandte sich dann aber wieder dem Langlands-Programm zu. Seit 2004 befasst er sich auch mit Fragen der Mathematik-Ausbildung in Frankreich.

1996 erhielt er den Prix Peccot des Collège de France und 1998 die Bronzemedaille der CNRS. 2000 erhielt er den Clay Preis. 1998 war er Invited Speaker auf dem Internationalen Mathematikerkongress (ICM) in Berlin (Chtoucas de Drinfeld et applications). 2000 errang er den Clay Research Award. 2002 erhielt er beim ICM in Peking, Volksrepublik China die Fields-Medaille (neben Wladimir Wladislawowitsch Wojewodski) für seine überragenden Beiträge zum Langlands-Programm in der Zahlentheorie. Dort hielt er auch einen Plenarvortrag (Chtoucas de Drinfeld, Formule de trace d´Arthur-Selberg et correspondance de Langlands). Seit 2003 ist er Mitglied der Académie des sciences, deren Prix Jacques Herbrand er 2001 erhielt. Er ist Ehrendoktor der University of Notre Dame.

Er ist der Bruder des Mathematikers Vincent Lafforgue.

1936: Lars Valerian Ahlfors, Jesse Douglas |
1950: Laurent Schwartz, Atle Selberg |
1954: Kodaira Kunihiko, Jean-Pierre Serre |
1958: Klaus Friedrich Roth, René Thom |
1962: Lars Hörmander, John Milnor |
1966: Michael Atiyah, Paul Cohen, Alexander Grothendieck, Stephen Smale |
1970: Alan Baker, Heisuke Hironaka, Sergei Nowikow, John G. Thompson |
1974: Enrico Bombieri, David Mumford |
1978: Pierre Deligne, Charles Fefferman, Grigori Margulis, Daniel Quillen |
1982: Alain Connes, William Thurston, Shing-Tung Yau |
1986: Simon Donaldson, Gerd Faltings, Michael Freedman |
1990: Vladimir Drinfeld, Vaughan F. R. Jones, Shigefumi Mori, Edward Witten |
1994: Jean Bourgain, Pierre-Louis Lions, Jean-Christophe Yoccoz, Efim Zelmanov |
1998: Richard Borcherds, Timothy Gowers, Maxim Konzewitsch, Curtis McMullen |
2002: Laurent Lafforgue, Wladimir Wojewodski |
2006: Andrei Okunkow, Grigori Perelman, Terence Tao, Wendelin Werner |
2010: Elon Lindenstrauss, Ngô Bảo Châu, Stanislaw Smirnow, Cédric Villani |
2014: Artur Ávila, Manjul Bhargava, Martin Hairer, Maryam Mirzakhani
