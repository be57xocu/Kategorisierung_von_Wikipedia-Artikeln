Frederic Morton (* 5. Oktober 1924 in Wien als Fritz Mandelbaum; † 20. April 2015 ebenda[1]) war ein US-amerikanischer Schriftsteller österreichischer Herkunft. 


Fritz Mandelbaum wuchs in der Thelemangasse 8 im Wiener 17. Bezirk, Hernals, unweit von Brunnenmarkt, Yppenmarkt und Gürtel als Sohn von Franz Mandelbaum in bürgerlichen Lebensumständen auf und ging auf das Hernalser Gymnasium Geblergasse.[2][3][4] Einem 2002 publizierten Lebenslauf zufolge war er der beste Leichtathlet der Schule (Morton, 2002: Ich war ein schlechter Schüler. Das Einzige, was mich interessierte, war Sport. … Ich wollte eigentlich Sportlehrer werden …).
Das Haus in der Thelemangasse 8 sowie die drei anrainenden Häuser Nr. 2, 4 und 6 waren im Besitz seiner Familie. Großvater und Vater waren erfolgreiche Eisenwarenfabrikanten. Gegründet wurde das Familienunternehmen von Großvater Bernhard Mandelbaum, der für Kaiser Franz Joseph I. k.u.k. Orden und Medaillen schmiedete. Er kaufte das Haus Thelemangasse 8 der Erinnerung von Morton nach von einem Mitglied der Familie Kuffner, die bis 1938 die bis heute bestehende Ottakringer Brauerei besaß, derzeit die einzige Wiener Großbrauerei.

In der Thelemangasse 8 vermietete Großvater Mandelbaum zuletzt Räume an ein jüdisch-orthodoxes Bethaus, das nach dem Machtübergang an die Nationalsozialisten und dem „Anschluss“ Österreichs an das Deutsche Reich im Zuge der Novemberpogrome 1938 zerstört wurde. In diesem Zusammenhang wurde Mortons Vater kurz im KZ Dachau in Bayern festgehalten.

1939 musste die jüdische Wiener Familie Mandelbaum nach England emigrieren, seit 1940 lebte sie in New York.

In den Vereinigten Staaten angekommen, änderte Franz Mandelbaum, Frederic Mortons Vater, den Familiennamen auf Morton. Dem Lebenslauf 2002 zufolge habe der Vater als Hr. Mandelbaum nicht in die damals antisemitischen US-Gewerkschaften eintreten dürfen. Frederic Morton besuchte eine Bäckergewerbeschule und arbeitete 1940–1949 als Bäcker. Er studierte Nahrungsmittelchemie (Morton: Ich war der Einzige aus der Gewerbeschule, der an die Universität ging.[5]) bis zum Bachelor of Science.

Von 1949 an studierte Morton Literaturwissenschaft bis zum Master degree in Sprachphilosophie. Dabei lernte er seine Frau Marcia kennen und heiratete sie im Schloss Mirabell in Salzburg, dem Sitz des Magistrats der Stadt Salzburg.

Als US-amerikanischer Korrespondent kam Morton 1951 zeitweise nach Wien zurück, das damals teilweise amerikanisch besetzt war. Sein literarisches Talent soll von Thomas Mann gelobt worden sein (nähere Angaben dazu fehlen).

Von 1959 an war Morton freiberuflicher Autor. Er arbeitete unter anderem als Kolumnist für The New York Times, den Playboy, Esquire und Village Voice. Mit seinem 1962 erschienenen Roman The Rothschilds. A Family Portrait, der in zahlreichen Sprachen erschien, etablierte er sich als Bestsellerautor. Er erhielt 1963 den „Author of the Year Award“ der Anti-Defamation League der USA.

Seine Frau, Marcia Morton, war ebenfalls publizistisch tätig. Sie schrieb unter anderem Art of Viennese Cooking (Die Kunst der Wiener Mehlspeisen) und Chocolate: An Illustrated History.

Die Zeitschrift Vanity Fair meinte 2015 in einem Nachruf auf ihrer  Website, Austria never left him, und nannte speziell sein Buch A Nervous Splendor: Vienna, 1888–1889, das ebenso wie The Rothschilds ins Finale zum National Book Award der USA kam. Auf der Website The New York Times war zu lesen, Morton habe in den Vereinigten Staaten  a celebrated literary career gemacht. The Washington Post schrieb, Morton sei a highly regarded chronicler of his abandoned homeland gewesen, capturing in works of history and fiction the Viennese society at the fin de siecle and on the eve of two world wars.

Die Thelemangasse, in der Morton geboren wurde, wird nicht selten als Thelemanngasse zitiert. Ein mit Morton geführtes Interview zur Ausgabe 2002 des Romans Ewigkeitsgasse (siehe Abschnitt Eigene Werke) wurde mit einem Foto des Autors illustriert, der in einem Innenraum vor einer Straßentafel Thelemann-Gasse steht. Dabei handelt es sich nicht um eine Originaltafel, da der Gassenname auch vor hundert Jahren nicht mit Doppel-n geschrieben wurde.
