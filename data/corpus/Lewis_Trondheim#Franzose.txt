Lewis Trondheim (* 11. Dezember 1964 in Fontainebleau, mit bürgerlichem Namen Laurent Chabosy) ist ein französischer Comiczeichner und Gründungsmitglied des Verlags L’Association. Er ist verheiratet mit Brigitte Findakly (die einige seiner Alben kolorierte), hat zwei Kinder und lebt mit seiner Familie in Montpellier in Frankreich, wo er auch arbeitet.

Das Pseudonym nach der norwegischen Stadt Trondheim erklärt Lewis Trondheim selber wie folgt: „Als Nachnamen wollte ich einen Städtenamen, aber Lewis Bordeaux oder Lewis Toulouse klang nicht so gut. Da fiel mir diese Stadt ein, Trondheim … Vielleicht veröffentliche ich irgendwann ein Album unter meinem richtigen Namen, um unerkannt zu bleiben.“

Trondheim hat seit 1990 über hundert eigene Arbeiten veröffentlicht. In Frankreich werden die meisten seiner Alben vom Verlag L’Association veröffentlicht. Der Verlag wurde im Mai 1990 von Trondheim und den französischen Comiczeichnern Jean-Christophe Menu, David B., Patrice Killoffer, Mattt Konture, Stanislas sowie Mokeït gegründet.

Trondheims Arbeiten sind in viele Sprachen übersetzt worden, unter anderem auch ins Englische, ins Japanische und ins Deutsche.

In Deutschland wurde Trondheim mit der Comicserie Herrn Hases haarsträubende Abenteuer bekannt. In vielerlei manchmal zusammenhängenden Szenarien erleben dort der Held Herr Hase und seine Freunde und Feinde (alle im Funny-animal-Format gezeichnet) allerlei absonderliche, abstruse, fantastische und philosophische Abenteuer. Besonders die Dialoge fanden viel Lob und Beachtung seitens der Kritik. Der erste französische Band, „Lapinot et les Carottes de Patagonie“ (zu deutsch etwa „Herr Hase und die Mohrrüben von Patagonien“) erschien bisher wegen seines gigantischen Umfangs von über 500 Seiten nicht außerhalb Frankreichs, nach Aussagen von Trondheim selbst ist auf absehbare Zeit auch nicht mit einer deutschen Veröffentlichung zu rechnen.

Seine andere international bekannte und auch ins Deutsche übersetzte Serie ist Donjon, eine gemeinsam mit Joann Sfar produzierte Parodie auf das Fantasy-Genre in epischer Länge und mehreren Zyklen, von der bislang 36 Bände auf deutsch veröffentlicht wurden. Seit 2011 produziert er mit Ralph Azham eine weitere Fantasy-Serie, die Donjon stark ähnelt, aber in einer anderen Fantasy-Welt spielt.

Trondheims Comicfigur die Fliege (der Comic dazu ist ohne Worte) gab Anlass zur Produktion eines Zeichentrickfilms, der in Deutschland im öffentlich-rechtlichen Fernsehen innerhalb der Sendungen Sendung mit der Maus und Käpt’n Blaubär Club lief. Eine weitere Verfilmung seiner Comic-Serien wurde mit der 26teiligen Trickfilmreihe Kaput & Zösky realisiert, welche im deutschsprachigen Raum bislang nur im Pay-TV-Sender Junior sowie kurzzeitig auf ORF 1 zu sehen war.

Des Weiteren schreibt und zeichnet Trondheim an einer Vielzahl von Projekten, sowohl alleine als auch in Kooperation mit anderen Zeichnern und Autoren.
