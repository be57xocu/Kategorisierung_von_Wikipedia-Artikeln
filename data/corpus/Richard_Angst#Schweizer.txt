Richard Angst (* 23. Juli 1905 in Zürich; † 24. Juli 1984 in Berlin) war ein Schweizer Kameramann.

Richard Angst begann seine Karriere als Fotolaborant. Er war ein begeisterter Skifahrer und Bergsteiger. So kam es, dass er als Kameraassistent in den Filmen Milak, der Grönlandjäger und Arnold Fancks Der große Sprung teilnahm. Beide Filme wurden im Jahr 1927 gedreht. Hierbei arbeitete er u.a. mit den Kameramännern Sepp Allgeier und Albert Benitz zusammen.

In der Folge realisierte er weitere Filme mit Arnold Fanck, als dessen Stammkameramann er ab den Dreharbeiten zu den Film Die weiße Hölle vom Piz Palü (1929) gelten kann. Hierbei nahm er bis 1939 als Kameramann für Dokumentarfilme an verschiedenen Expeditionen Teil, die ihn unter anderem nach Grönland und Asien führten. Auf den Expeditionen wurde er auch für andere Regisseure tätig.

Bis in die 1960er Jahre gehörte er zu den großen Kameramännern des deutschsprachigen Films. Dabei arbeitete er u.a. mit Regisseuren wie Fritz Lang, Robert Siodmak, Hans Steinhoff und Paul Verhoeven zusammen. Richard Angst hat fast alle Filme von Kurt Hoffmann umgesetzt. Ein letztes Mal stand er für die Filme De Sade und Die Hochzeitsreise (1969) hinter der Kamera. Enttäuscht über den Verfall im deutschen Filmwesen zog er sich ins Privatleben zurück und eröffnete in Berlin-Moabit das Restaurant Provinz.

Im Ruhestand schrieb Angst (mit Unterstützung des Journalisten Hans Borgelt) seine Memoiren (Arbeitstitel "47 Jahre objektiv gesehen") und war u.a. als Dozent an der Hochschule für Fernsehen und Film München tätig.

Richard Angst starb einen Tag nach seinem 79. Geburtstag am 24. Juli 1984. Seine Urne wurde auf dem Waldfriedhof in Grünwald bei München beigesetzt, doch ist die Grabstätte inzwischen aufgehoben. Sein Nachlass wird von der Stiftung Deutsche Kinemathek verwaltet.
