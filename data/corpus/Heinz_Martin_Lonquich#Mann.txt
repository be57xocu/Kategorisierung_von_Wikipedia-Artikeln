Heinz Martin Lonquich (* 23. März 1937 in Trier; † 23. Juli 2014 in Köln) war ein deutscher Komponist, Kirchenmusiker und Diakon.

Lonquich studierte an den Musikhochschulen Saarbrücken und Köln (Klavier bei Alexander Sellier, Komposition bei Bernd Alois Zimmermann, elektronische Musik bei Herbert Eimert). Von 1958 bis 1973 war er an den Städtischen Bühnen Münster, Braunschweig und Köln als Solo-Repetitor und Kapellmeister tätig, daneben war er auch Liedbegleiter. 1969 bis 1972 war er Mitglied der Gruppe 8 Köln. Von 1973 bis 2002 war er Lehrbeauftragter für Repetition an der Musikhochschule Köln und Kirchenmusiker an St. Nikolaus in Köln-Sülz, seit 1976 dort auch Diakon. 2003 ging er in den Ruhestand. Er starb nach langer Krankheit am 23. Juli 2014 in Köln. Der Pianist Alexander Lonquich (* 1960) ist sein Sohn.

Lonquich wurde am 1. August 2014 nach der Auferstehungsmesse in der Kirche St. Pius in Köln-Zollstock auf dem Südfriedhof (Flur 75 Nr.112) bestattet.

Lonquich schuf zahlreiche Kompositionen für verschiedenste Besetzungen in nahezu allen Gattungsbereichen mit den Schwerpunkten Klavier und Kammermusik, Sologesang und geistliche Musik; darunter auch viele Neue Geistliche Lieder. Verschiedene Aufnahmen seiner Werke, darunter diverse Produktionen von Liedern, Klavier- und Kammermusikwerken, entstanden durch den WDR Köln.
