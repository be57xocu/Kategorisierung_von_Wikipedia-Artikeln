Boy Christian Klée Gobert (* 5. Juni 1925 in Hamburg; † 30. Mai 1986 in Wien) war ein deutsch-österreichischer[1]Theaterschauspieler, Filmschauspieler und Theaterregisseur.

Boy Gobert war der Sohn des Hamburger Kultursenators Ascan Klée Gobert. Er kam nach einigen Theaterengagements, unter anderem bei seinem Schauspiellehrer Helmuth Gmelin am Theater im Zimmer, 1954 zum Film, wo er vor allem auf das Rollenfach von Dandys, Snobs und Bonvivants festgelegt war. „In über 50 Filmen der Nierentischzeit juxte Gobert dann näselnd, blasiert, durch das Land des Lächelns“, schrieb Der Spiegel im Nachruf auf Gobert 1986.[2]
Gobert wurde als Nachfolger von Kurt Raeck 1969 Intendant des Hamburger Thalia-Theaters, das er bis 1980 leitete. Dort gelang es ihm, auch sein eigenes Rollenspektrum zu erweitern und weiterzuentwickeln. Unter namhaften Regisseuren spielte er Rollen der Weltliteratur, darunter Shakespeares Richard III., Coriolan und Goethes Faust, aber auch moderne Klassiker wie Arthur Schnitzlers Anatol und Carl Sternheims Snob. Daneben widmete er sich als Regisseur und Darsteller dem angelsächsischen Gegenwartstheater mit Autoren wie Harold Pinter und Trevor Griffiths. Ein besonderes Interesse entwickelte er als Intendant und Regisseur außerdem für den „gehobenen Boulevard“ unter der Devise „Ein Optimum an Kunst und Kasse“.[2]
Im Jahr 1980 wechselte er als Generalintendant an die Staatlichen Schauspielbühnen Berlin. Trotz vereinzelter künstlerischer Erfolge wie der Hans-Fallada-Revue Jeder stirbt für sich allein (Regie Peter Zadek) und Hans Neuenfels’ anspruchsvollen Inszenierungen von Goethes Iphigenie auf Tauris, Heinrich von Kleists Penthesilea, Robert Musils  Die Schwärmer und Jean Genets  Der Balkon gelang es Gobert insgesamt nicht, die an ihn gestellten, hohen Erwartungen als Nachfolger Hans Lietzaus zu erfüllen. Sein Vertrag wurde über die Spielzeit 1984/85 hinaus nicht verlängert. 

Auch die Abschlussproduktion von Schillers Wallenstein mit Gobert in der Titelrolle (Inszenierung Klaus Emmerich, dramaturgische Mitarbeit Heiner Müller) wurde verrissen. So schrieb Hellmuth Karasek: „Eine ziemliche Pleite, eine Beerdigung dritter Klasse. […] Ein Abschied vertan, verschwendet, vergeigt. Wenn etwas an diesen Abenden tragische Größe hätte haben können, dann Goberts böses Erwachen aus dem Gründgens-Traum.“[3]
Danach sollte Gobert mit der Spielzeit 1986/87 die Direktion des Wiener Theaters in der Josefstadt übernehmen. Dort war er bereits in den Proben von Edward Albees Wer hat Angst vor Virginia Woolf? mit Ingrid Andree, starb jedoch überraschend noch vor der Spielzeiteröffnung an Herzversagen in seinem Haus in Wien-Neustift am Walde – nur wenige Wochen vor dem als Dramaturg ebenfalls neu engagierten Ernst Wendt. Goberts ehrenhalber gewidmetes Grab befindet sich auf dem Neustifter Friedhof (Gruppe 22, Reihe 6, Nummer 1) in Wien-Döbling.

Seit 1960 war Gobert Mitglied des Wiener Burgtheaters. Durch den österreichischen Bundespräsidenten erhielt er den Titel eines Kammerschauspielers. Im Jahr 1961 wurde Gobert mit dem Deutschen Kritikerpreis ausgezeichnet. Im Jahr 1973 verliehen ihm die Mitglieder der Hamburger Volksbühne den Ehrenpreis Silberne Maske. Im Jahr 1977 erhielt er für seine Verdienste das Silberne Blatt der Dramatiker Union und war 1980 Preisträger der Goldenen Kamera als Erzähler und Darsteller in Der gute Doktor.[4]
Mit dem seit 1981 verliehenen Boy-Gobert-Preis für Nachwuchsschauspieler an Hamburger Bühnen, vergeben von der Körber-Stiftung und mit 10.000 Euro dotiert, wird Gobert posthum geehrt.[5]
Leopold Jessner |
Hermann Röbbeling |
Erich Ziegel |
Paul Mundorf |
Erich Kühn |
Ernst Leudesdorff |
Robert Meyn |
Willy Maertens |
Kurt Raeck |
Boy Gobert |
Peter Striebeck |
Jürgen Flimm |
Ulrich Khuon |
Joachim Lux
