Simon Bougis (* 1630 in Sées; † 1. Juli 1714 in Paris) war ein französischer Benediktinermönch, -abt und Generaloberer der Kongregation von Saint-Maur.

Bougis wurde als Spross eines alten adligen Geschlechts in der Normandie geboren. Er trat 1650 in den Benediktinerorden (Congrégation de S. Maur) ein, absolvierte sein Noviziat unter dem Prior Vincent Marsolle und dessen Stellvertreter Claude Martin in der Abtei Sainte-Trinité in Vendôme und legte am 6. Juli 1651 seine Profess ab.

Nach Abschluss seiner Studien im Jahre 1660 wurde Bougis (auch Bougy oder Bougie) Stellvertreter des Priors in der Abtei von Marmoutier, 1665 Prior in Lagny, 1672 Sekretär des Generaloberen Vincent Marsolle und gleichzeitige Mitarbeit an der Edition der Werke Augustins. 1682 nach dem Tode Marsolles wurde Bougis zum Prior der Abtei Saint-Denis-en-France gewählt, 1684 Visitator der Kongregation für die Provinz Normandie ernannt, 1687 Prior in der Abtei Saint-Ouen (Rouen).

1690 wurde er Assistent des Generaloberen und ergriff die Flucht nach Vendôme als er beim Kapitel des Jahres 1699 selbst zum Generaloberen gewählt wurde, um die Leitung der Kongregation nicht übernehmen zu müssen, so wurde stattdessen Dom Boistard ernannt.

Simon Bougis diente hiernach in der Abtei Saint-Pierre-de-Jumièges und wurde 1705 von den Nonnen des Klosters Val-de-Grâce als Visitator gewählt. Nachdem er beim Kapitel des Jahres 1705 nicht mehr vermeiden konnte zum Generaloberen der Kongregation ernannt zu werden, führte er dieses Amt gewissenhaft bis 1712. Nach kurzer Krankheit starb er am 1. Juli 1714 in Paris und wurde in der Kapelle von Sainte-Vierge neben Jean Mabillon beigesetzt.
