Gerti Senger (* 1942; eigentlich Gertrude Senger-Ernst[1]) ist eine österreichische Moderatorin und Psychologin. Sie praktiziert und lebt in Wien.

Senger studierte Psychologie und Pädagogik und ist eingetragene Klinische Gesundheitspsychologin und Psychotherapeutin (Verhaltenstherapie). Sie ist Mitbegründerin und Mitglied des wissenschaftlichen Beirates des Instituts für angewandte Tiefenpsychologie (IFAT). Sie ist Co-Vorsitzende der Österreichischen Gesellschaft für Sexualforschung (ÖGS) und Ehrenmitglied der Steirischen Gesellschaft für Lebens- und Sozialberatung. In den 1990er Jahren bildete sie mit Rotraud Perner, Ernest Borneman und Dieter Schmutzer das Ö3-Sexhotline-Team.

Sie ist Expertin für die Imago-Therapie, des Weiteren für Persönlichkeitstraining, Krisenbewältigung, Konfliktlösung sowie Beziehungstherapien. 

Von 1988 bis 1993 war sie Expertin in der wöchentlichen Beratungssendung love line im ORF; von 1994 bis 2004 moderierte sie die wöchentliche TV-Rubrik in Willkommen Österreich (ORF 2).
