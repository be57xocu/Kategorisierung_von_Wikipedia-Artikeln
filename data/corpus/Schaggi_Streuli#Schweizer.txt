Schaggi Streuli (* 4. Juli 1899 in Bauma; † 3. November 1980 ebenda; eigentlich Emil Kägi) war ein Schweizer Drehbuchautor, Kabarettist, Mundartdichter und Dialekt-Schauspieler.

Streulis Stiefvater war ein alkoholkranker Kleinbauer und Zimmermann. Seine Mutter, eine Fabrikarbeiterin, zog nach der Scheidung 1905 mit den sechs Kindern nach Höngg. Nach der Schule absolvierte Streuli eine kaufmännische Lehre und arbeitete anschliessend als Angestellter in der Wertschriftenabteilung einer Bank. Daneben begann er, Theaterstücke zu schreiben. 1928 heiratete er Elisabeth «Lisel» Eigenheer und eröffnete im Tessin ein Wirtshaus, das allerdings bald bankrott ging. 1929 kehrte Streuli mit seiner Frau nach Zürich zurück und nahm eine Stelle als Ausläufer in einer Metzgerei in Oerlikon an. Nach dem frühen Tod ihres gemeinsamen Kindes Hans Rudolf («Hansruedeli»), das 1931 knapp zweijährig starb, trennte er sich 1933 von seiner Frau. Ab 1936 war er als Schauspieler tätig. 1939 heiratete er Hedwig Obrist, mit der er in den 1950er-Jahren in Ober-Langenhard (Gemeinde Zell), danach in Schmidrüti (Gemeinde Turbenthal) sowie auf einem kleinen Bauernhof bei Steinenbach in der Nähe von Wila lebte, wo er sich in seiner Freizeit mit seinem Federvieh und Holzarbeiten beschäftigte. Nach kurzer Krankheit starb er am 3. November 1980 im Spital von Bauma.

Eine erste Anstellung als Schauspieler erhielt er 1936 am Corso-Theater in Zürich. Weitere  Auftritte hatte er in den 1930er- und 1940er-Jahren im Cabaret Cornichon in Zürich und an einer Volksbühne. Von 1947 bis in die 1950er Jahre gehörte Streuli als Autor und Schauspieler dem Cabaret Fédéral an, in dem Darsteller wie Lukas Ammann, Max Haufler, Stephanie Glaser, Blanche Aubry, César Keiser und Margrit Läubli mitspielten.

Von 1939 an benutzte Emil Kägi den Künstlernamen Schaggi Streuli. Seine erste kleine Rolle in einem Film hatte er 1938 im Schweizer Spielfilm Füsilier Wipf. Bekannt wurde er vor allem in seiner Paraderolle des Polizischt Wäckerli aus den gleichnamigen 17-teiligen Radio-Hörspiel (1949–1950) und durch den darauf basierenden Spielfilmen (1955/1966) und Fernsehserie (1963–1964), bei dem er zugleich als Autor und Schauspieler tätig war.

Weitere in der damaligen Zeit beliebte Hörspielreihen mit Schaggi Streuli als Autor und Hauptdarsteller waren Landarzt Dr. Hilfiker (1952), Oberstadtgass (1955) und Familie Heiri Aeppli (1960). Diese waren auch als Bühnenstücke 1950 (Polizischt Wäckerli) und 1953 (Landarzt Dr. Hilfiker) erfolgreich. Die verschiedenen Radiohörspiele, an denen er als Autor und Schauspieler mitwirkte, waren in den 1950er Jahren Strassenfeger und oftmals Vorlage für erfolgreiche Spielfilme wie Polizischt Wäckerli und Oberstadtgass.

Mit seinem Theaterensemble tourte Streuli in den 1950er- und 1960er-Jahren mit Stücken wie De Kampf ums Rächt (1951), Pinocchio (1952), der Bühnenadaption seines Hörspiels Fritz Vollenweider (1957), dem Dialektlustspiel Sie und de Chef (1960), Familie Heiri Aeppli und dem volkstümlichen Schwank Vater tut das nicht...! (1967) durch die Schweiz.

Seine letzte grosse Rolle hatte Streuli 1975 im Spielfilm De Grotzepuur von Mark M. Rissi. Darin spielte er einen alten Bauern, der mit dem Versuch, mittels Intensivmast das grosse Geld zu machen, tragisch scheitert.

Im Juli 2004 wurde an seinem letzten Wohnort im zürcherischen Tösstal zu seinem Gedenken und im Beisein des Kabarettisten Walter Roderer (dessen Förderer Streuli in den 1950er Jahren gewesen war) der «Schaggi-Streuli-Wanderweg» eingeweiht. Auf verschiedenen Informationstafeln entlang des Wanderweges wird dabei das Leben und Wirken des Schauspielers gewürdigt.

Kinofilme

Fernsehen
