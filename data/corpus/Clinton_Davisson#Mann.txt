Clinton Joseph Davisson (* 22. Oktober 1881 in Bloomington, Illinois; † 1. Februar 1958 in Charlottesville, Virginia) war ein US-amerikanischer Physiker und Nobelpreisträger.

Clinton Joseph Davisson wurde am 22. Oktober 1881 als Sohn des Handwerkers Joseph Davisson und der Lehrerin Mary Calvert in Bloomington im US-Bundesstaat Illinois geboren. Er begann im September 1902, nach dem Abschluss der High School in Bloomington, ein Studium der Mathematik und Physik an der University of Chicago, musste jedoch das Studium nach einem Jahr aus finanziellen Gründen abbrechen und nahm eine Stelle bei einer Telefongesellschaft in Bloomington an. Auf Empfehlung von Millikan, den er während seines Jahres in Chicago kennengelernt hatte, erhielt er im Januar 1904 eine Assistentenstelle an der Purdue University, von Juni 1904 bis August 1905 konnte er sein Studium in Chicago fortsetzen. Im September 1904 wurde er, erneut auf Empfehlung von Millikan, auf eine Teilzeitstelle als Physikausbilder an der Princeton University berufen, die er bis 1910 innehatte. Während seiner Freizeit besuchte er Vorlesungen bei Francis Magie, Edwin Plimpton Adams, James Jeans und Owen Willans Richardson. In den Sommersemestern besuchte er mehrmals die Vorlesungen in Chicago und erhielt dort 1908 den Bachelor of Science. 1910/11 erhielt er ein Stipendium für Physik an der Princeton University und promovierte dort 1911 unter Professor Richardson über die thermische Emission positiver Ionen der Erdalkalisalze. Er war von September 1911 bis April 1917 Ausbilder im Physik-Department am Carnegie Institute of Technology in Pittsburgh, im Juni 1917 nahm er für die Zeit des Krieges  eine Stelle in der Ingenieurabteilung der Western Electric Company (den späteren Bell Telephone Laboratories) in New York City an, nachdem er zuvor von der US-Army abgelehnt worden war. Nach Kriegsende lehnte er eine Assistenzprofessur am Carnegie Institute of Technology ab und blieb bei Western Electric.
Er wurde 1946 bei den Bell Telephone Laboratories nach 29 Dienstjahren pensioniert und war von 1947 bis 1949 Visiting Professor für Physik an der University of Virginia in Charlottesville, Virginia.

Davisson heiratete 1911 Charlotte Sara Richardson, die Schwester seines Doktorvaters Professor Richardson, und hatte vier Kinder, drei Söhne und eine Tochter. Er starb am 1. Februar 1958 in Charlottesville.

Davisson erhielt 1937 den Physik-Nobelpreis für die experimentelle Bestätigung der von de Broglie vorhergesagten Materiewellen, die ihm 1926 zusammen mit Lester Germer durch den Nachweis der Diffraktion von Elektronen an Kristallen gelungen war. LEED ist heute eine wichtige analytische Methode in der Oberflächenchemie. Die zweite Hälfte des Preises ging an George Paget Thomson.

1901: Röntgen |
1902: Lorentz, Zeeman |
1903: Becquerel, M. Curie, P. Curie |
1904: Rayleigh |
1905: Lenard |
1906: J. J. Thomson |
1907: Michelson |
1908: Lippmann |
1909: Braun, Marconi |
1910: van der Waals |
1911: Wien |
1912: Dalén |
1913: Kamerlingh Onnes |
1914: Laue |
1915: W. H. Bragg, W. L. Bragg |
1916: nicht verliehen |
1917: Barkla |
1918: Planck |
1919: Stark |
1920: Guillaume |
1921: Einstein |
1922: N. Bohr |
1923: Millikan |
1924: M. Siegbahn |
1925: Franck, G. Hertz |
1926: Perrin |
1927: Compton, C. T. R. Wilson |
1928: O. W. Richardson |
1929: de Broglie |
1930: Raman |
1931: nicht verliehen |
1932: Heisenberg |
1933: Schrödinger, Dirac |
1934: nicht verliehen |
1935: Chadwick |
1936: Hess, C. D. Anderson |
1937: Davisson, G. P. Thomson |
1938: Fermi |
1939: Lawrence |
1940–1942: nicht verliehen |
1943: Stern |
1944: Rabi |
1945: Pauli |
1946: Bridgman |
1947: Appleton |
1948: Blackett |
1949: Yukawa |
1950: Powell |
1951: Cockcroft, Walton |
1952: Bloch, Purcell |
1953: Zernike |
1954: Born, Bothe |
1955: Lamb, Kusch |
1956: Shockley, Bardeen, Brattain |
1957: Yang, T.-D. Lee |
1958: Tscherenkow, Frank, Tamm |
1959: Segrè, Chamberlain |
1960: Glaser |
1961: Hofstadter, Mößbauer |
1962: Landau |
1963: Wigner, Goeppert-Mayer, Jensen |
1964: Townes, Bassow, Prochorow |
1965: Feynman, Schwinger, Tomonaga |
1966: Kastler |
1967: Bethe |
1968: Alvarez |
1969: Gell-Mann |
1970: Alfvén, Néel |
1971: Gábor |
1972: Bardeen, Cooper, Schrieffer |
1973: Esaki, Giaever, Josephson |
1974: Ryle, Hewish |
1975: A. N. Bohr, Mottelson, Rainwater |
1976: Richter, Ting |
1977: P. W. Anderson, Mott, Van Vleck |
1978: Kapiza, Penzias, R. W. Wilson |
1979: Glashow, Salam, Weinberg |
1980: Cronin, Fitch |
1981: Bloembergen, Schawlow, K. Siegbahn |
1982: K. Wilson |
1983: Chandrasekhar, Fowler |
1984: Rubbia, van der Meer |
1985: von Klitzing |
1986: Ruska, Binnig, Rohrer |
1987: Bednorz, Müller |
1988: Lederman, Schwartz, Steinberger |
1989: Paul, Dehmelt, Ramsey |
1990: Friedman, Kendall, R. E. Taylor |
1991: de Gennes |
1992: Charpak |
1993: Hulse, J. H. Taylor Jr. |
1994: Brockhouse, Shull |
1995: Perl, Reines |
1996: D. M. Lee, Osheroff, R. C. Richardson |
1997: Chu, Cohen-Tannoudji, Phillips |
1998: Laughlin, Störmer, Tsui |
1999: ’t Hooft, Veltman |
2000: Alfjorow, Kroemer, Kilby |
2001: Cornell, Ketterle, Wieman |
2002: Davis Jr., Koshiba, Giacconi |
2003: Abrikossow, Ginsburg, Leggett |
2004: Gross, Politzer, Wilczek |
2005: Glauber, Hall, Hänsch |
2006: Mather, Smoot |
2007: Fert, Grünberg |
2008: Nambu, Kobayashi, Maskawa |
2009: Kao, Boyle, Smith |
2010: Geim, Novoselov |
2011: Perlmutter, Schmidt, Riess |
2012: Haroche, Wineland |
2013: Englert, Higgs |
2014: Akasaki, Amano, Nakamura |
2015: Kajita, McDonald |
2016: Thouless, Haldane, Kosterlitz |
2017: Barish, Thorne, Weiss
