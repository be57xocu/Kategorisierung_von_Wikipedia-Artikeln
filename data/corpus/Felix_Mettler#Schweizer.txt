Felix Mettler (* 21. November 1945 in Adliswil im Kanton Zürich) ist ein Schweizer Schriftsteller. 

Mettler studierte Tiermedizin und war mehrere Jahre als Oberassistent am Institut für Veterinär-Pathologie der Universität Zürich tätig. Seine beiden Romane wurden mehrfach aufgelegt, unter anderem auch im Fischer Taschenbuch Verlag. Sein Erstling, Der Keiler, wurde ins Englische und Italienische übersetzt. Der Roman diente auch als Grundlage für den Film Tod eines Keilers (2006) mit Joachim Król.[1] Felix Mettler lebt in Teufen AR.
