Adrienne Monnier (* 26. April 1892 in Paris; † 19. Juni 1955 ebenda) war als Buchhändlerin und Verlegerin eine zentrale Gestalt der Pariser Literaturszene zwischen dem Ersten und dem Zweiten Weltkrieg.

Adrienne Monnier arbeitete zunächst als Lehrerin. 1915 eröffnete sie zusammen mit ihrer Freundin Suzanne Bonnierre in der Rue de l'Odéon in Paris eine französische Buchhandlung mit dem Namen „La Maison des Amis des Livres“. Adrienne Monnier war eine der ersten Buchhändlerinnen in Frankreich. Sie veranstaltete im „Maison“ Lesungen mit zeitgenössischen französischen Schriftstellern wie André Gide, Paul Valéry und Jules Romains und trug so dazu bei, diese einem größeren Kreis Literaturinteressierter bekannt zu machen. Auch Colette, André Breton, Paul Claudel und Rainer Maria Rilke verkehrten in der Buchhandlung.

Mit Monniers Hilfe konnte Sylvia Beach im November 1919 die erste englischsprachige Leihbücherei und Buchhandlung in Paris, Shakespeare and Company gründen, zunächst in der Rue Dupuytren Nr. 8. Schnell wurde diese Buchhandlung zum Treffpunkt besonders für US-Amerikaner. 1921 zog das Geschäft um in die Rue de l'Odéon Nr. 12, gegenüber von Monniers Buchhandlung, auf dem linken Seineufer.

Monnier und Beach waren mit vielen Größen der Pariser Literatur- und Kunstszene bekannt, u. a. D. H. Lawrence, Ernest Hemingway, Ezra Pound, T. S. Eliot, Valéry Larbaud, Thornton Wilder, André Gide, Léon-Paul Fargue, George Antheil, Robert McAlmon, Gertrude Stein, Stephen Benet, Aleister Crowley, John Quinn, Berenice Abbott, Gisèle Freund und Man Ray. Als Buchhändlerin stand sie zusammen mit Sylvia Beach im Zentrum dieses intellektuellen Zirkels, der von Monnier gern als „Odéonia“ bezeichnet wurde.

Im Juni 1925 veröffentlichte Adrienne Monnier erstmals eine französische Literaturzeitschrift La navire d'argent.  Adrienne publizierte – neben einer Vielzahl französischer Schriftsteller – auch eine Übersetzung von T. S. Eliots The Love Song of J. Alfred Prufrock, das erste längere Gedicht Eliots, das auf Französisch erschien. Monniers Zeitschrift publizierte Listen von amerikanischen Werken, die in Übersetzung erhältlich waren, und widmete eine Ausgabe den amerikanischen Schriftstellern William Carlos Williams, Ernest Hemingway und e.e. cummings. Jean Prévost publizierte sein erstes Werk, L'Aviateur, im Navire d'argent. Die Zeitschrift erlebte zwölf Ausgaben.

Von Januar 1938 bis Mai 1945 gab Monnier die Gazette des amis des livres heraus (10 Nummern in 9 Lieferungen).

Adrienne Monnier starb 1955 durch Suizid.

Ihre Schwester Marie Monnier war verheiratet mit dem französischen Maler und Buchillustratoren Paul-Émile Bécat (1885–1960), der sowohl Adrienne Monnier als auch Sylvia Beach porträtierte.
