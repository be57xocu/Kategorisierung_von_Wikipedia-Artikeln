William Kotzwinkle (* 22. November 1943 in Scranton, Pennsylvania) ist ein amerikanischer Schriftsteller.

Bekannt wurde Kotzwinkle vor allem als Autor des Buchs zum Film „E. T.“ sowie einer Fortsetzung, die auf dem Heimatplaneten der außerirdischen Hauptfigur spielt; außerdem schrieb er unter anderem das Drehbuch zum US-amerikanischen Horrorfilm „Nightmare 4 – The Dream Master“.

Kotzwinkle besuchte das Rider College und die Pennsylvania State University. Nach verschiedenen Jobs begann er 1968 mit dem Schreiben und erhielt seitdem mehrere literarische Auszeichnungen. Er ist mit der Schriftstellerin Elizabeth Gundy verheiratet und lebt abwechselnd in den USA und Kanada.
