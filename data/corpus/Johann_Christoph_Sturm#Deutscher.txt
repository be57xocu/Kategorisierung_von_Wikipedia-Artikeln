Johann Christoph Sturm (lat. Johannes Christophorus Sturmius; * 3. November 1635 in Hilpoltstein (Mittelfranken); † 26. Dezember 1703[1] in Altdorf) war ein deutscher Astronom und Mathematiker.

Johann Christoph Sturm war der Sohn des Johann Eucharius Sturm und der Gertraud, geb. Bock. Ab 1646 war er Schüler der Lateinschule in Weißenburg in Bayern und  1653 als Zögling des Theologen Daniel Wülfer (Dozent für Logik, Metaphysik und Physik) in Nürnberg. Von 1656 bis 1662 studierte er Mathematik, Physik und Theologie in Jena, zwischenzeitlich ein Jahr in Leiden.

Nach kurzer Lehrtätigkeit in Nürnberg war Sturm ab 1664 Pfarrer in Deiningen (nahe Nördlingen). 1669 wurde er Professor für Mathematik und Physik an der Universität Altdorf, eine Position, die er bis zu seinem Tode innehatte. Während dieser Zeit war er mehrfach Dekan der philosophischen Fakultät. Er war dreimal verheiratet und hatte neun überlebende Kinder, darunter den Architekturtheoretiker Leonhard Christoph Sturm (1669–1719). Im Dezember 1703 starb Johann Sturm an den Folgen eines Schlaganfalles.

Sturm galt als besonders begabter Lehrer der Naturwissenschaften, der seinen Studenten nicht trockenes Bücherwissen eintrichterte, sondern den Lehrstoff auf verständliche Weise nahebrachte. Er vertrat weder die altmodische Schule nach Aristoteles noch die extrem moderne Richtung der Cartesianer, sondern eine Mischform. Lateinische Fachausdrücke der Naturwissenschaften wurden von Sturm erstmals eingedeutscht. Er gilt zudem als Begründer der für die Frühaufklärung wichtigen eklektischen Philosophie.

Mit der Christoph-Sturm-Straße trägt die ehemalige Obere Marktstraße in der Innenstadt seiner Geburtsstadt Hilpoltstein heute seinen Namen. Gleichfalls nach ihm benannt ist die Sturm Cove, eine Bucht in der Antarktis.
