Charlot Strasser (* 11. Mai 1884 in Bern; † 4. Februar 1950 in Zürich) war ein Schweizer Psychiater und Schriftsteller.

Der Sohn des Anatomieprofessors Hans Strasser absolvierte ein Studium der Medizin in Bern, Berlin, Leipzig, München und Paris (Promotion 1912). Nach Reisen durch Russland, China, Japan und Südamerika war er ab 1913 als Psychiater in Zürich tätig. Von 1935 bis 1944 war er ärztlicher Leiter des Männerheims zur Weid in Rossau (Gemeinde Mettmenstetten).

Zusammen mit Alfred Adler und Carl Furtmüller publizierte Strasser ab 1914 die «Zeitschrift für Individualpsychologie», mit seiner Ehefrau Vera Strasser verfasste er Arbeiten, die sich kritisch mit der Psychoanalyse beschäftigten. Sein literarisches Werk umfasste expressionistische Novellen, welche die Zürcher Dada-Szene beeinflussten.
