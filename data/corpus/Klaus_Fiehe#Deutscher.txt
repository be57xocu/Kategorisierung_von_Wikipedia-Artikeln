Klaus „John“ Fiehe (* 24. Januar 1957 in Hamm) ist ein deutscher Musiker und Hörfunkmoderator. 

Bekannt wurde Klaus Fiehe als Mitglied bei der Neue-Deutsche-Welle-Band Geier Sturzflug (Gesang, Saxophon).

Seit 1996 ist er Moderator beim Radiosender 1 Live. Dort führt er in 1 Live Fiehe (bis 2006: Raum und Zeit) jeden Sonntagabend von 22 bis 1 Uhr durch eine der letzten noch verbliebenen Autorensendungen im deutschen Radio. Die Musik wird nicht durch eine Redaktion bestimmt, sondern von Fiehe selbst. Klaus Fiehe wird oft mit dem britischen Kult-DJ und BBC-Moderator John Peel verglichen, ist doch auch seine Sendung genreübergreifend von altem Krautrock bis modernem Minimal und Dubstep.

Klaus Fiehe lebt in Solingen. Nach eigenen Angaben verfügt er über eine Sammlung von 40.000 Schallplatten. Fiehe ist ehemaliges Mitglied der britischen Punk-Band The Bollock Brothers, in der er neben Sänger und Freund Jock McDonald das Saxophon spielte.

Seit 2008 arbeitet Fiehe ehrenamtlich im Internetradioprogramm ByteFM mit, das der Medienjournalist Ruben Jonas Schnell im selben Jahr in Hamburg gründete. Die einstündige Sendung "Karamba" ist jeden Donnerstag um 20 Uhr auf byte.fm per Internet-Stream zu hören.
