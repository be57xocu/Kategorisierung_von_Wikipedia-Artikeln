Remigio Ratti (* 14. November 1944 in Balerna) ist ein Schweizer Volkswirtschaftler und Politiker.

Nach dem Schulbesuch im Tessin studierte Remigio Ratti an der Universität Freiburg Wirtschaftswissenschaften und wurde 1967 Diplom-Volkswirt. Anschliessend spezialisierte er sich an der Universität Leeds und an der Universität Triest in Wohlfahrtsökonomie und Verkehrswesen. 1970 promovierte er zum Doktor der Wirtschafts- und Sozialwissenschaften.

Nach seiner Habilitation zum Privatdozent 1975 war Remigio Ratti als Lehrbeauftragter an der Universität Freiburg tätig. 1982 wurde er zum Titularprofessor für Regionalökonomie berufen.

In den folgenden Jahren war er ausserdem als Gastprofessor an der Universität Neuenburg, an der Universität Basel und an der Universität Bergamo tätig. Gegenwärtig ist er Dozent für Wirtschaft und Institutionen an der Universität der italienischen Schweiz in Lugano.

1978 bis 1992 war Remigio Ratti Mitglied des Schweizerischen Wissenschaftsrates. 1992 bis 1995 war er Vorsitzender der Schweizerischen Studiengesellschaft für Raumordnungs- und Regionalpolitik (ROREP) und 1997 bis 2000 der eidgenössischen Beratenden Kommission für internationale Entwicklung und Zusammenarbeit.

1995 wurde Remigio Ratti in den Nationalrat gewählt. Während seiner Amtszeit war er Mitglied der Delegation EFTA/Europäisches Parlament, der Kommission für Wissenschaft, Erziehung Forschung (bis 1998) und der Kommission für Verkehr und Fernmeldewesen (ab 1998).

Im Frühjahr 1999 war Ratti neben Joseph Deiss und Adalbert Durrer einer der drei offiziellen Kandidaten der Christlichdemokratischen Volkspartei (CVP) für die Nachfolge von Bundesrat Flavio Cotti, schied jedoch nach dem zweiten Wahlgang aus.

Zwischen dem 1. Januar 2000 und dem 30. November 2006 war Remigio Ratti Generaldirektor der öffentlich-rechtlichen Rundfunk- und Fernsehanstalt der italienischsprachigen Schweiz (RTSI) und Mitglied der Geschäftsleitung der Schweizerischen Rundfunk- und Fernsehgesellschaft (SRG SSR idée suisse).

Ratti ist gegenwärtig Präsident des Stiftungsrats der Glückskette und der italienischsprachigen Rundfunk- und Fernsehgemeinschaft.
