Rainer Maria Salzgeber (* 15. August 1969 in Brig, Kanton Wallis) ist ein Schweizer Sportjournalist, Fussballkommentator und Fernsehmoderator.

Salzgeber ist in Raron aufgewachsen und spielte als Fussballtorwart zwei Spielzeiten in der 2. Liga beim FC Brig. Später zog er nach Zürich um, wo er beim Schweizer Fernsehen als Redaktor und Moderator arbeitet. In der Doku-Soap «Der Match» spielte Salzgeber auf seiner Position des Torwarts für die Promi-Mannschaft.

Für seine Moderationen anlässlich der Fussball-Europameisterschaft 2008 bekam er den Schweizer Fernsehpreis in der Kategorie Star National überreicht. Zudem wurde er zum Ende des Jahres 2008 zum Schweizer Sportjournalisten des Jahres ausgezeichnet.
