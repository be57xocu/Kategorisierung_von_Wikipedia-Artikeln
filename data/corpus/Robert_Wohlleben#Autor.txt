Robert Wohlleben (* 15. Juli 1937 in Hamburg-Rahlstedt) ist ein deutscher Lyriker, Essayist, Übersetzer und Verleger.

Nach dem Studium der Germanistik und Anglistik arbeitete er von 1961 bis 1964 als Wirtschaftsredakteur bei der Zeitschrift Erdöl und Kohle – Erdgas – Petrochemie. Von 1972 bis 1993 unterrichtete er Deutsch als Fremdsprache.

Wohlleben trat als Lyriker hervor. Er wirkt außerdem als Lyrik- und Graphikverleger. Er veröffentlicht in seinem Verlag fulgura frango die Schriftenreihe Meiendorfer Drucke, darin Texte u.a. von Gabriele d’Annunzio, Albrecht Barfod, Ernst-Jürgen Dreyer, Ludwig Harig, Arno Holz, Matthias Koeppel, Herbert Laschet Toussaint, Werner Laubscher, Ina Paul, Klaus M. Rarisch, Karl Riha, Ralf Thenior, Heinrich Würzer.

Auch veröffentlichte er Untersuchungen, etwa über Theodor Storm, Gorch Fock, Arno Holz und Arno Schmidt (etwa zu dessen Caliban über Setebos[1]). Neben Sonetten von Shakespeare übersetzte er James Fenimore Coopers The Monikins ins Deutsche. Für die Reihe Haidnische Alterthümer im Verlag Zweitausendeins erstellte er 1997 die schwierige typografische Wiedergabe der vollständigen Neuausgabe des originalen Textes von Johann Gottfried Schnabels vierbändigem Roman Insel Felsenburg. Auch Publikationen von CineGraph und  – bis 2008 – CineFest betreut er typografisch.

Bei diesem Programm sind Sammelbesprechungen nicht zu erwarten. (Vgl. jedoch z. B. Heinz Klunker, Vergnügen für Verstand und Witz, in: Deutsches Allgemeines Sonntagsblatt, Nr. 45, 5. November 1967, oder H. H., Erst als sie teure Drucke anboten, stieg das Interesse, in: Welt am Sonntag, Nr. 21, 26. Mai 1968.)
