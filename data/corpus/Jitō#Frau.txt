Jitō (jap. 持統天皇, Jitō-tennō; * 645; † 13. Januar 703) war die 41. Tennō von Japan (686–697). Sie war eine Tochter des Tenji-tennō und heiratete Temmu, den damaligen Kronprinzen und jüngeren Bruder ihres Vaters. Nach dem Tod des Temmu-tennō übernahm sie das Amt des Tennō und führte die Reformen ihres Mannes fort. Sie residierte auch weiterhin im Kiyomihara-Palast ihres Mannes, bis sie im Jahr 694 die Hauptstadt von Asuka-kyō nach Fujiwara-kyō (heute: Kashihara, Präfektur Nara) verlegen ließ.

Sie war für ihren starken Charakter und ihre Weisheit bekannt. 

Einige Forscher vermuten, das Bild der Sonnengöttin Amaterasu in den Geschichtsbüchern Kojiki und Nihonshoki, deren Abfassung zu Jitōs Zeiten begonnen wurde, reflektiere die Figur dieser Tennō.

Jitō hatte einen Sohn von Temmu, genannt Kusakabe. Prinz Kusakabe wurde der Kronprinz Temmus, aber er starb, bevor er den Thron besteigen konnte. Karu no Miko, der Sohn Kusakabes, war noch ein Kind, sodass Jitō den Thron bestieg, bis ihr Enkel erwachsen wurde und als Mommu das Amt, nach ihrer Abdankung am 17.08.697, von seiner Großmutter übernahm.

Strenggenommen sind die „himmlischen Majestäten“ erst ab Kaiserin Jitō als solche, also Tennō (天皇), zu bezeichnen, vorher sollte von den „Herrschern von Wa“ gesprochen werden. Auch dieser Landesname wurde erst Ende des sechsten Jahrhunderts in Nippon geändert.
