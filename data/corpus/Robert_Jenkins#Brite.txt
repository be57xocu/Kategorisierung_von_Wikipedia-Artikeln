Robert Jenkins (nachweisbar 1731–1745) war ein englischer Handelskapitän.

Als Jenkins mit der Brigg Rebecca auf der Heimfahrt von den Westindischen Inseln nach England war, enterten Mitglieder der spanischen Küstenwache unter Juan de Léon Fandino am 9. April 1731 das Schiff, durchwühlten den Laderaum und schnitten Jenkins ein Ohr ab.

Nach seiner Ankunft in England am 11. Juni 1731 brachte Jenkins eine Beschwerde über den Vorfall bei König Georg II. vor. Ein Bericht des britischen Oberbefehlshabers auf den Westindischen Inseln bestätigte Jenkins’ Darstellung.

Zunächst erregte der Fall kein größeres Aufsehen. Im März 1738 wiederholte Jenkins seinen Bericht jedoch detailliert und auf dramatische Weise vor einem Ausschuss des britischen Unterhauses, wobei er dem Parlament sogar ein in Alkohol eingelegtes Ohr zeigte, das er als das seinige ausgab.

Daraufhin heizte sich die latente anti-spanische Stimmung in England weiter auf, und Jenkins’ Bericht wurde von der englischen Presse und Opposition weiter aufgebauscht. Der von dem Vorfall letztlich mit ausgelöste Krieg zwischen England und Spanien 1739 wurde denn auch als „War of Jenkins’ Ear“ (übertragen etwa: Jenkins’-Ohr-Krieg, in der deutschsprachigen Literatur ist jedoch die Verwendung des englischen Begriffs üblich) bekannt.

Jenkins erhielt daraufhin den Befehl über ein Schiff der Britischen Ostindien-Kompanie und wurde später Gouverneur auf der Insel St. Helena im Südatlantik.

1741 wurde er von England aus nach St. Helena gesandt, um Korruptionsvorwürfe gegen den dortigen Gouverneur zu untersuchen, und von Mai 1741 bis März 1742 verwaltete er die Insel.

Danach wurde er wieder als Seefahrer tätig. Es wird berichtet, dass er im Kampf mit einem Piratenschiff sein eigenes Schiff und drei weitere unter seiner Obhut stehende Schiffe retten konnte.
