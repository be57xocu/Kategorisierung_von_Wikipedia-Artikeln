Beth Gibbons (* 4. Januar 1965 in Exeter, Großbritannien) ist die Sängerin der britischen Trip-Hop-Gruppe Portishead, Komponistin und Filmmusikkomponistin.

Zusammen mit ihrer Mutter und ihren beiden Schwestern wuchs sie auf einer Farm auf. Gibbons mochte das harte Farmerleben und vor allem den daraus resultierenden starken Zusammenhalt in der Familie. Im Jugendalter zog sie es vor, mit ihrer Mutter die Abende zu Hause zu verbringen und sich Platten anzuhören, anstatt mit ihren Freunden in der nahegelegenen Stadt zu feiern. Mit 22 Jahren ging sie nach Bristol, um sich als Sängerin zu behaupten. Dort traf sie 1991 auf Geoff Barrow, ihren zukünftigen Partner bei Portishead. Zusammen mit Jazz-Gitarrist Adrian Utley nahmen sie ein erstes Demo auf und wurden vom Label Go!Beat unter Vertrag genommen.

Zusammen mit Paul Webb von Talk Talk erlangte Beth Gibbons als „Beth Gibbons and Rustin’ Man“ auch außerhalb von Portishead Anerkennung. Sie schrieb auch einen Song für Joss Stones Album Mind, Body & Soul.

Beth Gibbons komponierte den Soundtrack zu dem französischen Film L’Annulaire von Diane Bertrand.[1]
Dummy • Portishead • Third

Roseland NYC

Glory Times

Roseland NYC

Sour Times • Numb • Glory Box • All Mine • Over • Only You • Machine Gun • The Rip • We Carry On • Magic Doors • Chase the Tear
