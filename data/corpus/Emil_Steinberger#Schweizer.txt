Emil Steinberger (* 6. Januar 1933 in Luzern; heimatberechtigt ebenda) ist ein Schweizer Kabarettist, Schriftsteller, Regisseur und Schauspieler, der seit den 1970er Jahren einem breiten Publikum in Deutschland, Österreich und der Schweiz dank seiner TV-Sketche als Emil bekannt ist.

Emil Steinberger ist der Sohn des Buchhalters Rudolf Steinberger und dessen Frau Creszentia, geb. Horat. Schon als Junge improvisierte er Sketche. Nach einer Ausbildung zum Postbeamten und neun Jahren Schalterdienst besuchte er ab 1960 fünf Jahre lang die Luzerner Schule für Gestaltung (heute Hochschule Luzern – Design und Kunst) und wurde diplomierter Grafiker. Schon damals spielte er im Kabarett «Cabaradiesli» mit. 1966 heiratete er Maya Rudin; Sohn Philipp wurde 1969 geboren. Im September 1967 eröffneten sie zusammen das Kleintheater am Bundesplatz, heute Kleintheater Luzern, in dem Jazzkonzerte, Theater- und Kabarettvorstellungen stattfanden und wo er seine ersten eigenen Programme aufführte. Er führte in Luzern zudem während einiger Jahre das bis heute unter gleichem Namen existierende Kino moderne und baute 1973 selbst ein Studiokino mit 150 Plätzen auf, das 2008 geschlossene Atelier-Kino.

Anfang der 70er Jahre füllte Steinberger mit seinen Soloprogrammen «Geschichten, die das Leben schrieb», «E wie Emil» und «Emil träumt» alle Theater der Schweiz und bald auch in Deutschland. Zur Bekanntheit in Deutschland trugen auch die von der ARD ausgestrahlten Emil-Aufzeichnungen bei. Es folgten diverse Tourneen durch die Bundesrepublik und die Schweiz, auch in der DDR trat er mehrmals auf.

Im Jahr 1977 stand er für neun Monate in der Manege des Circus Knie.

Ein weiterer Meilenstein seiner Karriere war eine der beiden Hauptrollen in dem Film Die Schweizermacher unter dem Regisseur Rolf Lyssy.

1980 war er finanzieller Geburtshelfer und Regisseur beim Neustart des Circus Roncalli in Köln. Im selben Jahr wurde sein zweiter Sohn Martin geboren.[1] Sein Bühnenprogramm Feuerabend, das er 1980 startete, war so erfolgreich, dass er beschloss, nur noch Emil zu sein.

In der französischen Schweiz spielte er in den 80er Jahren seine Nummern auch auf Französisch in den Programmen «Une heure avec Emil» und «Feu et flamme». In dem 1986 von Willy Bogner produzierten Sportfilm Feuer und Eis übernahm er in der deutschsprachigen Synchronisation die Rolle des Erzählers. 1987 trat er zum letzten Mal als Emil auf und beendete damit vorläufig seine Bühnenkarriere.

1989 wurde seine Ehe mit Maya geschieden.

Von 1990 bis 1991 gehörte Emil Steinberger zum Rateteam in Ja oder Nein, einem Was bin ich?-Remake mit Joachim Fuchsberger.
Gleichzeitig war er auch erfolgreich in der Werbung. Er schrieb und inszenierte unter anderem 100 Werbespots für Melitta-Kaffee, mit denen der Melitta-Mann Egon Wellenbrink berühmt wurde. Auch für Fisherman’s Friend und Bico-Matratzen schrieb er Werbespots und führte Regie. In dieser Zeit inszenierte er auch eine grosse Werbetour für «Schweiz Tourismus» unter dem Titel «Schweiz Plus» und tourte mit einer grossen Truppe durch Deutschland. 2008 wurde Steinberger nochmals als Werber aktiv, indem er zehn TV-Werbespots für Rivella International schrieb und spielte.

Ende 1993 ging er nach New York, um dort ein Leben in der Anonymität zu führen. Am 28. Mai 1999 heiratete er in New York Niccel Kristuf. 1999 kehrten sie zusammen in die Schweiz zurück und lebten 15 Jahre lang am Genfersee.

1999 erschien sein erstes Buch «Wahre Lügengeschichten». Im Jahr 2000 gründete er mit seiner Frau Niccel einen eigenen Verlag, die Edition E, in der 2001 sein zweites Buch «Emil via New York» erschien. In diesem Verlag veröffentlichen Steinbergers ihre Bücher und auch CDs und DVDs mit Emils früheren Programmen. 2013 ging die Edition E eine Kooperation mit dem Oltner Knapp Verlag ein. Seit 1999 war er zunächst mit Lesungen unterwegs, die sich aber schnell zu einem neuen Bühnenprogramm, «Drei Engel!», entwickelten. Mit diesem Programm trat er (Stand 2013) an die 850-mal auf. Von 2015 bis Ende 2017 machte er eine Tournee mit dem Programm «Emil – no einisch» («Emil – noch einmal»), das er auf Schweizerdeutsch, Deutsch mit Schweizer Lokalkolorit und Französisch gespielt hat[2] und in dem viele erfolgreiche Nummern aus seinen Programmen der 1960er- bis 1980er-Jahre enthalten sind. Im November 2017 wurde im Gloria-Theater in Bad Säckingen eine Aufzeichnung dieses Programmes für das Schweizer Radio und Fernsehen gemacht [3] und am 6. Januar 2018 anlässlich seines 85. Geburtstages auf SRF 1 ausgestrahlt.[4]
Zwischendurch war er auch als Sprecher in Kinderhörspielen (z. B. in Michel vo der Schwand (Michel aus Lönneberga) von Astrid Lindgren). Auch für Hans Fischers Kindergeschichte Pitschi wirkte er als Sprecher. Im Auftrag des Diogenes-Verlages las er für eine Hörbuch-Produktion auch Hugo Loetschers «Der Waschküchenschlüssel».

2006 stellten Emil und Niccel Steinberger im Haus der Kunst St. Josef in Solothurn zum ersten Mal ihre gemeinsam gezeichneten «Wochenblätter» aus. Es folgten Ausstellungen in Göttingen, Stuttgart, Lauchheim, Luzern, Montreux und anderen Orten.

Zu seinem 75. Geburtstag wurde er 2008 von der Stadt Luzern zum Ehrenbürger ernannt. 2013 brachte der Knapp-Verlag anlässlich des 80. Geburtstags Steinbergers neuestes Buch «Lachtzig» in der Perlenreihe heraus.

Emil Steinberger lebt seit 2014 mit seiner Frau Niccel, die in unregelmässigen Abständen «Lachseminare» anbietet,[5] in Basel.[6]
Emil Steinberger erhielt eine grosse Anzahl von Auszeichnungen:
