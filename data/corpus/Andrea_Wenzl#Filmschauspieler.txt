Andrea Wenzl (* 1979 in Leibnitz, Steiermark) ist eine österreichische Schauspielerin.

Andrea Wenzl besuchte die Ballettschule in ihrer Heimatstadt Leibnitz. Sie war auch an der Wiener Staatsoper, am Konservatorium Graz und an der Grazer Oper tätig. Nach dem Schauspielstudium an der Universität für Musik und darstellende Kunst Graz hatte die Steirerin von 2002 bis 2010 ein festes Engagement am Schauspielhaus Graz. Für die Darstellung der „Alice“ in Alice im Wunderland in der Regie von Viktor Bodó wurde sie 2008 für den Nestroy-Theaterpreis in der Kategorie „Beste Schauspielerin“ nominiert. Sowohl 2012 als auch 2013 wurde sie vom Kurier für die Auszeichnung mit einer ROMY in der Kategorie „Beliebteste Schauspielerin“ nominiert.

Von 2010 bis 2011 war Wenzl am Wiener Volkstheater tätig und wechselte 2011 ans Bayerische Staatsschauspiel. Seit 2015 ist sie am Wiener Burgtheater engagiert.

2017 wird sie das erste Mal bei den Salzburger Festspielen zu sehen sein. Sie spielt die Lulu in einer Inszenierung von Andrea Breth  in Die Geburtstagsfeier, einem Stück von Harold Pinter.

Mit dem Schauspieler Dominik Warta hat sie eine gemeinsame Tochter.[1]