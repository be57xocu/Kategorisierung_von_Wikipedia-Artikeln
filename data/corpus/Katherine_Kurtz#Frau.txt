Katherine Irene Kurtz (* 18. Oktober 1944 in Coral Gables, Florida) ist eine US-amerikanische Schriftstellerin, Naturwissenschaftlerin und Historikerin. Sie hat einen akademischen Grad in Chemie (BS) und einen in englischer mittelalterlicher Geschichte (MA), studierte ein Jahr Medizin und ist ausgebildete Erikson-Hypnotiseurin. Mehrere Jahre arbeitete sie als technische Schreiberin für das Los Angeles Police Department.

Ihr literarisches Debüt gab Kurtz Anfang der 1970er Jahre mit dem Fantasy-Zyklus Deryni über ein christliches Zauberergeschlecht. Fortan war Kurtz hauptberuflich als Schriftstellerin tätig. Zusammen mit Deborah T. Harris verfasste sie den Adept-Zyklus, in dem sich Elemente von Kriminalroman und Fantasy mischen. Ihr umfangreiches Werk umfasst neben Romanen auch Kurzgeschichten, Essays und Anthologien wie z. B. die 2002 veröffentlichten Anthologie zur Deryni-Serie.

Katherine Kurtz ist seit 1983 verheiratet mit Scott MacMillan, seit 1986 lebt das Paar in der neugotischen Villa Holybrooke Hall in Irland – mit einem Hund, sechs Katzen und angeblich mindestens zwei Geistern. 2007 kehrten Kurtz und MacMillan in die USA zurück. Sie bewohnen ein sogenanntes historisches Haus im neugotischen Stil in Virginia. Kurtz bezeichnet sich als Christin mit großer Affinität zu jener Spiritualität, die sie in den Deryni-Geschichten manchmal aufscheinen lasse (»Deryni-Tales«, S. 25). 

Natürlich will Kurtz mit ihren Romanen und Geschichten unterhalten, sie hofft aber auf mehr: 

Die Romane und Geschichten um die zauberfähigen Deryni gelten als Höhepunkte der historischen Fantasy und ihre Autorin als eine der Erfinderinnen dieses Subgenres und »queen of historical fantasies« (MBR Bookwatch, MidwestBook Review, Nov. 2003). 

In erster Linie geht es in den bisher rund 20 Büchern um den Konflikt von Kirche und Magie (oder zumindest magisch erscheinender Kräfte) und um die Probleme, die es für Menschen mit »Besonderheiten« oder Auffälligkeiten in der Gesellschaft geben kann. Exemplarisch werden anhand dieser Problematik aber auch viele andere Aspekte behandelt. Obwohl die Geschichten in einer mittelalterlichen Welt spielen, in der es so etwas wie Magie gibt, sind sie in vielen Punkten leicht übertragbar auf unsere Welt und sogar unsere Zeit – und das ist von der Autorin beabsichtigt und macht einen Teil des Reizes aus.
Nach eigenen Aussagen träumte sie 1964 die Grundstory zur Deryni-Saga und machte daraus ein Jahr später eine Geschichte. 1970 erschien der darauf basierende Roman »Deryni Rising«, der sofort zum Bestseller wurde.
Ihr aufklärerischer Ansatz wird schon in den ersten Romanen deutlich und von Buch zu Buch erkennbarer, auch werden im Subtext immer mehr Themenbereiche angesprochen, so etwa die Auseinandersetzung mit anderen Religionen und Lebensentwürfen, und das Plädoyer für Toleranz wird immer stärker. Dies alles, ohne dass Spannung oder Qualität leiden, im Gegenteil.

In diesem Punkt folgt Kurtz dem Vorbild der von ihr sehr geschätzten Darkover-Romane von Marion Zimmer Bradley, auch wenn sie nie so explizit wie Bradley die sexuelle Orientierung zum Thema macht. Sie folgt Bradley auch im großzügigen Umgang mit Fan-Fiction.

Die Erzählung Des Marluks Untergang, 1982 (Sword Against Marluk, 1977) in Ashtaru der Schreckliche, 1982, E. Ringer und H. Urbanek (Hrsg.) spielt einige Jahre vor Beginn des späten Deryni-Zyklus.

Alle Bände wurden übersetzt von Horst Pukallus. Auch wenn es häufig (wie o. a.) so dargestellt wird, dass die Collection Die Deryni-Archive der 4. Band des Zyklus »Die Geschichte von König Kelson« ist, so ist dies falsch. Dieser Irrtum resultiert aus der falschen Ankündigung des Heyne-Verlages.

Die Collection ist Bestandteil des (übergeordneten) Deryni-Zyklus, jedoch ein Sammelband mit Geschichten und Essays. Der 4. Band der »Geschichte von König Kelson« ist der Roman King Kelson's Bride (2000, nicht auf Deutsch erschienen).[1]
mit Deborah Turner Harris

mit Scott MacMillan
