Michaela Sburny (* 17. Mai 1959 in Wien) ist eine österreichische Politikerin der Grünen.

Nach der Matura am Wirtschaftskundlichen Realgymnasium Wien 1977 besuchte Michaela Sburny von 1983 bis 1986 die Pädagogische Akademie Wien. Anschließend nahm sie von 1990 bis 1992 an einem Hochschullehrgang für Politische Bildung an der Universität für Bildungswissenschaften Klagenfurt teil. Von 1994 bis 1996 besuchte sie den Lehrgang Organisationsentwicklung am interuniversitären Institut für interdisziplinäre Forschung und Fortbildung der Universität Klagenfurt.

Bis 1991 war Sburny als Lehrerin tätig und ist seither in verschiedenen Funktionen innerhalb der Grünen Partei aktiv. Von 1998 bis 2001 und von 2004 bis 2009 war sie Bundesgeschäftsführerin der Grünen. Ihr folgte Stefan Wallner in dieser Funktion nach.[1]
Seit 20. Dezember 2002 war Michaela Sburny Abgeordnete zum Nationalrat, bis Herbst 2006 war sie Wirtschaftssprecherin der Grünen. Nach der Nationalratswahl vom Oktober 2006 wurde sie Vorsitzende des Parlamentsausschusses für Forschung, Technologie und Innovation. Zur vorgezogenen Nationalratswahl 2008 erhielt Sburny von ihrer Partei keinen Platz mehr auf der Wahlliste, sie schied im Herbst 2008 aus dem Nationalrat aus.

Sie lebt heute im niederösterreichischen Langenlois und hat zwei Töchter (geb. 1976 und 1978).
