Tom Holt (* 13. September 1961 in London) ist ein britischer Fantasy-Autor. Er veröffentlicht auch Fantasy unter dem Pseudonym K. J. Parker.[1]
Sein erstes Buch veröffentlichte Holt im Alter von 13 Jahren: Poems by Tom Holt. Während seines Studiums in Oxford begann er, humoristische Fantasy zu schreiben, doch auch gut recherchierte  (aber nie ganz ernsthafte) historische Romane und eine satirische Biographie über Margaret Thatcher (I, Margaret) wurden von ihm veröffentlicht.
Er lebt mit seiner Frau und einer Tochter in Somerset.
