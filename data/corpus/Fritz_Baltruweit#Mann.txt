Friedrich Wilhelm (Fritz) Baltruweit (* 28. Juli 1955 in Gifhorn) ist ein evangelisch-lutherischer Pastor und Liedermacher.

Baltruweit studierte evangelische Theologie und war ab Februar 1984 Pastor an der Stephanus-Kirchengemeinde in Garbsen-Berenbostel. 1992 wurde er Studienleiter am Predigerseminar Loccum. Seit 2001 arbeitet er im Referat für Projekte und Öffentlichkeitsarbeit im Haus kirchlicher Dienste der hannoverschen Landeskirche und zudem seit 2004 im Evangelischen Zentrum für Gottesdienst und Kirchenmusik der Evangelisch-lutherischen Landeskirche Hannovers im Michaeliskloster Hildesheim.  Er war Mitglied der Ökumenischen Textautoren- und Komponisten-Gruppe der Werkgemeinschaft Musik e.V. und der AG Musik in der Ev. Jugend e.V., heute Textautoren- und Komponistengruppe TAKT.

Baltruweit komponierte mehrere hundert Neue Geistliche Lieder. Verschiedene davon fanden Aufnahme im Evangelischen und Mennonitischen Gesangbuch. 
