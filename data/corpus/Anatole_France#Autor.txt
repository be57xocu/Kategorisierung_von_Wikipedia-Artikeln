Anatole France (François Anatole Thibault; * 16. April 1844 in Paris; † 12. Oktober 1924 in Saint-Cyr-sur-Loire) war ein französischer Schriftsteller. 1921 erhielt er den Literaturnobelpreis.

France wuchs als Sohn eines hochgebildeten Buchhändlers auf und beendete 1864 seine Gymnasialzeit am katholischen Pariser Collège Stanislas mit dem baccalauréat. Früh interessierte er sich für Literatur und erarbeitete sich eine profunde humanistische Bildung. 1866 lernte er den Verleger Alphonse Lemerre kennen und wurde bei ihm freiberuflicher Lektor, als der er zum Beispiel eine mehrbändige Anthologie zeitgenössischer Lyrik herausgab. 1876 übernahm er, um für seine Heirat und Familiengründung ein festes Einkommen zu haben, einen Posten als Bibliotheksangestellter, den er 1890 aufgab, nachdem er von seiner Schriftstellerei leben konnte.

Als Autor begann er mit Lyrik im Stil der Dichter des Parnasse, in deren Kreis um Charles Leconte de Lisle er sich ab 1867 bewegte. Er betätigte sich aber früh auch als Erzähler sowie als Literaturkritiker (der zum Beispiel den neuen Symbolismus etwa Mallarmés oder Verlaines zunächst nicht goutierte).

Sein Durchbruch war 1881 der Roman Le Crime de Sylvestre Bonnard, membre de l'Institut (Das Verbrechen Sylvestre Bonnards, Mitglied des Instituts), der mit dem Prix de l'Académie française ausgezeichnet wurde, ihm Zugang zu den Pariser literarischen Salons verschaffte, u.a. dem von Mme de Caillavet, und ihm 1884 das Kreuz der Ehrenlegion eintrug. Le Crime (der Titel ist übrigens ironisch zu verstehen) ist ein rührseliger Roman der in Tagebuchform die Geschichte eines weltfremden älteren Privatgelehrten erzählt, der im zunächst eher zufälligen Einsatz für Hilfsbedürftige, insbesondere die verwaiste Enkelin seiner Jugendliebe, das wirkliche Leben findet.

Gemäß seinem gutbürgerlichen Herkommen vertrat France lange Zeit eine eher konservative Einstellung, so z.B. in dem zur Zeit der Pariser Commune spielenden Roman Les désirs de Jean Servien (1882) oder 1887 in einer negativen Besprechung Émile Zolas. 1888 sympathisierte er kurze Zeit sogar mit dem Chauvinismus von Georges Boulanger, des „Général revanche“. Gegen 1890 rückte er jedoch langsam nach links. Er öffnete sich antiklerikalen und humanitär-sozialistischen Ideen, wobei ihn weniger eine Revolutionierung der Gesellschaft interessierte als die Emanzipation des Individuums von inhumanen materiellen und moralischen Zwängen. Nicht unbeteiligt an seinem Umdenken war vermutlich der biografische Umstand, dass er 1888 ein außereheliches Verhältnis mit Mme de Caillavet begonnen hatte, das ihn 1892 zur Trennung von Frau und Tochter führte.

Ein Zeugnis seines Umdenkens war 1889/90 sein erster historischer Roman, Thaïs. Er erzählt die im kosmopolitischen Alexandria des 4. Jahrhunderts spielende Geschichte eines asketischen christlichen Mönchs, der die heidnische Kurtisane Thaïs zu bekehren versucht, dabei aber selbst zu der Einsicht bekehrt wird, dass der Verzicht auf jegliche Sinnenfreude nicht gottgewollt sein kann (1894 von Jules Massenet als Oper Thaïs vertont).

Antiklerikales und progressistisches Denken zeigt sich auch in dem sehr erfolgreichen Kurzroman La Rôtisserie de la Reine Pédauque (Die Bratküche zur Königin Pedauque) von 1892/93. Es ist ein handlungsreiches Werk im Stil der philosophischen Romane und Erzählungen des 18. Jahrhunderts, das angeblich einem zufällig wiedergefundenen Manuskript dieser Zeit entnommen ist. Hierin berichtet ein pikaresker Ich-Erzähler seine vielfältigen Erlebnisse mit dem sehr unorthodoxen ehemaligen Kirchenmann und Gymnasialprofessor Jérôme Coignard (dessen Figur eines undogmatischen Skeptikers und Freidenkers France im selben Jahr 1893 auch in der satirischen Artikelserie Les opinions de Jérôme Coignard benutzte).

In der Gegenwart dagegen spielt der autobiografisch inspirierte Roman Le lys rouge (Die rote Lilie) von 1894, der die Geschichte der schwierigen Liebe einer Bankiersgattin zu einem Künstler erzählt (1899 zu einem Stück verarbeitet und aufgeführt).

1895 wurde France in seiner Eigenschaft als gemäßigt progressistischer Autor zum Offizier der Ehrenlegion befördert. Am 23. Januar 1896 wurde der vielseitige Literat und glänzende Stilist als Nachfolger des verstorbenen Ferdinand de Lesseps in die Académie française aufgenommen (Fauteuil 38).

Deutlicher Ausdruck seiner ständig weiter nach links driftenden Position ist die Romantetralogie Histoire contemporaine (Zeitgeschichte). Waren die Bände I und II (beide 1897) noch ein satirisches Sittengemälde der von klerikalen und monarchistischen Kräften beherrschten französischen Provinz, so stehen Band III (1899) und vor allem Band IV (1901), dessen Handlung um den Universitätsdozenten Bergeret in Paris spielt, unter dem Eindruck der sich ab Ende 1897 verschärfenden Dreyfus-Affäre. Sie zeigen einen Übergang von der bloßen Gesellschaftskritik aus der Perspektive eines gemäßigt linken Republikaners zum dezidiert linken Engagement eines Sympathisanten der sozialistischen Partei und ihres Führers Jean Jaurès.

Sein neues Engagement manifestierte sich auch 1898 in seinen publizistischen Stellungnahmen zur Dreyfus-Affäre. So unterschrieb er die am 15. Januar 1898 veröffentlichte  Petition in Le Temps, in der die Revision des Fehlurteils gegen Alfred Dreyfus gefordert wurde. Außerdem äußerte er sich zum politisch motivierten Prozess gegen Émile Zola. Es zeigte sich weiterhin in der bissigen Erzählung L'Affaire Crainquebille (1901), wo er schildert, wie ein rücksichtsloser Richter im Verein mit einem autoritären Polizisten einen kleinen Gemüsehändler aburteilt und den so Vorbestraften seiner bürgerlichen Existenz beraubt (1903 zu einem Stück verarbeitet und aufgeführt).

Politisch linke Intentionen verfolgt France auch in der Biografie La Vie de Jeanne d’Arc (1908), wo er die Figur der Johanna von Orleans zu entzaubern versucht, die von der französischen Rechten gerade zur nationalen Ikone stilisiert wurde (Seligsprechung durch den Papst 1909).

Zwei Ausflüge ins Theaterfach mit Noces corinthiennes (1902) und La Comédie de celui qui épouse une muette (1908) blieben eher folgenlos.

Am berühmtesten wurden die Romane L'Île des pingouins (Die Insel der Pinguine) von 1908 und Les dieux ont soif (Die Götter dürsten) von 1912. Ersterer ist ein sarkastischer Abriss der französischen Geschichte von den Anfängen bis in die Gegenwart, verkleidet dargestellt als die Geschichte eines fiktiven Pinguin-Reichs, wobei der Autor dessen Zukunft aufgrund der Habgier und hochmütigen Uneinsichtigkeit der „Pinguine“ sehr pessimistisch beurteilt. Der andere Roman erzählt die Geschichte eines doktrinären Revolutionärs und dessen Mitwirkens an der blutrünstigen Schreckensherrschaft von 1793/94. Es ist ein Aufruf gegen den ideologischen und politischen Fanatismus, der das Frankreich der Zeit polarisierte.

Im Ersten Weltkrieg bezog France, nachdem er anfangs noch als Friedensmahner aufzutreten versucht hatte, eine gemäßigt patriotische Position.

Nach dem Auszug der Kommunisten aus der Sozialistischen Partei Ende 1920 schlug er sich auf ihre Seite und war damit einer der ersten prokommunistischen Intellektuellen von Rang. Parteimitglied wurde er jedoch nicht, und schon 1922 setzte er sich wegen ihrer absoluten Moskau-Hörigkeit vorsichtig von ihnen ab. Zum Ende desselben Jahres durften keine Texte von ihm mehr in parteinahen Journalen gedruckt werden.

1921 erhielt er als vierter französischer Autor den Literatur-Nobelpreis. Vom Vatikan dagegen wurde sein Gesamtwerk 1922 auf den Index Librorum Prohibitorum gesetzt.

Zu seinem 80. Geburtstag 1924 wurde France mit Ehrungen überhäuft und bei seinem Tod noch im selben Jahr mit einem Staatsbegräbnis in Paris ausgezeichnet. Nach den Feiern wurden seine sterblichen Überreste gemäß seinem Wunsch beigesetzt auf dem Alten Friedhof von Neuilly-sur-Seine, wo auch seine Eltern begraben worden waren.

Der Ruhm verblasste jedoch bald, nicht zuletzt, weil France’s Protagonisten auf heutige Leser psychologisch flach und undifferenziert wirken, indem sie oft zu eindeutig das vom Autor Gewollte oder Abgelehnte repräsentieren. Auch wurde er im letzten Lebensjahr durch die prokommunistischen Surrealisten, insbesondere Louis Aragon, als pseudolinker Bourgeois geschmäht, was ihm bei vielen linken Intellektuellen der Zwischenkriegs-, Kriegs- und Nachkriegszeit das Odium eines verkappten Rechten eintrug.

Von heutigen Lesern wird Anatole France vor allem als Romancier und Autor von L'Ile des Pingouins und Les dieux ont soif wertgeschätzt.

« [...] la majestueuse égalité des lois, qui interdit au riche comme au pauvre de coucher sous les ponts, de mendier dans les rues et de voler du pain. »

„[...] unter der majestätischen Gleichheit des Gesetzes, das Reichen wie Armen verbietet, unter Brücken zu schlafen, auf den Straßen zu betteln und Brot zu stehlen.“

Prudhomme (1901) |
Mommsen (1902) |
Bjørnson (1903) |
F. Mistral/Echegaray (1904) |
Sienkiewicz (1905) |
Carducci (1906) |
Kipling (1907) |
Eucken (1908) |
Lagerlöf (1909) |
Heyse (1910) |
Maeterlinck (1911) |
Hauptmann (1912) |
Tagore (1913) |
nicht verliehen (1914) |
Rolland (1915) |
Heidenstam (1916) |
Gjellerup/Pontoppidan (1917) |
nicht verliehen (1918) |
Spitteler (1919) |
Hamsun (1920) |
France (1921) |
Benavente (1922) |
Yeats (1923) |
Reymont (1924) |
Shaw (1925) |
Deledda (1926) |
Bergson (1927) |
Undset (1928) |
Mann (1929) |
Lewis (1930) |
Karlfeldt (1931) |
Galsworthy (1932) |
Bunin (1933) |
Pirandello (1934) |
nicht verliehen (1935) |
O’Neill (1936) |
Martin du Gard (1937) |
Buck (1938) |
Sillanpää (1939) |
nicht verliehen (1940–1943) |
Jensen (1944) |
G. Mistral (1945) |
Hesse (1946) |
Gide (1947) |
Eliot (1948) |
Faulkner (1949) |
Russell (1950) |
Lagerkvist (1951) |
Mauriac (1952) |
Churchill (1953) |
Hemingway (1954) |
Laxness (1955) |
Jiménez (1956) |
Camus (1957) |
Pasternak (1958) |
Quasimodo (1959) |
Perse (1960) |
Andrić (1961) |
Steinbeck (1962) |
Seferis (1963) |
Sartre (1964) |
Scholochow (1965) |
Agnon/Sachs (1966) |
Asturias (1967) |
Kawabata (1968) |
Beckett (1969) |
Solschenizyn (1970) |
Neruda (1971) |
Böll (1972) |
White (1973) |
Johnson/Martinson (1974) |
Montale (1975) |
Bellow (1976) |
Aleixandre (1977) |
Singer (1978) |
Elytis (1979) |
Miłosz (1980) |
Canetti (1981) |
García Márquez (1982) |
Golding (1983) |
Seifert (1984) |
Simon (1985) |
Soyinka (1986) |
Brodsky (1987) |
Mahfuz (1988) |
Cela (1989) |
Paz (1990) |
Gordimer (1991) |
Walcott (1992) |
Morrison (1993) |
Ōe (1994) |
Heaney (1995) |
Szymborska (1996) |
Fo (1997) |
Saramago (1998) |
Grass (1999) |
Gao (2000) |
Naipaul (2001) |
Kertész (2002) |
Coetzee (2003) |
Jelinek (2004) |
Pinter (2005) |
Pamuk (2006) |
Lessing (2007) |
Le Clézio (2008) |
Müller (2009) |
Vargas Llosa (2010) |
Tranströmer (2011) |
Mo (2012) |
Munro (2013) |
Modiano (2014) |
Alexijewitsch (2015) |
Dylan (2016) |
Ishiguro (2017)
