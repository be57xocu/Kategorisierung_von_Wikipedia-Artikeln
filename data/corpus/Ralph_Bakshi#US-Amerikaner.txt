Ralph Bakshi (* 29. Oktober 1938 in Haifa, Palästina, heute Israel) ist ein US-amerikanischer Filmregisseur, der überwiegend Trickfilme für ein Erwachsenenpublikum und gelegentlich andere Spielfilme, zuletzt meist für Fernsehformate, hervorbrachte.

1967 wurde Bakshi Produzent und Direktor der Paramount Cartoon Studios. Sein erster Spielfilm, eine Trickfilmversion des Comics Fritz the Cat, war 1972 ein Kassenschlager. Der Film erlangte Kultstatus, der Autor Robert Crumb distanzierte sich allerdings deutlich von der Verfilmung seines Werkes.

Zwei weitere Erwachsenentrickfilme aus seiner Werkstatt, Heavy Traffic (1973) und Coonskin (1974), die sich der Thematik des schwarzen Amerikas widmeten, bekamen von Filmkritikern viel Lob.

Im Jahr 1977 drehte er die erste Kinofassung von J. R. R. Tolkiens Herrn der Ringe als Zeichentrickfilm im aufwendigen Rotoskopie-Verfahren (engl. rotoscoping): Echte Darsteller wurden gefilmt und dann Bild für Bild überzeichnet. Der Film wurde ein finanzieller Erfolg, fand jedoch unterschiedliche Reaktionen der Filmkritiker.

Bakshi versuchte sich in mehreren Filmen an einer Mischung aus Trickfilm und realen Filmszenen; waren diese in Heavy Traffic visuell noch voneinander getrennt, so agierte Brad Pitt 1992 in Cool World als realer Darsteller innerhalb einer Zeichentrick-Umgebung.

Bakshi arbeitet in den letzten Jahren vorwiegend für das US-Fernsehen und produzierte 1997 eine kurzlebige Fernsehserie namens Spicy City.
