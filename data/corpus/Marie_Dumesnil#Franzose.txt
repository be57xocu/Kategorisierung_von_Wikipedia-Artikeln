Marie Françoise Dumesnil, eigentlich Marie Françoise Marchand (* 2. Januar 1713 in Paris; † 20. Februar 1803 in Boulogne-sur-Mer) war eine französische Schauspielerin.

Marie Dumesnil war die Tochter eines vermögenslosen Edelmanns und zog zuerst mit wandernden Schauspielertruppen in der Provinz umher, bei denen sie Rollen im leichten Genre spielte. 1737 debütierte sie dann als Klytämnestra am Théâtre français in Paris mit großem Erfolg und wirkte seitdem nur in hochtragischen Rollen, wie Medea, Merope, Kleopatra, Athalia, Semiramis, Agrippina etc., wozu sie in hervorragender Weise beanlagt war. 

Sie wurde bereits nach Jahresfrist Societärin des genannten Theaters, zog sich 1776 von der Bühne zurück und starb, 90 Jahre alt, am 20. Februar 1803 in Boulogne-sur-Mer.
