Angelika Barbe (* 26. November 1951 als Angelika Mangoldt in Brandenburg an der Havel) ist eine deutsche Politikerin. Sie war DDR-Oppositionelle, Gründungsmitglied der DDR-SPD, Bundestagsabgeordnete der SPD (1990–1994) und ist seit 1996 CDU-Mitglied.

Angelika Barbe studierte 1970 bis 1974 an der Humboldt-Universität zu Berlin Biologie. Von 1975 bis 1979 war sie als Biologin bei der Hygieneinspektion Berlin-Lichtenberg beschäftigt, danach war sie Hausfrau.

Sie war seit 1986 im Pankower Friedenskreis um Ruth Misselwitz aktiv, war 1987 Mitbegründerin des Johannisthaler Frauenarbeitskreises und engagierte sich 1988/89 im Friedensarbeitskreis um Ulrike Poppe, Jens Reich und Marianne Birthler. Vom DDR-Ministerium für Staatssicherheit wurde sie bis 1989 im Operativen Vorgang „Hysterie“ beobachtet. Sie gehörte zum Kreis derer, die kurz vor dem Ende der DDR die Sozialdemokratische Partei in der DDR (SDP) gründeten.

Nach der Wende wurde sie Mitglied des Parteivorstandes der gesamtdeutschen SPD und gehörte vom 20. Dezember 1990 bis zum 10. November 1994 dem 12. Deutschen Bundestag an. Sie wurde über die Landesliste der Sozialdemokratischen Partei Deutschlands (SPD) in Berlin gewählt. 1995 bis 1998 arbeitete sie als Assistentin des Ärztlichen Leiters des Krankenhauses Prenzlauer Berg. 1996 war sie Mitbegründerin des Berliner Bürgerbüros zur Aufarbeitung von Folgeschäden der SED-Diktatur und trat aus Protest gegen die Zusammenarbeit der SPD mit der PDS mit weiteren DDR-Bürgerrechtlern wie Günter Nooke und Vera Lengsfeld der CDU bei.

Seit 2001 war sie Mitglied des Bundesvorstandes der Dachorganisation Union der Opferverbände kommunistischer Gewaltherrschaft e. V. (UOKG), bis Juli 2007 amtierte sie als stellvertretende Vorsitzende.

Im Jahre 2000 wurde Angelika Barbe als Kandidatin für das Amt des Landesbeauftragten für die Stasiunterlagen in Sachsen vorgeschlagen. Bis zum Frühjahr 2017 war sie bei der Sächsischen Landeszentrale für politische Bildung tätig und befindet sich heute im Ruhestand.

Angelika Barbe ist verheiratet und hat drei Kinder.
