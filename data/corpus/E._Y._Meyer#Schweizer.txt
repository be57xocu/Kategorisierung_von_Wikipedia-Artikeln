E. Y. Meyer (* 11. Oktober 1946 als Peter Meyer in Liestal) ist ein Schweizer Schriftsteller.

Peter Meyer studierte ab 1966 Germanistik, Geschichte und Philosophie an der Universität Bern. Er brach dieses Studium ab und absolvierte eine Ausbildung am Staatlichen Lehrerseminar in Bern. Anschliessend arbeitete er als Primarlehrer in Ittigen. Seit 1974 lebt er als freier Schriftsteller unter dem Namen E. Y. Meyer in Bern.

E. Y. Meyer ist Verfasser von Romanen, Erzählungen, Theaterstücken, Hörspielen und Gedichten. Seine Texte sind stark beeinflusst von seinen philosophischen Studien und der Ablehnung des vorherrschenden Fortschrittsdenkens, dessen Funktion bei der „Wiederherstellung“ der Welt nach Meyers Ansicht Kunst und Kreativität übernehmen sollten.

E. Y. Meyer ist seit 1973 Mitglied des Schweizerischen Schriftstellerinnen- und Schriftstellerverbandes (SSV), heute Autorinnen und Autoren der Schweiz (AdS). 2011 wurde er vom Deutschschweizer  P.E.N.-Zentrum für den Literatur-Nobelpreis vorgeschlagen.[1]