Annette Ramelsberger (* 22. März 1960 in Vilshofen, Niederbayern) ist eine deutsche Journalistin.

Annette Ramelsberger besuchte nach dem Abitur die Deutsche Journalistenschule in München, studierte danach Politik, Jura und Journalistik an der Ludwig-Maximilians-Universität München und schloss als Diplom-Journalistin ab. 

Von 1985 bis 1988 war sie Redakteurin bei der Nachrichtenagentur Associated Press (AP) in Frankfurt, ab 1988 AP-Korrespondentin in der DDR.

1991 wechselte sie zur Berliner Zeitung. Zwei Jahre später zog sie wieder nach München; als Bayern-Korrespondentin berichtete sie für das Nachrichtenmagazin Der Spiegel. Von 1997 an war Annette Ramelsberger im innenpolitischen Ressort der Süddeutschen Zeitung tätig, sie schrieb hauptsächlich Reportagen aus Deutschland. Von Juli 2008 bis Frühling 2010 leitete sie das Ressort Bayern der Süddeutschen Zeitung. Nach einer Umstrukturierung in der Süddeutschen Zeitung war sie neben Peter Fahrenholz und Christian Meyer eine von drei stellvertretenden Leitern in einer von Ulrich Schäfer und Christian Krügel geführten Redaktion, die für München, das Münchner Umland und Bayern zuständig ist. Seit Januar 2012 arbeitet sie als Gerichtsreporterin der Süddeutschen Zeitung.

Seit Februar 2008 ist ihr Buch „Der deutsche Dschihad. Islamistische Terroristen planen den Anschlag“, das beim Econ Verlag erscheint, auf dem Markt.

Für ihren Beitrag „Für eine Abschiebung ist es nie zu spät“ erhielt Ramelsberger den Theodor-Wolff-Preis in der Kategorie Allgemeines. Für ihre Berichterstattung über den NSU-Prozess wurde sie 2014 in der Kategorie „Reporter“ als Journalistin des Jahres ausgezeichnet. Ihre „journalistische Aufarbeitung der ebenso wichtigen wie juristisch zähen Materie“ sei „vorbildlich“ und „echter Qualitätsjournalismus“.[1]