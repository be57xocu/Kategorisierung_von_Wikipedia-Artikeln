Walter Schmidinger (* 28. April 1933 in Linz, Oberösterreich; † 28. September 2013 in Berlin[1]) war ein österreichischer Schauspieler.

Schmidinger absolvierte zunächst eine Lehre als Verkäufer und Dekorateur in einer Tuchhandlung. Ab 1951 begann er seine Ausbildung als Schauspieler am Max-Reinhardt-Seminar in Wien. Sein erstes Engagement erhielt er am dortigen Theater in der Josefstadt. 1954 erhielt er einen Ruf an die Bühnen der Stadt Bonn, dessen Ensemble er bis 1969 angehörte. Danach gehörte Schmidinger für drei Jahre dem Ensemble der Münchner Kammerspiele an. Von dort wechselte er an das Bayerische Staatsschauspiel München. Hier entwickelte er sich zum Publikumsliebling. Er blieb bis 1984 in München, dann ging er zunächst an die Schaubühne in Berlin, ein Jahr später an das Berliner Schiller-Theater, wo er bis zur Schließung im Jahr 1993 auf der Bühne stand und auch als Regisseur wirkte.[2] In den neunziger Jahren des vorigen Jahrhunderts spielte er dann am Deutschen Theater in Berlin. Anschließend gehörte er auch dem Berliner Ensemble an. Schmidinger gastierte darüber hinaus an allen wichtigen deutschsprachigen Bühnen, z.B. dem Deutschen Schauspielhaus Hamburg und dem Burgtheater Wien sowie bei den Salzburger Festspielen.

Seit Anfang der 1970er Jahre spielte Schmidinger auch immer wieder im Fernsehen. So übernahm er Gastrollen im Tatort, Derrick und Der Alte. Auch an einigen Fernsehspielen (z.B. Fast wie im richtigen Leben, Spiel im Schloß, Kir Royal, Opernball) wirkte er mit.

Im Kino begann seine Karriere 1973 mit einer kleinen Nebenrolle in Maximilian Schells Der Fußgänger. Es folgte eine Vielzahl von Filmrollen. Neben Maximilian Schell arbeitete Schmidinger dabei mit Ingmar Bergman, Peter Schamoni, Karin Brandauer und István Szabó zusammen.

Häufig machte Schmidinger auch Lesungen und Rezitationsabende. Dabei beschäftigte er sich z.B. mit Werken von Thomas Bernhard, Joseph Roth, Arthur Schnitzler, Else Lasker-Schüler, Johann Nestroy, Heinrich Heine und Franz Kafka. Einzigartig waren seine Interpretationen von Texten Karl Valentins.

Im Jahr 2001 war Schmidinger Juror des Alfred-Kerr-Darstellerpreises.

Walter Schmidinger starb am 28. September 2013 in Berlin und wurde auf dem Dorotheenstädtischen Friedhof beigesetzt.
Am 1. Dezember 2013 fand im Berliner Ensemble eine Matinee („Mit den Zugvögeln fort…“) zur Erinnerung an den Schauspieler statt. Kollegen und Freunde wie Carmen-Maja Antoni, Robert Wilson, Andrea Eckert, Angela Winkler, Claus Peymann und Meret Becker erinnerten noch einmal an den Verstorbenen.

Für seine Darstellung des Willi in Franz Xaver Kroetz’ Heimarbeit an den Münchner Kammerspielen wurde Schmidinger zum besten Schauspieler des Jahres gewählt. Seither gehörte er zu den Stars des deutschsprachigen Theaters. Er beeindruckte durch ein facettenreiches Spiel und außergewöhnliche schauspielerische Dominanz. Seine graziöse Schwermut machte einen gefährdeten Eindruck.

Besonders zerrissene Figuren und gebrochene Seelen waren seine Domäne. Seine Darstellungen des Malvolio in Shakespeares Was ihr wollt, sein Hamlet, König Lear oder Nathan (in Lessings Nathan der Weise) haben Theatergeschichte geschrieben. Die Presse umjubelte ihn als „eingebildetsten Kranken aller Zeiten“ in Molières Titelrolle. Unvergessen auch sein Salieri in Peter Shaffers Amadeus. In Berlin wurde er für seine Verkörperung des Musikkritikers Reger in Thomas Bernhards Alte Meister mit dem Kritikerpreis der Berliner Zeitung geehrt.

Hellmuth Karasek würdigte Schmidinger im Tagesspiegel als einen „der wenigen Schauspieler, die man, ohne zu zögern, ‚begnadet‘ nennen darf, wobei man sich darüber klar sein muss, dass ‚begnadet‘ immer auch ‚verflucht‘ heißt und bedeutet: Gnade und Fluch sind zwei Seiten der gleichen Medaille […] Große Kunst ist immer auch ein Pakt mit dem Teufel.“

Im Jahr 1976 erlitt Schmidinger bei einem Gastspiel des Münchener Residenztheaters in Schweinfurt wegen eines „Hängers“ in Gerhart Hauptmanns Michael Kramer einen Nervenzusammenbruch und begann die Dekorationen zu zertrümmern. Das Publikum hielt das zunächst für einen Teil der Inszenierung, bis die Kollegen den verwirrten Schauspieler von der Bühne zerrten und der Vorhang fiel.[3]
Im November 2006 erhielt er den Nestroy-Theaterpreis für sein Lebenswerk.
