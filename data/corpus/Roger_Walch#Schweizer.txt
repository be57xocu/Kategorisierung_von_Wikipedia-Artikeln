Roger Walch (* 27. Januar  1965 in St. Gallen) ist ein Schweizer Japanologe, Filmemacher und Publizist.

Er veröffentlichte Beiträge unter anderem in NZZ, Tagblatt, Saiten, Asiatische Studien, Japan Magazin und Kansai Time Out. Sein Themenbereich ist die Japanische Kultur.

Walch studierte Japanologie, Ethnologie und Soziologie an der Universität Zürich. Nach Unterrichtstätigkeiten an der Kantonsschule St.Gallen und am Ostasiatischen Seminar der Universität Zürich leitete er während zwei Jahren das Sankt Galler Programmkino "Kinok" und arbeitete danach als Verlagsleiter und Chefredakteur der Ostschweizer Kulturzeitschrift "Saiten". 

Er besuchte Filmkurse bei den Regisseuren Hans-Ulrich Schlumpf (Zürich) und Kanai Katsu (Image Forum Institut,  Tokio). Seit 1998 lebt Walch in Kyōto (Japan). Nach 11 Jahren Lehrtätigkeit am Goethe-Institut, an der Dōshisha-Universität und an der Städtischen Kunstuniversität Kyōto ist Walch hauptsächlich als Filmemacher und als Publizist aktiv.

Sein Dokumentarfilm "Auch ein grosser Fluss beginnt mit einem Wassertropfen" gewann am internationalen Dokumentarfilmfestival der Expo 2005 in Japan den "Friendship Preis". In ihrer ästhetischen Qualität und surrealen Konzeption erinnern Walchs Filme an die Werke von japanischen Regisseuren wie Seijun Suzuki oder Shūji Terayama. Walch spielt seit seinem neunten Lebensjahr Klavier und komponiert die Musik zu seinen Filmen selbst. 2004 hat er ein dreisprachiges Fotobuch über seine japanische Wahlheimat Kyōto herausgegeben.
