Konrad Bayer (* 17. Dezember 1932 in Wien; † 10. Oktober 1964 ebenda) war ein österreichischer Schriftsteller und Dandy.

Im konservativen Nachkriegsösterreich versuchte Bayer, an die literarische Avantgarde anzuknüpfen und diese wieder zu beleben. Ähnlich wie in Deutschland nach dem Zweiten Weltkrieg (Nachkriegszeit) herrschte in Österreich ein Klima der Verunsicherung darüber, welche Literatur überhaupt zu lesen sei, nachdem im Nationalsozialismus große Teile der Literatur als entartet galten, teilweise auch einfach verschwunden waren und man nun andererseits auch die im Nationalsozialismus propagierte Literatur mied. Man bevorzugte klassische Literatur, da diese am sichersten als unbedenklich erschien.

Avantgardistische Literatur, wie sie Konrad Bayer schrieb, wirkte daher enorm provozierend. Die Provokation war programmatisch, sachlich bestand der avantgardistische und experimentelle Umgang mit Literatur und Sprache in dem Versuch, Sprachroutinen aufzubrechen, sprachlich transportierte Ideologismen aufzudecken und sogar das Bewusstsein auf diese Weise von Denkgewohnheiten zu befreien.

Bayer war befreundet mit Schriftstellern wie Oswald Wiener, Gerhard Rühm, H.C. Artmann und Friedrich Achleitner, die er ab 1951 im Art Club kennengelernt hatte. Von 1954 bis 1960 bildeten sie die Wiener Gruppe. Vor allem der Art Club war Podium für verschiedene Happenings, in denen es – meist ohne vorher abgesprochenes Programm – sehr dadaistisch in erster Linie um die Provokation des Publikums ging. Entsprechend oft gerieten die Veranstaltungen zu Skandalen, bei denen häufig auch die Polizei eingriff.
Freundschaft und musikalische Interessen verbanden ihn mit dem Komponisten Gerhard Lampersberg, mit dem er auch die Literaturzeitschrift „edition 62“ herausbrachte (2 Hefte erschienen).

In vielen Gemeinschaftsarbeiten mit diesen gleichgesinnten Autoren brachte Bayer Lyrik, literarische Montagen und dadaistische Unsinnstexte hervor, die heute vor allem witzig wirken und deren Lektüre ein intellektuelles Vergnügen bereitet. Hinter der Fragmentierung seiner Prosa und seines Weltbilds steht der Wunsch, einen neuen, magischen Zusammenhang in der Wirklichkeit zu entdecken.

Konrad Bayer tötete sich nach einem Besuch bei der Gruppe 47, in der seine präsentierten Werke eine äußerst kritische Aufnahme gefunden hatten. Er ruht in einem ehrenhalber gewidmeten Grab auf dem Hernalser Friedhof (Gruppe 67, Reihe 10, Nummer 11) in Wien.

Im Zentrum des Requiems für einen jungen Dichter von Bernd Alois Zimmermann stehen die Verse Konrad Bayers:

Ernst Bloch beschrieb die Texte Bayers bei einer Lesung der Gruppe 47 als beeindruckend und philosophisch. Sie zeigten eine Heimatlosigkeit auf, aber auch Witz.
Oswald Wiener führte die Wirkung seiner Texte auch auf seine charismatische Persönlichkeit zurück.[2]
Sein aus Zitaten und Assoziationen zum Seefahrer und Entdecker Vitus Bering montierter Roman „der kopf des vitus bering“ wurde vom Verlag Jung und Jung neu aufgelegt.

Uraufführung der meisten Dialoge im Rahmen des ersten und zweiten cabarets der Wiener Gruppe am 6. Dezember 1958 und 15. April 1959

Zusammen mit Gerhard Rühm

Ferry Radax (* 1932) hat drei Filme mit, über und von Konrad Bayer gemacht.
