Theodore Lyman (* 23. November 1874 in Boston, Massachusetts; † 11. Oktober 1954 in Cambridge, Massachusetts) war ein amerikanischer Physiker. 

Lyman studierte in Harvard, wo er ab 1921 als Professor lehrte. 1906 entdeckte er die nach ihm benannte Spektralserie (Lyman-Serie) des Wasserstoffatoms.

In Würdigung seiner Verdienste wurde er 1901 in die American Academy of Arts and Sciences aufgenommen, der er darüber hinaus von 1924 bis 1927 als Präsident vorstand.

Im Jahr 1970 benannte die Internationale Astronomische Union zu seinen Ehren einen Mondkrater nach ihm.

James Bowdoin (1780–1790) |
John Adams (1791–1814) |
Edward Augustus Holyoke (1814–1820) |
John Quincy Adams (1820–1829) |
Nathaniel Bowditch (1829–1838) |
James Jackson (1838–1839) |
Jacob Bigelow (1846–1863) |
Asa Gray (1863–1873) |
Charles Francis Adams (1873–1880) |
Joseph Lovering (1880–1892) |
Josiah Parsons Cooke (1892–1894) |
Alexander Agassiz (1894–1903) |
William Watson Goodwin (1903–1908) |
John Trowbridge (1908–1915) |
Henry Pickering Walcott (1915–1917) |
Charles Pickering Bowditch (1917–1919) |
Theodore William Richards (1919–1921) |
George Foot Moore (1921–1924) |
Theodore Lyman (1924–1927) |
Edwin Bidwell Wilson (1927–1931) |
Jeremiah D. M. Ford (1931–1933) |
George Howard Parker (1933–1935) |
Roscoe Pound (1935–1937) |
Dugald C. Jackson (1937–1939) |
Harlow Shapley (1939–1944) |
Howard Mumford Jones (1944–1951) |
Edwin Herbert Land (1951–1954) |
John Ely Burchard (1954–1957) |
Kirtley F. Mather (1957–1961) |
Hudson Hoagland (1961–1964) |
Paul A. Freund (1964–1967) |
Talcott Parsons (1967–1971) |
Harvey Brooks (1971–1976) |
Victor Weisskopf (1976–1979) |
Milton Katz (1979–1982) |
Herman Feshbach (1982–1986) |
Edmund H. Levi (1986–1989) |
Leo Beranek (1989–1994) |
Jaroslav Pelikan (1994–1997) |
Daniel C. Tosteson (1997–2000) |
James O. Freedman (2000–2001) |
Patricia Meyer Spacks (2001–2006) |
Emilio Bizzi (2006–2009) |
Leslie Cohen Berlowitz (2010–2013) |
Jonathan Fanton (seit 2014)
