Eliza Hansen (* 31. Mai 1909 in Bukarest; † 19. Mai 2001 in Hamburg) war eine deutsche Klavierpädagogin, Pianistin und Cembalistin rumäniendeutscher Abstammung.

Eliza Hansen begann im Alter von zwölf Jahren ihre Ausbildung als Pianistin an der Musikhochschule in Bukarest,
wo sie mit 19 ihr Konzertexamen ablegte. Zwei Jahre später ging sie nach Berlin, wo sie Schülerin von Artur Schnabel und Edwin Fischer war.

Als Pianistin und Cembalistin trat sie in wichtigen Zentren wie London, Barcelona, Madrid und Paris auf. Sie spielte mit führenden Orchestern und Dirigenten und trat mit renommierten Instrumentalisten auf, darunter George Malcolm, David Geringas, Josef Suk und Henryk Szeryng. Es liegen auch mehrere Einspielungen von ihr vor.

Hansen gehörte von 1959 bis 1984 der Hochschule für Musik und Theater Hamburg an und leitete als Professorin eine Klavier- und Cembaloklasse. Zu ihren Schülern zählten Christoph Eschenbach, Justus Frantz, Manfred Fock, Hartmut Leistritz, Marianne Schroeder, Vera Schwarz und Johanna Wiedenbach. 

Eliza Hansen war verheiratet mit dem Pianisten Conrad Hansen.

Zu ihrem 85. Geburtstag verlieh ihr der Hamburger Senat die Biermann-Ratjen-Medaille, um sie für ihre Verdienste um das Kulturleben der Freien und Hansestadt Hamburg zu ehren.
