Karl Schwarzschild (* 9. Oktober 1873 in Frankfurt am Main; † 11. Mai 1916 in Potsdam) war ein deutscher Astronom und Physiker und gilt als einer der Wegbereiter der modernen Astrophysik.

Karl Schwarzschild wurde in Frankfurt als ältestes von sechs Kindern einer wohlhabenden deutsch-jüdischen Familie geboren (ließ sich aber später taufen). Die Familie Schwarzschild-Ochs war eine alteingessenene Händlerdynastie der Textilbranche, Zweig einer alten niederrheinischen 1499 in Frankfurt eingewanderten jüdischen Familie mit einem Geschäft in herausragender Lage am Rossmarkt 13 (vormals Rossmarkt 7) und in der Leipziger Straße. Seine Eltern waren Henrietta Sabel und Moses Martin Schwarzschild. Seine fünf jüngeren Brüder waren Alfred, Otto, Hermann und Robert, sowie seine einzige Schwester Clara. Alfred (1874–1948) wurde Kunstmaler [1]. Karl wuchs in einem kultivierten großbürgerlichen Umfeld auf, in dem vielseitige Interessen (u. a. Musik und Kunst) gepflegt wurden. In Frankfurt besuchte er die jüdische Elementarschule und danach das Städtische Gymnasium, wo frühzeitig sein Interesse an der Astronomie geweckt wurde. Bereits als 16-jähriger Schüler veröffentlichte er in den Astronomischen Nachrichten zwei Arbeiten zur Bahnbestimmung von Planeten und von Doppelsternen.

Nach dem Abitur, das er als Bester seines Jahrgangs bestand, studierte er ab 1890 an der Universität Straßburg Astronomie. 1892 wechselte er an die Ludwig-Maximilians-Universität München, wo er 1896 unter Hugo von Seeliger zum Thema Die Entstehung von Gleichgewichtsfiguren in rotierenden Flüssigkeiten promovierte.

Von 1897 an arbeitete er zwei Jahre als Assistent an der Kuffner-Sternwarte in Wien. Dort beschäftigte er sich mit der Photometrie von Sternhaufen und legte die Grundlagen für eine Formel, die die Beziehung zwischen Intensität des Sternenlichts, Belichtungszeit und Schwärzung der Fotoplatte in der Astrofotografie beschreibt. Ein wichtiges Glied dieser Formel ist der Schwarzschild-Exponent. 1899 kehrte er nach München zurück und habilitierte sich dort.

Von 1901 bis 1909 war Schwarzschild Professor und Direktor der Sternwarte Göttingen. Dort konnte er mit Persönlichkeiten wie David Hilbert und Hermann Minkowski zusammenarbeiten.

1905 wurde er zum ordentlichen Mitglied der Göttinger Akademie der Wissenschaften gewählt.[2] 1909 wurde er Direktor des Astrophysikalischen Observatoriums in Potsdam.

Bei Ausbruch des Ersten Weltkrieges 1914 meldete er sich freiwillig zur Armee. Er diente in der Artillerietruppe an der Ost- und Westfront und hatte dort unter anderem ballistische Berechnungen durchzuführen. Während des Krieges erkrankte er jedoch schwer an einer Autoimmunerkrankung der Haut (Pemphigus vulgaris) und kehrte im März 1916 als Invalide von der Front zurück. Er starb zwei Monate später im Alter von nur 42 Jahren.

Sein Grab und das seiner Familie befindet sich auf dem Stadtfriedhof Göttingen (Abteilung 35).

Karl Schwarzschild war verheiratet mit Elisabeth Rosenbach, einer Urenkelin Friedrich Wöhlers.  Karl Schwarzschild ist der Vater des Astrophysikers Martin Schwarzschild und von Klara Schwarzschild, die den Astrophysiker Robert Emden heiratete.

Während des Kriegsdienstes schrieb er 1915 in Russland eine Abhandlung über die Relativitätstheorie und eine über Quantenphysik.

Seine Arbeit zur Relativität erbrachte die ersten genauen Lösungen der Feldgleichungen der Allgemeinen Relativitätstheorie – eine für nicht rotierende kugelförmige symmetrische Körper und eine für statische isotrope leere Räume um feste Körper.

Schwarzschild leistete einige grundlegende Arbeiten über klassische Schwarze Löcher. Einige Eigenschaften Schwarzer Löcher erhielten deshalb seinen Namen, nämlich die Schwarzschild-Metrik, die Schwarzschild-Tangherlini-Metrik und der Schwarzschildradius. Das Zentrum eines nicht rotierenden, ungeladenenen Schwarzen Loches wird Schwarzschild-Singularität genannt.

In der Astronomie arbeitete er unter anderem über die fotografische Helligkeitsmessung von Sternen. Im Zuge von Studien zum Strahlungstransport in der Sonnenatmosphäre prägte Schwarzschild den Begriff des Strahlungsgleichgewichts.
Mit Methoden der Stellarstatistik untersuchte er die Verteilung der Sterne in der Milchstraße.

Karl Schwarzschild entdeckte 1899 den Schwarzschild-Effekt. Er verbesserte des Weiteren die Theorie optischer Systeme.

Während seiner Zeit am astrophysikalischen Observatoriums in Potsdam beschäftigte er sich auch mit der Erklärung der Emissionsspektren von Atomen. Dabei führte er Methoden der Himmelsmechanik zur Berechnung von Emissionsspektren in das bohr-sommerfeldsche Atommodell ein[3]. So ist seine letzte Publikation ein Beitrag zur frühen Quantenmechanik über den Stark-Effekt.[4]
Der wissenschaftliche Nachlass von Schwarzschild wird in den Spezialsammlungen der Niedersächsischen Staats- und Universitätsbibliothek Göttingen aufbewahrt.

zu optischen Systemen:

zur Helligkeitsmessung:

zur Sonnenatmosphäre:

zu den einsteinschen Feldgleichungen

Im Jahr 1910 wurde Schwarzschild zum Mitglied der Leopoldina gewählt,[5] 1912 wurde er Mitglied der Preußischen Akademie der Wissenschaften.

First Karl Schwarzschild Meeting on Gravitational Physics, held in Frankfurt, Germany to celebrate the 140th anniversary of Schwarzschild's birth. They are grouped into 4 main themes: I. The Life and Work of Karl Schwarzschild; II. Black Holes in Classical General Relativity, Numerical Relativity, Astrophysics, Cosmology, and Alternative Theories of Gravity; III. Black Holes in Quantum Gravity and String Theory; IV. Other Topics in Contemporary Gravitation. Inspired by the foundational principle ``By acknowledging the past, we open a route to the future.

Die Straße an der European Organisation for Astronomical Research in the Southern Hemisphere (ESO) in 85748 Garching bei München trägt seinen Namen, ebenso wie Straßen in Berlin und Göttingen.

Seit der Eröffnung am 19. Oktober 1960 trägt die heutige Thüringer Landessternwarte Tautenburg den Namen Karl-Schwarzschild-Observatorium.

Der Mondkrater Schwarzschild und der Asteroid (837) Schwarzschilda wurden nach Karl Schwarzschild benannt.

„Es ist immer angenehm, über strenge Lösungen einfacher Form zu verfügen.“
