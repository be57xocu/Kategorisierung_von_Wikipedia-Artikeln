Micha Pansi (* 1964 in der Steiermark) ist eine Schweizer Fantasyautorin.

Geboren in der Steiermark und aufgewachsen in Österreich und der Schweiz, lebt sie heute in Zürich. Sie arbeitet als Malerin, Autorin sowie als Kulturredakteurin für eine Wochenzeitung. Nebenbei spielt sie Gitarre bei ihrer Heavy-Metal-Band Bloodstar.

Micha Pansi schreibt an einer neuen Trilogie in der Welt der Daimonen, aber unabhängig und zu einem viel späteren Zeitpunkt spielend als die erste.
