Pierre Mauroy [pjɛʁ mo'ʁwa] (* 5. Juli 1928 in Cartignies, Département Nord; † 7. Juni 2013 in Clamart) war ein französischer sozialistischer Politiker der Parti socialiste (PS). Er war fast 30 Jahre lang Bürgermeister von Lille und gleichzeitig von 1981 bis 1984 Ministerpräsident.

Mauroy wurde als Sohn eines französisch-flämischen Lehrers geboren. Er besuchte das Gymnasium in Cambrai und später die Berufsschule in Cachan. Ab 1952 war Mauroy als Berufsschullehrer in Colombes bei Paris tätig.

Er engagierte sich ab 1944 in der alten sozialistischen Partei Section française de l’Internationale ouvrière (SFIO) und war von 1949 bis 1958 Generalsekretär der Sozialistischen Jugend. 1955 wurde er Generalsekretär der Berufsschullehrergewerkschaft[1]. 1961 wurde er Generalsekretär der Nordföderation der SFIO. Mit der Gründung des PS im Jahr 1971, bei der die SFIO, die Fédération de la Gauche Démocrate et Socialiste und die Parti radical socialiste vereinigt wurden, wurde er Koordinationssekretär in deren Nationalbüro. Schritt für Schritt machte er dann weiter Karriere. Auf Vorschlag von Augustin Laurent erreichte er die zweite Position auf der Wahlliste der sozialistischen Partei bei den Kommunalwahlen 1971 in Lille. Augustin Laurent wurde wiedergewählt, trat zwei Jahre später, am 8. Januar 1973, zurück und Mauroy wurde sein Nachfolger. Am 11. März 1973 wurde er außerdem zum Abgeordneten des Départements Nord in der Nationalversammlung gewählt. 1974 wurde er zum Präsidenten des Regionalrats der Region Nord-Pas-de-Calais gewählt[2]. Von 1979 bis 1980 war er Mitglied des Europäischen Parlaments.

Als wichtige Stütze von François Mitterrand bei der Schaffung der linken Koalition 1981, als überzeugter Europäer und entschiedener Gegner der Zentralisierung Frankreichs wurde Mauroy nach der Wahl von François Mitterrand zum Staatspräsidenten von ihm am 21. Mai 1981 zum Premierminister ernannt. Die erste Koalitionsregierung Mauroys, die nach der gewonnenen Wahl zur Nationalversammlung gebildet wurde, schloss vier Minister der kommunistischen Partei (PCF) ein. In Erfüllung der Wahlversprechen des Präsidenten präsentierte Mauroy ein soziales Sofortprogramm und erste Schritte eines ambitionierten Reformprogramms: Einführung der 39-Stunden-Woche, fünf Wochen bezahlten Jahresurlaub, Einstellung zusätzlicher Beamter, Dezentralisierung der Verwaltung, Verstaatlichung von Großunternehmen (wie Bull Computer, Rhône-Poulenc, Dassault, Sacilor, Usinor und Thomson) und Banken (Crédit Lyonnais, Compagnie financière de Suez), Vermögenssteuererhöhungen, Erhöhung der Einkommen, Abschaffung verschiedener Sicherheitsgesetze, Rente mit 60 Jahren, Abschaffung der Todesstrafe, Reform der Medien, Schwangerschaftsabbruch (gegen den Rat des Präsidenten).

Wie auch in Westdeutschland und Großbritannien infolge der Ölkrise stiegen in Frankreich die Inflationsrate und die Arbeitslosigkeit. Dazu kam eine Währungskrise, die Mauroy bereits ein Jahr später zwang, eine restriktive Fiskalpolitik (politique de l'austérité) zu ergreifen, personifiziert durch den Finanzminister Jacques Delors. Die Indexierung der Einkommen in Relation zum Preisniveau wurde aufgegeben, wodurch ein Sinken der Realeinkommen möglich wurde, die private Berufsausbildung wurde zurückgestellt. Diese Fiskalpolitik wurde von vielen Wählern der Linken abgelehnt. Die kommunistischen Minister schieden dann 1983 nach zunehmenden Auseinandersetzungen aus der Regierung aus. Am 22. März 1983 beauftragte Mitterrand erneut Mauroy mit der Regierungsbildung seiner dritten Regierung. Zwei Wochen später, am 6. April 1983, kündigte Mauroy in seiner Regierungserklärung Notverordnungen zur Durchsetzung eines drastischen Spar- und Restriktionsprogramms ein. Insbesondere die kommunistische Gewerkschaft CGT organisierte daraufhin landesweite Streiks. Am 18. Juli 1984 entschied Staatspräsident Mitterrand, Mauroy durch Laurent Fabius als Ministerpräsidenten zu ersetzen.

Mauroy kehrte daraufhin in den Norden zurück, wo er großes politisches Gewicht besaß. 1988 wurde er Vorsitzender (Erster Sekretär) der PS. Er gab diesen Posten 1992 auf, als er zum Senator gewählt wurde. Vom 17. September 1992 bis 1999 war Mauroy Nachfolger Willy Brandts als Präsident der Sozialistischen Internationale. 2001 wurde Jacques Delors' Tochter Martine Aubry seine Nachfolgerin als Bürgermeisterin von Lille. Sie war bis dahin seine „rechte Hand“ gewesen.

Am 7. Juni 2013 starb Mauroy im Militärkrankenhaus Percy in Clamart.[3] Bereits 14 Tage später benannte die Metropolregion Lille das Grand Stade Lille Métropole in Villeneuve-d’Ascq in Stade Pierre-Mauroy um.[4]
Michel Debré |
Georges Pompidou |
Maurice Couve de Murville |
Jacques Chaban-Delmas |
Pierre Messmer |
Jacques Chirac |
Raymond Barre |
Pierre Mauroy |
Laurent Fabius |
Jacques Chirac |
Michel Rocard |
Édith Cresson |
Pierre Bérégovoy |
Édouard Balladur |
Alain Juppé |
Lionel Jospin |
Jean-Pierre Raffarin |
Dominique de Villepin |
François Fillon |
Jean-Marc Ayrault |
Manuel Valls |
Bernard Cazeneuve |
Édouard Philippe

Morgan Phillips (1951–1957) |
Alsing Andersen (1957–1962) |
Erich Ollenhauer (1963) |
Bruno Pittermann (1964–1976) |
Willy Brandt (1976–1992) |
Pierre Mauroy (1992–1999) |
António Guterres (1999–2005) |
Giorgos A. Papandreou (seit 2006)
