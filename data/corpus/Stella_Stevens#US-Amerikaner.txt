Stella Stevens; eigentlich Estelle Caro Eggleston (* 1. Oktober 1938 in Yazoo City, Mississippi), ist eine US-amerikanische Schauspielerin.

Stella Stevens wurde in Yazoo City (mitunter wird auch Hot Coffee, Mississippi, genannt) als Estelle Caro Eggleston geboren. Im Rahmen einer Modeltätigkeit in Memphis wurde sie als Schauspielerin entdeckt und erhielt einen Vertrag bei 20th Century Fox. Es folgten einige kleinere und mittlere Rollen in Fernseh- und Filmproduktionen, bis ihr ein Playboy-Auftritt als Playmate of the Month im Januar 1960 zu zahlreichen Filmangeboten verhalf.

Dabei war sie meist auf den Typus der sexbetonten Blondine festgelegt, wie etwa in der Dr. Jekyll & Mr. Hyde-Parodie Der verrückte Professor  von Jerry Lewis und in dem Katastrophenfilm Die Höllenfahrt der Poseidon. Ihr schauspielerisches Talent stellte sie auch in dem Horrorthriller Das Haus der blutigen Hände neben Shelley Winters unter Beweis.

Erfolge hatte sie auch mit dem vielschichtigen Spät-Western Abgerechnet wird zum Schluss von Sam Peckinpah und in der Südstaatenserie Flamingo Road. Daneben spielte sie vor allem in Fernsehfilmen und -serien wie General Hospital und California Clan. 1960 wurde sie mit einem Golden Globe Award als Beste Nachwuchsdarstellerin ausgezeichnet.

Von 1954 bis 1957 war sie mit Noble Herman Stephens verheiratet, mit dem sie einen Sohn, den Schauspieler Andrew Stevens, hat.[1]