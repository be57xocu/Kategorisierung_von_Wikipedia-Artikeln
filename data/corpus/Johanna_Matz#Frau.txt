Johanna („Hannerl“) Matz (* 5. Oktober 1932 in Wien) ist eine österreichische  Kammerschauspielerin. 

Bereits als Vierjährige erhielt sie Ballettunterricht bei Toni Birkmeyer. Von 1940 bis 1948 besuchte sie die Hochschule für Musik und Darstellende Kunst in Wien. 1950 absolvierte sie das Max-Reinhardt-Seminar. Bei der Abschlussaufführung wurde sie von Berthold Viertel entdeckt und für das Burgtheater engagiert. Sie blieb dem Haus als Mitglied bis 1993 verbunden. 

1951 wurde sie von Ernst Marischka für den Film entdeckt und hatte im selben Jahr ihr Filmdebüt. 1953 wurde sie von dem Regisseur Otto Preminger nach Hollywood geholt, wo sie in der deutschen Fassung von The Moon is Blue (Die Jungfrau auf dem Dach) spielte. Seit Ende der 1960er Jahre war die beliebte Schauspielerin vor allem für das österreichische Fernsehen tätig.

Johanna Matz verkörperte unvergleichlich den Typus des „Wiener Mädels“.

Sie war mit dem Schauspieler Karl Hackenberg verheiratet. Heute lebt Johanna Matz zurückgezogen in Wien und Unterach am Attersee.
