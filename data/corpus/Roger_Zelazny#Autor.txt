Roger Zelazny (* 13. Mai 1937 in Cleveland, Ohio; † 14. Juni 1995 in Santa Fe, New Mexico) war ein US-amerikanischer Autor von Fantasy- und Science-Fiction-Kurzgeschichten und Romanen. Zelaznys Werke wurden mehrfach ausgezeichnet; so gewann er mit dreien seiner Storys den Nebula Award, sechsmal den Hugo Award und zweimal den Locus Award.[1]
Roger Zelazny wuchs in Euclid, Ohio, auf und studierte zunächst Psychologie, dann englische Literatur an der Columbia-Universität, wo er 1962 mit einem MA abschloss (Elisabethanisches und Jakobinisches Theater). Von 1960 bis 1963 war er bei der Nationalgarde von Ohio, bis 1966 in der Reserve der US-Armee in der Abschussmannschaft einer Nike-Batterie und bei einer Einheit zur psychologischen Kriegsführung tätig. Bis 1969 arbeitete er in Sozialämtern in Cleveland, Ohio, und Baltimore, Maryland.[2][3]
1967–68 war er in der SFWA Schatzmeister, ehe er 1969 hauptberuflicher Schriftsteller wurde. Von 1975 bis zu seinem Tod lebte er in Santa Fe (New Mexico).

Zelazny begann schon früh zu schreiben – erste Veröffentlichung in einer College-Zeitschrift stammen von 1954[4] –
und verfasste zahlreiche Kurzgeschichten, ehe sich auch der längeren Form zuwandte. Zunächst schrieb er nur Lyrik und wechselte ab 1962 zur Science Fiction.[5] Seine erste erfolgreiche Veröffentlichung war die im The Magazine of Fantasy and Science Fiction im Jahr 1963 erschienene Geschichte A Rose for Ecclesiastes (dt. Die 2224 Tänze von Locar), die 1963 für den Hugo Award nominiert wurde. 1966 wurden This Immortal (dt. Fluch der Unsterblichkeit), 1968 Lord of Light (dt. Herr des Lichts), 1976 Home Is the Hangman (dt. Der Henker ist heimgekehrt), 1982 Unicorn Variation (dt. Die Einhorn-Variante), 1986 24 Views of Mt. Fuji, by Hokusai (dt. 24 Ansichten des Berges Fuji, von Hokusai) und 1987 Permafrost (dt. Leopard am Kilimandscharo) mit dem Hugo Award ausgezeichnet.

Mit seine bekanntesten Geschichten bilden die Romanserie Die Chroniken von Amber. Er schrieb zeit seines Lebens weiterhin Gedichte und nahm 1965 mit der Erzählung He Who Shapes (Herr der Träume) den Cyberspace vorweg. Zusammen mit Samuel R. Delany, Thomas Disch, Harlan Ellison und John T. Sladek bildete Zelazny den amerikanischen Ableger der New Wave in der Science-Fiction-Literatur.[5]
Zelazny benutzte auch das Pseudonym Harrison Denmark.

In den ersten fünf Bänden, dem ursprünglichen Amber-Zyklus, wird die Geschichte des Corwin von Amber erzählt. (Die Übersetzungen ins Deutsche stammen von Thomas Schlück.)

In den später erschienenen fünf Bänden ist Merlin (Merle Corey), der Sohn von Corwin und Dara, die Hauptperson. (Die Übersetzungen ins Deutsche stammen von Irene Bonhorst.) 

Eine 10-teilige Serie mehrerer SF- und Fantasy-Autoren …

Der Roman Damnation Alley wurde 1977 unter Regie von Jack Smight verfilmt; der Film lief 1978 als Straße der Verdammnis in den deutschen Kinos.

„Zelazny läßt eine hochtechnisierte Zukunft zum Leben erwachen, in der Selbstmord die vorrangigste Todesursache geworden ist. Er präsentiert Tiere mit dämmernder Intelligenz … Zelaznys zudem schlüssige Charakterisierungen und farbige Landschaften ummanteln eine Handlung,  die für sich genommen möglicherweise nur einen einzigen Kerngedanken aufweist, nämlich die Übersetzung der heutigen Tiefenpsychologie ins Morgen, diesen aber im Zusammenspiel mit der gelungenen ästhetischen Aufbereitung zu einer atmosphärischen Einheit verschmilzt … aus welchen Gründen auch immer – ein Klassiker bleibt dieses Buch allemal.“
