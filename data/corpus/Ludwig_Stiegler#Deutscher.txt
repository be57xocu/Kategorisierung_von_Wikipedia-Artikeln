Ludwig Stiegler (* 9. April 1944 in Parsberg, Oberpfalz) ist ein deutscher Rechtsanwalt und Politiker der SPD.

Stiegler war von Juli bis Oktober 2002 Vorsitzender der SPD-Bundestagsfraktion und, mit Ausnahme dieser Zeit, von 1998 bis 2007 einer ihrer stellvertretenden Vorsitzenden. In den Jahren 2003 bis 2009 war Stiegler überdies Vorsitzender der SPD Bayern.

Ludwig Stiegler wurde als Sohn von Ludwig und Walburga Stiegler aus Vilshofen 1944 in Parsberg geboren. Der Vater war Kalkwerker bei der Maxhütte. Ludwig Stiegler wuchs in Vilshofen bei Rieden in der Oberpfalz auf. Nach der Volksschule in Vilshofen besuchte Stiegler ab 1956 das Progymnasium der Claretiner in Weißenhorn, ehe er 1960 auf das Erasmus-Gymnasium in Amberg wechselte. Nach dem Abitur 1964 verpflichtete sich Stiegler zunächst als Zeitsoldat bei der Bundeswehr. 1967 schied er als Leutnant der Reserve aus. Er begann dann ein Studium der Rechtswissenschaft, Soziologie und Politik in München und Bonn, das er 1973 mit dem Ersten und 1976 mit dem Zweiten Juristischen Staatsexamen beendete. Seit 1979 ist er als Rechtsanwalt am Oberlandesgericht Köln zugelassen.[1]
Ludwig Stiegler war mit Barbara Stiegler verheiratet, hat drei Kinder und lebte in Weiden. Seit 2003 ist er mit Literaturwissenschaftlerin Birgit Göbel-Stiegler verheiratet, der Lebensmittelpunkt hat sich nach Berlin verlagert. [2]
1960 trat Ludwig Stiegler zunächst in den JU-Ortsverband Vilshofen ein, 1964 wechselte Stiegler in die SPD. 1999 bis 2007 gehörte er dem Parteivorstand, 2005 bis 2007 auch dem Präsidium der SPD an. Von 1985 bis 1993 und von 1997 bis 2004 war er stellvertretender Landesvorsitzender der SPD in Bayern und von 2003 bis 2009 Landesvorsitzender, bis 2004 zunächst geschäftsführend. Von 1982 bis 1997 war er Vorsitzender des SPD-Regionalbezirks Oberpfalz. Stiegler war außerdem bis 2009 Vorsitzender des SPD-Unterbezirks Weiden-Neustadt-Tirschenreuth.

Seit 1980 war er Mitglied des Deutschen Bundestages. Von 1987 bis 1992 und von 1994 bis 2005 war er Vorsitzender der Bayerischen SPD-Landesgruppe im Deutschen Bundestag. Seit 1998 war er Mitglied im Vorstand der SPD-Bundestagsfraktion. Von Juli bis Oktober 2002 war er dann Vorsitzender der SPD-Bundestagsfraktion. Ab Oktober 2002 bis 2007 war er einer ihrer Stellvertretenden Vorsitzenden.

Ludwig Stiegler zog stets über die Landesliste Bayern in den Bundestag ein. Sein Wahlkreis war Weiden.

Seine Mitgliedschaft endete mit dem 16. Deutschen Bundestag im Jahr 2009. Stiegler hatte nicht mehr für die 17. Wahlperiode kandidiert.  

Im Juli 2002 äußerte er, die Oppositionsparteien CDU und FDP hätten im Rahmen der Debatte um das NPD-Verbot nicht das Recht, den Bundesinnenminister Otto Schily zu kritisieren, da ihre Vorgängerparteien schließlich Adolf Hitler zur Macht verholfen hätten. 

Im Juli 2005 geriet er bundesweit mit einem Nazi-Vergleich in die Kritik, als er äußerte, dass ihm zum CDU-Programm zur Bundestagswahl („Vorfahrt für Arbeit“) nur das Motto der nationalsozialistischen Konzentrationslager „Arbeit macht frei“ einfallen würde.

Kurt Schumacher |
Erich Ollenhauer |
Fritz Erler |
Helmut Schmidt |
Herbert Wehner |
Hans-Jochen Vogel |
Hans-Ulrich Klose |
Rudolf Scharping |
Peter Struck |
Ludwig Stiegler |
Franz Müntefering |
Peter Struck |
Frank-Walter Steinmeier |
Thomas Oppermann |
Andrea Nahles

Georg von Vollmar (1894–1918) |
Erhard Auer (1918–1933) |
Lisa Albrecht (1946) |
Wilhelm Hoegner (1946–1947) |
Waldemar von Knoeringen (1947–1963) |
Volkmar Gabert (1963–1972) |
Hans-Jochen Vogel (1972–1977) |
Helmut Rothemund (1977–1985) |
Rudolf Schöfberger (1985–1991) |
Renate Schmidt (1991–2000) |
Wolfgang Hoderlein (2000–2003) |
Ludwig Stiegler (2003–2009) |
Florian Pronold (2009–2017) |
Natascha Kohnen (seit 2017)
