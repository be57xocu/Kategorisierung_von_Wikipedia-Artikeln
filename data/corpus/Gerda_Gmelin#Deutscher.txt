Gerda Gmelin (* 23. Juni 1919 in Braunschweig; † 14. April 2003 in Hamburg) war eine deutsche Schauspielerin und Theaterintendantin.

Gmelin war die Tochter des Schauspielers und Theatergründers Helmuth Gmelin und die Nichte des Schriftstellers Otto Gmelin. Nach der Mittleren Reife besuchte sie von 1937 bis 1939 die Schauspielschule im Hamburger Schauspielhaus. Ihr erstes Engagement erhielt sie am Theater Koblenz, an das sie nach einer kriegsbedingten Unterbrechung 1950 zurückkehrte.

1955 ging sie an das Hamburger Theater im Zimmer, das ihr Vater 1947 gegründet hatte. Nach ihres Vaters Tod im Oktober 1959 übernahm sie die Leitung der Bühne. Bis zur Schließung 1999 leitete sie das Theater als Direktorin.[1]
Neben der Schauspielerei war sie Regisseurin, Dramaturgin und Lehrerin. Bekannt wurde Gmelin auch durch zahlreiche Produktionen im Fernsehen, wie in Die Unverbesserlichen, in insgesamt neun Filmen der Tatortreihe, Pappa ante Portas und dem bekannten Sketch Kosakenzipfel von und mit Loriot. Eine prägnante Rolle hatte sie 1988 in Die Bertinis, einer Romanverfilmung nach Ralph Giordano und die längste Serienrolle war Berta Rogalla in der Vorabendserie Der Landarzt.

Sie wurde für ihre künstlerischen Verdienste um die Stadt Hamburg vom Senat der Freien und Hansestadt mit der Biermann-Ratjen-Medaille geehrt. 1988 erhielt sie den Ehrenpreis Silberne Maske der Hamburger Volksbühne. Über ihr Privatleben ist nur bekannt, dass sie alleinerziehende Mutter zweier Söhne war.[2]
Ihr Grab befindet sich im Garten der Frauen auf dem Friedhof Ohlsdorf in Hamburg.[3]