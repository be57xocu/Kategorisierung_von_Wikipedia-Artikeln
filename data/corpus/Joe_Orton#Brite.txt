Joe Orton (* 1. Januar 1933 in Leicester; † 9. August 1967 in London) war ein britischer Dramatiker.

Orton entstammte dem Arbeitermilieu. Von 1950 bis 1953 studierte er an der Royal Academy of Dramatic Art in London. Er bestritt seinen Lebensunterhalt unter anderem als Aktmodell. 1963 verbüßte er zusammen mit seinem Lebensgefährten Kenneth Halliwell eine Gefängnisstrafe für den Diebstahl von Bibliothekseigentum und dessen Besudelung mit Obszönitäten.

Orton hatte vor seinem internationalen Erfolg auf dem Theater Erzählungen geschrieben, ohne zu Lebzeiten Veröffentlichungen zu erreichen. In den ersten Jahren war Halliwell eine Art Lehrer für Orton, der nur eine sehr oberflächliche Bildung besaß, und half diesem bei der Entwicklung seines Schreibstils, den man später „Ortonesque“ nennen sollte.[1][2]
Halliwell ertrug den alleinigen Erfolg seines Lebenspartners nicht.[3] In einem Abschiedsbrief bezog sich Halliwell auch auf den letzten Abschnitt von Ortons Tagebuch, in dem dieser eine Vielzahl sexueller Abenteuer schildert. Halliwell erschlug Orton am 9. August 1967 mit neun Hammerschlägen und nahm sich mit Tabletten das Leben. Beide wurden am nächsten Tag von einem Chauffeur gefunden, der Orton zu einem Treffen mit den Beatles bringen sollte, für die er ein Drehbuch geschrieben hatte.[4]
Orton, der in seinen wenigen Stücken häufig Tabus der Zeit brach, schrieb nur über einen Zeitraum von drei Jahren für das Theater.

Stephen Frears verfilmte 1987 das Leben von Joe Orton unter dem Titel Das stürmische Leben des Joe Orton (engl. Prick Up Your Ears) mit Gary Oldman in der Rolle von Joe Orton und Alfred Molina als Halliwell.[5] Der Film beruht auf der gleichnamigen Biographie von John Lahr.[6]
Alle Romane wurden posthum veröffentlicht.
