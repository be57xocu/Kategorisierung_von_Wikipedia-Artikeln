Thomas Gsella (* 19. Januar 1958 in Essen) ist ein deutscher Satiriker und Schriftsteller.

Thomas Gsella war von 1992 bis 2008 Redakteur und von Oktober 2005 bis zu seinem Ausscheiden Chefredakteur der Frankfurter Satirezeitschrift Titanic. Er schreibt für verschiedene Magazine, Zeitungen und Rundfunksender. Gsella ist vor allem als Verfasser von komischer Lyrik hervorgetreten. Er ist Mitglied der Titanic Boy Group (zusammen mit Oliver Maria Schmitt und Martin Sonneborn), mit der er neben seinen vielfältigen Soloprogrammen auf Lesereise geht.

Thomas Gsella lebt mit Frau und zwei Kindern in Aschaffenburg.
