Claude Carliez (* 10. Januar 1925 in Nancy; † 17. Mai 2015 in Saint-Mandé[1][2]) war ein französischer Stuntman und Schauspieler.

Claude Carliez hat in Deutschland oft als Nebendarsteller, der dann auch Stuntrollen übernahm, in bekannten Filmen mitgespielt. Weiterhin führte er Regie beim Film Le Paria.

Carliez war Vorsitzender der Académie d’Armes de France.[3]