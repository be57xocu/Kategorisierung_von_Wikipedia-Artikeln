George Ames Plimpton (* 18. März 1927 in New York City; † 25. September 2003 ebenda) war ein US-amerikanischer Schriftsteller und vor allem ein Förderer junger Schriftsteller.

Plimpton war das älteste von vier Kindern der Eltern Francis T. Plimpton und Pauline Ames Plimpton. Er hatte zwei Brüder und eine Schwester. Er ging in New Hampshire auf die Phillips Exeter Academy und schrieb für die Schulzeitung. Nach seinem Schulabschluss 1944 ging er nach Harvard, studierte dort Englisch und arbeitete nebenbei für den Harvard Lampoon.

Von 1945 bis 1948 diente Plimpton in der United States Army, davon einige Zeit als Panzerfahrer in Italien. Er wurde in der Armee zum Lieutenant befördert. Nach dem Austritt aus der Armee studierte er am King's College in Cambridge weiter und machte dort 1952 seinen B.A. und zwei Jahre später den M.A.

Nachdem Plimpton 1952 Paris besucht hatte, diskutierte er dort mit einigen Freunden, darunter Peter Matthiessen, über die Gründung einer Literaturzeitschrift, die 1953 auch als Paris Review verwirklicht wurde und dessen (ehrenamtlicher) Chef-Redakteur Plimpton wurde. Er behielt diese Tätigkeit bis zuletzt bei. In den Jahren 1956 bis 1958 war er Lehrer am Barnard College und 1959 boxte der sportbegeisterte Plimpton mit dem damaligen Halbschwergewichts-Champion Archie Moore.

In dem Dokumentarfilm When We Were Kings - Einst waren wir Könige (Regie: Leon Gast, 1997  Oscar für den besten Dokumentarfilm) kommentierte Plimpton gemeinsam mit Norman Mailer den historischen Boxkampf Rumble in the Jungle (deutsch: „Der Kampf im Dschungel“), zwischen dem damaligen Schwergewichts-Weltmeister George Foreman und dem Ex-Weltmeister Muhammad Ali, der am 30. Oktober 1974 in Kinshasa stattfand.

Plimpton starb mit 76 Jahren in New York an einem Herzinfarkt. Der Asteroid (7932) Plimpton wurde am 7. Juni 2009 nach ihm benannt.
