Max Landa (eigentlich: Max Landau; * 24. April 1873 in Minsk[1], Russisches Kaiserreich; † 9. November 1933 in Bled, Jugoslawien) war ein österreichischer Bühnen- sowie Stummfilmschauspieler.

Landa erhielt seine Schauspielausbildung bei Karl Arnau. Er debütierte 1899 an verschiedenen Wanderbühnen, war dann in Hannover, von wo aus er an das Deutsche Theater in Berlin und schließlich als erster Held und Liebhaber an das Breslauer Theater verpflichtet wurde, wo er auch das Sommertheater leitete. Zuletzt spielte er an verschiedenen Berliner Bühnen, u. a. am Lessing-Theater, Deutschen Künstlertheater sowie am Kleinen Theater (am Berliner Südwestkorso).[1]
Er begann seine Karriere als Filmschauspieler 1911. 1913 agierte er in Die Suffragette an der Seite von Asta Nielsen, seiner Entdeckerin für den Stummfilm[2], und drehte danach mit ihr noch weitere sechs Filme unter der Regie von Urban Gad. 1915/16 war er unter Joe May in der Titelrolle von dessen Joe-Deebs-Reihe, einer beliebten Detektivserie, zu sehen.

Ewald André Dupont drehte 1918/19 eine Max-Landa-Serie mit insgesamt 12 Filmen. 1921 stand Max Landa noch einmal mit Asta Nielsen für Die Geliebte Roswolskys vor der Kamera, 1928 hatte er seinen letzten Filmauftritt.

Von 1921 bis zum Ende der Stummfilmzeit war Landa auch als selbständiger Filmproduzent tätig.[2]
Landa und seine Frau Margot Walter flohen nach der Machtübergabe an die Nationalsozialisten 1933 aus dem Deutschen Reich. Am 9. November 1933 nahm sich Max Landa, ein Opfer der veränderten Verhältnisse[3], das Leben.
