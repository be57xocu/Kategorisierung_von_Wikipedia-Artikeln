Franz Hegi (* 16. April 1774 in Lausanne; † 14. März 1850 in Zürich) war ein Schweizer Maler und Kupferstecher.

Die Familie von Franz Hegi lebte bei seiner Geburt in Lausanne, woher seine Mutter kam, Eléonore Verdeil. Sein Vater Johannes Hegi war Goldschmied, Juwelier und Kupferstecher. 1780 zog die Familie nach Zürich. Hier lernte der junge Franz Hegi beim Maler Matthias Pfenninger (1739–1813) die Aquatintatechnik, die zwischen 1765 und 1768 erfunden worden war.

Hegi wurde für seine unzähligen Kupferstiche und Aquatinta für Neujahrsblätter, Almanache und Bücher bekannt. Seine Bilder sind heute eine wertvolle Quelle für die Geschichte der Schweiz des späten 18. und frühen 19. Jahrhunderts.

Das Oberdorftor

Das Augustinertor und der Fröschengraben

Das Rennwegtor

Das Grendel- oder Seetor

Blick auf Köln rheinabwärts

Sankt Goar mit Ruine Rheinfels
