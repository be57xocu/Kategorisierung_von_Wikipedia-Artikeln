Carlos Thompson (* 7. Juni 1923 in Buenos Aires, Argentinien; † 10. Oktober 1990 ebenda; mit bürgerlichem Namen Juan Carlos Mundin Schaffter) war ein argentinischer Schauspieler und Schriftsteller.

Der Sohn deutsch-schweizerischer Einwanderer begann seine Karriere 16-jährig als Schauspieler beim Film 1939 in Argentinien mit Carlos F. Borcosque Werk Y mañana serán hombres (Männer von morgen). Er trat danach auch in Stücken von Giraudoux, Lope de Vega, Anouilh und anderen auf. Nach einigen Rollen trat er einen Studienaufenthalt in New York an und studierte danach einige Semester an der Universität von Buenos Aires.

Später unternahm er eine Weltreise über die Vereinigten Staaten, Kanada, Afrika und Kleinasien. Schließlich kehrte er zum argentinischen Film zurück. Daneben ging er auch seinen anderen künstlerischen Neigungen als Bildhauer, Maler und Schriftsteller nach.

In Argentinien erschienen ein Band mit Kurzgeschichten unter dem Titel All is God und der preisgekrönte Roman The Other Cheek und The Night and the Sun.

Thompson startete nach seiner Arbeit beim Film endgültig eine zweite Karriere als Schriftsteller und Historiker. Er unternahm eine detaillierte, auf eigenen Recherchen basierende Widerlegung der von David Irving (Accident. The Death of General Sikorski, 1967) und Rolf Hochhuth (Theaterstück Soldaten, deutsche Uraufführung 1967) aufgestellten Behauptung, Winston Churchill habe General Sikorski, den Chef der polnischen Exilregierung, ermorden lassen, indem er dessen Flugzeug sabotieren ließ. Sikorski kam am 4. Juli 1943 in Gibraltar bei einem Absturz kurz nach dem Start ums Leben.

Thompson heiratete am 21. September 1957 die Schauspielerin Lilli Palmer. Er motivierte sie, ebenfalls literarisch tätig zu sein, womit sie auf dem deutschen Markt großen Erfolg hatte. Vier Jahre nach dem Tod seiner Ehefrau nahm sich Thompson 1990 in Buenos Aires das Leben. Beigesetzt wurde er auf dem Cementerio de la Chacarita in Buenos Aires.
