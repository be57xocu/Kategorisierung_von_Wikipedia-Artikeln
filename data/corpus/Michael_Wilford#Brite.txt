Michael Wilford, CBE, FRSA (* 9. September 1938 in Hartfield, East Sussex) ist ein britischer Architekt.

Wilford war über viele Jahre hinweg Partner von James Stirling (ab 1960). 1971 gründeten sie das gemeinsame Büro James Stirling, Michael Wilford and Associates, das bis zu Stirlings Tod 1992 existierte. Von 2002 bis 2013 war Wilford zusammen mit Manuel Schupp und Stephan Gerstner Geschäftsführer von Wilford Schupp Architekten, Stuttgart.[1] Wilford Schupp Architekten wurde Mitte 2015 in das Architekturbüro Orange Blu Building Solutions (unter Leitung von Manuel Schupp, Peter Vorbeck und Siggi Wernik) überführt.

Ein für Wilford typisches Bauwerk ist die Britische Botschaft in Berlin.

Wilford ist Commander of the British Empire (C.B.E.), Mitglied des Royal Institute of British Architects, des Singapore Institute of Architects, des Royal Institute of Arbitrators und Fellow of Royal Society of Arts. Im Bund Deutscher Architekten ist er Ehrenmitglied. Er unterrichtet Architektur an Hochschulen in Australien, Großbritannien, Kanada und den Vereinigten Staaten.
