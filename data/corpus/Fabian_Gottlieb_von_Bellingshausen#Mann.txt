Fabian Gottlieb Thaddeus von Bellingshausen (russisch Фаддей Фаддеевич Беллинсгаузен, Faddei Faddejewitsch Bellinsgausen; * 9. Septemberjul./ 20. September 1778greg. Gut Lahhetagge (estnisch: Lahetaguse, heute Landgemeinde Saaremaa) auf Ösel (estnisch: Saaremaa); † 13. Januarjul./ 25. Januar 1852greg. in Kronstadt) war ein russischer Seefahrer und Offizier deutschbaltischer Herkunft.

Geboren auf der estnischen Insel Ösel (heute Saaremaa) als Spross einer deutschbaltischen Adelsfamilie, begann Bellingshausen bereits 1789 mit 11 Jahren seine militärische Laufbahn als Kadett an der Marine-Kadettenschule in Kronstadt. Im Jahr 1796 machte er seine erste Seereise nach England. 1797 trat er als Fähnrich zur See in die russische Flotte ein. 1803 bis 1806 diente er auf dem Schiff Nadeschda und nahm an der ersten russischen Weltumseglung unter A. J. von Krusenstern teil. Nach der Reise wurde er zum Kapitänleutnant befördert und war Kommandant verschiedener Schiffe der russischen Baltischen Flotte und der Schwarzmeerflotte.

1819 wurde er mit der Leitung der vom russischen Zaren Alexander I. initiierten ersten russischen Expedition in die Südpolarregion beauftragt. Als Kapitän der Korvette Wostok (935 t) brach er zusammen mit dem Versorgungsschiff Mirny (530 t, Kapitän: Michail Lasarew) im August in Kronstadt auf. Auf der Hinreise wurden zunächst die polynesischen Tuamotuinseln und die Macquarie-Insel kartografiert. In den 751 Tagen der Reise entdeckte die Expedition 29 neue Inseln im Pazifik und Atlantik. Den Ruf, Entdecker der Antarktis zu sein, erlangte Bellingshausen, weil er am 28. Januar 1820 erstmals den Rand eines „Eis-Kontinents“ sichtete. Bellingshausen hatte das Schelfeis beschrieben, das als Teil des antarktischen Kontinents betrachtet werden kann.[1] Im Verlaufe der eigentlichen Unternehmung wurde dann sechsmal der südliche Polarkreis überquert und die Antarktis umsegelt. Es war nach den Forschungsreisen von James Cook die zweite Expedition überhaupt, die so weit nach Süden vorstieß. Im August 1820 wurde der 70. Breitengrad erreicht und dabei das antarktische Festland auf einem südlicheren Kurs umsegelt als von Cooks Expedition.

1821 entdeckte die Expedition die der Antarktischen Halbinsel vorgelagerte Alexander-I.-Insel und die Peter-I.-Insel. Die Alexanderinsel hielt Bellingshausen für einen Teil des antarktischen Festlands. Dieser Irrtum wurde erst im Jahr 1940 bemerkt und korrigiert.

Im August 1821 kehrten die Schiffe der Expedition über den Atlantik nach Kronstadt zurück.

Nach der Rückkehr wurde Bellingshausen zum Kapitän zur See befördert und zum Verbandschef in der Baltischen Flotte ernannt. 1828 zum Vizeadmiral ernannt, nahm er bis 1829 an der Belagerung der Festung von Warna während des Russisch-türkischen Krieges von 1828 bis 1829 teil. 1839 erfolgte die Ernennung zum Kriegsgouverneur und Hafenkommandant von Kronstadt. 1843 wurde Bellingshausen zum Admiral befördert.

Die Beschreibung seiner Südpolar-Expedition erschien 1831 in russischer Sprache in Sankt Petersburg. Eine deutsche Übersetzung wurde erst 1902 veröffentlicht.

Nach ihm wurden zahlreiche geografische Objekte insbesondere in der Antarktis benannt:

1. Geographische Objekte

2. Astronomische Objekte:

3. Technische Objekte:
