Leif Panduro (* 18. April 1923 in Frederiksberg; † 16. Januar 1977 in Kopenhagen) war ein dänischer Schriftsteller und Zahnarzt.

Panduro studierte zwischen 1942 und 1946 Zahnmedizin in Kopenhagen und praktizierte nach Abschluss seines Studiums 1947 bis 1962 als Zahnarzt. Sein vielfältiges Werk als Schriftsteller ist von der Psychoanalyse stark beeinflusst. 1976 wurde er Mitglied der Dänischen Akademie (Det Danske Akademi).
