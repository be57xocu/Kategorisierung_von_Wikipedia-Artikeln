Ramón Antonio Pérez Almorós (* 25. Januar 1922 in Madrid; † 30. November 1977 ebenda) war ein spanischer Schauspieler.

Almorós studierte am Konservatorium in Madrid und schloss sich dann der Theatertruppe von María Basso und Enrique Navárro an. Ab 1947 spielte er in zahlreichen Filmen und Fernsehsendungen.[1] In Deutschland wurde er vor allem durch seine Mitwirkung im Film Das Vermächtnis des Inka bekannt.
