Ed Rush, bürgerlich Ben Settle, (* 1973 in London) ist ein britischer Drum-and-Bass-DJ und -Produzent aus London.

Im Sog der aufblühenden Rave Szene startete er seine ersten, im Hardcore-Bereich anzusiedelnden Produktionen, Anfang der 1990er Jahre in seinem Studio auf dem Speicher. In der Szene bekannt wurde er mit dem Hardcore Jungle-Hit Bludclot Artattack.

Der Produzent Nico gehörte zu den ersten, die Rush eine Plattform boten, um seine Musik zu veröffentlichen. Das erste Album ("Torque"), das 1996 auf No U Turn erschien, entstand in Zusammenarbeit mit den DJs und Produzenten Trace und Fierce. Geboren war der moderne Techstep: "...massive Wände aus verzerrten Basslines mit futuristisch technoiden Soundsamples, unterlegt mit dem für diese Musik charakteristischen Two Step Drumpattern."

Danach veröffentlichte er einige 12-inches auf verschiedenen Labels wie Goldies Metalheadz und Grooveriders Prototype.

Über Dominic Angas, besser bekannt als Dom & Roland, und dessen Schulfreund Jamie Quinn (Matrix) kommt der Kontakt mit dessen älterem Bruder Matt Quinn (Optical) zustande. Zusammen gründen sie Virus Recordings, auf dem 1998 das Debüt Wormhole erscheint. Das Album erreicht sogar die Top 40 der UK Albumcharts und wird bei den "Annual Knowledge D&B Awards" des englischen Knowledge Magazins als Best Artist Album 1999 ausgezeichnet.
