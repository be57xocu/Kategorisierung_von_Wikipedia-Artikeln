Charles Reznikoff (* 30. August 1894 in Brooklyn, New York; † 22. Januar 1976) war ein US-amerikanischer Poet.

Der Sohn jüdisch-russischer Einwanderer wuchs in Brooklyn auf und fing schon in jungen Jahren an zu schreiben. Er fand in seinem Viertel Freunde, mit denen er über die Werke junger Autoren wie Arthur William Symons diskutierte. Seine eigenen, frühen Gedichte sind geprägt vom Imagismus, seine Prosa lässt den Einfluss von James Joyce erkennen.

Sein Studium der Geschichte gab er schnell zugunsten eines Jurastudiums auf, auch wenn er zeitlebens ein starkes Interesse an der Geschichte hatte. Er wurde Rechtsanwalt, gab diesen Beruf aber nach wenigen Wochen ebenfalls wieder auf.

Reznikoff druckte und verlegte seine Werke selbst, kümmerte sich aber nicht um deren Verbreitung.
