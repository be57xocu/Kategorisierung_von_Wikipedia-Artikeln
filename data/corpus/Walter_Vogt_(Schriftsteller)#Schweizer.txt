Walter Vogt (* 31. Juli 1927 in Zürich; † 21. September 1988 in Muri bei Bern) war ein Schweizer Schriftsteller und Psychiater.

Walter Vogt ging in Bern zur Schule und arbeitete nach seinem Medizinstudium in Bern, das er 1956 mit der Promotion zum Doktor der Medizin abschloss, an der Universität Zürich zunächst als Röntgenarzt an einem Krankenhaus in Bern. Ende der 1960er Jahre liess er sich zum Psychiater ausbilden. Anschliessend führte er eine Facharztpraxis in Muri bei Bern. Vogt war verheiratet und hatte drei Kinder. 1978 war er erster "Swiss writer in residence" an der University of Southern California, Los Angeles. Er starb 1988 an Herzversagen.

Walter Vogt begann nach einer Krankheit 1961 schriftstellerisch tätig zu werden[1] und ab Mitte der 1960er Jahre eigene literarische Texte zu veröffentlichen. Mit seinem ersten Roman Wüthrich, einer fulminanten und bitteren Satire auf Ärzteschaft und Krankenhauswesen, erzielte er einen Skandalerfolg. Viele von Vogts späteren Werken sind autobiografisch gefärbt. Themen wie die eigene Drogenabhängigkeit und Bisexualität, zu denen er sich in den 1980er Jahren bekannte, durchziehen sein gesamtes Werk, seien es Kriminalromane nach Dürrenmatt'schem Vorbild, exotische Reisereportage oder fantastische Prosaerzählungen.

Walter Vogt war Gründungsmitglied der Gruppe Olten – von 1976 bis 1980 deren Präsident – und gehörte dem Deutschschweizer PEN-Zentrum an. Sein Nachlass wird im Schweizerischen Literaturarchiv aufbewahrt.

Eine zehnbändige Werkausgabe wurde 1991–97 im Nagel & Kimche Verlag, Zürich, herausgegeben:
