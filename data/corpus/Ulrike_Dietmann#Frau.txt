Ulrike Dietmann (* 30. Januar 1961) ist eine deutsche Autorin und Seminarleiterin. 

Dietmann studierte Theaterwissenschaft, Philosophie und Publizistik an der Freien Universität Berlin und Szenisches Schreiben an der Universität der Künste Berlin. Sie gründete und leitete mehrere Jahre die Berliner Theatergruppe „Magnetische Felder“, die sich am Theaterkonzept Antonin Artauds orientierte. 1987 wurde ihr erstes Theaterstück „Heloise und Abelard“ in Mannheim uraufgeführt. Es folgten zahlreiche Theaterstücke, Hörspiele, Romane und Sachbücher. 2008 gründete sie die Pegasus-Schreibschule, die Seminare, Workshops und eine Ausbildung zum Romanautor anbietet. 2006 veröffentlichte sie ihren ersten Pferderoman „Moonwalker – Pferd der Freiheit“ im Loewe Verlag. Seither ist die Beziehung zwischen Pferd und Mensch Thema ihrer Romane und Sachbücher. Seit 2008 ist sie als Seminarleiterin im pferdegestützten Erfahrungslernen international tätig. 2010 absolvierte sie die Ausbildung zum Eponaquest Instructor bei Linda Kohanov. Ein Schwerpunkt ihrer Arbeit ist das Modell der „Heldenreise“ nach Joseph Campbell. Ulrike Dietmann lebt im Raum Stuttgart.

Hörspiele

Theaterstücke

Übersetzungen

Sachbuch

Autobiografie

Prosa
