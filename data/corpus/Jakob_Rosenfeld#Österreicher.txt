Jakob Rosenfeld, auch General Luo, chinesisch 羅生特 / 罗生特, Pinyin Luó Shēngtè (* 11. Januar 1903 in Lemberg, Österreich-Ungarn; † 22. April 1952 in Tel Aviv, Israel) war ein Arzt, der von den Nationalsozialisten aus Österreich vertrieben wurde. In seiner neuen Heimat China avancierte er bis zum Gesundheitsminister.

Jakob Rosenfeld verbrachte den größten Teil seiner Jugend in Wöllersdorf in Niederösterreich, wohin die Familie 1910 übersiedelt war.

Er besuchte die Kaiserjubiläumsschule in Wöllersdorf und anschließend das Staatsgymnasium in Wiener Neustadt.[1] 1921 übersiedelte Rosenfeld nach Wien, um Medizin zu studieren. 1928 promovierte er zum Dr. med. univ. und spezialisierte sich auf die Fächer Urologie und Gynäkologie. Er arbeitete in Wien zunächst als Turnusarzt und Assistent am Rothschuld-Spital. Mit seiner Schwester eröffnete er eine Praxis in Wien. Nach dem „Anschluss“ Österreichs 1938 wurde Rosenfeld zuerst im KZ Dachau und später im KZ Buchenwald interniert. Im Juni 1939 wurde er mit der Auflage freigelassen, das Land binnen 14 Tagen zu verlassen.

Da China für jüdische Zwangsemigranten in Shanghai kein Visum verlangte, floh er 1939 mit dem Schiff und begann dort eine Tätigkeit als niedergelassener Chirurg.

In einem Café, welches unter Exil-Österreichern sehr beliebt war, traf er durch einen Freund 1941 einen chinesischen Armeearzt und schloss sich, als überzeugter Antifaschist, der kommunistischen Neuen Vierten Armee und 1942 der Kommunistischen Partei Chinas an. Diese kämpfte dort gegen die japanischen Invasoren und im chinesischen Bürgerkrieg gegen die von Chiang Kai-shek geführte Armee der Kuomintang. Rosenfeld wurde in Maos Roter Armee in die Provinz Shandong geschickt, wo er der Leibarzt des Marschalls Luo Ronghuan, des Kommandanten der 8. Feldarmee, wurde.

1945 wurde Rosenfeld zum Leiter des Gesundheitswesens der 1. Armee in der Mandschurei ernannt; er war als General einer Sanitätsbrigade für die medizinische Versorgung verantwortlich. Er diente unter anderem unter Chinas späterem Präsidenten Liu Shaoqi. 1947 wurde er Gesundheitsminister in Maos provisorischer Regierung[2].

Aufgrund seiner guten Arbeiten als Armeearzt bekam er von den kommunistischen Soldaten den Spitznamen "Tigerbalsamarzt", da Tigerbalsam als Heilmittel galt.

1949 nach dem Einmarsch der Kommunisten in Peking kehrte er über Shanghai nach Wien zurück, seine Verwandten in Österreich waren aber alle ausgewandert oder, wie auch seine Mutter, in Konzentrationslagern ermordet worden. Eine geplante Rückkehr nach China stellte sich in dieser Zeit als sehr schwierig heraus, deshalb emigrierte Rosenfeld 1951 nach Israel, wo er 1952 acht Monate nach seiner Ankunft an Herzversagen starb.

Im Frühjahr 1944 bewirkte er den Bau des Rosenfeld Spitals, welches nach seiner Zerstörung durch japanische Bomber 1946 wiedererbaut wurde. Zu seinen Ehren wurde vor dem Spital eine Statue für ihn aufgestellt.
Heute wird Jakob Rosenfeld in China als Nationalheld gefeiert. An seinem 100. Geburtstag wurde er mit einer großen Ausstellung in Peking gewürdigt.
