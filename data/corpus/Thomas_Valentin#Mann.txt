Thomas Valentin (* 13. Januar 1922 als Armin Gerold Valentin in Weilburg; † 22. Dezember 1980 in Lippstadt) war ein deutscher Schriftsteller.
Sein Vater war Otto Valentin aus Gießen, seine Mutter eine geb. Lina Gelbert aus Löhnberg.

Valentin lebte bis 1925 mit seinen Eltern in Weilburgs Nachbargemeinde Löhnberg. Die Familie verzog 1925 nach Weilburg, wo er die Volksschule besuchte und 1932 bis 1936 das Gymnasium Philippinum. Ab 1936 ging er auf das Gymnasium in Dillenburg, wo er 1940 das Abitur ablegte. Im Anschluss daran studierte er Literaturwissenschaft, Geschichte,
Philosophie und Psychologie in Gießen und München. Ab 1947 war er fünfzehn Jahre lang Lehrer in Lippstadt in Westfalen; von 1955 bis 1958 leitete er ehrenamtlich die dortige Volkshochschule.

Nach dem Erfolg seines ersten 1961 veröffentlichten Romans Hölle für Kinder arbeitete er seit 1962 als freier Schriftsteller mit Wohnsitz in Lippstadt; die Sommermonate verbrachte er häufig in Italien am Gardasee und auf Sizilien. Von 1964 bis 1966 war er Chefdramaturg am Stadttheater in Bremen. Er war Mitglied des PEN-Zentrums der Bundesrepublik Deutschland. 1981 wurde ihm postum der Adolf-Grimme-Preis mit Gold für das Drehbuch zum Fernsehspiel Grabbes letzter Sommer verliehen (zusammen mit Sohrab Shahid Saless (Regie) und Wilfried Grimpe (Darsteller)).

Thomas Valentin hat zeitkritische Romane, Erzählungen, Theaterstücke und Drehbücher verfasst. Sie schildern häufig Probleme von Kindern und Jugendlichen. Den Roman Die Unberatenen verfilmte Peter Zadek 1966 als TV-Inszenierung sowie 1969 als Kinofilm unter dem Titel Ich bin ein Elefant, Madame; der Film wurde auf der Berlinale 1969 mit einem Silbernen Bären ausgezeichnet.

Die Stadt Lippstadt, auch Sitz der 1996 gegründeten Thomas-Valentin-Gesellschaft e.V., vergibt seit 1993 alle vier Jahre den Thomas-Valentin-Literaturpreis. Die dortige Stadtbücherei trägt seit 1997 seinen Namen.
