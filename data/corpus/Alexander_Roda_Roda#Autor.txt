Alexander Friedrich Ladislaus Roda Roda (* 13. April 1872 in Drnowitz, Mähren als Sándor Friedrich Rosenfeld; † 20. August 1945 in New York) war ein österreichischer Schriftsteller und Publizist.

Roda Roda wuchs in Puszta Zdenci, heute Grubišno Polje (Kroatien), auf. Dort war sein Vater Leopold Rosenfeld als Gutsverwalter tätig. Die Familie nannte sich inoffiziell Roda (serb./kroat. für Storch), um nicht mit dem jüdischen Familiennamen Rosenfeld Anstoß zu erregen. Zusammen mit seiner drei Jahre jüngeren Schwester Marie (Mi) schrieb er Romane. Die Geschwister vereinbarten: „Über allem soll als Verfassername stehen: A. M. Roda Roda – zum Zeichen, dass wir ein Doppelwesen sind.“

Nach dem Abbruch eines Studiums der Rechtswissenschaften an der Universität Wien verpflichtete sich Roda Roda zu einem zwölfjährigen Militärdienst und begann ihn am 1. Oktober 1893 beim Korpsartillerieregiment in Agram (Zagreb). Im Jahr 1894 ließ er sich nach Esseg (Osijek) versetzen und katholisch taufen, hier entstanden seine Slavonischen Dorfgeschichten. Im Jahr 1899 wurde sein Familienname standesamtlich in Roda geändert, 1906 in Roda Roda.

Im Jahr 1900 erschienen erste Arbeiten von ihm im Simplicissimus. Nach mehreren Disziplinarstrafen ließ sich Roda Roda 1901 als Oberleutnant in die Reserve versetzen. Er verstärkte seine literarische Tätigkeit. Im Jahr 1902 verarbeitete er eine kurze, leidenschaftliche Liebesaffäre mit der Star-Schauspielerin Adele Sandrock, die zehn Jahre älter war als er, in dem Theaterstück Dana Petrowitsch.

Er unternahm Reisen über den Balkan, nach Italien und Spanien. Im Jahr 1904 machte er Station in Berlin und 1906 in München, wohin er 1920 zurückkehrte. Wegen diverser Verstöße gegen die Offiziersehre wurde Roda Roda 1907 unter Aberkennung seines Ranges aus der Armee entlassen. Aus dem leuchtend roten Rockfutter seiner Uniform ließ er sich eine Weste schneidern und trug sie bei nahezu allen seinen zahlreichen Auftritten auf Kleinkunstbühnen. Sie wurde für Jahrzehnte sein Markenzeichen.

Im Jahr 1909 wurde die zusammen mit Carl Rössler geschriebene Militärkomödie Der Feldherrnhügel uraufgeführt, in der militärische Engstirnigkeit und sinnentleerte Hierarchie verspottet werden.

Am 11. August 1914 rückte Roda Roda als Kriegsberichterstatter für die Neue Freie Presse ein und schrieb für sie bis 1917 mehr als 700 Beiträge. Auch für die seit 1854 in Budapest erscheinende deutschsprachige Zeitung Pester Lloyd verfasste er mehrere Beiträge. In den 1920er-Jahren hatte Roda Roda mit humoristischen Buchveröffentlichungen großen Erfolg. Er trat in Kabaretts auf, unternahm ausgedehnte (Gastspiel-)Reisen und pflegte Kontakte zu Dutzenden Autoren, Schauspielern, Filmemachern und anderen Künstlern.

Im Jahr 1932, zum 60. Geburtstag Roda Rodas, erschien eine dreibändige Werkausgabe. Am 10. Mai 1932 gehörte er zu der Gruppe demokratischer Intellektueller, die Carl von Ossietzky bei Antritt seiner Haftstrafe in Berlin demonstrativ begleiteten. Nach Hitlers Machtergreifung in Deutschland 1933 übersiedelte Roda Roda nach Graz und reiste 1938 wenige Tage vor dem Anschluss Österreichs an das Deutsche Reich in die Schweiz aus. Am 1. November 1940 forderten ihn die Schweizer Behörden auf, bis zum Jahresende das Land zu verlassen, und untersagten ihm zugleich jede Tätigkeit für schweizerische Medien. Roda Roda emigrierte in die USA. Dort blieben die Bemühungen des mittlerweile Siebzigjährigen um einen schriftstellerischen Broterwerb ohne größeren Erfolg.

Seine Schwester Gisela Januszewska, die nicht emigrieren konnte, wurde nach Theresienstadt deportiert und dort ermordet.[1]
Am 20. August 1945 starb Roda Roda 73-jährig in New York an Leukämie.

Er verband sich im September 1905 in freier Ehe mit Elsbeth Anna Freifrau von Zeppelin geb. Leuckfeld von Weysen (1882–1960).[2] Ihre Tochter Dana (1909–1990) heiratete 1933 den Schriftsteller Ulrich Becher. Deren Sohn Martin Roda Becher (geb. 1944) lebt als Schriftsteller in Basel.

Sein ehrenhalber gewidmetes Grab (Abteilung 2, Ring 1, Gruppe 2, Nummer 31) befindet sich im Urnenhain der Feuerhalle Simmering. Im Jahr 1952 wurde in Wien Floridsdorf (21. Bezirk) die Roda-Roda-Gasse nach ihm benannt. In der Stadt Osijek in Kroatien steht eine Büste Roda Rodas vor dem Bibliotheksgebäude in der Europska Avenija.

Roda Roda war auch ein leidenschaftlicher Schachspieler und spielte oft im Münchener Schachcafé Stefanie. Hier wurde er zu seiner Schachhumoreske Das Pensionistengambit inspiriert.

Sein Werk besteht großteils aus humoristischen bzw. satirischen Erzählungen und Romanen, in welchen er in liebevoll-nachsichtiger Weise die Schwächen und Kuriositäten der Donaumonarchie und besonders des k.u.k. Offizierskorps aufs Korn nahm.[3]
Verzeichnis aller Werke siehe Wikisource

Drehbuch

Literarische Vorlage

TV

12-teilige Serie des ORF in Zusammenarbeit mit dem ZDF, der die Serie in 25 Teilen unter dem Namen Roda Roda Geschichten gesendet hat. Mit Peter Weck als Alexander Roda Roda.
