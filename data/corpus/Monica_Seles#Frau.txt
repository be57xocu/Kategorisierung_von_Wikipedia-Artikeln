Monica Seles (serbisch Monika Seleš/Моника Селеш, ungarisch Szeles Mónika; * 2. Dezember 1973 in Novi Sad, SFR Jugoslawien) ist eine ehemalige jugoslawische und US-amerikanische Tennisspielerin. Sie besitzt seit 2007 auch die ungarische Staatsbürgerschaft.[1] Seles gewann 53 Turniere im Einzel, darunter neun Grand-Slam-Turniere, und sechs Turniere im Doppel. Monica Seles, deren Karriere 1993 durch ein Attentat in Hamburg überschattet wurde, zählt zu den besten und erfolgreichsten Tennisspielerinnen aller Zeiten.

Die 1,78 Meter große Linkshänderin benutzte sowohl ihre Vorhand als auch ihre Rückhand beidhändig. Noch ungewöhnlicher ist, dass sie nicht umgriff, wenn sie die Seiten wechselte. Angesichts ihrer beidseitig beidhändigen Spielweise war es Seles möglich, extreme Winkel zu spielen, womit das gegnerische Spielfeld weit geöffnet wird. Die beidhändige Spielweise sorgte auch für eine stark verdeckte Schlagausführung, sodass die Antizipation der Schläge schwerfiel. Eines ihrer Markenzeichen waren ihre kräftigen Lautäußerungen bei harten Schlägen. Dieses Charakteristikum hatte wiederholt für Kontroversen mit Kontrahentinnen (unter anderem Nathalie Tauziat, Martina Navrátilová, Katerina Maleewa) gesorgt, die sie aufforderten, dies während des Wettkampfs zu unterlassen.

Monica Seles kommt aus einer Familie ungarischer Abstammung. Mit sechs Jahren begann sie, angeleitet von Vater Károly, mit dem Tennissport. Das erste Turnier gewann sie mit neun Jahren. Aus dieser Zeit ist von ihr selbst die Anekdote überliefert, dass sie, die noch nicht in der Lage war, bei Wettkampfspielen den Spielstand selbst zu erfassen, von Zeit zu Zeit ihrem Vater zugerufen habe: „Gewinne ich?“

1985, mit elf Jahren, gewann Seles die Orange Bowl in Miami, eines der weltweit bedeutendsten Nachwuchsturniere. Dabei erweckte das beidhändig spielende Mädchen das Interesse des US-amerikanischen Tenniscoachs Nick Bollettieri. 1986 zog Familie Seles in die Vereinigten Staaten, wo Monica für zwei Jahre unter Bolletieri an seiner Akademie ihr Spiel weiter verbesserte. Bolletieri war von ihrer ungewöhnlichen Spielweise von Anfang an überzeugt. So ließ er eigens für sie einen von hohen Wänden umgebenen Platz errichten, um so ungestört, geschützt vor neugierigen Blicken der potenziellen Konkurrenz, Seles’ Powerspiel zu entwickeln.

Wie gut diese Taktik aufging, erwies sich bereits zwei Jahre später, als Seles im Alter von 15 Jahren Profispielerin wurde. Im Mai desselben Jahres gewann sie in Houston ihren ersten großen Titel, als sie im Finale die Weltranglistendritte Chris Evert schlug. Für viele gilt Seles als die eigentliche Begründerin des modernen Power-Tennis.

Einen Monat später erreichte sie bei ihrem ersten Grand-Slam-Turnier gleich das Halbfinale der French Open; hier traf sie auf die Weltranglistenerste Steffi Graf. Seles beendete ihr erstes Profijahr auf dem sechsten Platz in der Weltrangliste.

Das Jahr 1990 brachte für Seles den Durchbruch. Bereits im Mai bezwang sie bei den German Open in Berlin erstmals Steffi Graf. Diese dominierte seit Jahren das Welttennis wie kaum eine andere Spielerin zuvor und war in 66 aufeinander folgenden Partien unbesiegt. Zwei Wochen später gelang Seles die Wiederholung des Triumphs. Bei den French Open besiegte sie in einem umkämpften Finale erneut die Weltranglistenerste, die im Tiebreak des ersten Satzes vier Satzbälle nicht hatte verwerten können. Seles gewann in zwei Sätzen. Mit 16 Jahren und 6 Monaten ist sie bis heute die jüngste French-Open-Siegerin aller Zeiten.

Das Jahr 1991 war das erste von zwei Jahren, in denen Seles das Welttennis dominierte. Sie begann im Januar mit dem Gewinn der Australian Open, wo sie bei ihrer ersten Teilnahme im Finale Jana Novotná schlug. Im März gelang es ihr, Steffi Graf als Weltranglistenerste zu entthronen. Im Juni verteidigte sie ihren French-Open-Titel gegen Arantxa Sánchez Vicario, die Siegerin von 1989. Während der Wimbledon Championships nahm sich Seles überraschend eine sechswöchige Auszeit. Im September erreichte sie dann auf Anhieb das Finale ihrer ersten US Open, in dem sie Martina Navrátilová besiegte.

Auch das Jahr 1992 verlief ähnlich erfolgreich: Seles konnte ihre drei Grand-Slam-Titel verteidigen. Einen Höhepunkt bildete das Endspiel in Roland Garros, wo sie erneut der wieder erstarkten Graf gegenüberstand und diese in einem legendären Match mit 10:8 im dritten Satz niederrang. Seles erreichte auch erstmals das Wimbledon-Finale, unterlag auf Rasen aber deutlich ihrer Vorgängerin; Graf siegte mit 6:2 und 6:1.

Zwischen Januar 1991 bis Februar 1993 gewann Seles 22 Turniere und erreichte 33 Mal das Finale – bei insgesamt 34 gespielten Turnieren. Sie kam dabei auf die Bilanz von 159 Siegen bei 12 Niederlagen (92,9 %). Bei Grand-Slam-Turnieren stand ihre Quote bei 55 Siegen und einer Niederlage. Innerhalb der ersten vier Jahre auf der Tour (1989–1992) erreichte sie bei 30 Turniersiegen eine Siegquote von 231:25 Spielen (90,2 %). Nur Chris Evert kann mit 91,4 % und 34 Turniersiegen zwischen 1971 und 1974 auf einen besseren Karrierestart zurückblicken.

Seles ging als stärkste Spielerin in das Jahr 1993. Im Januar startete sie erneut mit einem Titelgewinn bei den Australian Open. Sie schlug Graf in drei Sätzen und verbuchte in Down Under ihren dritten Titel in Folge.

Die ständige Rivalität zwischen Steffi Graf und Monica Seles zwischen 1989 und 1993 gehört zu den großen Kapiteln des Tennissports. Die meist hart umkämpften Duelle der beiden in ihrer Persönlichkeit und ihrem Spielsystem so unterschiedlichen Athletinnen markierten Höhepunkte in beiden Karrieren und führten das Damentennis auf ein neues Niveau.

In ihren Auseinandersetzungen trafen nicht nur sehr unterschiedliche Persönlichkeiten aufeinander: Hier die introvertierte stille Graf, die den Medienrummel scheute und sich anfangs jedes Lächeln verkniff. Dort die extrovertierte Seles, die ihre Gegnerinnen zur Begrüßung als 15-Jährige mit Rosensträußen beehrte, um sie dann vom Platz zu fegen. Auch die so unterschiedlichen Spielsysteme nährten die große Rivalität: Hier die schnörkellos spielende, schnelle Graf mit ihrer gewaltigen Vorhand und der den Ball tief eingrabenden unterschnittenen Rückhand. Dort Monica Seles, die extrem stöhnend jeden Ball mit maximaler Härte spielte und die Gegnerinnen mit raffiniertem Winkelspiel aus dem Platz zwang. Der absolute Anspruch beider, die Beste zu sein, machte die Aufeinandertreffen der beiden Dominatorinnen zu emotional aufgeladenen Sternstunden des Tennissports.

Bis zum Erscheinen der jungen Monica Seles hatte Graf das Welttennis beherrscht und als nahezu unbesiegbar gegolten. Dies hatte sich 1990 im zweiten Jahr von Seles auf der Tour geändert, in der Graf erstmals unterlag und in den Duellen sieglos blieb. 1991, als es bei Graf insgesamt kriselte und sie für ihre Verhältnisse auch ungewöhnlich oft gegen andere Spielerinnen verlor, hatte Seles die Führung in der Weltrangliste übernommen und drei der vier Grand-Slam-Turniere gewonnen, wobei sich beide in keinem einzigen dieser Endspiele gegenüberstanden. Graf hatte Seles in den einzigen beiden Aufeinandertreffen des Jahres bei zwei kleineren Turnieren besiegt, jedoch – mit Ausnahme des Sieges in Wimbledon – kein anderes Major-Endspiel erreicht. Auch 1992 gewann Seles drei Grand-Slam-Titel. Beide standen sich im Verlaufe des Jahres zweimal gegenüber: bei den French Open siegte Seles über Graf mit 10:8 im dritten Satz, in Wimbledon gewann die Deutsche. Seles revanchierte sich Anfang 1993 mit einem Dreisatzsieg bei den Australian Open.

Im April 1993 wies die Gesamtbilanz sechs Siege für Graf und vier für Seles aus, wobei Seles sechs der letzten acht Grand-Slam-Turniere gewonnen hatte. Anfang 1993 deutete sich eine langjährige sportliche Rivalität an, die in ihrer Brisanz und Spannung ein Versprechen für die Zukunft zu sein schien; ein Kampf um die Vormachtstellung im Damentennis, der eine ähnliche Qualität versprach, wie man sie früher im Zweikampf zwischen Chris Evert und Martina Navratilova erlebt hatte.

Doch die hoch gespannten Erwartungen fanden im April des Jahres 1993 ein unvorhergesehenes, jähes Ende, über das die frühere Weltklassespielerin Pam Shriver sagte: „Man hat uns um das Duell einer Generation betrogen.“

Das Jahr 1993 hatte für Monica Seles mit der erfolgreichen Titelverteidigung bei den Australian Open begonnen. Ein Turniersieg in Chicago gegen Martina Navrátilová folgte. Doch es war etwas später Altmeisterin Navrátilová, die Seles’ 34 Siege umfassende Serie im Finale der Paris Indoor Championships beendete. Eine hartnäckige Virusinfektion zwang die Weltranglistenerste im Anschluss zum Verzicht auf die Turniere von Indian Wells, Miami, Hilton Head und Barcelona.

Ihre erfolgreiche Rückkehr auf die Tour feierte sie beim Tennisturnier in Hamburg. Am 30. April 1993 wurde Monica Seles während ihrer Viertelfinalbegegnung gegen Magdalena Maleewa von dem psychisch gestörten Günter Parche, einem damals 38-jährigen deutschen Fan ihrer stärksten Kontrahentin Steffi Graf, beim Seitenwechsel in den Rücken gestochen. Seles bestritt nach dem Attentat zwei Jahre lang keine Profispiele mehr. Wenige Wochen später übernahm Graf wieder die Führung in der Weltrangliste.

Nachrichten über eine mögliche Rückkehr von Seles tauchten immer wieder auf. Zwar waren die physischen Verletzungen des Messerattentats schon bald verheilt, doch anders verhielt es sich mit der Psyche: Seles litt unter einer posttraumatischen Belastungsstörung und in der Folge unter depressiven Verstimmungen. Comeback-Ankündigungen folgten immer wieder Absagen. Am 17. Mai 1994 wurde Seles US-Bürgerin.

1995 verdichteten sich die Anzeichen für das Comeback. Seles war zu Gast bei den French Open, präsentierte sich als Zuschauerin der Spiele erstmals wieder der Öffentlichkeit und deutete eine mögliche Rückkehr auf die Tour an.

Den ersten Schritt auf dem Weg zum Comeback als Profispielerin machte sie am 29. Juli 1995 in Atlantic City mit einem Schaukampf gegen Navrátilová. Um Seles einen fairen Wiedereinstieg ins Profigeschehen zu ermöglichen, hatte die WTA ihre Regularien geändert. Mit Zustimmung der Weltranglistenersten Steffi Graf wurde die Neu-US-Amerikanerin für die ersten sechs gespielten Turniere als Co-Nummer 1 im Ranking geführt. Sinn der Regelung war es, dass Seles ohne Druck Ranglistenpunkte sammeln konnte, aus denen sich ihre tatsächliche Weltranglistenposition dann ergeben sollte.

Im August feierte Seles ein furioses Comeback bei den Canadian Open in Toronto. Sie gab im Turnierverlauf keinen Satz und nur 14 Spiele ab und bezwang unter anderem Gabriela Sabatini mit 6:1 und 6:0.
Auch bei den darauf folgenden US Open setzte sich ihr Siegeszug zunächst fort. Sie besiegte Anke Huber, Jana Novotná und Conchita Martínez und zog in das mit Spannung erwartete Finale gegen Steffi Graf ein. Seles unterlag in einem hochklassigen und von starken Emotionen geprägten engen Match mit 6:7 (6:8), 6:0 und 3:6.

Im Anschluss stoppten Verletzungsprobleme die Fortsetzung von Seles’ bemerkenswerter Rückkehr. Aufgrund von Überanstrengungserscheinungen im linken Knie und Fuß war sie gezwungen, auf die geplanten Starts in Oakland und bei den WTA Championships zu verzichten.

Seles konnte nicht wieder an die überragende Form und die alten Erfolge anknüpfen. Chronische Verletzungsprobleme und der Tod des Vaters im Frühjahr 1998 warfen sie immer wieder zurück.

Bis zu den Australian Open 1996 schien ihre Karriere nahtlos dort anzuschließen, wo sie zwei Jahre zuvor so abrupt unterbrochen worden war. Seles gewann das Vorbereitungsturnier in Sydney gegen Lindsay Davenport und wenig später in Melbourne auch ihr insgesamt neuntes Grand-Slam-Turnier. Sie blieb damit bei ihrem insgesamt vierten Start in Australien ungeschlagen. Mit den Spielen, die sie im folgenden Jahr bei den Australian Open gewinnen sollte, brach sie den 73 Jahre alten Rekord der Tennislegende Suzanne Lenglen. Sie war seit ihrem ersten Auftreten dort insgesamt 33 Spiele in Folge unbesiegt geblieben. Auf dem Weg zum Titel 1996 besiegte sie Spielerinnen wie Anke Huber, Conchita Martínez und Jana Novotná. Seles kehrte in die Top Ten der Weltrangliste zurück.

Eine Schulterentzündung zwang sie dann zu einer erneuten Verletzungspause, die bis in den späten Mai andauerte. Bei ihrer ersten Rückkehr zu den French Open seit dem Attentat erreichte Seles das Viertelfinale, in dem sie Jana Novotná mit 6:7 und 3:6 unterlag. Im Anschluss gelang ihr mit dem Sieg beim Wimbledon-Vorbereitungsturnier in Eastbourne der erste Titelgewinn auf Rasen. Weniger erfolgreich verlief ihre Rückkehr nach Wimbledon, wo sie bereits in der zweiten Runde der unbekannten Slowakin Katarína Studeníková unterlag.

Bei den US Open konnte Seles erneut ins Finale einziehen, in dem sie wie im Vorjahr Steffi Graf mit 5:7 und 4:6 unterlag. Im weiteren Verlauf des Jahres gewann Seles die Turniere in Montreal und Tokio. Dann aber folgten elf Monate Wartezeit.

1997 blieb sie bis zum August sieglos. Eine Fingerverletzung zwang sie zur Absage der Frühjahrsturniere. Dann besiegte sie im Finale von Manhattan Beach die Weltranglistenerste Lindsay Davenport und triumphierte eine Woche später in Toronto. Es war ihr 40. Sieg bei einem WTA-Turnier. Weitere Finalteilnahmen in Miami und Hilton Head sowie der Halbfinaleinzug bei den French Open bescherten Seles kurzzeitig die Rückkehr auf den zweiten Platz der Weltrangliste. Sowohl in Hilton Head als auch in Paris unterlag sie jeweils nur äußerst knapp der neuen Nummer eins des Rankings, Martina Hingis. Doch bereits eine Woche später führte eine Schulterverletzung erneut zur Absage weiterer Einsätze. Bei den US Open unterlag Seles bereits im Viertelfinale Irina Spîrlea. Die Presse berichtete erstmals über die schwere Krebserkrankung ihres Vaters Karoly, der 1998 starb.

Im Viertelfinale der Australian Open feierte Seles 1999 den ersten Sieg (7:5, 6:1) seit ihrem Comeback über Steffi Graf, verlor aber im Halbfinale gegen Martina Hingis. Das letzte Aufeinandertreffen der großen Rivalinnen Seles und Graf fand 1999 im Halbfinale der French Open statt. Graf gewann die Partie mit 6:7, 6:3 und 6:4.

Die nächsten Jahre waren von einigen Höhen und vielen Tiefen geprägt: Langen Verletzungspausen folgten immer wieder Turniersiege. Der letzte datiert vom Mai 2002, als Monica Seles die Madrid Open gewann. Es war der insgesamt 53. Turniererfolg der US-Amerikanerin.

Seles’ letzter Auftritt auf der Profitour fand bei den French Open im Jahr 2003 statt. Die dreifache Paris-Siegerin unterlag in der ersten Runde der French Open völlig außer Form der Russin Nadja Petrowa mit 4:6 und 0:6. Ständige Rücken- und Fußprobleme zwangen Seles zum Rückzug von der Tour. Eine auf Anraten ihrer Ärzte eingelegte neunmonatige Pause brachte Seles zufolge nicht die erhoffte Verbesserung. Dem von Monica Seles in größeren Zeitabständen immer wieder geäußerten Wunsch nach einem Comeback folgten Monate des Schweigens. Spekulationen um ein stilles Ende ihrer Karriere ohne offizielle Rücktrittserklärung machten die Runde.

Am 3. Dezember 2007 erklärte Monica Seles gegenüber der Zeitung Los Angeles Times ihre Absicht, möglicherweise im März 2008 beim Turnier in Miami auf die Tour zurückzukehren und ein Comeback zu versuchen. Seles betonte, dass sie sich nicht mehr im Stande sehe, eine komplette Saison zu spielen. Die mittlerweile 34-Jährige, deren letztes Spiel auf der Profitour mehr als vier Jahre zurücklag, erklärte, deshalb nur noch die Grand-Slam-Turniere sowie einige Veranstaltungen der zweithöchsten Kategorie in Angriff nehmen zu wollen.

Am 15. Februar 2008 fanden alle Spekulationen um eine mögliche Rückkehr der ehemaligen Weltranglistenersten ein Ende. Fünf Jahre nachdem Seles ihr letztes offizielles Match bestritten hatte, ließ sie in Miami von ihrem Manager offiziell ihren Rücktritt vom Profitennissport erklären. Grund hierfür waren auch die weiterhin anhaltenden Schmerzen, die ihr immer wieder Probleme bereiteten.

(Finale: Seles – Graf 7:6, 6:4)

Drei Wochen nachdem Monica Seles die Weltranglistenerste Steffi Graf ausgerechnet bei deren erklärtem Lieblingsturnier in Berlin erstmals bezwungen hatte, trafen die beiden im Endspiel der French Open wieder aufeinander. Seles führte schnell mit 3:0, ehe das Match wegen Regens unterbrochen werden musste. Graf kam hochmotiviert aus der Kabine zurück und schien entschlossen zu zeigen, wer die beste Spielerin der Welt ist. Nach einer Aufholjagd führte Graf im Tie-Break mit 6:2 und hatte vier Satzbälle. Doch Seles gewann alle folgenden sechs Punkte in Serie und damit den ersten Satz. Graf musste auch den zweiten Satz mit 4:6 abgeben und Seles feierte ihren ersten Sieg bei einem Grand-Slam-Turnier.

(Finale: Seles – Sabatini 6:4, 5:7, 3:6, 6:4, 6:2)

Erstmals seit 1902 ging 1990 ein Damenmatch wieder über die Distanz von fünf Sätzen. Seles hatte zuvor in einem harten Dreisatzmatch Arantxa Sánchez Vicario besiegt, während Gabriela Sabatini Steffi Graf aus dem Rennen werfen konnte. Seles gewann den ersten Satz nach einer Dreiviertelstunde mit 6:4, ehe sich Sabatini zurückkämpfte und sich die Sätze zwei und drei sichern konnte, womit ein normales Damenfinale beendet gewesen wäre. Damals waren jedoch drei Gewinnsätze zum Sieg beim Masters nötig. Seles kämpfte sich zurück in die Partie und ging schlussendlich nach nahezu vier Stunden Spielzeit als Siegerin vom Platz. Für Seles war es der erste Triumph beim Masters, womit sie erstmals Platz 2 der Weltrangliste einnahm.

(Finale: Seles – Graf 6:2, 3:6, 10:8)

1992 stand Seles zum dritten Mal in Folge im Endspiel der French Open, wie schon 1990 gegen Steffi Graf. Seles begann extrem druckvoll und ließ Graf beim 6:2 im ersten Satz kaum eine Chance. Doch Graf kämpfte sich zurück in die Partie und konnte den zweiten Satz mit 6:3 für sich entscheiden. Nun begann ein echter Schlagabtausch der beiden. Seles gelang das erste Break im letzten Satz und sie hatte beim Stande von 5:3 Matchbälle, diese Graf jedoch allesamt abwehren konnte. Graf gelang das 4:5 und danach das Break zum 5:5. Vor einem begeisterten Pariser Publikum kämpften beide Spielerinnen weiter, ehe Seles bei einer 9:8-Führung erneut Matchbälle hatte, von denen sie den zweiten schließlich nutzen konnte.

(Finale: Seles – Graf 2:6, 1:6)

Einen negativen Höhepunkt in der sportlichen Laufbahn von Monica Seles stellt das Wimbledon-Finale 1992 dar. Seles zeigte sich von Debatten um ihre Stöhngeräusche beeindruckt. Ihre Gegnerinnen im Viertelfinale und Halbfinale, Tauziat und Navratilova, hatten sich mehrfach beschwert. Seles versuchte nun im Endspiel möglichst still zu sein, was ihr Spiel negativ beeinflusste. Umgekehrt schien Graf an jenem Tag extrem motiviert, die French-Open-Niederlage vier Wochen zuvor wettzumachen. Das Spiel wurde mehrmals wegen Regens unterbrochen, was zu einer Gesamtspielzeit von nahezu fünf Stunden führte. Die reine Spielzeit betrug jedoch nur 58 Minuten, in denen Graf jederzeit die Kontrolle auf dem schnellen Untergrund hatte und ohne Probleme mit 6:2 und 6:1 siegte.
Für die Weltranglistenerste aus Jugoslawien war es die erste Grand-Slam-Niederlage seit den US Open 1990.

(Finale: Graf-Seles 7:6, 0:6, 6:3)

Das Finale der US Open 1995 hat fraglos als eines der emotionalsten, spannendsten und härtesten in die Geschichte des modernen Tennis Einzug gefunden. Zum ersten Mal seit dem Attentat des Graf-Fans Parche im Jahr 1993 standen sich Graf und Seles auf dem Platz gegenüber. Für Seles war der Einzug ins Endspiel die Krönung eines bis dahin makellosen Comebacks. Auf Anhieb hatte sie das Turnier in Toronto gewinnen können und auch bei den US Open war sie nicht zu stoppen. Die Tenniswelt wartete gespannt auf das Duell der beiden Tennisköniginnen der neunziger Jahre. Für beide bedeutete das neuerliche Zusammentreffen eine besondere emotionale Herausforderung: Für Seles, deren glanzvolle Karriere durch den fanatischen Graf-Bewunderer abrupt unterbrochen worden war. Für Graf, die immer in dem Bewusstsein und mit dem Makel hatte leben müssen, dass es Parches Absicht war, ihre schärfste Konkurrentin aus dem Weg zu räumen. Graf befand sich zudem inmitten eines Verfahrens wegen Steuerhinterziehung, das zur Verhaftung ihres Vaters Peter geführt hatte.

Die ersten Ballwechsel machten glauben, die Zeit sei stehen geblieben. Beide spielten auf allerhöchstem Niveau. Seles und Graf lieferten sich einen bedingungslos geführten Schlagabtausch. Schlaghärte und Schnelligkeit des Spiels waren atemberaubend. Beide zeigten keinerlei Schwäche. Fast folgerichtig endete Satz eins im Tiebreak und auch dieser verlief sehr eng. Seles hatte schließlich Satzball, ihr Aufschlag durch die Mitte landete im Bereich der Linie und wurde Aus gegeben, was Seles empörte. Graf gewann die nächsten Punkte und damit auch den ersten Durchgang.

Im zweiten Satz dominiert Seles. Mit unglaublicher Härte setzt sie unerreichbare direkte Gewinnschläge aus allen erdenklichen Ecken des Platzes. Ihre Gegnerin ist in dieser Phase chancenlos. Es ist das erste Mal, dass Graf bei den US Open einen Satz 0:6 verliert.

Im entscheidenden Satz gibt sich keine der beiden eine Blöße. Aber Graf gelingt ein frühes Break, das sie halten kann. Beim Stande von 5:3 für Graf kann Seles den ersten Matchball der Deutschen noch mit einem unerreichbaren Return abwehren. Beim zweiten Matchball landet eine beidhändige Vorhand von Seles im Netz. Graf siegt mit 7:6, 0:6, 6:3.


















































































































































































































































































































































































































































































































































Zeichenerklärung: S = Turniersieg; F, HF, VF, AF = Einzug ins Finale / Halbfinale / Viertelfinale / Achtelfinale; 1, 2, 3 = Ausscheiden in der 1. / 2. / 3. Hauptrunde; RR = Round Robin (Gruppenphase); n. a. = nicht ausgetragen; a. K. = andere Kategorie; PO (Playoff) = Auf- und Abstiegsrunde im Fed Cup; K1, K2, K3 = Teilnahme in der Kontinentalgruppe I, II, III im Fed Cup.

Berücksichtigt wurden die Turniere der Kategorie Tier I (bis 2008) bzw. die Turniere der Kategorien Premier Mandatory und Premier 5 (seit 2009).

Wiktoryja Asaranka |
Tracy Austin |
Jennifer Capriati |
Kim Clijsters |
Lindsay Davenport |
Chris Evert |
Evonne Goolagong |
Steffi Graf |
Simona Halep |
Justine Henin |
Martina Hingis |
Ana Ivanović |
Jelena Janković |
Angelique Kerber |
Amélie Mauresmo |
Garbiñe Muguruza |
Martina Navrátilová |
Karolína Plíšková |
Dinara Safina |
Arantxa Sánchez-Vicario |
Marija Scharapowa |
Monica Seles |
Serena Williams |
Venus Williams |
Caroline Wozniacki
