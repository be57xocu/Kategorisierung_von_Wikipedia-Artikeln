Peter Lauster (* 21. Januar 1940 in Stuttgart) ist ein deutscher Psychologe und Autor zahlreicher Selbsthilfebücher.

Er ist bekannt geworden durch seinen langjährigen Bestseller „Die Liebe. Psychologie eines Phänomens“. Dieses Buch hat inzwischen eine Auflage von über 1 Million Exemplaren erreicht. Viele seiner Bücher wurden in mehrere Sprachen übersetzt. 

Lauster studierte  in Tübingen Psychologie, Philosophie, Kunstgeschichte und Anthropologie. Seit Januar 1971 betreibt er in Köln eine Praxis für psychologische Diagnostik und Beratung.

Seit 1971 veröffentlicht Lauster populärwissenschaftliche psychologische Sachbücher. Die deutsche Gesamtauflage seiner Bücher beträgt über 4,5 Millionen Exemplare. 

Lauster nimmt für seine Bücher nicht in Anspruch, eine Universallösung für das jeweils besprochene Problem zu sein, sondern präsentiert sie als Denkanstoß und betont stets die Wichtigkeit professioneller psychologischer Behandlung in Form einer Therapie. Er vermeidet es, durch Porträtierung eines psychologischen Übermenschen dem Leser ein Gefühl von Hoffnungslosigkeit oder Unvollkommenheit zu vermitteln.

Kennzeichnend für Peter Lauster ist seine verständliche, nicht zynische oder belehrende Sprachweise. Dabei unterlegt er seine Ausführungen nicht mit Querverweisen zu wissenschaftlichen Quellen, sondern überlässt es dem Leser, über die Annahme der Inhalte zu entscheiden.

Lauster gehört zu jener Generation deutscher Psychologen und Soziologen (wie z. B. auch Dieter Duhm), die die Institution Ehe in den 1970er und in den 1980er Jahren durch ihre Bücher stark in Frage stellten, mithin die bürgerliche Moral als krebserregend bezeichneten.[1] Der Begriff der Liebe, der hier vermittelt wird, orientiert sich an der Sensitivität. Der Mensch solle sich und seine Umgebung wahrnehmen und ganz im Hier und Jetzt aufgehen. Begriffe wie Treue galten Lauster als überholt,[2] ja als Ausdruck einer krankhaften, verkümmerten Psyche. Eine ewige, große Liebe bezeichnete er als Mythos. Hier geht es also um Liebe als Eros, nicht um Agape (wenngleich in Die sieben Irrtümer der Männer die Agape als gleichwertig mit dem Eros angesehen wird).

(chronologisch geordnet)
