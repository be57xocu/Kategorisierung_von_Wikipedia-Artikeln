Henry Enfield Roscoe (* 7. Januar 1833 in London; † 18. Dezember 1915 in West Horsley) war ein britischer Chemiker. Er stellte 1867 metallisches Vanadium dar.

Er war der Sohn des Barristers Henry Roscoe und seiner Frau Maria, Tochter des Kaufmanns Thomas Fletcher.
Roscoe besuchte das University College in London. Nach einem Studium in Heidelberg arbeitete er unter Robert Wilhelm Bunsen, mit dem ihn eine lebenslange Freundschaft verband. Er übernahm 1857 den Lehrstuhl für Chemie am Owens-College in Manchester. Wichtige Arbeitsgebiete waren Fotometrie (Bunsen-Roscoe-Gesetz) und Spektralanalyse sowie die Chemie der Übergangselemente Wolfram und Vanadium.

1863 heiratete er Lucy Potter († 1910), die Tochter von Edmund Potter. Beatrix Potter war seine Nichte.

1863 wurde er als Mitglied („Fellow“) in die Royal Society gewählt, die ihm 1873 die Royal Medal verlieh. 1874 wurde er zum korrespondierenden Mitglied der Göttinger Akademie der Wissenschaften gewählt.[1] 1884 wurde er zum Knight geadelt.[2] 1882 wurde er korrespondierendes Mitglied der Bayerischen Akademie der Wissenschaften.[3] Im Jahr 1887 wurde er zum Mitglied der Leopoldina gewählt. 1890 wurde er in die American Academy of Arts and Sciences gewählt.
