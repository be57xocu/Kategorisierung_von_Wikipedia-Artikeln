Noah Gordon (* 11. November 1926 in Worcester, Massachusetts) ist ein US-amerikanischer Schriftsteller.

Noah Gordon wurde als zweites Kind von Robert und Rose Gordon in Worcester, Massachusetts, USA geboren. Sein Vorname wurde nach seinem Großvater Noah Melnikoff gewählt. Im Jahre 1945 schloss er die Highschool ab. Trotz Brille und Farbenblindheit trat er in die Armee ein, später in die Navy. Nach dem Zweiten Weltkrieg studierte er zunächst Medizin, dann jedoch entdeckte er seine Lust am Schreiben und studierte Journalismus. 1950 erhielt er seinen Bachelor in Journalismus und später seinen Master in Englisch und Creative Writing an der Boston University.

Er heiratete Lorraine Seay und wohnte mit ihr in New York. Nach ihrem ersten Kind zogen sie nach Boston. Danach arbeitete er viele Jahre beim Boston Herald.
Nach der Veröffentlichung etlicher Erzählungen gelang ihm bereits mit seinem ersten Roman Der Rabbi der Durchbruch. Besonders erfolgreich sind seine Romane rund um die fiktive Mediziner-Dynastie der Familie Cole (Der Medicus, Der Schamane, Die Erben des Medicus). Seine neuesten Werke befassen sich mit der Inquisition und der jüdischen Kulturgeschichte.

Für den zweiten Roman Der Schamane aus der Medicus-Serie erhielt er im Frühjahr 1993 den James Fenimore Cooper Prize for Historical Fiction.

Gordon hat drei Kinder. Er lebt heute mit seiner Frau in einem Altersheim und betreut dort die Bibliothek, welche er dem Altersheim geschenkt hat.[1]
„Familie Cole“-Zyklus

andere Romane

Kinder und Jugendbuch 

Der Medicus (Film) vom Regisseur Philipp Stölzl 2013
