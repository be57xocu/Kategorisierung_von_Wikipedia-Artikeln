Friedrich Karl Barth (* 7. Februar 1938 in Kassel) ist ein deutscher Geistlicher. Er war Pfarrer in Bad Hersfeld, ab 1971 war er in der Beratungsstelle für die Gestaltung von Gottesdiensten in Frankfurt am Main tätig. 1990 bis 1997 war er Kurpfarrer in Bad Wildungen. Barth verfasste viele Texte Neuer geistlicher Lieder. Er arbeitete unter anderem mit Peter Horst, Peter Janssens, Uwe Seidel, Reiner Weiß und Reinhard Wolf zusammen. Er war Mitglied der Präsidialversammlung des DEKT, der AGOK und AGOFF (1973 bis 1989), der Oekumenischen Textautoren- und Komponisten-Gruppe der Werkgemeinschaft Musik e. V. und der AG Musik in der Evangelischen Jugend e. V., heute Textautoren- und Komponistengruppe TAKT. Auf seine Idee geht der Kirchentagshocker zurück. 

Bücher

Lieder
