Ewald Christian von Kleist (* 7. März 1715 auf dem väterlichen Gut Zeblin[1] in Hinterpommern; † 24. August 1759 in Frankfurt (Oder)) war ein deutscher Dichter und preußischer Offizier.

Seine Eltern waren Joachim Ewald von Kleist (1684–1738), Erbherr auf Zeblin und Warnin, und dessen Ehefrau Juliane von Manteuffel (1688–1719) aus dem Haus Poplow.

Kleist besuchte das Gymnasium in Danzig und die Universität Königsberg. Er wurde 1736 dänischer Offizier, 1740 aber von Friedrich II. reklamiert und zum Leutnant beim Regiment des Prinzen Heinrich ernannt.

Johann Wilhelm Ludwig Gleim, der zu jener Zeit in Potsdam lebte, weckte zuerst Kleists dichterische Begabung. Karl Wilhelm Ramler, den Kleist 1749 kennenlernte, brachte ihn dazu, seine Texte stilistisch zu überarbeiten, vielfach allerdings ohne Rücksicht auf die Kleistschen Eigentümlichkeiten. Eine unglückliche Liebe zu Wilhelmine von der Goltz trübte früh die natürliche Heiterkeit von Kleists Gemüt.

Nachdem er 1744 bis 1745 am Zweiten Schlesischen Krieg teilgenommen hatte, wurde er 1749 zum Stabskapitän befördert. Zwei Jahre später wurde ihm eine Kompanie unterstellt. Nach einer Reise in die Schweiz, wo er fast ein Jahr lang auf Werbung war, und einer überstandenen schweren Krankheit trat er im Mai 1756 einen Kuraufenthalt in Freienwalde an, von dem ihn jedoch ein Befehl zu seinem Regiment zurückrief, mit dem er daraufhin ins Feld zog. 1757 wurde er zum Major und bald darauf zum Direktor eines in Leipzig errichteten Feldlazaretts ernannt.

In Leipzig begann er sein kleines Epos Cissides und Paches und schloss unter anderem Freundschaft mit Gotthold Ephraim Lessing, der ihn veranlasste, ein Trauerspiel zu schreiben. Es entstand der Entwurf des Seneca, den Kleist selbst für einen Fehlversuch hielt.

Im Mai 1758 folgte Kleist dem Korps des Prinzen Heinrich, das die Reichsarmee bis hinter Hof zurücktrieb. Trotz mehrfacher Zurücksetzung konnte er sich nicht dazu entschließen, seinen Abschied zu nehmen. In der Schlacht bei Kunersdorf am 12. August 1759 drang er an der Spitze seines Bataillons gegen eine feindliche Batterie vor. Er wurde an der rechten Hand verwundet, nahm den Degen in die Linke und stürmte weiter, bis ihm drei Kartätschenkugeln das rechte Bein zerschmetterten. Ohnmächtig blieb Kleist die Nacht über auf dem Schlachtfeld liegen, wurde von Kosaken ausgeplündert und erst am nächsten Tag nach Frankfurt (Oder) gebracht. Der behandelnde junge Arzt wurde in dem Moment erschossen, als er die Wunden Kleists mit Spiritus säuberte. Am 24. August 1759 erlag Kleist in Frankfurt (Oder) seinen Verletzungen und wurde von der russischen Garnison ehrenvoll begraben.

Kleist war Freimaurer in einer unbekannt gebliebenen Loge. 1780 setzte die Loge „Zum aufrichtigen Herzen“ in Frankfurt (Oder) ein Grabdenkmal für Ewald Christian von Kleist.[2] Die Grabinschrift lautet:

Für Friedrich kämpfend sank er nieder,
So wollte es sein Heldengeist.
Unsterblich groß durch seine Lieder
Der Menschenfreund, der weise Kleist.

Kleists reines Gemüt spiegelt sich in allen seinen Werken, vor allem in den Erzählungen Die Freundschaft und Arist sowie in der Idylle Irin. Korrektheit des Ausdrucks, glücklich gewählte Bilder, in denen er gewöhnlich die Natur lebendig zeichnet, sowie Fülle und Wohlklang der Diktion charakterisieren seine Gedichte. Neben dem beschreibenden Gedicht versuchte sich Kleist auch in der Fabel, in der Idylle und in der Hymne. Als sein Hauptwerk gilt das in Hexametern abgefasste Gedicht Der Frühling, das zuerst 1749 für Freunde gedruckt erschien und später zahlreiche Auflagen erlebte.

Kleist schrieb um die 400 Hexameter, die bei ihrer Art der Dichtung jedoch auf Johann Peter Uz zurückgehen. Neben den idyllischen Gedichten, welche dominierten, schrieb Kleist auch Liebes- und Trinklieder. Nach Gotthold Ephraim Lessings Laokoon, welches dem Genre der beschreibenden Poesie zugeordnet wurde, wurde auch Kleists Der Frühling immer wieder diesem Genre zugeordnet. Jedoch leugnete Kleist selbst, eine bloße Beschreibung des Frühlings zu geben.

Kleists Sämtliche Werke wurden 1760 von Karl Wilhelm Ramler (Berlin, 3. Aufl. 1771) und 1803 mit einer Biographie von Wilhelm Körte (ebd., 5. Aufl. 1853) in jeweils zwei Bänden herausgegeben, 1884 zusammen mit den Briefen in drei Bänden von August Sauer (ebd.).

Im Kleist-Museum Frankfurt (Oder) wird auch das literarische Erbe Ewald Christian von Kleists gepflegt.

