Eduard Macheiner (* 18. August 1907 in Fresen, Steiermark; † 17. Juli 1972 in Salzburg) war von 1969 bis 1972 Erzbischof von Salzburg.

Eduard Macheiner übersiedelte als Kleinkind nach Seetal bei Tamsweg, da seine Eltern dort das Gasthaus „Klauswirt“ angekauft hatten. Er besuchte in der Folge die einklassige Volksschule in Seetal, 1919 trat er in das Borromäum in Salzburg ein. Macheiner empfing 1932 die Priesterweihe und wurde 1935 als Hofkaplan und erzbischöflicher Sekretär von Fürsterzbischof Sigismund Waitz nach Salzburg berufen. Ab 1939 war er Domchorvikar. 1940 promovierte er an der Universität Wien zum Doktor der Theologie. 1945/46 dozierte er Moraltheologie an der wiedererstandenen Universität Salzburg. Danach war er bis 1949 als Pfarrer in Bischofshofen und darauf als Dekan in Tamsweg tätig.

1952 erfolgte seine Berufung in das Domkapitel. In Graz wurde er 1953 in den Ritterorden vom Heiligen Grab zu Jerusalem investiert; von 1956 bis 1966 war er Prior für die Komturei Salzburg der Grabesritter. Am 1. März 1963 wurde er zum Weihbischof in Salzburg und zum Titularbischof von Selja ernannt. Die Bischofsweihe spendete ihm der Salzburger Erzbischof Andreas Rohracher; Mitkonsekratoren waren Bischof Joseph Köstner von Gurk und Bischof Josef Schoiswohl von Graz-Seckau. 1967 Promotor der Diözesansynode und 1969 Kapitelsvikar. In dieser Funktion ernannte er im selben Jahr Karl Berg zu seinem Generalvikar. 

Am 9. Oktober 1969 wurde Macheiner durch das Domkapitel als Nachfolger des zurückgetretenen Erzbischofs Andreas Rohracher zum Erzbischof von Salzburg gewählt. Er widmete sich mit großem Erfolg dem ökumenischen Aufbauwerk und dem Ziel, geistige Brücken zwischen den Generationen und zwischen Priestern und Laien zu bauen. Nach seinem überraschenden Ableben wurde Erzbischof Macheiner in der Krypta des Salzburger Doms beigesetzt. Zu seinem Nachfolger wurde Karl Berg gewählt.

Erzbischöfe und FürsterzbischöfeEberhard III. von Neuhaus |
Berthold von Wehingen (ernannter Gegenerzb.) |
Eberhard IV. von Starhemberg |
Johann II. von Reisberg |
Friedrich IV. Truchsess von Emmerberg |
Sigismund I. von Volkersdorf |
Burkhard II. von Weißpriach |
Bernhard von Rohr |
Johann III. Beckenschlager |
Christoph Ebran von Wildenberg (letzter gewählter Gegenerzbischof) |
Friedrich V. Graf von Schaunberg |
Sigmund II. von Hollenegg |
Leonhard von Keutschach |
Matthäus Lang von Wellenburg |
Ernst Herzog von Bayern (Administrator) |
Michael von Kuenburg |
Johann Jakob Khuen von Belasi |
Georg von Kuenburg |
Wolf Dietrich von Raitenau |
Markus Sittikus Graf von Hohenems |
Paris Graf von Lodron |
Guidobald Graf von Thun und Hohenstein |
Max Gandolf von Kuenburg |
Johann Ernst Graf von Thun und Hohenstein |
Franz Anton Fürst von Harrach |
Leopold Anton Freiherr von Firmian |
Jakob Ernst Graf von Liechtenstein-Kastelkorn |
Andreas Jakob Graf von Dietrichstein |
Sigismund III. Christoph Graf von Schrattenbach |
Hieronymus Graf von Colloredo (letzter regierender Fürsterzbischof)

Sigmund Christoph Graf von Waldburg zu Zeil und Trauchburg (Administrator) |
Leopold Maximilian Graf von Firmian (Administrator) |
Augustin Johann Joseph Gruber |
Friedrich Johannes Fürst zu Schwarzenberg |
Maximilian Joseph von Tarnóczy |
Franz Albert Eder |
Johannes Evangelist Haller |
Johannes Baptist Katschthaler |
Balthasar Kaltner |
Ignatius Rieder |
Sigismund Waitz |
Andreas Rohracher  letzter titul. Fürsterzbischof

ErzbischöfeEduard Macheiner |
Karl Berg |
Georg Eder |
Alois Kothgasser |
Franz Lackner
