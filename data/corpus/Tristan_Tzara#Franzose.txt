Tristan Tzara, eigentlich Samuel Rosenstock (* 4. Apriljul./ 16. April 1896greg. in Moinești, Rumänien; † 24. Dezember 1963 in Paris) war ein rumänischer Schriftsteller und Mitbegründer des Dadaismus.

1912 veröffentlichte er als Gymnasiast die Zeitschrift „Simbolul“ mit Hilfe von Marcel Janco und Ion Vinea. Er begründete 1916 mit Hans Arp und Hugo Ball die Zürcher Gruppe des Dadaismus. Tzara schrieb die ersten Dada-Texte (unter anderem La Première aventure céleste de Monsieur Antipyrine, 1916; Vingt-cinq poèmes, 1918; Sept manifestes Dada, 1924) und beteiligte sich an den Aufführungen im Cabaret Voltaire. Er erfand das Simultangedicht, beispielsweise das Werk L'Amiral cherche une maison à louer. 

1919 zog Tzara nach Paris und beteiligte sich an den Aktivitäten des Pariser Dadaismus, unter anderem mit André Breton, Philippe Soupault und Louis Aragon. Die Gruppe schockierte die Öffentlichkeit mit dem Versuch der Desintegration der Sprachstruktur. Anlässlich des fiktiven Dadaisten-„Prozesses“ um den nationalistischen Schriftsteller und Politiker Maurice Barrès überwarf Tzara sich im Mai 1921 mit den Pariser Dadaisten. Dieser Streit führte zur Auflösung der Gruppe und wenige Monate später zur Gründung des Surrealismus.[1] Erst um 1930 und im Zuge einer zunehmenden Politisierung wandte Tzara sich auch dem Surrealismus zu (1931: Essai sur la situation de la poésie, Der approximative Mensch, 1933: L'Antitête, 1935: Grains et issues).

Später kämpfte Tzara im Spanischen Bürgerkrieg, nahm an kommunistischen Tätigkeiten und im Zweiten Weltkrieg an der französischen Widerstandsbewegung, Résistance, teil. Nach dem Krieg wandte er sich gegenwartsbezogeneren Themen und existenziellen Problemen zu (1946: Terre sur terre, 1950: De Mémoire, Parler seul, 1953: La Face intérieure).

Am 6. September 1960 gehörte er zu den Unterzeichnern des Manifests der 121. Sein Talent und seine Aktivitäten bezogen sich vor allem auf die Organisation von Dada, weniger auf bleibende künstlerische Darstellungen.

Tristan Tzara wurde auf dem Cimetière Montparnasse (Division 8) in Paris beigesetzt.

Auf der documenta 8 im Jahr 1987 in Kassel wurden Aufnahmen von ihm im Rahmen der „Archäologie der akustischen Kunst 2: Dada-Musik“ als offizieller Ausstellungsbeitrag aufgeführt.

Im Jahr 1924 traf Tzara die schwedische Künstlerin, Kunstkritikerin und Schriftstellerin Greta Knutson (1899–1983) in Frankreich. Sie heirateten am 8. August 1925 in Stockholm. Ihr Sohn Christophe wurde am 15. März 1927 in Neuilly-sur-Seine geboren.

1925/1926 baute der österreichische Architekt Adolf Loos im Pariser Stadtteil Montmartre für die Familie die Maison Tristan Tzara, eine repräsentative Villa. Finanziert wurde der Bau von Knutson aus einem Erbe. Die Ehe wurde nach der Trennung 1937 im Jahr 1942 geschieden.[2][3]