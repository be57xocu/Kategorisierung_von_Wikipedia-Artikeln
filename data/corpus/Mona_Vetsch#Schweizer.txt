Monika «Mona» Vetsch[1] (* 23. Juni 1975 in Hattenhausen) ist eine Schweizer Fernseh- und Radiomoderatorin.

Mona Vetsch ist in einem kleinen Dorf im Kanton Thurgau als Bauerntochter aufgewachsen und lebt heute im Zürcher Unterland. Nach abgebrochenem Wirtschaftsstudium an der Universität St. Gallen begann sie ihre journalistische Tätigkeit bei der Thurgauer Zeitung und bei Radio Thurgau, wechselte aber 1997 zur Redaktion und Moderation der SF 1-Jugendsendung Oops!.

2005 studierte sie neben der 60%igen Anstellung bei Radio DRS 3 an der Universität Zürich Politikwissenschaft und Soziologie.[2] Seit dieser Zeit war sie auch als Kolumnistin für diverse Schweizer Zeitungen und Zeitschriften wie Aargauer Zeitung, Berner Zeitung, Facts, Weltwoche und WOZ tätig.

Sie redigiert und moderiert die Rundfunksendungen Der Morgen (früher Vitamin 3) und Focus auf Radio SRF 3. Für das Schweizer Fernsehen moderiert sie die Sendungen der Redaktion SF Spezial, insbesondere die Sommer-Reisesendung SF Spezial Fernweh sowie die SF Spezial Langzeitreportagen (unter anderem Lauberhorn 2005 oder EURO 2008). Seit Januar 2012 moderiert sie die Diskussionssendung Der Club (neben Karin Frei) auf SRF 1.[3]
Frühere Sendungen von Mona Vetsch im Schweizer Fernsehen waren unter anderem die Unterhaltungsshow PISA – Kampf der Kantone (neben Ueli Schmezer), die Reisesendung einfachluxuriös sowie das Magazin Quer (als Stellvertreterin für Patrick Rohr).

Im März 2006 führte eine «freche» Reportage[4] von Mona Vetsch über Kassel und Saarbrücken zu einer Intervention des Kasseler Oberbürgermeisters Bertram Hilgen, zu einer Absage von zwölf Personen einer gebuchten Reise nach Zürich und hunderten von ablehnenden, teils gehässigen Leserbriefen.[5] Auf der anderen Seite führte diese Reportage bereits zu erhöhter Aufmerksamkeit für diese Städte sowie Einladungen an den Rhein zu Nachtwächterführungen [6] sowie Wanderungen auf dem Deichwanderweg.[7]
Am 23. Juni 2017 (ihrem 42. Geburtstag) moderierte sie nach 17 Jahren zum letzten Mal die Morgensendung auf SRF3. Ihre Kollegen organisierten eine Abschiedsparty für Sie, bei der u.a. Dabu Fantastic, Pablo Infernal, Lo und Leduc und Stephan Eicher live im Studio auftraten.[8]
Sie hat der schweizerdeutschen Synchronisation des englisch-deutschen Spielfilms Earth – Unsere Erde ihre Stimme geliehen.[9]
Vetsch ist seit 2009 verheiratet und hat zwei Söhne (* 2009, * 2011).
