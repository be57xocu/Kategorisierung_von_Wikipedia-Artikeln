Eric Rücker Eddison (* 24. November 1882 in Adel, Yorkshire, heute ein Vorort von Leeds; † 18. August 1945) war ein britischer Schriftsteller.

Eddison besuchte die Privatschule von Eton und studierte am Trinity College von Cambridge. Er trat 1906 als Beamter in das britische Handelsministerium ein. 1938 reichte er seine Kündigung ein, um sich ganz auf seine schriftstellerische Arbeit zu konzentrieren.

1922 debütierte Eddison mit The Worm Ouroboros. Dieses Werk galt seiner barocken Sprache wegen lange als unübersetzbar und wurde erst 1981 ins Deutsche übertragen (Der Wurm Ouroboros). 1926 folgte der teilweise historische Wikingerroman Styrbiorn The Strong, der sogar erst 1996 als Styrbjörn der Starke zum ersten Mal auf deutsch erschien. 

1935 kehrte Eddison mit seinem Roman Mistress of Mistresses (deutsch: Die Herrin Zimiamvias) wieder in die Welt des Wurms zurück. Der Roman wurde 1941 mit A Fish Dinner in Memison (Ein Fischessen in Memison) fortgesetzt. Als Eddison 1945 überraschend starb, war der dritte Band noch nicht vollendet. Seine Frau stellte aus dem Nachlass eine Version zusammen, die 1958 als The Mezentian Gate (deutsch: Das Tor von Mezentia) erschien.

Die ersten Übersetzungen seiner Werke ins Deutsche waren kaum erfolgreich. Erst die Neuauflage des Wurms in einer Übersetzung von Helmut W. Pesch zeigte, dass Eddison mit seiner kraftvollen und intensiven Sprache auch heute noch seine Leser in Bann schlagen kann. Der Wurm Ouroboros gilt heute als Klassiker der Fantasy und Vorreiter von J. R. R. Tolkiens Der Herr der Ringe.
