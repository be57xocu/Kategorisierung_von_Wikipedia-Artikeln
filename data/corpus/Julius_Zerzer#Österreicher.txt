Julius Zerzer (* 5. Jänner 1889 in Mureck, Steiermark; † 29. Oktober 1971 in Linz) war ein österreichischer Schriftsteller.

Der Sohn eines Arztes war Professor an einem Gymnasium in Linz. Der christlich-konservative Schriftsteller historischer Romane sah sich selbst in der Nachfolge Adalbert Stifters, thematisierte in seinem historischen Roman Der Kronenerbe (1953) das „naturbedingte Gesetz“ menschlichen Seins. 

Nach dem Anschluss Österreichs 1938 beteiligte sich Zerzer mit einem Beitrag am Bekenntnisbuch österreichischer Dichter (herausgegeben vom Bund deutscher Schriftsteller Österreichs, dem Zerzer auch angehörte),[1] das den Anschluss begeistert begrüßte.

In Linz-St. Magdalena wurde im Jahre 1972 die Zerzerstraße nach ihm benannt.
