Andrea Nuyt (* 10. Juli 1974 in Gouda) ist eine niederländische Eisschnellläuferin. Nuyt ist insbesondere auf den kurzen Strecken über 500 und 1.000 m erfolgreich.

Nuyt ist zweifache niederländische Meisterin im Sprint. Bei den Weltmeisterschaften 2002 im norwegischen Hamar belegte sie hinter der US-Amerikanerin Catriona LeMay-Doan den zweiten Platz.

Zweimal nahm Nuyt bislang an Olympischen Spielen teil. 1998 in Nagano stürzte sie jedoch über 500 m. Bei den Spielen in Salt Lake City wurde sie über 500 m Vierte und über 1.000 m Achte.

Nuyt ist mit dem Eisschnellläufer Carl Verheijen verlobt.
