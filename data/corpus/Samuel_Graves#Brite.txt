Samuel Graves (* 1713; † 1787 in Hembury Fort, Honiton, Devon) war ein britischer Admiral, der im Unabhängigkeitskrieg der amerikanischen Kolonien gegen Großbritannien auf britischer Seite gekämpft hat.

Die Stammlinie der Familie Graves lässt sich bis zur Herrschaft von Henry III. (1216–1272) zurückverfolgen, als die Greves oder Greaves in der Gemeinde Beeley bei Chatsworth lebten, im nördlichen Teil von Derbyshire.

Viele Nachkommen der Familie Graves ließen sich in London und anderen britischen Großstädten nieder. Vor allem in der Zeit zwischen 1629 und 1649 wanderten Mitglieder der Familie Graves in die nordamerikanischen Kolonien aus, und Samuel Graves gehört zur 8. Generation der Nachkommen der Graves-Auswanderer.

Samuel Graves wurde 1713 geboren und heiratete zuerst Elizabeth Sedgwick, Tochter von John Sedgwick aus Staindrop (Grafschaft Durham). Nach dem Tod Elizabeths 1767 heiratete er 1769 Margaret Spinkes, Tochter von Elmer Spinkes aus Aldwinkle in Northamptonshire und Schwester von Elizabeth Simcoes Mutter. Er war Taufpate von John Graves Simcoe, der im Jahr 1782 Elizabeth heiratete.

Graves nahm 1759 als Kapitän der HMS Duke an der Seeschlacht in der Bucht von Quiberon teil. 1774 wurde er mit dem Auftrag nach Nordamerika entsandt, als Befehlshaber des Nordamerika-Geschwaders der Royal Navy die Einhaltung der britischen Handelsgesetze durchzusetzen. Er befehligte die britischen Flottenoperationen in der Frühphase des Unabhängigkeitskriegs bei den Kämpfen um Boston 1775/76. Am 18. Oktober 1775 befahl er die Bombardierung von Falmouth in der britischen Kolonie Maine. Anfang 1776 wurde er von Admiral Richard Howe abgelöst. Sein Cousin Thomas Graves war später Befehlshaber der vereinigten britischen Geschwader in der Seeschlacht von Chesapeake 1781.
