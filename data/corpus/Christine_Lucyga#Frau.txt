Christine Lucyga (* 6. April 1944 in Kolberg, Provinz Pommern) ist eine deutsche Politikerin. Sie war von 1990 bis 2005 Mitglied des Deutschen Bundestages. Christine Lucyga studierte in Rostock Slawistik und Hispanistik und promovierte 1980 über Lateinamerikanische Literatur. 

Von 1969 bis 1989 war sie Fremdsprachenlehrerin an der Hochschule für Seefahrt in Warnemünde und an der Universität Rostock.

Im Zuge der Neuordnung der politischen Szene in der DDR engagierte sie sich seit September 1989 im Neuen Forum Rostock. Ende 1989 trat sie in die SPD ein. Sie wurde bei der Volkskammerwahl 1990 am 18. März für die SPD gewählt und war bis zum 2. Oktober 1990 Volkskammerabgeordnete.

Christine Lucyga war seit dem 3. Oktober 1990 bis zum Ende der 15. Legislaturperiode 2005 Abgeordnete im Deutschen Bundestag für den Wahlkreis Rostock. Sie wurde bei den Bundestagswahlen 1990, 1994, 1998 und 2002 jeweils als Direktkandidatin gewählt. 

Nach der Auflösung des Bundestages 2005 trat sie nicht wieder zur Wahl an. Ihr Nachfolger wurde Christian Kleiminger.
