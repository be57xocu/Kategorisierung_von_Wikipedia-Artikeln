Richard Reisch (* 17. April 1866 in Wien; † 14. Dezember 1938 ebenda) war ein österreichischer Jurist, Politiker (Christlichsoziale Partei) und Präsident der Oesterreichischen Nationalbank.

Richard Reisch studierte zunächst Rechtswissenschaften in Wien und Innsbruck und war ab 1891 Beamter im k.k. Finanzministerium. 1910 stieg Reisch zum Sektionschef auf und kam dann in die Direktion der Allgemeinen Bodencreditanstalt, deren Vizepräsident er 1921 wurde. Nachdem er vom 17. Oktober 1919 bis zum 20. November 1920 in der Staatsregierung Renner III und der Staatsregierung Mayr I den Posten des Staatssekretärs (= Ministers) im Staatsamt für Finanzen innegehabt hatte, bestimmte er 1922–1932 als erster Präsident der Oesterreichischen Nationalbank die Führung der österreichischen Währungspolitik mit. Neben seiner politischen Tätigkeit lehrte Reisch auch an der Universität Wien, ab 1914 als Professor für Finanzrecht.

Erste Republik:Steinwender |
Schumpeter |
Reisch |
Grimm |
Gürtler |
Ségur-Cabanac |
Kienböck |
Ahrer |
Kollmann |
Kienböck |
Josef Mittelberger |
Juch |
Redlich |
Weidenhoffer |
Draxler |
Neumayer

Zweite Republik:Zimmermann |
Margarétha |
Kamitz |
Heilingsetzer |
Klaus |
Korinek |
Schmitz |
Koren |
Androsch |
Salcher |
Vranitzky |
Lacina |
Staribacher |
Klima |
Edlinger |
Grasser |
Molterer |
Pröll |
Fekter |
Spindelegger |
Schelling |
Löger

Richard Reisch |
Viktor Kienböck |
Eugen Kaniak |
Hans Rizzi |
Eugen Margarétha |
Reinhard Kamitz |
Wolfgang Schmitz |
Hans Kloss |
Stephan Koren |
Hellmuth Klauhs |
Maria Schaumayer |
Klaus Liebscher |
Ewald Nowotny

Karl Renner |
Jodok Fink

Julius Deutsch |
Arnold Eisler |
Matthias Eldersch |
Wilhelm Ellenbogen |
Otto Glöckel |
Ferdinand Hanusch |
Johann Löwenfeld-Russ |
Michael Mayr |
Wilhelm Miklas |
Ludwig Paul |
Rudolf Ramek |
Richard Reisch |
Josef Resch |
Josef Stöckler |
Julius Tandler |
Erwin Waiß |
Johann Zerdik

Michael Mayr |
Ferdinand Hanusch /
Eduard Heinl

Walter Breisky |
Julius Deutsch |
Wilhelm Ellenbogen |
Otto Glöckel |
Alfred Grünberger |
Alois Haueis |
Wilhelm Miklas |
Karl Pesta |
Richard Reisch |
Josef Resch |
Karl Renner |
Julius Roller |
Julius Tandler
