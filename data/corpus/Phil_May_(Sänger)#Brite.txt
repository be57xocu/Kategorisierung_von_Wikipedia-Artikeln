Phil May (* 9. November 1944 in Dartford als Philip Arthur Dennis Wadey) ist Sänger und Komponist der Rockband The Pretty Things.

Durch seinen im Blues verwurzelten Gesang und seine Kompositionen prägt er zusammen mit dem Gitarristen Dick Taylor seit der Gründung 1963 wesentlich den Sound der Pretty Things. Er fiel in den frühen 1960er Jahren auch durch sein Image auf. So galt er damals als „der Mann mit den längsten Haaren Europas“. Die aggressiven, manchmal auch chaotischen Bühnenauftritte von May und den Pretty Things der Anfangszeit sorgten für Aufsehen und harte Rocksongs wie Don’t bring me down und Midnight to six men für einige Hitparadennotierungen.

1968 schufen The Pretty Things, basierend auf einer Kurzgeschichte von May, mit S. F. Sorrow das erste Konzeptalbum der Rockgeschichte. Damals ein kommerzieller Misserfolg, gilt das Werk heute als Meilenstein der psychedelischen Rockmusik. 

May und seiner Band, deren Mitglieder immer wieder wechselten, blieb der große Durchbruch trotz häufigen Kritikerlobes verwehrt. Er ist aber trotz seiner von vielen Schicksalsschlägen begleiteten Karriere immer noch regelmäßig auf Tournee.
