Leo Lesser Ury (* 7. November 1861 in Birnbaum, Provinz Posen; † 18. Oktober 1931 in Berlin) war ein deutscher Maler und Grafiker der impressionistischen Berliner Secession. Seine Motive waren anfangs Landschaften, Großstadtbilder und Stillleben, in seiner Spätzeit schuf er auch Monumentalbilder mit biblischen Motiven.

Der Sohn eines jüdischen Bäckermeisters kam 1873 nach Berlin. Von 1879 bis 1880 studierte Lesser Ury bei Andreas Müller und Heinrich Lauenstein an der Kunstakademie Düsseldorf Malerei, anschließend in Brüssel. Er sammelte in Paris wertvolle Erfahrungen unter anderen bei Jules-Joseph Lefebvre, erkundete Flandern und München. Dort bewarb er sich erfolgreich an der Akademie der Bildenden Künste, wo er am selben Tag aufgenommen wurde wie Ernst Oppler. Noch vor Oppler zog Ury bereits 1887 nach Berlin, von 1920 bis zu seinem Tode hatte er Atelier und Wohnung am Nollendorfplatz 1 in Berlin-Schöneberg. 1890 hatte Lesser Ury auf Empfehlung von Adolph Menzel den Michael-Beer-Preis erhalten, der mit einem Stipendium der Berliner Akademie der Künste einherging. Dies ermöglichte ihm eine mehrmonatige Reise durch Italien, mit Aufenthalt in der Villa Strohl-Fern in Rom. Lovis Corinth holte Ury an die Berliner Secession. Ein großer Förderer war der Industrielle Carl Schapira.

Eine Anekdote zur Feindschaft Urys und Liebermanns lautet wie folgt: Ury soll eines Tages in Liebermanns Atelier gestanden und an einem dessen Bilder herumgekrittelt haben. Später verbreitete er, er habe Liebermanns letztes Bild fertiggestellt: „Det darf er“, soll Liebermann darauf geantwortet haben und setzte nach: „Wenn er allerdings behauptet, eines seiner Bilder sei von mir, dann verklag´ ich ihn.“

Zu seinen bevorzugten Motiven gehörten die Straßen Berlins und die Landschaft der Mark Brandenburg. Vor allem für die Weltstadt Berlin empfand Ury ab dem ersten Moment eine ganz besondere Sympathie. Dies schlug sich so sehr in seiner Kunst nieder, dass er zu seinem 60. Geburtstag vom Oberbürgermeister Berlins als „künstlerischer Verherrlicher der Reichshauptstadt“ geehrt wurde.

In der Ölmalerei stellte Ury Blumenbilder, Stillleben sowie die für ihn typischen Kaffeehaus- und Straßenszenen dar. In seinen Pastellen gelang es Ury, Luft- und Lichtspiegelungen in der Landschaft nuancenreich wiederzugeben. Ury, als Mensch eher ein Einzelgänger, beschritt auch in der Kunst einen einzelgängerischen Weg, während die Berliner Zeitgenossen Max Liebermann, Max Slevogt und Lovis Corinth gemeinsame künstlerische Interessen verbanden. Vielleicht aus Konkurrenzgründen war Max Liebermann, dem Präsidenten der Akademie und einflussreichen Wortführer der Kunstszene, der zunehmende Bekanntheitsgrad Urys ein Dorn im Auge: Liebermann versuchte mit allen Mitteln, Urys Karriere zu blockieren. Ury konnte erst regelmäßig und erfolgreich in der Berliner Secession ausstellen, als Corinth Nachfolger Liebermanns wurde. 1921 wurde er Ehrenmitglied der Secession. Ury begab sich in diesem Jahrzehnt mehrmals auf Reisen nach London, Paris und in verschiedene deutsche Städte. Von jeder Reise brachte der Künstler jeweils eine Fülle neuer Bilder mit. Kurz nach einer Parisreise 1928 verschlechterte sich der Gesundheitszustand des Malers durch einen Herzanfall zunehmend. Nationalgalerie und Secession wollten das Lebenswerk Urys zu seinem 70. Geburtstag (1931) ehren, drei Wochen vorher starb der Künstler jedoch in seinem Berliner Atelier.

Galerie:

Am Bahnhof Friedrichstraße, 1888

Unter den Linden nach dem Regen, 1888

Nachtbeleuchtung in Berlin, 1889

Leipziger Straße, 1889

Im Café Bauer, 1895

Abend im Café Bauer, 1898

Damen, einer Droschke entsteigend, 1920

Brücke über den Landwehrkanal, 1920

Hochbahnhof Bülowstraße, 1922

Unter den Linden, 1922

Berliner Straße im Regen, 1925

Tiergartenallee mit Siegessäule, 1925

Ohne Abbildung:

Eine weit verbreitete, aber falsche Legende besagt, dass die Hälfte von Urys Werk durch die Nationalsozialisten und den Zweiten Weltkrieg zerstört worden sei. Nach seinem Tod wurden in seinem Atelier vielmehr eine Vielzahl von Bildern und 30.000 Reichsmark (RM) entdeckt. Die meisten Bilder des Nachlasses wurden im Oktober 1932 beim Auktionshaus Paul Cassirer von Privatleuten ersteigert. Viele seiner Bilder befinden sich daher noch heute in Privatsammlungen.
Ein Beispiel hierfür ist die im April 2014 in der Sendung Kunst und Krempel vorgestellte Pastellzeichnung einer Straßenszene am Berliner Alexanderplatz  aus den 1910er-Jahren, die in der Sendung auf um die 130.000 € geschätzt wurde und Ende Juli 2017 bei der Versteigerung durch Christie’s in London einen Preis von umgerechnet 200.000 € erreichte (mit Auktionsgebühren knapp 250.000 €).[1]
Das Ehrengrab von Lesser Ury befindet sich im Feld G 1, Ehrenreihe auf dem Jüdischen Friedhof Berlin-Weißensee.

Die letzte größere Werkschau fand 1995 im Käthe-Kollwitz-Museum in Berlin statt.

2010 fand in dem Berliner Auktionshaus Villa Grisebach eine Versteigerung einer Hamburger Privatsammlung von Werken des Malers statt.

2014 zeigte das Museum für Kunst und Technik des 19. Jahrhunderts im LA8, Baden-Baden die Ausstellung Lesser Ury und das Licht.[2]