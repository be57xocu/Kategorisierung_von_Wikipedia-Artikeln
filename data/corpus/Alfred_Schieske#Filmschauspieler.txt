Alfred Schieske (* 6. September 1908 in Stuttgart; † 14. Juli 1970 in Berlin) war ein deutscher Schauspieler.

Der Sohn eines Deutschen und einer Französin nahm Schauspielunterricht bei Willy Reichert und gab 19-jährig sein Debüt am Landestheater Stuttgart. Danach spielte er in Heidelberg, Eßlingen am Neckar, Bochum und Köln. 1940 folgte er dem Ruf von Gustaf Gründgens an das Berliner Staatstheater.  Bei Kriegsende kam er noch auf die Gottbegnadeten-Liste.

Nach dem Krieg agierte er zuerst in Köln, ehe er 1947 in Berlin ein Engagement am Theater am Schiffbauerdamm antrat, wo er bis 1950 arbeitete. Dann wirkte er am Schillertheater und Schlossparktheater in West-Berlin, außerdem in Düsseldorf, Recklinghausen, Jagsthausen sowie bei Gastspielen.

Zu seinen Rollen gehörten Milota in König Ottokars Glück und Ende, Klesel in Ein Bruderzwist in Habsburg, Oberst Henry in Die Affäre Dreyfus (von Wilhelm Herzog), Phil Cook in Ein Mädchen vom Lande (von Clifford Odets), Bolingbroke in Richard II., Wladimir in Warten auf Godot, Clarence in Richard III., Tobias in Was ihr wollt, Adam in Der zerbrochne Krug, Götz in Götz von Berlichingen und Big Daddy in Die Katze auf dem heißen Blechdach. Ab 1961 hatte er in Berlin und Hamburg große Erfolge als Vater Dolittle im Musical My Fair Lady.

Seit 1941 war Schieske auch in Spielfilmen zu sehen. Seine bedeutendste Rolle erhielt er 1948 in dem DEFA-Streifen Affaire Blum als Kriminalist Otto Bonte, der den angeklagten Juden Blum rettet und den wahren Täter überführt. In den 60er Jahren profilierte er sich vor allem als Fernsehdarsteller in Literaturverfilmungen wie Wer einmal aus dem Blechnapf frißt und Jeder stirbt für sich allein mit Edith Schultze-Westrum und Anneli Granget, nach Hans Fallada. Sein Sohn Geriet Schieske (* 1945) wurde ebenfalls Schauspieler.

Alfred Schieske starb im Alter von 61 Jahren und ruht auf dem Friedhof Zehlendorf im gleichnamigen Berliner Ortsteil.[1]