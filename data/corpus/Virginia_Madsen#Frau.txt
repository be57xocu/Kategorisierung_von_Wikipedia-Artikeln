Virginia Madsen (* 11. September 1961 in Chicago, Illinois) ist eine US-amerikanische Schauspielerin.

Nach Absolvierung der Highschool besuchte Madsen das Ted Liss Studio for Performing Arts in Chicago und das Harand Camp Adult Theater Seminar in Elkhart Lake, Wisconsin. Ihr älterer Bruder Michael sowie dessen Sohn Christian sind ebenfalls Schauspieler.

Mitte der 1980er Jahre, als sie eine auf die Rolle der Verführerin festgelegte Schauspielerin war, gelang ihr nicht der Durchbruch. Nachdem sie Gastrollen in Serien wie CSI: Miami übernommen hatte, schaffte sie ein Comeback im Film Sideways. Für ihre Leistung in der melancholischen Komödie erhielt sie Nominierungen sowohl für den Oscar als auch für den Golden Globe Award. Für ihre Rolle in Candyman’s Fluch erhielt sie 1993 einen Saturn Award in der Kategorie Beste Schauspielerin.

Von 1989 bis 1992 war Madsen mit dem Schauspieler und Regisseur Danny Huston verheiratet. Mit dem Schauspieler Antonio Sabato junior hat sie einen gemeinsamen Sohn.
