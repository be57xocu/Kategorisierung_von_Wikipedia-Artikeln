Antonia Gordiana war die Tochter des römischen Kaisers Gordian I., die Schwester des Kaisers Gordian II. und die Mutter des Kaisers Gordian III.

Der in der Historia Augusta zugeschriebene Name Maecia Faustina wird von modernen Historikern als fiktiv abgelehnt. Auch die übrigen Angaben des notorisch unzuverlässigen Geschichtswerks zu Gordianas Biografie sind mit Vorsicht zu genießen. Demzufolge wuchs sie zusammen mit ihrem älteren Bruder Gordian im früheren Haus des Pompeius in Rom auf, das später unter anderem Marcus Antonius und Tiberius gehört hatte. Über ihre Mutter soll sie eine Urenkelin des Herodes Atticus gewesen sein. 

Nach 214 heiratete Gordiana einen Senator, der in der Historia Augusta den (wohl ebenfalls erfundenen) Namen Iunius Balbus trägt und vor 238 verstorben sein soll. In diesem Jahr wurde ihr 13-jähriger Sohn auf Druck der Bevölkerung von den Senatskaisern Pupienus und Balbinus zum Caesar und nach deren (vielleicht von Gordiana angestifteten) Ermordung am 29. Juli zum Augustus erhoben. 

Um sich der Gunst des Senats zu versichern, nahm der junge Kaiser, dessen Geburtsname unbekannt ist, den Namen Marcus Antonius Gordianus an, den auch sein Großvater und sein Onkel getragen hatten. Mit Hilfe deren ehemaliger Unterstützer konnte Gordiana zu Beginn der Regierungszeit ihres Sohnes Einfluss auf die Staatsgeschäfte nehmen.

241 verheiratete Gordiana ihren Sohn mit Furia Sabinia Tranquillina, der Tochter des Prätorianerpräfekten Timesitheus, der dadurch zum De-facto-Regenten des Imperiums aufstieg. Unklar ist, ob und wie lange sie ihren 244 im Orient ums Leben gekommenen Sohn überlebt hat und durch welche Umstände sie gestorben ist.
