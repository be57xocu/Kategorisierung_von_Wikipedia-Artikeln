Anaïs Nin (* 21. Februar 1903 als Angela Anaïs Juana Antolina Rosa Edelmira Nin y Culmell in Neuilly-sur-Seine bei Paris; † 14. Januar 1977 in Los Angeles) war eine amerikanische Schriftstellerin.

Anaïs Nin war die Tochter des kubanisch-spanischen Komponisten und Konzertpianisten Joaquín Nin y Castellanos und der Dänin Rosa Culmell y Vigaraud, die französische und kubanische Vorfahren hatte. Als sie elf Jahre alt war, verließ der Vater die Familie. Die Mutter siedelte daraufhin mit den drei Kindern nach New York über. Dort brach Anaïs Nin die Schule ab und arbeitete zeitweise als Modell. Die Trennung vom Vater versuchte sie in ihrem Tagebuch zu verarbeiten.

1923 heiratete sie den Bankangestellten Hugh Parker Guiler, der später als Filmproduzent Ian Hugo bekannt wurde. Mit ihm zog sie 1924 nach Paris. Gemeinsam mit ihrem Mann unterstützte sie avantgardistische Künstler und lernte so auch Henry Miller kennen, auf dessen literarisches Schaffen sie großen Einfluss hatte; darüber entstand 1990 Philip Kaufmans Film Henry & June. Mit Miller hatte sie eine langjährige Affäre.[1]
Nach dem deutschen Sieg über Frankreich 1940 kehrte sie in die USA zurück. 1947 folgte eine heimliche Eheschließung mit dem siebzehn Jahre jüngeren Rupert Pole. Mit ihm lebte sie in Los Angeles, in New York weiterhin mit Hugh Guiler.

Literarisch bekannt ist Nin vor allem durch ihre Tagebücher, die sie schon in jungen Jahren zu schreiben begonnen hat: Romane und erotische Erzählungen mit äußerst explizit beschriebenen sexuellen Handlungen, die u. a. in Das Delta der Venus zusammengefasst sind. In dem Roman Ein Spion im Haus der Liebe hat man eine andere Art Selbstporträt von Anaïs Nin. Es ergänzt das Bild, das man sich inzwischen von der Freundin und Förderin Millers und Artauds gemacht hat.

In ihren Werken mischen sich Biografie und Fiktion. Traumleben und das Unbewusste spielen eine wesentliche Rolle; sie war von den psychoanalytischen Theorien von Sigmund Freud und Carl Gustav Jung stark beeinflusst.
Nin studierte Psychoanalyse bei Otto Rank und ließ sich selbst zeitweise von Carl Gustav Jung behandeln.

1994 wurde der Venuskrater Nin nach ihr benannt.

Weitere Bücher mit Tagebucheinträgen
