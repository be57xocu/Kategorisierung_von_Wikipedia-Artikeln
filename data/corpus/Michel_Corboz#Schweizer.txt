Michel Corboz (* 14. Februar 1934 in Marsens, Kanton Freiburg, Schweiz) ist ein Schweizer Dirigent und Komponist.

Michel Corboz besuchte zuerst in Freiburg (Schweiz) das Lehrerseminar und studierte dann am dortigen Konservatorium Gesang und Theorie sowie im Institut Ribaupierre in Lausanne Komposition. 1961 gründete er das Ensemble vocal et instrumental de Lausanne, dessen Leiter er auch ist und mit welchem er zahlreiche Konzerte im In- und Ausland gegeben sowie Plattenaufnahmen gemacht hat.
