Thea Dorn (* 23. Juli 1970 in Offenbach am Main, eigentlich Christiane Scherer)[1][2] ist eine deutsche Schriftstellerin, Dramaturgin und Fernsehmoderatorin.

Thea Dorn wuchs in Frankfurt am Main als Tochter von zwei Volkswirten auf und hat einen Bruder.[2] Nach dem Abitur am Lessing-Gymnasium begann sie eine Gesangsausbildung; später studierte sie Philosophie und Theaterwissenschaft in Frankfurt, Wien und Berlin. An der Freien Universität Berlin legte sie die Magisterprüfung in Philosophie mit einer Arbeit über Selbsttäuschung ab. Anschließend arbeitete sie als wissenschaftliche Mitarbeiterin an der FU Berlin. Später wurde sie Dramaturgin und Autorin am Schauspielhaus Hannover. Ihren Künstlernamen hat sie in Anspielung auf den Philosophen Theodor W. Adorno gewählt.[3]
Charakteristisch für ihre Krimis ist die plastische Auseinandersetzung mit Gewaltszenen. Mit Die Brut (2004) löste Dorn sich vom Krimi-Genre und konzentrierte sich auf kritisch-analysierende Milieucharakterisierungen. Im Februar 2008 erschien Mädchenmörder, der die ungewöhnliche Beziehung eines Opfers zum kaltblütigen Täter beschreibt.

2000 schrieb Dorn das Theaterstück Marleni, die Inszenierung einer Begegnung von Marlene Dietrich und Leni Riefenstahl. Das Drama wurde am 15. Januar 2000 am Deutschen Schauspielhaus Hamburg uraufgeführt. Zu den Folgen Der schwarze Troll (2003) und Familienaufstellung (2007, Ausstrahlung 2009) der ARD-Reihe Tatort verfasste sie die Drehbücher.

Im September 2006 erschien ihr erstes Sachbuch Die neue F-Klasse. Es enthält zwei Essays und elf Gespräche mit Frauen von Charlotte Roche bis Silvana Koch-Mehrin.

Von 2003 bis 2004 moderierte sie zusammen mit Dirk Schümer die Sendung Schümer und Dorn. Der Büchertalk im SWR. Seit Oktober 2004 führt sie durch die Sendung Literatur im Foyer, seit November 2008 im Wechsel mit Felicitas von Lovenberg. Von Januar 2008 bis Dezember 2009 moderierte Dorn im Wechsel mit der französischen Journalistin Isabelle Giordano die Talkshow Paris-Berlin auf dem deutsch-französischen Fernsehsender ARTE.
Unter dem Motto Hinaus ins Ungewisse! kuratierte Thea Dorn das forum:autoren beim Literaturfest München 2012.[4]
Seit März 2017 ist Dorn festes Ensemblemitglied in der ZDF-Sendung Das Literarische Quartett;[5] von 2017 bis 2019 wird sie als Jurorin beim Bayerischen Buchpreis amtieren.[6]
Thea Dorn bezeichnet sich selbst als Agnostikerin und ist Mitglied im PEN-Zentrum Deutschland. Sie lebt in Berlin.[7]
Artikel, Interviews
