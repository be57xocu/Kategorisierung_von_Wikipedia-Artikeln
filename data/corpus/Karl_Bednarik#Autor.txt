Karl Bednarik (* 18. Juli 1915 in Wien; † 14. Jänner 2001 ebenda) war ein Wiener Maler und Schriftsteller mit sozialkritischem Engagement.

Bednarik bekämpfte sowohl den Austrofaschismus als auch den Hitler-Faschismus.
Karl Bednarik erlernte den Beruf des Buchdruckers, war dann jedoch unter anderem als Elektroschweißer tätig. Daneben bildete er sich autodidaktisch als Künstler weiter. Im Jahr 1946 hatte er seine erste Ausstellung als Maler, auch die Albertina erwarb einige seiner Bilder.[1]
In seinem utopischen Roman Omega Fleischwolf beschreibt Bednarik eine Arbeiterschaft, die sich vom Kampf für den gesellschaftlichen Fortschritt abwendet und nur an individuellen Vorteilen interessiert ist.[2]
Karl Bednarik wurde am Friedhof der Feuerhalle Simmering in Wien bestattet. Seit Februar 2006 gibt es im 22. Wiener Gemeindebezirk eine Karl-Bednarik-Gasse. 

Bednariks Nachlass befindet sich im Literaturarchiv der Österreichischen Nationalbibliothek.
Bedeutende Teile des künstlerischen Nachlasses von Karl Bednarik gingen 2016/17 aufgrund einer Schenkung der Familie an die MUSA Museum Startgalerie Artothek.[3]