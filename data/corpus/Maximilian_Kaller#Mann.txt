Maximilian Josef Johann Kaller (* 10. Oktober 1880 in Beuthen, Oberschlesien; † 7. Juli 1947 in Frankfurt am Main) war  Bischof von Ermland in Ostpreußen.

Maximilian Kaller war das zweite von acht Kindern einer Kaufmannsfamilie. 1899 legte er das Abitur ab. Danach begann er seine theologische Ausbildung in Breslau. 1903 empfing er ebendort die Priesterweihe. Zunächst war er Kaplan in Groß Strehlitz, seine erste Pfarrstelle trat er als Missionspfarrer der St. Bonifatius-Kirchengemeinde auf Rügen an.

Ab 1917 war er Pfarrer von St. Michael in Berlin-Mitte und -Kreuzberg. 1926 wurde er zum Administrator der Apostolischen Administratur Schneidemühl ernannt.

Kaller war Ehrenmitglied der Katholischen Studentenverbindungen Normannia in Greifswald und Ermland (Warmia) in München im KV.

1930 wurde Maximilian Kaller zum Bischof von Ermland gewählt. Die Bischofsweihe spendete ihm am 28. Oktober 1930 der damalige Apostolische Nuntius beim Deutschen Reich, Cesare Orsenigo; Mitkonsekratoren waren Edward Aleksander Wladyslaw O’Rourke, Bischof von Danzig, und Johannes Hillebrand, Weihbischof in Paderborn. Sein Wahlspruch lautete: lateinisch „Caritas Christi urget me“ („die Liebe Christi drängt mich“). Er trat seine Stellung in seiner Diözese in Frauenburg an.

Kallers Wirken während der NS-Zeit ergibt ein differenziertes Bild. In der Anfangsphase des NS-Staates geriet Kaller mehrfach in Gegensatz zum Regime: So organisierte er Diözesanwallfahrten u. a. nach Dietrichswalde, dem Marienwallfahrtsort der polnischsprachigen Minderheit im Ermland. Kallers Vorgänger Thiel und Bludau hatten eine Teilnahme an derartigen Wallfahrten hingegen stets vermieden. Im September 1934 hielt Kaller die Kirchweihpredigt auf Deutsch und nach der Messe auf Polnisch. Eine im November 1934 auf Polnisch gehaltene Predigt, die mit den Worten „Geliebtes polnisches Volk“ begann, brachte ihm eine Beschwerde des ostpreußischen Gauleiters Erich Koch in Berlin ein.

In einem Hirtenbrief Kallers vom April 1935 heißt es: „Die katholische Kirche Ostpreußens befindet sich zur Zeit in schwerster Bedrängnis. […] Ein Sturmbefehl der SA fordert zum Austritt aus den katholischen Vereinen auf unter Androhung sofortiger Entlassung. Unsere katholische Aktion ist des Hochverrats beschuldigt.“

Die Auseinandersetzung ging im Jahr 1937 weiter, als zum einen Kallers Hirtenwort zur Fastenzeit beschlagnahmt, sowie zum anderen die Druckerei der Ermländischen Zeitung enteignet wurde, nachdem dort 30.000 Exemplare der Enzyklika Mit brennender Sorge von Papst Pius XI. gedruckt worden waren.

Im weiteren Verlauf des Jahres 1937 kam es zu Verhaftungen und Verurteilungen von Geistlichen und Laien des Bistums. Alle katholischen Vereine wurden verboten. Im Fastenhirtenbrief von 1938 sagt Kaller dazu: „Wir sind vogelfrei; andere dürfen uns höhnen und lästern. Wir dürfen kein Wort der Erwiderung bringen. Von Gewissensfreiheit kann nicht mehr die Rede sein.“

Seit 1939 lässt sich jedoch eine Änderung in Kallers Linie erkennen. Das Regierungspräsidium Allenstein verlangte eine Reduzierung der polnischen Gottesdienste in der Allensteiner Sankt-Jakobi-Kirche, woraufhin Kaller den Erzpriester Hanowski im August 1939 entsprechend anwies: „Hierdurch ordne ich an, daß angesichts der unruhigen gespannten Zeitverhältnisse in allen Städten der Diözese bis auf weiteres von polnischen Predigten und polnischem Gesang Abstand zu nehmen ist.“

Am 25. Januar 1941 erklärte er in einem Hirtenwort ausgesprochen regimetreu: „Wir bekennen uns freudig zur deutschen Volksgemeinschaft und fühlen uns mit ihr untrennbar verbunden in guten wie in trüben Tagen […] In diesem echt christlichen Geist durchleben wir nun auch mit der Teilnahme unseres ganzen Herzens den großen Kampf unseres Volkes um Sicherung seines Lebens und seiner Geltung in der Welt. Mit Bewunderung schauen wir auf unser Heer, das im heldenhaften Ringen unter hervorragender Führung beispiellose Erfolge erzielt hat und weiterhin erzielt. Wir danken Gott für seinen Beistand. Gerade als Christen sind wir entschlossen, unsere ganze Kraft einzusetzen, damit der endgültige Sieg unserem Vaterland gesichert werde. Gerade als gläubige, von der Liebe Gottes durchglühte Christen stehen wir treu zu unserem Führer, der mit sicherer Hand die Geschicke unseres Volkes leitet.“

Am 7. Februar 1945 wurde er von der SS wegen der drohenden Einnahme des Gebietes durch die Rote Armee zwangsweise aus dem Ermland deportiert.

Bischof Kaller und viele Zivilisten kehrten nach Abflauen der militärischen Kampfhandlungen zurück in das Ermland. Jedoch wurde Kaller vom polnischen Primas Kardinal August Hlond während der Vertreibungen unter Bruch des geltenden Kirchenrechts zum Verzicht auf seine Amtsausübung gezwungen. Er ließ sich danach in Westdeutschland nieder.

1946 wurde er von Papst Pius XII. als Päpstlicher Sonderbeauftragter für die Heimatvertriebenen berufen. Am 7. Juli 1947 starb Bischof Kaller plötzlich an einem Herzinfarkt in Frankfurt am Main. Er wurde am 10. Juli 1947 neben der Pfarrkirche St. Marien in Königstein im Taunus begraben.

50 Jahre später wurde eine Erinnerungsfeier zu Ehren Bischof Kallers mit dem heutigen polnischen Bischof von Warmia/Ermland und der Gemeinde aus Deutschland abgehalten. Je eine Büste von Bischof Kaller, die Erika Maria Wiegand im Jahr 1980 schuf, wurde im Dom von Frauenburg und in Deutschland aufgestellt.

Maximilian Kaller ist eine der drei Figuren auf dem Denkmal für die Königsteiner Kirchenväter in Königstein im Taunus. Das Denkmal wurde von Christoph Loch entworfen und am 1. September 2011 eingeweiht.

Seit den 1990er Jahren bemüht sich die Bischof-Maximilian-Kaller-Stiftung e. V., das Andenken an Bischof Kaller wach zu halten und den Seligsprechungsprozess zu unterstützen. Am 4. Mai 2003 wurde der Seligsprechungsprozess bei der Wallfahrt der Ermländer in Werl eröffnet.
