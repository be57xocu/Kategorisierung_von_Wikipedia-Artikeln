Alex Randolph (* 4. Mai 1922 in Böhmen, nahe Brünn[1]; † 27. April 2004 in Venedig) war der Erfinder von weit mehr als hundert Gesellschaftsspielen (Spieleautor).

Mit Spielen wie Twixt, Sagaland, Inkognito, Hol’s der Geier, Rasende Roboter, Tempo, kleine Schnecke und vielen anderen zum Teil preisgekrönten Ideen begeisterte Randolph Generationen von kleinen und großen Spielern.

Weil seine Spiele stark nachgefragt waren, konnte er bei den Produzenten erreichen, dass sein Name auf der Verpackung als Spieleautor genannt wurde. Sein Beispiel setzte sich in der Folge vielfach durch. „Der Grandseigneur der Spielekultur galt vor allem in Deutschland als Guru der Branche“, so der Spiegel.[2]
Die Mutter des 1922 in Böhmen geborenen Randolphs war eine aus Colorado stammende Amerikanerin, sein Vater Russe. Nach einigen Jugendjahren in Venedig wurde er im Alter von zehn Jahren auf ein Internat in Champéry geschickt, wo er auch die französische, spanische und italienische Sprache lernte. 1938 kehrte er mit der Familie in die Heimat der Mutter zurück.[1]
Während des Zweiten Weltkrieges war Randolph Agent des US-Nachrichtendienstes, für den er feindliche Codes entschlüsselte. Anschließend schrieb er Romane und Werbetexte in Boston. Spiele hat Randolph schon früh erfunden; er dachte aber nicht daran, dass man diese auch verkaufen könnte. Etwa 1959 wurde einer seiner Kunden auf eines seiner an der Wand hängenden selbst entwickelten Spiele aufmerksam. Nachdem Pan-Kai – ein Spiel  mit Pentomino-Steinen – 1961 beim Verlag Phillips veröffentlicht wurde, konzentrierte er sich ganz auf sein Hobby, dem Spiele erfinden. Randolph lebte dann in Wien und entwickelte 1961 im Café Hawelka das Spiel Twixt, welches 1962 veröffentlicht und ein Bestseller wurde.[3][4][5][6][7][8]
Von dem Geld, welches er durch Twixt eingenommen hatte, finanzierte er sich eine Studienreise nach Japan um dort Shōgi, eine Variante von Schach zu lernen. Er liebte klassische Denkspiele und spielte begeistert Schach und Go. Viele seiner damals veröffentlichten Spiele sind taktische Spiele für zwei. Randolph lebte sieben Jahre in Japan, ehe er schließlich Mitte der 1970er nach Venedig zog.[4]
Randolph entwickelte nun mehr Familienspiele, so z.B. 1981 Sagaland, 1985 Tempo, kleine Schnecke, 1988 Hol’s der Geier und Inkognito, 1989 Gute Freunde. Sagaland wurde Spiel des Jahres 1982 und das mit Leo Colovini entwickelte Spiel Inkognito erhielt den Sonderpreis „Schönes Spiel“ 1988 und Gute Freunde den Sonderpreis „Kinderspiel“ von der Jury zum Spiel des Jahres.

Randolph erhielt 1992 den Sonderpreis zum 70. Geburtstag und für sein Lebenswerk als Spieleautor beim Deutschen Spiele Preis.

1995 gründete er gemeinsam mit Tom Kremer, Phil E. Orbanes und Mike Meyers den US-amerikanischen Spieleverlag Winning Moves. Im selben Jahr gründete Randolph mit Leo Colovini und Dario de Toffoli den italienischen Verlag Venice Connection.[9]
Auch 1996 und 1997 erhält Randolph für die Spiele Venice Connection bzw. Leinen los! den Sonderpreis „Schönes Spiel“ bzw. den Sonderpreis „Kinderspiel“ von der Jury zum Spiel des Jahres. 1999 erscheint das erfolgreiche Spiel Rasende Roboter.

Randolph starb in seiner Wahlheimat Venedig im Alter von fast 82 Jahren; bis zum Schluss hat er an neuen Spielideen gearbeitet.

Nach ihm ist der seit 2005 von der Spieleautorenzunft SAZ verliehene Medienpreis Alex benannt.
