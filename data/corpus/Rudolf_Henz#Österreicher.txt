Rudolf Henz (Pseudonym: R. Miles) (* 10. Mai 1897 in Göpfritz an der Wild (Niederösterreich); † 12. Februar 1987 in Wien) war ein österreichischer Schriftsteller und Programmdirektor des Österreichischen Rundfunks.

Rudolf Henz trat 1908 gemeinsam mit Jakob Kern in das Knabenseminar Hollabrunn ein und legte 1916 die Kriegsreifeprüfung am k. k. Staatsgymnasium Hollabrunn ab. Danach besuchte er die Kadettenschule an der theresianischen Militärakademie. Nachdem er seinen Kriegsdienst geleistet hatte, studierte er an der Universität Wien die Fächer Germanistik und Kunstgeschichte. 1923 promovierte er zum Dr. phil.

In den kommenden Jahren arbeitete er in der katholischen Volksbildung. Im Jahr 1931 wurde er Direktor der wissenschaftlichen Abteilung der RAVAG und führte 1932 den Schulfunk ein. 

Nach der Ermordung des österreichischen Bundeskanzlers Engelbert Dollfuß im Zuge des gescheiterten Juliputsches 1934 verfasste Henz im Auftrag dessen Nachfolgers Kurt Schuschnigg den Text für eine Hymne Lied der Jugend der austrofaschistischen Bewegung mit dem Titel Ihr Jungen schließt die Reihen gut, die politisch gegen das Horst-Wessel-Lied eingesetzt wurde.

Nach dem „Anschluss“ Österreichs an das Deutsche Reich verlor er 1938 seinen Posten. Henz wurde freier Schriftsteller und verdiente nebenbei als Glasmaler etwas Geld. In den Jahren 1945 bis 1957 war er Programmdirektor des Österreichischen Rundfunks. Er gründete 1947 die Wiener und 1948 die österreichische Katholische Aktion. 1952 war er Präsident des österreichischen Katholikentages. Von 1950 bis 1952 gab er die Literaturzeitschrift Dichtung der Gegenwart, ab 1955 die Literaturzeitschrift Wort in der Zeit heraus. Von 1967 bis 1980 war Rudolf Henz Präsident des Österreichischen Kunstsenats.

Als sein Hauptwerk gilt das Vers-Epos Der Turm der Welt (1952), an dem er über viele Jahre hinweg arbeitete. [1]
Er ruht in einem Ehrengrab auf dem Wiener Zentralfriedhof (Gruppe 33 G, Nummer 75).

