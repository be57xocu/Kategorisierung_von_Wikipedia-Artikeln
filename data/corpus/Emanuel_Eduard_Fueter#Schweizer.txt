Emanuel Eduard Fueter (ausgesprochen [fuətər];[1] * 2. Mai 1801 in Bern; † 30. April 1855 ebenda) war ein Schweizer Arzt und Medizinprofessor.

Er wurde 1832 Professor für Pathologie und Therapie an der Akademie Bern. Von 1834 bis 1855 wirkte er als außerordentlicher Professor für Poliklinik und spezielle Therapie an der Universität Bern. 1835 wurde er in den  Grossen Rat gewählt.

Auf Fueters Anregung hin schrieb sein Jugendfreund Jeremias Gotthelf 1843/1844 den zweibändigen Roman Anne Bäbi Jowäger haushaltet und wie es ihm mit dem Dokteren geht als Mahnschrift gegen Quacksalberei.
