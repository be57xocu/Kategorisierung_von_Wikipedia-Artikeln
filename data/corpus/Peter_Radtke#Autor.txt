Peter Radtke (* 19. März 1943 in Freiburg im Breisgau) ist ein deutscher Autor und Schauspieler.

Von 1957 bis 1961 absolvierte Peter Radtke eine Dolmetscherausbildung in Englisch, Französisch und Spanisch an einer privaten Fremdsprachenschule in Regensburg. Des Weiteren legte er 1963 die Externenprüfung der Universität Pennsylvania mit Erwerb des „Certificate in American Culture and Civilization“ ab. Sein Abitur erarbeitete er sich von 1964 bis 1968 über den Zweiten Bildungsweg am Abendgymnasium Regensburg, danach studierte er von 1968 bis 1976 Germanistik und Romanistik an den Universitäten Regensburg und Genf mit abschließendem Erstem Staatsexamen und Promotion.Von 1977 bis 1984 war Peter Radtke Fachgebietsleiter für das „Behindertenprogramm“ der Münchner Volkshochschule und von 1984 
bis 2008 war er Geschäftsführer und Leitender Redakteur der Arbeitsgemeinschaft Behinderung und Medien. Von 2003 an war er Mitglied im Nationalen Ethikrat, seit 2008 ebenfalls in dessen Nachfolger Deutscher Ethikrat. Peter Radtke hat Glasknochen und sitzt im Rollstuhl.

Radtke ist Träger des Bundesverdienstkreuzes am Bande sowie des Bundesverdienstkreuzes 1. Klasse. Neben dem Bayerischen Verdienstorden wurde ihm auch die Bayerische Verfassungsmedaille in Silber verliehen. Darüber hinaus wurde er mit dem Kulturförderpreis der Stadt Regensburg, dem Kulturpreis der Stadt Regensburg und dem Medienpreis der Bundesvereinigung Lebenshilfe geehrt.

Außerdem schrieb er Theaterstücke, Hörspiele und weitere Artikel und hatte auch großen Erfolg als Schauspieler mit Bericht an eine Akademie von Franz Kafka an den Münchner Kammerspielen, an denen er auch als Stalin zu sehen war. Er arbeitete u. a. mit Tabori zusammen.

In der Verfilmung des Romans Die Rättin von Günter Grass spielt Radtke den Matzerath. Eine weitere Filmrolle spielte er in Michael Verhoevens Mutters Courage.
