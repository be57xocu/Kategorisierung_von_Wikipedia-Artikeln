Louis Conne (* 2. Dezember 1905 in Zürich; † 6. Juni 2004 ebenda) war ein Schweizer Bildhauer und Grafiker.

Nach seiner Lehre als Bildhauer, arbeitete er in Paris an der Académie de la Grande Chaumière bei Antoine Bourdelle. Louis Conne war Mitglied in der Künstler-Vereinigung Abstraction-Création. Von 1946 bis 1971 arbeitete er als Lehrer an der Kunstgewerbeschule Zürich.
