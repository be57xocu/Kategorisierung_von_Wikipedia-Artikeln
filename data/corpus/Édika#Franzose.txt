Édika (* 27. Januar 1940 in Heliopolis/Kairo, Ägypten) ist der Künstlername des französischen Comiczeichners und -autors Édouard Karali. 

Édika begann seine berufliche Karriere in der Werbebranche in Ägypten, zog dann aber Ende der 1970er Jahre nach Frankreich, wo er schon bald erste Veröffentlichungen seiner Comics im Charlie Mensuel feiern konnte. Édikas Familie hat mittlerweile einige bekannte Namen der französischen Szene hervorgebracht, so auch Édikas kleinen Bruder Carali, in dessen Comicmagazin Psykopat Édika später auch veröffentlichte. Hauptsächlich arbeitete Édika jedoch für das Magazin Fluide Glacial.

Édika setzt sich in seinen Comics häufig selbst unter dem Namen Bronsky Proko in Szene. Seine Frau Olga, der Sohn Paganini, die Tochter Georges und die Katze Clark Gaybeul stellen seine Familie dar, und auch die Redaktion des Fluide Glacial bekommt häufig Auftritte in den Geschichten des Franzosen. 1979 erschienen erste Beiträge unter dem Namen „Edi“ im Fluide Glacial. 1981 erschien das erste Édika-Album in Frankreich (Debiloff profondikoum). In den 80er und 90er Jahren erschienen mehrere Édika-Bände in Deutschland. In Deutschland erschienen seine Comics seit den frühen 80er Jahren im Magazin U-Comix. Das Magazin aus dem Volksverlag litt unter häufiger Indizierung durch die Bundesprüfstelle für jugendgefährdende Medien, was nicht selten an den darin von Édika veröffentlichten Beiträgen lag. 

Édika prägt einen quirligen, teils wirr wirkenden Zeichenstil. 

Seine Geschichten eskalieren oft in wilden Sexszenen, die, wie die Geschichten überhaupt, absurd verlaufen. Nicht wenige Geschichten enden ohne Pointe.

Édika legt keinen Wert darauf, die Schönheit des Menschen darzustellen oder erotische Geschichtchen zu verfassen. Er arbeitet -in seinem Rahmen- immer am äußersten Extrem. Bei ihm gibt es keine sexuellen Andeutungen, sondern gleich Überzeichnungen. Seine Charaktere, gelegentlich auch Tiere, sind endlos schwach und geben sich stets ihren Trieben hin, meist auf skurrile, überzeichnete Art.

Édika kommt völlig ohne chauvinistische Inhalte aus. Gewalt -wie extrem auch immer- ist bei ihm immer als komisch grotesk erkennbar, dient nie irgendwelchen Machtfantasien, sondern stets dem komischen. Sein Frauenbild verbindet das hilflos hässliche, mit dem unwiderstehlich anziehenden: Männer wie Frauen, wie Kinder wie Tiere sind bei ihm allesamt liebenswerte Nasenmännchen, Opfer ihrer Umstände.

Édika lässt das Thema Politik außen vor. Seine satirischen Personenbeschreibungen, nach deren Lektüre der Leser niemanden mehr ernst nehmen können soll, sind ihm Statement genug.

Äußeres Merkmal sind die langen Nasen der Charaktere, nicht unähnlich denen der Werner-Comics. Typisch sind auch die riesigen Textmengen, die er in den Sprechblasen unterbringt. Nötigenfalls verkleinert er die Schrift zu Gekritzel, um anzudeuten, dass es noch endlos fortfahren könnte. 

Da er immer wieder die fiktive Ebene des Comic-Geschehens verlässt, um Figuren aus den Bildrahmen klettern zu lassen oder darzustellen, dass er noch einige Seiten zeichnen muss, um sein Gehalt zu bekommen, lässt er auch zum Ende einiger Geschichten die Bildrahmen immer kleiner werden, weil er die Geschichte eben im gegebenen Seitenumfang nicht zu Ende erzählen kann.
