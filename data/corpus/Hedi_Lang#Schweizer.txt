Hedwig «Hedi» Lang-Gehri (* 30. Oktober 1931 in Uster; † 31. März 2004 in Zollikerberg)  war eine Schweizer Politikerin. Sie präsidierte als zweite Frau nach Elisabeth Blunschy (1977) den Nationalrat. 

Hedi Lang war Mitglied der Sozialdemokratischen Partei. 1971, kurz nach Einführung des Frauenstimmrechts, wurde sie in den Nationalrat gewählt (bis 1983). 1981 wurde sie Nationalratspräsidentin.

1983 wurde sie in den Regierungsrat des Kantons Zürich gewählt - auch das eine Premiere für eine Frau. Bis 1991 leitete sie die Direktion der Justiz und des Innern, danach stand sie bis 1995 der Volkswirtschaftsdirektion vor. Zweimal (1989/90 und 1994/95) war sie auch Präsidentin des Regierungsrates. 1973 bis 1983 war sie Präsidentin des Verbandes Pro Familia Schweiz.

Hedi Lang war ab 1957 verheiratet mit Erwin A. Lang.
