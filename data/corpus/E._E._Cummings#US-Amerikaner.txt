Edward Estlin Cummings (* 14. Oktober 1894 in Cambridge; Massachusetts; † 3. September 1962 in North Conway, New Hampshire) war ein US-amerikanischer Dichter und Schriftsteller.

Cummings entstammte einer sehr liberalen Familie. Sein frühes Interesse an Lyrik (bereits 1904, als Zehnjähriger, soll er Gedichte geschrieben haben), wurde von seinen Eltern gefördert. Von 1911 bis 1915 studierte er in Harvard Literatur, wo er in der Redaktion der Universitätszeitung John Dos Passos begegnete, mit dem er sich anfreundete. 1912 erschien in eben jener Universitätszeitung erstmals einer seiner Texte. In seinem Abschlussjahr kam er in Kontakt mit modernen, avantgardistischen Texten von Autoren wie Gertrude Stein oder Ezra Pound, die ihn beeinflussten.

1917 veröffentlichte er erstmals Gedichte in einer Sammlung. Kurz darauf meldete er sich freiwillig als Sanitäter und ging nach Frankreich, wo er am Ersten Weltkrieg teilnahm. Irrtümlich wurde er dort für einen Verräter gehalten und für vier Monate in einem Lager in der Normandie interniert. Diese Erfahrung bildete die Grundlage seines Romans Der ungeheure Raum. Nach Kriegsende blieb Cummings in Paris, wo er unter anderem Pablo Picasso begegnete, der ihn sehr beeindruckte.

1923 erschien sein erster Gedichtband, Tulips & Chimneys. In den 1920er und 1930er Jahren reiste Cummings viel, unter anderem in die Sowjetunion, lebte abwechselnd in den USA und Frankreich und veröffentlichte regelmäßig neue Gedichtbände.

Cummings war intellektuell nie so radikal und unzugänglich wie seine revolutionären Vorbilder, seine schlichte, aber nie einfältige Sicht auf die Welt machte ihn jedoch zu einem der volkstümlichsten modernen Lyriker englischer Sprache.

In seinem künstlerischen Schaffen kritisierte er oftmals die patriotische Verlogenheit des American Dream und den Konformismus des American Way of Life, aber auch den allgegenwärtigen Wahnwitz in der Kulturindustrie und Konsumwelt der „affluent society“, der zu einem bloßen Anhäufen von äußerlichen Scheinwerten führt, wobei das eigentlich Wertvolle verloren geht.[1]
Er war Unterstützer des America First Committees, einer isolationistischen Bewegung, die 1940/41 die Teilnahme der USA am Zweiten Weltkrieg zu verhindern suchte.

Cummings war mehrmals verheiratet. 1924 heiratete er Elaine Orr, die zuvor mit dem Schriftsteller Scofield Thayer verheiratet war. Seit 1918 hatte Cummings eine Affäre mit ihr, aus der seine 1919 geborene Tochter Nancy hervorging. Die Ehe scheiterte nach weniger als einem Jahr. Elaine zog nach Irland, seine Tochter sah Cummings erst 1946 wieder. Seine zweite Ehe mit Anne Minnerly Barton dauerte von 1929 bis 1932. Danach lebte Cummings mit Marion Morehouse zusammen, die Beziehung hielt bis zu seinem Tod 1962.
