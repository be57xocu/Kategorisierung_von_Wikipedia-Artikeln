Arwed Emminghaus, eigentlich Arwed Karl Bernhard Emminghaus (* 22. August 1831 in Niederroßla bei Apolda; † 8. Februar 1916 in Gotha), war ein deutscher Nationalökonom und Journalist.

Arwed Emminghaus wurde als drittes von vier Kindern der Eheleute Justizrat Justus Christian Bernhard Emminghaus (1799–1875) und Amalie Selma, geb. Sturm, geboren und besuchte in Bad Berka die Grundschule.[1] Seine Ausbildung erfuhr Emminghaus nicht nur durch die Schule, sondern auch durch Hauslehrer. In Keilhau besuchte er vier Jahre lang die von Friedrich Fröbel hierher verlegte Allgemeine Deutsche Bildungsanstalt Fröbel. Im Weimarer Ernst-Wilhelm-Gymnasium wurde er ab Sekunda unterrichtet und legte zu Ostern 1851 dort das Abitur ab mit Bestnote.

Emminghaus studierte von 1851 bis 1854 Rechtswissenschaft und Nationalökonomie an der Universität Jena, wurde 1855 zum Dr. jur. promoviert  und absolvierte anschließend die erste juristische sowie die kameralistische Staatsprüfung. Während des Studiums sammelte er praktische Erfahrungen in der Landwirtschaft. Ab 1855 war er im Ministerium des Großherzogtums Sachsen-Weimar-Eisenach in Weimar beschäftigt. Am 31. August 1858 schied er aus dem Staatsdienst aus, behielt aber das Recht einer Rückkehr.

Am 1. September 1858 trat er als Bureauchef in eine Dresdener Feuerversicherungsgesellschaft ein.

Am 19. November 1858 wurde er als Leiter der Schweizer Vertretung in Bern mit dem Aufbau und der Organisation des Versicherungsgeschäftes in der Schweiz beauftragt.

Die Arbeit für die Dresdner Feuerversicherung wurde am 1. September 1859 mit einem eigenen Büro fortgesetzt, sie endete jedoch schon zwei Jahre später, am 30. September 1861.

Im Herbst 1861 übernahm er die Redaktion des Bremer Handelsblattes in Bremen als Nachfolger von Victor Böhmert. In dieser Eigenschaft wurde er zur treibenden Kraft beim Aufbau eines organisierten Seenotrettungswesens in Deutschland. Dies mündete 1863 in der Gründung des Bremer Vereins zur Rettung Schiffbrüchiger, aus dem am 29. Mai 1865 die Deutsche Gesellschaft zur Rettung Schiffbrüchiger (DGzRS) hervorging, deren erster Sekretär (Schriftführer) und langjähriger Berater er war.

Im Herbst 1865 gab er die Redaktionsleitung beim Bremer Handelsblatt an August Lammers ab, um einer Berufung an das Polytechnikum zu Karlsruhe zu folgen, wo er ab 1866 als Professor des neu geschaffenen Lehrstuhls der Wirtschaftslehre lehrte. Zu diesem Zweck übersiedelte die Familie im Frühjahr 1866 nach Karlsruhe. Im gleichen Jahr gründete Emminghaus eine Baugenossenschaft für Lehrer und Beamte in Karlsruhe mit dem Ziel, den Mitgliedern zu günstigen Konditionen einen Haus- und Grunderwerb - Vorläufer der heutigen Wohnungsbaugenossenschaften. Seine Professur behielt Emminghaus bis 1873.

Er siedelte 1873 als Vorsitzender Direktor der Gothaer Lebensversicherungs-Gesellschaft nach Gotha über. In dieser Position übernahm Emminghaus zahlreiche Ämter, war häufig Gründungsmitglied von Vereinen und engagierte sich in vielen Institutionen.

Im Jahre 1901 richtete Emminghaus aus Spenden zu seinem 70. Geburtstag seine "Arwed Emminghaus-Stiftung" ein für gemeinnützige Zwecke.

Am 1. Juli 1903 wurde er pensioniert. Sein Nachfolger wurde als Generaldirektor Karl August Friedrich Samwer. Emminghaus starb am 8. Februar 1916 in Gotha, auf dessen Hauptfriedhof die Grabstätte der Familie ist.

Die Ehe mit Karoline Luise Alberti († 16. April 1907) wurde am 10. Mai 1859 in Hohenleuben bei Gera geschlossen, aus der folgende Kinder hervorgingen:

Im Dezember 1875 starb Arweds Vater, Bernhard Emminghaus, in Weimar.

Emminghaus war Autor zahlreicher Schriften, Bücher und Artikel, u. a. :
