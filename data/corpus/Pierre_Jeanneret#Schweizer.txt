Pierre Jeanneret (* 22. März 1896 in Genf; † 4. Dezember 1967 ebenda) war ein Schweizer Architekt.

Pierre Jeanneret war viele Jahre Partner seines berühmten Vetters (zweiten Grades) Le Corbusier (Charles Edouard Jeanneret). Beide gründeten 1923 in Paris ein gemeinsames Architekturbüro, Jeanneret war an vielen der in diesem Büro entworfenen, für die Entwicklung der modernen Architektur bedeutenden Projekte beteiligt. 

Das gemeinsame Büro wurde 1940 aufgelöst. In den 1950er Jahren arbeiteten sie beim Bau der indischen Stadt Chandigarh noch einmal zusammen an einem Projekt. Pierre Jeanneret begleitete das Projekt 15 Jahre lang bis kurz vor seinem Tod. 

THE house of Swiss-born architect Pierre Jeanneret converted into a museum,Chandigarh,India
