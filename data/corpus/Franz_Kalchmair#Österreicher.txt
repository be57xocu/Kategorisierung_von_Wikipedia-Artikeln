Franz Kalchmair (* 22. Dezember 1939 in Thalheim bei Wels) ist ein österreichischer Opern- und Liedersänger (Bass) und Musikpädagoge.

Kalchmair kam 1949 nach St. Florian, um die Hauptschule zu besuchen und wurde Mitglied der Florianer Sängerknaben am Stift Sankt Florian.[1]
Er studierte am Brucknerkonservatorium Linz und besuchte die Meisterklasse bei Kurt Equiluz. Erste Engagements führten ihn ans Landestheater Niederösterreich und an die Sommerbühne Baden. Mit dem Beginn des Landesmusikschulwesens in Oberösterreich wurde er als Gesangslehrer in den Landesmusikschulen in Kremsmünster und Wartberg an der Krems verpflichtet.

Er übernahm den elterlichen Bauernhof, den er Ende der 1980er-Jahre aufgab und die Stallungen zu einem Konzertsaal umfunktionierte. Die Konzert und Kulturveranstaltungen wurden unter dem Titel Kunst im G´wölb publiziert.

Er war langjähriges Mitglied des Ensembles des Linzer Landestheaters und mehrfach Gastsänger an der Volksoper Wien und an der Grazer Oper. Weitere Engagements erhielt er bei den Mörbischer Seespielen, am Salzburger Landestheater und fallweise auch an Bühnen im Ausland.

Bekannt wurde er als Bass-Buffo. Kalchmairs Repertoire umfasst viele der großen Bass-Opernrollen, wie etwa Falstaff oder Tannhäuser. Er tat sich als Interpret klerikaler Musik und von Bach-Kantaten sowie als Darsteller lustiger Partien (etwa als Schweinebaron im Zigeunerbaron oder als Tevje in Anatevka) hervor. Er wirkte an Uraufführungen von Werken namhafter Künstler wie Balduin Sulzer, Gunter Waldek und Ernst Ludwig Leitner mit.

Kalchmair spielte mehrere Balladen- und Arien-CDs ein, darunter die Balladen von Carl Loewe. 
