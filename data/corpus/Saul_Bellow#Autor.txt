Saul Bellow (* 10. Juni 1915 als Solomon Bellows in Lachine, Québec, Kanada; † 5. April 2005 in Brookline, Massachusetts, USA) war ein US-amerikanischer Schriftsteller und Träger des Nobelpreises für Literatur.[1] Seine mehrfach ausgezeichneten Romane, Erzählungen und Essays verschafften ihm die Anerkennung, neben Bernard Malamud und Philip Roth zu den bedeutendsten Vertretern der jüdisch-amerikanischen Literatur des 20. Jahrhunderts zu zählen.[2]
Saul Bellow wurde 1915 als Solomon Bellows in Lachine, einem Vorort von Montreal, als Sohn einer jüdischen Immigrantenfamilie aus Sankt Petersburg geboren, die 1913 ausgewandert war. Die ersten neun Lebensjahre verbrachte Bellow in einem vielsprachigen Armenviertel Montréals, in dem zahlreiche jüdische Einwanderer vornehmlich aus Ost- und Südeuropa lebten. Außer Englisch und Französisch lernte der junge Saul Bellow Hebräisch und Jiddisch, das er auch zu Hause sprach. Er wurde orthodox erzogen und sollte nach dem Wunsch seiner Mutter später talmudischer Schriftgelehrter werden. 1924 zog die Familie nach Chicago um, das Bellow fortan als seine Heimat ansah und zum Schauplatz vieler seiner Werke machte.[3] Er selbst und sein literarisches Schaffen wurden zeitlebens durch das östlich-jüdisch geprägte Großstadtmilieu, in dem er aufwuchs, beeinflusst.

1934 begann er an der University of Chicago sein Studium, das er ab 1935 an der Northwestern University fortsetzte. Dort erhielt er 1937 seinen Bachelor in den Fächern Sozialanthropologie und Soziologie und arbeitete anschließend an der University of Wisconsin.[4] Er heiratete die Soziologin Anita Goshkin und arbeitete als Journalist, später als Universitätsprofessor für Literatur. Kurzzeitig war er auch Mitarbeiter der Encyclopædia Britannica. Er gehörte dem internationalen Salzburg Seminar an.

Bereits sein früher existenzialistischer Tagebuchroman Dangling Man erregte 1944 Aufmerksamkeit. Hier kreist das Geschehen um einen jungen Mann, der seine Tätigkeit in einem Reisebüro aufgegeben hat, um sich für seine Einberufung bereitzuhalten. Der Wartezustand in dem Zeitraum zwischen Dezember 1942 und April 1943 macht ihn zu eben jenem dangling man, d. h. dem „Mann in der Schwebe“, der ohne Zielsetzung die Auflösung seiner Identität und die Entfernung von seinem früheren Ich erlebt. Dieser in dem Titel seines Erstlingsroman bezeichnete Schwebezustand sowie die erlebte Diskrepanz zwischen Wollen und Vermögen oder Anforderung und Realisierbarkeit, die der Protagonist als ein Dilemma der Menschheit schlechthin begreift, bringt zugleich eine Grundbefindlichkeit zum Ausdruck, die ebenso charakteristisch für viele der späteren Hauptgestalten in Bellows Erzählwerk werden sollte. Drei Jahre später folgte mit The Victim eine subtile psychologische Studie über die Beziehung zwischen einem Juden und einem Antisemiten, in der die Rollen von Opfer und Täter wechseln. Daneben thematisiert Bellow in seinem Frühwerk die Vereinsamung und Entfremdung des Individuums in der materialistischen amerikanischen Großstadtgesellschaft.[5]
Der literarische Durchbruch gelang Bellow nach seinen Anfangserfolgen 1953 mit dem pikaresken Roman The Adventures of Augie March, dessen lebenshungriger Held sich in seinem Versuch, unterschiedliche Lebensformen auszuprobieren, deutlich abhebt von den eher passiven, reflektierenden und zweifelnden oder verzweifelnden Figuren aus den früheren Werken. Noch in der gleichen Dekade veröffentlichte Bellow mit Seize the Day (1956) und Henderson the Rain King (1959) zwei weitere Romane, die ihn zunehmend auch international bekannt machten. Umfasst Augie March in einer losen Folge von Episoden einen Handlungszeitraum von mehr als drei Jahrzehnten, so ist das wiederum eher bedrückende Geschehen in dem straff strukturierten Kurzroman Seize the Day auf die Zeitspanne eines einzigen Tages konzentriert, an dem der 44-jährige Protagonist mit seinem bisherigen Leben abrechnet und zu der Überzeugung gelangt, in entscheidenden Bereichen versagt zu haben. In seinem Versuch, ein ihm fremdes Ich aufzubauen, hat er Lasten und Verpflichtungen auf sich genommen, die er nicht mehr tragen kann. Am Ende bleibt es offen, ob es ihm gelingen wird, das in dem Titel anklingende Carpe-diem-Motiv des Horaz in einem neuen Lebensentwurf zu verwirklichen. Auch der als Ich-Erzähler auftretende Held in Henderson the Rain King, der als mehrfacher Millionär keinerlei materielle Not leidet, ist auf der Suche nach einem befriedigenden Lebenssinn und neuer Selbstbestimmung. Er begibt sich auf eine Afrikareise und erlebt eine Reihe phantastischer, tragikomischer und grotesker Abenteuer, die seine Lebensentwürfe immer wieder in Frage stellen. Er resigniert jedoch nicht und schafft es, die sich ihm stellenden Bewährungsproben zu bestehen. Dabei erlebt er schließlich mit einer spontanen Hinwendung zum Nächsten eine Art von spiritueller Wiedergeburt.[6]
Ein großer kommerzieller Erfolg gelang Bellow mit dem 1964 erschienenen tragikomischen Roman Herzog, den viele Kritiker für sein künstlerisch gelungenstes und anspruchsvollstes Werk halten. Darin setzte er sich mit Philosophen wie Spinoza, Nietzsche und Heidegger (oder auch mit dem schon entfernteren Oswald Spengler) auseinander. Dabei nahm er einen sowohl europäischen wie amerikanischen Blickpunkt ein, suchte damit gewissermaßen zwei Identitäten zu verbinden. Mit diesem Roman löste Bellow weltweit ein starkes Echo aus und erschloss sich auch die Leserschaft in Deutschland. Die Titelfigur des Werks, Moses Herzog, ist ein angesehener jüdischer Intellektueller und Universitätsprofessor, der nach dem Scheitern seiner Ehe und einer tiefen Enttäuschung über seinen besten Freund vor einem psychischen Zusammenbruch steht. Um mit sich selbst wieder ins Reine zu kommen und seine Gegenwart und Zukunft neu auszurichten, ist er bemüht, die Bestandteile seines bisherigen Lebens, darunter seine Kindheitserinnerungen an das Leben im jüdischen Immigrantengetto von Montreal, zu sammeln und zu ordnen. Er versucht seine Gedanken festzuhalten, indem er zahllose Briefe verfasst, die an seine Freunde, Bekannten und Verwandten oder an berühmte Persönlichkeiten seiner Zeit, aber auch der Vergangenheit, gerichtet sind.  Die Bewältigung seiner persönlichen Lebenskrise führt Herzog so auf der abstrakten Ebene einer Rationalisierung zu der Auseinandersetzung mit Philosophen und Wissenschaftlern verschiedener Zeiten, deren Aussagen er kritisch überprüft. Diese geistige Bestandsaufnahme seiner Zeit, an der Bellow den Leser teilhaben lässt, endet mit Herzogs Entscheidung gegen einen modischen Nihilismus. Er gelangt zu der Überzeugung, dass der Wert des Menschen und die Sinnfindung für sein Leben sich genau darin zeigen, dass er sich im Leben bewährt und seine persönlichen und praktischen Existenzprobleme bewältigt.[7]
Das Hauptthema der Werke Bellows ist die Situation des männlichen, jüdischen Intellektuellen in den heutigen USA, der ohne religiöse Bindung seinen Weg im Kampf des Lebens sucht, insbesondere verwirrt im Kampf der Geschlechter. Die Schauplätze seiner Romane sind vorwiegend New York und Chicago. Die eigene Minderheitenerfahrung sensibilisierte ihn für soziale Antagonismen; zugleich war er stets ein wacher Diagnostiker kultureller Tendenzen und Veränderungen. In den älteren Jahren des Autors werden die Protagonisten in den Werken zu aktiven, ihr Los meisternden und auf ihre Umwelt einwirkenden Gestalten.

Bellow bildete gemeinsam mit William Faulkner „das Rückgrat der amerikanischen Literatur des 20. Jahrhunderts“, so die Einschätzung des Autors Philip Roth.[8]
1976 erhielt Bellow den Nobelpreis für Literatur „für das menschliche Verständnis und die subtile Kulturanalyse, die in seinem Werk vereinigt sind“.[9] Kurz zuvor wurde er für den Roman Humboldts Vermächtnis (engl. Humboldt’s Gift) mit dem Pulitzer-Preis ausgezeichnet.

Bellow war fünfmal verheiratet, unter anderem mit der Mathematikerin Alexandra Bellow. Zum Zeitpunkt seines Todes hinterließ er neben drei Söhnen eine fünfjährige Tochter.

Prudhomme (1901) |
Mommsen (1902) |
Bjørnson (1903) |
F. Mistral/Echegaray (1904) |
Sienkiewicz (1905) |
Carducci (1906) |
Kipling (1907) |
Eucken (1908) |
Lagerlöf (1909) |
Heyse (1910) |
Maeterlinck (1911) |
Hauptmann (1912) |
Tagore (1913) |
nicht verliehen (1914) |
Rolland (1915) |
Heidenstam (1916) |
Gjellerup/Pontoppidan (1917) |
nicht verliehen (1918) |
Spitteler (1919) |
Hamsun (1920) |
France (1921) |
Benavente (1922) |
Yeats (1923) |
Reymont (1924) |
Shaw (1925) |
Deledda (1926) |
Bergson (1927) |
Undset (1928) |
Mann (1929) |
Lewis (1930) |
Karlfeldt (1931) |
Galsworthy (1932) |
Bunin (1933) |
Pirandello (1934) |
nicht verliehen (1935) |
O’Neill (1936) |
Martin du Gard (1937) |
Buck (1938) |
Sillanpää (1939) |
nicht verliehen (1940–1943) |
Jensen (1944) |
G. Mistral (1945) |
Hesse (1946) |
Gide (1947) |
Eliot (1948) |
Faulkner (1949) |
Russell (1950) |
Lagerkvist (1951) |
Mauriac (1952) |
Churchill (1953) |
Hemingway (1954) |
Laxness (1955) |
Jiménez (1956) |
Camus (1957) |
Pasternak (1958) |
Quasimodo (1959) |
Perse (1960) |
Andrić (1961) |
Steinbeck (1962) |
Seferis (1963) |
Sartre (1964) |
Scholochow (1965) |
Agnon/Sachs (1966) |
Asturias (1967) |
Kawabata (1968) |
Beckett (1969) |
Solschenizyn (1970) |
Neruda (1971) |
Böll (1972) |
White (1973) |
Johnson/Martinson (1974) |
Montale (1975) |
Bellow (1976) |
Aleixandre (1977) |
Singer (1978) |
Elytis (1979) |
Miłosz (1980) |
Canetti (1981) |
García Márquez (1982) |
Golding (1983) |
Seifert (1984) |
Simon (1985) |
Soyinka (1986) |
Brodsky (1987) |
Mahfuz (1988) |
Cela (1989) |
Paz (1990) |
Gordimer (1991) |
Walcott (1992) |
Morrison (1993) |
Ōe (1994) |
Heaney (1995) |
Szymborska (1996) |
Fo (1997) |
Saramago (1998) |
Grass (1999) |
Gao (2000) |
Naipaul (2001) |
Kertész (2002) |
Coetzee (2003) |
Jelinek (2004) |
Pinter (2005) |
Pamuk (2006) |
Lessing (2007) |
Le Clézio (2008) |
Müller (2009) |
Vargas Llosa (2010) |
Tranströmer (2011) |
Mo (2012) |
Munro (2013) |
Modiano (2014) |
Alexijewitsch (2015) |
Dylan (2016) |
Ishiguro (2017)
