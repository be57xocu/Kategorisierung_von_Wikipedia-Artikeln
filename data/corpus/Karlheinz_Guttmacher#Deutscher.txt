Karlheinz Guttmacher (* 24. August 1942 in Danzig) ist ein deutscher Politiker (FDP).

Nach dem Abitur 1961 in Wernigerode absolvierte Guttmacher nach dem Wehrdienst ab 1963 ein Studium der Chemie an der Friedrich-Schiller-Universität Jena, welches er 1970 als Diplom-Chemiker und Diplomlehrer beendete. 1975 erfolgte seine Promotion und 1990 seine Habilitation in der Anorganischen Chemie.

Karlheinz Guttmacher war verheiratet und hat zwei Kinder.

1961 trat Guttmacher in die LDPD der DDR ein. Seit 1991 ist er Mitglied im Landesvorstand der FDP Thüringen, von 1995 bis 2003 auch des Präsidiums. Im Juni 2002 wurde er als Nachfolger von Andreas Kniepert zum Landesvorsitzenden gewählt. Er kündigte einen Generationenwechsel an und trat entsprechend im November 2003 zu Gunsten von Uwe Barth als Landesvorsitzender zurück. Von 2002 bis 2003 war er Mitglied im Bundesvorstand der FDP, dem er auch bereits von 1994 bis 1997 angehörte. Er hatte bis zum Jahreswechsel 2009/2010 ein Mandat als Stadtrat in Jena [1]. 

Über die Landesliste Thüringen zog Guttmacher 1990 in den Deutschen Bundestag ein und war bis 2005 Abgeordneter. In der 15. Wahlperiode war er ab Februar 2004 Vorsitzender des Petitionsausschusses. 2005 trat er nicht mehr für die Wahl zum 16. Deutschen Bundestag an.

Luise Albertz (SPD) |
Helene Wessel (SPD) |
Maria Jacobi (CDU) |
Lieselotte Berger (CDU) |
Gero Pfennig (CDU) |
Christa Nickels (Grüne) |
Heidemarie Lüth (PDS) |
Marita Sehn (FDP) |
Karlheinz Guttmacher (FDP) |
Kersten Steinke (Linke)

Andreas Kniepert (1990–1994) |
Peter Röhlinger (1994–1999) |
Heinrich Arens (1999) |
Andreas Kniepert (1999–2002) |
Karlheinz Guttmacher (2002–2003) |
Uwe Barth (2003–2014) |
Franka Hitzing (2014–2015) |
Thomas L. Kemmerich (seit 2015)
