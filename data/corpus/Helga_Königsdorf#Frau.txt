Helga Königsdorf, verehelicht Bunke (* 13. Juli 1938 in Gera; † 4. Mai 2014 in Berlin) war eine deutsche Mathematikerin und Schriftstellerin.[1]
Nach ihrer Promotion 1963 (mit der Arbeit Zur Stabilität stochastischer Differentialgleichungssysteme[2]) und der Habilitation 1972 war sie von 1974 bis zu ihrer vorzeitigen Emeritierung 1990 als Professorin für Mathematik an der Akademie der Wissenschaften der DDR in Ost-Berlin tätig, wo sie eine Abteilung für Wahrscheinlichkeitsrechnung und Statistik leitete.[3][4] Als Mathematikerin veröffentlichte sie unter dem Namen Helga Bunke.

Königsdorf veröffentlichte 1978 in der DDR ihre ersten Erzählungen. Sie setzte sich wie Christa Wolf, Brigitte Reimann und Maxie Wander in den 1970er und 1980er Jahren mit den Folgen der in der DDR praktizierten Gleichberechtigung der Frauen auseinander. 

1990 dokumentierte sie den Abschied von der DDR in 18 Gesprächsprotokollen (Adieu DDR) und reflektierte später in mehreren Essays die Geschichte der DDR.[5] Viele ihrer Erzählungen befassen sich mit dem Wissenschaftsbetrieb in der DDR.[6] Königsdorf war Mitglied im PEN-Zentrum Deutschland.

Königsdorf war mit dem Mathematiker Olaf Bunke verheiratet. Der Mathematiker Ulrich Bunke ist ihr gemeinsamer Sohn, die deutsch-argentinische Guerillera Tamara Bunke war ihre Schwägerin. Mehr als 30 Jahre litt Helga Königsdorf unter der Parkinsonschen Krankheit.[7]