Joseph Henry (* 17. Dezember 1797 in Albany, New York; † 13. Mai 1878 in Washington, D.C.) war ein führender US-amerikanischer Physiker.

Henry wurde in Albany (New York) geboren und lernte zuerst Uhrmacher und Silberschmied. Sein Großvater war aus Schottland eingewandert.

Henry wurde 1826 Professor für Mathematik und Naturphilosophie an der Albany Academy und begann 1827 die ersten Versuche mit der Elektrizität. Noch vor den Versuchen Samuel Morses lieferte Henry den Nachweis, dass durch den elektrischen Telegraphen zwischen zwei entfernten Orten Nachrichten ausgetauscht werden können (1831 im American Journal of Science).

1832 wurde Henry Professor der Naturwissenschaften am Princeton College und blieb dort bis 1837. 1840 wurde er in die American Academy of Arts and Sciences gewählt. 1846 wurde er der erste Sekretär der Smithsonian Institution in der Bundeshauptstadt Washington und kann als geistiger Vater dieser Einrichtung gelten.

1849 war er Präsident der American Association for the Advancement of Science.

Im Jahr 1850 wurde Henry zum Mitglied der Leopoldina gewählt, 1852 wurde er Mitglied der Leuchtturmkommission, deren Präsident er zwischen 1871 und 1878 war. Von 1868 bis 1878 war er Präsident der National Academy of Sciences, zu deren Gründungsmitgliedern er 1863 gehörte.

Henrys hauptsächliches Arbeitsgebiet war der Elektromagnetismus, bei dem er das Phänomen der Selbstinduktion entdeckte. Zur gleichen Zeit formulierte Michael Faraday in England das Prinzip der gegenseitigen Induktion, die technisch beim Transformator verwendet wird. Nachdem Hans Christian Ørsted die magnetischen Effekte eines stromdurchflossenen Leiters entdeckt hatte, war Henry der erste, der auf die Idee kam, durch vielfache Leiterschleifen oder eine Spule um einen Eisenkern einen Elektromagneten zu schaffen. Ein von ihm für Yale gebauter Elektromagnet konnte eine Last von 2300 Pounds (etwa 1040 kg) heben. Außerdem erfand er 1835 das elektromagnetische Relais, ohne das weder das einfache Schalten von elektrischen Stromkreisen noch die Telegraphie über größere Entfernungen möglich gewesen wären.

Sein Hauptinteresse galt nun dem Wettergeschehen. Er warb freiwillige Wetterbeobachter an und organisierte ein telegraphisches Netz zur Übermittlung der Wetterdaten. Henry zeichnete die erste Wetterkarte in der Geschichte der Meteorologie und schuf die wissenschaftlichen Grundlagen für das System der täglichen Wetterprognosen.

Im Auftrag der amerikanischen Regierung konstruierte er Leuchttürme und Nebelhörner mit größerer Reichweite und erfand verbesserte Navigationsinstrumente, um die Küstenschifffahrt sicherer zu machen. Zu seinen Ehren ist die SI-Einheit der elektrischen Induktivität mit Henry (Einheitenzeichen H) benannt worden. Vor der Smithsonian Institution wurde ihm zu Ehren ein Denkmal errichtet. Außerdem sind der Mondkrater Henry und die Henry Mountains im Südosten von Utah nach ihm benannt worden.

Joseph Henry (1846–1878) |
Spencer Fullerton Baird (1878–1887) |
Samuel Pierpont Langley (1887–1906) |
Charles Doolittle Walcott (1907–1927) |
Charles Greeley Abbot (1928–1944) |
Alexander Wetmore (1944–1952) |
Leonard Carmichael (1953–1964) |
S. Dillon Ripley (1964–1984) |
Robert McCormick Adams (1984–1994) |
I. Michael Heyman (1994–1999) |
Lawrence M. Small (2000–2007) |
G. Wayne Clough (2008–2015) |
Dr. David Skorton (2015–)
