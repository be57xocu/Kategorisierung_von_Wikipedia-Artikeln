Farley McGill Mowat OC (* 12. Mai 1921 in Belleville, Ontario, Kanada; † 6. Mai 2014 in Port Hope, Ontario, Kanada) war ein kanadischer Schriftsteller, dessen Bücher in mehr als 50 Sprachen übersetzt wurden. Viele seiner erfolgreichsten Werke handeln von Erinnerungen an seine Kindheit, seinen Militärdienst und seine Arbeit als Naturforscher. Sein Urgroßonkel war der Politiker Oliver Mowat.

Farley Mowat wurde 1921 in Belleville, Ontario als Sohn von Angus McGill Mowat und Helen Anne Thomson geboren. Sein Vater war Bibliothekar und nebenher weitgehend erfolglos als Schriftsteller tätig. Während seine Familie in den Jahren 1930–1933 in Windsor lebte, begann Farley mit dem Schreiben. Auf dem Höhepunkt der Weltwirtschaftskrise zog die Familie dann nach Saskatoon in Saskatchewan um.

Als Junge war Mowat fasziniert von der Natur und den Tieren. Im Alter von 13 Jahren schrieb er für eine Zeitung und kümmerte sich um Vögel, die im Winter nicht in den Süden flogen. Im Jahr 1935 reiste er mit 15 Jahren gemeinsam mit seinem Großonkel Frank, einem Vogelkundler, zum ersten Mal in die Arktis.[1]
Während des Zweiten Weltkriegs kämpfte Mowat als Offizier in einem kanadischen Bataillon gegen das nationalsozialistische Deutschland. Am 10. Juli 1943 war seine Einheit an der Landung der alliierten Truppen auf Sizilien beteiligt.[2]
Nach dem Krieg kehrte Mowat nach Kanada zurück und studierte Biologie an der Universität von Toronto. Während einer Exkursion in die Arktis erfuhr er von der Not der Inuit. Diese Erlebnisse verarbeitete er 1952 in seinem ersten Roman „People of the Deer“, durch den er zu einer literarischen Berühmtheit wurde. Das Werk trug dazu bei, das Verhältnis der kanadischen Regierung zu den Inuit zu verbessern. Im Jahr 1956 folgte das preisgekrönte Kinderbuch „Lost in the Barrens“.

Nach dem Ende seines Studiums war Mowat in Regierungsauftrag als Biologe in der Arktis tätig. Aufgrund von Befürchtungen, dass Wölfe die riesigen Karibuherden dezimieren könnten, erwog die kanadische Regierung damals, die Wölfe zu töten. Nach monatelangen Beobachtungen kam Mowat jedoch zu dem Schluss, dass die Wölfe sich vor allem von Mäusen ernährten und nur alte oder kranke Karibus töteten. Seine Erkenntnisse schrieb er 1963 in dem Buch „Never Cry Wolf“ nieder, das weltweite Verbreitung fand (deutscher Titel: Ein Sommer mit Wölfen, Rowohlt-Taschenbuch). 1983 verfilmte Walt Disney das Buch, der Film wurde in Deutschland unter dem Titel „Wenn die Wölfe heulen“ gezeigt.[3]
Mowat lebte acht Jahre lang in Burgeo, Neufundland. Dort schrieb er weitere Bücher und setzte sich gegen den Walfang ein. 1981 war er Co-Autor für einen Film mit Peter Strauss und Richard Widmark. Als ihm 1985 auf einer Werbetournee für sein neuestes Buch die Einreise in die USA verweigert wurde, erregte der Fall weltweit Aufmerksamkeit.

In seinem Haus hielt Mowat Vögel und einen Alligator.

Mowats Engagement richtete die Aufmerksamkeit auf die schwere Situation der Inuit und die grandiose Natur des Nordens. Seine Werke wurden bislang in 52 Sprachen übersetzt, bisher wurden mehr als 17 Millionen Bücher verkauft. Er war Mitglied im internationalen Leitungsgremium der Umweltschutzorganisation Sea Shepherd Conservation Society, deren Flaggschiff ihm zu Ehren R/V Farley Mowat getauft wurde.[4]
Mowat unterstützte die Grüne Partei von Kanada. 1981 wurde Mowat als Officer in den Order of Canada aufgenommen.[5]
Von Biologen wurde Mowat für seine eher unkonventionellen Forschungsmethoden kritisiert.

Es gibt Hinweise von John Goddard, wonach Mowats Erlebnisberichte eine Mischung aus Beobachtungen, Berichten und Wunschdenken seien. Auch seine Behauptungen, zwei Winter und einen Sommer mit Wolfsbeobachtungen zugebracht zu haben, werden als übertrieben bezeichnet; stattdessen hätte er nur 90 Stunden lang die Wölfe erforscht. Nach der Veröffentlichung dieser Kritik im kanadischen Magazin Saturday Night entbrannte eine heftige Diskussion um den Realitätsgehalt seiner Berichte.[6]
Romane und Erzählungen

Erlebnisberichte
