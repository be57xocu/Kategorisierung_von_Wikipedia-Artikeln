Eldred Gregory Peck (* 5. April 1916 in La Jolla, Kalifornien; † 12. Juni 2003 in Los Angeles, Kalifornien) war ein US-amerikanischer Schauspieler.

Die Verkörperung von oft in der Heldenrolle auftretenden, zumeist aufrechten, integren und sympathischen Figuren machte ihn in der zweiten Hälfte des 20. Jahrhunderts zu einem berühmten Filmstar. Das American Film Institute wählte ihn 1999 auf Platz 12 der 25 bedeutendsten männlichen Leinwandlegenden des US-amerikanischen Films. Auf der 2003 entstandenen Liste der 50 größten Figuren des US-amerikanischen Films nimmt seine Rolle als Atticus Finch – Verteidiger eines im Ergebnis unschuldig der Vergewaltigung angeklagten schwarzen Farmarbeiters – in Wer die Nachtigall stört den ersten Platz ein. Für die darstellerische Leistung in diesem Film gewann Peck im Jahr 1963 den Oscar als bester Hauptdarsteller.

Gregory Peck wurde 1916 im kalifornischen La Jolla als Sohn eines irischstämmigen Apothekers geboren. Den Vornamen „Eldred“, den er zeitlebens hasste, gab ihm seine Großmutter, um Verwechslungen mit Pecks Vater (dessen Vorname ebenfalls Gregory war) vorzubeugen. Pecks Eltern ließen sich scheiden, als er drei Jahre alt war, daher wuchs er bei seiner Großmutter mütterlicherseits auf. Im Alter von zehn Jahren schickte man ihn auf eine römisch-katholische Militärschule in Los Angeles, Kalifornien. 

Nach dem Abschluss studierte er ein Jahr lang an der San Diego State University und arbeitete anschließend für kurze Zeit als Lastwagenfahrer für ein Ölunternehmen. 1936 schrieb er sich zum Vorbereitungskurs für ein Medizinstudium an der University of California, Berkeley, ein. Er wechselte schließlich zum Englischstudium und wurde Mitglied der Universitätsrudermannschaft. Kaum von dem kleinen Theater der Universität angeworben, nahm er in seinem Abschlussjahr bereits an fünf Aufführungen teil.

Nach seinem Studium legte Peck 1939 den Namen „Eldred“ ab und ging nach New York City, New York, um im Neighborhood Playhouse zu lernen. Oft hatte er kein Geld und übernachtete dann sogar im Central Park. 1939 jobbte er auf der Weltausstellung und als Führer für die NBC. Peck gab 1942 sein Broadwaydebüt in Emlyn Williams Morning Star, wo er sofort als Hauptdarsteller auftrat. Bereits 1943 spielte er, unter der Regie von Max Reinhardt, in Irwin Shaws Stück Sons and Soldiers seine letzte Theaterrolle. Er war von Talentsuchern aus Hollywood entdeckt worden und hatte lukrative Filmangebote erhalten.

Gregory Peck war seit 1942 in erster Ehe mit Greta Kukkonen (1911–2008) verheiratet und hatte mit ihr drei Kinder. Diese Ehe wurde 1955 geschieden.

Da viele bekannte Filmschauspieler, zum Beispiel Clark Gable, David Niven oder James Stewart, im Zweiten Weltkrieg in der Armee dienten, suchte Hollywood während dieser Zeit dringend nach neuen Darstellern. So erhielten Schauspieler wie Robert Mitchum, William Holden oder auch Gregory Peck die Chance, in diesen Jahren eine erfolgreiche Filmkarriere zu begründen. Peck war wegen einer Rückenverletzung, die er sich auf dem College zugezogen hatte, vom Militärdienst freigestellt worden und stand daher sofort für wichtige Rollen zur Verfügung. Schlank, dunkelhaarig und mit einer stattlichen Größe von 1,90 m, galt er als einer der attraktivsten Filmschauspieler und verkörperte für viele Zuschauer das Idealbild männlicher Schönheit. Mit nur wenigen Filmen etablierte er sich Mitte der 1940er Jahre als neuer Star.

Pecks erster Film war Days of Glory, der 1944 in die Kinos kam. Der Schauspieler konnte sich schnell als vielseitiger Charakterdarsteller profilieren und wurde kontinuierlich von Hollywoods führenden Regisseuren eingesetzt. Zwischen 1946 und 1963 war Gregory Peck fünfmal für den Oscar als bester Hauptdarsteller nominiert. 

Alfred Hitchcock setzte Gregory Peck in den Thrillern Ich kämpfe um dich (1945) und Der Fall Paradin (1947) ein, wo er einen traumatisierten Psychiater und einen britischen Anwalt darstellte. Auch später war Peck in erfolgreichen Filmen dieses Genres zu sehen, darunter in Ein Köder für die Bestie (1962), wo er als unbescholtener Familienvater von einem psychopathischen Kriminellen (Robert Mitchum) herausgefordert wird, und in Die 27. Etage (1965), wo er einen Mann mit Gedächtnisverlust spielte. 

Bereits 1946 übernahm Peck seine erste Westernrolle und spielte unter der Regie von King Vidor in dem aufwändigen Farbfilm Duell in der Sonne, wo er in der Rolle des schurkischen Lewt McCanles zu sehen war. Später stellte Peck kaum noch negativ besetzte Charaktere dar. Peck zählte zu den beliebtesten Westerndarstellern und war in Filmen wie Der Scharfschütze (1950), Weites Land (1958), Das war der Wilde Westen (1962), Mackenna’s Gold (1969) oder Begrabt die Wölfe in der Schlucht (1973) zu sehen. Er war auch für die Hauptrolle im Klassiker Zwölf Uhr mittags im Gespräch, welche dann von Gary Cooper gespielt wurde.

Peck war ein gefragter Hauptdarsteller in Abenteuerfilmen wie Die Wildnis ruft (1946), Des Königs Admiral (1951) oder Sturmfahrt nach Alaska (1952). Seine wohl bekannteste Rolle in diesem Genre ist die des fanatischen Waljägers Käpt'n Ahab in John Hustons Moby Dick (1956). Der Schauspieler war auch in Komödien wie Sein größter Bluff (1953) oder Warum hab’ ich ja gesagt? (1957) zu sehen. Besondere populär wurde der bittersüße Liebesfilm Ein Herz und eine Krone (1953), in dem sich Peck in der Rolle eines abgebrühten Journalisten in eine junge Prinzessin verliebt. Der Film, entstanden unter der Regie von William Wyler, entwickelte sich zu einem Klassiker der Filmgeschichte und machte die bis dato unbekannte Audrey Hepburn in der Rolle der Prinzessin über Nacht zum Star.

Gregory Peck trat regelmäßig in Kriegsfilmen in Erscheinung, zum Beispiel in Der Kommandeur (1949) oder Flammen über Fernost (1954). In diesem Genre konnte er 1960 mit dem epischen Kriegsabenteuer Die Kanonen von Navarone (Regie: J. Lee Thompson) einen großen Erfolg verbuchen. Der aufwändige und hoch besetzte Film - neben Peck agierten unter anderem Anthony Quinn und David Niven - schilderte den Ablauf eines alliierten Kommandounternehmens im Zweiten Weltkrieg und wurde zu einem der größten Kinohits der frühen 1960er Jahre.

Peck trat immer wieder in Filmen auf, die sich mit sozialen oder politischen Problematiken auseinandersetzten. 1947 war er in Tabu der Gerechten zu sehen, einem von Elia Kazan inszenierten Filmdrama, in dem der latente Antisemitismus der US-Gesellschaft thematisiert wurde. In Das letzte Ufer war er 1959 unter der Regie von Stanley Kramer als Kapitän eines U-Bootes zu sehen, welches nach einem globalen Atomkrieg das noch unverseuchte Australien ansteuert. 

1962 erreichte Gregory Peck mit dem schwarzweißen Filmdrama Wer die Nachtigall stört (Regie: Robert Mulligan) einen Karrierehöhepunkt. Er spielte den Anwalt Atticus Finch, der im rassistischen Alabama der 1930er Jahre einen jungen Schwarzen verteidigt. Nach dem gleichnamigen Bestseller von Harper Lee entstanden, wurde der Film zu einem durchschlagenden Erfolg bei Kritik und Publikum und gilt als einer der bedeutendsten amerikanischen Filmklassiker der 1960er Jahre. Gregory Peck erhielt für seine Darstellung des engagierten Anwalts den Oscar als bester Hauptdarsteller. Dies war die fünfte und letzte Oscar-Nominierung für den Schauspieler.

Als Privatmann war Peck für seine liberalen Ansichten bekannt, in den 1960er Jahren engagierte er sich zunehmend auch politisch. Er setzte sich an der Seite Martin Luther Kings für die Rechte der Schwarzen in den Vereinigten Staaten ein und kritisierte öffentlich den Vietnamkrieg. 1970 lehnte er jedoch ein Angebot der Demokraten ab, sich als Gegenkandidat zu Ronald Reagan für die Wahl zum Gouverneur von Kalifornien aufstellen zu lassen. 1980 engagierte er sich unentgeltlich mit öffentlichen Auftritten für die wirtschaftlich schwer angeschlagene Chrysler Corporation, um den Verlust tausender Arbeitsplätze zu verhindern.

Weitere Erfolge konnte Gregory Peck mit der Thrillerkomödie Arabeske (1966) und dem SF-Drama Verschollen im Weltraum (1969) verbuchen, in dem er als Chef der Weltraumbehörde NASA zu sehen war. 1976 sorgte er für Aufsehen, als er unter der Regie von Richard Donner in dem Horrorfilm Das Omen zu sehen war, der zu einem großen Kinoerfolg wurde. Bis dato war kein hochrangiger Hollywood-Star in einem Film dieses Genres aufgetreten. 

1977 spielte Peck die Titelrolle in der Filmbiographie MacArthur – Held des Pazifik, 1978 trat er in The Boys from Brazil in der ungewohnten Rolle des Nazi-Verbrechers Dr. Josef Mengele in Erscheinung. 1980 war Peck neben Roger Moore und David Niven in dem Kriegsfilm Die Seewölfe kommen zu sehen.

Gregory Peck war seit 1955 in zweiter Ehe mit Veronique Passani (1932–2012) verheiratet. Er hatte die französische Journalistin bei einem Interview kennengelernt und sie einen Tag nach der Scheidung von seiner ersten Frau geheiratet. Aus dieser Ehe, die bis zu Pecks Tod bestehen blieb, gingen zwei Kinder hervor. Jonathan Peck, sein Sohn aus erster Ehe, beging 1975 Selbstmord. Peck bezeichnete dies als die größte Tragödie in seinem Leben.

In den frühen 1980er Jahren zog sich Peck weitgehend aus dem aktiven Filmgeschäft zurück und war bis zu seinem Tod nur noch in vier Kinofilmen und fünf TV-Produktionen zu sehen. In Das Geld anderer Leute agierte er 1991 als integrer Unternehmer "vom alten Schlag", der sich gegen einen skrupellosen Firmenaufkäufer (Danny DeVito) zur Wehr setzt. In Kap der Angst, Martin Scorseses Remake von Ein Köder für die Bestie (1962), war er 1991 in einer Gastrolle als Strafverteidiger zu sehen, der den psychopathischen Schurken Max Cady (Robert De Niro) verteidigt.

Seinen letzten Auftritt absolvierte der 82-jährige Gregory Peck 1998 in dem aufwändigen TV-Film Moby Dick, in dem Patrick Stewart in Pecks einstiger Paraderolle des Kapitän Ahab zu sehen war. Peck selbst verkörperte in einem Cameo zu Anfang des Films den Prediger Mapple, den 1956 Orson Welles gespielt hatte.

1989 bekam Gregory Peck vom American Film Institute für sein Lebenswerk den Lifetime Achievement Award verliehen. 

Gregory Peck verbrachte seine letzten Jahre zurückgezogen und starb am 12. Juni 2003 im Alter von 87 Jahren in seinem Haus in Los Angeles an den Folgen einer Lungenentzündung. Seine letzte Ruhestätte fand Peck in der Cathedral of Our Lady of the Angels in Los Angeles.[1]
Die Synchronisation von Peck übernahmen diverse bekannte deutsche Synchronsprecher. Beispielsweise Paul Klinger, Wolfgang Lukschy oder Martin Hirthe taten dies mehrere Male. Des Weiteren wurde er auch von Heinz Engelmann, Curt Ackermann oder Wolf Ackva gesprochen.

Douglas Fairbanks |
William C. de Mille |
Mike C. Levee |
Conrad Nagel |
Theodore Reed |
Frank Lloyd |
Frank Capra |
Walter Wanger |
Bette Davis |
Walter Wanger |
Jean Hersholt |
Charles Brackett |
George Seaton |
George Stevens |
B. B. Kahane |
Valentine Davies |
Wendell Corey |
Arthur Freed |
Gregory Peck |
Daniel Taradash |
Walter Mirisch |
Howard W. Koch |
Fay Kanin |
Gene Allen |
Robert Wise |
Richard Kahn |
Karl Malden |
Robert Rehme |
Arthur Hiller |
Robert Rehme |
Frank Pierson |
Sid Ganis |
Tom Sherak |
Hawk Koch |
Cheryl Boone Isaacs
