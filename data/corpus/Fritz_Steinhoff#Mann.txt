Fritz Steinhoff (* 23. November 1897 in Wickede (Landkreis Dortmund); † 22. Oktober 1969 in Hagen) war ein deutscher SPD-Politiker und der dritte Ministerpräsident des Landes Nordrhein-Westfalen.

Fritz Steinhoff wurde 1897 in einer Bergarbeiterfamilie geboren und wuchs in Unna-Massen auf. Noch während der Volksschule musste er nebenher auf einem Bauernhof Geld verdienen. Mit siebzehn Jahren wurde er dann Bergmann. 1917 wurde er zur Marine eingezogen und diente bis 1919 auf einem Torpedoboot.[1] Danach arbeitete er wieder als Bergmann und trat der SPD bei. Dort gehörte er zum national gesinnten Hofgeismarer Kreis von Jungsozialisten.1922 schickte der 'Verband der Bergarbeiter Deutschlands’ Steinhoff für zwei Semester an die Akademie der Arbeit in Frankfurt. Dort hörte er bei Franz Oppenheimer und Erik Nölting Vorlesungen über Wirtschaft und Politik; danach kehrte er nach Maassen zurück. Bald darauf wurde er arbeitslos. Er ging nach Berlin-Schöneberg, hörte Vorlesungen an der Hochschule für Politik, unter anderem bei Theodor Heuss, und lebte von Gelegenheitsarbeiten.[1]
1926 wurde Steinhoff Volontär bei dem SPD-Parteiblatt Westfälische Allgemeine Volkszeitung (WAVZ) in Dortmund. 1927 wurde er Geschäftsführer eines Zeitungsvertriebs, 1927 Parteisekretär in Hagen. Bei den Kommunalwahlen 1929 erreichte die SPD die Mehrheit in Hagen und Steinhoff wurde ehrenamtlicher Magistrat für Sportjugendpflege und Stadtgärtnerei.

Nach der Machtergreifung der NSDAP, die Steinhoff energisch bekämpft hatte, wurde er mehrfach verhaftet. Er arbeitete jetzt als Vertreter und eröffnete 1937 ein Herd- und Ofenreinigungsgeschäft.

Am 12. Oktober 1938 wurde er zu drei Jahren Zuchthaus verurteilt, weil er 1934 Hefte der sozialdemokratischen Zeitung Vorwärts nach Deutschland geschmuggelt habe. Nach seiner Entlassung am 16. Januar 1941 arbeitete er wieder als Hilfsarbeiter. Nach dem Attentat vom 20. Juli 1944 wurde er erneut verhaftet und ins Konzentrationslager Sachsenhausen gebracht, wo auch Fritz Henßler gefangen war.
Am 21. April 1945 räumten SS-Einheiten das KZ Sachsenhausen und trieben 33.000 Häftlinge [2] auf einen Todesmarsch. Steinhoff wurde von amerikanischen Truppen in Mecklenburg befreit.

Nach dem Krieg wurde er als Stadtverordneter in Iserlohn eingesetzt. 1946 wurde er Oberbürgermeister von Hagen. Obwohl die CDU nach den Kommunalwahlen die stärkste Fraktion im Stadtrat stellte, behielt Steinhoff sein Amt bis 1956. Gleichzeitig war er Mitglied des Provinzialrates von Westfalen und des ersten Landtages von Nordrhein-Westfalen. Von 1949 bis 1950 war er zudem Wiederaufbauminister im Kabinett von Karl Arnold. 1950 wurde er stellvertretender Fraktionsvorsitzender. Nach dem überraschenden Tod Henßlers am 4. Dezember 1953 folgte er diesem als Fraktionsvorsitzender und führte die SPD NRW als Spitzenkandidat in den Landtagswahlkampf 1954. Die CDU erhielt 41,3 % (plus 4,4 Prozentpunkte), konnte aber nicht wie erhofft ihre Koalition mit der Zentrumspartei fortsetzen, weil die beiden nur 99 der 200 Abgeordnetensitze hatten. Ministerpräsident Arnold hätte mit der FDP eine Zweier-Koalition bilden können; er entschied sich aber für eine Dreierkoalition aus CDU, FDP und Zentrum.

Nachdem es auf Bundesebene zu einem Konflikt zwischen CDU und FDP gekommen war (→ Näheres hier), wandte sich auch in Nordrhein-Westfalen die FDP von der CDU ab. Steinhoff konnte mit Hilfe der „Jungtürken“ in der FDP („Jungtürken“ im osmanischen Militär hatten 1908 Sultan Abdülhamid II. zu Reformen gezwungen) am 20. Februar 1956 ein erfolgreiches Konstruktives Misstrauensvotum gegen Ministerpräsident Arnold stellen und sein Nachfolger werden.

Die sozialliberale Koalition war allerdings auf Unterstützung des Zentrums angewiesen. Deshalb konnten einige als wichtig erachtete Reformen zum Beispiel im Schulwesen nicht durchgeführt werden. Die Reform des kommunalen Finanzausgleiches war jedoch erfolgreich. Auch wurde die Forschungsförderung ausgeweitet, insbesondere auf dem Gebiet der Kernenergie. So wurde auch der Grundstein zur Kernforschungsanlage Jülich gelegt.

Bei der Landtagswahl 1958 erhielt die SPD 39,2 Prozent der Stimmen; die CDU erhielt 50,5 Prozent und Franz Meyers wurde Ministerpräsident.

Steinhoff wurde noch 1958 Vorsitzender des Siedlungsverbandes Ruhrkohlenbezirk. Im September 1961 errang er das Direktmandat im Wahlkreis Hagen und zog in den Bundestag ein. Von 1963 bis 1964 war er wieder Oberbürgermeister von Hagen. Bei der Bundestagswahl 1965 erhielt er wieder das Direktmandat und blieb bis zu seinem Tod MdB.

Steinhoffs Grabstelle befindet sich auf dem Hauptfriedhof in Iserlohn.

Die Stadt Hagen verlieh Steinhoff 1967 die Ehrenbürgerwürde. 1975 wurde die erste Hagener Gesamtschule nach Fritz Steinhoff benannt und 1989 ein Denkmal zu seinen Ehren eingeweiht.

Kabinett Arnold I – Kabinett Steinhoff

Rudolf Amelunxen (1946–1947) |
Karl Arnold (1947–1956) |
Fritz Steinhoff (1956–1958) |
Franz Meyers (1958–1966) |
Heinz Kühn (1966–1978) |
Johannes Rau (1978–1998) |
Wolfgang Clement (1998–2002) |
Peer Steinbrück (2002–2005) |
Jürgen Rüttgers (2005–2010) |
Hannelore Kraft (2010–2017) |
Armin Laschet (seit 2017)

Hugo Paul (1946–1948) |
Ernst Gnoß (1948–1949) |
Fritz Steinhoff (1949–1950) |
Otto Schmidt (1950–1953) |
Willi Weyer (1954–1956) |
Fritz Kaßmann (1956–1958) |
Peter Erkens (1958–1962) |
Joseph Blank (1962–1963) |
Joseph Paul Franken (1963–1966) |
Franz Berding (1966) |
Hermann Kohlhase (1966–1970) |
Christoph Zöpel (1980–1990) |
Ilse Brusis (1990–1995) |
Michael Vesper (1995–2005) |
Oliver Wittke (2005–2009) |
Lutz Lienenkämper (2009–2010) |
Harry Voigtsberger (2010–2012) |
Michael Groschek (2012–2017) |
Ina Scharrenbach (2017–)

Fritz Henßler (1946–1953) |
Fritz Steinhoff (1953–1956) |
Emil Groß (1956–1958) |
Fritz Steinhoff (1958–1961) |
Fritz Kaßmann (1961–1962) |
Heinz Kühn (1962–1967) |
Johannes Rau (1967–1970) |
Fritz Kaßmann (1970–1975) |
Dieter Haak (1975–1980) |
Karl Josef Denzer (1980–1985) |
Friedhelm Farthmann (1985–1995) |
Klaus Matthiesen (1995–1998) |
Manfred Dammeyer (1998–2000) |
Edgar Moron (2000–2005) |
Hannelore Kraft (2005–2010) |
Norbert Römer (seit 2010)

Peter Matthias Wülfingh (kommissarisch) |
Heinrich Wilhelm Emminghaus |
Heinrich Caspar Hiltrop |
Johann Caspar Hücking |
Heinrich Arnold Wülfingh |
Peter Matthias Jule |
Carl Ludwig Christian Dahlenkamp |
Wilhelm Möllenhoff |
F. Pütter |
J. H. Wille |
Friedrich Wilhelm Kämper |
Johann Peter Aubel |
Friedrich Wilhelm Kämper |
Ferdinand Elbers |
Johann Diedrich Friedrich Schmidt |
Friedrich Dödter |
August Prentzel |
Willi Cuno |
Alfred Finke |
Cuno Raabe |
Heinrich Vetter |
Werner Dönneweg (kommissarisch) |
Ewald Sasse |
Fritz Steinhoff |
Helmut Turck |
Fritz Steinhoff |
Lothar Wrede |
Rudolf Loskand |
Renate Löchter |
Dietmar Thieser |
Wilfried Horn |
Peter Demnitz |
Jörg Dehm |
Erik O. Schulz
