Gustav Zumsteg (* 11. Oktober 1915 in Zürich; † 17. Juni 2005 ebenda) war ein Schweizer Kunstsammler, Seidenhändler und Inhaber des Zürcher Restaurants «Kronenhalle».

Gustav Zumsteg war der Sohn von Hulda Zumsteg, Eigentümerin der bekannten «Kronenhalle». Er machte die Lehre im Seidenhaus Abraham, wurde dessen Chefdesigner und erwarb es später. Zumsteg arbeitete mit den bekannten Pariser Modehäusern zusammen, etwa mit Cristóbal Balenciaga, Christian Dior, Hubert de Givenchy, Coco Chanel, vor allem aber mit Yves Saint Laurent. 
