Dorian Yates (* 19. April 1962 in Sutton Coldfield, Birmingham, England) ist Großbritanniens erfolgreichster Bodybuilder. Er gewann den Mr.-Olympia-Wettbewerb von 1992 bis 1997 und trat danach vom Wettkampfsport zurück. Er war einer der wenigen Bodybuilder, die ausschließlich nach dem Prinzip des High Intensity Trainings, das von Arthur Jones entwickelt wurde, trainierten. Er wurde im Jahr 2003 in die Hall of Fame der International Federation of Bodybuilding & Fitness aufgenommen. Er ist verheiratet, hat einen Sohn und eine Tochter.

Liste der Träger des Titels Mr. Olympia

Larry Scott 1965; 1966 |
Sergio Oliva 1967; 1968; 1969 |
Arnold Schwarzenegger 1970; 1971; 1972; 1973; 1974; 1975; 1980 |
Franco Columbu 1976; 1981 |
Frank Zane 1977; 1978; 1979 |
Chris Dickerson 1982 |
Samir Bannout 1983 |
Lee Haney 1984; 1985; 1986; 1987; 1988; 1989; 1990; 1991 |
Dorian Yates 1992; 1993; 1994; 1995; 1996; 1997 |
Ronnie Coleman 1998; 1999; 2000; 2001; 2002; 2003; 2004; 2005 |
Jay Cutler 2006; 2007; 2009; 2010 |
Dexter Jackson 2008 |
Phil Heath 2011; 2012; 2013; 2014; 2015; 2016
