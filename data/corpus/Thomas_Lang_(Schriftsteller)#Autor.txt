Thomas Lang (* 19. März 1967 in Nümbrecht, NRW) ist ein deutscher Schriftsteller.

Lang studierte Literaturwissenschaft in Frankfurt am Main und schloss als M. A. ab. Er lebt als freier Autor seit 1997 in München. Neben seiner schriftstellerischen Arbeit verfasst er Sachartikel in Computerzeitschriften und arbeitete als Dozent für Manuskriptum, den Kurs für Kreatives Schreiben der LMU. Er ist verheiratet und hat drei Töchter.
