Moyse Amyraut, auch Moses Amyraldus (* September 1596 in Bourgueil bei Tours; † 8. Januar 1664 in Saumur, heute im Département Maine-et-Loire) war ein reformierter Theologe aus Frankreich.

Amyraut wirkte in Saumur zunächst als Pfarrer und ab 1633 als Professor der Theologie an einer Akademie.[1] In seinem Traité de la prédestination versuchte er 1634 die strenge Prädestinationslehre der Dordrechter Synode abzumildern durch einen Universalismus hypotheticus, also durch die Lehre von einem gnädigen Willen Gottes, alle Menschen unter der Bedingung des Glaubens selig zu machen.

Auf verschiedenen französischen Nationalsynoden angeklagt, wurde er immer wieder freigesprochen. Eine zeitweilige Verurteilung seiner Lehre, Amyraldismus genannt, erreichten schließlich 1674 der Zürcher Professor Johann Heinrich Heidegger und sein Genfer Kollege François Turrettini (1623–1687) mit dem Consensus Helveticus.
