Michael Frayn, FRSL, (* 8. September 1933 in London, England) ist ein britischer Schriftsteller. 

Während des Wehrdienstes bei der Armee wurde Frayn zum militärischen Dolmetscher in der russischen Sprache ausgebildet. 1954 begann er sein Französisch- und Russisch-Studium am  Emmanuel College an der Universität Cambridge, wechselte aber nach einem Jahr zur Philosophie (Moral Sciences). Nach dem Abschluss 1957 war er Reporter und Kolumnist für den Manchester Guardian (1957-62) und den Observer (1962-68). Seither ist er als freier Schriftsteller tätig. Er ist ein erfolgreicher Dramatiker und Romanautor.

Frayn schrieb unter anderem die Farce Noises off (1982), die 1992 mit Michael Caine und Christopher Reeve unter der Regie von Peter Bogdanovich verfilmt wurde. 1998 erschien Kopenhagen, ein Stück über ein Gespräch zwischen den beiden Atomphysikern Niels Bohr und Werner Heisenberg. Kopenhagen wurde ein großer internationaler Erfolg, erhielt unter anderem den Tony Award und den Prix Molière und löste über die Theaterkreise hinaus eine historische Debatte über Heisenbergs Rolle im Nuklearprogramm des Dritten Reichs, dem so genannten Uranprojekt, aus.[1]
2003 folgte mit Demokratie ein Stück über Willy Brandt und die Guillaume-Affäre, für das Frayn nach Kopenhagen erneut mit dem Evening Standard-Preis und dem Critics Circle Award ausgezeichnet wurde.[2] Die deutsche Uraufführung fand am 6. Mai 2004 im Berliner Renaissance-Theater statt. 

Sein jüngstes Werk Afterline hatte 2008 im Londoner Nationaltheater Premiere. Das Stück über den Theaterregisseur und Schauspieler Max Reinhardt erfuhr am 18. März 2010 unter dem Titel Reinhardt im Alten Schauspielhaus Stuttgart seine deutsche Erstaufführung.

Bekannt ist Michael Frayn darüber hinaus für seine Tschechow-Übersetzungen ins Englische. Michael Frayn ist in zweiter Ehe mit der englischen Literaturkritikerin Claire Tomalin verheiratet.

(DSE = deutschsprachige Erstaufführung)
