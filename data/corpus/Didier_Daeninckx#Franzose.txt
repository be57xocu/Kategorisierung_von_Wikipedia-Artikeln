Didier Daeninckx (* 27. April 1949 in Saint Denis) ist ein französischer Krimiautor, der als einer der bedeutenden zeitgenössischen Kriminalschriftsteller Frankreichs gilt.

Daenickx gilt als Mitbegründer des neuen Roman noir. Wie bei vielen Roman-noir-Schriftstellern spielt auch bei ihm die Gesellschaftskritik eine entscheidende Rolle. Mit Themen wie der Résistance, der Kollaboration von Teilen der französischen Bevölkerung mit den Nazis, dem Algerienkrieg, den Problemen von Flüchtlingen in der Gesellschaft und der Abschiebepolitik des französischen Staates lösen seine Kriminalromane immer wieder gesellschaftliche Debatten aus. 1985 erhielt Daeninckx für seinen Roman Meurtes pour mémoire den renommierten Grand prix de littérature policière.

Der Autor schreibt auch Kinder- und Jugendbücher sowie Texte für Comics. Vor seiner Laufbahn als Schriftsteller war er Drucker, Sozialarbeiter  und Lokalredakteur. Er lebt heute in Aubervilliers.
