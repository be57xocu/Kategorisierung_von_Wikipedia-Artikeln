Humphrey William Bouverie Carpenter (* 29. April 1946 in Oxford; † 4. Januar 2005 ebenda) war ein britischer Biograph und Kinderbuchautor.

Nach seinem Studium in Oxford hielt Carpenter am dortigen Keble College Vorlesungen über die englische Sprache und Literatur. Dort traf er mehrere Male mit J. R. R. Tolkien zusammen. 

Carpenter arbeitete eine Zeit lang für das Radioprogramm der BBC, bis er begann, an der autorisierten Biographie von Tolkien zu arbeiten. Diese erschien 1977 unter dem Titel J.R.R. Tolkien – A Biography. Außerdem gab er eine Auswahl von Briefen Tolkiens heraus. 

Später verfasste er weitere Biographien, unter anderem von W. H. Auden (1981), Ezra Pound (1988), Benjamin Britten (1992), Robert Runcie (1997), Spike Milligan (2004) und den Inklings. Für die letzte, eine Gruppenbiographie über die Mitglieder der Inklings (J. R. R. Tolkien, C. S. Lewis und deren Freunde), bekam Carpenter den Somerset Maugham Award.

Carpenter veröffentlichte auch mehrere Kinderbücher, unter anderem die Mr. Majeica-Reihe und 1984 den Oxford Companion to Children’s Literature, sowie Theaterstücke und Radiohörspiele. Darüber hinaus war Carpenter ein begabter Jazz-Musiker, der mit einer eigenen Band, den Vile Bodies, auftrat.

Carpenter, der an der Parkinson-Krankheit litt, starb am 4. Januar 2005 zu Hause in Oxford an Herzversagen.
