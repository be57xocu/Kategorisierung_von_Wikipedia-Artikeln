Alec Newman (* 27. November 1974 in Glasgow als Mark Newman) ist ein schottischer Theater- und Filmschauspieler.

Newman wurde als Sohn von Sandy Newman, einem Mitglied der schottischen 1970er-Jahre-Musikgruppe The Marmalade, in Glasgow, Schottland geboren. Seine Familie zog drei Jahre später nach London um, wo er ein begeisterter Fußballspieler wurde. Eine angestrebte Profikarriere musste er nach einem folgenreichen Beinbruch aufgeben.

Mit 17 trat Newman erstmals am National Youth Theatre auf, wo aus seinen zahllosen Rollen, vor allem seine Darstellung des Iago in Shakespeares Othello, hervorstach. Es folgten weitere Theaterauftritte und Gastrollen in zahlreichen britischen Fernsehserien, die zu seinem ersten größeren Filmauftritt in Greenwich Mean Time (1999) führten, für die er durchweg sehr positive Kritiken erhielt.

Zunächst kehrte Newman zum Theater zurück und spielte in Plenty mit Cate Blanchett und in einer Neuaufführung von Max Frischs Andorra. Es folgten erste Hauptrollen in internationalen Filmproduktionen wie Dune – Der Wüstenplanet (2000) und Frankenstein (2004) mit Donald Sutherland and Julie Delpy. Die Rolle des Paul Atreides spielte er auch in der Fortsetzung Children of Dune (2003), in der er den Vater von James McAvoys Charakter spielt; im britischen Komödiendrama Bright Young Things aus demselben Jahr spielen sie Gleichaltrige. Auch in amerikanischen TV-Serien wie Angel – Jäger der Finsternis und Star Trek: Enterprise hat er wiederkehrende Gastrollen.

Alec Newman lebt heute im Norden von London und entwickelt sich zu einem immer gefragteren Bühnenschauspieler, ohne dass er dabei auf eine Filmkarriere verzichtet.
