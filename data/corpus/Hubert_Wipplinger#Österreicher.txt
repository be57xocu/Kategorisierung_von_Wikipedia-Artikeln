Hubert Wipplinger (* 6. August 1941 in Taufkirchen an der Pram, Bezirk Schärding; † 20. Oktober 2004) war ein Präsident der Arbeiterkammer Oberösterreich und Sozialdemokrat.

Nach einer Lehre als Betriebsschlosser war Wipplinger bis 1967 in der Privatwirtschaft tätig. Schon seit seiner Lehrzeit in der Gewerkschaft tätig wurde er 1964 Landesjugendobmann der Gewerkschaft Bau-Holz und ab 1967 Sekretär dieser Gewerkschaft sowie Landesobmann der Gewerkschaftsjugend und Gemeinderat in Diersbach bei Schärding. Ab 1975 war Wipplinger Landessekretär seiner Gewerkschaft, von 1989 bis 2003 ÖGB-Landesvorsitzender und von 1979 bis 1991 Gemeinderat in Linz.

Wipplinger war als Nachfolger Fritz Freyschlags vom 1. August 1999 bis zum 20. Oktober 2003 Präsident der Arbeiterkammer Oberösterreich. Sein Nachfolger wurde Johann Kalliauer.
Zentrale Anliegen waren ihm der Kampf gegen die Arbeitslosigkeit sowie die Bildungsarbeit für die Kammermitglieder.[1]
Er war außerdem stellvertretender Bundesvorsitzender der Gewerkschaft Bau-Holz, Vizepräsident der Bundesarbeitskammer und Funktionär in der Gebietskrankenkasse und im Berufsförderungsinstitut.

Ihm wurden zahlreiche Auszeichnungen verliehen. Unter anderem erhielt er das Goldene Ehrenzeichen für Verdienste um die Republik Österreich, das Goldene Ehrenzeichen des Landes Oberösterreich, die Würde des Ehrensenators der Johannes Kepler Universität Linz und die Viktor-Adler-Plakette.[2]