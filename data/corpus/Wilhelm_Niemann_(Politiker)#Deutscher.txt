Wilhelm Niemann (* 14. Juli 1949 in Dortmund; † 26. März 2012 in Osnabrück) war ein deutscher Politiker (CDU). Von 1999 bis 2004 war er der erste hauptamtliche Bürgermeister von Rheine. [1]
Niemann, geboren 1949 in Dortmund, studierte Rechtswissenschaften in Würzburg und war von 1989 bis 1999 Wahlbeamter und Dezernent beim Landkreis Osnabrück. Am 12. September 1999 wurde er bei der ersten Direktwahl des Bürgermeisters in Rheine mit einer absoluten Mehrheit von 52,1 % gewählt und löste damit den amtierenden Bürgermeister und Favoriten Günter Thum ab. Bei der nächsten Bürgermeisterwahl 2004 musste er sich einer Stichwahl stellen und verlor überraschend gegen Angelika Kordfelder von der SPD.

Vom Regierungspräsident als Sparberater nach Waltrop beordert, begleitete er die Stadt bis zum 31. Dezember 2007 bei Sparmaßnahmen.

Niemann war verheiratet und hatte drei Kinder, einen Sohn und zwei Töchter.
