Brigitte Fossey (* 15. Juni 1946 in Tourcoing) ist eine französische Schauspielerin.

Berühmt wurde sie bereits als Sechsjährige im Antikriegsdrama Verbotene Spiele (1952). Nach ihrem Comeback Ende der 1960er Jahre arbeitet sie vorwiegend im Autorenkino, in Deutschland unter anderem mit Hans W. Geißendörfer in Die gläserne Zelle. Anfang der 1980er Jahre war sie in den La Boum-Filmen an der Seite von Claude Brasseur die Mutter von Sophie Marceau.

In der Langfassung von Giuseppe Tornatores Film Cinema Paradiso (1988) spielte sie die Rolle der älteren Elena. Die Szenen, die aus der Kinofassung herausgeschnitten wurden, sind im Directors Cut in italienischer Sprache enthalten.

Brigitte Fossey war mit dem Filmemacher Jean-François Adam verheiratet. Aus der Ehe ging die Schauspielerin Marie Adam hervor.
