Mimnermos  von Kolophon (altgriechisch Μίμνερμος Mímnermos, latinisiert Mimnermus) war ein griechischer Dichter, der wohl um 600 v. Chr. lebte. Seine Werke sind nur fragmentarisch erhalten.

Mimnermos war der erste, der die Elegie zum Träger seiner Liebespoesie machte. Seine Schaffenszeit fiel in die Krisenzeit der Unabhängigkeitskämpfe der ionischen Städte Kleinasiens, die sich der wachsenden Kraft der lydischen Könige erwehrten. Eines der erhalten gebliebenen Fragmente seiner Gedichte bezieht sich auf diesen Kampf und kontrastiert die zunehmende Verweichlichung seiner Landsmänner mit der Tapferkeit derjenigen, die einmal den lydischen König Gyges besiegt hätten.

Seine wichtigsten Gedichte waren ein Satz Klagelieder, die an eine Nanno gerichtet und nach ihr benannt waren. Sie war seine Geliebte und Flötenspielerin. Gesammelt wurden diese Gedichte in zwei Büchern. Immer wiederkehrendes Motiv und Hauptthema ist der Gegensatz von liebesfroher Jugend und leidvollem Alter, aber auch Mythisches und Geschichtliches nahm er in den Kreis seiner Poesie auf. An sie knüpften dann alexandrinische und römische Elegiker an.

Seine Lebensdaten sind weitgehend unbekannt, jedoch lassen Schilderungen einer Sonnenfinsternis auf die Jahre um 600 schließen.
