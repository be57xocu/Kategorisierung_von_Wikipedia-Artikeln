Ernst Didring (* 18. Oktober 1868 in Stockholm; † 13. Oktober 1931 ebenda) war ein schwedischer Schriftsteller, der in seinen Romanen die industrielle Entwicklung seines Heimatlandes (besonders Norrlands und Stockholms) zeitkritisch schildert.

Ernst Didring wurde am 18. Oktober 1868 im Stockholmer Stadtteil Östermalm geboren.
Er begann eine Lehrerausbildung, die er jedoch wegen Geldmangels nicht abschließen konnte. Seit 1884 arbeitete er als Büroschreiber in der Leitung der Schwedischen Staatsbahn und engagierte sich später für die Gründung der Pensionsversicherung der Eisenbahn (1910). 
1899 heiratete er die dänische Lehrerin Jeanne Rye.

Im Jahr 1897 begann er mit dem Verfassen von Bühnenwerken sowie Dramen und Erzählungen. 1914 gab er seinen Beruf gänzlich zugunsten seiner Schriftstellertätigkeit auf und war in der Folge freischaffend tätig. In den Zeiträumen 1915–1920 und 1923–1929 stand er der Schwedischen Schriftstellervereinigung vor. Kurz vor seinem Tod, im Jahr 1931, wurde er mit dem Preis des „Samfundet De Nios“ ausgezeichnet.

Besonders hervorzuheben ist sein gesellschaftspolitisches Engagement, was einerseits in seinen Werken deutlich wird, andererseits auch durch aktive Mitarbeit in verschiedenen Vereinen. So leitete er in der Zeit von 1915 bis 1920 das Hilfskomitee des Schwedischen Roten Kreuzes für Kriegsgefangene. 
1920–1922 reiste er durch Europa (Frankreich, Schweiz, Italien und Deutschland).

Ernst Didring starb im Krankenhaus des Roten Kreuzes in Stockholm.

Sein Hauptwerk und damit das bekannteste Buch ist die Romantrilogie Malm (Erz), die von 1914 bis 1919 herausgegeben wurde. Darüber hinaus sind einige seiner weiteren Romane (Zyklus Skärenleben, Die Weltspinne, Die dunkelblaue Briefmarke) literaturhistorisch bedeutsam.

Seine Bücher zeichnen (in naturalistischer Weise) ein umfassendes Bild des Lebens in Schwedens Gesellschaft zur Zeit der Jahrhundertwende, welches manchmal auch heute aktuell erscheint. Oft vergingen nur wenige Monate zwischen der Herausgabe des Originals und seinen Übersetzungen. Vor allem in Deutschland fanden seine Werke einen großen Widerhall (die deutschen Ausgaben wurden fast alle vom Georg Westermann Verlag herausgegeben). Dabei zeichnen sich besonders die Übersetzungen von Else von Hollander-Lossow durch eine detailgetreue Textwiedergabe aus. Weiterhin schrieb er eine ganze Reihe von Theaterstücken, die in Stockholm sehr bekannt waren und zu seiner Zeit oft gespielt wurden.

In seiner Romantrilogie Malm (deutsch: Erz) schildert Ernst Didring die Arbeit an der „Erzbahn“ Kiruna–Narvik, den Aufbau der Grubenstadt Kiruna und das ökonomische Spiel mit dem Wert, der in den Erzfeldern geschaffen wird.

Pioniere ist einer der großen Romane über die Gleisbauer in der schwedischen Literatur. Aber es ist auch einer der großartigsten Schilderungen der Bergwelt, ein Dokument über eine Aufbruchszeit in der schwedischen Geschichte und eine warme und herzliche Anerkennung der Menschen hinter einer Pionierarbeit: Der Bau der Erzbahn zur Jahrhundertwende.

(schwedisch: 1914; deutsch: 1917 (Kiepenheuer), 1924 (Westermann) unter dem Titel: Hölle im Schnee; finnisch 1917)

Der Erzabbau wurde in Kiruna in vergleichsweise großem Stil aufgenommen – wir schreiben jetzt das Jahr 1909. Einige der Gleisarbeiter, die wir im „Roman aus dem Norden“ trafen, sind jetzt Bergarbeiter. Der Generalstreik wirft seinen Schatten über das Erzfeld und der Kampf wird nicht nur zwischen den Arbeitern und den Arbeitgebern geführt, sondern auch zwischen verschiedenen Fraktionen innerhalb der Arbeiter. Das Emigrationsfieber rast in die Grubengesellschaft – Brasilien ist das Ziel. Ein Teil wendet sich nach und nach zurück – aber nicht alle. Und am Krater des Romans (= Kiruna) ist in der Zwischenzeit viel geschehen ...

(schwedisch: 1915, deutsch: 1924) 

Stockholm, im Jahr vor dem Ersten Weltkrieg. Das Jahr des Generalstreiks, aber auch der Glanzzeit der Aktienspekulanten. Im Zentrum der Spekulationen stehen die Aktien, die ihren Wert durch den Erzabbau in Gällivare und Kiruna erhalten hatten. Der Wert steigt – die Welt ruft ja nach Erz, und es geht das Gerücht, dass der Staat das Erzfeld übernehmen will. Einer der Spieler ist Erik Valerius. Er spielt hoch. Auch in seiner Ehe mit Anna. Er gewinnt und er verliert. Und als ihn eines schönen Tages in der Baltischen Ausstellung in Malmö die Nachricht über den Kriegsausbruch erreicht – welche Karten hat er da noch zum Ausspielen?

(schwedisch: 1919, deutsch: 1924)

Die Geschichte dreier Generationen einer Familie, die auf Grålöga in den Stockholmer Schären spielt.

„Eine beredte, hinreißende, lebendige Hymne auf die Schären und ihre Menschen. Es sind keine Idealfiguren oder billige Lockvögel, die Didring hier aufstellt, sondern blutwarme Menschen mit ihren kleinen Tugenden und Sünden. Man muss sogleich an sie glauben, wie man an den Sonnenschein und den Sturm glaubt. Diese lebenswarmen Menschen stellt Didring in die große Natur. Es ist keine Übertreibung, wenn man sagt, dass der Zyklus „Schärenleben“ nicht nur eins der besten Bücher seines Verfassers ist (vielleicht das allerbeste), sondern auch einer der wertvollsten Schären-Romane, die überhaupt in schwedischer Sprache geschrieben sind – August Strindberg nicht ausgenommen.“

Aus einer zeitgenössischen Verlagsrezension: „Ein schwedischer Chemiker hat die fixe Idee, es müsse eine internationale Geldliga existieren, die spinnengleich ihre Fäden über die Welt zieht; Kriege, Revolutionen und Frieden macht, wenn es nur zu verdienen gibt. Also eine Geißel der Völker, eine Gesellschaft der eigentlichen Herren der Welt, die es zu entlarven gilt! Eine Jagd über den Erdball hebt an, einige Male glaubt der Enthusiast dem Ziele nah zu sein, aber schließlich muss er doch seine Idee als Phantom erkennen. Lose verknüpft mit der Haupthandlung ist das Schicksal seines französischen Freundes, eines Radiumforschers, der im Dienste seiner Wissenschaft als echter, sich aufopfernder Held dem sicheren Tode entgegen geht. Es sei gewarnt, das Buch an einem Abend zu beginnen – man liest es die Nacht durch.“[1]
(Schwedisch: 1923, deutsch: 1925, ungarisch: 1926)
