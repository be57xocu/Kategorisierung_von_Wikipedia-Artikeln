Irmgard Karwatzki (* 15. Dezember 1940 in Duisburg; † 9. Dezember 2007 in Mönchengladbach) war eine deutsche Politikerin (CDU).

Sie war von 1982 bis 1987 Parlamentarische Staatssekretärin beim Bundesminister für Jugend, Familie und Gesundheit, von 1987 bis 1989 beim Bundesminister für Bildung und Wissenschaft und von 1994 bis 1998 beim Bundesminister der Finanzen.

Irmgard Karwatzki war die Tochter eines Duisburger Hafenarbeiters. Nach dem Besuch der Volksschule absolvierte Irmgard Karwatzki eine kaufmännische Lehre und war anschließend als kaufmännische Angestellte tätig. Daneben besuchte sie die Höhere Fachschule für Sozialarbeit, die sie als staatlich anerkannte Sozialarbeiterin (grad.) beendete. Anschließend war sie Referentin beim Bund der Deutschen Katholischen Jugend (BDKJ) im Bistum Essen und von 1971 bis 1976 Referentin an der Katholischen Fachhochschule Nordrhein-Westfalen (KFH NW).

Sie starb kurz vor ihrem 67. Geburtstag an einem Herzinfarkt.

Irmgard Karwatzki war seit 1965 Mitglied der CDU und gehörte ab 1975 dem Landesvorstand der CDU des Rheinlandes an.

Von 1975 bis 1977 sowie von 1979 bis 1990 gehörte sie dem Rat der Stadt Duisburg an.

Von 1976 bis 2005 war Irmgard Karwatzki Mitglied des Deutschen Bundestages. Sie zog stets über die Landesliste Nordrhein-Westfalen in den Bundestag ein.

Von Oktober 1979 bis Juni 1983 war sie Dritte Bürgermeisterin der Stadt Duisburg.

Am 4. Oktober 1982 wurde Irmgard Karwatzki als Parlamentarische Staatssekretärin beim Bundesminister für Jugend, Familie und Gesundheit in die von Bundeskanzler Helmut Kohl geführte Bundesregierung berufen. Nach der Bundestagswahl 1987 wechselte sie am 12. März 1987 in gleicher Funktion zum Bundesminister für Bildung und Wissenschaft und schied anlässlich einer Kabinettumbildung am 21. April 1989 aus dem Amt.

Nach der Bundestagswahl 1994 wurde sie am 17. November 1994 zur Parlamentarischen Staatssekretärin beim Bundesminister der Finanzen ernannt. Nach der Bundestagswahl 1998 schied sie am 28. Oktober 1998 aus der Regierung aus.

Seit 1999 gehörte sie dem Vorstand des Caritasverbandes und dem Aufsichtsrat der Bank im Bistum Essen eG an.
