Jacob Nufer (* 15. Jahrhundert; † 16. Jahrhundert) war ein Schweineschneider (Tierkastrator) aus dem Schweizer Kanton Thurgau. Er soll um das Jahr 1500 in Siegershausen die erste Schnittentbindung (Kaiserschnitt) durchgeführt haben, bei dem die Mutter, Elisabeth Alespach, die Operation überlebt hat.

Die Hebammen dieser Zeit kannten zwar die Technik des Kaiserschnitts, doch bis in das frühe 16. Jahrhundert hinein durften Kaiserschnitte nur an toten Müttern vorgenommen werden. Das Leben der Mütter zu retten und dafür zu riskieren, die Kinder im Mutterleib zu töten, war strengstens verboten. Als Jacob Nufers Ehefrau schon mehrere Tage in den Wehen lag und anscheinend einem qualvollen Tod entgegenging, entschloss sich ihr Mann, den lebensrettenden Eingriff selbst vorzunehmen. Mutter und Kind überlebten. Die Frau soll danach – allerdings auf normalem Weg – im nächsten Jahr noch Zwillinge und danach weitere vier Kinder zur Welt gebracht haben.

Es wird angenommen, dass Jacob Nufer die für den Eingriff notwendigen anatomischen Kenntnisse durch genaue Beobachtung des Viehs erlangte, mit dem er täglich arbeitete. Wahrscheinlich verfügte er in dieser Hinsicht über ein umfassenderes Wissen als die Ärzte seiner Zeit.

In Siegershausen wurde die Jakob Nuferstrasse nach ihm benannt.
