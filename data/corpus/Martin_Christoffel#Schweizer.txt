Martin Christoffel (* 2. September 1922 in Basel; † 3. April 2001 in Rombach) war ein Schweizer Schachspieler.

Christoffel war Mitglied der Schachgesellschaft Baden. Er wurde viermal Schweizer Meister, nämlich 1943 in St. Gallen, 1945 in Lugano, 1948 in Bern (nach Stichkampf mit Fritz Gygli) und 1952 in Zürich. 1944 siegte er im Coupe Suisse.

Ein grosser internationaler Erfolg war 1946 in London der zweite Platz hinter Euwe. Im internationalen Turnier Zürich 1952 belegte er gemeinsam mit Euwe hinter Lundin Platz 2. 1952 wurde Christoffel Internationaler Meister der FIDE.[1]
Nach seiner Pensionierung wurde er wieder aktiver. So gewann er zwischen 1990, 1991, 1992 und 1994 viermal die Schweizer Seniorenmeisterschaft.

Auch im Fernschach war Christoffel aktiv. Er nahm an der 9., 10. und 11. Olympiade teil. 1989 erhielt er den Titel Internationaler Fernschach-Meister, 2000 den Titel Verdienter Internationaler Meister. Auch war er an der Gründung der Schweizer Fernschachvereinigung (SFSV) massgeblich beteiligt.

Ab 1983 leitete Christoffel die Schweizer Fernturniere. Von 1987 bis 1991 war er Zentralpräsident des Schweizer Schachverbandes. Zeitweise war er Mitglied des Vorstandes der Schweizerischen Fernschachvereinigung (SFSV).
