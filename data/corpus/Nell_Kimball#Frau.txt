Nell Kimball (* 14. Juni 1854, Illinois, USA; † 1934, USA) war eine US-amerikanische Prostituierte und Autorin.

Kimball ist ausschließlich durch ihre Autobiografie bekannt, die als Sittengeschichte ihrer Zeit gelten kann.

Kimball wuchs unter ärmlichen Bedingungen auf einer Farm auf. Engste Bezugsperson war eine Schwester der Mutter, eine ehemalige Prostituierte, die hier ihren Lebensabend fristete. 

Im Alter von 15 Jahren brannte sie mit einem Bürgerkriegsveteranen durch, der sie ausnahm und bald verließ. Daraufhin bewarb sie sich in einem Bordell, wobei sie ihre Tante als Referenz angab.

Später lebte sie in bürgerlichen Verhältnissen als ausgehaltene Geliebte, dann als Ehefrau eines Bankräubers / Safeknackers, dann als alleinerziehende Witwe. Nach dem Tod ihres Kindes kehrte sie in ihr früheres Leben zurück, jetzt allerdings als Betreiberin eines Bordells. Sie schloss ihr letztes Bordell, als die Prostitution während des Ersten Weltkrieges in New Orleans verboten wurde.

In der Weltwirtschaftskrise verlor sie ihr Vermögen und versuchte mit der Veröffentlichung ihrer Memoiren ein neues Einkommen zu erschließen. Wegen des kontroversen Charakters ihres Werkes verzögerte sich die Veröffentlichung mehrere Jahrzehnte bis nach ihrem Tod.
