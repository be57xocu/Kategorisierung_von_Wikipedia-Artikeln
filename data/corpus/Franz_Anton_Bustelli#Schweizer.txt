Franz Anton Bustelli (* 11./12. April 1723 in Locarno, Schweiz; † 18. April 1763 in München) war Bildhauer und Modellierer von Porzellanfiguren. Er gilt als der bedeutendste Porzellankünstler des Rokoko neben Johann Joachim Kändler, der für Meissen arbeitete.

Über sein frühes Leben ist nichts bekannt. In der Schweiz geboren, scheint er in Bayern aufgewachsen zu sein. Er schrieb deutsch und benutzte dazu die deutsche Schrift. Vom 3. November 1754 bis zu seinem Tod 1763 wirkte er an der Porzellanmanufaktur Nymphenburg, die bis 1761 im Schlösschen Neudeck in der Münchener Au untergebracht war. Er schuf ca. 150 Figuren zu den Themen Frömmigkeit, Chinoiserie, Galanterie, ovidischer Götterhimmel und Volksleben. Sein berühmtestes Werk sind die 16 Figuren der Commedia dell’arte (zu sehen im Porzellanmuseum in Schloss Nymphenburg). Typisch für die Werke Bustellis sind die flachen, schlichten Rocaille-Sockel und die langgezogenen Glieder der Figuren, die sie – im Gegensatz zu den Arbeiten Kändlers – besonders leicht und grazil erscheinen lassen.

Liebesgruppe, 1756, Porzellanmanufaktur Nymphenburg, Bayerisches Nationalmuseum in München

Liebesgruppe, 1756, Porzellanmanufaktur Nymphenburg, Bayerisches Nationalmuseum in München

Liebesgruppe, 1756, Porzellanmanufaktur Nymphenburg, Bayerisches Nationalmuseum in München

Liebesgruppe, 1756, Porzellanmanufaktur Nymphenburg, Bayerisches Nationalmuseum in München
