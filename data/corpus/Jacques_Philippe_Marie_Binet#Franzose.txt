Jacques Philippe Marie Binet (* 2. Februar 1786 in Rennes; † 12. Mai 1856 in Paris) war ein französischer Mathematiker.

Binet war vom 22. November 1804 bis zu seiner Graduierung 1806 an der École polytechnique und arbeitete danach an der École Nationale des Ponts et Chaussées. Ab 1807 lehrte er an der École Polytechnique und wurde 1823 Professor für Astronomie am Collège de France. 

Grundlagen der Matrixtheorie werden Binet zugesagt, etwa die Definition der Matrizenmultiplikation. Nach ihm sind der Satz von Binet-Cauchy benannt sowie eine nicht-rekursive Formel für das n-te Glied un{\displaystyle u_{n}} der Fibonacci-Folge, die er 1843 fand:

Diese Formel war zuvor jedoch bereits Leonhard Euler, Daniel Bernoulli und Abraham de Moivre bekannt.

Nach Binet ist die Summe zweier Hauptträgheitsmomente eines Körpers immer größer als dessen weiteren Trägheitsmomente. Ferner sind alle Hauptträgheitsmomente immer Summen zweier Binet’scher Trägheitsmomente.[2]