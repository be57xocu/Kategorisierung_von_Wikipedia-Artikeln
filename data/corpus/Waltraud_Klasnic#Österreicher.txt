Waltraud Klasnic [klasnik] (* 27. Oktober 1945 in Graz, Steiermark als Waltraud Tschiltsch, später adoptierte Mlinaritsch) ist eine österreichische Politikerin (ÖVP) und war von 23. Jänner 1996 bis zum 25. Oktober 2005 Landeshauptfrau der Steiermark.

Klasnic trat 1970 der Österreichische Frauenbewegung, einer Organisation der ÖVP, bei und engagierte sich hier sowie später auch im Wirtschaftsbund (ebenfalls eine ÖVP-Teilorganisation). Politisch war sie von 1977 bis 1981 als Mitglied des Bundesrats tätig, wechselte dann in den Steiermärkischen Landtag und wurde 1983 3. Landtagspräsidentin. 1988 wurde sie Landesrätin für Wirtschaft, Tourismus und Verkehr und ab 1993 gleichzeitig Landeshauptmannstellvertreterin in der Steiermark. 1990 bis 1997 war sie Landesgruppenobfrau des Wirtschaftsbunds, von März 1996 bis März 2006 war sie Landesparteiobfrau der Steirischen Volkspartei.

Nachdem bei der Landtagswahl 1995 die ÖVP nur noch knapp den ersten Platz halten konnte, trat Josef Krainer junior zurück und schlug Landesrat Gerhard Hirschmann zum Nachfolger vor, dieser verzichtete aber zugunsten Klasnics. Diese wurde am 23. Jänner 1996 zur Landeshauptfrau der Steiermark gewählt und wurde somit die erste Landeschefin Österreichs. Anders als die acht Jahre später zur Salzburger Landeshauptfrau gewählte Gabi Burgstaller legte Klasnic darauf Wert, als „Frau Landeshauptmann“ angesprochen zu werden.

Bei den Landtagswahlen 2000 konnte die ÖVP um mehr als 11 % zulegen, sie verpasste im Landtag um nur ein Mandat die absolute Mehrheit, die sie jedoch in der nach Proporz besetzten Landesregierung erhielt. In ihre Zeit als Landeshauptfrau fiel der Aufbau des „Autoclusters Steiermark“, in dem rund um den Leitbetrieb Magna Steyr speziell kleine und mittlere Zulieferfirmen gefördert wurden.

Danach geriet sie durch einen Skandal beim Landesenergieversorger ESTAG und nach dem Scheitern eines Motorsportprojektes am A1-Ring in Spielberg, dem Rechnungshofbericht zum Tierpark Herberstein im August 2005 und der Tatsache, dass ihr ehemaliger Parteikollege Hirschmann bei der darauf folgenden Landtagswahl mit einer eigenen Liste antrat, immer stärker unter Druck. Bei der Landtagswahl am 2. Oktober 2005 verlor die steirische Volkspartei über acht Prozent der Stimmen und die Mehrheit in der Landesregierung, worauf Klasnic noch am Abend des Wahltages ankündigte, der zukünftigen Landesregierung und auch dem Landtag nicht mehr angehören zu wollen. In der Landesregierung folgte ihr Hermann Schützenhöfer als führender Vertreter der ÖVP nach, der stellvertretender Landeshauptmann wurde. Ihr Nachfolger als Landeshauptmann wurde am 25. Oktober 2005 Franz Voves (SPÖ).

Seit ihrem Ausscheiden aus der Berufspolitik engagiert sich Klasnic ehrenamtlich unter anderem als Vorsitzende des Kuratoriums des Zukunftsfonds der Republik Österreich (2006 bis Jänner 2011) und als Präsidentin des Dachverbandes Hospiz Österreich. Seit 2006 ist Klasnic Mitglied im Europäischen Wirtschafts- und Sozialausschuss. Sie war auch Beraterin für sozio-ökonomische Fragen im Magna-Konzern Frank Stronachs und hat 2007 mit ihrem langjährigen Mitarbeiter Herwig Hösele die Beratungsfirma Dreischritt GmbH gegründet.

Im März 2010 wurde Klasnic zur Opferbeauftragten der österreichischen katholischen Kirche ernannt. In dieser Position leitet sie eine Kommission, welche die Unabhängigkeit der Aufklärung der aktuellen Missbrauchsfälle von Kindern durch Angehörige der katholischen Kirche in Österreich gewährleisten soll. Vertreter der Plattform Betroffener kirchlicher Gewalt[1] stehen dieser Entscheidung skeptisch gegenüber und hinterfragen die Unabhängigkeit der angekündigten Stelle.[2][3]
Im Juni 2012 wurde Waltraud Klasnic vom Senat der Montanuniversität Leoben für die Funktionsperiode 2013 bis 2018 zum Mitglied des Universitätsrats bestellt.

Im Jahr 2017 zog der ÖSV Klasnic als Opferschutzbeauftragte bei, nachdem die ehemalige Schifahrerin Nicola Werdenigg über mehrere sexuelle Übergriffe im Umfeld des ÖSV in den siebziger Jahren berichtet hatte.[4]
Nach einer Ausbildung im Fachhandel baute sie gemeinsam mit ihrem Gatten ein Transportunternehmen auf. Waltraud Klasnic ist verwitwet (ihr Mann Simon starb 2015) und hat drei Kinder sowie fünf Enkelkinder.

Erste Republik:Wilhelm Kaan |
Anton Rintelen |
Franz Prisching |
Alfred Gürtler |
Hans Paul |
Anton Rintelen |
Alois Dienstleder |
Karl Maria Stepan |
Rolph Trummer

Nationalsozialismus (Gauleiter):
Sepp Helfrich |
Siegfried Uiberreither

Zweite Republik:Reinhard Machold |
Anton Pirchegger |
Josef Krainer senior |
Friedrich Niederl |
Josef Krainer junior |
Waltraud Klasnic |
Franz Voves |
Hermann Schützenhöfer


Siehe auch: Liste der Landesregierungen der Steiermark


Alois Dienstleder |
Alfons Gorbach |
Josef Krainer senior |
Friedrich Niederl |
Josef Krainer junior |
Waltraud Klasnic |
Hermann Schützenhöfer

Erste Präsidenten:
Josef Wallner |
Franz Thoma |
Josef Wallner |
Karl Brunner |
Richard Kaan |
Franz Koller |
Hanns Koren |
Franz Feldgrill |
Franz Wegart |
Franz Hasiba |
Reinhold Purr |
Siegfried Schrittwieser |
Kurt Flecker |
Manfred Wegscheider |
Franz Majcen |
Bettina Vollath

Zweite Präsidenten:
Franz Stockbauer |
Karl Operschall |
Anton Afritsch |
Franz Ileschitz |
Hans Gross |
Annemarie Zdarsky |
Christoph Klauser |
Dieter Strenitz |
Anna Rieder |
Walburga Beutl |
Franz Majcen |
Ursula Lackner |
Manuela Khom

Dritte Präsidenten:
Franz Scheer |
Anton Stephan |
Franz Koller |
Helmut Heidinger |
Franz Feldgrill |
Waltraud Klasnic |
Lindi Kálnoky |
Ludwig Rader |
German Vesko |
Hans Kinsky |
Walburga Beutl |
Barbara Gross |
Ursula Lackner |
Werner Breithuber |
Gerhard Kurzmann
