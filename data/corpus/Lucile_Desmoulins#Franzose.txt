Lucile Pierre Desmoulins, geborene Duplessis (* 1770 in Paris; † 13. April 1794 in Paris auf der Guillotine) war die Frau des französischen Revolutionärs Camille Desmoulins.

Anne-Lucile-Philippe Laridon Duplessis war die Tochter von Claude Etienne Laridon Duplessis und Anne Françoise Marie Boisdeveix. Ihre wohlhabenden, bürgerlichen Eltern waren Patrioten und Revolutionäre und empfingen in ihrem Haus unter anderem Robespierre, Brissot, Danton und Desmoulins. Robespierre machte Lucile Anträge, doch sie gab Desmoulins den Vorzug.

Sie heiratete Camille Desmoulins am 29. Dezember 1790. Bei der Hochzeit in der Kirche St-Sulpice waren Robespierre, Brissot und Pétion de Villeneuve unter den Trauzeugen. Aus der Ehe ging ein Sohn, Horace Camille Desmoulins (* 6. Juni 1792; † im Juni 1825 in Haiti), hervor. Robespierre war dessen Taufpate.

Vor der Hinrichtung ihres geliebten Mannes am 5. April 1794 schrieb Lucile Desmoulins Gnadenersuche an Robespierre, in denen sie auf deren Freundschaft hinwies. Während der Exekution wandte sie sich an das Volk und rief zum Aufstand gegen das Regime auf. Als Anführerin eines Komplotts zur Befreiung ihres Mannes wurde sie der Verschwörung angeklagt, zum Tode verurteilt und am 13. April 1794 auf der Guillotine hingerichtet. Sie soll über ihr Urteil glücklich gewesen sein: „Sendet mich zu meinem Ehemann.“

Georg Büchner machte sie zur Heldin seines Dramas Dantons Tod.

1978 entstand der TV-Film La passion de Camille et Lucile Desmoulins mit Claude Jade und Bernard Alane in den Hauptrollen.

Antonius Lux (Hrsg.): Große Frauen der Weltgeschichte. Tausend Biographien in Wort und Bild. Sebastian Lux Verlag, München 1963, S. 128
