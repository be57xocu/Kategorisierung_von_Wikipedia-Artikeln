Lewis Paul Bremer III (* 30. September 1941 in Hartford, Connecticut) ist ein US-amerikanischer Diplomat und Regierungsbeamter. Er wurde am 6. Mai 2003 von Präsident George W. Bush zum Zivilverwalter für den Irak ernannt und löste damit den bisherigen Verwalter Jay Garner nach nur wenigen Wochen ab.

Bremer absolvierte die Phillips Academy in Andover (Massachusetts). Danach studierte er an den Universitäten von Yale und Harvard sowie am Institut d’études politiques de Paris Ökonomie und Politikwissenschaft.

Von 1966 bis 1989 war der Republikaner Bremer für das US-Außenministerium tätig. In dieser Zeit bekleidete er unter anderem den Posten des Botschafters in den Niederlanden (1983 bis 1986). Ab 1986 leitete er als Assistant Secretary of State die Unterabteilung für Terrorbekämpfung.[1] 1989 wechselte Bremer als Experte für Risikoabschätzung in die freie Wirtschaft. 
1989 wurde er führender Manager bei Kissinger and Associates, einem Beratungsunternehmen, das Henry Kissinger gegründet hatte.
Im Juni 2000 warnte er als Vorsitzender der Nationalen Terrorismuskommission vor einem großen Terroranschlag in den USA. Im Jahr 2002 verpflichtete ihn Präsident Bush für sein Beratungsgremium für Innere Sicherheit. Am 6. Mai 2003 wurde er Chef der Koalitions-Übergangsverwaltung für den Irak und behielt die Stellung bis zur Übergabe der Souveränität an eine irakische Übergangsregierung am 28. Juni 2004 bei.
[2]
Am 19. September 2003 erließ Paul Bremer in seiner Funktion als Verwalter eine Reihe von Verordnungen, welche die irakische Wirtschaftspolitik gründlich veränderten: Er privatisierte praktisch alle staatlichen Betriebe innerhalb kürzester Zeit, zum Teil zu sehr niedrigen Preisen, erlaubte volle Besitzrechte ausländischer Firmen an irakischen Betrieben und völlige Repatriierung von Profiten; auch irakische Banken wurden für die Übernahme durch ausländisches Kapital geöffnet, Zölle wurden vollständig abgeschafft. Allerdings war die Ölindustrie von diesen Vorgaben vorerst ausgenommen.

Weitere wesentliche Entscheidungen Bremers waren das Verbot der Baath-Partei mit 50.000 Mitgliedern und die Auflösung der Irakischen Armee mit 450.000 Angehörigen. Privatisierung, Parteiverbot und Armee-Auflösung bewirkten soziale Deklassierung für Millionen Iraker und gelten als Grund für den bis heute fehlenden Ausgleich zwischen den politischen Gruppen bzw. den Religionsgemeinschaften des Irak[3]. Seit der Besetzung des Iraks durch amerikanische Truppen bis heute sind ca. 120.000 Zivilisten umgekommen[3].

Es wurde auch ein Einheitssteuersystem mit einem degressiven Steuersatz eingeführt. Streiks wurden verboten und das Recht auf gewerkschaftliche Organisierung eingeschränkt.[4]
2004 überreichte US-Präsident George W. Bush Bremer die Freiheitsmedaille („The Presidential Medal of Freedom“), die höchste zivile Auszeichnung in den USA.

Abd al-Karim Qasim |
Ahmad Hasan al-Bakr |
Tahir Yahya |
Arif Abd ar-Razzaq |
Abd ar-Rahman al-Bazzaz |
Nadschi Talib |
Abd ar-Rahman Arif |
Tahir Yahya |
Abd ar-Razzaq an-Naif |
Ahmad Hasan al-Bakr |
Saddam Hussein |
Saadun Hammadi |
Muhammad Hamza az-Zubaidi |
Ahmad Hussein al-Khudayir |
Saddam HusseinUS-Zivilverwalter: 
Jay Garner |
Paul BremerIyad Allawi |
Ibrahim al-Dschafari |
Nuri al-Maliki |
Haider al-Abadi
