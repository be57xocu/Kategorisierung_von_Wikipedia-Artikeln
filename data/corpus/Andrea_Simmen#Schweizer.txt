Andrea Simmen (* 15. Dezember 1960 in Zürich; † 19. Juli 2005 in Flaach, Schweiz) war eine Schweizer Schriftstellerin. 

Andrea Simmen arbeitete als Köchin, Dekorateurin und Gärtnerin. Nebenher begann sie zu schreiben. Ab 1990 war sie vollberufliche Schriftstellerin. Sie veröffentlichte Romane und Erzählungen.

1991, 1993 und 1995 erschienen die Erzählbände Ich bin ein Opfer des Doppelpunkts, Landschaft mit Schäfer und anderen Reizen und Vielleicht heisst er Paul. 2001 folgte der Roman Der eingeschneite Hund.

Andrea Simmen wurde mit verschiedenen Preisen geehrt: So erhielt sie 1991 die Ehrengabe des Kantons Zürich, 1994 den Werkbeitrag Pro Helvetia, 1995 und 1999 ein Werkjahr und einen Werkbeitrag des Kantons Aargau. 1994 nahm sie am Ingeborg-Bachmann-Wettbewerb in Klagenfurt teil.
