François-Régis de Chantelauze (* 23. März 1821 in Montbrison, Loire; † 3. Januar 1888 in Paris) war ein französischer Historiker.

Régis de Chantelauze befasste sich insbesondere mit bestimmten Epochen der französischen Geschichte, die bislang nicht im Mittelpunkt der Geschichtsschreibung standen. Er verfasste mehrere Monographien anhand bislang unveröffentlichter Quellen, zu denen das Tagebuch des Leibarztes von Maria Stuart, Dominique Bourgoing, gehörte.
