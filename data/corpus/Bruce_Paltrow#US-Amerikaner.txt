Bruce Paltrow (* 26. November 1943 in Brooklyn, New York City, New York; † 3. Oktober 2002 in Rom, Italien) war ein US-amerikanischer Film- und Fernsehproduzent.

Paltrow studierte an der Tulane Universität in New Orleans, Louisiana. In den späten 1960er Jahren begann er Regie bei Bühnenproduktionen in New York City zu führen, wo er die Schauspielerin Blythe Danner kennenlernte und 1969 heiratete. Zu seinen Produktionen zählten die Fernsehserien The White Shadow und Chefarzt Dr. Westphall. Seine letzte Produktion war der Spielfilm Traumpaare mit seiner Tochter Gwyneth Paltrow als Hauptdarstellerin. Auch sein Sohn Jake Paltrow ist im Showgeschäft als Produzent und Regisseur tätig.

Bruce Paltrow starb im Alter von 58 Jahren während eines gemeinsamen Urlaubs in Rom zur Feier des 30. Geburtstags seiner Tochter. Er hatte mehrere Jahre an Krebs gelitten und erlag schließlich den Folgen einer Lungenentzündung.
