Marika Kilius (* 24. März 1943 in Frankfurt am Main) ist eine ehemalige deutsche Roll- und Eiskunstläuferin, die sowohl bei Weltmeisterschaften als auch bei den Olympischen Winterspielen im Paarlauf erfolgreich war. In ihrer kurzen Schallplattenkarriere war sie ebenfalls erfolgreich.

Bereits mit vier Jahren lernte Kilius Rollschuhlaufen und wurde mit 15½ Jahren Rollkunstlauf-Weltmeisterin, die jüngste deutsche Weltmeisterin aller Zeiten. Mit Franz Ningel wurde sie von 1955 bis 1957 deutsche Meisterin im Rollkunstlauf der Paare.

Als Tochter eines Friseurs in Frankfurt geboren, begann Marika Kilius mit dem Eiskunstlauf zunächst als Einzelläuferin, wechselte jedoch früh zum Paarlauf. Ihr erster Partner war Franz Ningel. Mit ihm wurde sie 1955 bis 1957 deutscher Meister im Paarlauf. Im gleichen Zeitraum nahm das Paar dreimal an Europameisterschaften teil und gewann dort, in Budapest, Paris und Wien, stets die Bronzemedaille. Bei Weltmeisterschaften errangen Kilius und Ningel zwei Medaillen. 1956 in Garmisch-Partenkirchen gewannen sie Bronze und 1957 in Colorado Springs wurden sie Vizeweltmeister hinter den Kanadiern Barbara Wagner und Robert Paul. Bei den Olympischen Spielen 1956 in Cortina d’Ampezzo verpassten sie als Vierte knapp eine Medaille. 

Noch 1957 wechselte Kilius den Eiskunstlaufpartner und trat von da an mit Hans-Jürgen Bäumler an. Mit ihm wurde sie in den Jahren 1958, 1959, 1963 und 1964 deutsche Meisterin im Paarlauf. Von 1959 bis 1964 wurden sie sechsmal in Folge Europameister. Ihre erste Weltmeisterschaftsmedaille gewannen sie 1959 in Colorado Springs mit Silber hinter den Kanadiern Barbara Wagner und Robert Paul. 1960 in Vancouver errangen sie die Bronzemedaille. 1963 in Cortina d’Ampezzo und 1964 in Dortmund wurden Kilius und Bäumler Weltmeister. Zusammen bestritten sie zwei Olympische Spiele. Sowohl 1960 in Squaw Valley wie auch 1964 in Innsbruck gewannen sie die Silbermedaille, erst hinter Wagner und Paul, dann hinter Ljudmila Beloussowa und Oleg Protopopow aus der Sowjetunion. 1964 wurde ihnen die Medaille jedoch aberkannt, nachdem im August bekannt geworden war, dass sie noch vor den Olympischen Spielen einen Profivertrag unterzeichnet hatten.[1][2] Daraufhin gaben sie die 1964er Medaillen 1966 an das IOC zurück. 1987 bekamen sie jedoch die Medaillen vom Internationalen Olympischen Komitee zurück und wurden vollständig rehabilitiert.

Sie und ihr Partner Hans-Jürgen Bäumler gaben Ende Juni 1966 bei einem Schaulaufen in Berlin ihren Rücktritt vom aktiven Sport bekannt.[3]
2011 wurde sie – zusammen mit Hans-Jürgen Bäumler – in die Hall of Fame des deutschen Sports aufgenommen.

(mit Franz Ningel)

(mit Hans-Jürgen Bäumler)

Unmittelbar nach den Olympischen Winterspielen 1964 und noch vor den 1964er Weltmeisterschaften schloss Marika Kilius einen Schallplattenvertrag mit dem deutschen Ableger der US-amerikanischen Plattenfirma Columbia – CBS ab, der im Februar 1964 die Single Wenn die Cowboys träumen / Zwei Indianer aus Winnipeg veröffentlichte. Der Titel Wenn die Cowboys träumen erschien bereits Anfang März in der Hit-Parade der Musikfachzeitschrift Musikmarkt und stieg danach bis zum Platz zwei auf. Ebenso erfolgreich war die Duettaufnahme mit Hans-Jürgen Bäumler Honeymoon in St. Tropez, die im Juli 1964 Platz zwei eroberte. Weitere Platzierungen erreichten die Titel Kavalier, Kavalier (18.), Ich bin kein Eskimo (23.) und ein weiteres Duett mit Hans-Jürgen Bäumler (Erst kam ein verliebter Blick, 26.). Bis 1965 nahm Marika Kilius fünf Solo- und drei Duettsingles (mit H.-J. Bäumler) auf. 

Parallel zu ihren ersten Plattenaufnahmen wirkte Marika Kilius in dem Eisrevuefilm von Franz Antel Die große Kür mit. Der Film, in dem sie sich selbst spielte und in dem sie ihre Schallplattentitel Kavalier, Kavalier, Ich bin kein Eskimo und Honey-Moon in St. Tropez (mit H.-J. Bäumler) sang, hatte im Oktober 1964 Premiere. 1967 drehte sie mit Bäumler noch eine Fortsetzung, Das große Glück und war mit ihm 1971 im Film Einer spinnt immer in einem Cameo-Auftritt zu sehen.

1964 heiratete Marika Kilius den 23-jährigen Fabrikantensohn (Ibelo-Feuerzeuge) Werner Zahn aus Sulzbach am Main/Frankfurt. Wie die erste Ehe wurde auch die zweite Ehe mit dem amerikanischen Geschäftsmann Jake Orfield geschieden. Sie hat zwei Kinder aus ihrer ersten Ehe sowie zwei Enkeltöchter.

Im Oktober 2006 gehörte sie gemeinsam mit ihrem ehemaligen Partner Hans-Jürgen Bäumler der Jury in der RTL-Show Dancing on Ice an.

1908: Anna Hübler und Heinrich Burger |
1909: Phyllis Johnson und James H. Johnson |
1910: Anna Hübler und Heinrich Burger |
1911: Ludowika Jakobsson und Walter Jakobsson |
1912: Phyllis Johnson und James H. Johnson |
1913: Helene Engelmann und Karl Mejstrik |
1914: Ludowika Jakobsson und Walter Jakobsson |
1922: Helene Engelmann und Alfred Berger |
1923: Ludowika Jakobsson und Walter Jakobsson |
1924: Helene Engelmann und Alfred Berger |
1925: Herma Szabó und Ludwig Wrede |
1926: Andrée Joly und Pierre Brunet |
1927: Herma Szabó und Ludwig Wrede |
1928: Andrée Joly und Pierre Brunet |
1929: Lilly Scholz und Otto Kaiser |
1930: Andrée Brunet und Pierre Brunet |
1931: Emília Rotter und László Szollás |
1932: Andrée Brunet und Pierre Brunet |
1933–35: Emília Rotter und László Szollás |
1936–39: Maxi Herber und Ernst Baier |
1947–48: Micheline Lannoy und Pierre Baugniet |
1949: Andrea Kékesy und Ede Király |
1950: Karol Kennedy und Peter Kennedy |
1951–52: Ria Baran und Paul Falk |
1953: Jennifer Nicks und John Nicks |
1954–55: Frances Dafoe und Norris Bowden |
1956: Sissy Schwarz und Kurt Oppelt |
1957–60: Barbara Wagner und Robert Paul |
1962: Maria Jelinek und Otto Jelinek |
1963–64: Marika Kilius und Hans-Jürgen Bäumler |
1965–68: Ljudmila Beloussowa und Oleg Protopopow |
1969–72: Irina Rodnina und Alexei Ulanow |
1973–78: Irina Rodnina und Alexander Saizew |
1980: Marina Tscherkassowa und Sergei Schachrai |
1981: Irina Worobjowa und Igor Lissowski |
1982: Sabine Baeß und Tassilo Thierbach |
1983: Jelena Walowa und Oleg Wassiljew |
1984: Barbara Underhill und Paul Martini |
1985: Jelena Walowa und Oleg Wassiljew |
1986–87: Jekaterina Gordejewa und Sergei Grinkow |
1988: Jelena Walowa und Oleg Wassiljew |
1989–90: Jekaterina Gordejewa und Sergei Grinkow |
1991–92: Natalja Mischkutjonok und Artur Dmitrijew |
1993: Isabelle Brasseur und Lloyd Eisler |
1994: Jewgenija Schischkowa und Wadim Naumow |
1995: Radka Kovaříková und René Novotný |
1996: Marina Jelzowa und Andrej Buschkow |
1997: Mandy Wötzel und Ingo Steuer |
1998–99: Jelena Bereschnaja und Anton Sicharulidse |
2000: Maria Petrowa und Alexei Tichonow |
2001: Jamie Salé und David Pelletier |
2002–03: Shen Xue und Zhao Hongbo |
2004–05: Tatjana Totmjanina und Maxim Marinin |
2006: Pang Qing und Tong Jian |
2007: Shen Xue und Zhao Hongbo |
2008–09: Aljona Savchenko‎ und Robin Szolkowy |
2010: Pang Qing und Tong Jian |
2011–12: Aljona Savchenko‎ und Robin Szolkowy |
2013: Tatjana Wolossoschar und Maxim Trankow |
2014: Aljona Savchenko‎ und Robin Szolkowy |
2015–16: Meagan Duhamel und Eric Radford |
2017: Sui Wenjing und Han Cong

1930–31: Olga Orgonista und Sándor Szalay |
1932: Andrée Brunet und Pierre Brunet |
1933: Idi Papez und Karl Zwack |
1934: Emília Rotter und László Szollás |
1935–39: Maxi Herber und Ernst Baier |
1947: Micheline Lannoy und Pierre Baugniet |
1948–49: Andrea Kékesy und Ede Király |
1950: Marianna Nagy und László Nagy |
1951–52: Ria Baran und Paul Falk |
1953: Jennifer Nicks und John Nicks |
1954: Silvia Grandjean und Michel Grandjean |
1955: Marianna Nagy und László Nagy |
1956: Sissy Schwarz und Kurt Oppelt |
1957–58: Věra Suchánková und Zdeněk Doležal |
1959–64: Marika Kilius und Hans-Jürgen Bäumler |
1965–68: Ljudmila Beloussowa und Oleg Protopopow |
1969–72: Irina Rodnina und Alexei Ulanow |
1973–78: Irina Rodnina und Alexander Saizew |
1979: Marina Tscherkassowa und Sergei Schachrai |
1980: Irina Rodnina und Alexander Saizew&;|
1981: Irina Worobjowa und Igor Lissowski |
1982–83: Sabine Baeß und Tassilo Thierbach |
1984–86: Jelena Walowa und Oleg Wassiljew |
1987: Larissa Selesnjowa und Oleg Makarow |
1988: Jekaterina Gordejewa und Sergei Grinkow |
1989: Larissa Selesnjowa und Oleg Makarow |
1990: Jekaterina Gordejewa und Sergei Grinkow |
1991–92: Natalja Mischkutjonok und Artur Dmitrijew |
1993: Marina Jelzowa und Andrej Buschkow |
1994: Jekaterina Gordejewa und Sergei Grinkow |
1995: Mandy Wötzel und Ingo Steuer |
1996: Oksana Kasakowa und Artur Dmitrijew |
1997: Marina Jelzowa und Andrei Buschkow |
1998: Jelena Bereschnaja und Anton Sicharulidse |
1999–2000: Maria Petrowa und Alexei Tichonow |
2001: Jelena Bereschnaja und Anton Sicharulidse |
2002–06: Tatjana Totmjanina und Maxim Marinin |
2007–09: Aljona Savchenko‎ und Robin Szolkowy |
2010: Juko Kawaguti und Alexander Smirnow |
2011: Aljona Savchenko‎ und Robin Szolkowy |
2012–14: Tatjana Wolossoschar und Maxim Trankow |
2015: Juko Kawaguti und Alexander Smirnow |
2016: Tatjana Wolossoschar und Maxim Trankow |
2017: Jewgenija Tarassowa und Wladimir Morosow

1907: Anna Hübler und Heinrich Burger |
1909: Anna Hübler und Heinrich Burger |
1911: Alice Rolle und Bruno Grauel |
1912: Hedwig Winzer und Hugo Winzer |
1913: Schnell und Georg Velisch |
1914: Else Lischka und Oscar Hoppe |
1920: Margarete Klebe und Paul Metzner |
1922–23: Grete Weise und Georg Velisch |
1924: Else Flebbe und Rudolf Eilers |
1925: Milly Förster und Hellmuth Jüngling |
1926: Ilse Kishauer und Herbert Haertel |
1927–31: Ilse Kishauer und Ernst Gaste |
1932–33: Wally Hempel und Otto Weiß |
1934–36: Maxi Herber und Ernst Baier |
1937: Eva Prawitz und Otto Weiß |
1938–41: Maxi Herber und Ernst Baier |
1942–43: Gerda Strauch und Günther Noack |
1944: Hertha Ratzenhofer und Emil Ratzenhofer |
1947–52: Ria Baran und Paul Falk |
1953: Helga Krüger und Peter Voss |
1954: Inge Minor und Hermann Braun |
1955–57: Marika Kilius und Franz Ningel |
1958–59: Marika Kilius und Hans-Jürgen Bäumler |
1960–62: Margret Göbl und Franz Ningel |
1963–64: Marika Kilius und Hans-Jürgen Bäumler |
1965–66: Sonja Pfersdorf und Günther Matzdorf |
1967–68: Margot Glockshuber und Wolfgang Danne |
1969: Gudrun Hauss und Walter Häfner |
1970: Brunhilde Baßler und Eberhard Rausch |
1971–73: Almut Lehmann und Herbert Wiesinger |
1974–76: Corinna Halke und Eberhard Rausch |
1977–78: Susanne Scheibe und Andreas Nischwitz |
1979–81: Christina Riegel und Andreas Nischwitz |
1982: Bettina Hage und Stefan Zins |
1983–84: Claudia Massari und Leonardo Azzola |
1985: Claudia Massari und Daniele Caprano |
1986: Kerstin Kiminus und Stefan Pfrengle |
1987: Sonja Adalbert und Daniele Caprano |
1988: Brigitte Groh und Holger Maletz |
1989–90: Anuschka Gläser und Stefan Pfrengle |
1991: Mandy Wötzel und Axel Rauschenbach |
1992: Peggy Schwarz und Alexander König |
1993: Mandy Wötzel und Ingo Steuer |
1994: Anuschka Gläser und Axel Rauschenbach |
1995–97: Mandy Wötzel und Ingo Steuer |
1998–2000: Peggy Schwarz und Mirko Müller |
2001: Claudia Rauschenbach und Robin Szolkowy |
2002: Sarah Jentgens und Mirko Müller |
2003: Eva-Maria Fitze und Rico Rex |
2004–09: Aljona Savchenko und Robin Szolkowy |
2010: Maylin Hausch und Daniel Wende |
2011: Aljona Savchenko und Robin Szolkowy |
2012: Maylin Hausch und Daniel Wende |
2013: Annabelle Prölß und Ruben Blommaert |
2014: Aljona Savchenko und Robin Szolkowy |
2015: Mari-Doris Vartmann und Aaron Van Cleave |
2016: Aljona Savchenko und Bruno Massot |
2017: Mari-Doris Vartmann und Ruben Blommaert |
2018: Aljona Savchenko und Bruno Massot
