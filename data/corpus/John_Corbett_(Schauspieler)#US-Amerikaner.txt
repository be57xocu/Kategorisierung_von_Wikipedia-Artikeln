John Corbett (* 9. Mai 1961 als John Joseph Corbett Junior in Wheeling, West Virginia) ist ein US-amerikanischer Schauspieler.

Seine erste größere Rolle hatte Corbett von 1990 bis 1995 als Chris Stevens in der Fernsehserie Ausgerechnet Alaska. 1997 übernahm er die Hauptrolle in der von Roland Emmerich produzierten Serie The Visitor. Danach wirkte er in der Serie Sex and the City mit und übernahm unter anderem Rollen in Filmen wie Weil es Dich gibt, Raise Your Voice – Lebe deinen Traum und Liebe auf Umwegen. Sein wohl bekanntester Film ist My Big Fat Greek Wedding – Hochzeit auf griechisch (2002). Er spielte auch in der Fortsetzung My Big Fat Greek Wedding 2 (2016) mit. Sein Schaffen umfasst rund 60 Film- und Fernsehproduktionen.

John Corbett ist seit 2002 mit der Schauspielerin Bo Derek liiert.
