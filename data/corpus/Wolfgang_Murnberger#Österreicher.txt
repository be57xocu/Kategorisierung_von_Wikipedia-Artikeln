Wolfgang Murnberger (* 13. November 1960 in Wiener Neustadt) ist ein österreichischer Regisseur und Drehbuchautor.

Murnberger maturierte 1980 am Bundesgymnasium und Bundesrealgymnasium Mattersburg. Er studierte Regie, Drehbuch und Schnitt an der Filmakademie Wien und stellte ab 1984 (Abschminken) einige Kurzfilme fertig. Sein autobiografisch inspirierter Abschlussfilm, der Spielfilm Himmel oder Hölle (1991), wurde an den Österreichischen Filmtagen ausgezeichnet und sorgte auch bei mehreren internationalen Festivals für Aufmerksamkeit und Preise.

Murnbergers zweiter Film Ich gelobe (1994), der erste nach seinem Abschluss an der Filmakademie, wies ebenfalls autobiografische Züge auf. Der Film, zu dem er selbst das Drehbuch verfasste, wurde sein erster großer nationaler Erfolg. Der Film, der von einem österreichischen Grundwehrdiener mit Identitätskrise handelt, erreichte in Österreich 52.000 Kinobesucher und wurde an der Viennale für die beste Regie mit dem Wiener Filmpreis ausgezeichnet. Er war 1995 Österreichs Beitrag für den Auslands-Oscar, wurde aber nicht nominiert.

Es folgten der Fernsehspielfilm Auf Teufel komm raus (1995) und der Fernsehfilm, Quintett komplett (1998). 2000 erschien Murnbergers bis dahin erfolgreichste Produktion, die Detektiv-Satire Komm, süßer Tod nach einer Romanvorlage von Wolf Haas mit Josef Hader in der Hauptrolle. Mit 230.000 Kinobesuchern in Österreich war er der am sechsthäufigsten besuchte Film seit Beginn der Besucherzählung 1981.[1] Auch in Deutschland konnte der Film 122.000 Kinobesucher anlocken.[2] Die Fortsetzung Silentium erschien vier Jahre später und war noch erfolgreicher. Zwar erreichte der Film in Österreich mit 205.000 etwas weniger Kinobesucher als der Vorgänger, doch erschien der Film auch in der Schweiz (12.000 Kinobesucher) und erreichte in Deutschland 171.000 Besucher.[3] Auch auf Filmfestivals erhielt die Fortsetzung mehr Beachtung, etwa mit dem zweiten Rang beim Publikumspreis der Berlinale 2005.

Von 2002 bis 2005 erreichte Murnberger mit der Fernseh-Trilogie Brüder, in der beliebte Kabarettisten mitwirkten, ebenfalls großen Publikumszuspruch. Neben Kino- und Fernsehfilmen inszenierte Murnberger auch einzelne Folgen von Fernsehserien, bisher Vier Frauen und ein Todesfall (2005–2007) sowie drei Tatort-Folgen (1997, 2007, 2011).

2009 kam der dritte Teil der Simon-Brenner-Trilogie in die Kinos. Mit mehr als 278.000 Kinobesuchern allein in Österreich wurde Der Knochenmann zu Murnbergers bislang größtem Kinoerfolg.

Wolfgang Murnberger gründete 2009 zusammen mit anderen österreichischen Filmschaffenden die Akademie des Österreichischen Films, er gehört dem Vorstand an.

Seit dem Wintersemester 2013 ist Wolfgang Murnberger Professor im Fach Regie an der Filmakademie Wien.[4]