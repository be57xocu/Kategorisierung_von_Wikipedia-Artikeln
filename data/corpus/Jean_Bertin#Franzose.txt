Jean Bertin (* 5. September 1917 in Druyes-les-Belles-Fontaines im Département Yonne; † 21. Dezember 1975 in Neuilly-sur-Seine) war ein französischer Ingenieur und Aeronautiker. Sein Name ist eng verbunden mit der Entwicklung des Aérotrain.

Er studierte an der École polytechnique (Abschlussjahrgang 1938) und an der École supérieure de l'aéronautique (SUPAERO). Seit 1944 arbeitete er für die staatliche französische Gesellschaft zur Entwicklung von Flugmotoren (Société nationale d'études et de construction de moteurs d'aviation, SNECMA) ein.

Im Jahre 1955 gründete er die Firma Bertin & Cie, deren bekannteste Aktivität die Entwicklung des Aérotrain ist. Bertin starb knapp eineinhalb Jahre, nachdem die französische Regierung den Vertrag über eine Aérotrain-Linie zwischen Cergy und La Défense gekündigt hatte.
