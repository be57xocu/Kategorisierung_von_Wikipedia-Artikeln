Georg Moser (* 10. Juni 1923 in Leutkirch im Allgäu; † 9. Mai 1988 in Stuttgart) war Bischof von Rottenburg-Stuttgart.

Georg Moser wurde geboren als achtes von elf Kindern des Schmiedemeisters Alois Moser und seiner Frau Maria, geb. Miller. Nach dem Studium der Katholischen Theologie an der Eberhard-Karls-Universität Tübingen und dem Wilhelmsstift, das er von 1942 bis 1947 absolvierte, empfing er am 19. März 1947 das Sakrament der Priesterweihe. Anschließend war er bis 1950 Vikar in Ludwigsburg und Stuttgart und von 1950 bis 1953 Präfekt des bischöflichen Internates Josefinum in Ehingen (Donau), ehe er Studentenpfarrer in Tübingen wurde. Dort wurde er 1962 mit einer Dissertation über „Die Eschatologie in der Katechetischen Unterweisung“ zum Doktor der Theologie promoviert. 1961 übernahm er die Leitung der katholischen Akademie Stuttgart-Hohenheim.

Papst Paul VI. ernannte ihn am 12. Oktober 1970 zum Titularbischof von Thiges und zum Weihbischof im Bistum Rottenburg. Die Bischofsweihe empfing er am 14. November desselben Jahres zusammen mit Anton Herre durch Bischof Carl Joseph Leiprecht in Stuttgart. Mitkonsekratoren waren die Weihbischofe Wilhelm Sedlmeier aus Rottenburg und Karl Gnädinger aus Freiburg. Am 25. Februar 1975 ernannte ihn Paul VI. zum Nachfolger Leiprechts als Bischof der Diözese Rottenburg, die 1978 in Rottenburg-Stuttgart umbenannt wurde.

Als zuständiger Bischof versuchte er, die 1968 entstandenen Differenzen zwischen dem an der Universität Tübingen lehrenden Theologieprofessor Hans Küng und dem Vatikan beizulegen, was aber nur bedingt gelang. Entgegen den Darstellungen in Küngs neuestem Werk (zweiter Band der Erinnerungen) ergab eine Auswertung der persönlichen Dokumente Mosers im Jahre 2007, dass der damalige Rottenburger Bischof bis an die Grenzen des Möglichen gekämpft hat, um die Maßregelung Küngs zu verhindern.[1]
Moser starb an den Folgen eines jahrzehntelangen Nierenleidens im Alter von 64 Jahren in Stuttgart. Seine letzte Ruhestätte fand er in der Bischofsgruft der Friedhofskirche Sülchen.

Georg Moser ist der Verfasser mehrerer Bücher, die teilweise hohe Auflagen erzielten und in andere Sprachen übersetzt wurden. Unter anderem sind das:

Weihbischöfe in Rottenburg (1915 bis 1978)Joannes Baptista Sproll |
Franz Joseph Fischer |
Carl Joseph Leiprecht |
Wilhelm Sedlmeier |
Anton Herre |
Georg Moser |
Franz Josef Kuhnle 

Weihbischöfe in Rottenburg-Stuttgart (seit 1978)Franz Josef Kuhnle |
Bernhard Rieger |
Johannes Kreidler |
Thomas Maria Renz |
Matthäus Karrer

Bischöfe von Rottenburg (1821 bis 1978)Johann Baptist von Keller |
Josef von Lipp |
Karl Joseph von Hefele |
Wilhelm von Reiser |
Franz Xaver von Linsenmann (Elekt) |
Paul Wilhelm von Keppler |
Joannes Baptista Sproll |
Carl Joseph Leiprecht |
Georg Moser

Bischöfe von Rottenburg-Stuttgart (seit 1978)Georg Moser |
Walter Kasper |
Gebhard Fürst
