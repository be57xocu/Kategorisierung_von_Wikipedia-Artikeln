Daniel Julius Bernstein (* 29. Oktober 1971 in East Patchogue, Long Island, New York), auch bekannt als djb, ist deutsch-amerikanischer Mathematiker (Algorithmische Zahlentheorie), Kryptologe, Programmierer und Professor an der University of Illinois in Chicago.[1]
Bernstein studierte Mathematik an der New York University (Bachelor-Abschluss 1991) und promovierte 1995 bei Hendrik Lenstra an der University of California, Berkeley. Ab 1995 war er Research Assistant Professor an der University of Illinois at Chicago, ab 1998 Assistant Professor, 2001 Associate Professor und seit 2005 ist er Professor in der Fakultät für Mathematik, Statistik und Informatik, seit 2003 gleichzeitig als Adjunct Professor in der Fakultät für Informatik.
Er war Gastprofessor an der Technischen Universität Dänemarks in Lyngby (2006), an der University of Sydney (2004) und Gastwissenschaftler am Fields Institute in Toronto und am Mathematical Sciences Research Institute als Key Senior Scientist in algorithmischer Zahlentheorie (2000).

1996 erarbeitete Bernstein zusammen mit Eric Schenk SYN-Cookies, um Server vor durch SYN-Flood verursachten Denial of Service zu schützen.

2001 erregte er Aufmerksamkeit, als er Überlegungen veröffentlichte (Circuits for integer factorization – a proposal, 2001), wonach die damals verfügbare Computer-Hardware ausreichen könnte, um (der Stellenanzahl nach) dreimal größere Zahlen zu faktorisieren als bis dahin angenommen. Damals lag die Messlatte faktorisierbarer Zahlen bei Zahlen mit etwa 512 Bits; seinen Argumenten zufolge wären also Zahlen mit 1500 Bits angreifbar, im Gegensatz zu den allgemeinen Vorstellungen über sichere RSA-Schlüssel, die auf veröffentlichter Literatur beruhten – der Kenntnisstand der NSA und anderer Geheimdienste ist naturgemäß der Öffentlichkeit unbekannt. Bernsteins Arbeit wurde aber von anderen Mathematikern[2] kritisiert.

Im Mai 2005 veröffentlichte Bernstein einen Artikel[3] über eine unerwartet einfache Timing-Attacke auf den Advanced Encryption Standard (AES).

Bernstein ist Mitbegründer der PQCrypto, einer Konferenz zum Thema Post-Quanten-Kryptographie.

Bernstein schrieb eine Software-Bibliothek für die Schnelle Fourier-Transformation (FFT), DJBFFT. Er entwickelte mit A. O. L. Atkin und implementierte (in Form des Programms primegen) auch ein schnelles Primzahlsieb (Sieb von Atkin).[4]
Seit den 1990er Jahren entwickelte und implementierte er auch schnelle Algorithmen für das Zahlkörpersieb[5] und Kryptographie mit Elliptischen Kurven.

Er ist unter anderem Autor folgender Programme:

Der Autor und seine Software sind sehr umstritten, denn einerseits ist die von ihm veröffentlichte Software von hoher Qualität; auf der anderen Seite setzt er sich bewusst über existierende Standards hinweg (beispielsweise die Platzierung von Dateien im Verzeichnisbaum), seine Software wird von ihm nicht gewartet (er sieht sie als fehlerfrei an) und er nimmt keine Erweiterungsvorschläge an. Lange Zeit veröffentlichte er seine Software unter Lizenzen, die nicht als freie Software anerkannt waren, weswegen viele Linux-Distributionen sich weigerten, diese aufzunehmen. Allerdings hat er im November 2007 fast alle Software als gemeinfrei deklariert, womit dieses Problem nicht mehr besteht.

Bernstein hat für einige seiner Softwareprojekte Preise für Finder von Sicherheitslücken ausgeschrieben. Er zahlte im März 2009 1.000 US-Dollar an Matthew Dempsky für das Auffinden einer Sicherheitslücke in djbdns (siehe auch qmail#Sicherheit).[6]
Bernstein hat den Hash-Algorithmus CubeHash[7] als Vorschlag für SHA-3 entwickelt, dieser schaffte es allerdings nicht in die Runde der Finalisten. Weiterhin entwickelt er zurzeit die DNSSEC-Alternative DNSCurve, die das von ihm entwickelte Elliptische-Kurven-Kryptosystem Curve25519 verwendet. Ebenfalls aufbauend auf Curve25519 arbeitet er an CurveCP, einem zu TCP alternativen Transportprotokoll, das Vertraulichkeit und Authentizität garantiert.[8]
Bernstein hat das Benchmarking-Tool SUPERCOP[9] entwickelt, welches
eine Vielzahl kryptographischer Algorithmen unter realitätsnahen Bedingungen testet. Zusammen mit Tanja Lange
betreibt er die Website eBACS,[10] eine umfangreiche Sammlung von Benchmarkergebnissen kryptographischer Algorithmen.
Die eBACS-Ergebnisse zu Hashfunktionen sind laut NIST ein wichtiges
Kriterium für die Auswahl von SHA-3.[11]
Bernstein hat einen langen Kampf gegen die amerikanischen Exportbeschränkungen für Kryptographie hinter sich.[12] Er kritisierte auch mehrere bekannte Software-Patente (zum Beispiel von Whitfield Diffie und Martin Hellman über Public-Key-Kryptographie) aufgrund des US-Patentrechts (nach dem die Veröffentlichung der Patent-Gegenstände bei Beantragung des Patents nicht länger als ein Jahr zurückliegen darf).
