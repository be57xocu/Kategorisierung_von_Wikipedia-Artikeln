Jens Rehn (* 18. September 1918 als Otto Jens Luther in Flensburg; † 3. Januar 1983 in Berlin) war ein deutscher Schriftsteller.

Jens Rehn wuchs in Berlin als Sohn des Kammervirtuosen Paul Luther auf. Nach dem Besuch des Gymnasiums und Konservatoriums schlug er ab 1937 die Laufbahn eines Offiziers bei der Kriegsmarine ein. Im Zweiten Weltkrieg war er vom 4. Juni 1943 bis 15. Juli 1943 Kommandeur des U-Boots U-135. 1943 geriet er für vier Jahre in Gefangenschaft, die er in Afrika, Kanada und England verbrachte. 

Von 1947 bis 1949 war er Freiberufler, von 1950 bis 1981 Redakteur der Literaturabteilung des RIAS in Berlin. In den Jahren 1954 bis 1958 absolvierte er ein Studium der Philosophie, Anglistik und Musikwissenschaft an der Freien Universität Berlin. Neben seiner Tätigkeit als Redakteur komponierte Rehn, schrieb eine Reihe von Hörspielen und unternahm ausgedehnte Reisen nach Ostasien, Indien und in die USA.

Sein Roman Nichts in Sicht, in dem er seine Kriegserlebnisse verarbeitete, wurde viel beachtet und von Marcel Reich-Ranicki als "Parabel von hoher Anschaulichkeit und Suggestivität" bezeichnet; der Kritiker schrieb: "Das Buch Nichts in Sicht sollten wir, dürfen wir nicht vergessen: Es ist beides in einem - ein zeitgeschichtliches und ein künstlerisches Dokument.[1]" In dem expressionistischen Science-Fiction-Roman Die Kinder des Saturn thematisierte Jens Rehn 1959 die Themen Atomkrieg und Strahlentod.

Jens Rehn, der Mitglied des PEN-Zentrums der Bundesrepublik Deutschland war, erhielt 1956 den Berliner Kunstpreis "Junge Generation" und 1979 ein Villa-Massimo-Stipendium.
