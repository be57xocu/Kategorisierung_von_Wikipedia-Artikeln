Ronald Kukulies (* 8. Dezember 1971 in Düsseldorf) ist ein deutscher Schauspieler.

Kukulies studierte an der Hochschule für Schauspielkunst „Ernst Busch“ Berlin. Von 1999 bis 2006 spielte er an der Schaubühne am Lehniner Platz. Seit 2004 ist er in deutschen Fernsehproduktionen zu sehen. Von 2006 bis 2013 gehörte er zum Ensemble des Maxim-Gorki-Theaters in Berlin.
