Donghua Li (* 10. Dezember 1967 in Chengdu) ist ein Schweizer Turner chinesischer Herkunft. Nachdem er 1989 mit seiner damaligen Schweizer Ehefrau in die Schweiz gezogen war, nahm er vorläufig nur an nationalen Meisterschaften teil. Fünf Jahre später erhielt er die schweizerische Staatsangehörigkeit und war somit für die Schweiz auch international startberechtigt.

Für die Schweiz nahm er 1996 bei den Olympischen Spielen in Atlanta teil und erreichte in seiner Paradedisziplin am Pauschenpferd olympisches Gold. Im gleichen Jahr trat er vom Spitzensport zurück.[1]
Donghua Li hat eine Tochter (* 1996) aus erster Ehe und einen Sohn (* 2012) mit seiner neuen Partnerin.[2]
1974:

1978:

1983:

1984:

1986:

1987:

1988:

1989:

1990:

1991:

1994:

1995:

1996:
Das erfolgreichste Jahr: (alle internationalen Erfolge am Pferd)

1896: Louis Zutter |
1904: Anton Heida |
1924: Josef Wilhelm |
1928: Hermann Hänggi |
1932: István Pelle |
1936: Konrad Frey |
1948: Paavo Aaltonen, Veikko Huhtanen und Heikki Savolainen |
1952: Wiktor Tschukarin |
1956: Boris Schachlin |
1960: Boris Schachlin und Eugen Ekman |
1964: Miroslav Cerar |
1968: Miroslav Cerar |
1972: Wiktor Klimenko |
1976: Zoltán Magyar |
1980: Zoltán Magyar |
1984: Li Ning und Peter Vidmar |
1988: Ljubomir Geraskow, Zsolt Borkai und Dmitri Bilosertschew |
1992: Wital Schtscherba und Pae Gil-su |
1996: Donghua Li |
2000: Marius Urzică |
2004: Teng Haibin |
2008: Xiao Qin |
2012: Krisztián Berki |
2016: Max Whitlock

Liste der Olympiasieger im Turnen
