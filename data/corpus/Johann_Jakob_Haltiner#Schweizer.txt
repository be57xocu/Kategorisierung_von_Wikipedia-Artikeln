Johann Jakob Haltiner (* 1728; † 26. Juni 1800[1]) war ein Schweizer Baumeister aus Altstätten.

Haltiner erlernte sein Handwerk als erster Geselle von Hans Ulrich Grubenmann in Teufen. Er heiratet Barbara Zürcher die Tochter einer Schwester von Grubenmann. Ein Sohn aus dieser Ehe ist der Baumeister Hans Ulrich Haltiner[2] (1755–1814).

Zu seinen Werken zählen die Kirchen von Opfikon, die Reformierte Kirche Kloten, Katholische Kirche in Altstätten, Hemberg, Flawil, die Reformierte Kirche Horgen, die Reformierte Kirche Bauma, der Profanbau des Gasthauses Rössli in Mogelsberg, sowie 1772 die Reburg, Altstätten. 

Die Idee dem Kirchenbauer in Horgen ein Denkmal zu setzen wurde nicht umgesetzt, trotzdem resultierte daraus das einzige bekannte Bildnis[3] Haltiners.
