Serge Klarsfeld (* 17. September 1935 in Bukarest) ist ein französischer Rechtsanwalt und Historiker.

Serge Klarsfeld wurde am 17. September 1935 als Sohn jüdischer Eltern (Raissa und Arno Klarsfeld) in Bukarest (Rumänien) geboren. Die Eltern hatten sich 1929 in Paris kennengelernt. Nach dem deutschen Einmarsch im Juni 1940 flohen sie in die unbesetzte Zone Frankreichs (Zone libre). In Nizza wurde Serge Klarsfeld als Kind fast Opfer einer der großen Razzien des Kommandos von Alois Brunner (30. September 1943). Die Familie hatte sich hinter der doppelten Wand eines Wandschranks versteckt, doch der Vater, der in der Wohnung geblieben war, damit die Gestapo keinen Verdacht schöpfte, wurde festgenommen, deportiert und in Auschwitz ermordet.

Nach dem Krieg studierte Klarsfeld Geschichte an der Sorbonne und Politik am Institut d’études politiques de Paris (IEP). Er schloss 1960 mit einem Diplom in Politikwissenschaft ab. Klarsfeld wurde promoviert (französisch Docteur ès lettres). Ab 1970 studierte Klarsfeld Rechtswissenschaft und wurde Rechtsanwalt in Paris.

Seine spätere Frau Beate Künzel lernte er 1960 kennen. Nachdem Klarsfeld 1963 eine Stelle bei der  ORTF angetreten hatte, heirateten sie. Kurze Zeit  später kündigte Klarsfeld beim Rundfunk und ging zur Getreidehandelsfirma „Continental Grain“. Dort wurde ihm 1970 gekündigt. Beate erregte weltweit Aufsehen, als sie am 7. November 1968 öffentlich den damaligen Bundeskanzler Kurt Georg Kiesinger ohrfeigte, dem sie seine Nazivergangenheit vorwarf.[1]
Serge Klarsfeld gründete 1979 zusammen mit seiner Frau die Vereinigung „Association des fils et filles des déportés juifs de France“.

Klarsfeld ist einer der profiliertesten Rechercheure, die nach 1945 untergetauchte und unentdeckte Täter des Holocaust vorwiegend in Frankreich und Deutschland aufspürten und der Justiz zuführten. Es ist unter anderem ihm und seiner Frau zu verdanken, dass NS-Verbrecher und Kollaborateure wie Klaus Barbie, René Bousquet, Jean Leguay, Maurice Papon, Paul Touvier, Kurt Lischka, Ernst Heinrichsohn und Herbert M. Hagen vor Gericht gestellt oder zumindest, wie etwa Ernst Achenbach, demaskiert wurden. Es gelang Klarsfeld auch, Alois Brunner, seinen einstmaligen Verfolger, ausfindig zu machen, doch Syrien, wo Brunner für den Geheimdienst gearbeitet haben soll, verweigerte die Auslieferung.

Klarsfeld ist Autor eines Standardwerks zur Judenverfolgung in Frankreich, „Vichy – Auschwitz“ (auch auf Deutsch unter dem gleichen Titel erschienen). Sein Sohn Arno Klarsfeld ist ein bekannter Rechtsanwalt und Berater von Nicolas Sarkozy, französischer Staatspräsident von Mai 2007 bis Mai 2012. 

Die Jagd auf Klaus Barbie wurde 2008 unter dem Titel Die Hetzjagd verfilmt.

Zusammen mit seiner Frau Beate Klarsfeld erhielt er im Mai 2015 das Bundesverdienstkreuz.[2] Im Oktober 2015 wurden beide von der UNESCO zu UNESCO-Sonderbotschaftern für Bildung über den Holocaust und die Verhinderung von Völkermorden ernannt.[3]