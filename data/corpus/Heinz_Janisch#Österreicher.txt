Heinz Janisch (* 1960 in Güssing/Burgenland) ist ein österreichischer Kinderbuchautor und Ö1-Redakteur.

Janisch studierte Germanistik und Publizistik in Wien. Er ist Journalist beim Rundfunk (Redakteur der ORF-Reihe „Menschenbilder“) und hat zahlreiche Erzählungen, Gedichte und Bilderbücher veröffentlicht. Janischs Werke zeichnen sich durch einen lyrischen Ton aus und wurden in viele Sprachen übersetzt.
Heute lebt Heinz Janisch in Wien und im Burgenland.
