Johann Friedrich Endersch (* 25. Oktober 1705 in Dörnfeld an der Heide, Thüringen; † 28. März 1769) war ein kaiserlicher Kartograf.

Er lebte in Elbing und fertigte im Jahre 1755 eine Landkarte von Ermland (lateinisch Warmia) mit Widmung für Fürstbischof Adam Stanislaus Grabowski an. Der Titel der Landkarte ist Tabula Geographica Episcopatu Warmiensem In Prussia Exhibens. Auf dieser Karte sind alle Städte und Orte des preußischen Fürstbistums Ermland aufgeführt. Das östliche Preußen ist als Borussiae Orientalis bezeichnet. Die Karte wurde für den Kaiser des Heiligen Römischen Reichs hergestellt.

Endersch fertigte ebenfalls einen Kupferstich von einer Galiotte oder Segelschiff, welches in seiner Heimatstadt Elbing im Jahre 1738 gebaut wurde.
Das Schiff wurde D' Stadt Elbing genannt.
