Lyman Frank Baum (* 15. Mai 1856 in Chittenango, New York; † 6. Mai 1919 in Los Angeles) war ein US-amerikanischer Schriftsteller. Er schrieb den Kinderbuch-Klassiker Der Zauberer von Oz (The Wonderful Wizard of Oz) sowie 13 Fortsetzungsromane, die in der von ihm erdachten Zauberwelt Oz spielen.

Baums deutschstämmiger Vater, Benjamin Ward Baum, war ein erfolgreicher Geschäftsmann und hatte mit Ölfeldern in Pennsylvania ein großes Vermögen gemacht, und so verbrachte Baum seine Kindheit in dem luxuriösen Anwesen Rose Lawn. Zu seinem 15. Geburtstag schenkte ihm sein Vater eine Druckerpresse, die Baums Interesse am Schreiben weckte. 1882 heiratete er Maud Gage, die Tochter einer führenden Frauenrechtlerin, und zog mit ihr nach South Dakota.

Baum versuchte sich in zahlreichen Geschäftsfeldern. Er arbeitete im Theater, für Zeitungen und Zeitschriften, stellte das patentierte Schmieröl Baum’s Castorine her, leitete das Warenhaus Baum’s Bazaar und war zeitweilig auch Hühnerfarmer.
Sein erstes Buch, das er 1886 veröffentlichte, handelt von der Hühnerzucht. Baum war ebenso Herausgeber einer eigenen Wochenzeitschrift, die jedoch wie die meisten seiner Unternehmen nicht besonders erfolgreich war.

Baum war auch publizistisch tätig; erhalten sind zwei Zeitungskommentare der Jahre 1890 und 1891, in denen er forderte, den Konflikt mit den amerikanischen Ureinwohnern durch deren „totale Auslöschung“ zu beenden. Hierbei ging er von der Annahme aus, dass den Ureinwohnern über Jahrhunderte Unrecht getan wurde und diese somit immer ein Unruheherd bleiben müssten. Diesem Problem könne man nur durch eine endgültige Lösung Herr werden.[1]
1891 zog die Familie nach Chicago und Baum suchte nach einer neuen Möglichkeit, Geld zu verdienen. Da er sich stets mit Freude Gute-Nacht-Geschichten für seine eigenen Kinder ausgedacht hatte und diese ihnen am Bett erzählte, entschloss er sich, Geschichten für andere Kinder zu schreiben. In seinen ersten beiden Büchern erzählte er vorwiegend traditionelle Geschichten nach, wobei er in einer dieser Geschichten ein junges Mädchen namens „Dorothy“ einführte. Dies führte ihn zur Schaffung des Landes der Wunder und Freuden Oz. Im Jahr 1900 erschien sein Buch The Wonderful Wizard Of Oz, das über Nacht zu einem riesigen Erfolg wurde. Binnen zweier Jahre wurde es als Bühnen-Musical adaptiert, für das Baum selbst das Drehbuch schrieb. Baum verfasste noch 13 weitere Romane um das wunderbare Land, wobei er mehrfach erklärte, die Serie jetzt abgeschlossen zu haben und keine weiteren Oz-Fortsetzungen mehr zu schreiben. Unterdessen wandte er sich zahlreichen anderen Kindergeschichten zu, unter anderem unter dem Pseudonym Edith Van Dyne. Aufgrund der hohen Nachfrage nach weiteren Oz-Romanen und wegen der Briefe vieler Kinder setzte er die Serie jedoch immer wieder fort.

Am 4. September 1892 trat er der Theosophischen Gesellschaft bei und folgte 1895 nach der Spaltung derselben infolge der Judge Case der Theosophischen Gesellschaft in Amerika. Mehrere seiner Aufsätze und Bücher behandelten theosophische Themen.

Lyman Frank Baum starb 1919 in Hollywood. Er hat in seinem Leben über 60 Bücher veröffentlicht.

Die Oz-Romane

Weitere Oz-bezogene Werke Baums
