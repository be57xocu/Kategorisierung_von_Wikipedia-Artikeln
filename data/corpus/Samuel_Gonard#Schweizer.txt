Samuel Gonard (* 8. Juni 1896 in Neuenburg; † 3. Mai 1975 in Corseaux) war ein Schweizer Jurist und ranghoher Berufsoffizier der Schweizer Armee. Im Dezember 1961 wurde er in das Internationale Komitee vom Roten Kreuz (IKRK) aufgenommen und schied kurz darauf aus dem Militärdienst aus. Drei Jahre später wurde er zum Präsidenten des Komitees gewählt. Diese Funktion hatte er bis zu seinem altersbedingten Rücktritt im Januar 1969 inne.

Samuel Gonard besuchte das Gymnasium in Neuenburg und studierte Rechtswissenschaften in Neuchâtel und Lausanne. Noch während seines Studiums trat er in die Schweizer Armee ein und wurde am 31. Dezember 1919 zum Leutnant ernannt. Nachdem er sein Studium im Juni 1921 mit dem Lizentiat abgeschlossen hatte, wechselte er 1923 von der Artillerie zum Instruktionsdienst der Armee. In den folgenden Jahren besuchte er mehrfach die École Supérieure de Guerre in Paris. 1927 folgte seine Beförderung zum Hauptmann, vier Jahre später wurde er Mitglied des Generalstabs. Im Jahr 1933 wurde er zum Major ernannt. 1937 wechselte er nach Bern in die Generalstabsabteilung. 

Nach seiner Beförderung zum Oberstleutnant erhielt er 1939 von General Henri Guisan den Auftrag, dessen persönlichen Stab einzurichten und zu leiten. Ein Jahr später hatte er als Chef der Operationsabteilung des Generalstabs wesentlichen Anteil an der Planung und Durchführung des Réduits national zur Verteidigung der Schweiz nach der Besetzung Frankreichs im Zweiten Weltkrieg. 1943 wurde er zum Oberst-Brigadier befördert und Unterstabschef in der Armeeleitung. Er gehörte damit zum engsten Mitarbeiterkreis von General Guisan. Nach seiner Beförderung zum Oberstkorpskommandanten und der Übernahme des Kommandos verschiedener Armeeeinheiten, zuletzt des 1. Armeekorps, schied er am 21. Dezember 1961 aus dem Militärdienst aus.

Parallel zu seinem Wirken in der Armee unterrichtete er von 1946 bis 1952 als Dozent für Kriegsgeschichte und Taktik an der Eidgenössischen Technischen Hochschule ETH in Zürich. Nach seinem Ausscheiden aus dem Militärdienst war er darüber hinaus auch Honorarprofessor am Genfer Institut für Internationale Studien.

Gonard war zweimal verheiratet, in erster Ehe mit Hélène Dubois Gonard geb. Louis, in zweiter Ehe mit Manon Bosshard Gonard geb. Bosshard, Tochter des schweizerischen Malers Rodolphe Théophile Bosshard (1889–1960).

Bereits vor seinem Ausscheiden aus der Armee war Gonard 1961 vom Internationalen Komitee vom Roten Kreuz als Mitglied kooptiert worden. 1962 wurde er Vizepräsident und unternahm in dieser Funktion im gleichen Jahr eine längere Reise nach Afrika. Im September 1964 wurde er einstimmig zum Präsidenten des IKRK gewählt und übernahm damit die Nachfolge von Léopold Boissier, der auf eigenen Wunsch zurückgetreten war. Während seiner Amtszeit kam es zu verstärkten Aktivitäten des IKRK im Vietnam-Krieg und im Bürgerkrieg in Nigeria. Im Januar 1969 trat Gonard aus Altersgründen als Präsident zurück.

Guillaume-Henri Dufour (1863–1864) |
Gustave Moynier (1864–1910) |
Gustave Ador (1910–1928) |
Max Huber (1928–1944) |
Carl Jacob Burckhardt (1945–1948) |
Paul Ruegger (1948–1955) |
Léopold Boissier (1955–1964) |
Samuel Gonard (1964–1969) |
Marcel Naville (1969–1973) |
Eric Martin (1973–1976) |
Alexandre Hay (1976–1987) |
Cornelio Sommaruga (1987–1999) |
Jakob Kellenberger (2000–2012) |
Peter Maurer (seit 2012)
