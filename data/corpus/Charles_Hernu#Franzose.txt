Charles Hernu (* 3. Juli 1923 in Quimper, Département Finistère; † 17. Januar 1990 in Villeurbanne) war ein französischer Politiker der Sozialisten.

Hernu begann seine Karriere im nationalen Außenhandelszentrum. Später wurde er Journalist.

1953 gründete er den Jakobinerclub, einen dem Parti radical socialiste von Pierre Mendès France nahestehenden politischen Club.

Am 2. Januar 1956 wurde er im 6. Sektor Seine (Aubervilliers, Saint-Denis, Montreuil, Vincennes) als Abgeordneter der Front républicain gewählt, dem Listenzusammenschluss von SFIO und Radikalen. Im außenpolitischen Ausschuss der Nationalversammlung wurde er Sekretär. Nach der Rückkehr von General Charles de Gaulle an die Macht verlor er sein parlamentarisches Mandat.

1962 nahm er an der Gründung des Parti socialiste unifié (PSU) teil, schloss sich dann 1971 dem Parti Socialiste (PS) von François Mitterrand an und wurde in den 1970er Jahren Verteidigungsexperte seiner Partei. Im April 1974 schuf er le Coran, eine Verbindung von Reserveoffizieren für eine neue Armee, die schnell mit der Verteidigungskommission des PS fusioniert wurde. Er wurde verantwortlich für die Verteidigungs- und Nuklearpolitik seiner Partei.

Er galt als überzeugter Anhänger der atomaren Abschreckung durch die force de frappe und bezeichnete die sowjetische Mittelstreckenrakete SS-20 als Bedrohung. Hernu trat für den NATO-Doppelbeschluss und gegen die Einbeziehung der französischen Atomwaffen in die START-Verhandlungen ein. 
SDI (das 1983 verkündete US-Forschungsprogramm für eine weltraumgestützte Verteidigung) lehnte Hernu ab.

Im März 1977 wurde Hernu zum Bürgermeister von Villeurbanne gewählt, wo er 1978 auch Abgeordneter wurde.

Im Mai 1981 wurde er nach dem Sieg von François Mitterrand bei den Präsidentschaftswahlen zum Verteidigungsminister ernannt. Diese Funktion behielt er in den drei Kabinetten Mauroy (Mai 1981 bis Juli 1984) und im Kabinett Laurent Fabius (Juli 1984 bis März 1986) bis zu seinem Rücktritt.

Am 10. Juli 1985 wurde das Greenpeace-Schiff Rainbow Warrior im Hafen von Auckland in Neuseeland durch Agenten des DGSE in die Luft gesprengt. Dieses Attentat kostete den portugiesischen Fotografen Fernando Pereira das Leben. Der Rainbow-Warrior-Skandal gipfelte im Rücktritt von Hernu am 20. September 1985. 
Die meisten Beobachter stimmten darin überein, dass er als Bauernopfer für höhere Autoritäten geopfert wurde, die sicher über die Operation informiert waren.

2005 publizierte die Tageszeitung Le Monde Auszüge eines Berichts aus dem Jahr 1986, der vom früheren Chef der DGSE, Admiral Pierre Lacoste geschrieben war. Der Zeitung zufolge behauptete der Admiral, dass die französischen Agenten, die die Bomben platziert hatten, dem Befehl von Mitterrand persönlich entsprochen hatten. Aus einem unbekannten Grunde verwendete Jacques Chirac, der die Verwicklung des Präsidenten kannte, dies nicht während des Präsidentschaftswahlkampfs 1988.

1996 veröffentlichte das Magazin L’Express einen Bericht, dem zufolge Hernu zehn Jahre lang ein Agent des sowjetischen Geheimdienstes KGB und seiner Satellitendienste gewesen sei.[1]
Hernu war Bürgermeister von Villeurbanne von 1977 bis 1990. Der Ort gab der Metrostation Charpennes - Charles Hernu seinen Namen, die die erste Station der Linien A und B in der Gemeinde Villeurbanne sind.

Charles de Gaulle (Nationale Verteidigung) und Pierre Guillaumat (Armeen) |
Pierre Guillaumat |
Pierre Messmer |
Michel Debré |
Robert Galley |
Jacques Soufflet |
Yvon Bourges |
Joël Le Theule |
Robert Galley |
Charles Hernu |
Paul Quilès |
André Giraud |
Jean-Pierre Chevènement |
Pierre Joxe |
François Léotard |
Charles Millon |
Alain Richard |
Michèle Alliot-Marie |
Hervé Morin |
Alain Juppé |
Gérard Longuet |
Jean-Yves Le Drian |
Sylvie Goulard
