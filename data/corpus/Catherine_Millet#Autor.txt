Catherine Millet (* 1. April 1948 in Bois-Colombes) ist eine Expertin für Moderne Kunst und Chefredakteurin der Kunstzeitschrift art press.

Aufgewachsen in Bois-Colombes, wurde sie ohne eine akademische Ausbildung eine anerkannte Expertin für Kunstkritik. Dabei orientierte sie sich an US-amerikanischer formalistischer Kunstkritik. 
Sie ist seit 1991 mit dem französischen Fotografen und Schriftsteller Jacques Henric verheiratet, den sie 1972 kennenlernte.[1] Das kinderlose Paar lebt im 12. Pariser Arrondissement.

Publiziert hat sie u.a. ein Buch über den Lady-Chatterley-Autor D. H. Lawrence.

Ihre Bücher sind nach eigener Aussage faktisch deskriptiv und basieren auf genauer Beobachtung.
2001 erschien ihr autobiografisches Buch La vie sexuelle de Catherine M. (2001); es war in vielen Ländern ein Skandalerfolg. Die deutsche Übersetzung (Das sexuelle Leben der Catherine M.) machte Millet auch im deutschsprachigen Raum bekannt. 

Millets Buch ist ein emotionslos präziser Bericht über ihr libertinäres Sexualleben, darunter Gruppensex mit mehreren, ihr meist unbekannten Partnern beiderlei Geschlechts. In ihrem liberalen Milieu brauchte sie es nicht verbergen; sie beschrieb es als angenehm.  Das Buch gibt Einblicke in die französische Swingerszene von etwa 1970 bis 2000. Es galt als vielgelesener Titel des Jahres 2001 und wurde von einigen als pornografisch eingeschätzt. 
.

In ihren autobiografischen Buch Eifersucht (Originalausgabe 2008) beschrieb sie ihre tiefe Krise, nachdem sie entdeckte, dass ihr Mann Jacques Henric eine Affäre hatte. 

In Traumhafte Kindheit schildert sie aus ihrer schwierigen Kindheit u.a. ihre Wahrnehmung des Leids ihrer Eltern und die Zerrissenheit ihrer Familie. 
Ihre Mutter hatte eine psychische Störung und beging schließlich Suizid.[2]  
Offen spricht sie von ihrer Einsamkeit und ihren Ängsten; ihre Familie sei ein ,,Glutofen der Hölle" gewesen. Sie habe auf dem Schulhof die unschönen Ereignisse, das alltägliche Elend der Eltern, umzudrehen und durch Witz interessant zu machen.[3]