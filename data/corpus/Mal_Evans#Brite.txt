Mal Evans (* 27. Mai 1935 in Großbritannien; † 5. Januar 1976 in Los Angeles, USA) war bekannt als Assistent und Roadmanager der Beatles.

Evans lernte George Harrison in den frühen 1960er Jahren während eines dreimonatigen Engagements im Cavern Club in Liverpool kennen. Er wurde zuerst Türsteher und danach als Roadmanager und Leibwächter der Beatles eingestellt. Er hatte diesen Job bis zur Auflösung der Band inne.

Er produzierte anfangs das Soloalbum Two Sides of the Moon von Keith Moon, war an Helter Skelter beteiligt und ist in der ursprünglichen Aufnahme von A Day in the Life zu hören. Daneben war er auch in den Beatles-Filmen Help!, Magical Mystery Tour und Let It Be zu sehen.

Nach dem Scheitern seiner Ehe geriet er in eine persönliche Krise. Am 5. Januar 1976 drohte er, offenbar unter Drogeneinfluss, mit Selbstmord. Eine Freundin rief die Polizei. Evans hatte eine Waffe in der Hand. Als er diese auf einen Polizisten richtete, wurde er erschossen. 

Mal Evans war verheiratet und hinterließ zwei Kinder.
