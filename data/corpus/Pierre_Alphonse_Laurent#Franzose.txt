Pierre Alphonse Laurent (* 18. Juli 1813 in Paris; † 2. September 1854 ebenda) war ein französischer Mathematiker.

Laurent war als Ingenieur in der französischen Armee am Ausbau des Hafens von Le Havre tätig. Seine einzige bekannte mathematische Arbeit wurde erst nach seinem Tode veröffentlicht, obwohl Augustin Louis Cauchy bereits 1843 über deren Inhalt in der französischen Akademie berichtete. Gegenstand dieser Arbeit waren Konvergenzuntersuchungen über die nach Laurent benannten Laurent-Reihen. Die in der Algebra untersuchten Laurent-Polynome sind wegen der offensichtlichen Analogie zu den Laurent-Reihen ebenfalls nach ihm benannt.

Pierre Alphonse Laurent ist nicht zu verwechseln mit weiteren französischen Mathematikern dieser Zeit, Hermann Laurent (1841–1908) und Pierre Laurent Wantzel (1814–1848).
