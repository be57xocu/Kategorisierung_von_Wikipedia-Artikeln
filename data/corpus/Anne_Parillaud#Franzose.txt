Anne Parillaud (* 6. Mai 1960 in Paris) ist eine französische Schauspielerin.

Ursprünglich wollte Anne Parillaud Anwältin werden. Ab Ende der 1970er-Jahre hatte sie jedoch mit freizügigen Auftritten in Liebeskomödien wie Das Strandhotel (1978) und Girls – Die kleinen Aufreisserinnen, dem deutschen Softsex-Film Patricia – Einmal Himmel und zurück (beide 1980) oder in Alain Delons Thriller Rette deine Haut, Killer (1981) erste Erfolge als Schauspielerin.

International Aufmerksamkeit erregte sie 1990 in der Rolle einer von der Staatsgewalt abgerichteten Auftragsmörderin in Luc Bessons Actionfilm Nikita. Für diese Leistung erhielt Anne Parillaud einen César als Beste Hauptdarstellerin.

Der Versuch, 1992 als ebenso mörderischer wie sympathischer Vampir in John Landis’ Bloody Marie – Eine Frau mit Biß auch im US-Kino erfolgreich Fuß zu fassen, gelang nicht. 1993 fand die epische Liebesgeschichte Flucht aus dem Eis lediglich bei der Kritik Aufmerksamkeit, jedoch kaum beim Publikum. 1998 spielte Anne Parillaud als Königin Anna die Mutter von Leonardo DiCaprio in der Hollywood-Verfilmung von Alexandre Dumas’ Der Mann in der eisernen Maske. Im selben Jahr erinnerte ihre Rolle in dem Psychothriller Phantome des Todes an „Nikita“, fand jedoch kaum Zuschauer.

Erfolgreich ist Anne Parillaud weiterhin im französischen Film mit Hauptrollen in Actionstreifen wie Gangsters (2002) und Deadlines (2004), für den sie beim Filmfestival von Paris als „Beste Hauptdarstellerin“ ausgezeichnet wurde. In dem „Film im Film“ Sex Is Comedy von 2002 war sie eine Regisseurin, die Probleme hat, Darsteller, die sich privat nicht ausstehen können, zu Liebesszenen vor der Kamera zu überreden.

Anne Parillaud, die in Interviews kaum über ihr Privatleben spricht, hat eine Tochter mit Luc Besson. Sie war vom 11. Mai 2005 bis 2010 mit dem französischen Komponisten Jean Michel Jarre verheiratet. Die Ehe wurde 2010 geschieden.[1]