Stanley Morison (* 6. Mai 1889 in Wanstead, England; † 11. Oktober 1967 in London) war ein britischer Typograf und Schrifthistoriker.

Von 1923 bis zu seinem Tode war er künstlerischer Berater der Monotype Corporation und verantwortlich für das außerordentliche Wachstum ihrer Schriftbibliothek. Von 1929 bis 1960 war er zusätzlich als künstlerischer Berater für die Tageszeitung The Times tätig. Diese Tätigkeit führte auch zur wohl bekanntesten von Morison entwickelten Schrift: der Times. Die Schrift “Times New Roman” erschien zum ersten Mal in der Ausgabe der “Times” vom  3. Oktober 1932. Nur 40 Jahre später hatten sich die Bedingungen des Zeitungsdrucks stark verändert. [1] Die Bleisatztechnik bzw. Linotype-Setzmaschine wurde schrittweise durch den Fotosatz verdrängt.

Eine weitere von Morisons Schriften ist die 1929 herausgebrachte Bembo, benannt nach dem Kardinal Pietro Bembo, dessen Abhandlung De Aetna 1495 in einer Schrift gedruckt wurde, die als Vorbild für die moderne Bembo diente. 

Von 1935 bis 1951 veröffentlichte er ein vierbändiges Werk über die Geschichte der The Times. Er war korrespondierendes Mitglied im Grolier Club.

Seine Bibliothek von rund 6000 Bänden kam 1968 in die Cambridge University Library.[2]