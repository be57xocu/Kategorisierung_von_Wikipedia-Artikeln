Thomas Gansch (* 31. Dezember 1975 in St. Pölten) ist ein österreichischer Trompeter.

Gansch wuchs in Melk als jüngstes Kind einer Musikerfamilie auf. Sein Vater, Johann Gansch, war Blasmusik-Kapellmeister und Komponist, der Bruder Hans Gansch war bis 2014 Professor für Trompete am Salzburger Mozarteum. Bereits im Alter von 15 Jahren begann er sein Studium an der Musikhochschule in Wien. In dieser Zeit bildete sich das Blasmusik-Ensemble Mnozil Brass, als dessen musikalischer Leiter und Moderator er international in Erscheinung tritt.

1999 wurde Gansch von Mathias Rüegg in das Vienna Art Orchestra (VAO) aufgenommen. Gemeinsam mit weiteren Mitgliedern des VAO gründete Gansch seine erste eigene Band Gansch&Roses für die er selbst Kompositionen und Arrangements schuf. 2001 und 2002 war Gansch&Roses immer wieder als Stageband des Wiener Jazzlokals Porgy & Bess auf der Bühne. Seit 2013 gastiert er in der Vorweihnachtszeit eine Woche im  Wiener Jazzland mit Spitzenmusikern seiner Wahl: u.a. mit Oliver Kent, Mario Gonzi, Florian Trübsbach, Leonhard Paul, Stephan Zimmermann, Matthieu Michel, Johannes Herrlich, Herwig Gradischnig, Roman Schwaller, Andreas Scherer, Paulo Cardoso, Alex Deutsch, Michael Hornek, Gary Smulyan u.v.a.

Neben seiner Mitwirkung in Jazz- und Blasmusik-Gruppen tritt Gansch auch als Sänger von Wienerliedern und Schlagern auf.
