Adolf Pichler (* 4. September 1819 in Erl bei Kufstein; † 15. November 1900 in Innsbruck) war ein österreichischer Schriftsteller und Naturwissenschaftler.

Sein Vater Josef Anton war ein subalterner Zollbeamter, der im Laufe seines Lebens wiederholt Dienstversetzungen über sich ergehen lassen musste und sich nur wenig um seinen Sohn kümmerte. Auch seine Mutter Josefa, die die Hauptlast der Erziehung zu tragen gehabt hätte und mit ihrem Mann von einer Dienststelle zur nächsten zog, vernachlässigte den Knaben sträflich, sodass dieser schon in jungen Jahren auf sich alleine gestellt war. Pichler hat diese schwere Zeit – nicht die letzte in seinem Leben – in seinen 1892 in Leipzig erschienenen Lebenserinnerungen Zu meiner Zeit, Schattenbilder aus der Vergangenheit eingehend und offenherzig beschrieben. Dass er trotz der von ihm geschilderten widrigen Umstände das Gymnasium in Innsbruck absolvieren konnte, verdankte er wohlgesinnten Verwandten, die ihn finanziell ein wenig unterstützten.

Nach dem erfolgreichen Abschluss der „Philosophie“, dem damaligen Vorbereitungskurs für die Universität, wandte sich Pichler 1840 dem Studium der Rechtswissenschaften zu, das ihn aber nicht zu fesseln vermochte. Anstatt sich auf Prüfungen vorzubereiten, widmete er sich lieber den Schriften Hegels und der „Jungdeutschen“, deren freisinnige Gedanken auf ihn einen tiefen Eindruck machten.

1842 begab sich Pichler nach Wien, um Medizin zu studieren. Neben seinem Studium fand er noch genügend Zeit, sich auch als Schriftsteller zu betätigen. Seine ersten literarischen Versuche fanden zwar noch wenig Beachtung, öffneten ihm aber den Zugang zu Wiener Literatenkreisen.

1845 sammelte Pichler Beiträge für eine jungtirolische Anthologie, die wegen der strengen Zensurbestimmungen des Metternich’schen Regimes erst 1846 unter dem Titel Frühlingslieder aus Tirol veröffentlicht werden konnte. Er selbst lieferte, ohne seinen Namen zu nennen, mehrere Epigramme, den Hauptteil der Sammlung bildeten Werke seines Dichterkollegen Hermann von Gilm.

Eine unglückliche Liebe zu einer Wiener Bürgerstochter hätte den Dichter fast aus der Bahn geworfen. Pichler folgte seiner Geliebten, der von den Eltern ein Ortswechsel anbefohlen worden war, bis nach Ungarn, wo er erkennen musste, dass alle seine Bemühungen erfolglos waren. Den Schmerz über diesen Verlust hat er in seinem Emma-Zyklus festgehalten:

Sie hetzten mich aus Deiner trauten Nähe;
Ich zählte fliehend nicht der Wanderung Stunden.
Ich habe eines nur: der Trennung Wehe,
doch nicht des Körpers Müdigkeit empfunden.

Trotz dieser schmerzhaften Erfahrung hat Pichler sein Medizinstudium mit dem Doktorat zeitgerecht abgeschlossen. Das Vorhaben, sich in Wien als Arzt zu betätigen, konnte er jedoch wegen der im März 1848 ausgebrochenen Unruhen nicht mehr in die Tat umsetzen. Als die Nachricht nach Wien drang, dass Tirol von den Italienern bedrängt sei, fasste Pichler den Entschluss, ein aus Tiroler Akademikern gebildetes Freiwilligencorps auszuheben und mit diesem zum Schutze seiner Heimat an die Südgrenze zu marschieren. Unter den 131 Freiwilligen, die mit ihm aufbrachen, befand sich auch ein Kampfgenosse Andreas Hofers, der greise Kapuzinerpater Joachim Haspinger. Am 27. April zogen die von Pichler geführten Studenten in Bozen ein, wo sie von der Bevölkerung mit großem Jubel empfangen wurden. Am nächsten Tag wurden die jungen Männer, die zum Zeichen ihrer deutschnationalen Gesinnung eine schwarz-rot-goldene Fahne mit sich führten, von Erzherzog Johann gemustert. Dabei entspann sich zwischen Adolf Pichler und dem Erzherzog beim Abschreiten der Formation folgendes Gespräch:

Pichler: „Wer hätte geahnt, dass je Tiroler Schützen unter diesen Farben in’s Feld ziehen?“ Darauf der Erzherzog: „Geahnt? – O wir Älteren waren davon überzeugt, dass dieser Tag noch einmal anbrechen werde; er ist gekommen, ja! Folgt dieser Fahne immer und überall, sie möge euch im Kampfe voran leuchten, verlasst sie nie!“[1]
36 Jahre später wurde Pichler vom Feldzeugmeister Graf Johann Carl Huyn, der damals Generalstabsoffizier der in Südtirol liegenden Division Graf Lichnowsky war, darüber aufgeklärt, was der volksverbundene Erzherzog vom Freiwilligen-Korps wirklich dachte: Als Huyn ihn gegen Ende des Feldzuges ersuchte, die Studentenkompanie aus Storo abzulösen, „weil es doch schade wäre, wenn diese Summe von Intelligenz, wie sie in dieser Kompanie vertreten ist, große Verluste erleiden würde“, antwortete jener: „Wenn von diesen Kerls gar keiner zurückkommt, umso besser!“[2]
Pichler und seine Männer ahnten von all dem nichts. Sie waren mit anderen Dingen beschäftigt:

„Das Leben zu Storo war ziemlich langweilig, die schlecht gesinnten Einwohner suchten es uns auch nicht zu versüßen; unter den Weibern fände man wahre Prachtstücke von Eumeniden, wie Aeschylos in Athen schwerlich bessere antraf. Es wurde daher aus Mangel an besserer Unterhaltung viel Wein, zu ungeheueren Ladungen Polenta und Schöpsbraten vertilgt.“[3]
Nur einmal, bei Ponte Tedesco, wurden die Akademiker in ein ernsteres Scharmützel verwickelt, bei dem das Korps mehrere Verletzte und sogar einen Toten zu beklagen hatte. Diesem hat Pichler in seinem Fra Serafico ein Denkmal gesetzt:

Aus trübem Dunkel stiegen mir
Empor die Bilder der Vergangenheit.
Ich dachte Friese’s, der am Grenzstein fiel,
Der Erste dort am Ufer des Chiese;
Ich dachte Haspingers, des greisen Helden,
Wie er im Priesterkleide vor dem Sarg
Die Hand auf seine rothen Wunden legte.

Am 10. Juni kehrte das Freiwilligen-Korps nach zweimonatigem Dienst an der Grenze nach Bozen zurück, wo sich die Kompanie in alle Winde zerstreute. Anstatt belobigt zu werden, wurden einzelne Mitglieder des Korps unter polizeiliche Aufsicht gestellt, da man befürchtete, dass sie mit ihrer deutschnationalen Gesinnung das Volk aufwiegeln könnten. In einem Polizeibefehl des Kreisamtes Schwaz, in den Pichler später Einsicht nehmen konnte, wurden die Studenten, die eben noch in uneigennütziger Weise mitgeholfen hatten, die Grenzen ihres Heimatlandes zu schützen, als unruhige, vom revolutionären Geist ergriffene „Schwindelköpfe“ tituliert, die einer strengen Beobachtung bedürften.[4]
Nach der Auflösung des Freikorps begab sich Pichler nach Wien zurück, wo er den Oktoberaufstand miterlebte; er selbst nahm aber keinen Anteil mehr daran.

Im November 1848 übernahm er an der philosophischen Fakultät der Universität Innsbruck eine Supplentenstelle an der Lehrkanzel für Naturgeschichte. Seine Hoffnung, dass ihm diese Hilfstätigkeit später von Nutzen sein könnte, erfüllte sich nicht; zweimal wurde ihm bei der Vergabe der Professur ein Mitbewerber vorgezogen. Anstatt sich – wie dies aufgrund seiner Ausbildung zu erwarten gewesen wäre – als Arzt zu betätigen, legte Pichler nach diesen Absagen die Lehramtsprüfung ab. Aber auch die Anstellung am Gymnasium verzögerte sich, weil er – wie er später meinte – „sich nicht bückte und stets mit mehr Ehrlichkeit als Klugheit schwarz schwarz und weiß weiß nannte.“ Schließlich gelang es ihm seine Ernennung doch noch durchzusetzen; diese erfolgte mit Dekret vom 18. Juni 1851.

Noch vor dem Eintritt in den Schuldienst eilte Pichler im August 1850 ins Schleswig-Holsteinische Rendsburg, um „dem von den Dänen unterdrückten kleinen deutschen Bruderstamm“ militärische Hilfestellung anzubieten. Nach der Erkundung der Lage kehrte er nach Innsbruck zurück, um ein aus Tirolern gebildetes Freiwilligen-Korps zusammenzustellen. Aber noch bevor er sein Vorhaben umsetzen konnte, wurde er zum Statthalter zitiert, wo ihm klargelegt wurde, dass die Regierung eine Teilnahme von Tirolern am Schleswig-Holsteinischen Krieg nicht gestatte. Pichler fühlte sich – wie er seinem Tagebuch anvertraut – dadurch „für immer kaltgestellt“ und um seine politischen Ideale betrogen.

Da ihn die Lehrtätigkeit am Gymnasium nicht ausfüllte, wandte sich Pichler in seiner Freizeit der Geologie und der Mineralogie zu. Um Anschauungsmaterial für seine geognostischen Forschungen zu sammeln, unternahm er ausgedehnte Wanderungen durch die Tiroler Bergwelt. Indem er das auf diesen Streifzügen gesammelte Material auf das Genaueste beschrieb, gelangte er zu einem Gesamtbild, das unvergleichlich schärfer war als das seiner Vorgänger. Die Zahl der von ihm gesammelten Mineralien beläuft sich auf mehrere tausend Stück.

Aufsehen erregte Pichler in der geologischen Welt, als er 1863 Köfelsit, ein schlackenartiges Gestein aus dem gleichnamigen Ortsteil der Gemeinde Umhausen, das ihm zur Untersuchung zugesandt worden war, als Auswurfsprodukt einer nach der Eiszeit stattgefundenen vulkanischen Eruption qualifizierte. Damit wäre diese Fundstätte das einzige jungvulkanische Gesteinsvorkommen im Alpenraum gewesen. Wie jüngste Forschungen zeigen, irrte Pichler jedoch in diesem Punkt: die lavaähnliche Konsistenz des Gesteins ist nicht vulkanischen Ursprungs, sondern das Ergebnis der bei großen Bergstürzen auftretenden Hitzeentwicklung, die zu bimssteinähnlichen Gesteinsverglasungen führt.

Das Bestreben, seine geologische Ausbildung zu vervollkommnen, seine Funde mit außeralpinen Museen zu vergleichen und mit anderen Forschern in Kontakt zu treten, veranlasste Pichler an der Wende des Jahres 1855/56 zu einer mehrmonatigen Reise nach München, Berlin, Dresden, Wien, Graz und Istrien. Bei dieser Gelegenheit stattete er dem greisen Alexander von Humboldt in Berlin einen Besuch ab:

„Seine Gestalt war klein, fast schwächlich, das sparsame, kurz geschnittene Haar schneeweiß; machtvoll und bedeutend ragte die bleiche Stirne empor, unter der zwei nicht große, aber helle und scharfe Augen mit ungewöhnlichem Glänze blitzten, so dass ich fast an jene Büsten erinnert wurde, denen die Römer statt der Pupillen Edelsteine einsetzten. … Wir sprachen zuerst über Literarisches. … Dann wendete sich die Rede auf die Naturwissenschaften. Dabei sagte er mir unter anderem, dass er an seinem Kosmos (Entwurf einer physischen Weltbeschreibung) in den Nachtstunden zwischen 11 und 3 Uhr zu arbeiten pflege, denn unter Tags sei er ganz von Dingen in Anspruch genommen, die kaum mit dem engeren Kreise seines Studiums zusammenhängen.“[5]
Nach dieser Zusammenkunft kehrte Pichler „sein Glück preisend, dass es ihm vergönnt war, einen der Koryphäen der großen klassischen Zeit noch Aug in Auge kennen zu lernen“ nach Innsbruck zurück, wo er zu Weihnachten 1856 mit Josefine, der Tochter des Kunsthändlers Johann Groß, bekannt wurde. Nach einer kurzen Verlobungszeit fand am 9. September 1857 in St. Johann in Tirol die Hochzeit statt. Seine Frau gebar ihm drei Kinder, einen Sohn und zwei Töchter, von denen eine in jungen Jahren verstarb. Über sein Familienleben ist nur wenig bekannt. Besonders glücklich scheint die Ehe allerdings nicht gewesen zu sein, zumal sich in den Aufzeichnungen Pichlers immer wieder Bemerkungen über frühere Liebschaften eingestreut finden, während seine Frau in seinen Schriften kaum eine Rolle spielt. Dennoch glaubte Pichler von sich sagen zu können, dass er seiner Gattin ein ehrlicher Mann und seinen Kindern ein treuer Vater gewesen sei. Seine Frau, mit der er zuletzt nur mehr brieflich verkehrte, schied 1888 nach zweitägiger tiefer Bewusstlosigkeit aus dem Leben. Sein „an Geist und Körper reich ausgestatteter armer unglücklicher Sohn“ (der ebenfalls Adolf hieß) starb unter nicht bekannten Umständen, wahrscheinlich durch eigene Hand, erst 33-jährig, im Herbst 1893.[6]
Im Mai 1859 entdeckte Pichler auf einer Wanderung im Kaisertal eine merkwürdige „Knochenhöhle“ (die so genannte Tischofer Höhle), in der nach Ansicht des Entdeckers „in grauer Vorzeit ganze Geschlechter von Bären gehaust haben mussten“. Bei späteren archäologischen Grabungen in dieser Höhle wurden mehrere Speerspitzen zu Tage gefördert, die aus der Zeit um 30 000 v. Chr. stammen. Sie gelten heute als das älteste Zeugnis für die Besiedlung Tirols.

Als im selben Jahr der neu geschaffene Lehrstuhl für Germanistik zu besetzen war, machte sich Pichler, der sich mit seinen Untersuchungen Über das Drama des Mittelalters in Tirol (1850) auch auf dem Gebiet der Literaturgeschichte einen Namen gemacht hatte, Hoffnungen auf den Posten. Da er aber als unangepasster Liberaler galt, der der Monarchie und der Kirche kritisch gegenüberstand, war er politisch nicht durchzusetzen. Schließlich kehrte er an das akademische Gymnasium in der Angerzellgasse zurück, wo er vom Februar 1861 bis Juni 1867 unterrichtete. Acht Jahre musste er auf dieser Stelle ausharren, bis ihm mit der Ernennung zum Professor der Geologie am 23. April 1867 die lang ersehnte akademische Würde zuteilwurde.

Mit der Bestellung zum Professor der Geologie hatte Pichler den Zenit seiner akademischen Laufbahn erreicht. Seine Einstellung den Mächtigen gegenüber hat sich aber dadurch nicht geändert, wenngleich er seine Gedanken jetzt nicht mehr öffentlich äußerte, sondern nur mehr seinem Tagebuch anvertraute:

„Ich verbringe den Sommer mit Steinklopfen; die petrifizierten Sauriere sind mir viel wichtiger als all die Säue in der Politik.“[7]
Im September 1869 fand in Innsbruck die 43. Versammlung der Naturforscher und Ärzte statt, bei welcher Pichler mit Carl Vogt, Rudolf Virchow, Karl Semper und anderen wissenschaftlichen Größen bekannt wurde.

Im Sommer 1874 wäre Pichler bei einer Exkursion beinahe zu Tode gestürzt. Nur durch einen entschlossenen Sprung über den Abgrund konnte er sich retten.

1879 wählte man ihn zum Rektor der Universität Innsbruck, eine Ehre, die er aber ohne Angabe von Gründen ablehnte.[8]
Als er im Herbst 1890 nach 42 Dienstjahren in den Ruhestand trat, notierte er in sein Tagebuch:

„Ich habe in diesen Jahren, soweit es ein Mensch von sich sagen darf, meine Pflicht redlich erfüllt und darf daher mit einer gewissen Beruhigung zurückblicken. Ich bin still und ernst, fast feierlich gestimmt; ein neuer Lebensabschnitt beginnt für mich, möge ich in Geduld alles ertragen, was er mir noch von Leid bringt und mir endlich Frieden vergönnt sein bis zu meinem Ende.“[9]
Trotz seiner Errungenschaften auf dem Gebiet der Geognosie fühlte Pichler vor allem als Dichter. In seiner Jugendzeit und auch noch in seinen Mannesjahren hatte er geglaubt, dass das Drama jene Kunstform sei, in der er sich am besten auszudrücken vermöchte. Die von ihm verfassten Szenen Der Student (1838), der Entwurf zum Revolutionsdrama Ulrich von Hutten (1839) sowie die zwei Jahrzehnte später entstandenen Tragödien Die Tarquinier (1861) und Rodrigo (1862) bestätigen diese Selbsteinschätzung nicht, auch wenn das zuletzt genannte Stück am k.k. Nationaltheater in Innsbruck mit großem Erfolg aufgeführt wurde. Die Innsbrucker Nachrichten berichten über die Vorstellung am 5. April 1862:

„Das Ereignis dieser Woche bleibt für Innsbruck die gestern stattgefundene erste Aufführung des Dramas Rodrigo von unserm vaterländischen Dichter Adolf Pichler. Das historische Drama wurde gestern mit allgemeinem stürmischem Beifall aufgenommen und dessen Dichter sowie die Darsteller der Hauptpartien nach den Aktenschlüssen mehr als zehnmal gerufen worden, was bei einem Trauerspiele hier zu den Seltenheiten gehört. Deutschland ist um ein sehr gelungenes Drama reicher geworden!“

Noch größeres Lob erntete Pichler aber für seine lyrischen Werke. Als seine bedeutendste Leistung auf diesem Gebiet gelten seine Hymnen. Der Dichter selbst schreibt über sein Werk in seinen Tagebüchern:

„30. Juni 1855: Hymnen bei Wagner in einer Auflage von 250 Stück gedruckt. Sie wurden beifällig aufgenommen. Alexander von Humboldt bezeichnete sie in einem Briefe an mich als ernst erhabene und zugleich anmutig lebensfrische Dichtungen.“[10]
Neben seinen dramatischen und lyrischen Arbeiten hat Pichler auch epische Werke hinterlassen. Beachtung haben seine Wanderskizzen gefunden, die er, um die Kosten für seine Reisen zu decken, in diversen Zeitschriften veröffentlichte, bis schließlich im Jahre 1861 ein Teil von ihnen zum Buch Aus Tiroler Bergen vereinigt wurde. Später wurde diese Sammlung von ihm ergänzt und im Jahre 1896 unter dem Titel Kreuz und quer neu herausgegeben. Ebenfalls aus diesem Fundus stammen die Geschichten aus Tirol (1867), denen sich 1897 die Jochrauten und 1898 der Sammelband Letzte Alpenrosen anschlossen, die Pichler dem deutschen Dichter Ferdinand von Saar widmete.

Den Wanderskizzen verwandt sind die von Pichler selbst als „erzählende Gedichte“ bezeichneten Schriften, die in den Sammlungen Marksteine und Neue Marksteine zusammengefasst sind. Die Werke des zuletzt genannten Bandes sind im reimlosen Blankvers geschrieben, der die möglichste Annäherung an die Sprache des Alltags gestattet. Ihrem Inhalt nach sind es Charakterzeichnungen von einfachen Bergnaturen, in denen die Hauptperson, der alte Steinklopfer, unschwer als der Dichter selbst zu erkennen ist:

Ein schwarz Gewölk, durchflammt von hellem Blitz
Sah ich des Hagels Streifen näher zieh’n.
Der Sturm fuhr eisig kalt in mein Gesicht;
Nach Hause war’s für heut zu spät, ich fasste
Stock, Büchse rasch, bald spornten zum Galopp
Mich schwere Schloßen, bis des Hexenmeisters
Verfemte Hütte keuchend ich erreicht.
(Aus: Der Hexenmeister, 1871)

Seine letzten Lebensjahre verbrachte Adolf Pichler abwechselnd in Freundsheim bei Barwies und in Innsbruck. Er hat sich seine geistige Frische bis zuletzt bewahren können. Anlässlich der Gründung des deutschen Volksvereins für Tirol im März 1898 verfasste er folgende kraftvolle Verse über Dietrich von Bern, die gleichzeitig auch als sein Vermächtnis gelten können:[11]
Kennt den Dietrich Ihr den Berner?
Den man einst in Fesseln schloss –
Flammen atmet er im Zorne –
Dass wie Wachs das Eisen floss
Folgt dem Beispiel Eures Helden –
Duldet nie ein fremdes Joch –
Duldet nie als Herrn den Sklaven –
Der am Boden vor Euch kroch.

Am 15. November 1900 erlag Pichler einem Herzschlag. Das Trauerhaus, in dem man seine Leiche aufbahrte, wurde bald zu einer Art Wallfahrtsstätte: „Jung und Alt, Reich und Arm, Vornehm und Nieder pilgert dahin, um persönlich von dem großen Toten Abschied zu nehmen“, berichtete eine Innsbrucker Zeitung. Und: „Eine Auszeichnung seltener Art, wie sie nur den Besten der Nation zuteil wird, ist dem edlen Vorkämpfer der Freiheit beschieden: die deutsche Jungmannschaft unserer Hochschule erweist ihm die letzte Ehre; die Burschenschaft, die deutschnationalen Verbindungen und die Corps bilden die Ehrenwache des hochverdienten Lehrers unserer alma mater – in voller Wichs, mit gezogenen Schlägern, stehen ihrer zwei im Trauergemach, die nach jeder Stunde von anderen abgelöst werden.“[12]
In den ersten drei Jahrzehnten seines Lebens war Pichlers Denken und Handeln noch ganz vom Liberalismus bestimmt. Von der Vorstellung erfüllt, das Recht auf freie Rede sowie freie Äußerung und öffentliche Verbreitung der Meinung in Wort und Schrift durchzusetzen und den absoluten Machtanspruch des Kaisers durch eine geschriebene Verfassung einzuschränken, nahm er 1848 am Märzaufstand teil. Nach dessen Niederschlagung musste er sich politischer Äußerungen enthalten, um seine Chancen auf eine Anstellung nicht zunichtezumachen, was ihm allerdings nur teilweise gelang. Erst als das Kaisertum nach den verlorenen Kriegen von 1859 und 1866 schwächelte, konnte er wieder seine Stimme erheben. Damals trat er noch als Gastredner bei Versammlungen der Liberalen auf, musste aber bald erkennen, dass seinem Wort nicht mehr das Gewicht beigemessen wurde, wie er es von früher her gewohnt war.

Während sich die Liberalen in den sechziger Jahren mehr und mehr von deutschnationalen Ideen verabschiedeten, hielt Pichler an diesem Gedanken fest und ließ keine Gelegenheit aus, die sprachliche, kulturelle und politische Einheit Österreichs mit Deutschland zu betonen. Da man ihm nicht darin folgte, begann er sich von seinen ehemaligen Gesinnungsgenossen zu entfremden:

„Wie Würmer nach einem Frühlingsregen brachen damals aus allen Winkeln, wo sie sich bisher sicher geborgen hatten, die Liberalen hervor; bald hieß es, ich sei zu weit links, ein Freimaurer und Atheist, ein Republikaner und Demokrat, oder aber: meine Wahl würde der Sache des Fortschritts schaden.“[13]
Sein politisches Credo lautete:

„Ich wünsche nur, dass jeder Deutsche in den Geschichten, die ich hier erzähle, den Schlag verwandter Herzen fühlen und erkennen möge, dass die Männer am Inn und an der Etsch nicht zu den verkrüppelten und schwachen Zweigen, sondern zu den Kernstämmen des großen deutschen Volkes gehören. Gebe Gott, dass nach langer Zersplitterung und Ohnmacht die Morgenröte neuer Größe, neuer Herrlichkeit dem viel duldenden deutschen Volke zu leuchten beginne!“[14]
Als im Deutsch-Französischen Krieg von 1870/71 der Sieg Deutschlands verkündet wurde, schien Pichler wieder mit der Welt versöhnt zu sein. Der noch immer großdeutsch Fühlende erwartete sich von seinen Studenten nach dieser Siegesmeldung eine entsprechende Reaktion und war bitter enttäuscht, als diese ausblieb. Wie so oft in seinem Leben, verlieh er daraufhin seinen Gefühlen in einem Gedicht Ausdruck, das in der bei Lipperheide erschienenen Gedichtsammlung Deutsche Tage in Tirol aufgenommen wurde. In diesem Werk ging er mit den Studenten derart scharf ins Gericht, dass mehrere Korporierte der Corps Gothia und Corps Athesia Innsbruck beim Rektor der Universität eine Klage einbrachten und von diesem verlangten, Pichler zum Widerruf zu veranlassen.

Pichler hatte sich von solchen Angriffen – die in diesem Fall ja aus dem eigenen Lager kamen – nicht beeindrucken lassen. Er hat an seiner deutschen Gesinnung bis zum Tod festgehalten.

Pichler hatte seine Einstellung zu den Juden in seinem Tagebuch festgehalten. Er schrieb:

„Ich achte sie als Mitmenschen und trete dafür ein, dass das Gesetz der Menschenliebe auch ihnen gegenüber ohne Einschränkung gilt. Als Mitbürger will ich sie (auch wenn sie Rekruten stellten und Steuern zahlten) aber solange nicht behandeln, als sie unter sich im Guten und im Bösen eine solidarische Gesellschaft bilden.“[15]
Pichler verlangte von den Juden mehr als bloße Integration, er forderte von ihnen die völlige Anpassung an die deutschen Sitten und Gebräuche, was einer Aufgabe der eigenen kulturellen Identität gleichkam. Die Juden waren für ihn nur geduldete Gäste, denen das Aufenthaltsrecht bei Vorliegen von triftigen Gründen – etwa bei staatsschädigendem Verhalten – jederzeit entzogen werden konnte. Diese Einstellung zeugt von einer klar antisemitischen Haltung Pichlers, auch wenn sie noch nicht ideologisch untermauert ist und seinen Äußerungen auch die rassistische Zielrichtung des Nationalsozialismus fehlt.

Pichlers Leben ist von einem beständigen Konflikt mit dem Klerus überschattet, der in der zweiten Hälfte des 19. Jahrhunderts die Speerspitze der konservativ-reaktionären Partei bildete. Wo immer es ihm möglich war, bei passender oder unpassender Gelegenheit, schüttete er seine Häme über die kirchlichen Würdenträger aus. So meinte er einmal:

„Die Pfaffen mit ihren verschiedenen Trachten und Kutten, die Bischöfe und Äbte mit ihren Spitzkappen, goldenen Rauchmänteln und Krummstäben kommen mir vor, als ragten sie von einer ausgestorbenen Tierwelt in die Gegenwart, auf die sie kein Recht mehr haben sollen.“[16]
Ein anderes Mal reizte er die hohe Geistlichkeit mit dem in Knittelreimen verfassten Gedicht „Verwandlungen, allen verstockten Gegnern des Darwinismus gewidmet“, was sogar die Staatsgewalt auf den Plan rief. Die Folge war, dass Pichler verurteilt und das „schlüpfrige“ Werk wegen des Vergehens nach § 302 Strafgesetzbuch (Aufreizung gegen Nationalitäten, religiöse Genossenschaften und dgl.) eingezogen und verboten wurde.[17]
Die Anhänger der klerikal konservativen Partei, die wegen ihrer Papsttreue spöttisch als „Ultramontane“ bezeichnet wurden, zahlten Pichler sein herausforderndes Verhalten mit gleicher Münze zurück. Dass dieser bei der Besetzung des Lehrstuhles für Geologie immer wieder übergangen wurde, ist zweifellos auf konservative Intrigen zurückzuführen. Selbst im höchsten Alter gönnte man dem Dichter die Ruhe nicht. Noch ein Vierteljahr vor seinem Tode sah sich Pichler nach einem Angriff von konservativer Seite veranlasst, in der nationalliberalen Zeitschrift Der Scherer eine Entgegnung zu bringen.

Adolf Pichler gilt als Schutzherr einer Tiroler intellektuellen Moderne, die gegen den alles dominierenden Klerikalismus und die Habsburgtreue sich einen Platz in der Öffentlichkeit erobern wollte.[18] Er war kein Mann des Ausgleichs und ging selten Kompromisse ein, sodass er, wenn er seine Stimme erhob, mit seiner Meinung auf die Gesellschaft polarisierend wirkte. Dass er sich damit bei seinen Gegnern unbeliebt machte, nahm er billigend in Kauf.

Trotz seines Widerspruchsgeistes war Pichler ein Mann von trefflichen Eigenschaften. Als Schriftsteller war Pichler fest in seiner Zeit und in seinem Volke verwurzelt. Die Ideen, die sich hinter seinen Dichtungen verbargen, haben im Laufe der Zeit ihre Aktualität verloren, sodass seine lyrischen Werke heute etwas schwerfällig wirken. Noch immer lesenswert sind hingegen seine Wanderskizzen durch Tirol und das autobiographische Werk Zu meiner Zeit, Schattenbilder aus der Vergangenheit, welches mit der Auswahl Aus Tagebüchern 1850–1899 später eine entsprechende Ergänzung fand. Zu erwähnen sind auch die bei Josef Keck & Sohn erschienene Broschüre Aus dem wälsch-tirolischen Kriege[19] und der Bericht über Das Sturmjahr, Erinnerungen aus den März- und Oktobertagen zu Wien 1848, beides Werke, die auch heute noch zu fesseln vermögen.

Seine Leistungen auf dem Gebiet der Geologie wurden in den 1930er Jahren von Robert Ritter von Srbik ausführlich gewürdigt.[20] Pichler selbst war weit davon entfernt, seinen Forschungsergebnissen solche weltumspannende Bedeutung beizumessen, wenn er auch von sich mit vollem Recht sagte:

Seine Beiträge zur Geognosie Tirols wurden in der Zeitschrift des Ferdinandeums, den Jahrbüchern der Geologischen Reichsanstalt, den Jahrbüchern für Mineralogie und Geologie von Leonhard von Bronn und den mineralogisch-petrographischen Mitteilungen veröffentlicht.

Zu seinen Lebzeiten ist Pichler diese Anerkennung lange vorenthalten worden, was den sonst nicht nach Lorbeeren Strebenden doch verletzt zu haben scheint, denn als man ihn einmal bei einem Kongress der Naturforscher als tirolischen Dichter vorstellte, quittierte er diese Floskel mit der Bemerkung:

Die Verleihung des Adelsprädikates „Ritter von Rautenkar“ war die erste bedeutende Ehrung des offiziellen Österreich, die dem Dichter zuteil wurde. Zu seinem 70. Lebensjahr wurde ihm die Ehrenbürgerschaft der Stadt Innsbruck verliehen.

Die Feier seines 80. Geburtstages gestaltete sich zu einem großen Triumph für ihn und die nationale Sache im Lande; der letzte Umstand freute ihn besonders. Der Scherer gab dazu eine illustrierte Festnummer heraus. Pichler selbst hielt das Tagwerk seines Daseins nun für beendet und begann seine Schriften für eine Gesamtausgabe vorzubereiten, die nunmehr seine Tochter Mathilde besorgte.

Nach dem Ableben Pichlers wurden der Kunstmaler Professor Josef Schretter und der Bildhauer Professor Edmund Klotz damit beauftragt, die Totenmaske des Dichters abzunehmen. Seine Leiche wurde auf dem Westfriedhof in Innsbruck gemäß dem Wunsch des Verstorbenen, außerhalb der Arkaden, „im Freien“ bestattet.[22]
An nächsten Verwandten hinterließ Pichler eine Tochter, die ihn in seinen letzten Lebensjahren treu pflegte, zwei Enkel und einen Bruder.

Von Pichler existieren zahlreiche Gemälde und Skulpturen. Der Historienmaler Reisacher hat ihn 1848 als Hauptmann der studentischen Landwehr verewigt. Als besonders gelungen gilt das Bild des Künstlers Mahlknecht, das 1854 im Museum Ferdinandeum zur Besichtigung ausgestellt war.[23] Der Bildhauer Heinrich Fuß hat zwei Bronzestatuen von Pichler angefertigt.[24]
Im Mai 1909 wurde am Karl-Ludwig-Platz (der heute nach dem Dichter benannt ist) ein von Edmund Klotz geschaffenes Standbild enthüllt. Es zeigt Pichler im Alltagskleid, leicht nach vorne gebeugt, mit Hut und Stock in der Hand, so wie er seinen Zeitgenossen in Erinnerung geblieben ist. Sein alter Freund Dr. Alois Brandl, ein gebürtiger Innsbrucker, hielt die Festrede (Innsbrucker Nachrichten, 17. Mai 1909).

Am Fuße der Kalkkögel im Senderstal erinnert die 1903 errichtete Adolf-Pichler-Hütte an den Dichter.

Pichler wohnte in Wilten in der Müllerstraße 33. Das schlichte Gebäude ziert eine Gedenktafel mit der Aufschrift:

Sein Nachlass wird vom Tiroler Landesmuseum Ferdinandeum verwaltet und ist derzeit der Universität Innsbruck als Leihgabe überlassen.[25]
Die noch vor dem Ersten Weltkrieg von Georg Müller in München verlegte Gesamtausgabe von Pichlers Werken umfasst 17 Bände:
