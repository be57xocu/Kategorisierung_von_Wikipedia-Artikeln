Stefan Slupetzky (* 1962 in Wien) ist ein österreichischer Schriftsteller, Krimiautor, Kinderbuchautor, Illustrator und Musiker.

Von 1981 bis 1990 studierte Slupetzky an der Akademie der bildenden Künste in Wien.
Parallel dazu betätigte er sich auch als Saxophonist in verschiedenen Jazzbands und als Schauspieler.

1989 wurde seine Tochter Fanny geboren.

Nach Beendigung des Studiums unterrichtete er ein Jahr Kunst- und Werkerziehung an einem Wiener Gymnasium.

Seit 1991 arbeitet er als freischaffender Autor und Illustrator in Wien.

Seit 2005 dramatisiert er Romane und Novellen österreichischer Klassiker (u. a. Stefan Zweig und Arthur Schnitzler) für die Festspiele Reichenau.

2005 bekam er für seinen Roman Der Fall des Lemming den Friedrich-Glauser-Preis verliehen, 2006 für Lemmings Himmelfahrt den Burgdorfer Krimipreis. 2007 wurde Das Schweigen des Lemming von den Wienern zu einem ihrer hundert Lieblingsbücher gewählt. Ende Juni 2009 erhielt Slupetzky für seine Lemming-Romane den Radio Bremen Krimipreis zugesprochen, der ihm am 16. September 2009 im Rahmen des Bremer Krimifestivals PrimeTimeCrimeTime überreicht wurde.[1]
Von 2006 bis 2009 betreute er – auf Radio Wien – die literarische Ecke von Willi Resetarits' sonntäglicher Radiosendung „Trost und Rat“.

Sein Roman Der Fall des Lemming wurde 2008 mit Fritz Karl in der Titelrolle verfilmt - Der Fall des Lemming.

2007 wurde sein Sohn Samuel geboren.

2010 wurde sein Roman Lemmings Zorn sowohl für den Friedrich-Glauser-Preis als auch für den erstmals ausgelobten Leo-Perutz-Preis der Stadt Wien nominiert und mit dem Leo-Perutz-Preis ausgezeichnet.

Slupetzky ist Mitbegründer des Vereins zur Verwertung von Gedankenüberschüssen und wirkte an der Erfindung und Entwicklung unverzichtbarer Gebrauchsgegenstände wie etwa des „Transzebra Portable“, eines ausrollbaren Zebrastreifens, mit.

Neben regelmäßigen Autorenlesungen ist Stefan Slupetzky auch als Texter und Sänger der Wienerliedcombo Trio Lepschi (mit Tomas Slupetzky und Martin Zrost) aktiv.
