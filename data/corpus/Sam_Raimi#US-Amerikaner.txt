Samuel Marshall „Sam“ Raimi (* 23. Oktober 1959 in Royal Oak, Michigan) ist ein US-amerikanischer Regisseur, Filmproduzent, Drehbuchautor und Schauspieler.

Seine bekanntesten Werke sind die Tanz-der-Teufel- und die Spider-Man-Filme.

Raimi wurde in Royal Oak geboren und wuchs in Birmingham in einer jüdischen Familie auf. Seine Eltern sind russisch-jüdischer und ungarisch-jüdischer Herkunft. Der ursprüngliche Familienname „Reingewertz“ wurde zu „Raimi“, als sein Großvater in die Vereinigten Staaten auswanderte. Er studierte englische Literatur an der Michigan State University in East Lansing, brach jedoch das Studium nach drei Semestern ab, um sein Spielfilmdebüt Tanz der Teufel zu drehen.

Raimi ist das vierte von fünf Kindern. Mit seinem älteren Bruder Ivan und seinem jüngeren Ted Raimi arbeitet er oft gemeinsam an Filmprojekten. Seine ältere Schwester Andrea Raimi Rubin ist Anwältin. Sein ältester Bruder Sander verstarb 1968 bei einem Badeunfall in Israel im Alter von 15 Jahren.

Raimis Schwiegervater war Lorne Greene, dessen Tochter Gillian er 1993 heiratete. Die beiden haben zusammen vier Kinder: Lorne Daniel Raimi (* 22. März 1994), Henry Raimi (* 1996), Emma Rose Raimi (* 1999) und Dashiell William Schooley Raimi (* 11. Dezember 2003). Emma, Lorne und Henry übernahmen kleine Rollen in Spider-Man 3 und Drag Me to Hell.

1979 gründete er zusammen mit Produzent Robert Tapert und B-Movie Schauspieler Bruce Campbell die Produktionsfirma Renaissance Pictures. Seinen langjährigen Freund Bruce Campbell besetzt er sehr oft in Haupt- oder Nebenrollen seiner Filme.

Er ließ sich im Auftrag Blizzard Entertainments von Legendary Pictures dazu verpflichten, als Regisseur des Warcraft-Films zu fungieren, im März 2013 erklärte er jedoch, dass er nicht mehr an dem Projekt beteiligt sei. Raimi wird bei den Remakes von Poltergeist[1] und Tanz der Teufel als ausführender Produzent agieren.[2] Im Mai 2012 verklagte er mit seiner Firma Renaissance Pictures die Independent Film Company Award Pictures für deren Plan, einen vierten Teil von The Evil Dead zu drehen.[3]
2015 entwickelte er die Fernsehserie Ash vs. Evil Dead basierend auf der Evil-Dead-Trilogie.

Gemeinsam mit Florian Henckel von Donnersmarck und mit Unterstützung durch die chinesische Beijing Cultural Investment Holding gründete Raimi 2016 das Produktionsunternehmens Allegory Films, über das künftig Filme mit einem Budget zwischen 30 und 80 Mio. Dollar produziert werden sollen.[4]
Zu seinen Markenzeichen zählen unter anderem ein gelbes Oldsmobile Delta 88 aus dem Jahre 1973 („The Classic“) in vielen seiner Filme, selbst als Planwagen getarnt in seinem Western Schneller als der Tod. Eine Flasche Maker's Mark Whiskey ist auch regelmäßig zu sehen. Stilbildend wirkte die von Raimi erfundene und in Tanz der Teufel erstmals eingesetzte Methode der „Shakycam“, bei der eine Kamera auf ein Brett geschnallt wurde, das anschließend von zwei Leuten an den Enden durch den Wald getragen wurde und dadurch die ungewöhnlichsten Kameraperspektiven ermöglichte.[5] Wenn er Regie führt, trägt er wie Alfred Hitchcock immer einen Anzug am Set.
