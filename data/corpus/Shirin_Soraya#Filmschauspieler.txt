Shirin Soraya (* 26. Januar 1976 in München) ist eine deutsche Schauspielerin.

Bereits als Kind wirkte Shirin Soraya, Tochter eines persischen Vaters und einer deutschen Mutter, in verschiedenen Theaterproduktionen des Staatstheaters am Gärtnerplatz in München mit, darunter mit 15 Jahren im Musical „Anatevka“ in der Rolle der Sprintze. Daneben nahm sie an Hörspiel-Produktionen des Bayerischen Rundfunks teil.[1]
2001 absolvierte sie an der Hochschule für Film und Fernsehen „Konrad Wolf“ ein Schauspielstudium. Größere Bekanntheit erlangte sie durch ihre erste TV-Rolle in der Comedy-Show „Was guckst du?!“; es folgten weitere Auftritte in der Kinderserie „Schloss Einstein“ sowie ab 2002 eine Hauptrolle in der Comedy-Serie „Sechserpack“.

Nach dem Ende von Sechserpack gründete sie 2009 ihr eigenes Unternehmen LichtRaumBerlin, das sich mit „Energiearbeit“ beschäftigt und in dem sie als „Energieseherin“ tätig ist.[2]
Neben ihrer Muttersprache Deutsch spricht Soraya fließend Englisch und Italienisch.[3]