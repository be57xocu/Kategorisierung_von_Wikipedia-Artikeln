Paco Ignacio Taibo II (* 11. Januar 1949 in Gijón als Francisco Ignacio Taibo Mahojo) ist ein mexikanischer Autor. 

Taibo, der Sohn des Autors und Intellektuellen Paco Ignacio Taibo I (1924–2008), zog mit seinen Eltern 1957 nach Mexiko. Dort studierte er Literatur, Soziologie und Geschichte. Bevor er sich als Autor etablieren konnte, arbeitete Taibo als Journalist, Universitätsdozent und Sachbuchautor. 
Er politisierte sich in der blutig niedergeschlagenen Studentenbewegung des Jahres 1968. Seine Reaktion auf das Massaker war permanente Opposition. Gemeinsam mit seiner Frau Paloma ist er bis heute den Protestbewegungen Mexikos in Solidarität verbunden.
Taibo II gilt als Begründer des neuen lateinamerikanischen Kriminalromans, der Stilmittel des Abenteuerromans, Politthrillers und Krimis miteinander verbindet. Weltweit bekannt wurde er durch die Figur des erst gestorbenen und dann wiederauferstandenen unabhängigen Detektivs Héctor Belascoarán Shayne, der im Großstadtdschungel von Mexiko-Stadt Ermittlungen durchführt, die ihn immer wieder in einen Sumpf von Korruption und politischer Repression führen. 
Er ist Mitbegründer der Internationalen Vereinigung der Kriminalschriftsteller (AIEP) und Organisator der Semana Negra, eines jährlich stattfindenden internationalen Krimifestivals in Gijon, Spanien.
Taibo II ist zum einen für seine Krimis bekannt, zum anderen als politischer Autor. Seine Krimis enthalten oft historische und politische Referenzen. Sie sind häufig in politisch brisanten Situationen angesiedelt, wie dem Vorfeld des spanischen Bürgerkriegs, oder sie enthalten klare, nicht selten sozialkritische Bezüge zur Situation des mexikanischen Alltags.

Als Historiker hat er sich u.a. durch die Biografien von Ernesto Che Guevara und Pancho Villa ausgezeichnet.
In seinen Arbeiten um Che Guevara entdeckte er dessen Aufenthalt in Afrika wieder, der in Das Jahr in dem wir nirgendwo waren ausführlich geschildert wird.
