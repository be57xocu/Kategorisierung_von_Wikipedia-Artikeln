Boris Starling (* 1970 in London) ist ein englischer Schriftsteller.

Boris Starling studierte an der University of Cambridge. Bevor als Schriftsteller tätig wurde, arbeitete er für den Daily Telegraph und die Sun als Reporter und als Privatdetektiv.

Der Fan von „Tim und Struppi“-Comics sowie Romanen von Dick Francis lebt heute in Suffolk (London).
