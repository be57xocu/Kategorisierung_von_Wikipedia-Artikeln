Tahar Ben Jelloun (* 1. Dezember 1944 in Fès) ist ein frankophoner marokkanischer Schriftsteller und Psychotherapeut und gilt als bedeutendster Vertreter der französischsprachigen Literatur aus dem Maghreb.

Nach dem Besuch einer arabisch-frankophonen Grundschule zog Tahar Ben Jelloun als 18-Jähriger von seinem Geburtsort Fès nach Tanger, um dort ein französisches Gymnasium (Lycée) zu besuchen. 1963 begann er ein Philosophiestudium an der Universität Muhammad V in Rabat, wo er auch seine ersten Gedichte verfasste, die in dem Sammelband Hommes sous linceul de silence 1971 erschienen sind. Nach Abschluss seines Studiums unterrichtete er Philosophie in Marokko, musste jedoch 1971 nach Frankreich emigrieren, da die Lehre der Philosophie arabisiert wurde und er dafür nicht ausgebildet war. Er wurde dort 1975 in sozialer Psychiatrie promoviert, wovon auch seine Werke beeinflusst wurden (z. B. La Réclusion solitaire, 1976). Ab 1972 schrieb er regelmäßig für Le Monde.

In den folgenden Jahren veröffentlichte Ben Jelloun zahlreiche Romane, unter anderem 1985 L’Enfant de sable, womit er bekannt wurde. La Nuit Sacrée, die Fortsetzung von Enfant de sable, wurde 1987 mit dem Prix Goncourt ausgezeichnet. Damit war Ben Jelloun der erste aus Nordafrika stammende Autor, der diesen Preis erhielt.

1994 wurde er mit dem Grand Prix littéraire du Maghreb ausgezeichnet, womit er sich endgültig als französischer Autor durchsetzte.

Im Juni 2004 erhielt Ben Jelloun den mit 100.000 Euro dotierten irischen Literaturpreis IMPAC für sein Buch Cette aveuglante absence de lumière (deutsch: „Das Schweigen des Lichts“), in dem er die Zustände im – inzwischen geschlossenen – Gefangenenlager Tazmamart beschreibt. Dem Roman liegt eine wahre Geschichte zugrunde.

Jellouns Werke wurden in zahlreiche Sprachen übersetzt, so zum Beispiel L’Enfant de sable und La Nuit sacrée in 43 Sprachen oder Le Racisme expliqué à ma fille in 25 Sprachen.[1]
Sein Roman Les Raisins de la galère wurde außerdem in NRW und Niedersachsen für das Zentralabitur 2008 und 2009 in Französisch vorausgesetzt.

Tahar Ben Jelloun lebt mit seiner Frau und seiner Tochter Mérième, für die er einige pädagogische Bücher geschrieben hat, in Paris. In Le Racisme expliqué à ma fille (deutsch: „Papa, was ist ein Fremder?“) von 1997 erklärt er seiner Tochter – und allen anderen Interessierten – einfühlsam und dennoch sachlich, warum Rassismus entsteht und wie man dem entgegenwirken kann. Dieses Kinderbuch und L’Islam expliqué aux enfants (deutsch: „Papa, was ist der Islam?“), welche Jelloun beide als Essay schrieb, wurden sehr bald zu internationalen Bestsellern.

1988 wurde Ben Jelloun zum Ritter und 2008 zum Großoffizier der Ehrenlegion ernannt.[2] Außerdem wurde ihm am 19. Juni die Ehrendoktorwürde der Universität Montreal verliehen.[3]
2011 schrieb Tahar Ben Jelloun das Werk Arabischer Frühling, in welchem er sich mit den Protesten in der arabischen Welt auseinandersetzte[4] und für das er im selben Jahr mit dem Erich-Maria-Remarque-Friedenspreis der Stadt Osnabrück ausgezeichnet wurde.
