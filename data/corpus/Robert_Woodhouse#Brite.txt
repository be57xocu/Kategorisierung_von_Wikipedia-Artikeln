Robert Woodhouse (* 28. April 1773 in Norwich, England; † 28. Dezember 1827 in Cambridge) war ein britischer Professor der Mathematik.

Woodhouse besuchte die Schule in North Walsham, ab 1790 auf das Caius College, Cambridge. Späte Zeit wurde er Professor der Mathematik, Astronomie und der experimentellen Philosophie. 1802 wurde er Mitglied der Königlichen Gesellschaft. Von 1820 bis 1822 war er Lucasischer Professor für Mathematik in Cambridge.

Woodhouse interessierte sich für die theoretischen Fundamente der Analysis, die Notation der Differentialrechnung, die Natur der imaginären Zahlen und ähnlichen Themen. Er schrieb einige Werke zur Analysis und griff dort die "kontinentale" Mathematik an. 

