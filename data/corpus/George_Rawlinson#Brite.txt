George Rawlinson (* 23. November 1812 in Chadlington, Oxfordshire; † 6. Oktober 1902 in Canterbury) war ein britischer Historiker und Geistlicher der Kirche von England.

Rawlinson war der dritte Sohn des Pferdezüchters Abram Tyzack Rawlinson und dessen Ehefrau Elizabeth Eudocia, einer Tochter von Henry Creswicke. Der Diplomat und Keilschriftenforscher Sir Henry Creswicke Rawlinson war sein Bruder.

Nach dem Besuch der Swansea Grammar School und Ealing School immatrikulierte sich Rawlinson 1834 am Trinity College in Oxford. Dort erreichte er 1835 den Grad eines B.A. und schloss 1841 sein Studium mit dem Titel eines M.A. ab. Bereits im darauffolgenden Jahr wurde er zum Priester geweiht. Von 1861 bis 1889 war er Camden-Professors für Alte Geschichte an der Universität Oxford.

Am 6. Juli 1846 heiratete Rawlinson Louisa Wildman, eine Tochter von Sir Robert Chermside. Mit ihr hatte er fünf Töchter und vier Söhne. An der Gründung des Oxford Political Economy Club war Rawlinson maßgeblich mitbeteiligt.

Zusammen mit seinem Bruder übersetzte er Herodot ins Englische; überhaupt machte sich Rawlinson vor allem durch seine kritische Quellenarbeit verdient. Von seinen zahlreichen Werken, die teils großen Einfluss hatten, ist das bekannteste wohl The Five Great Monarchies of the Ancient Eastern World, einer Geschichte vom Babylonisch-chaldäischen Reich bis zu den Achämeniden. Später folgten Nachfolgebände über die Parther und Sassaniden. Das Werk, obwohl in weiten Teilen freilich veraltet, wird auch heute noch als eine lesenswerte Darstellung, gerade der politischen Geschichte, angesehen.

Im Alter von nahezu 90 Jahren starb Rawlinson am 6. Oktober 1902 nach längerer Krankheit in seinem Haus in Canterbury. Seine letzte Ruhestätte fand er auf dem Hollywell Friedhof in Oxford.

Degory Wheare (1622–1647) |
Robert Waryng (1647–1648) |
Lewis Du Moulin (1648–1660) |
John Lamphire (1660–1688) |
Henry Dodwell (1688–1691) |
Charles Aldworth (1691–1720) |
Sedgwick Harrison (1720–1727) |
Richard Frewin (1727–1761) |
John Warneford (1761–1773) |
William Scott (1773–1785) |
Thomas Warton (1785–1790) |
Thomas Winstanley (1790–1823) |
Peter Elmsley (1823–1825) |
Edward Cardwell (1825–1861) |
George Rawlinson (1861–1889) |
Henry Francis Pelham (1889–1907) |
Francis John Haverfield (1907–1919) |
Henry Stuart Jones (1920–1927) |
John George Clark Anderson (1927–1936) |
Hugh Last (1936–1949) |
Ronald Syme (1949–1970) |
Peter Brunt (1970–1982) |
Fergus Millar (1984–2002) |
Alan K. Bowman (2002–2010) |
Nicholas Purcell (seit 2011)
