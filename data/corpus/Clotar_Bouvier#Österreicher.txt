Clotar Bouvier (* 12. April 1853 in Weiz, Steiermark; † 26. September 1930 in Oberradkersburg, Slowenien) war ein österreichischer Rebenzüchter, auf den die Rebsorte Bouvier zurückgeht.

Bouvier war hauptberuflich im Bankgeschäft tätig. Seine Passion sah er jedoch im Weinbau. Auf dem Familien-Weingut in Herzogenberg führte er Experimente mit dem Ziel der Bekämpfung von Reblaus und Mehltau durch. Bevorzugt wurde von ihm die von Hermann Goethe gezüchtete Unterlagsrebe "Rupestris 9". Im Auftrag der ehemaligen Sektkellerei Kleinoscheg in Graz-Östing selektionierte er die später nach ihm benannte Rebsorte Bouvier.

Die Rebsorte Bouvier verbreitete er in der Steiermark (→ Weinbau in Österreich), in Slowenien und in der Slowakei. Im Jahre 1882 gründete Bouvier in Oberradkersburg eine Sektkellerei, die noch heute besteht. Das Gut in Slowenien wurde im Jahre 1945 enteignet, sein Sohn Fritz führte nach dem Zweiten Weltkrieg den steirischen Teil des Besitzes weiter.
