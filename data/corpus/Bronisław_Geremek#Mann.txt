Bronisław Geremek (ursprünglich Benjamin Lewertow; * 6. März 1932 in Warschau; † 13. Juli 2008 in Lubień bei Nowy Tomyśl) war ein polnischer Historiker und Politiker. Von 1997 bis 2000 bekleidete er das Amt des polnischen Außenministers.

Geremek stammte aus einer jüdischen Familie, sein Großvater war Rabbiner. Sein Vater, der eine Kürschnerei besaß, wurde im KZ Auschwitz ermordet, er selbst wurde mit seiner Mutter 1943 aus dem Warschauer Ghetto geschmuggelt und überlebte bei einer polnischen Familie. Nach dem Abitur in Wschowa studierte er an der Universität Warschau Geschichte, wurde 1955 Magister, promovierte 1960 und schloss 1972 seine Habilitation an der Polnischen Akademie der Wissenschaften (PAN) ab. 1989 wurde Geremek außerordentlicher Professor für mittelalterliche Geschichte, 1993 ordentlicher Professor; er erhielt zudem über 20 Ehrendoktortitel.

In den Jahren 1962–1965 war er Hochschuldozent an der Pariser Sorbonne, 1992/93 Gastprofessor am Collège de France und 2002 wurde er Professor am Europakolleg in Natolin im Süden Warschaus.

Bronisław Geremek starb im Alter von 76 Jahren am 13. Juli 2008 um 13.20 Uhr am Steuer seines Autos bei einem Frontalzusammenstoß in der Nähe von Lubień (bei Nowy Tomyśl).[1] Er wurde am 21. Juli auf dem Warschauer Powązki-Friedhof unter Anteilnahme seiner Familie, in- und ausländischer Politiker und der Warschauer Bevölkerung beigesetzt[2].

Geremek war von 1950 bis 1968 Mitglied der kommunistischen PVAP. Er verließ die Partei wegen des Einmarsches der Truppen des Warschauer Paktes in die Tschechoslowakei[3][4]. 1980 war er Berater der Solidarność. Nach der Verhängung des Kriegsrechtes 1981 wurde er interniert.

1989 nahm er an den Gesprächen am Runden Tisch teil. Ab 1990 war er Mitglied der liberalen Parteien Unia Demokratyczna (UD) und ihrer Nachfolgepartei Unia Wolności (UW). Von 2000 bis 2001 führte er die UW als Parteivorsitzender an. Von 1989 bis 2001 war Geremek Mitglied des Sejm der Republik Polen, unter anderem Vorsitzender des Ausschusses für Verfassungsfragen, Vorsitzender des Ausschusses für auswärtige Angelegenheiten und Vorsitzender des Ausschusses für europäisches Recht. Von 1997 bis 2000 war er Außenminister Polens in der Regierung Buzek. Für seine Verdienste um die europäische Einigung wurde er 1998 mit dem Karlspreis ausgezeichnet.[5]
2002 sprach Bronisław Geremek anlässlich der Gedenkstunde zum Tag des Gedenkens an die Opfer des Nationalsozialismus vor dem Deutschen Bundestag. Seit Beginn der 6. Wahlperiode des Europäischen Parlaments 2004 war Geremek als Europa-Abgeordneter (MdEP) der linksliberalen Partei Partia Demokratyczna (DP) tätig. Die Abgeordneten der Partia Demokratyczna waren Mitglieder der europäischen Liberalen-Fraktion Allianz der Liberalen und Demokraten für Europa (ALDE). Geremek war Mitglied des ALDE-Fraktionsvorstands, er war 2004 Kandidat der Europäischen Liberalen für das Amt des Präsidenten des Europäischen Parlaments. Er war Mitglied im Ausschuss für konstitutionelle Fragen, im Ausschuss für auswärtige Angelegenheiten und Mitglied der Delegation im Parlamentarischen Kooperationsausschuss EU–Russland.

Geremek war erklärter Gegner des in Berlin geplanten Zentrums gegen Vertreibungen. Er war Unterstützer der im April 2007 gegründeten internationalen Kampagne für eine Parlamentarische Versammlung bei den Vereinten Nationen.[6]
„Geremek war ein unerschütterlicher Europäer, der mit seinem großen internationalen Ansehen den Weg Polens in die Nato und die EU geebnet hat. […] Geremek war ein zutiefst liberaler Mann, der die Kunst der geschmeidigen Lügen und den abrupten Frontwechsel der postkommunistischen Wendehälse stets verachtet hat. Ob Außenminister […], ob Vortragender oder Zuhörer ist der polnische Historiker, in welcher Funktion immer, ein offener, freundlicher Brückenbauer, ein liberaler Europäer geblieben. […] Zur Zeit des großen Ausverkaufs der europäischen Werte und des Aufstiegs stromlinienförmiger Opportunisten fallen die Masken der Politiker von Berlin bis Warschau, von Wien bis Budapest. Bronislaw Geremek trug nie eine Maske.“

Am 14. Januar 2009 gab das Europäische Parlament bekannt, dass der Hauptinnenhof seines offiziellen Sitzungsgebäudes, das „Immeuble Louise Weiss“ in Straßburg, fortan den Namen „Bronisław-Geremek-Agora“ (Agora Bronisław Geremek) tragen würde.[8] Die Agora wurde am 21. April 2009 eingeweiht.[9]
Krzysztof Skubiszewski |
Andrzej Olechowski |
Władysław Bartoszewski |
Dariusz Rosati |
Bronisław Geremek |
Władysław Bartoszewski |
Włodzimierz Cimoszewicz |
Adam Daniel Rotfeld |
Stefan Meller |
Anna Fotyga |
Radosław Sikorski |
Grzegorz Schetyna |
Witold Waszczykowski |
Jacek Czaputowicz

1950: Coudenhove-Kalergi |
1951: Brugmans |
1952: De Gasperi |
1953: Monnet |
1954: Adenauer |
1956: Churchill |
1957: Spaak |
1958: Schuman |
1959: Marshall |
1960: Bech |
1961: Hallstein |
1963: Heath |
1964: Segni |
1966: Krag |
1967: Luns |
1969: Kommission der europäischen Gemeinschaften |
1970: Seydoux de Clausonne |
1972: Jenkins |
1973: Madariaga |
1976: Tindemans |
1977: Scheel |
1978: Karamanlis |
1979: Colombo |
1981: Veil |
1982: Juan Carlos I. |
1984: Carstens |
1986: Das Volk von Luxemburg |
1987: Kissinger |
1988: Mitterrand und Kohl |
1989: Frère Roger |
1990: Horn |
1991: Havel |
1992: Delors |
1993: González |
1994: Brundtland |
1995: Vranitzky |
1996: Beatrix |
1997: Herzog |
1998: Geremek |
1999: Blair |
2000: Clinton |
2001: Konrád |
2002: Euro |
2003: Giscard d’Estaing |
2004: Cox |
2005: Ciampi |
2006: Juncker |
2007: Solana |
2008: Merkel |
2009: Riccardi |
2010: Tusk |
2011: Trichet |
2012: Schäuble |
2013: Grybauskaitė |
2014: Van Rompuy |
2015: Schulz |
2016: Franziskus |
2017: Garton Ash |
2018: Macron

Außerordentlicher Karlspreis:
2004: Johannes Paul II.
