Guy Jean Léonard Thys (* 6. Dezember 1922 in Antwerpen; † 1. August 2003) war ein belgischer Fußballtrainer.

Guy Thys bestritt zwischen 1952 und 1953 zwei Länderspiele für sein Heimatland (beide gegen die Niederlande) und wurde in den beiden Jahren insgesamt viermal in die Nationalelf berufen.

Guy Thys war von 1976 bis 1989 Trainer der belgischen Fußballnationalmannschaft. Dann legte er eine Pause von sechs Monaten ein und übernahm die Stelle, nachdem Walter Meeuws entlassen worden war, noch einmal bis Mai 1991. Er führte die Nationalmannschaft 1980 in das Finale der Europameisterschaft, das sie mit 1:2 gegen Deutschland verloren.

1982, 1986 und 1990 erreichten die Roten Teufel unter der Leitung von Guy Thys die Qualifikation zur Fußball-Weltmeisterschaft. Dabei hatten sie mit dem 4. Platz in Mexiko 1986 den größten Erfolg. Im Halbfinale unterlag seine Mannschaft gegen Argentinien mit 0:2.

Insgesamt bestritt die Nationalmannschaft unter ihm 114 Spiele. Von diesen gewann sie 50 Spiele, 27 Mal gab es ein Unentschieden, 37 Spiele wurden verloren.

Guy Thys ist der Sohn des zweifachen belgischen Torschützenkönigs Ivan Thys (1897–1982). Sein Onkel, der Bruder von Ivan Thys, ist der ebenfalls in der Nationalmannschaft Belgiens im Einsatz gewesene Joseph Thys (1888–1941).

William Maxwell |
Charles Bunyan |
William Maxwell |
Victor Löwenfeldt |
Hector Goetinck |
Jules Turnauer |
Jack Butler |
François Demol |
Bill Gormlie |
Dougall Livingstone |
André Vandeweyer |
Louis Nicolay |
Géza Toldi |
Constant Vanden Stock |
Raymond Goethals |
Guy Thys |
Walter Meeuws |
Guy Thys |
Paul Van Himst |
Wilfried Van Moer |
Georges Leekens |
Robert Waseige |
Aimé Anthuenis |
René Vandereycken |
Franky Vercauteren |
Dick Advocaat |
Georges Leekens |
Marc Wilmots |
Roberto Martínez
