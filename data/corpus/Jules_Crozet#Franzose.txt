Jules (Julien Marie) Crozet (* 26. November 1728 in Port-Louis, Département Morbihan, Bretagne, Frankreich; † 24. September 1782 in Paris) war ein französischer Seefahrer und Entdecker. 

Er war Namensgeber für die Crozetinseln, die er unter dem Kommando von Marc-Joseph Marion du Fresne 1772 für Europa entdeckte.

Crozet war Zweiter Kommandant auf der Expedition von du Fresne, der auf der Reise von Māori getötet wurde. Crozet übernahm das Kommando, traf 1775 den Entdecker James Cook und informierte ihn über die Entdeckung der Inseln.
