Paul Zerbst (* 25. Juli [1]1997 in Berlin) ist ein deutscher Schauspieler.

Zerbst wurde im Alter von neun Jahren für den Film entdeckt. Seine erste Rolle hatte er 2006 im Film Pommery und Leichenschmaus. Es folgten Auftritte in Geküsst wird vor Gericht (2006), 3 Engel auf der Chefetage (2006) und Polizeiruf 110 (2007). Großen Erfolg hatte Zerbst in der ZDF-Serie Meine wunderbare Familie (2008–2010).
