Anthony Chisholm ist ein US-amerikanischer Schauspieler. Bekannt wurde er durch seine Rollen in der TV-Serie Oz – Hölle hinter Gittern als Charakter Burr Redding sowie als Tito im Film Premium Rush.

Chisholms Karriere als Schauspieler begann mit seiner kurzen Rolle im Film Black Power im Jahr 1968, anschließend folgten einige weitere kleinere Rollen in Filmen – so in Putney Swope als Cowboy (1969) und in Wenn es Nacht wird in Manhattan (Original: Cotton Comes To Harlem, 1970) als „Black Plainclothesman“. Es folgten weitere Rollen in US-amerikanischen Fernsehserien und Kurzfilmen.

Im Jahr 1998 spielte Chisholm den Charakter des Langhorne im Film Menschenkind. Größere Bekanntheit erreichte er in den Jahren 2001 bis 2003, als er an insgesamt drei Staffeln der Serie Oz teilnahm. 2012 folgte seine Rolle als Tito in Premium Rush.

Darüber hinaus ist Chisholm auch im Theater aktiv. So spielte er im Stück Tracers im Seymoure Center in Sydney, andere Rollen hat er unter anderem in den Theatern von Melbourne sowie am Royal Court Theatre in London gespielt. Dort gewann er den Laurence Olivier Award in der Kategorie Best New Play.
