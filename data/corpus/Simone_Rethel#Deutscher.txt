Simone Rethel-Heesters (* 15. Juni 1949 in Herrsching am Ammersee) ist eine deutsche Schauspielerin und Buchautorin.

Simone Rethel ist die Tochter des Malers und Designers Alfred Rethel und Enkelin des Flugzeugkonstrukteurs Walter Rethel sowie Nachfahrin des Historienmalers Otto Rethel, des Bruders von Alfred Rethel.

Als Schülerin spielte sie 1965 die Hauptrolle in dem Film Die fromme Helene unter der Regie von Axel von Ambesser (mit Theo Lingen und Friedrich von Thun). Nach ihrer Schulzeit absolvierte sie eine Schauspielausbildung bei Hanna Burgwitz und debütierte noch während ihrer Ausbildung 1966 am Bayerischen Staatsschauspiel in München in Die Freier von Joseph von Eichendorff,– wieder unter der Regie von Axel von Ambesser, der seine Entdeckung immer wieder für die Bühne und das Fernsehen für verschiedene Rollen engagierte.

Rethel tritt vor allem auf Boulevard-Bühnen auf und wirkte in zahlreichen TV-Serien wie Aktenzeichen XY … ungelöst, Der Kommissar, Derrick, Der Alte, Schöne Ferien und Diese Drombuschs mit.

Von 1992 bis zu dessen Tod im Jahr 2011 war Simone Rethel mit dem 46 Jahre älteren Johannes Heesters verheiratet.[1]
Neben ihrer Arbeit als Schauspielerin widmet sie sich auch der Malerei und der Fotografie. Seit Anfang 2005 ist Simone Rethel-Heesters als Botschafterin für die Initiative Altern in Würde tätig und ist bemüht, das Thema Alzheimersche Krankheit mehr in die Öffentlichkeit zu bringen. Rethel-Heesters brachte Anfang 2010 das Sachbuch Sag nie, du bist zu alt heraus, in dem sie das negative gesellschaftliche Bild des Älterwerdens kritisiert. Sie fordert darin, im Alter weiterhin aktiv zu sein und nicht aufzuhören, sich weiterhin Ziele im Leben zu stecken.
