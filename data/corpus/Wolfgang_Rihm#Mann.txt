Wolfgang Michael Rihm (* 13. März 1952 in Karlsruhe) ist ein deutscher Komponist, Musikwissenschaftler und Essayist.

Wolfgang Rihm wuchs in Karlsruhe auf. Angeregt durch frühe Begegnungen mit Malerei, Literatur und Musik begann er 1963 zu komponieren. Bereits während seiner Schulzeit am humanistischen Bismarck-Gymnasium studierte er von 1968 bis 1972 Komposition bei Eugen Werner Velte an der Hochschule für Musik Karlsruhe. Er beschäftigte sich mit der Musik der Zweiten Wiener Schule, instrumentierte Arnold Schönbergs Klavierstücke op. 19 und orientierte sich vorübergehend am aphoristisch-knappen Stil Anton Weberns. Weitere Kompositionslehrer von Wolfgang Rihm waren Wolfgang Fortner und Humphrey Searle. Parallel zum Abitur legte er 1972 das Staatsexamen in Komposition und Musiktheorie an der Musikhochschule ab. Es folgten Studien bei Karlheinz Stockhausen 1972/73 in Köln sowie von 1973 bis 1976 an der Hochschule für Musik Freiburg bei Klaus Huber (Komposition) und Hans Heinrich Eggebrecht (Musikwissenschaft). Erste eigene Erfahrung als Dozent sammelte Rihm 1973 bis 1978 in Karlsruhe, ab 1978 bei den Darmstädter Ferienkursen (die er seit 1970 besucht hatte) und 1981 an der Musikhochschule München. 1985 übernahm er als Nachfolger seines Lehrers Eugen Werner Velte den Lehrstuhl für Komposition an der Musikhochschule Karlsruhe.

Nach der Aufführung seines Orchesterstücks Morphonie – Sektor IV bei den Donaueschinger Musiktagen 1974 fand Rihm in den Folgejahren breite Anerkennung innerhalb des Musikbetriebs. Seit 1982 ist er Präsidiumsmitglied des Deutschen Komponistenverbands, seit 1984 Präsidiumsmitglied des Deutschen Musikrats, seit 1985 Kuratoriumsmitglied der Heinrich Strobel-Stiftung, seit 1989 gehört er dem Aufsichtsrat der GEMA an. 1984/85 und 1997 war er Fellow am Wissenschaftskolleg zu Berlin und Präsidiumsmitglied des Deutschen Musikrats, 1984 bis 1989 Mitherausgeber der Musikzeitschrift Melos, 1984 bis 1990 musikalischer Berater der Deutschen Oper Berlin, 1990 bis 1993 musikalischer Berater des Zentrums für Kunst und Medientechnologie (ZKM) in Karlsruhe. Auf Einladung von Walter Fink war er 1995 der fünfte Komponist im jährlichen Komponistenporträt des Rheingau Musik Festival. Die Freie Universität Berlin würdigte ihn 1998 mit einer Ehrendoktorwürde als Künstler, der „in seinem überaus umfangreichen kompositorischen Werk die Freiheit des Kreativen verkörpert und für eine Ästhetik der Freiheit der Kunst eintritt, der zahlreiche, theoretisch fundierte Schriften verfasst hat, die außerordentliche musikwissenschaftliche Bedeutung besitzen.“[1] 2013/2014 war er Capell-Compositeur der Sächsischen Staatskapelle Dresden. Im Sommer 2016 übernahm Rihm die künstlerische Gesamtleitung der von Pierre Boulez gegründeten Lucerne Festival Academy.[2]
Wolfgang Rihm lebt in Karlsruhe und Berlin. Er hat einen Sohn und eine Tochter.[3]
Als Komponist und Musikschriftsteller vertritt Rihm eine Ästhetik, die das subjektive Ausdrucksbedürfnis in den Mittelpunkt stellt. Vorbilder in diesem Sinn waren für ihn Hans Werner Henze[4], später Karlheinz Stockhausen und noch später Luigi Nono. Darüber hinaus vermittelten ihm literarische Texte wichtige Impulse: die Lyrik Paul Celans, die Philosophie Friedrich Nietzsches, die Theater-Schriften von Antonin Artaud und Heiner Müller. Die von James Joyce formulierte Idee eines „work in progress“ hat ihn insofern beeinflusst, als er seine Stücke gern als Provisorien („Versuche“) betrachtet, die – durch Erweiterung, Ergänzung, Tropierung, Vernetzung und Verflechtung des einmal entwickelten Materials – einander fortlaufend korrigieren oder ergänzen können. Rihm benutzt hierfür gern Metaphern aus der bildenden Kunst, er spricht von „Übermalungen“ oder von Bildhauerei: „Ich habe die Vorstellung eines großen Musikblocks, der in mir ist. Jede Komposition ist zugleich ein Teil von ihm, als auch eine in ihn gemeißelte Physiognomie.“[5]
Vergleichbare Verfahren gibt es unter anderem im Schaffen von Pierre Boulez (dieser spricht von „Ableitungen“ und „Wucherungen“). 1973 lernt Rihm den österreichischen Maler Kurt Kocherscheidt kennen, dessen offene, radikale Art des Zeichnens ihn unmittelbar angesprochen hat. Beeinflusst haben ihn ferner junge Künstler der Kunstakademie Karlsruhe, die seit den 1970er Jahren dort gelehrt haben und später zu den bedeutendsten Vertretern der deutschen Malerei der 1980er Jahre werden sollten, darunter Markus Lüpertz, Georg Baselitz oder Per Kirkeby.

Stilistisch lassen sich grob drei Perioden im Schaffen von Rihm unterscheiden: Seine frühen Stücke knüpfen an eine Tradition an, die sich von den späten Instrumentalwerken Beethovens hin zu Schönberg, Berg und Webern spannt. Wegen ihrer dezidierten Subjektivität wurde Rihms Musik damals gelegentlich der sogenannten Neuen Einfachheit zugerechnet. Ab den 1980er Jahren entwickelt sich ein lakonisch-ausdrucksknapper Stil; Klänge werden als Zeichen („Chiffren“) gedeutet, im Sinne einer neuen Erforschung musikalischer Semantik. Ab den 1990er Jahren erscheinen schließlich diese beiden Positionen als These und Antithese zugespitzt; zugleich sucht Rihm immer wieder Möglichkeiten einer Synthese. Zunehmende Prägnanz der musikalischen Formulierung lässt Gebilde von hoher Virtuosität entstehen.

Neben zahlreichen Kompositionen für kleinere Besetzungen und drei Symphonien schreibt er Bühnenwerke. Von ihm wurde zunächst die Kammeroper Faust und Yorick (1976; mit einem Libretto von Frithjof Haas nach dem gleichnamigen Stück von Jean Tardieu) bekannt. Zwischen 1983 und 1986 folgte Die Hamletmaschine, ein Musiktheaterstück in fünf Teilen mit einem Libretto nach dem gleichnamigen Theaterstück und 1987 Oedipus nach bzw. mit Texten von Sophokles. 1992 brachte die Hamburger Staatsoper Die Eroberung von Mexico von ihm auf die Bühne. Proserpina (2009), nach dem gleichnamigen Stück von Johann Wolfgang von Goethe im  Schwetzinger Schlosstheater, die über Nietzsche phantasierende Oper Dionysos (2010; Salzburg, Berlin, Amsterdam) und ein Hornkonzert (2013–2014) sind seine neueren großen Arbeiten. Rihm wurde mit einer Komposition für die Auftaktveranstaltung in der Hamburger Elbphilharmonie im Großen Saal am 11. Januar 2017 beauftragt. Das NDR Elbphilharmonie Orchester mit T. Hengelbrock als Dirigent brachte das/den Triptychon und Spruch in memoriam Hans Henny Jahnn dank Radio-, Fernseh- und Online-Übertragungen vor einem europaweiten Millionenpublikum zur Uraufführung.[6] Rihm hat Hans Henny Jahnn nie getroffen[7].

Bücher

Einzeltexte

Monographien, Sammelbände

Einzelstudien, Aufsätze

Hans Peter Haller (1989) |
Pierre Boulez (1990) |
Steffen Schleiermacher (1991) |
György Ligeti (1992) |
André Richard (1994) |
Robyn Schulkowsky (1995) |
Wolfgang Rihm (1996) |
Mario Davidovsky (1997) |
Hans-Jürgen von Bose (1998) |
Gottfried Michael Koenig (1999) |
Péter Eötvös (2000) |
Kaija Saariaho (2001) |
Christoph Poppen (2002) |
Aleksandra Gryka und Mateusz Bien (2004) |
Márton Illés (2005) |
Mark Andre (2006) |
Jörg Widmann (2007) |
Minas Borboudakis und Konstantia Gourzi (2008) |
Enno Poppe (2009) |
Wilhelm Killmayer (2010) |
Adriana Hölszky (2011) |
Josef Anton Riedl, Nico Sauer und Luis Codera Puzo (2013) |
Isabel Mundry (2014) |
Erkki-Sven Tüür (2015) |
Georges Aperghis (2016) |
Anna Korsun (2017)

Benjamin Britten (1974) |
Olivier Messiaen (1975) |
Mstislaw Rostropowitsch (1976) |
Herbert von Karajan (1977) |
Rudolf Serkin (1978) |
Pierre Boulez (1979) |
Dietrich Fischer-Dieskau (1980) |
Elliott Carter (1981) |
Gidon Kremer (1982) |
Witold Lutosławski (1983) |
Yehudi Menuhin (1984) |
Andrés Segovia (1985) |
Karlheinz Stockhausen (1986) |
Leonard Bernstein (1987) |
Peter Schreier (1988) |
Luciano Berio (1989) |
Hans Werner Henze (1990) |
Heinz Holliger (1991) |
H. C. Robbins Landon (1992) |
György Ligeti (1993) |
Claudio Abbado (1994) |
Sir Harrison Birtwistle (1995) |
Maurizio Pollini (1996) |
Helmut Lachenmann (1997) |
György Kurtág (1998) |
Arditti Quartet (1999) |
Mauricio Kagel (2000) |
Reinhold Brinkmann (2001) |
Nikolaus Harnoncourt (2002) |
Wolfgang Rihm (2003) |
Alfred Brendel (2004) |
Henri Dutilleux (2005) |
Daniel Barenboim (2006) |
Brian Ferneyhough (2007) |
Anne-Sophie Mutter (2008) |
Klaus Huber (2009) |
Michael Gielen (2010) |
Aribert Reimann (2011) |
Friedrich Cerha (2012) |
Mariss Jansons (2013) |
Peter Gülke (2014) |
Christoph Eschenbach (2015) |
Per Nørgård (2016) |
Pierre-Laurent Aimard (2017)
