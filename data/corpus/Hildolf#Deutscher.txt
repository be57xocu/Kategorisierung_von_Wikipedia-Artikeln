Hildolf (* vor 1061; † 21. Juli 1078) war von 1076 bis 1078 Erzbischof des Erzbistums Köln.

Seine Herkunft und Abstammung sind nicht abschließend geklärt. Da er nur in sehr wenigen Dokumenten erwähnt wird, nimmt man an, dass er von niederer Herkunft war. Das Xantener Totenbuch bezeichnet ihn als „Bruder“, so dass er möglicherweise vom Niederrhein stammt.

Hildolf wurde, bevor er Erzbischof von Köln wurde, als Kanoniker in Goslar erwähnt.

Er war der Hofkaplan von König Heinrich IV. und hatte von diesem 1076 auch das Amt des Erzbischofs von Köln erhalten. Hildolf selbst hatte sich gegen diese Ernennung gesträubt, war aber schließlich dem Wunsch seines Königs gefolgt. Auch die Kölner sperrten sich gegen die Wahl des Kaisers und bezeichneten Hildolf als klein, unansehnlich, von dunkler Herkunft und bei Hofe unbeliebt. Schließlich beugten sich die Kölner Gesandten, so dass Heinrich IV. ihn noch in Goslar durch den Bischof von Utrecht weihen ließ. Angeblich versprach er den Verwandten des Bischofs hierfür das Bistum Paderborn.

Auch nach seiner Wahl weilte Hildolf zumeist bei Hofe. Ende Oktober 1076 wurde er vom Hofe entlassen, da der Kaiser auf Druck der Fürsten seine Räte entlassen musste. Als er kurz darauf seine alten Räte wieder zurückholte, war Hildolf nicht mehr unter ihnen.

Bei Hildolfs Einzug in die Stadt Köln kam es zu Ausschreitungen der Kölner Bürger, die zwei Jahre zuvor bereits seinen Vorgänger Erzbischof Anno II. mit einem Aufstand aus der Stadt vertrieben hatten.

Angeblich war Hildolf ab Sommer 1076 unter den von Papst Gregor VII. Gebannten, da er zu den Anhängern Heinrich IV. gehörte. Dem entspricht, dass er bis zu seinem Tod das Pallium nie erhalten hat. Trotzdem wurde er in seiner Stadt anerkannt.

Hildof wurde im Kölner Dom beigesetzt.
