Rita Mae Brown (* 28. November 1944 in Hanover, Pennsylvania) ist eine US-amerikanische Schriftstellerin und frühere Aktivistin in der lesbischen Frauenbewegung der USA. Zu Beginn ihrer schriftstellerischen Laufbahn verfasste sie vor allem feministische Bücher, seit Anfang der 1990er Jahre sind es vor allem Kriminalromane, obschon auch noch andersgeartete Werke erscheinen.

Rita Mae Brown wurde als uneheliches Kind in Hanover, Pennsylvania geboren und bald nach ihrer Geburt von Verwandten ihrer leiblichen Mutter adoptiert.[1] Bis ihre Familie im Sommer 1955[2] nach Fort Lauderdale, Florida umsiedelte, wuchs Brown in York,[3] Pennsylvania auf. Sie studierte an der University of Florida und in New York Anglistik und Kinematographie. Sie war aktiv in der Frauenbewegung; 1970 war sie eine der Begründerinnen der Radicalesbians. (Zu ihrer Rolle in deren Vorgeschichte siehe Lavender Menace.)

Berühmt wurde sie mit Rubinroter Dschungel und nicht zuletzt durch ihre Kriminalromane mit der Tigerkatze Sneaky Pie Brown als Koautorin. Auf Deutsch sind inzwischen 21 Romane über die Abenteuer der Tigerkatze Mrs. Murphy, ihrer Freundin, der Hündin Tee Tucker und ihr Frauchen Mary Minor Haristeen, genannt Harry, erschienen. Außerdem gibt es eine Verfilmung unter dem Titel „Detektiv auf Samtpfoten“. Die Handlung spielt in Crozet, Virginia. Zu ihren Romanen fiel Reclams Krimi-Lexikon lediglich folgende ambivalente Charakterisierung ein: „Katzen sind Erkennungszeichen lesbischer Krimiautorinnen bzw. -heldinnen, ähnlich wie Kriminalhauptkommissare gerne Pfeife rauchen.“[4]
Rita Mae Brown führte Beziehungen mit Martina Navrátilová (die sie in ihrem Roman Die Tennisspielerin aufarbeitete), der Schriftstellerin Fannie Flagg und der Politikerin Elaine Noble.

Rita Mae Brown lebt als Schriftstellerin und Drehbuchautorin auf einer Farm in Charlottesville, Virginia.

(ins Deutsche übersetzt von Margarete Längsfeld)

Nur auf Englisch erhältlich:
