Benjamin Biolay [bɛ̃ʒaˈmɛ̃ bjɔˈlɛ] (* 20. Januar 1973 in Villefranche-sur-Saône) ist ein französischer Sänger und Schauspieler. Zusammen mit einigen anderen Interpreten, wie Dominique A oder Philippe Katerine, zählt er zu den bekanntesten Vertretern des Nouvelle Chanson.

Biolay hat eine klassische Ausbildung für Tuba und Violine am Konservatorium Lyon absolviert. Anfang der 1990er Jahre wandte er sich der Popmusik zu, wobei seine erste Band „Mateo Gallion“ wenig erfolgreich war. Ab 1996 arbeitete er als Solokünstler mit einem Vertrag bei EMI. Erst im Duo mit Keren Ann wurde er 1999 bekannter. Die beiden schrieben Stücke für das Comeback-Album „Chambre avec vue“ (2000) von Henri Salvador.

Neben eigenen Werken wurde Biolay vor allem durch die Zusammenarbeit mit namhaften Künstlerinnen wie Isabelle Boulay und Valérie Lagrange erfolgreich, an deren Comeback-Album von 2003 er mitarbeitete. Für seine Schwester Coralie Clément schrieb und produzierte er die drei Alben „Salle de pas perdus“ (2001), „Bye Bye Beauté“ (2004) und „Toystore“ (2008). Auf dem Album „Home“ arbeitete er mit seiner Frau Chiara Mastroianni, von der er inzwischen geschieden ist.

Biolay gilt als Vertreter des „Neuen Chanson“, wobei er selbst mit dem Begriff wenig anfangen kann.

Er ist auch als Schauspieler aktiv; sein Debüt gab er 2004 in dem Film Why (Not) Brazil?. 2008 war er in dem Film Stella zu sehen.

2013 wurde er president-du-jury-de-la-6eme-edition-du-festival-d-Angouleme.

Weitere Alben

Weitere Singles
